﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using Attachment = System.Net.Mail.Attachment;

namespace Fruitful.Email
{
    public class Emailer
    {
        private static readonly string _sendGridApiKey = "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";
        
        private readonly EmailSetting _settings;

        public Emailer(DbContext db)
        {
            _settings = db.Set<EmailSetting>().First();
        }

        public bool SendEmail(MailMessage message, bool ccSupport = true)
        {
            try
            {
                message.From = new MailAddress(_settings.Email);
                message.Body += "<br />" +
                                "<table width='100%' cellspacing='0' cellpadding='0' border='0'>" +
                                    "<tr>" +
                                        "<td colspan='3' height='30' bgcolor='#f78f3a' style='min-width: 500px;'>" +
                                            "<table width='100%' cellspacing='0' cellpadding='0' border='0'>" +
                                                "<tr>" +
                                                    "<td width='1%'></td>" +
                                                    "<td width='98%' height='30'>" +
                                                        "<font style='text-align: left; font-family: Helvetica,Arial,sans-serif; color: #ffffff; font-size: 15px; line-height: 30px; font-weight: bold;'>People Service Product</font>" +
                                                    "</td>" +
                                                    "<td width='1%'></td>" +
                                                "</tr>" +
                                            "</table>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td colspan='3' height='30' bgcolor='#8dc63e'></td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td colspan='3' height='20'></td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td colspan='3'>" +
                                            "<font style='text-align: left; font-family: Helvetica,Arial,sans-serif; font-size: 10px; font-weight: normal; color: #000000;'>" +
                                                "<span style='display: block;'>This email, and any attachment, contains information which may be confidential, legally privileged and protected by copyright, and is intended solely for the attention and use of the named addressee(s). If you are not the intended recipient you must not use, disclose, copy, distribute or retain this message or any part of it. If you do receive this email in error, please destroy any hard copies, delete the email and any attachments to it from your system and notify the sender immediately. The contents of this message may contain personal views which are not the view of Office-Groceries. Although we have taken steps to ensure that this email and any attachments to it are free from any virus, you should, in keeping with good computer practice, ensure that they are actually virus free.</span>" +
                                            "</font>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>";
                if (ccSupport)
                {
                    message.Bcc.Add("min@quikflw.com");
#if !DEBUG
                    message.Bcc.Add("support@office-groceries.com");
#endif
                }

                // client.Send(message);
                SendBySendGrid(message);
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        private void SendBySendGrid(MailMessage mailMessage)
        {
            var sendGridClient = new SendGridClient(_sendGridApiKey);
            var from = new EmailAddress("orders@office-groceries.com", "Office-Groceries");
            var sendGridMail = new SendGridMessage();
            sendGridMail.SetFrom(from);
            sendGridMail.SetReplyTo(from);
            sendGridMail.HtmlContent = mailMessage.Body;
            sendGridMail.SetSubject(mailMessage.Subject);
            
            // SendGrid does not allow duplicate email addresses in the To, Cc and Bcc fields
            var emailAddresses = new HashSet<EmailAddress>();

            AddEmails(mailMessage.To, emailAddresses, (emailAddress) => sendGridMail.AddTo(emailAddress));
            AddEmails(mailMessage.Bcc, emailAddresses, (emailAddress) =>sendGridMail.AddBcc((emailAddress)));
            AddEmails(mailMessage.CC, emailAddresses, (emailAddress) => sendGridMail.AddCc((emailAddress)));
            
            if (mailMessage.Attachments.Count > 0)
                sendGridMail.Attachments = mailMessage.Attachments.Select(x =>
                {
                    byte[] byteArray;
                    using (var ms = new MemoryStream())
                    {
                        x.ContentStream.CopyTo(ms);
                        byteArray = ms.ToArray();
                    }
                    return new SendGrid.Helpers.Mail.Attachment
                    {
                        Content = Convert.ToBase64String(byteArray),
                        Type = x.ContentType.Name,
                        Filename = x.Name,
                        Disposition = "attachment",
                        ContentId = x.ContentId
                    };
                }).ToList();

            var result = sendGridClient.SendEmailAsync(sendGridMail).ConfigureAwait(false).GetAwaiter().GetResult();
            if (!result.IsSuccessStatusCode)
            {
                var errorMsg = result.Body.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter().GetResult();
                throw new Exception(errorMsg);
            }
        }

        private void AddEmails(MailAddressCollection addresses, HashSet<EmailAddress> existingEmails, Action<EmailAddress> addEmail)
        {
            foreach (var address in addresses.Select(x => new EmailAddress(x.Address, x.DisplayName)))
            {
                if (existingEmails.Contains(address))
                {
                    continue;
                }

                existingEmails.Add(address);
                addEmail(address);
            }
        }
        
        public MailMessage CreateMailMessage(string email, string subject, string body, IEnumerable<Attachment> attachments)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(email);
            mailMessage.From = new MailAddress(_settings.Email);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;

            foreach (var attachment in attachments)
            {
                mailMessage.Attachments.Add(attachment);
            }

            return mailMessage;
        }
        
        public MailMessage CreateMailMessage(IEnumerable<string> emailList, string subject, string body, IEnumerable<Attachment> attachments)
        {
            MailMessage mailMessage = new MailMessage();
            foreach (var email in emailList)
            {
                mailMessage.To.Add(email);
            }
            mailMessage.From = new MailAddress(_settings.Email);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;

            foreach (var attachment in attachments)
            {
                mailMessage.Attachments.Add(attachment);
            }

            return mailMessage;
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
