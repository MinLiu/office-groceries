﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Fruitful.Utils.Encrypts
{
    public static class Sha1
    {
        public static string Hash(string plainText)
        {
            var textBytes = Encoding.UTF8.GetBytes(plainText);
            var sha1Managed = new SHA1Managed();
            var hashBytes = sha1Managed.ComputeHash(textBytes);
            var hashString = string.Empty;
            var s = string.Join("", hashBytes.Select(x => $"{x:x2}"));
            foreach (var x in hashBytes)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}