﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Fruitful.Import
{
    public static class ImportService
    {
        public static DataTable CopyExcelFileToTable(string filePath, bool deleteFile, string sheetName = null)
        {
            try
            {
                using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
                {
                    IExcelDataReader excelReader = null;

                    if (Path.GetExtension(filePath) == ".xlsx")
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    else if (Path.GetExtension(filePath) == ".csv")
                        excelReader = ExcelReaderFactory.CreateCsvReader(stream);
                    else
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                    var result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        FilterSheet = (tableReader, sheetIndx) => sheetName == null ? true : tableReader.Name == sheetName,
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });

                    if (result.Tables.Count == 0)
                        throw new Exception(string.Format("Can not find sheet '{0}'", sheetName));

                    DataTable table = result.Tables[0];

                    return table;
                }
            }
            finally
            {
                // Delete the file from the server to free up space if required.
                if (deleteFile)
                {
                    try { File.Delete(filePath); }
                    catch { }
                }
            }
        }

        public static bool IsTableValid(DataTable table, string[] columns, out string message)
        {
            bool valid = true;
            message = String.Empty;
            var missingColumns = new List<string>();

            // Check all columns exist and are labelled correctly. 
            foreach (var label in columns)
            {
                if (!table.Columns.Contains(label))
                {
                    valid = false;
                    missingColumns.Add(label);
                }
            }

            if (!valid)
            {
                message = String.Format("Import file is missing the following columns: '{0}'", String.Join(",", missingColumns));
                return false;
            }

            return valid;
        }
    }

}
