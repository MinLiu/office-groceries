﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkProductRepository : EntityRespository<MilkProduct>
    {
        public MilkProductRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkProductRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(MilkProduct entity)
        {
            var set = _db.Set<MilkProduct>().Where(a => a.ID == entity.ID);

            foreach (var milk in set)
            {
                milk.Deleted = true;
            }

            var milkOrderItems = _db.Set<MilkOrder>().Where(x => x.MilkProductID == entity.ID)
                                                     .Where(x => x.OrderHeader.OrderMode == OrderMode.Regular || x.OrderHeader.OrderMode == OrderMode.OneOff);
            _db.Set<MilkOrder>().RemoveRange(milkOrderItems);

            _db.SaveChanges();
        }

    }

}
