﻿namespace NuIngredient.Models
{
    public class MilkExcludeCompanyRepository : EntityRespository<MilkExcludeCompany>
    {
        public MilkExcludeCompanyRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkExcludeCompanyRepository(SnapDbContext db)
            : base(db)
        {

        }

    }
}