﻿namespace NuIngredient.Models
{
    public class MilkProductTagPriceRepository : EntityRespository<MilkProductTagPrice>
    {
        public MilkProductTagPriceRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkProductTagPriceRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
