﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkProductExclusivePriceRepository : EntityRespository<MilkProductExclusivePrice>
    {
        public MilkProductExclusivePriceRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkProductExclusivePriceRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
