﻿namespace NuIngredient.Models
{
    public class TagRepository : EntityRespository<Tag>
    {
        public TagRepository()
            : this(new SnapDbContext())
        {

        }

        public TagRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
