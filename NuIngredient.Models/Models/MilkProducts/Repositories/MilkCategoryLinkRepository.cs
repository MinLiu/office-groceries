﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkCategoryLinkRepository : EntityRespository<MilkCategoryLink>
    {
        public MilkCategoryLinkRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkCategoryLinkRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
