﻿using System.Linq;

namespace NuIngredient.Models
{
    public class MilkProductCategoryRepository : EntityRespository<MilkProductCategory>
    {
        public MilkProductCategoryRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkProductCategoryRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(MilkProductCategory entity)
        {
            // Delete subcategories
            var subCategories = _db.Set<MilkProductCategory>().Where(x => x.ParentID == entity.ID).ToList();
            foreach (var subCategory in subCategories)
            {
                Delete(subCategory);
            }

            // Delete milk products link to this category
            var milkProductRepo = new MilkProductRepository();
            var milkProducts = milkProductRepo.Read().Where(x => x.CategoryID == entity.ID).ToList();
            foreach (var milkproduct in milkProducts)
            {
                milkProductRepo.Delete(milkproduct);
            }

            // Delete this entity
            entity = _db.Set<MilkProductCategory>().Find(entity.ID);
            entity.Deleted = true;

            _db.SaveChanges();
        }

    }

}
