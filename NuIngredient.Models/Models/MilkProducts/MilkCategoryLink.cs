﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class MilkCategoryLink : IEntity
    {
        public MilkCategoryLink() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid MilkProductID { get; set; }
        public int SortPos { get; set; }

        // Navigations
        public virtual MilkProductCategory Category { get; set; }
        public virtual MilkProduct MilkProduct { get; set; }
    }

}
