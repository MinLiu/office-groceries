﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class Tag : IEntity
    {
        public Tag() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string HexColour { get; set; }
    }

    public class TagGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string HexColour { get; set; }
    }

    public class TagGridMapper : ModelMapper<Tag, TagGridViewModel>
    {
        public override void MapToViewModel(Tag model, TagGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(TagGridViewModel viewModel, Tag model)
        {
            model.Name = viewModel.Name;
            model.HexColour = viewModel.HexColour;
        }
    }
}
