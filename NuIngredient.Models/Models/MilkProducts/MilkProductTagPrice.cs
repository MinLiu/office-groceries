﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class MilkProductTagPrice : IEntity
    {
        public MilkProductTagPrice() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public Guid TagID { get; set; }
        public decimal Price { get; set; }

        public virtual MilkProduct Product { get; set; }
        public virtual Tag Tag { get; set; }
    }

    public class MilkProductTagPriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public TagGridViewModel Tag { get; set; }
        public decimal Price { get; set; }
    }

    public class MilkProductTagPriceMapper : ModelMapper<MilkProductTagPrice, MilkProductTagPriceViewModel>
    {
        private static readonly TagGridMapper TagMapper = new TagGridMapper();
        public override void MapToViewModel(MilkProductTagPrice model, MilkProductTagPriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Tag = TagMapper.MapToViewModel(model.Tag);
            viewModel.Price = model.Price;
        }

        public override void MapToModel(MilkProductTagPriceViewModel viewModel, MilkProductTagPrice model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.TagID = Guid.Parse(viewModel.Tag.ID);
            model.Price = viewModel.Price;

        }
    }
}
