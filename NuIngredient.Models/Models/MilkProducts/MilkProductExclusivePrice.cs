﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class MilkProductExclusivePrice : IEntity
    {
        public MilkProductExclusivePrice() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        [ForeignKey("Customer")]
        public Guid CusomterID { get; set; }
        public decimal Price { get; set; }
        public virtual MilkProduct Product { get; set; }
        public virtual Company Customer { get; set; }
    }




    public class MilkProductExclusivePriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public decimal Price { get; set; }
        public SelectItemViewModel Customer { get; set; }
    }



    public class MilkProductExclusivePriceMapper : ModelMapper<MilkProductExclusivePrice, MilkProductExclusivePriceViewModel>
    {
        public override void MapToViewModel(MilkProductExclusivePrice model, MilkProductExclusivePriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Price = model.Price;
            viewModel.Customer = new SelectItemViewModel() { ID = model.Customer.ID.ToString(), Name = model.Customer.Name };
        }

        public override void MapToModel(MilkProductExclusivePriceViewModel viewModel, MilkProductExclusivePrice model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.CusomterID = Guid.Parse(viewModel.Customer.ID);
            model.Price = viewModel.Price;
        }

    }
}
