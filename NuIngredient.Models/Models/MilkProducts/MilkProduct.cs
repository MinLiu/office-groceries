﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class MilkProduct : IEntity
    {
        public MilkProduct() { ID = Guid.NewGuid(); IsActive = true; IsProspectsVisible = true; }
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid? AisleID { get; set; }
        public Guid? CategoryID { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool Deleted { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public decimal VAT { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        public int SortPosition { get; set; }
        

        // Navigations
        public virtual MilkProductCategory Category { get; set; }
        public virtual ICollection<MilkProductExclusivePrice> ExclusivePrices { get; set; }
        public virtual ICollection<MilkProductTagPrice> TagPrices { get; set; }
        public virtual ICollection<MilkCategoryLink> Categories { get; set; }
        public virtual ICollection<MilkExcludeCompany> ExcludeCompanies { get; set; }
        public virtual ICollection<SupplierMilkProductCode> SupplierProductCodes { get; set; }

        // Methods
        public void GetPrice(Company company)
        {
            if (company == null)
                return;

            if (TryGetCustomPrice(out var customPrice))
            {
                Price = customPrice.Value;
                return;
            }

            if (TryGetTagPrice(out var tagPrice))
            {
                Price = tagPrice.Value;
                return;
            }

            bool TryGetCustomPrice(out decimal? price)
            {
                var exclusivePrice = ExclusivePrices.FirstOrDefault(x => x.CusomterID == company.ID);
                price = exclusivePrice?.Price;
                return exclusivePrice != null;
            }

            bool TryGetTagPrice(out decimal? price)
            {
                price = null;
                if (!company.TagItems.Any())
                    return false;

                var companyTagIds = company.TagItems.Select(t => t.TagID).ToList();
                var tPrice = TagPrices?.Where(t => companyTagIds.Contains(t.TagID))
                    .OrderByDescending(x => x.Price)
                    .FirstOrDefault();

                price = tPrice?.Price;

                return tPrice != null;
            }
        }

        public decimal ExclusivePrice(Company company)
        {
            if (company == null)
                return Price;

            if (TryGetCustomPrice(out var customPrice))
                return customPrice.Value;

            if (TryGetTagPrice(out var tagPrice))
                return tagPrice.Value;

            return Price;

            bool TryGetCustomPrice(out decimal? price)
            {
                var exclusivePrice = ExclusivePrices.FirstOrDefault(x => x.CusomterID == company.ID);
                price = exclusivePrice?.Price;
                return exclusivePrice != null;
            }

            bool TryGetTagPrice(out decimal? price)
            {
                price = null;
                if (!company.TagItems.Any())
                    return false;

                var companyTagIds = company.TagItems.Select(t => t.TagID).ToList();
                var tPrice = TagPrices?.Where(t => companyTagIds.Contains(t.TagID))
                    .OrderByDescending(x => x.Price)
                    .FirstOrDefault();

                price = tPrice?.Price;

                return tPrice != null;
            }
        }

        public decimal ExclusivePrice(Guid? companyID)
        {
            Company company = null;
            if (companyID != null)
            {
                company = new CompanyRepository().Find(companyID.Value);
            }
            return ExclusivePrice(company);
        }
    }

    #region EditProductViewModel

    public class EditMilkProductViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        [Required(ErrorMessage = "* The Product Name field is required.")]
        [Display(Name = "Product Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "* The Product Code field is required.")]
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        public string ShortDescription { get; set; }

        [AllowHtml]
        public string Description { get; set; }
        public string AisleID { get; set; }

        public string CategoryID { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public decimal Cost { get; set; }
        public decimal VAT { get; set; }

        public List<string> CategoryIDs { get; set; }
    }

    public class EditMilkProductModelMapper : ModelMapper<MilkProduct, EditMilkProductViewModel>
    {
        public override void MapToModel(EditMilkProductViewModel viewModel, MilkProduct model)
        {
            //model.CategoryID = Guid.Parse(viewModel.CategoryID);
            model.ProductCode = viewModel.ProductCode;
            model.Name = viewModel.Name;
            model.ShortDescription = viewModel.ShortDescription;
            model.Description = viewModel.Description;
            model.IsExclusive = viewModel.IsExclusive;
            model.IsProspectsVisible = viewModel.IsProspectsVisible;
            model.IsActive = viewModel.IsActive;
            model.Price = viewModel.Price;
            model.Cost = viewModel.Cost;
            model.Unit = viewModel.Unit;
            model.VAT = viewModel.VAT;
        }
        public override void MapToViewModel(MilkProduct model, EditMilkProductViewModel viewModel)
        {
            viewModel.CategoryID = model.Category != null ? model.Category.ID.ToString() : "";
            viewModel.Cost = model.Cost;
            viewModel.ShortDescription = model.ShortDescription;
            viewModel.Description = model.Description;
            viewModel.ImageURL = model.ImageURL;
            viewModel.ID = model.ID.ToString();
            viewModel.IsActive = model.IsActive;
            viewModel.IsExclusive = model.IsExclusive;
            viewModel.IsProspectsVisible = model.IsProspectsVisible;
            viewModel.Name = model.Name;
            viewModel.Price = model.Price; 
            viewModel.ProductCode = model.ProductCode; 
            viewModel.Unit = model.Unit;
            viewModel.VAT = model.VAT;
            viewModel.CategoryIDs = model.Categories?.Select(x => x.CategoryID.ToString()).ToList() ?? new List<string>();
        }
    }

    #endregion

    #region ProductGridViewModel

    public class MilkProductGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string CategoryName { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
        public decimal Cost { get; set; }
    }

    public class MilkProductGridMapper : ModelMapper<MilkProduct, MilkProductGridViewModel>
    {
        public override void MapToViewModel(MilkProduct model, MilkProductGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductCode = model.ProductCode;
            viewModel.Name = model.Name;
            viewModel.ShortDescription = model.ShortDescription;
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.Price = model.Price;
            viewModel.VAT = model.VAT;
            viewModel.Cost = model.Cost;
            viewModel.IsActive = model.IsActive;
            viewModel.IsExclusive = model.IsExclusive;
            viewModel.IsProspectsVisible = model.IsProspectsVisible;
            viewModel.CategoryName = String.Join("\n", model.Categories.Select(c => c.Category.Name).OrderBy(c => c).ToList());
        }


        public override void MapToModel(MilkProductGridViewModel viewModel, MilkProduct model)
        {
            throw new NotImplementedException();
        }

    }

    #endregion

    #region ProductListViewModel

    public class MilkProductListViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
    }

    public class MilkProductListViewMapper : ModelMapper<MilkProduct, MilkProductListViewModel>
    {
        public override void MapToModel(MilkProductListViewModel viewModel, MilkProduct model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(MilkProduct model, MilkProductListViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.Description = !string.IsNullOrEmpty(model.Description)? HttpUtility.HtmlEncode(model.Description).Replace("#", "\\#") : "";
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.ShortDescription = model.ShortDescription ?? "";
            viewModel.Unit = model.Unit ?? "";
            viewModel.Price = model.Price;
            viewModel.VAT = model.VAT;
        }

        public MilkProductListViewModel MapToViewModel(MilkProduct model, Company company)
        {
            var viewModel = new MilkProductListViewModel();
            model.GetPrice(company); // Get Exclusive price
            MapToViewModel(model, viewModel);

            return viewModel;
        }
    }

    #endregion

    public class MilkProductViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductName { get; set; }

        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
    }
    public class MilkProductMapper : ModelMapper<MilkProduct, MilkProductViewModel>
    {
        public override void MapToViewModel(MilkProduct model, MilkProductViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductName = model.Name;
            viewModel.ProductCode = model.ProductCode;
            viewModel.ShortDescription = model.ShortDescription ?? "";
            viewModel.Description = model.Description ?? "";
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL;
            viewModel.Price = model.Price;
            viewModel.VAT = model.VAT;
        }
        public override void MapToModel(MilkProductViewModel viewModel, MilkProduct model)
        {
            throw new NotImplementedException();
        }
        public MilkProductViewModel MapToViewModel(MilkProduct model, Company company)
        {
            var viewModel = new MilkProductViewModel();
            model.GetPrice(company); // Get Exclusive price
            MapToViewModel(model, viewModel);

            return viewModel;
        }
    }




}
