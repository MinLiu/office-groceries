﻿using System;

namespace NuIngredient.Models
{
    public class MilkExcludeCompany : ProductExcludeCompany
    {
        public Guid MilkProductID { get; set; }
        public virtual MilkProduct MilkProduct { get; set; }
    }

    public class MilkExcludeCompanyGridMapper : ModelMapper<MilkExcludeCompany, ProductExcludeCompanyGridViewModel>
    {
        public override void MapToViewModel(MilkExcludeCompany model, ProductExcludeCompanyGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Customer = new SelectItemViewModel(model.CompanyID.ToString(), model.Company?.Name);
            viewModel.ProductID = model.MilkProductID.ToString();
        }

        public override void MapToModel(ProductExcludeCompanyGridViewModel viewModel, MilkExcludeCompany model)
        {
            model.MilkProductID = Guid.Parse(viewModel.ProductID);
            model.CompanyID = Guid.Parse(viewModel.Customer.ID);
        }
    }
}