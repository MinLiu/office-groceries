﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkProductCategory : IEntity
    {
        public MilkProductCategory() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public Guid? AisleID { get; set; }
        public Guid? ParentID { get; set; }
        [Required]
        public string Name { get; set; }
        public int SortPos { get; set; }
        public bool Deleted { get; set; }

        public void GetChildrenCategory(List<MilkProductCategory> list)
        {
            if (!Deleted)
            {
                list.Add(this);
                foreach (var child in Children)
                {
                    child.GetChildrenCategory(list);
                }
            }
        }

        //Foreign References
        public virtual MilkProductCategory Parent { get; set; }
        public virtual ICollection<MilkProductCategory> Children { get; set; }
        public virtual Aisle Aisle { get; set; }
    }




    public class MilkProductCategoryViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ParentID { get; set; }
        public string Name { get; set; }
        public int SortPos { get; set; }
        public string AisleID { get; set; }
    }



    public class MilkProductCategoryMapper : ModelMapper<MilkProductCategory, MilkProductCategoryViewModel>
    {
        public override void MapToViewModel(MilkProductCategory model, MilkProductCategoryViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.ParentID = model.ParentID.HasValue? model.ParentID.ToString() : "";
            viewModel.SortPos = model.SortPos;
            viewModel.AisleID = model.AisleID.ToString();
        }

        public override void MapToModel(MilkProductCategoryViewModel viewModel, MilkProductCategory model)
        {
            model.Name = viewModel.Name;
            model.ParentID = string.IsNullOrWhiteSpace(viewModel.ParentID) ? (Guid?)null : Guid.Parse(viewModel.ParentID);
            model.SortPos = viewModel.SortPos;
            model.AisleID = Guid.Parse(viewModel.AisleID);
        }

    }
}
