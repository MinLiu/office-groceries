﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class FruitProduct : IEntity
    {
        public FruitProduct() { ID = Guid.NewGuid(); IsActive = true; IsProspectsVisible = true; }
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid? CategoryID { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool Deleted { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public decimal VAT { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        public bool ApplyPriceBreakToExclusive { get; set; }
        public int SortPosition { get; set; }
        public FruitProductType FruitProductType { get; set; }


        // Navigations
        public virtual FruitProductCategory Category { get; set; }
        public virtual ICollection<FruitProductExclusivePrice> ExclusivePrices { get; set; }
        public virtual ICollection<FruitProductTagPrice> TagPrices { get; set; }
        public virtual ICollection<FruitProductPrice> Prices { get; set; }
        public virtual ICollection<FruitCategoryLink> Categories { get; set; }
        public virtual ICollection<FruitExcludeCompany> ExcludeCompanies { get; set; }
        public virtual ICollection<SupplierFruitProductCode> SupplierProductCodes { get; set; }

        // Methods
        //public void GetPrice(Company company)
        //{
        //    if (company != null)
        //    {
        //        var exclusivePrice = ExclusivePrices.OfType<FruitProductExclusivePrice>().Where(x => x.CusomterID == company.ID).FirstOrDefault();
        //        if (exclusivePrice != null)
        //            Price = exclusivePrice.Price;
        //    }
        //}

        //public decimal ExclusivePrice(Company company)
        //{
        //    if (company != null)
        //    {
        //        var exclusivePrice = ExclusivePrices.OfType<FruitProductExclusivePrice>().Where(x => x.CusomterID == company.ID).FirstOrDefault();
        //        if (exclusivePrice != null)
        //            return exclusivePrice.Price;
        //    }
        //    return Price;
        //}

        //public decimal ExclusivePrice(Guid? companyID)
        //{
        //    Company company = null;
        //    if (companyID != null)
        //    {
        //        company = new Company() { ID = companyID.Value };
        //    }
        //    return ExclusivePrice(company);
        //}

        public decimal GetBasicPrice()
        {
            var qty = 1;
            if (HasSetPrice(qty))
            {
                return GetSetPrice(1);
            }
            else
            {
                return Price;
            }
        }

        public decimal GetPrice(int qty, Guid? companyID)
        {
            var exclusiveUnitPrice = GetExclusivePrice(qty, companyID);
            if (exclusiveUnitPrice != null)
                return exclusiveUnitPrice.Value;

            if (qty == 0)
                return 0;
            else if (HasSetPrice(qty))
                return GetSetPrice(qty);
            else
                return GetNotSetPrice(qty);
        }

        private bool HasSetPrice(int qty)
        {
            return Prices.Where(x => x.BreakPoint == qty).Any();
        }

        private decimal GetSetPrice(int qty)
        {
            var priceBreak = Prices.Where(x => x.BreakPoint == qty).First();
            return priceBreak.Price;
        }

        private decimal GetNotSetPrice(int qty)
        {
            var priceBreak = Prices.OrderByDescending(x => x.BreakPoint).FirstOrDefault();
            if (priceBreak != null)
            {
                var price = priceBreak.Price;
                var exceedQty = qty - priceBreak.BreakPoint;
                var totalPrice = price + exceedQty * Price;
                return totalPrice;
            }
            else
                return Price * qty;
        }

        private decimal? GetExclusivePrice(int qty, Guid? companyID)
        {
            if (companyID == null) return null;
            
            if (qty == 0)
                return 0;

            var exclusivePrice = ExclusivePrices.FirstOrDefault(x => x.CusomterID == companyID);

            if (exclusivePrice == null)
            {
                if (FruitTagPriceHelper.TryGetTagPrice(companyID.Value, ID, out decimal price))
                {
                    return qty * price;
                }
                
                return null;
            }

            // If this product doesn't apply price break with exclusive price
            if (ApplyPriceBreakToExclusive == false)
                return qty * exclusivePrice.Price;

            // Apply price break
            if (HasSetPrice(qty))
            {
                return GetSetPrice(qty);
            }
            else
            {
                return GetNotSetPrice(qty);
            }
        }
    }

    #region EditProductViewModel

    public class EditFruitProductViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        [Required(ErrorMessage = "* The Product Name field is required.")]
        [Display(Name = "Product Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "* The Product Code field is required.")]
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        public string ShortDescription { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        public string CategoryID { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        public bool ApplyPriceBreakToExclusive { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public decimal Cost { get; set; }
        public decimal VAT { get; set; }

        public List<string> CategoryIDs { get; set; }
        public FruitProductType FruitProductType { get; set; }
    }

    public class EditFruitProductModelMapper : ModelMapper<FruitProduct, EditFruitProductViewModel>
    {
        public override void MapToModel(EditFruitProductViewModel viewModel, FruitProduct model)
        {
            //model.CategoryID = Guid.Parse(viewModel.CategoryID);
            model.ProductCode = viewModel.ProductCode;
            model.Name = viewModel.Name;
            model.ShortDescription = viewModel.ShortDescription;
            model.Description = viewModel.Description;
            model.IsExclusive = viewModel.IsExclusive;
            model.IsProspectsVisible = viewModel.IsProspectsVisible;
            model.IsActive = viewModel.IsActive;
            model.ApplyPriceBreakToExclusive = viewModel.ApplyPriceBreakToExclusive;
            model.Price = viewModel.Price;
            model.Cost = viewModel.Cost;
            model.Unit = viewModel.Unit;
            model.VAT = viewModel.VAT;
            model.FruitProductType = viewModel.FruitProductType;

        }
        public override void MapToViewModel(FruitProduct model, EditFruitProductViewModel viewModel)
        {
            viewModel.CategoryID = model.Category != null ? model.Category.ID.ToString() : "";
            viewModel.Cost = model.Cost;
            viewModel.ShortDescription = model.ShortDescription;
            viewModel.Description = model.Description;
            viewModel.ImageURL = model.ImageURL;
            viewModel.ID = model.ID.ToString();
            viewModel.IsActive = model.IsActive;
            viewModel.IsExclusive = model.IsExclusive;
            viewModel.IsProspectsVisible = model.IsProspectsVisible;
            viewModel.ApplyPriceBreakToExclusive = model.ApplyPriceBreakToExclusive;
            viewModel.Name = model.Name;
            viewModel.Price = model.Price; 
            viewModel.ProductCode = model.ProductCode; 
            viewModel.Unit = model.Unit;
            viewModel.VAT = model.VAT;
            viewModel.CategoryIDs = model.Categories != null ? model.Categories.Select(x => x.CategoryID.ToString()).ToList() : new List<string>();
            viewModel.FruitProductType = model.FruitProductType;
        }
    }

    #endregion

    #region ProductGridViewModel

    public class FruitProductGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string CategoryName { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        public decimal Price { get; set; }
        public string Prices { get; set; }
        public decimal VAT { get; set; }
        public decimal Cost { get; set; }
    }

    public class FruitProductGridMapper : ModelMapper<FruitProduct, FruitProductGridViewModel>
    {
        public override void MapToViewModel(FruitProduct model, FruitProductGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductCode = model.ProductCode;
            viewModel.Name = model.Name;
            viewModel.ShortDescription = model.ShortDescription;
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.Price = model.Price;
            viewModel.VAT = model.VAT;
            viewModel.Cost = model.Cost;
            viewModel.IsActive = model.IsActive;
            viewModel.IsExclusive = model.IsExclusive;
            viewModel.IsProspectsVisible = model.IsProspectsVisible;
            viewModel.CategoryName = String.Join("\n", model.Categories.Select(c => c.Category.Name).OrderBy(c => c).ToList());
            viewModel.Prices = $"Unit Price => {model.Price.ToString("C")}\n" + String.Join("\n", model.Prices.OrderBy(x => x.BreakPoint).Select(x => x.BreakPoint + " => " + x.Price.ToString("C")));
        }

        public override void MapToModel(FruitProductGridViewModel viewModel, FruitProduct model)
        {
            throw new NotImplementedException();
        }

    }

    #endregion

    #region ProductListViewModel

    public class FruitProductListViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
    }

    public class FruitProductListViewMapper : ModelMapper<FruitProduct, FruitProductListViewModel>
    {
        public override void MapToModel(FruitProductListViewModel viewModel, FruitProduct model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(FruitProduct model, FruitProductListViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.Description = !string.IsNullOrEmpty(model.Description) ? HttpUtility.HtmlEncode(model.Description).Replace("#", "\\#") : "";
            viewModel.ShortDescription = model.ShortDescription ?? "";
            viewModel.Unit = model.Unit ?? "";
            viewModel.Price = model.GetBasicPrice();
            viewModel.VAT = model.VAT;
        }

        //public FruitProductListViewModel MapToViewModel(FruitProduct model, Company company)
        //{
        //    var viewModel = new FruitProductListViewModel();
        //    model.GetBasicPrice();
        //    MapToViewModel(model, viewModel);

        //    return viewModel;
        //}
    }

    #endregion

    public class FruitProductViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductName { get; set; }

        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
    }
    public class FruitProductMapper : ModelMapper<FruitProduct, FruitProductViewModel>
    {
        public override void MapToViewModel(FruitProduct model, FruitProductViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductName = model.Name;
            viewModel.ProductCode = model.ProductCode;
            viewModel.ShortDescription = model.ShortDescription ?? "";
            viewModel.Description = model.Description ?? "";
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL;
            viewModel.Price = model.Price;
            viewModel.VAT = model.VAT;
        }
        public override void MapToModel(FruitProductViewModel viewModel, FruitProduct model)
        {
            throw new NotImplementedException();
        }
        public FruitProductViewModel MapToViewModel(FruitProduct model, Company company)
        {
            var viewModel = new FruitProductViewModel();
            model.GetBasicPrice();
            MapToViewModel(model, viewModel);

            return viewModel;
        }
    }




}
