﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class FruitProductPrice : IEntity
    {
        public FruitProductPrice() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public int BreakPoint { get; set; }
        public decimal Price { get; set; }
        public virtual FruitProduct Product { get; set; }
    }


    public class FruitProductPriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public decimal Price { get; set; }
        public int BreakPoint { get; set; }
    }



    public class FruitProductPriceMapper : ModelMapper<FruitProductPrice, FruitProductPriceViewModel>
    {
        public override void MapToViewModel(FruitProductPrice model, FruitProductPriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Price = model.Price;
            viewModel.BreakPoint = model.BreakPoint;
        }

        public override void MapToModel(FruitProductPriceViewModel viewModel, FruitProductPrice model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.Price = viewModel.Price;
            model.BreakPoint = viewModel.BreakPoint;
        }

    }
}
