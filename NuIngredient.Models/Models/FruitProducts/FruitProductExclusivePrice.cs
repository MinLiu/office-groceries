﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class FruitProductExclusivePrice : IEntity
    {
        public FruitProductExclusivePrice() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        [ForeignKey("Customer")]
        public Guid CusomterID { get; set; }
        public decimal Price { get; set; }
        public virtual FruitProduct Product { get; set; }
        public virtual Company Customer { get; set; }
    }




    public class FruitProductExclusivePriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public decimal Price { get; set; }
        public SelectItemViewModel Customer { get; set; }
    }



    public class FruitProductExclusivePriceMapper : ModelMapper<FruitProductExclusivePrice, FruitProductExclusivePriceViewModel>
    {
        public override void MapToViewModel(FruitProductExclusivePrice model, FruitProductExclusivePriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Price = model.Price;
            viewModel.Customer = new SelectItemViewModel() { ID = model.Customer.ID.ToString(), Name = model.Customer.Name };
        }

        public override void MapToModel(FruitProductExclusivePriceViewModel viewModel, FruitProductExclusivePrice model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.CusomterID = Guid.Parse(viewModel.Customer.ID);
            model.Price = viewModel.Price;
        }

    }
}
