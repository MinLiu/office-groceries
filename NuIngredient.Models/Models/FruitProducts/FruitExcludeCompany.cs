﻿using System;

namespace NuIngredient.Models
{
    public class FruitExcludeCompany : ProductExcludeCompany
    {
        public Guid FruitProductID { get; set; }
        public virtual FruitProduct FruitProduct { get; set; }
    }
    
    public class FruitExcludeCompanyGridMapper : ModelMapper<FruitExcludeCompany, ProductExcludeCompanyGridViewModel>
    {
        public override void MapToViewModel(FruitExcludeCompany model, ProductExcludeCompanyGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Customer = new SelectItemViewModel(model.CompanyID.ToString(), model.Company?.Name);
            viewModel.ProductID = model.FruitProductID.ToString();
        }

        public override void MapToModel(ProductExcludeCompanyGridViewModel viewModel, FruitExcludeCompany model)
        {
            model.FruitProductID = Guid.Parse(viewModel.ProductID);
            model.CompanyID = Guid.Parse(viewModel.Customer.ID);
        }
    }
}