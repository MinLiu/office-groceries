﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitProductCategory : IEntity
    {
        public FruitProductCategory() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public Guid? ParentID { get; set; }
        [Required]
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public int SortPos { get; set; }


        public void GetChildrenCategory(List<FruitProductCategory> list)
        {
            if (!Deleted)
            {
                list.Add(this);
                foreach (var child in Children)
                {
                    child.GetChildrenCategory(list);
                }
            }
        }

        //Foreign References
        public virtual FruitProductCategory Parent { get; set; }
        public virtual ICollection<FruitProductCategory> Children { get; set; }
    }




    public class FruitProductCategoryViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ParentID { get; set; }
        public string Name { get; set; }
        public int SortPos { get; set; }
    }



    public class FruitProductCategoryMapper : ModelMapper<FruitProductCategory, FruitProductCategoryViewModel>
    {
        public override void MapToViewModel(FruitProductCategory model, FruitProductCategoryViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.ParentID = model.ParentID.HasValue? model.ParentID.ToString() : "";
            viewModel.SortPos = model.SortPos;
        }

        public override void MapToModel(FruitProductCategoryViewModel viewModel, FruitProductCategory model)
        {
            model.Name = viewModel.Name;
            model.ParentID = string.IsNullOrWhiteSpace(viewModel.ParentID) ? (Guid?)null : Guid.Parse(viewModel.ParentID);
            model.SortPos = viewModel.SortPos;
        }

    }
}
