﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class FruitCategoryLink : IEntity
    {
        public FruitCategoryLink() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid FruitProductID { get; set; }
        public int SortPos { get; set; }

        // Navigations
        public virtual FruitProductCategory Category { get; set; }
        public virtual FruitProduct FruitProduct { get; set; }
    }

}
