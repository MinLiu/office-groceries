﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{
    public class FruitProductTagPrice : IEntity
    {
        public FruitProductTagPrice() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public Guid TagID { get; set; }
        public decimal Price { get; set; }

        public virtual FruitProduct Product { get; set; }
        public virtual Tag Tag { get; set; }
    }

    public class FruitProductTagPriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public TagGridViewModel Tag { get; set; }
        public decimal Price { get; set; }
    }

    public class FruitProductTagPriceMapper : ModelMapper<FruitProductTagPrice, FruitProductTagPriceViewModel>
    {
        private static readonly TagGridMapper TagMapper = new TagGridMapper();
        public override void MapToViewModel(FruitProductTagPrice model, FruitProductTagPriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Tag = TagMapper.MapToViewModel(model.Tag);
            viewModel.Price = model.Price;
        }

        public override void MapToModel(FruitProductTagPriceViewModel viewModel, FruitProductTagPrice model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.TagID = Guid.Parse(viewModel.Tag.ID);
            model.Price = viewModel.Price;

        }
    }
}
