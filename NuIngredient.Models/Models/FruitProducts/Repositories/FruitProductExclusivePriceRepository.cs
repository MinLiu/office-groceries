﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitProductExclusivePriceRepository : EntityRespository<FruitProductExclusivePrice>
    {
        public FruitProductExclusivePriceRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitProductExclusivePriceRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
