﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitProductPriceRepository : EntityRespository<FruitProductPrice>
    {
        public FruitProductPriceRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitProductPriceRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
