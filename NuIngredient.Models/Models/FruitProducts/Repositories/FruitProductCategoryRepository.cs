﻿using System.Linq;
namespace NuIngredient.Models
{
    public class FruitProductCategoryRepository : EntityRespository<FruitProductCategory>
    {
        public FruitProductCategoryRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitProductCategoryRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(FruitProductCategory entity)
        {
            // Delete subcategories
            var subCategories = _db.Set<FruitProductCategory>()
                                   .Where(x => x.Deleted == false)
                                   .Where(x => x.ParentID == entity.ID).ToList();
            foreach (var subCategory in subCategories)
            {
                Delete(subCategory);
            }

            // Delete milk products link to this category
            var fruitProductRepo = new FruitProductRepository();
            var fruitProducts = fruitProductRepo.Read()
                                                .Where(x => x.Deleted == false)
                                                .Where(x => x.CategoryID == entity.ID).ToList();
            foreach (var fruitproduct in fruitProducts)
            {
                fruitProductRepo.Delete(fruitproduct);
            }

            // Delete this entity
            entity = _db.Set<FruitProductCategory>().Find(entity.ID);
            entity.Deleted = true;

            _db.SaveChanges();
        }
    }

}
