﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitProductRepository : EntityRespository<FruitProduct>
    {
        public FruitProductRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitProductRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(FruitProduct entity)
        {
            var set = _db.Set<FruitProduct>().Where(a => a.ID == entity.ID);

            foreach (var milk in set)
            {
                milk.Deleted = true;
            }

            var milkOrderItems = _db.Set<FruitOrder>().Where(x => x.FruitProductID == entity.ID)
                                                     .Where(x => x.OrderHeader.OrderMode == OrderMode.Regular || x.OrderHeader.OrderMode == OrderMode.OneOff);
            _db.Set<FruitOrder>().RemoveRange(milkOrderItems);

            _db.SaveChanges();
        }

    }

}
