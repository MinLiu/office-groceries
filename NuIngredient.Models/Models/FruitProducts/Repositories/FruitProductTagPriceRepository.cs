﻿namespace NuIngredient.Models
{
    public class FruitProductTagPriceRepository : EntityRespository<FruitProductTagPrice>
    {
        public FruitProductTagPriceRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitProductTagPriceRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
