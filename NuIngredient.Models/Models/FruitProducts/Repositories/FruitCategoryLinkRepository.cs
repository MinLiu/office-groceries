﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitCategoryLinkRepository : EntityRespository<FruitCategoryLink>
    {
        public FruitCategoryLinkRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitCategoryLinkRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
