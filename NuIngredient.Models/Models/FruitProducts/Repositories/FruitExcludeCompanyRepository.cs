﻿namespace NuIngredient.Models
{
    public class FruitExcludeCompanyRepository : EntityRespository<FruitExcludeCompany>
    {
        public FruitExcludeCompanyRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitExcludeCompanyRepository(SnapDbContext db)
            : base(db)
        {

        }

    }
}