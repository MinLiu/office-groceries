﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class CarouselRepository : EntityRespository<Carousel>
    {
        public CarouselRepository()
            : this(new SnapDbContext())
        {

        }

        public CarouselRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
