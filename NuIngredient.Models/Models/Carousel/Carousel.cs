﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{
    public class Carousel : IEntity
    {
        public Carousel() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid? ProductID { get; set; }
        public string ImageURL { get; set; }
        public string LinkURL { get; set; }
        public string Title { get; set; }

        // Navigations
        public virtual Product Product { get; set; }
    }

    #region CarouselViewModel

    public class CarouselViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        [Required]
        public string ImageURL { get; set; }
        public string LinkURL { get; set; }
        public string Title { get; set; }
    }

    public class CarouselMapper : ModelMapper<Carousel, CarouselViewModel>
    {

        public override void MapToViewModel(Carousel model, CarouselViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ImageURL = model.ImageURL;
            viewModel.LinkURL = model.LinkURL;
            viewModel.Title = model.Title;
        }

        public override void MapToModel(CarouselViewModel viewModel, Carousel model)
        {
            throw new NotImplementedException();
        }

    }

    #endregion

    #region CarouselNIViewModel

    public class CarouselNIViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        [Required]
        public DateTime FromDate { get; set; }

        [Required]
        public DateTime ToDate { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string ImageURL { get; set; }
        public string LinkURL { get; set; }
        public string Title { get; set; }
        public bool IsLive { get; set; }
    }

    public class CarouselNIMapper : ModelMapper<Carousel, CarouselNIViewModel>
    {
        public override void MapToModel(CarouselNIViewModel viewModel, Carousel model)
        {
            model.FromDate = viewModel.FromDate;
            model.ToDate = viewModel.ToDate;
            model.ProductID = string.IsNullOrWhiteSpace(viewModel.ProductID) ? (Guid?)null : Guid.Parse(viewModel.ProductID);
            model.LinkURL = model.ProductID == null? viewModel.LinkURL : "/Products/Detail/" + model.ProductID.ToString();
            model.Title = viewModel.Title;
        }

        public override void MapToViewModel(Carousel model, CarouselNIViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.FromDate = model.FromDate;
            viewModel.ToDate = model.ToDate;
            viewModel.ImageURL = model.ImageURL;
            viewModel.LinkURL = model.LinkURL;
            viewModel.ProductID = model.ProductID == null ? "" : model.Product.ID.ToString();
            viewModel.ProductName = model.ProductID == null ? "" : model.Product.Name;
            viewModel.Title = model.Title;
            viewModel.IsLive = DateTime.Today >= model.FromDate && DateTime.Today <= model.ToDate;
        }
    }

    #endregion
}
