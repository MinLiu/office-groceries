﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class CompanySupplier : IEntity
    {
        [Key]
        public Guid ID { get; set; }

        public Guid CompanyID { get; set; }

        public Guid SupplierID { get; set; }
        public Guid? SupplierDepotID { get; set; }

        [Required]
        public string AccountNo { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public DateTime? DeliveryTimeFrom { get; set; }
        public DateTime? DeliveryTimeTo { get; set; }

        public bool DeliveryMon { get; set; }
        public bool DeliveryTue { get; set; }
        public bool DeliveryWed { get; set; }
        public bool DeliveryThu { get; set; }
        public bool DeliveryFri { get; set; }
        public bool DeliverySat { get; set; }
        public bool DeliverySun { get; set; }
        
        public bool IsIntermittentDelivery { get; set; }
        /// <summary>
        /// Used to determine whether to add "13" to MFS Report, only for supplier MACK Food Service ("765a4128-5010-4c83-9dae-d526ff485d2c")
        /// </summary>
        public bool IsMfs13 { get; set; }
        public int? DeliveryFrequency { get; set; }
        public DateTime? NextDeliveryWeek { get; set; }

        // Navigations

        public virtual Company Company { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual SupplierDepot SupplierDepot { get; set; }
        public CompanySupplier()
        {
            ID = Guid.NewGuid();
        }

    }

    public class CompanySupplierViewModel : IEntityViewModel
    { 
        public string ID { get; set; }
        [Required]
        public  string CompanyID { get; set; }
        [Required]
        public string SupplierID { get; set; }
        public string SupplierDepotID { get; set; }
        public SupplierType SupplierType { get; set; }
        public string SupplierTypeName { get; set; }
        public string SupplierName { get; set; }
        public string SupplierDefaultAisleName { get; set; }
        public string SupplierDefaultAisleLabel { get; set; }
        [Required]
        public string AccountNo { get; set; }
        [Required]
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DeliveryTimeFrom { get; set; }
        public DateTime? DeliveryTimeTo { get; set; }
        public bool DeliveryMon { get; set; }
        public bool DeliveryTue { get; set; }
        public bool DeliveryWed { get; set; }
        public bool DeliveryThu { get; set; }
        public bool DeliveryFri { get; set; }
        public bool DeliverySat { get; set; }
        public bool DeliverySun { get; set; }

        public bool IsIntermittentDelivery { get; set; }
        public bool IsMfs13 { get; set; }
        public int? DeliveryFrequency { get; set; }
        public DateTime? NextDeliveryWeek { get; set; }

    }

    public class CompanySupplierGridViewModel
    {
        public string ID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPostcode { get; set; }
        public DateTime? StartDate { get; set; }
    }

    public class CompanySupplierMapper : ModelMapper<CompanySupplier, CompanySupplierViewModel>
    {
        public override void MapToModel(CompanySupplierViewModel viewModel, CompanySupplier model)
        {
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.SupplierID = Guid.Parse(viewModel.SupplierID);
            model.SupplierDepotID = string.IsNullOrWhiteSpace(viewModel.SupplierDepotID) ? null as Guid? : Guid.Parse(viewModel.SupplierDepotID);
            model.AccountNo = viewModel.AccountNo;
            model.StartDate = viewModel.StartDate;
            model.EndDate = viewModel.EndDate;
            model.DeliveryTimeFrom = viewModel.DeliveryTimeFrom;
            model.DeliveryTimeTo = viewModel.DeliveryTimeTo;
            model.DeliveryMon = viewModel.DeliveryMon;
            model.DeliveryTue = viewModel.DeliveryTue;
            model.DeliveryWed = viewModel.DeliveryWed;
            model.DeliveryThu = viewModel.DeliveryThu;
            model.DeliveryFri = viewModel.DeliveryFri;
            model.DeliverySat = viewModel.DeliverySat;
            model.DeliverySun = viewModel.DeliverySun;
            model.IsMfs13 = viewModel.IsMfs13;
            model.IsIntermittentDelivery = viewModel.IsIntermittentDelivery;
            model.DeliveryFrequency = model.IsIntermittentDelivery ? viewModel.DeliveryFrequency : null;
            model.NextDeliveryWeek = model.IsIntermittentDelivery ? viewModel.NextDeliveryWeek : null;
        }

        public override void MapToViewModel(CompanySupplier model, CompanySupplierViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.AccountNo = model.AccountNo;
            viewModel.SupplierID = model.SupplierID.ToString();
            viewModel.SupplierName = model.Supplier.Name;
            viewModel.SupplierDepotID = model.SupplierDepotID?.ToString();
            viewModel.SupplierType = model.Supplier.SupplierType;
            viewModel.SupplierTypeName = model.Supplier.SupplierType.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.StartDate = model.StartDate;
            viewModel.EndDate = model.EndDate;
            viewModel.DeliveryTimeFrom = model.DeliveryTimeFrom;
            viewModel.DeliveryTimeTo = model.DeliveryTimeTo;
            viewModel.DeliveryMon = model.DeliveryMon;
            viewModel.DeliveryTue = model.DeliveryTue;
            viewModel.DeliveryWed = model.DeliveryWed;
            viewModel.DeliveryThu = model.DeliveryThu;
            viewModel.DeliveryFri = model.DeliveryFri;
            viewModel.DeliverySat = model.DeliverySat;
            viewModel.DeliverySun = model.DeliverySun;
            viewModel.SupplierDefaultAisleName = model.Supplier.DefaultAisleNames;
            viewModel.SupplierDefaultAisleLabel = model.Supplier.DefaultAisleLabels;
            viewModel.IsIntermittentDelivery = model.IsIntermittentDelivery;
            viewModel.IsMfs13 = model.IsMfs13;
            viewModel.DeliveryFrequency = model.DeliveryFrequency;
            viewModel.NextDeliveryWeek = model.NextDeliveryWeek;
        }

    }
}
