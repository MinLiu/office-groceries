﻿using System;

namespace NuIngredient.Models
{
    public class MonthlyPoNumber
    {
        public Guid ID { get; set; } = Guid.NewGuid();
        public Guid CompanyID { get; set; }
        public DateTime InvoiceMonth { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string PoNumber { get; set; }
        public bool IsExported { get; set; }
        public DateTime Created { get; set; }
        
        public virtual Company Company { get; set; }
    }
}