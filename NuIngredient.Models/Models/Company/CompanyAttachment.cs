﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{
    public class CompanyAttachment : IEntity
    {
        public CompanyAttachment()
        {
            ID = Guid.NewGuid();
        }
        
        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        public string Filename { get; set; }
        public string FilePath { get; set; }
        public DateTime Created { get; set; }
        
        public virtual Company Company { get; set; }
    }
    
    public class AttachmentViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Filename { get; set; }
        public string FilePath { get; set; }
        public DateTime Created { get; set; }
    }

    public class AttachmentGridMapper : ModelMapper<CompanyAttachment, AttachmentViewModel>
    {
        public override void MapToViewModel(CompanyAttachment model, AttachmentViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Filename = model.Filename;
            viewModel.FilePath = model.FilePath;
            viewModel.Created = model.Created;
        }

        public override void MapToModel(AttachmentViewModel viewModel, CompanyAttachment model)
        {
            throw new NotSupportedException();
        }
    }
    
}