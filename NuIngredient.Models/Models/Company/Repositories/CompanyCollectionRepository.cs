﻿namespace NuIngredient.Models
{
    public class CompanyCollectionRepository : EntityRespository<CompanyCollection>
    {
        public CompanyCollectionRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanyCollectionRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
