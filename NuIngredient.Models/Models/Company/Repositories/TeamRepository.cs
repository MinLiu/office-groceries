﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class TeamRepository : EntityRespository<Team>
    {
        public TeamRepository()
            : this(new SnapDbContext())
        {

        }

        public TeamRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Team entity)
        {
            _db.Set<TeamMember>().RemoveRange(_db.Set<TeamMember>().Where(x => x.TeamID == entity.ID));
            _db.SaveChanges();
            base.Delete(entity);
        }

    }

}
