﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MonthlyPoNumberRepository : EntityRespository<MonthlyPoNumber>
    {
        public MonthlyPoNumberRepository()
            : this(new SnapDbContext())
        {

        }

        public MonthlyPoNumberRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Create(MonthlyPoNumber entity)
        {
            entity.Created = DateTime.Now;
            base.Create(entity);
        }
    }

}
