﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class HierarchyMemberRepository : EntityRespository<HierarchyMember>
    {
        public HierarchyMemberRepository()
            : this(new SnapDbContext())
        {

        }

        public HierarchyMemberRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(HierarchyMember entity)
        {
            var ids = entity.GetAllUnderlingIDs();
            _db.Set<HierarchyMember>().RemoveRange(_db.Set<HierarchyMember>().Where(a => ids.Contains(a.ID) || a.ID == entity.ID));
            

            base.Delete(entity);
        }

    }

}
