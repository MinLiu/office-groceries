﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NuIngredient.Models
{
    public class UserRoleRepository : EntityRespository<IdentityUserRole>
    {
        public UserRoleRepository()
            : this(new SnapDbContext())
        {

        }

        public UserRoleRepository(SnapDbContext db)
            : base(db)
        {

        }
        
    }

}
