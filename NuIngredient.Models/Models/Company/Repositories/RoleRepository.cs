﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NuIngredient.Models
{
    public class RoleRepository : EntityRespository<IdentityRole>
    {
        public RoleRepository()
            : this(new SnapDbContext())
        {

        }

        public RoleRepository(SnapDbContext db)
            : base(db)
        {

        }
        
    }

}
