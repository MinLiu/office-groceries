﻿namespace NuIngredient.Models
{
    public class CompanyTagItemRepository : EntityRespository<CompanyTagItem>
    {
        public CompanyTagItemRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanyTagItemRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
