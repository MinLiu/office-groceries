﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class CompanyAttachmentRepository : EntityRespository<CompanyAttachment>
    {
        public CompanyAttachmentRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanyAttachmentRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Create(CompanyAttachment entity)
        {
            entity.Created = DateTime.Now;
            base.Create(entity);
        }
    }

}
