﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class UserRepository : EntityRespository<User>
    {
        public UserRepository()
            : this(new SnapDbContext())
        {

        }

        public UserRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(User entity)
        {
            _db.Set<TeamMember>().RemoveRange(_db.Set<TeamMember>().Where(a => a.UserId == entity.Id));

            var hiers = _db.Set<HierarchyMember>().Where(a => a.UserId == entity.Id).ToList();

            foreach (var h in hiers)
            {
                var ids = h.GetAllUnderlingIDs();
                _db.Set<HierarchyMember>().RemoveRange(_db.Set<HierarchyMember>().Where(a => ids.Contains(a.ID) || a.ID == h.ID));
            }


            var set = _db.Set<User>().Where(a => a.Id == entity.Id);

            _db.Set<User>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
