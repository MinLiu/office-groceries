﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class TeamMemberRepository : EntityRespository<TeamMember>
    {
        public TeamMemberRepository()
            : this(new SnapDbContext())
        {

        }

        public TeamMemberRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
