﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SageExportSettingsRepository : EntityRespository<SageExportSettings>
    {
        public SageExportSettingsRepository()
            : this(new SnapDbContext())
        {

        }

        public SageExportSettingsRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
