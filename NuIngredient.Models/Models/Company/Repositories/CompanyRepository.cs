﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Extensions;

namespace NuIngredient.Models
{
    public class CompanyRepository : EntityRespository<Company>
    {
        public CompanyRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanyRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(Company entity)
        {
            //check if the company is under multi-contracts
            CheckMultiContractAndMoveUsers(entity);

            //Always delete user stuff last.

            // Delete Message Entities
            _db.Set<MessageRead>().RemoveRange(_db.Set<MessageRead>().Where(x => x.CompanyID == entity.ID));
            
            // Delete Feedback Entities
            _db.Set<Feedback>().RemoveRange(_db.Set<Feedback>().Where(x => x.CompanyID == entity.ID));

            // Fruit Entities
            _db.Set<FruitCreditItem>().RemoveRange(_db.Set<FruitCreditItem>().Where(x => x.FruitCreditHeader.CompanyID == entity.ID));
            _db.Set<FruitCreditHeader>().RemoveRange(_db.Set<FruitCreditHeader>().Where(x => x.CompanyID == entity.ID));
            _db.Set<FruitOrder>().RemoveRange(_db.Set<FruitOrder>().Where(x => x.OrderHeader.CompanyID == entity.ID));
            _db.Set<FruitOneOffChangeTracker>().RemoveRange(_db.Set<FruitOneOffChangeTracker>().Where(x => x.OrderHeader.CompanyID == entity.ID));
            _db.Set<FruitOrderHeader>().RemoveRange(_db.Set<FruitOrderHeader>().Where(x => x.CompanyID == entity.ID));
            _db.Set<FruitProductExclusivePrice>().RemoveRange(_db.Set<FruitProductExclusivePrice>().Where(x => x.CusomterID == entity.ID));

            // Milk Entities
            _db.Set<MilkCreditItem>().RemoveRange(_db.Set<MilkCreditItem>().Where(x => x.MilkCreditHeader.CompanyID == entity.ID));
            _db.Set<MilkCreditHeader>().RemoveRange(_db.Set<MilkCreditHeader>().Where(x => x.CompanyID == entity.ID));
            _db.Set<MilkOrder>().RemoveRange(_db.Set<MilkOrder>().Where(x => x.OrderHeader.CompanyID == entity.ID));
            _db.Set<MilkOneOffChangeTracker>().RemoveRange(_db.Set<MilkOneOffChangeTracker>().Where(x => x.OrderHeader.CompanyID == entity.ID));
            _db.Set<MilkOrderHeader>().RemoveRange(_db.Set<MilkOrderHeader>().Where(x => x.CompanyID == entity.ID));
            _db.Set<MilkProductExclusivePrice>().RemoveRange(_db.Set<MilkProductExclusivePrice>().Where(x => x.CusomterID == entity.ID));

            // Dry Goods Entities
            _db.Set<ShoppingCartItem>().RemoveRange(_db.Set<ShoppingCartItem>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ProductExclusivePrice>().RemoveRange(_db.Set<ProductExclusivePrice>().Where(x => x.CusomterID == entity.ID));
            _db.Set<DryGoodsCreditItem>().RemoveRange(_db.Set<DryGoodsCreditItem>().Where(x => x.DryGoodsCreditHeader.CompanyID == entity.ID));
            _db.Set<DryGoodsCreditHeader>().RemoveRange(_db.Set<DryGoodsCreditHeader>().Where(x => x.CompanyID == entity.ID));
            _db.Set<OneOffOrderItem>().RemoveRange(_db.Set<OneOffOrderItem>().Where(x => x.CompanyID == entity.ID));
            _db.Set<OrderEventDetail>().RemoveRange(_db.Set<OrderEventDetail>().Where(x => x.OneOffOrder.CompanyID == entity.ID));
            _db.Set<OneOffOrder>().RemoveRange(_db.Set<OneOffOrder>().Where(x => x.CompanyID == entity.ID));
            
            _db.Set<CompanySupplier>().RemoveRange(_db.Set<CompanySupplier>().Where(x => x.CompanyID == entity.ID));
            _db.Set<User>().RemoveRange(_db.Set<User>().Where(x => x.CompanyID == entity.ID));
            _db.Set<CompanyAddress>().RemoveRange(_db.Set<CompanyAddress>().Where(x => x.CompanyID == entity.ID));
            _db.Set<TeamMember>().RemoveRange(_db.Set<TeamMember>().Where(x => x.Team.CompanyID == entity.ID));
            _db.Set<Team>().RemoveRange(_db.Set<Team>().Where(x => x.CompanyID == entity.ID));

            _db.Set<CompanyTagItem>().RemoveRange(_db.Set<CompanyTagItem>().Where(x => x.CompanyID == entity.ID));

            // Others
            _db.Set<SupAcctRequestLog>().RemoveRange(_db.Set<SupAcctRequestLog>().Where(x => x.CompanyID == entity.ID));
            _db.Set<CompanyAttachment>().RemoveRange(_db.Set<CompanyAttachment>().Where(x => x.CompanyID == entity.ID));
            _db.Set<IssueEmail>().RemoveRange(_db.Set<IssueEmail>().Where(x => x.CompanyID == entity.ID));
            _db.Set<MonthlyPoNumber>().RemoveRange(_db.Set<MonthlyPoNumber>().Where(x => x.CompanyID == entity.ID));
            _db.Set<AisleExcludeCompany>().RemoveRange(_db.Set<AisleExcludeCompany>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ProductExcludeCompany>().RemoveRange(_db.Set<ProductExcludeCompany>().Where(x => x.CompanyID == entity.ID));
            _db.Set<MilkExcludeCompany>().RemoveRange(_db.Set<MilkExcludeCompany>().Where(x => x.CompanyID == entity.ID));
            _db.Set<FruitExcludeCompany>().RemoveRange(_db.Set<FruitExcludeCompany>().Where(x => x.CompanyID == entity.ID));
            
            
            base.Delete(entity);
        }

        private void CheckMultiContractAndMoveUsers(Company entity)
        {
            var company = _db.Set<Company>().Where(c => c.ID == entity.ID).AsNoTracking().FirstOrDefault();
            if (company.CompanyCollectionID == null)
                return;

            var otherCompany = _db.Set<Company>().FirstOrDefault(x => x.CompanyCollectionID == company.CompanyCollectionID && x.ID != entity.ID);
            if (otherCompany == null) return;

            var users = _db.Set<User>().Where(x => x.CompanyID == entity.ID && x.CanSwitchCompany == true).ToList();
            users.ForEach(u => u.CompanyID = otherCompany.ID);
            _db.SaveChanges();
        }
    }

}
