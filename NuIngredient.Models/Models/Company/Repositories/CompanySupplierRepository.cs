﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class CompanySupplierRepository : EntityRespository<CompanySupplier>
    {
        public CompanySupplierRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanySupplierRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
