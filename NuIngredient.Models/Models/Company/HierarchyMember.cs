﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class HierarchyMember : IEntity
    {
        public HierarchyMember() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        [MaxLength(128)]
        public string UserId { get; set; }
        public Guid? ReportsToID { get; set; }

        public virtual Company Company { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }        
        public virtual HierarchyMember ReportsTo { get; set; }

        public virtual ICollection<HierarchyMember> Underlings { get; set; }
    }


    public static class HierarchyMemberExtensions
    {
        public static List<Guid> GetAllUnderlingIDs(this HierarchyMember member)
        {
            using (var context = new SnapDbContext())
            {
                var allHeirs = context.HierarchyMembers.Where(t => t.CompanyID == member.CompanyID).Include(i => i.Underlings).ToList();
                var list = new List<Guid>();
                GetUnderlingIDs(member, list, allHeirs);

                return list;
            }
        }

        public static List<Guid> GetUnderlingIDs(this HierarchyMember member)
        {
            using (var context = new SnapDbContext())
            {
                var allHeirs = context.HierarchyMembers.Where(t => t.CompanyID == member.CompanyID).Include(i => i.Underlings).ToList();
                var list = new List<Guid>();
                GetUnderlingIDs(member, list, allHeirs);
                return list;
            }
        }

        public static void GetUnderlingIDs(HierarchyMember member, List<Guid> list, List<HierarchyMember> allHeirs)
        {
            var model = allHeirs.Where(i => i.ID == member.ID).First();
            list.Add(model.ID);

            foreach(var under in model.Underlings.ToList()){
                GetUnderlingIDs(under, list, allHeirs);
            }
        }

        public static List<Guid> GetPeerIDs(this HierarchyMember member)
        {
            using (var context = new SnapDbContext())
            {
                var allHeirs = context.HierarchyMembers.Where(t => t.CompanyID == member.CompanyID).Include(i => i.Underlings).ToList();
                var list = new List<Guid>();
                GetPeerIDs(member, list, allHeirs);
                return list;
            }
        }


        public static void GetPeerIDs(this HierarchyMember member, List<Guid> list, List<HierarchyMember> allHeirs)
        {
            using (var context = new SnapDbContext())
            {

                var model = allHeirs.Where(i => i.ID == member.ID).First();
                var peers = allHeirs.Where(i => i.ReportsToID == model.ReportsToID);
                list.Add(model.ID);

                foreach (var peer in peers)
                {
                    list.Add(peer.ID);    
                }
            }
        }


        //public static string[] GetAllUnderlingUserIds(this HierarchyMember member)
        //{
        //    var list = new List<string>();
        //    GetUnderlingUsersIds(member, list);

        //    return list.ToArray();
        //}

        //public static void GetUnderlingUsersIds(HierarchyMember member, List<string> list, List<HierarchyMember> allHeirs)
        //{
        //    using (var context = new SnapDbContext())
        //    {
        //        var model = context.HierarchyMembers.Where(i => i.ID == member.ID).Include(i => i.Underlings).First();
        //        list.Add(model.UserId);

        //        foreach (var under in model.Underlings.ToList())
        //        {
        //            GetUnderlingUsersIds(under, list);
        //        }
        //    }
        //}


        //public static void GetPeerUsersIds(HierarchyMember member, List<string> list)
        //{
        //    using (var context = new SnapDbContext())
        //    {
        //        var model = context.HierarchyMembers.Where(i => i.ID == member.ID).Include(i => i.Underlings).First();
        //        list.Add(model.UserId);

        //        foreach (var peer in context.HierarchyMembers.Where(i => i.ReportsToID == model.ReportsToID))
        //        {
        //            list.Add(peer.UserId);
        //        }
        //    }
        //}
        
    }





    public class HierarchyMemberViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string ReportsToID { get; set; }
        public string UserId { get; set; }
        public string UserEmail { get; set; }
        public bool HasChildren { get; set; }
    }




    public class HierarchyMemberMapper : ModelMapper<HierarchyMember, HierarchyMemberViewModel>
    {
        public override void MapToViewModel(HierarchyMember model, HierarchyMemberViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.ReportsToID = (model.ReportsToID != null) ? model.ReportsToID.ToString() : null;
            viewModel.UserId = model.User.Id;
            viewModel.UserEmail = model.User.Email;
            viewModel.HasChildren = model.Underlings.Count() > 0;
        }

        public override void MapToModel(HierarchyMemberViewModel viewModel, HierarchyMember model)
        {
           // model.ID = viewModel.ID;
           //model.CompanyID = viewModel.CompanyID;
           model.ReportsToID = (!string.IsNullOrEmpty(viewModel.ReportsToID)) ? Guid.Parse(viewModel.ReportsToID) as Guid? : null;
           model.UserId = viewModel.UserId;
        }

    }
}
