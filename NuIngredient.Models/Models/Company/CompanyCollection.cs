﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class CompanyCollection : IEntity
    {
        public CompanyCollection() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
    }

}