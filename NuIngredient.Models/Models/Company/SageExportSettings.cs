﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SageExportSettings
    {
        [Key]
        [ForeignKey("Company"), Column("CompanyID")]
        public Guid CompanyID { get; set; }

        public decimal ExchangeRateGBP { get; set; }
        public decimal ExchangeRateUSD { get; set; }
        public decimal ExchangeRateEUR { get; set; }

        public virtual Company Company { get; set; }

        public SageExportSettings()
        {
            ExchangeRateGBP = 1.0m;
            ExchangeRateUSD = 1.0m;
            ExchangeRateEUR = 1.0m;
        }
    }



    public class SageExportSettingsViewModel
    {
        //Used to edit the display settings
        public Guid CompanyID { get; set; }

        public decimal ExchangeRateGBP { get; set; }
        public decimal ExchangeRateUSD { get; set; }
        public decimal ExchangeRateEUR { get; set; }
    }




    public class SageExportSettingsMapper : ModelMapper<SageExportSettings, SageExportSettingsViewModel>
    {

        public override void MapToViewModel(SageExportSettings model, SageExportSettingsViewModel viewModel)
        {
            viewModel.CompanyID = model.CompanyID;
            viewModel.ExchangeRateGBP = model.ExchangeRateGBP;
            viewModel.ExchangeRateUSD = model.ExchangeRateUSD;
            viewModel.ExchangeRateEUR = model.ExchangeRateEUR;
        }


        public override void MapToModel(SageExportSettingsViewModel viewModel, SageExportSettings model)
        {
            //model.CompanyID = viewModel.CompanyID;
            model.ExchangeRateGBP = viewModel.ExchangeRateGBP;
            model.ExchangeRateUSD = viewModel.ExchangeRateUSD;
            model.ExchangeRateEUR = viewModel.ExchangeRateEUR;            
        }
    }
}
