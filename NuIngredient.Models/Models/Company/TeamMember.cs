﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class TeamMember : IEntity
    {
        public TeamMember() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid TeamID { get; set; }

        [MaxLength(128)]
        public string UserId { get; set; }

        public virtual Team Team { get; set; }
        public virtual User User { get; set; }
    }





    public class TeamMemberViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string TeamID { get; set; }

        public UserItemViewModel User { get; set; }
    }




    public class TeamMemberMapper : ModelMapper<TeamMember, TeamMemberViewModel>
    {
        public override void MapToViewModel(TeamMember model, TeamMemberViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.TeamID = model.TeamID.ToString();
            viewModel.User = new UserItemViewModel { ID = model.User.Id, Email = model.User.Email };
        }

        public override void MapToModel(TeamMemberViewModel viewModel, TeamMember model)
        {
           // model.ID = viewModel.ID;
            model.TeamID = Guid.Parse(viewModel.TeamID);
            model.UserId = viewModel.User.ID;
        }

    }
}
