﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class CompanyTagItem : IEntity
    {
        public CompanyTagItem() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid TagID { get; set; }

        public virtual Company Company { get; set; }
        public virtual Tag Tag { get; set; }
    }

    public class CompanyTagItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public TagGridViewModel Tag { get; set; }
    }

    public class CompanyTagItemMapper : ModelMapper<CompanyTagItem, CompanyTagItemViewModel>
    {
        private static readonly TagGridMapper TagMapper = new TagGridMapper();
        public override void MapToViewModel(CompanyTagItem model, CompanyTagItemViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Tag = TagMapper.MapToViewModel(model.Tag);
        }

        public override void MapToModel(CompanyTagItemViewModel viewModel, CompanyTagItem model)
        {
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.TagID = Guid.Parse(viewModel.Tag.ID);
        }
    }
}
