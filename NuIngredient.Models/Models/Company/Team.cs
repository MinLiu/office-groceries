﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class Team : IEntity
    {
        public Team() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        [Required]
        public string Name { get; set; }
        
        public virtual Company Company { get; set; }
        public virtual ICollection<TeamMember> Members { get; set; }
    }



    public class TeamViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        [Required(ErrorMessage = "* The Team Name field is required.")]
        [Display(Name = "Team Name")]
        public string Name { get; set; }
    }



    public class TeamMapper : ModelMapper<Team, TeamViewModel>
    {
        public override void MapToViewModel(Team model, TeamViewModel viewModel)
        {            
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Name = model.Name;           
        }

        public override void MapToModel(TeamViewModel viewModel, Team model)
        {
            //model.ID = Guid.Parse(viewModel.ID);
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
            
        }

    }


}
