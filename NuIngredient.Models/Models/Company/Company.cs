﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace NuIngredient.Models
{
    public class Company : IEntity
    {
        public Company() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string AccountNo { get; set; }

        public DateTime SignupDate { get; set; }
        public bool AccountClosed { get; set; }
        public bool DeleteData { get; set; }

        [Required]
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        /// <summary>
        /// Only for email to all customers, not for any supplier specific customers messages. 
        /// </summary>
        public string SecondaryEmail { get; set; }
        public string VatNumber { get; set; }
        public string BillingCompanyName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingPostcode { get; set; }
        public string BillingEmail { get; set; }

        public decimal DefaultVatRate { get; set; }
        public int DefaultCurrencyID { get; set; }

        public bool MilkDeliveryMonday { get; set; }
        public bool MilkDeliveryTuesday { get; set; }
        public bool MilkDeliveryWednesday { get; set; }
        public bool MilkDeliveryThursday { get; set; }
        public bool MilkDeliveryFriday { get; set; }
        public bool MilkDeliverySaturday { get; set; }
        public bool MilkDeliverySunday { get; set; }

        public bool FruitsDeliveryMonday { get; set; }
        public bool FruitsDeliveryTuesday { get; set; }
        public bool FruitsDeliveryWednesday { get; set; }
        public bool FruitsDeliveryThursday { get; set; }
        public bool FruitsDeliveryFriday { get; set; }
        public bool FruitsDeliverySaturday { get; set; }
        public bool FruitsDeliverySunday { get; set; }

        public bool DryGoodsDeliveryMonday { get; set; }
        public bool DryGoodsDeliveryTuesday { get; set; }
        public bool DryGoodsDeliveryWednesday { get; set; }
        public bool DryGoodsDeliveryThursday { get; set; }
        public bool DryGoodsDeliveryFriday { get; set; }
        public bool DryGoodsDeliverySaturday { get; set; }

        public DateTime? AskingForFruitSupplier { get; set; }
        public DateTime? AskingForMilkSupplier { get; set; }
        public DateTime? AskingForDryGoodsSupplier { get; set; }
        public DateTime? AskingForSnackSupplier { get; set; }

        public string PermanentFruiPONo { get; set; }
        public string PermanentMilkPONo { get; set; }
        public string PermanentDryGoodsPONo { get; set; }
        public string DeliveryInstruction { get; set; }
        /// <summary>
        /// Instructions for MFS report
        /// </summary>
        public string InternalDeliveryInstruction { get; set; }
        /// <summary>
        /// For DPD Fruit Delivery Report
        /// </summary>
        public string DpdFruitAmendments { get; set; }
        public string Depot { get; set; }
        public string Round { get; set; }
        public string CompanyRegistration { get; set; }
        public bool AgreeToGetEmail { get; set; }
        public bool EasySuppliersActivated { get; set; }
        [NotMapped]
        public bool CanBeExported => Method_IsLive(SupplierType.Milk) || Method_IsLive(SupplierType.Fruits) || Method_IsLive(SupplierType.DryGoods) || Method_IsLive(SupplierType.Snacks);

        public bool Exported { get; set; }
        public bool IsMultiTenantedBuilding { get; set; }
        public bool HasMultiSites { get; set; }
        public bool IsTransferred { get; set; }
        public bool TransferEmailSent { get; set; }
        public Guid? CompanyCollectionID { get; set; }
        public bool NoDeliveryCharge { get; set; }
        public Guid? ProformaApproverID { get; set; }

        public virtual ProformaApprover ProformaApprover { get; set; }
        public virtual CompanyCollection CompanyCollection { get; set; }
        public virtual ICollection<User> ApplicationUsers { get; set; }
        public virtual ICollection<CompanyAddress> Addresses { get; set; }
        public bool UseTeams { get; set; }
        public virtual ICollection<Team> Teams { get; set; }

        public bool UseHierarchy { get; set; }
        public bool AccessSameLevelHierarchy { get; set; }
        public virtual ICollection<HierarchyMember> HierarchyMembers { get; set; }

        public virtual SageExportSettings SageExportSettings { get; set; }
        public virtual ICollection<CompanySupplier> CompanySuppliers { get; set; }
        public virtual ICollection<FruitOrderHeader> FruitOrders { get; set; }
        public virtual ICollection<MilkOrderHeader> MilkOrders { get; set; }
        public virtual ICollection<OneOffOrder> OneOffOrders { get; set; }
        public virtual ICollection<ShoppingCartItem> ShoppingCartItems { get; set; }
        public virtual ICollection<SupAcctRequestLog> SupAcctRequestLogs { get; set; }
        public virtual ICollection<CompanyTagItem> TagItems { get; set; }
        public virtual ICollection<CompanyAttachment> Attachments { get; set; }
        public virtual ICollection<MonthlyPoNumber> MonthlyPoNumbers { get; set; }
        public virtual ICollection<IssueEmail> IssueEmails { get; set; }
        // functions

        public ICollection<Company> AllRelatedCompanies()
        {
            return CompanyCollection?.Companies ?? new List<Company>() { this };
        }

        public bool Method_HasSupplier(SupplierType type)
        {
            return new SnapDbContext().CompanySuppliers.Where(cs => cs.CompanyID == ID)
                                                       .Where(cs => cs.Supplier.SupplierType == type)
                                                       .Where(cs => cs.StartDate.HasValue)
                                                       .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= DateTime.Today)
                                                       .Count() > 0;
        }

        public bool Method_IsLive(SupplierType type, DateTime date)
        {
            return new SnapDbContext().CompanySuppliers
                                      .Where(cs => cs.CompanyID == ID)
                                      .Where(cs => cs.Supplier.SupplierType == type)
                                      .Where(cs => cs.StartDate.HasValue && cs.StartDate <= date.Date)
                                      .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= date.Date)
                                      .Count() > 0;
        }

        public bool Method_IsLive(SupplierType type)
        {
            return Method_IsLive(type, DateTime.Today);
        }

        public bool Method_IsLive(Supplier supplier, DateTime date)
        {
            return new SnapDbContext().CompanySuppliers
                                      .Where(cs => cs.CompanyID == ID)
                                      .Where(cs => cs.StartDate.HasValue && cs.StartDate <= date.Date)
                                      .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= date.Date)
                                      .Where(cs => cs.SupplierID == supplier.ID)
                                      .Count() > 0;
        }

        public bool Method_IsLive(Supplier supplier)
        {
            return Method_IsLive(supplier, DateTime.Today);
        }

        public bool IsLiveWeek(SupplierType type, DateTime date)
        {
            int diff = (date.DayOfWeek - DayOfWeek.Monday) % 7;
            var mon = date.AddDays(-1 * diff).Date;
            var tue = mon.AddDays(1);
            var wed = mon.AddDays(2);
            var thu = mon.AddDays(3);
            var fri = mon.AddDays(4);
            var sat = mon.AddDays(5);
            return CompanySuppliers
                .Where(cs => cs.Supplier.SupplierType == type)
                .Where(cs => cs.StartDate != null)
                .Any(cs => (cs.StartDate <= mon && (cs.EndDate == null || cs.EndDate >= mon))
                             || (cs.StartDate <= tue && (cs.EndDate == null || cs.EndDate >= tue))
                             || (cs.StartDate <= wed && (cs.EndDate == null || cs.EndDate >= wed))
                             || (cs.StartDate <= thu && (cs.EndDate == null || cs.EndDate >= thu))
                             || (cs.StartDate <= fri && (cs.EndDate == null || cs.EndDate >= fri))
                             || (cs.StartDate <= sat && (cs.EndDate == null || cs.EndDate >= sat)));
        }

        public List<Supplier> FruitSupplier(DateTime date)
        {
            return CompanySuppliers.Where(cs => cs.Supplier.SupplierType == SupplierType.Fruits)
                                   .Where(cs => cs.StartDate.HasValue && cs.StartDate <= date)
                                   .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= date)
                                   .Where(cs => cs.Supplier.Deleted == false)
                                   .Select(cs => cs.Supplier)
                                   .ToList();
        }

        public List<Supplier> MilkSupplier(DateTime date)
        {
            return CompanySuppliers.Where(cs => cs.Supplier.SupplierType == SupplierType.Milk)
                                   .Where(cs => cs.StartDate.HasValue && cs.StartDate <= date)
                                   .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= date)
                                   .Where(cs => cs.Supplier.Deleted == false)
                                   .Select(cs => cs.Supplier)
                                   .ToList();
        }

        public List<CompanySupplier> SupplierWithAccountNo(SupplierType type, DateTime week)
        {
            int diff = (week.DayOfWeek - DayOfWeek.Monday) % 7;
            var mon = week.AddDays(-1 * diff).Date;
            var tue = mon.AddDays(1);
            var wed = mon.AddDays(2);
            var thu = mon.AddDays(3);
            var fri = mon.AddDays(4);
            var sat = mon.AddDays(5);
            
            return CompanySuppliers
                .Where(cs => cs.Supplier.SupplierType == type)
                .Where(cs => cs.StartDate != null)
                .Where(cs => (cs.StartDate <= mon && (cs.EndDate == null || cs.EndDate >= mon))
                           || (cs.StartDate <= tue && (cs.EndDate == null || cs.EndDate >= tue))
                           || (cs.StartDate <= wed && (cs.EndDate == null || cs.EndDate >= wed))
                           || (cs.StartDate <= thu && (cs.EndDate == null || cs.EndDate >= thu))
                           || (cs.StartDate <= fri && (cs.EndDate == null || cs.EndDate >= fri))
                           || (cs.StartDate <= sat && (cs.EndDate == null || cs.EndDate >= sat))).ToList();
        }   

        public IEnumerable<Supplier> DryGoodsSuppliers()
        {
            return DryGoodsSuppliers(this);
        }

        public IEnumerable<Supplier> DryGoodsSuppliers(Company company)
        {
            return CompanySuppliers.Where(cs => cs.Supplier.SupplierType == SupplierType.DryGoods)
                                   .Where(cs => cs.StartDate.HasValue && cs.StartDate <= DateTime.Now)
                                   .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= DateTime.Now)
                                   .Where(cs => cs.Supplier.Deleted == false)
                                   .Select(cs => cs.Supplier);
        }

        public Supplier DryGoodsSupplier()
        {
            return DryGoodsSupplier(this);
        }

        public Supplier DryGoodsSupplier(Company company)
        {
            return CompanySuppliers.Where(cs => cs.Supplier.SupplierType == SupplierType.DryGoods)
                                   .Where(cs => cs.StartDate.HasValue && cs.StartDate <= DateTime.Now)
                                   .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= DateTime.Now)
                                   .Where(cs => cs.Supplier.Deleted == false)
                                   .Select(cs => cs.Supplier)
                                   .FirstOrDefault();
        }

        public bool IsFruitDeliveryable(DateTime date)
        {
            return FruitSupplier(date).Count() > 0;
        }

        public bool IsMilkDeliveryable(DateTime date)
        {
            return MilkSupplier(date).Count() > 0;
        }

        public DateTime? NextDryGoodsDeliveryDate(Guid? supplierID)
        {
            if (supplierID == null)
                return null;
            
            var companySupplier = CompanySuppliers.Where(cs => cs.Supplier.SupplierType == SupplierType.DryGoods)
                                            .Where(cs => cs.StartDate.HasValue && cs.StartDate <= DateTime.Now)
                                            .Where(cs => !cs.EndDate.HasValue || cs.EndDate >= DateTime.Now)
                                            .Where(cs => cs.Supplier.Deleted == false)
                                            .Where(cs => cs.SupplierID == supplierID)
                                            .FirstOrDefault();
            if (companySupplier == null) return null;

            DateTime dateToday = DateTime.Now.AddHours(8).Date;
            DayOfWeek weekdayToday = dateToday.DayOfWeek;
            DateTime date = dateToday;
            DateTime? deliveryDate = null;
            while (deliveryDate == null)
            {
                date = date.AddDays(1);
                switch (date.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (companySupplier.DeliveryMon)
                            deliveryDate = date;
                        break;
                    case DayOfWeek.Tuesday:
                        if (companySupplier.DeliveryTue)
                            deliveryDate = date;
                        break;
                    case DayOfWeek.Wednesday:
                        if (companySupplier.DeliveryWed)
                            deliveryDate = date;
                        break;
                    case DayOfWeek.Thursday:
                        if (companySupplier.DeliveryThu)
                            deliveryDate = date;
                        break;
                    case DayOfWeek.Friday:
                        if (companySupplier.DeliveryFri)
                            deliveryDate = date;
                        break;
                    case DayOfWeek.Saturday:
                        if (companySupplier.DeliverySat)
                            deliveryDate = date;
                        break;
                }
            };

            return deliveryDate.Value;
        }

    }

    public class CompanyViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string AccountNo { get; set; }

        public DateTime SignupDate { get; set; }
        public bool AccountClosed { get; set; }
        public bool DeleteData { get; set; }

        [Required]
        public string Name { get; set; }

        public string Motto { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string SecondaryEmail { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string VatNumber { get; set; }

        public string BillingCompanyName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingPostcode { get; set; }
        public string BillingEmail { get; set; }

        public string LogoImageURL { get; set; }

        public decimal DefaultVatRate { get; set; }
        public int DefaultCurrencyID { get; set; }

        public bool UseTeams { get; set; }
        public bool UseHierarchy { get; set; }
        public bool AccessSameLevelHierarchy { get; set; }

        public int NumAccounts { get; set; }
        public int NumConfirmedAccounts { get; set; }

        public string FruitSupplierID { get; set; }
        public string MilkSupplierID { get; set; }
        public bool HasFruitSupplier { get; set; }
        public bool HasMilkSupplier { get; set; }
        public bool LiveOnFruit { get; set; }
        public bool LiveOnMilk { get; set; }
        public bool LiveOnDryGoods { get; set; }

        public bool MilkDeliveryMonday { get; set; }
        public bool MilkDeliveryTuesday { get; set; }
        public bool MilkDeliveryWednesday { get; set; }
        public bool MilkDeliveryThursday { get; set; }
        public bool MilkDeliveryFriday { get; set; }
        public bool MilkDeliverySaturday { get; set; }
        public bool MilkDeliverySunday { get; set; }

        public bool FruitsDeliveryMonday { get; set; }
        public bool FruitsDeliveryTuesday { get; set; }
        public bool FruitsDeliveryWednesday { get; set; }
        public bool FruitsDeliveryThursday { get; set; }
        public bool FruitsDeliveryFriday { get; set; }
        public bool FruitsDeliverySaturday { get; set; }
        public bool FruitsDeliverySunday { get; set; }

        public bool DryGoodsDeliveryMonday { get; set; }
        public bool DryGoodsDeliveryTuesday { get; set; }
        public bool DryGoodsDeliveryWednesday { get; set; }
        public bool DryGoodsDeliveryThursday { get; set; }
        public bool DryGoodsDeliveryFriday { get; set; }
        public bool DryGoodsDeliverySaturday { get; set; }

        public string PermanentFruiPONo { get; set; }
        public string PermanentMilkPONo { get; set; }
        public string PermanentDryGoodsPONo { get; set; }
        public string DeliveryInstruction { get; set; }
        public string InternalDeliveryInstruction { get; set; }
        public string DpdFruitAmendments { get; set; }
        public string Depot { get; set; }
        public string Round { get; set; }
        public string CompanyRegistration { get; set; }
        public bool EasySuppliersActivated { get; set; }
        public bool IsMultiTenantedBuilding { get; set; }
        public bool HasMultiSites { get; set; }
        public bool IsTransferred { get; set; }
        public bool TransferEmailSent { get; set; }
        public bool NoDeliveryCharge { get; set; }
        public string ProformaApproverID { get; set; }

        public List<CompanySupplierViewModel> DryGoodsSuppliers { get; set; }
        public List<SelectItemViewModel> AssociatedCompanies { get; set; }

    }

    public class CompanyMapper : ModelMapper<Company, CompanyViewModel>
    {
        public override void MapToViewModel(Company model, CompanyViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.AccountNo = model.AccountNo;
            viewModel.Name = model.Name;
            viewModel.Address1 = model.Address1;
            viewModel.Address2 = model.Address2;
            viewModel.Address3 = model.Address3;
            viewModel.Town = model.Town;
            viewModel.County = model.County;
            viewModel.Postcode = model.Postcode;
            viewModel.Telephone = model.Telephone;
            viewModel.Email = model.Email?.Trim();
            viewModel.SecondaryEmail = model.SecondaryEmail?.Trim();
            viewModel.SignupDate = model.SignupDate;

            viewModel.BillingAddress = model.BillingAddress;
            viewModel.BillingPostcode = model.BillingPostcode;
            viewModel.BillingCompanyName = model.BillingCompanyName;
            viewModel.BillingEmail = model.BillingEmail;

            viewModel.NumAccounts = model.ApplicationUsers.Count;
            viewModel.NumAccounts = model.ApplicationUsers.Where(u => u.EmailConfirmed == true).Count();

            viewModel.HasFruitSupplier = model.Method_HasSupplier(SupplierType.Fruits);
            viewModel.HasMilkSupplier = model.Method_HasSupplier(SupplierType.Milk);

            viewModel.LiveOnFruit = model.Method_IsLive(SupplierType.Fruits);
            viewModel.LiveOnMilk = model.Method_IsLive(SupplierType.Milk);
            viewModel.LiveOnDryGoods = model.Method_IsLive(SupplierType.DryGoods);

            viewModel.MilkDeliveryMonday = model.MilkDeliveryMonday;
            viewModel.MilkDeliveryTuesday = model.MilkDeliveryTuesday;
            viewModel.MilkDeliveryWednesday = model.MilkDeliveryWednesday;
            viewModel.MilkDeliveryThursday = model.MilkDeliveryThursday;
            viewModel.MilkDeliveryFriday = model.MilkDeliveryFriday;
            viewModel.MilkDeliverySaturday = model.MilkDeliverySaturday;
            viewModel.MilkDeliverySunday = model.MilkDeliverySunday;

            viewModel.FruitsDeliveryMonday = model.FruitsDeliveryMonday;
            viewModel.FruitsDeliveryTuesday = model.FruitsDeliveryTuesday;
            viewModel.FruitsDeliveryWednesday = model.FruitsDeliveryWednesday;
            viewModel.FruitsDeliveryThursday = model.FruitsDeliveryThursday;
            viewModel.FruitsDeliveryFriday = model.FruitsDeliveryFriday;
            viewModel.FruitsDeliverySaturday = model.FruitsDeliverySaturday;
            viewModel.FruitsDeliverySunday = model.FruitsDeliverySunday;

            viewModel.DryGoodsDeliveryMonday = model.DryGoodsDeliveryMonday;
            viewModel.DryGoodsDeliveryTuesday = model.DryGoodsDeliveryTuesday;
            viewModel.DryGoodsDeliveryWednesday = model.DryGoodsDeliveryWednesday;
            viewModel.DryGoodsDeliveryThursday = model.DryGoodsDeliveryThursday;
            viewModel.DryGoodsDeliveryFriday = model.DryGoodsDeliveryFriday;
            viewModel.DryGoodsDeliverySaturday= model.DryGoodsDeliverySaturday;

            viewModel.PermanentFruiPONo = model.PermanentFruiPONo;
            viewModel.PermanentMilkPONo = model.PermanentMilkPONo;
            viewModel.PermanentDryGoodsPONo = model.PermanentDryGoodsPONo;

            viewModel.DeliveryInstruction = model.DeliveryInstruction;
            viewModel.InternalDeliveryInstruction = model.InternalDeliveryInstruction;
            viewModel.DpdFruitAmendments = model.DpdFruitAmendments;
            viewModel.Depot = model.Depot;
            viewModel.Round = model.Round;
            viewModel.CompanyRegistration = model.CompanyRegistration;
            viewModel.EasySuppliersActivated = model.EasySuppliersActivated;

            viewModel.DryGoodsSuppliers = model.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.DryGoods)
                                                                .Where(x => x.EndDate == null || x.EndDate >= DateTime.Today)
                                                                .ToList()
                                                                .Select(x => new CompanySupplierMapper().MapToViewModel(x))
                                                                .ToList();

            viewModel.IsMultiTenantedBuilding = model.IsMultiTenantedBuilding;
            viewModel.HasMultiSites = model.HasMultiSites;
            viewModel.IsTransferred = model.IsTransferred;
            viewModel.TransferEmailSent = model.TransferEmailSent;
            viewModel.NoDeliveryCharge = model.NoDeliveryCharge;
            viewModel.ProformaApproverID = model.ProformaApproverID?.ToString();

            viewModel.AssociatedCompanies = model.CompanyCollection?.Companies.Where(c => c.ID != model.ID).Select(c => new SelectItemViewModel
            {
                ID = c.ID.ToString(),
                Name = "(" + c.AccountNo + ") " + c.Name + " (" + c.Postcode + ")"
            }).ToList();

        }

        public override void MapToModel(CompanyViewModel viewModel, Company model)
        {
            //ID = viewModel.ID;
            model.Name = viewModel.Name;
            model.Address1 = viewModel.Address1;
            model.Address2 = viewModel.Address2;
            model.Address3 = viewModel.Address3;
            model.Town = viewModel.Town;
            model.County = viewModel.County;
            model.Postcode = viewModel.Postcode;
            model.Telephone = viewModel.Telephone;
            model.Email = viewModel.Email?.Trim();
            model.SecondaryEmail = viewModel.SecondaryEmail?.Trim();

            model.BillingAddress = viewModel.BillingAddress;
            model.BillingPostcode = viewModel.BillingPostcode;
            model.BillingCompanyName = viewModel.BillingCompanyName;
            model.BillingEmail = viewModel.BillingEmail;

            model.MilkDeliveryMonday = viewModel.MilkDeliveryMonday;
            model.MilkDeliveryTuesday = viewModel.MilkDeliveryTuesday;
            model.MilkDeliveryWednesday = viewModel.MilkDeliveryWednesday;
            model.MilkDeliveryThursday = viewModel.MilkDeliveryThursday;
            model.MilkDeliveryFriday = viewModel.MilkDeliveryFriday;
            model.MilkDeliverySaturday = viewModel.MilkDeliverySaturday;
            model.MilkDeliverySunday = viewModel.MilkDeliverySunday;

            model.FruitsDeliveryMonday = viewModel.FruitsDeliveryMonday;
            model.FruitsDeliveryTuesday = viewModel.FruitsDeliveryTuesday;
            model.FruitsDeliveryWednesday = viewModel.FruitsDeliveryWednesday;
            model.FruitsDeliveryThursday = viewModel.FruitsDeliveryThursday;
            model.FruitsDeliveryFriday = viewModel.FruitsDeliveryFriday;
            model.FruitsDeliverySaturday = viewModel.FruitsDeliverySaturday;
            model.FruitsDeliverySunday = viewModel.FruitsDeliverySunday;

            model.DryGoodsDeliveryMonday = viewModel.DryGoodsDeliveryMonday;
            model.DryGoodsDeliveryTuesday = viewModel.DryGoodsDeliveryTuesday;
            model.DryGoodsDeliveryWednesday = viewModel.DryGoodsDeliveryWednesday;
            model.DryGoodsDeliveryThursday = viewModel.DryGoodsDeliveryThursday;
            model.DryGoodsDeliveryFriday = viewModel.DryGoodsDeliveryFriday;
            model.DryGoodsDeliverySaturday = viewModel.DryGoodsDeliverySaturday;

            model.PermanentFruiPONo = viewModel.PermanentFruiPONo;
            model.PermanentMilkPONo = viewModel.PermanentMilkPONo;
            model.PermanentDryGoodsPONo = viewModel.PermanentDryGoodsPONo;

            model.DeliveryInstruction = viewModel.DeliveryInstruction;
            model.InternalDeliveryInstruction = viewModel.InternalDeliveryInstruction;
            model.DpdFruitAmendments = viewModel.DpdFruitAmendments;
            model.Depot = viewModel.Depot;
            model.Round = viewModel.Round;
            model.AccountNo = viewModel.AccountNo;
            model.CompanyRegistration = viewModel.CompanyRegistration;

            model.NoDeliveryCharge = viewModel.NoDeliveryCharge;
            model.ProformaApproverID = viewModel.ProformaApproverID == null ? (Guid?)null : Guid.Parse(viewModel.ProformaApproverID);
        }

    }

    public class CompanyGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public DateTime SignupDate { get; set; }
        public string Name { get; set; }
        public string FruitSupplierName { get; set; }
        public string MilkSupplierName { get; set; }
        public bool HasFruitSupplier { get; set; }
        public bool HasSnackSupplier { get; set; }
        public bool HasMilkSupplier { get; set; }
        public bool LiveOnFruit { get; set; }
        public bool LiveOnMilk { get; set; }
        public bool LiveOnDryGoods { get; set; }
        public bool LiveOnSnack { get; set; }
        public bool EasySuppliersActivated { get; set; }
        public DateTime? AskingForFruitSupplier { get; set; }
        public DateTime? AskingForMilkSupplier { get; set; }
        public DateTime? AskingForDryGoodsSupplier { get; set; }
        public DateTime? AskingForSnackSupplier { get; set; }
        public string AccountNo { get; set; }
        public List<CompanyDryGoodsSupStatusViewModel> DryGoodsStatuses { get; set; }
    }

    public class CompanyGridMapper : ModelMapper<Company, CompanyGridViewModel>
    {
        public override void MapToModel(CompanyGridViewModel viewModel, Company model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(Company model, CompanyGridViewModel viewModel)
        {
            viewModel.HasFruitSupplier = model.Method_HasSupplier(SupplierType.Fruits);
            viewModel.HasSnackSupplier = model.Method_HasSupplier(SupplierType.Snacks);
            viewModel.HasMilkSupplier = model.Method_HasSupplier(SupplierType.Milk);
            viewModel.ID = model.ID.ToString();
            viewModel.LiveOnFruit = model.Method_IsLive(SupplierType.Fruits);
            viewModel.LiveOnSnack = model.Method_IsLive(SupplierType.Snacks);
            viewModel.LiveOnMilk = model.Method_IsLive(SupplierType.Milk);
            viewModel.LiveOnDryGoods = model.Method_IsLive(SupplierType.DryGoods);
            viewModel.Name = model.Name;
            viewModel.SignupDate = model.SignupDate;
            viewModel.AskingForDryGoodsSupplier = model.AskingForDryGoodsSupplier;
            viewModel.AskingForFruitSupplier = model.AskingForFruitSupplier;
            viewModel.AskingForMilkSupplier = model.AskingForMilkSupplier;
            viewModel.AskingForSnackSupplier = model.AskingForSnackSupplier;
            viewModel.AccountNo = model.AccountNo;
            viewModel.EasySuppliersActivated = model.EasySuppliersActivated;

            viewModel.DryGoodsStatuses = new List<CompanyDryGoodsSupStatusViewModel>();
            var difficultSups = new SupplierRepository().Read().Where(x => x.Deleted == false).Where(x => x.SupplierType == SupplierType.DryGoods).Where(x => x.UseMasterAccount == false).ToList();
            foreach (var sup in difficultSups)
            {
                var status = new CompanyDryGoodsSupStatusViewModel
                {
                    SupplierID = sup.ID.ToString(),
                    SupplierName = sup.Name,
                    CompanyID = model.ID.ToString(),
                    CompanyName = model.Name,
                    Live = false,
                    RequestTime = null
                };
                var t = model.DryGoodsSuppliers().ToList();
                if (model.DryGoodsSuppliers()
                         .Where(x => x.ID == sup.ID)
                         .Any())
                {
                    status.Live = true;
                }
                else if (model.SupAcctRequestLogs
                              .Where(x => x.SupplierID == sup.ID)
                              .Any())
                {
                    status.RequestTime = model.SupAcctRequestLogs
                                              .Where(x => x.SupplierID == sup.ID)
                                              .First()
                                              .RequestTime;
                }
                viewModel.DryGoodsStatuses.Add(status);
            }
        }
    }
    
    public class InactiveCompanyGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public DateTime SignupDate { get; set; }
        public string Name { get; set; }
        public string AccountNo { get; set; }
        public DateTime? LastOrderDate { get; set; }
    }
    
    public class InactiveCompanyGridMapper : ModelMapper<Company, InactiveCompanyGridViewModel>
    {
        public override void MapToModel(InactiveCompanyGridViewModel viewModel, Company model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(Company model, InactiveCompanyGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.SignupDate = model.SignupDate;
            viewModel.AccountNo = model.AccountNo;
            viewModel.LastOrderDate = GetLastOrderDate(model);
        }
        
        private DateTime? GetLastOrderDate(Company model)
        {
            var lastOneOffOrder = model.OneOffOrders.Where(x => x.Status == OneOffOrderStatus.Accepted).OrderByDescending(x => x.Created).FirstOrDefault();
            var lastMilkOrder = model.MilkOrders.Where(x => x.OrderMode == OrderMode.History).OrderByDescending(x => x.FromDate).FirstOrDefault();
            var lastFruitOrder = model.FruitOrders.Where(x => x.OrderMode == OrderMode.History).OrderByDescending(x => x.FromDate).FirstOrDefault();
            
            return new List<DateTime?> {lastOneOffOrder?.Created, lastMilkOrder?.FromDate, lastFruitOrder?.FromDate}.Max();
        }
    }

}