﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class Setting : IEntity
    {
        public Setting() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }

        public string MilkSupplierCenterEmail { get; set; }
        public string FruitSupplierCenterEmail { get; set; }
        public string DryGoodsSupplierCenterEmail { get; set; }
        public string FeedbackCenterEmail { get; set; }
        public string DefaultReference { get; set; }
        public long StartingNumber { get; set; }
        // Current DryGoods Order Number
        public int DryGoodOrderNumber { get; set; }
        
        public bool IsFreezeActivated { get; set; }
        public DateTime? FreezeStartDate { get; set; }
        public DateTime? FreezeEndDate { get; set; }
        public string FreezeMessage { get; set; }

    }
}
