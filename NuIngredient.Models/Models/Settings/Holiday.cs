﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{
    public class Holiday : IEntity
    {
        public Holiday() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        public Guid? SupplierID { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        
        public Supplier Supplier { get; set; }
    }

    public class HolidayGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        [Required, DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string SupplierID { get; set; }
    }

    public class HolidayGridMapper : ModelMapper<Holiday, HolidayGridViewModel>
    {
        public override void MapToModel(HolidayGridViewModel viewModel, Holiday model)
        {
            model.SupplierID = !string.IsNullOrWhiteSpace(viewModel.SupplierID) ? Guid.Parse(viewModel.SupplierID) : null as Guid?;
            model.Date = viewModel.Date.Date;
            model.Description = viewModel.Description;
        }

        public override void MapToViewModel(Holiday model, HolidayGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Date = model.Date.Date;
            viewModel.Description = model.Description;
            viewModel.SupplierID = model.SupplierID.ToString();
        }
    }
}
