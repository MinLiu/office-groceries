﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public enum OrderMode
    {
        Draft = 0,
        Regular = 1,
        Live = 2,
        OneOff = 3,
        History = 4,
        Credit = 5
    }

}
