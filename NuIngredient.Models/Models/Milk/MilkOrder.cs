﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkOrder : IEntity, ICloneable
    {
        public MilkOrder() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public LineItemType LineItemType { get; set; } = LineItemType.Product; 
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public Guid? MilkProductID { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int Sunday { get; set; }
        public decimal VATRate { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        public Guid OrderHeaderID { get; set; }
        public int SortPos { get; set; }
        [NotMapped]
        public int WeeklyVolume { get { return Monday + Tuesday + Wednesday + Thursday + Friday + Saturday + Sunday; } private set { } }

        [ForeignKey("MilkProductID")]
        public virtual MilkProduct Milk { get; set; }
        public virtual Company Company { get; set; }
        public virtual MilkOrderHeader OrderHeader { get; set; }
        public virtual ICollection<MilkCreditItem> Credits { get; set; }

        public int GetAmountByDayOfWeek(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return Monday;
                case DayOfWeek.Tuesday:
                    return Tuesday;
                case DayOfWeek.Wednesday:
                    return Wednesday;
                case DayOfWeek.Thursday:
                    return Thursday;
                case DayOfWeek.Friday:
                    return Friday;
                case DayOfWeek.Saturday:
                    return Saturday;
                case DayOfWeek.Sunday:
                    return Sunday;
                default:
                    return 0;
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public void CalculateTotal()
        {
            TotalNet = (Monday + Tuesday + Wednesday + Thursday + Friday + Saturday) * Price;
            TotalVAT = TotalNet * VATRate;
        }
    }

    #region MilkViewModel

    public class MilkOrderViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ImageUrl { get; set; }
        public string ProductName { get; set; }
        public string MilkID { get; set; }
        public string Unit { get; set; }
        public decimal VATRate { get; set; }
        public decimal Price { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int Sunday { get; set; }
        public int WeeklyVolume { get; set; }
        public decimal WeeklyTotal { get; set; }
        public DateTime? FromDate { get; set; }
        public OrderMode OrderMode { get; set; }
    }

    public class MilkOrderMapper : ModelMapper<MilkOrder, MilkOrderViewModel>
    {

        public override void MapToViewModel(MilkOrder model, MilkOrderViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ImageUrl = model.Milk?.ImageURL;
            viewModel.ProductName = model.ProductName;
            viewModel.MilkID = model.MilkProductID.ToString();
            viewModel.Monday = model.Monday;
            viewModel.Tuesday = model.Tuesday;
            viewModel.Wednesday = model.Wednesday;
            viewModel.Thursday = model.Thursday;
            viewModel.Friday = model.Friday;
            viewModel.Saturday = model.Saturday;
            viewModel.Sunday = model.Sunday;
            viewModel.Unit = model.Unit;
            viewModel.Price = model.Price;
            viewModel.WeeklyVolume = model.Monday + model.Tuesday + model.Wednesday + model.Thursday + model.Friday + model.Saturday + model.Sunday;
            viewModel.WeeklyTotal = model.TotalNet;
            viewModel.VATRate = model.VATRate;
        }

        public override void MapToModel(MilkOrderViewModel viewModel, MilkOrder model)
        {
            model.ProductName = viewModel.ProductName;
            model.Unit = viewModel.Unit;
            model.Monday = viewModel.Monday;
            model.Tuesday = viewModel.Tuesday;
            model.Wednesday = viewModel.Wednesday;
            model.Thursday = viewModel.Thursday;
            model.Friday = viewModel.Friday;
            model.Saturday = viewModel.Saturday;
            model.Sunday = viewModel.Sunday;
            model.MilkProductID = !string.IsNullOrWhiteSpace(viewModel.MilkID) ? Guid.Parse(viewModel.MilkID) : null as Guid?;
            model.Price = viewModel.Price;
            model.TotalNet = model.Price * (model.Monday + model.Tuesday + model.Wednesday + model.Thursday + model.Friday + model.Saturday + model.Sunday);
            model.VATRate = viewModel.VATRate;
            model.TotalVAT = model.TotalNet * model.VATRate;
        }

    }

    #endregion
}
