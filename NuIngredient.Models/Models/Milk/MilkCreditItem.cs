﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkCreditItem : IEntity
    {
        public MilkCreditItem() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid MilkCreditHeaderID { get; set; }
        public Guid MilkOrderItemID { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }

        public virtual MilkCreditHeader MilkCreditHeader { get; set; }
        public virtual MilkOrder MilkOrderItem { get; set; }
    }

    public class MilkCreditItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public string CreateBy { get; set; }
        public DateTime Created { get; set; }
        public string Narrative { get; set; }
    }

    public class MilkCreditItemMapper : ModelMapper<MilkCreditItem, MilkCreditItemViewModel>
    {
        public override void MapToModel(MilkCreditItemViewModel viewModel, MilkCreditItem model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(MilkCreditItem model, MilkCreditItemViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CreateBy = model.MilkCreditHeader.CreateBy.Email;
            viewModel.Created = model.MilkCreditHeader.Created;
            viewModel.Narrative = model.MilkCreditHeader.Narrative;
            viewModel.ImageURL = model.MilkOrderItem.Milk != null ? model.MilkOrderItem.Milk.ImageURL : "";
            viewModel.ProductName = model.MilkOrderItem.ProductName;
            viewModel.Unit = model.MilkOrderItem.Unit;
            viewModel.Monday = model.Monday;
            viewModel.Tuesday = model.Tuesday;
            viewModel.Wednesday = model.Wednesday;
            viewModel.Thursday = model.Thursday;
            viewModel.Friday = model.Friday;
            viewModel.Saturday = model.Saturday;
        }
    }
}
