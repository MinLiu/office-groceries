﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkCreditItemRepository : EntityRespository<MilkCreditItem>
    {
        public MilkCreditItemRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkCreditItemRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
