﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkCreditHeaderRepository : EntityRespository<MilkCreditHeader>
    {
        public MilkCreditHeaderRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkCreditHeaderRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
