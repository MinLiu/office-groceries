﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkOrderRepository : EntityRespository<MilkOrder>
    {
        public MilkOrderRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkOrderRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
