﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkOneOffChangeTrackerRepository : EntityRespository<MilkOneOffChangeTracker>
    {
        public MilkOneOffChangeTrackerRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkOneOffChangeTrackerRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
