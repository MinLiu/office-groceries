﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkOrderHeaderRepository : EntityRespository<MilkOrderHeader>
    {
        public MilkOrderHeaderRepository()
            : this(new SnapDbContext())
        {

        }

        public MilkOrderHeaderRepository(SnapDbContext db)
            : base(db)
        {

        }

        public new IQueryable<MilkOrderHeader> Read()
        {
            return Read(false);
        }

        public IQueryable<MilkOrderHeader> Read(bool includeDepreciated)
        {
            return base.Read().Where(x => x.Depreciated == includeDepreciated).OrderByDescending(x => x.LatestUpdated);
        }

        public override void Delete(MilkOrderHeader entity)
        {
            _db.Set<MilkOrder>().RemoveRange(_db.Set<MilkOrder>().Where(x => x.OrderHeaderID == entity.ID));
            _db.Set<MilkOneOffChangeTracker>().RemoveRange(_db.Set<MilkOneOffChangeTracker>().Where(x => x.OrderHeaderID == entity.ID));
            _db.Set<MilkOrderHeader>().Remove(entity);
            _db.SaveChanges();
        }

        public override void Delete(IEnumerable<MilkOrderHeader> entities)
        {
            foreach (var entity in entities.ToList())
            {
                Delete(entity);
            }
        }
    }

}
