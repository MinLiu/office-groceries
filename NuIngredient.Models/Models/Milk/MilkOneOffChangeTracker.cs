﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkOneOffChangeTracker : IEntity, ICloneable
    {
        public MilkOneOffChangeTracker() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid MilkProductID { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public Guid OrderHeaderID { get; set; }

        [ForeignKey("MilkProductID")]
        public virtual MilkProduct Milk { get; set; }
        public virtual MilkOrderHeader OrderHeader { get; set; }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

}
