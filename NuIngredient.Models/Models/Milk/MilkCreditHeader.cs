﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkCreditHeader : IEntity
    {
        public MilkCreditHeader() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid? CompanyID { get; set; }
        public Guid? SupplierID { get; set; }
        public string CreateByID { get; set; }
        public DateTime Created { get; set; }
        public Guid OrderHeaderID { get; set; }
        [Required]
        public string Narrative { get; set; }

        public virtual MilkOrderHeader OrderHeader { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<MilkOrder> Items { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual User CreateBy { get; set; }
    }

    public class MilkCreditHeaderViewModel : IEntityViewModel
    {
        public string ID { get; set; }
    }
}
