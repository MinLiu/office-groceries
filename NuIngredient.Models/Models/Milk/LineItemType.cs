﻿namespace NuIngredient.Models
{
    public enum LineItemType
    {
        Product = 0,
        DeliveryCharge = 1
    }
}