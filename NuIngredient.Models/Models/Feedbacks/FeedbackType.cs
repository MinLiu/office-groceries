﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FeedbackType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Colour { get; set; }
    }

    public static class FeedbackTypeValues
    {
        public static int Milk = 1;
        public static int Fruits = 2;
        public static int DryGoods = 3;
        public static int Others = 4;
    }

}
