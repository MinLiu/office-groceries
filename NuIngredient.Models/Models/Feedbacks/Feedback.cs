﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class Feedback : IEntity
    {
        public Feedback()
        {
            ID = Guid.NewGuid();
            Created = DateTime.Now;
        }

        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public string UserID { get; set; }
        public string Message { get; set; }
        public DateTime Created { get; set; }
        public string NIComment { get; set; }
        public string ReplierID { get; set; }
        public DateTime? ResponseTime { get; set; }
        public Guid? DryGoodsOrderID { get; set; }
        public int FeedbackTypeID { get; set; }

        public virtual Company Company { get; set; }
        public virtual User User { get; set; }
        public virtual User Replier { get; set; }
        public virtual FeedbackType FeedbackType { get; set; }
        public virtual OneOffOrder DryGoodsOrder { get; set; }
    }

    public class FeedbackViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Message { get; set; }
        public DateTime Created { get; set; }
        public string NIComment { get; set; }
        public DateTime? ResponseTime { get; set; }
        public Guid? DryGoodsOrderID { get; set; }
        public int FeedbackTypeID { get; set; }

        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public string Replier { get; set; }
        public FeedbackType FeedbackType { get; set; }
        public OneOffOrder DryGoodsOrder { get; set; }
    }

    public class FeedbackGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public DateTime Created { get; set; }
        public string Message { get; set; }
        public FeedbackType FeedbackType { get; set; }
        public DateTime? ResponseTime { get; set; }
        public string Replier { get; set; }
        public string NIComment { get; set; }
    }

    public class FeedbackGridMapper : ModelMapper<Feedback, FeedbackGridViewModel>
    {
        public override void MapToModel(FeedbackGridViewModel viewModel, Feedback model)
        {
            throw new NotImplementedException();
        }
        public override void MapToViewModel(Feedback model, FeedbackGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyName = model.Company.Name;
            viewModel.FeedbackType = model.FeedbackType;
            viewModel.Created = model.Created;
            viewModel.Message = model.Message;
            viewModel.ResponseTime = model.ResponseTime;
            viewModel.UserName = model.User?.FirstName + " " + model.User?.LastName;
            viewModel.Replier = model.Replier != null ? model.Replier?.FirstName + " " + model.Replier?.LastName : "";
            viewModel.NIComment = model.NIComment;
        }
    }

    public class FeedbackMapper : ModelMapper<Feedback, FeedbackViewModel>
    {
        public override void MapToModel(FeedbackViewModel viewModel, Feedback model)
        {
            model.NIComment = viewModel.NIComment;
            model.ResponseTime = DateTime.Now;
        }
        public override void MapToViewModel(Feedback model, FeedbackViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyName = model.Company.Name;
            viewModel.Created = model.Created;
            viewModel.DryGoodsOrderID = model.DryGoodsOrderID;
            viewModel.FeedbackType = model.FeedbackType;
            viewModel.FeedbackTypeID = model.FeedbackTypeID;
            viewModel.Message = model.Message;
            viewModel.NIComment = model.NIComment;
            viewModel.Replier = model.Replier != null ? model.Replier.FirstName + " " + model.Replier.LastName : "";
            viewModel.ResponseTime = model.ResponseTime;
            viewModel.UserName = model.User?.FirstName + " " + model.User?.LastName;
            if (model.DryGoodsOrderID != null)
                viewModel.DryGoodsOrder = model.DryGoodsOrder;
        }

    }
}
