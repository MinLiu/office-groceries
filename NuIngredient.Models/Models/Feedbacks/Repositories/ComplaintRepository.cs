﻿
namespace NuIngredient.Models
{
    public class FeedbackRepository : EntityRespository<Feedback>
    {
        public FeedbackRepository()
            : this(new SnapDbContext())
        {

        }

        public FeedbackRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
