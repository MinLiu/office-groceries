﻿namespace NuIngredient.Models
{
    public enum OrderReschedulePolicy
    {
        ReplaceExisting,
        AddToExisting
    }
}