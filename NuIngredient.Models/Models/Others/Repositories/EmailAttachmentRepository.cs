﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class EmailAttachmentRepository : EntityRespository<EmailAttachment>
    {
        public EmailAttachmentRepository()
            : this(new SnapDbContext())
        {

        }

        public EmailAttachmentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
