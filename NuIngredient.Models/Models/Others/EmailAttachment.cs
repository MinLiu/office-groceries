﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class EmailAttachment : IEntity
    {
        public EmailAttachment() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        [Index]
        public Guid ReferenceID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime Created { get; set; }

    }

    public class EmailAttachmentGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }

    public class EmailAttachmentGridMapper : ModelMapper<EmailAttachment, EmailAttachmentGridViewModel>
    {
        public override void MapToModel(EmailAttachmentGridViewModel viewModel, EmailAttachment model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(EmailAttachment model, EmailAttachmentGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.FilePath = model.FilePath;
            viewModel.FileName = model.FileName;
        }
    }
}