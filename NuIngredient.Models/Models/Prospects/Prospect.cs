﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{
    public class Prospect : IEntity
    {
        public Prospect() { ID = Guid.NewGuid(); }
        public Guid ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CompanyName { get; set; }
        public string ProspectToken { get; set; }
        public DateTime Created { get; set; }
        public string Postcode { get; set; }
        public string Note { get; set; }
        public int CountQuoteEmail { get; set; }
        public int CountPromotionEmail { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DateDeleted { get; set; }
    }

    #region Prospect View Model
    
    public class ProspectViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [Phone(ErrorMessage = "Invalid Phone Number")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Business Name")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "Delivery Postcode")]
        public string Postcode { get; set; }
    }

    public class ProspectMapper : ModelMapper<Prospect, ProspectViewModel>
    {
        public override void MapToModel(ProspectViewModel viewModel, Prospect model)
        {
            model.FirstName = viewModel.FirstName;
            model.LastName = viewModel.LastName;
            model.Email = viewModel.Email;
            model.Phone = viewModel.Phone;
            model.CompanyName = viewModel.CompanyName;
            model.Postcode = viewModel.Postcode;
        }

        public override void MapToViewModel(Prospect model, ProspectViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.FirstName = model.FirstName;
            viewModel.LastName = model.LastName;
            viewModel.Email = model.Email;
            viewModel.Phone = model.Phone;
            viewModel.CompanyName = model.CompanyName;
            viewModel.Postcode = model.Postcode;
        }
    }

    #endregion

    #region Prospect Grid View Model

    public class ProspectGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        public string ProspectToken { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string CompanyName { get; set; }

        public DateTime Created { get; set; }
        public string Postcode { get; set; }
        public string Note { get; set; }
        public DateTime? DateDeleted { get; set; }
        public int CountQuoteEmail { get; set; }
        public int CountPromotionEmail { get; set; }
        public decimal TotalValue { get; set; }
    }

    public class ProspectGridMapper : ModelMapper<Prospect, ProspectGridViewModel>
    {
        public override void MapToModel(ProspectGridViewModel viewModel, Prospect model)
        {
            model.Note = viewModel.Note;
            model.Postcode = viewModel.Postcode;
            model.CompanyName = viewModel.CompanyName;
            model.FirstName = viewModel.Name;
            model.Email = viewModel.Email;
            model.Phone = viewModel.Phone;
        }

        public override void MapToViewModel(Prospect model, ProspectGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProspectToken = model.ProspectToken;
            viewModel.Name = model.FirstName;
            viewModel.Email = model.Email;
            viewModel.Phone = model.Phone;
            viewModel.CompanyName = model.CompanyName;
            viewModel.Created = model.Created;
            viewModel.Postcode = model.Postcode;
            viewModel.Note = model.Note;
            viewModel.DateDeleted = model.DateDeleted;
            viewModel.CountQuoteEmail = model.CountQuoteEmail;
            viewModel.CountPromotionEmail = model.CountPromotionEmail;
        }
    }

    #endregion
}
