﻿using System;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class PromotionOffer : IEntity
    {
        public PromotionOffer() { ID = Guid.NewGuid(); }
        public Guid ID { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }

    #region PromotionOffer View Model

    public class PromotionOfferViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        public string Subject { get; set; }
        
        [AllowHtml]
        public string Content { get; set; }
    }

    public class PromotionOfferMapper : ModelMapper<PromotionOffer, PromotionOfferViewModel>
    {
        public override void MapToModel(PromotionOfferViewModel viewModel, PromotionOffer model)
        {
            model.Subject = viewModel.Subject;
            model.Content = viewModel.Content;
        }

        public override void MapToViewModel(PromotionOffer model, PromotionOfferViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Subject = model.Subject;
            viewModel.Content = model.Content;
        }
    }

    #endregion
}
