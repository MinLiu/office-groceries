﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class PromotionOfferRepository : EntityRespository<PromotionOffer>
    {
        public PromotionOfferRepository()
            : this(new SnapDbContext())
        {

        }

        public PromotionOfferRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override DbSet<PromotionOffer> Read()
        {
            var result = base.Read();
            if (!result.Any())
            {
                var model = new PromotionOffer()
                {
                    ID = Guid.NewGuid(),
                    Subject = "",
                    Content = "<div style='text-align: center;'><strong style='color: #32b24a; font-size: 28px;'>Promotional Offer</strong><br/>" + "<strong>Activate within the next 4 days and you will receive 15% off your 1st months invoice.</strong></div>"
                };
                Create(model);
                
                result = base.Read();
            }
            return result;
        }
    }

}
