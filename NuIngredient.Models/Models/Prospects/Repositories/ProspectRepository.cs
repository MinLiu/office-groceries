﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ProspectRepository : EntityRespository<Prospect>
    {
        public ProspectRepository()
            : this(new SnapDbContext())
        {

        }

        public ProspectRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Prospect entity)
        {
            if (!string.IsNullOrWhiteSpace(entity.ProspectToken))
            {
                _db.Set<FruitOrder>().RemoveRange(_db.Set<FruitOrder>().Where(x => x.OrderHeader.ProspectToken == entity.ProspectToken));
                _db.Set<FruitOrderHeader>().RemoveRange(_db.Set<FruitOrderHeader>().Where(x => x.ProspectToken == entity.ProspectToken));
                _db.Set<MilkOrder>().RemoveRange(_db.Set<MilkOrder>().Where(x => x.OrderHeader.ProspectToken == entity.ProspectToken));
                _db.Set<MilkOrderHeader>().RemoveRange(_db.Set<MilkOrderHeader>().Where(x => x.ProspectToken == entity.ProspectToken));
                _db.Set<ShoppingCartItem>().RemoveRange(_db.Set<ShoppingCartItem>().Where(x => x.ProspectToken == entity.ProspectToken));
                _db.SaveChanges();
            }
            base.Delete(entity);
        }
    }

}
