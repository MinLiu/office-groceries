﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class AisleMoreInfoContent : IEntity
    {
        public AisleMoreInfoContent() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid AisleID { get; set; }
        public string Label { get; set; }
        public string Content { get; set; }
        public int SortPosition { get; set; }

        public virtual Aisle Aisle { get; set; }
    }

    public class AisleMoreInfoContentViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string AisleID { get; set; }
        public string Label { get; set; }
        [AllowHtml]
        public string Content { get; set; }
    }

    public class AisleMoreInfoContentGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string AisleID { get; set; }
        public string Label { get; set; }
        [AllowHtml]
        public string Content { get; set; }
    }

    public class AisleMoreInfoContentMapper : ModelMapper<AisleMoreInfoContent, AisleMoreInfoContentViewModel>
    {
        public override void MapToModel(AisleMoreInfoContentViewModel viewModel, AisleMoreInfoContent model)
        {
            model.Label = viewModel.Label;
            model.Content = viewModel.Content;
            model.AisleID = Guid.Parse(viewModel.AisleID);
        }
        public override void MapToViewModel(AisleMoreInfoContent model, AisleMoreInfoContentViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.AisleID = model.AisleID.ToString();
            viewModel.Label = model.Label;
            viewModel.Content = model.Content;
        }
    }

    public class AisleMoreInfoContentGridMapper : ModelMapper<AisleMoreInfoContent, AisleMoreInfoContentGridViewModel>
    {
        public override void MapToModel(AisleMoreInfoContentGridViewModel viewModel, AisleMoreInfoContent model)
        {
            model.Label = viewModel.Label;
            model.Content = viewModel.Content;
            model.AisleID = Guid.Parse(viewModel.AisleID);
        }
        public override void MapToViewModel(AisleMoreInfoContent model, AisleMoreInfoContentGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.AisleID = model.AisleID.ToString();
            viewModel.Label = model.Label;
            viewModel.Content = model.Content;
        }
    }

}
