﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class Aisle : IEntity
    {
        public Aisle() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public bool Deleted { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string LandingPageFilePath { get; set; }
        public string ProspectContentFilePath { get; set; }
        public string CustomerContentFilePath { get; set; }
        public string LandingPageContent { get; set; }
        public string ProspectContent { get; set; }
        public string CustomerContent { get; set; }
        public bool IsConstant { get; set; }
        public int SortPosition { get; set; }
        public bool IsMain { get; set; }
        public Guid? DefaultSupplierID { get; set; }
        public string InternalName { get; set; }
        public string H1Tag { get; set; }
        public string AisleImageUrl { get; set; }
        public AisleType AisleType { get; set; }

        public virtual ICollection<DryGoodsAisle> Products { get; set; }
        [ForeignKey("DefaultSupplierID")]
        public virtual Supplier Supplier { get; set; }
        //public virtual ICollection<Supplier> DefaultSupplier { get; set; }
        public virtual ICollection<AisleMoreInfoContent> MoreInfo { get; set; }
        public virtual ICollection<AisleExcludeCompany> ExcludeCompanies { get; set; }

    }

    public class AisleViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [Required]
        public string Name { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        [Required]
        public string Label { get; set; }
        public string LandingPageFilePath { get; set; }
        public string ProspectContentFilePath { get; set; }
        public string CustomerContentFilePath { get; set; }
        [AllowHtml]
        public string LandingPageContent { get; set; }
        [AllowHtml]
        public string ProspectContent { get; set; }
        [AllowHtml]
        public string CustomerContent { get; set; }

        public bool IsConstant { get; set; }
        public int SortPosition { get; set; }
        public bool IsMain { get; set; }
        public string DefaultSupplierID { get; set; }
        public string InternalName { get; set; }
        public string H1Tag { get; set; }
        public string AisleImageUrl { get; set; }
        public AisleType AisleType { get; set; }
    }

    public class AisleGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string LandingPageFilePath { get; set; }
        public string ProspectContentFilePath { get; set; }
        public string CustomerContentFilePath { get; set; }
        public int SortPosition { get; set; }
        public bool IsConstant { get; set; }
        public string SupplierName { get; set; }
        public bool IsMain { get; set; }
    }

    public class AisleMapper : ModelMapper<Aisle, AisleViewModel>
    {
        public override void MapToModel(AisleViewModel viewModel, Aisle model)
        {
            model.StartDate = viewModel.StartDate;
            model.EndDate = viewModel.EndDate;
            model.Name = viewModel.Name;
            model.MetaTitle = viewModel.MetaTitle;
            model.MetaKeyword = viewModel.MetaKeyword;
            model.MetaDescription = viewModel.MetaDescription;
            model.Label= viewModel.Label;
            model.LandingPageFilePath= viewModel.LandingPageFilePath;
            model.ProspectContentFilePath= viewModel.ProspectContentFilePath;
            model.CustomerContentFilePath= viewModel.CustomerContentFilePath;
            model.LandingPageContent = viewModel.LandingPageContent;
            model.ProspectContent = viewModel.ProspectContent;
            model.CustomerContent = viewModel.CustomerContent;
            model.SortPosition = viewModel.SortPosition;
            model.IsMain = viewModel.IsMain;
            model.DefaultSupplierID = !string.IsNullOrEmpty(viewModel.DefaultSupplierID) ? Guid.Parse(viewModel.DefaultSupplierID) : null as Guid?;
            model.H1Tag = viewModel.H1Tag;
            model.AisleType = viewModel.AisleType;
    }
        public override void MapToViewModel(Aisle model, AisleViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.StartDate = model.StartDate;
            viewModel.EndDate = model.EndDate;
            viewModel.Name = model.Name;
            viewModel.MetaTitle = model.MetaTitle;
            viewModel.MetaKeyword = model.MetaKeyword;
            viewModel.MetaDescription = model.MetaDescription;
            viewModel.Label = model.Label;
            viewModel.LandingPageFilePath = model.LandingPageFilePath;
            viewModel.ProspectContentFilePath = model.ProspectContentFilePath;
            viewModel.CustomerContentFilePath = model.CustomerContentFilePath;
            viewModel.LandingPageContent = model.LandingPageContent;
            viewModel.ProspectContent = model.ProspectContent;
            viewModel.CustomerContent = model.CustomerContent;
            viewModel.SortPosition = model.SortPosition;
            viewModel.IsConstant = model.IsConstant;
            viewModel.IsMain = model.IsMain;
            viewModel.DefaultSupplierID = model.DefaultSupplierID.ToString();
            viewModel.InternalName = model.InternalName;
            viewModel.H1Tag = model.H1Tag;
            viewModel.AisleImageUrl = model.AisleImageUrl;
            viewModel.AisleType = model.AisleType;
        }
    }

    public class AisleGridMapper : ModelMapper<Aisle, AisleGridViewModel>
    {
        public override void MapToModel(AisleGridViewModel viewModel, Aisle model)
        {
            throw new NotImplementedException();
        }
        public override void MapToViewModel(Aisle model, AisleGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.StartDate = model.StartDate;
            viewModel.EndDate = model.EndDate;
            viewModel.Name = model.Name;
            viewModel.Label = model.Label;
            viewModel.LandingPageFilePath = model.LandingPageFilePath;
            viewModel.ProspectContentFilePath = model.ProspectContentFilePath;
            viewModel.CustomerContentFilePath = model.CustomerContentFilePath;
            viewModel.SortPosition = model.SortPosition;
            viewModel.IsConstant = model.IsConstant;
            viewModel.SupplierName = model.Supplier != null ? model.Supplier.Name : "";
            viewModel.IsMain = model.IsMain;
        }
    }

}
