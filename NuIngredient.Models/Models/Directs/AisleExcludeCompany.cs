﻿using System;

namespace NuIngredient.Models
{
    public class AisleExcludeCompany : IEntity
    {
        public AisleExcludeCompany()
        {
            ID = Guid.NewGuid();
        }

        public Guid ID { get; set; }
        public Guid AisleID { get; set; }
        public Guid CompanyID { get; set; }
        
        public virtual Aisle Aisle { get; set; }
        public virtual Company Company { get; set; }
    }

    public class AisleExcludeCompanyGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string AisleID { get; set; }
        public SelectItemViewModel Customer { get; set; }
    }
    
    public class AisleExcludeCompanyGridMapper : ModelMapper<AisleExcludeCompany, AisleExcludeCompanyGridViewModel>
    {
        public override void MapToViewModel(AisleExcludeCompany model, AisleExcludeCompanyGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Customer = new SelectItemViewModel(model.CompanyID.ToString(), model.Company?.Name);
            viewModel.AisleID = model.AisleID.ToString();
        }

        public override void MapToModel(AisleExcludeCompanyGridViewModel viewModel, AisleExcludeCompany model)
        {
            model.AisleID = Guid.Parse(viewModel.AisleID);
            model.CompanyID = Guid.Parse(viewModel.Customer.ID);
        }
    }
}