﻿namespace NuIngredient.Models
{
    public class AisleExcludeCompanyRepository : EntityRespository<AisleExcludeCompany>
    {
        public AisleExcludeCompanyRepository()
            : this(new SnapDbContext())
        {

        }

        public AisleExcludeCompanyRepository(SnapDbContext db)
            : base(db)
        {

        }

    }
}