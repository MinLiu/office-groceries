﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class AisleMoreInfoContentRepository : EntityRespository<AisleMoreInfoContent>
    {
        public AisleMoreInfoContentRepository()
            : this(new SnapDbContext())
        {

        }

        public AisleMoreInfoContentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
