﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class AisleRepository : EntityRespository<Aisle>
    {
        public AisleRepository()
            : this(new SnapDbContext())
        {

        }

        public AisleRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(IEnumerable<Aisle> entities)
        {
            var entitiesID = entities.Select(x => x.ID).ToList();

            var aisleMoreInfoContentRepo = new AisleMoreInfoContentRepository();
            aisleMoreInfoContentRepo.Delete(aisleMoreInfoContentRepo.Read().Where(x => entitiesID.Contains(x.AisleID)).AsNoTracking().ToList());

            var categoryRepo = new ProductCategoryRepository();
            categoryRepo.Delete(categoryRepo.Read().Where(x => entitiesID.Contains(x.AisleID.Value)).AsNoTracking().ToList());

            var dryGoodsAisleRepo = new DryGoodsAisleRepository();
            dryGoodsAisleRepo.Delete(dryGoodsAisleRepo.Read().Where(x => entitiesID.Contains(x.AisleID)).AsNoTracking().ToList());

            base.Delete(entities);

            foreach(var entity in entities)
            {
                entity.Deleted = true;
                Update(entities, new string[] { "Deleted" });
            }
        }

        public override void Delete(Aisle entity)
        {
            //var categoryRepo = new ProductCategoryRepository();
            //categoryRepo.Delete(categoryRepo.Read().Where(x => x.AisleID == entity.ID).AsNoTracking().ToList());

            //var dryGoodsAisleRepo = new DryGoodsAisleRepository();
            //dryGoodsAisleRepo.Delete(dryGoodsAisleRepo.Read().Where(x => x.AisleID == entity.ID).AsNoTracking().ToList());

            //base.Delete(entity);

            Delete(new List<Aisle>() { entity });
        }

    }

}
