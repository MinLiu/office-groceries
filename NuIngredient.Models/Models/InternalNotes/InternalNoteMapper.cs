﻿
namespace NuIngredient.Models
{
    public class InternalNoteMapper: ModelMapper<InternalNote, InternalNoteViewModel>
    {
        public override void MapToViewModel(InternalNote model, InternalNoteViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Title = model.Title;
            viewModel.Content = model.Content;
            viewModel.Creator = model.Creator;
            viewModel.CompletedUser = model.CompletedUser;
            viewModel.IsCompleted = model.IsCompleted;
            viewModel.Created = model.Created;
            viewModel.CompletedTime = model.CompletedTime;
        }

        public override void MapToModel(InternalNoteViewModel viewModel, InternalNote model)
        {
            model.Title = viewModel.Title;
            model.Content = viewModel.Content;
        }
    }
}
