﻿using System;

namespace NuIngredient.Models
{
    public class InternalNoteGridMapper : ModelMapper<InternalNote, InternalNoteGridViewModel>
    {
        public override void MapToViewModel(InternalNote model, InternalNoteGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Title = model.Title;
            viewModel.Creator = model.Creator;
            viewModel.Created = model.Created;
        }

        public override void MapToModel(InternalNoteGridViewModel viewModel, InternalNote model)
        {
            throw new NotSupportedException();
        }
    }
}
