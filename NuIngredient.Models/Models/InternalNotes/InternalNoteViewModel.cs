﻿using System;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class InternalNoteViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string Creator { get; set; }
        public string CompletedUser { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime Created { get; set; }
        public DateTime? CompletedTime { get; set; }
    }
}
