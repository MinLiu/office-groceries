﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class InternalNote : IEntity
    {
        public InternalNote()
        {
            ID = Guid.NewGuid();
        }

        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Creator { get; set; }
        public string CompletedUser { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime Created { get; set; }
        public DateTime? CompletedTime { get; set; }
    }
}
