﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class InternalNoteGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Creator { get; set; }
        public DateTime Created { get; set; }
    }
}
