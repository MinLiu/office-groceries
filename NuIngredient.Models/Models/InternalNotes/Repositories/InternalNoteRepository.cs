﻿
using System;

namespace NuIngredient.Models
{
    public class InternalNoteRepository : EntityRespository<InternalNote>
    {
        public InternalNoteRepository()
            : this(new SnapDbContext())
        {

        }

        public InternalNoteRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Create(InternalNote entity)
        {
            entity.Created = DateTime.Now;
            base.Create(entity);
        }
    }

}
