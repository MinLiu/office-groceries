﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MessageTargetRepository : EntityRespository<MessageTarget>
    {
        public MessageTargetRepository()
            : this(new SnapDbContext())
        {

        }

        public MessageTargetRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
