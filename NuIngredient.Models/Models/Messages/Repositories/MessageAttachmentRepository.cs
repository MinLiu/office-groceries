﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MessageAttachmentRepository : EntityRespository<MessageAttachment>
    {
        public MessageAttachmentRepository()
            : this(new SnapDbContext())
        {

        }

        public MessageAttachmentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
