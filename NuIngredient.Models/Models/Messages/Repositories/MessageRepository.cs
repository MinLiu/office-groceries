﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MessageRepository : EntityRespository<Message>
    {
        public MessageRepository()
            : this(new SnapDbContext())
        {

        }

        public MessageRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
