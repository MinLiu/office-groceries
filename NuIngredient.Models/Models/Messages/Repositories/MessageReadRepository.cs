﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MessageReadRepository : EntityRespository<MessageRead>
    {
        public MessageReadRepository()
            : this(new SnapDbContext())
        {

        }

        public MessageReadRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
