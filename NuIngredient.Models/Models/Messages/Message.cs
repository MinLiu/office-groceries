﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class Message : IEntity
    {
        public Message() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime CreatedDate { get; set; }
        
        public virtual ICollection<MessageTarget> MessageTargets { get; set; }
        public virtual ICollection<MessageRead> MessageRead { get; set; }
        public virtual ICollection<MessageAttachment> Attachments { get; set; }
    }

    public class MessageGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<Target> MessageTargets { get; set; }
    }

    public class MessageViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        [AllowHtml]
        public string Content { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        [Required]
        public DateTime ToDate { get; set; }
        public DateTime CreatedDate { get; set; }

        [Required(ErrorMessage = "The Targets field is required.")]
        public List<string> MessageTargets { get; set; }
    }

    public class MessageGridMapper : ModelMapper<Message, MessageGridViewModel>
    {
        public override void MapToModel(MessageGridViewModel viewModel, Message model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(Message model, MessageGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Title = model.Title;
            viewModel.Content = model.Content;
            viewModel.FromDate = model.FromDate;
            viewModel.ToDate = model.ToDate;
            viewModel.CreatedDate = model.CreatedDate;
            viewModel.MessageTargets = model.MessageTargets.Select(x => x.Target).ToList();
        }
    }

    public class MessageViewMapper : ModelMapper<Message, MessageViewModel>
    {
        public override void MapToModel(MessageViewModel viewModel, Message model)
        {
            model.Title = viewModel.Title;
            model.Content = viewModel.Content;
            model.FromDate = viewModel.FromDate;
            model.ToDate = viewModel.ToDate;
        }

        public override void MapToViewModel(Message model, MessageViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Title = model.Title;
            viewModel.Content = model.Content;
            viewModel.FromDate = model.FromDate;
            viewModel.ToDate = model.ToDate;
            viewModel.CreatedDate = model.CreatedDate;
            viewModel.MessageTargets = model.MessageTargets.Select(x => x.Target.ID.ToString()).ToList();
        }
    }

}
