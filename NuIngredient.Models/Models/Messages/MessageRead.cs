﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MessageRead
    {
        [Key]
        [Column(Order = 1)]
        public Guid MessageID { get; set; }

        [Key]
        [Column(Order = 2)]
        public Guid CompanyID { get; set; }

        public virtual Message Message { get; set; }

        public virtual Company Company { get; set; }
    }

}
