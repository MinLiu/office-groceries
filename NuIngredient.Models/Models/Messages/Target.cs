﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class Target
    {
        public Target() { Active = true; }
        
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Colour { get; set; }
        public bool Active { get; set; }
    }

    public static class TargetValues
    {
        public const int Milk = 1;
        public const int Fruits = 2;
        public const int DryGoods = 3;
    }

}
