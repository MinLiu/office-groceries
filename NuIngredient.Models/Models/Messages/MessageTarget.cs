﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MessageTarget
    {
        [Key]
        [Column(Order = 1)]
        public Guid MessageID { get; set; }

        [Key]
        [Column(Order = 2)]
        public int TargetID { get; set; }

        public virtual Message Message { get; set; }

        public virtual Target Target { get; set; }
    }

}
