﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class MessageAttachment : IEntity
    {
        public MessageAttachment() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid? MessageID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime Created { get; set; }

        public virtual Message Message { get; set; }
    }

    public class MessageAttachmentGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }

    public class MessageAttachmentGridMapper : ModelMapper<MessageAttachment, MessageAttachmentGridViewModel>
    {
        public override void MapToModel(MessageAttachmentGridViewModel viewModel, MessageAttachment model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(MessageAttachment model, MessageAttachmentGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.FilePath = model.FilePath;
            viewModel.FileName = model.FileName;
        }
    }
}
