﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitOneOffChangeTracker : IEntity, ICloneable
    {
        public FruitOneOffChangeTracker() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid FruitProductID { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public Guid OrderHeaderID { get; set; }

        [ForeignKey("FruitProductID")]
        public virtual FruitProduct Fruit { get; set; }
        public virtual FruitOrderHeader OrderHeader { get; set; }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

}
