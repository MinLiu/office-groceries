﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class FruitCategory : IEntity
    {
        public FruitCategory() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }

        //Foreign References
    }




    public class FruitCategoryViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        [AllowHtml]
        public string Description { get; set; }
    }



    public class FruitCategoryMapper : ModelMapper<FruitCategory, FruitCategoryViewModel>
    {
        public override void MapToViewModel(FruitCategory model, FruitCategoryViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
        }

        public override void MapToModel(FruitCategoryViewModel viewModel, FruitCategory model)
        {
            model.Name = viewModel.Name;
        }

    }
}
