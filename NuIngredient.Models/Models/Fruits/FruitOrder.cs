﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitOrder : IEntity, ICloneable
    {
        public FruitOrder() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public Guid FruitProductID { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int Sunday { get; set; }
        public decimal VATRate { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        public Guid OrderHeaderID { get; set; }
        public int SortPos { get; set; }
        [NotMapped]
        public int WeeklyVolume { get { return Monday + Tuesday + Wednesday + Thursday + Friday + Saturday + Sunday; } private set { } }

        [ForeignKey("FruitProductID")]
        public virtual FruitProduct Fruit { get; set; }
        public virtual Company Company { get; set; }
        public virtual FruitOrderHeader OrderHeader { get; set; }
        public virtual ICollection<FruitCreditItem> Credits { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public int GetAmountByDayOfWeek(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return Monday;
                case DayOfWeek.Tuesday:
                    return Tuesday;
                case DayOfWeek.Wednesday:
                    return Wednesday;
                case DayOfWeek.Thursday:
                    return Thursday;
                case DayOfWeek.Friday:
                    return Friday;
                case DayOfWeek.Saturday:
                    return Saturday;
                case DayOfWeek.Sunday:
                    return Sunday;
                default:
                    return 0;
            }
        }
    }

    #region FruitViewModel

    public class FruitOrderViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ImageUrl { get; set; }
        public FruitProductType ProductType { get; set; }
        public string ProductName { get; set; }
        public string FruitID { get; set; }
        public string Unit { get; set; }
        public decimal VATRate { get; set; }
        public decimal Price { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int Sunday { get; set; }
        public int WeeklyVolume { get; set; }
        public decimal WeeklyTotal { get; set; }
        public DateTime? FromDate { get; set; }
        public OrderMode OrderMode { get; set; }
    }

    public class FruitOrderMapper : ModelMapper<FruitOrder, FruitOrderViewModel>
    {

        public override void MapToViewModel(FruitOrder model, FruitOrderViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ImageUrl = model.Fruit != null ? model.Fruit.ImageURL : "";
            viewModel.ProductType = model.Fruit?.FruitProductType ?? FruitProductType.Fruit;
            viewModel.ProductName = model.ProductName;
            viewModel.FruitID = model.FruitProductID.ToString();
            viewModel.Monday = model.Monday;
            viewModel.Tuesday = model.Tuesday;
            viewModel.Wednesday = model.Wednesday;
            viewModel.Thursday = model.Thursday;
            viewModel.Friday = model.Friday;
            viewModel.Saturday = model.Saturday;
            viewModel.Sunday = model.Sunday;
            viewModel.Unit = model.Unit;
            viewModel.Price = model.Price;
            viewModel.WeeklyVolume = model.Monday + model.Tuesday + model.Wednesday + model.Thursday + model.Friday + model.Saturday + model.Sunday;
            viewModel.WeeklyTotal = model.TotalNet;
            viewModel.VATRate = model.VATRate;
        }

        public override void MapToModel(FruitOrderViewModel viewModel, FruitOrder model)
        {
            model.ProductName = viewModel.ProductName;
            model.Unit = viewModel.Unit;
            model.Monday = viewModel.Monday;
            model.Tuesday = viewModel.Tuesday;
            model.Wednesday = viewModel.Wednesday;
            model.Thursday = viewModel.Thursday;
            model.Friday = viewModel.Friday;
            model.Saturday = viewModel.Saturday;
            model.Sunday = viewModel.Sunday;
            model.FruitProductID = Guid.Parse(viewModel.FruitID);
            model.Price = viewModel.Price;
            model.TotalNet = model.Price * (model.Monday + model.Tuesday + model.Wednesday + model.Thursday + model.Friday + model.Saturday + model.Sunday);
            model.VATRate = viewModel.VATRate;
            model.TotalVAT = model.TotalNet * model.VATRate;
        }

    }

    #endregion
}
