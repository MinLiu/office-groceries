﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitOrderHeaderRepository : EntityRespository<FruitOrderHeader>
    {
        public FruitOrderHeaderRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitOrderHeaderRepository(SnapDbContext db)
            : base(db)
        {

        }

        public new IQueryable<FruitOrderHeader> Read()
        {
            return Read(false);
        }

        public IQueryable<FruitOrderHeader> Read(bool includeDepreciated)
        {
            return base.Read().Where(x => x.Depreciated == includeDepreciated).OrderByDescending(x => x.LatestUpdated);
        }

        public override void Delete(FruitOrderHeader entity)
        {
            _db.Set<FruitOrder>().RemoveRange(_db.Set<FruitOrder>().Where(x => x.OrderHeaderID == entity.ID));
            _db.Set<FruitOneOffChangeTracker>().RemoveRange(_db.Set<FruitOneOffChangeTracker>().Where(x => x.OrderHeaderID == entity.ID));
            _db.Set<FruitOrderHeader>().Remove(entity);
            _db.SaveChanges();
        }

        public override void Delete(IEnumerable<FruitOrderHeader> entities)
        {
            foreach (var entity in entities.ToList())
            {
                Delete(entity);
            }
        }
    }

}
