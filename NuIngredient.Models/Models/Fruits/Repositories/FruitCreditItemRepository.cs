﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitCreditItemRepository : EntityRespository<FruitCreditItem>
    {
        public FruitCreditItemRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitCreditItemRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
