﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitOneOffChangeTrackerRepository : EntityRespository<FruitOneOffChangeTracker>
    {
        public FruitOneOffChangeTrackerRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitOneOffChangeTrackerRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
