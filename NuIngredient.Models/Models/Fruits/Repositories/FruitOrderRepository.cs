﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitOrderRepository : EntityRespository<FruitOrder>
    {
        public FruitOrderRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitOrderRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
