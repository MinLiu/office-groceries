﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitCreditHeaderRepository : EntityRespository<FruitCreditHeader>
    {
        public FruitCreditHeaderRepository()
            : this(new SnapDbContext())
        {

        }

        public FruitCreditHeaderRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
