﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitOrderHeader : IEntity
    {
        public FruitOrderHeader() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid? CompanyID { get; set; }
        public Guid? SupplierID { get; set; } // Fruit Supplier
        public string AccountNo { get; set; } // Fruit Supplier
        public Guid? SnackSupplierID { get; set; } // Snack Supplier
        public string SnackAccountNo { get; set; } // Snack Supplier
        public string FileURL { get; set; }
        [MaxLength(50)]
        [Index("IX_ProspectToken", IsUnique = false)]
        public string ProspectToken { get; set; }
        public string OrderNumber { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        [NotMapped]
        public decimal TotalGross { get { return TotalNet + TotalVAT; } private set { } }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public OrderMode OrderMode { get; set; }
        public DateTime? LatestUpdated { get; set; }
        public bool Depreciated { get; set; }
        public string Note { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<FruitOrder> Items { get; set; }
        [ForeignKey("SupplierID")]
        public virtual Supplier Supplier { get; set; }
        [ForeignKey("SnackSupplierID")]
        public virtual Supplier SnackSupplier { get; set; }
        public virtual ICollection<FruitOneOffChangeTracker> OneOffChangeTrackers { get; set; }

        public void CalculateTotal()
        {
            TotalNet = Items?.Count() > 0 ? Items.ToList().Sum(x => x.TotalNet) : 0;
        }
    }

    public class FruitOrderHeaderViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string FileURL { get; set; }
        public string OrderNumber { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string SnackSupplierID { get; set; }
        public string SnackSupplierName { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        public decimal TotalGross { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public OrderMode OrderMode { get; set; }
        public DateTime? LatestUpdated { get; set; }
        public bool Depreciated { get; set; }
        public string Note { get; set; }
        public List<FruitOrderViewModel> Items { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryInstruction { get; set; }
    }

    public class FruitOrderHeaderMapper : ModelMapper<FruitOrderHeader, FruitOrderHeaderViewModel>
    {
        public override void MapToModel(FruitOrderHeaderViewModel viewModel, FruitOrderHeader model)
        {
            model.Note = viewModel.Note;
        }
        public override void MapToViewModel(FruitOrderHeader model, FruitOrderHeaderViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CompanyName = model.Company.Name;
            viewModel.OrderNumber = model.OrderNumber;
            viewModel.OrderMode = model.OrderMode;
            viewModel.SupplierID = model.SupplierID?.ToString();
            viewModel.SupplierName = model.Supplier?.Name ?? "";
            viewModel.SnackSupplierID = model.SnackSupplierID?.ToString();
            viewModel.SnackSupplierName = model.SnackSupplier?.Name ?? "";
            viewModel.FromDate = model.FromDate;
            viewModel.ToDate = model.ToDate;
            viewModel.TotalNet = model.TotalNet;
            viewModel.TotalVAT = model.TotalVAT;
            viewModel.TotalGross = model.TotalGross;
            viewModel.FileURL = model.FileURL;
            viewModel.LatestUpdated = model.OrderMode == OrderMode.History ? null : model.LatestUpdated;
            viewModel.Depreciated = model.Depreciated;
            viewModel.Note = model.Note;
            viewModel.Items = model.Items.ToList().Select(x => new FruitOrderMapper().MapToViewModel(x)).ToList();
            viewModel.DeliveryAddress = string.Join(", ", model.Company.Address1, model.Company.Postcode);
            viewModel.DeliveryInstruction = model.Company.DeliveryInstruction;
        }
    }
}
