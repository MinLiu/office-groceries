﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitCreditItem : IEntity
    {
        public FruitCreditItem() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid FruitCreditHeaderID { get; set; }
        public Guid FruitOrderItemID { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }

        public virtual FruitCreditHeader FruitCreditHeader { get; set; }
        public virtual FruitOrder FruitOrderItem { get; set; }
    }

    public class FruitCreditItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public string CreateBy { get; set; }
        public DateTime Created { get; set; }
        public string Narrative { get; set; }
    }

    public class FruitCreditItemMapper : ModelMapper<FruitCreditItem, FruitCreditItemViewModel>
    {
        public override void MapToModel(FruitCreditItemViewModel viewModel, FruitCreditItem model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(FruitCreditItem model, FruitCreditItemViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CreateBy = model.FruitCreditHeader.CreateBy.Email;
            viewModel.Created = model.FruitCreditHeader.Created;
            viewModel.Narrative = model.FruitCreditHeader.Narrative;
            viewModel.ImageURL = model.FruitOrderItem.Fruit != null ? model.FruitOrderItem.Fruit.ImageURL : "";
            viewModel.ProductName = model.FruitOrderItem.ProductName;
            viewModel.Unit = model.FruitOrderItem.Unit;
            viewModel.Monday = model.Monday;
            viewModel.Tuesday = model.Tuesday;
            viewModel.Wednesday = model.Wednesday;
            viewModel.Thursday = model.Thursday;
            viewModel.Friday = model.Friday;
            viewModel.Saturday = model.Saturday;
        }
    }
}
