﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{
    public class Enquiry : IEntity
    {
        public Enquiry() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public string BusinessName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool InterestOfMilk { get; set; }
        public bool InterestOfFruits { get; set; }
        public bool InterestOfDryGoods { get; set; }
        public string Note { get; set; }
        public bool NoGo { get; set; }
        public DateTime Timestamp { get; set; }
        public string Postcode { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DateDeleted { get; set; }
    }

    public class EnquiryGridViewModel: IEntityViewModel
    {
        public string ID { get; set; }
        public string BusinessName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool InterestOfMilk { get; set; }
        public bool InterestOfFruits { get; set; }
        public bool InterestOfDryGoods { get; set; }
        public string Note { get; set; }
        public DateTime Timestamp { get; set; }
        public string Postcode { get; set; }
        public DateTime? DateDeleted { get; set; }
    }

    public class EnquiryGridMapper : ModelMapper<Enquiry, EnquiryGridViewModel>
    {
        public override void MapToModel(EnquiryGridViewModel viewModel, Enquiry model)
        {
            model.BusinessName = viewModel.BusinessName;
            model.Note = viewModel.Note;
            model.Email = viewModel.Email;
            model.Postcode = viewModel.Postcode;
        }

        public override void MapToViewModel(Enquiry model, EnquiryGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.BusinessName = model.BusinessName;
            viewModel.FirstName = model.FirstName;
            viewModel.LastName = model.LastName;
            viewModel.Email = model.Email;
            viewModel.PhoneNumber = model.PhoneNumber;
            viewModel.InterestOfMilk = model.InterestOfMilk;
            viewModel.InterestOfFruits =model.InterestOfFruits;
            viewModel.InterestOfDryGoods = model.InterestOfDryGoods;
            viewModel.Timestamp = model.Timestamp;
            viewModel.Note = model.Note;
            viewModel.Postcode = model.Postcode;
            viewModel.DateDeleted = model.DateDeleted;
        }
    }
}
