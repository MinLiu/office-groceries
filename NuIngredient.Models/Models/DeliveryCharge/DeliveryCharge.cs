﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class DeliveryCharge : IEntity
    {
        public DeliveryCharge() { ID = Guid.NewGuid(); }
        public Guid ID { get; set; }
        public Guid? SupplierID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Charge { get; set; }
        public decimal VAT { get; set; }
        public bool IsActive { get; set; }
        public DeliveryChargeType Type { get; set; }
        public DeliveryChargeCategory Category { get; set; }
        
        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<DeliveryChargePostcode> Postcodes { get; set; }
    }
    
    public class DeliveryChargeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Charge { get; set; }
        public decimal VAT { get; set; }
        public DeliveryChargeType Type { get; set; }
        public DeliveryChargeCategory Category { get; set; }
        public bool IsActive { get; set; }
    }
    
    public class DeliveryChargeMapper : ModelMapper<DeliveryCharge, DeliveryChargeViewModel>
    {
        public override void MapToViewModel(DeliveryCharge model, DeliveryChargeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierName = model.Supplier?.Name;
            viewModel.SupplierID = model.SupplierID?.ToString();
            viewModel.Name = model.Name;
            viewModel.Code = model.Code;
            viewModel.Charge = model.Charge;
            viewModel.VAT = model.VAT;
            viewModel.Type = model.Type;
            viewModel.Category = model.Category;
            viewModel.IsActive = model.IsActive;
        }

        public override void MapToModel(DeliveryChargeViewModel viewModel, DeliveryCharge model)
        {
            model.Name = viewModel.Name;
            model.Code = viewModel.Code;
            model.Charge = viewModel.Charge;
            model.VAT = viewModel.VAT;
            model.IsActive = viewModel.IsActive;
        }
    }

    public class DeliveryChargeGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Charge { get; set; }
        public decimal VAT { get; set; }
        public DeliveryChargeType Type { get; set; }
        public DeliveryChargeCategory Category { get; set; }
        public bool IsActive { get; set; }
    }

    public class DeliveryChargeGridMapper : ModelMapper<DeliveryCharge, DeliveryChargeGridViewModel>
    {
        public override void MapToViewModel(DeliveryCharge model, DeliveryChargeGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierID = model.SupplierID?.ToString();
            viewModel.SupplierName = model.Supplier?.Name;
            viewModel.Name = model.Name;
            viewModel.Code = model.Code;
            viewModel.Charge = model.Charge;
            viewModel.VAT = model.VAT;
            viewModel.Type = model.Type;
            viewModel.Category = model.Category;
            viewModel.IsActive = model.IsActive;
        }

        public override void MapToModel(DeliveryChargeGridViewModel viewModel, DeliveryCharge model)
        {
            throw new NotImplementedException();
        }
    }
}