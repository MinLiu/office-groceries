﻿using System;

namespace NuIngredient.Models
{
    public class DeliveryChargePostcode : IEntity
    {
        public DeliveryChargePostcode() { ID = Guid.NewGuid(); }
        public Guid ID { get; set; }
        public Guid DeliveryChargeID { get; set; }
        public string Postcode { get; set; }
        
        public DeliveryCharge DeliveryCharge { get; set; }
    }

    public class DeliveryChargePostcodeGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string DeliveryChargeID { get; set; }
        public string Postcode { get; set; }
    }

    public class DeliveryChargePostcodeGridMapper : ModelMapper<DeliveryChargePostcode, DeliveryChargePostcodeGridViewModel>
    {
        public override void MapToViewModel(DeliveryChargePostcode model, DeliveryChargePostcodeGridViewModel viewModel)
        {
            viewModel.DeliveryChargeID = model.DeliveryChargeID.ToString();
            viewModel.Postcode = model.Postcode;
        }

        public override void MapToModel(DeliveryChargePostcodeGridViewModel viewModel, DeliveryChargePostcode model)
        {
            model.DeliveryChargeID = Guid.Parse(viewModel.DeliveryChargeID);
            model.Postcode = viewModel.Postcode;
        }
    }
}