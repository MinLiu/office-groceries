﻿namespace NuIngredient.Models
{
    public class DeliveryChargePostcodeRepository : EntityRespository<DeliveryChargePostcode>
    {
        public DeliveryChargePostcodeRepository()
            : this(new SnapDbContext())
        {

        }

        public DeliveryChargePostcodeRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
