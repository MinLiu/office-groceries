﻿using System.Linq;
using System.Linq.Dynamic;

namespace NuIngredient.Models
{
    public class DeliveryChargeRepository : EntityRespository<DeliveryCharge>
    {
        public DeliveryChargeRepository()
            : this(new SnapDbContext())
        {

        }

        public DeliveryChargeRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(DeliveryCharge entity)
        {
            // Delete postcodes
            var deliveryChargePostCodeRepo = new DeliveryChargePostcodeRepository();
            deliveryChargePostCodeRepo.Delete(deliveryChargePostCodeRepo.Read()
                .Where(x => x.DeliveryChargeID == entity.ID));
            
            base.Delete(entity);
        }
    }

}
