﻿using System;

namespace NuIngredient.Models
{
    public class ProformaApprover : IEntity
    {
        public Guid ID { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public string Email { get; set; }
    }
    
    public class ProformaApproverGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
    
    public class ProformaApproverGridMapper : ModelMapper<ProformaApprover, ProformaApproverGridViewModel>
    {
        public override void MapToModel(ProformaApproverGridViewModel viewModel, ProformaApprover model)
        {
            model.Name = viewModel.Name;
            model.Email = viewModel.Email;
        }

        public override void MapToViewModel(ProformaApprover model, ProformaApproverGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.Email = model.Email;
        }
    }
    
    public class ProformaApproverViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
    
    public class ProformaApproverMapper : ModelMapper<ProformaApprover, ProformaApproverViewModel>
    {
        public override void MapToModel(ProformaApproverViewModel viewModel, ProformaApprover model)
        {
            model.Name = viewModel.Name;
            model.Email = viewModel.Email;
        }

        public override void MapToViewModel(ProformaApprover model, ProformaApproverViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.Email = model.Email;
        }
    }
}