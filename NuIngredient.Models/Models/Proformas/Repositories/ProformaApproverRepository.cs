﻿namespace NuIngredient.Models
{
    public class ProformaApproverRepository : EntityRespository<ProformaApprover>
    {
        public ProformaApproverRepository()
            : this(new SnapDbContext())
        {

        }

        public ProformaApproverRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
