﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;

namespace NuIngredient.Models
{
    public class Testimonial : IEntity
    {
        public Testimonial() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public string Quote { get; set; }
        public string FromName { get; set; }
        public string FromTitle { get; set; }
        public string FromCompany { get; set; }
        public int SortPos { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
    }

    public class TestimonialViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Quote { get; set; }
        public string From { get; set; }
    }

    public class TestimonialGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Quote { get; set; }
        public string FromName { get; set; }
        public string FromTitle { get; set; }
        public string FromCompany { get; set; }
        public int SortPos { get; set; }
        public bool Active { get; set; }
    }

    public class TestimonialMapper: ModelMapper<Testimonial, TestimonialViewModel>
    {
        public override void MapToModel(TestimonialViewModel viewModel, Testimonial model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(Testimonial model, TestimonialViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Quote = model.Quote ?? "";
            viewModel.From = string.Join(" - ", new string[] { model.FromName, model.FromTitle, model.FromCompany }.Where(i => !string.IsNullOrEmpty(i)));
        }
    }

    public class TestimonialGridMapper : ModelMapper<Testimonial, TestimonialGridViewModel>
    {
        public override void MapToModel(TestimonialGridViewModel viewModel, Testimonial model)
        {
            model.Quote = viewModel.Quote;
            model.FromName = viewModel.FromName;
            model.FromTitle = viewModel.FromTitle;
            model.FromCompany = viewModel.FromCompany;
            model.Active = viewModel.Active;
        }

        public override void MapToViewModel(Testimonial model, TestimonialGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Quote = model.Quote;
            viewModel.FromName = model.FromName;
            viewModel.FromTitle = model.FromTitle;
            viewModel.FromCompany = model.FromCompany;
            viewModel.Active = model.Active;
        }
    }

}
