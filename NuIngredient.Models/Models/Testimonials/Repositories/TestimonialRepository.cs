﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class TestimonialRepository : EntityRespository<Testimonial>
    {
        public TestimonialRepository()
            : this(new SnapDbContext())
        {

        }

        public TestimonialRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
