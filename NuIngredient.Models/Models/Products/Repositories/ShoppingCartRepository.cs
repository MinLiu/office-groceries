﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ShoppingCartItemRepository : EntityRespository<ShoppingCartItem>
    {
        public ShoppingCartItemRepository()
            : this(new SnapDbContext())
        {

        }

        public ShoppingCartItemRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
