﻿namespace NuIngredient.Models
{
    public class DryGoodsExcludeCompanyRepository : EntityRespository<DryGoodsExcludeCompany>
    {
        public DryGoodsExcludeCompanyRepository()
            : this(new SnapDbContext())
        {

        }

        public DryGoodsExcludeCompanyRepository(SnapDbContext db)
            : base(db)
        {

        }

    }
}