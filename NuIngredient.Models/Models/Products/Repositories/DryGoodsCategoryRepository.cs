﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class DryGoodsCategoryRepository : EntityRespository<DryGoodsCategory>
    {
        public DryGoodsCategoryRepository()
            : this(new SnapDbContext())
        {

        }

        public DryGoodsCategoryRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
