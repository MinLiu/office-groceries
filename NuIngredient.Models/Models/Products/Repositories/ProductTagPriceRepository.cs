﻿namespace NuIngredient.Models
{
    public class ProductTagPriceRepository : EntityRespository<ProductTagPrice>
    {
        public ProductTagPriceRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductTagPriceRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
