﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class DryGoodsCreditHeaderRepository : EntityRespository<DryGoodsCreditHeader>
    {
        public DryGoodsCreditHeaderRepository()
            : this(new SnapDbContext())
        {

        }

        public DryGoodsCreditHeaderRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
