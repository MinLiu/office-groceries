﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class DryGoodsAisleRepository : EntityRespository<DryGoodsAisle>
    {
        public DryGoodsAisleRepository()
            : this(new SnapDbContext())
        {

        }

        public DryGoodsAisleRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
