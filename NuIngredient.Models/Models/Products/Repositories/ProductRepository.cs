﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ProductRepository : EntityRespository<Product>
    {
        public ProductRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(IEnumerable<Product> entities)
        {
            var IDs = entities.Select(x => x.ID).ToList();

            var shoppingcartItemRepo = new ShoppingCartItemRepository();
            shoppingcartItemRepo.Delete(shoppingcartItemRepo.Read().Where(x => IDs.Contains(x.ProductID)));

            foreach (var entity in entities)
            {
                entity.Deleted = true;
            }
            _db.SaveChanges();
        }

        public override void Delete(Product entity)
        {
            //_db.Set<DryGoodsCategory>().RemoveRange(_db.Set<DryGoodsCategory>().Where(x => x.ProductID == entity.ID));
            //_db.Set<DryGoodsAisle>().RemoveRange(_db.Set<DryGoodsAisle>().Where(x => x.ProductID == entity.ID));
            //_db.Set<ProductExclusivePrice>().RemoveRange(_db.Set<ProductExclusivePrice>().Where(x => x.ProductID == entity.ID));
            //_db.Set<ShoppingCartItem>().RemoveRange(_db.Set<ShoppingCartItem>().Where(x => x.ProductID == entity.ID));
            //_db.Set<Product>().RemoveRange(_db.Set<Product>().Where(a => a.ID == entity.ID));
            //entity.Deleted = true;
            //_db.SaveChanges();

            Delete(new List<Product>() { entity });
        }

    }

}
