﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace NuIngredient.Models
{
    public class ProductCategoryRepository : EntityRespository<ProductCategory>
    {
        public ProductCategoryRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductCategoryRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(ProductCategory entity)
        {
            // Delete children categories
            var childrenEntity = _db.Set<ProductCategory>().Where(x => x.ParentID == entity.ID).ToList();
            foreach (var item in childrenEntity)
            {
                Delete(item);
            }

            // Delete relationships
            var associatedProducts = _db.Set<DryGoodsCategory>().Where(x => x.CategoryID == entity.ID);
            _db.Set<DryGoodsCategory>().RemoveRange(associatedProducts);

            _db.Entry(entity).State = EntityState.Deleted;
            _db.SaveChanges();
        }

        public override void Delete(IEnumerable<ProductCategory> entities)
        {
            foreach(var entity in entities)
            {
                Delete(entity);
            }
        }

    }

}
