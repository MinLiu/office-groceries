﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class OrderEventDetailRepository : EntityRespository<OrderEventDetail>
    {
        public OrderEventDetailRepository()
            : this(new SnapDbContext())
        {

        }

        public OrderEventDetailRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
