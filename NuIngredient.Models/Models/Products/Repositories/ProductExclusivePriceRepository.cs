﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ProductExclusivePriceRepository : EntityRespository<ProductExclusivePrice>
    {
        public ProductExclusivePriceRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductExclusivePriceRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
