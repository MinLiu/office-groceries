﻿using System;
using System.Linq;
using System.Data.Entity;

namespace NuIngredient.Models
{
    public class OneOffOrderRepository : EntityRespository<OneOffOrder>
    {
        public OneOffOrderRepository()
            : this(new SnapDbContext())
        {

        }

        public OneOffOrderRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Create(OneOffOrder entity)
        {  
            var isUnique = false;

            using (var transaction = _db.Database.BeginTransaction())
            {
                var setting = _db.Set<Setting>().First();
                
                while(!isUnique)
                {
                    setting.DryGoodOrderNumber += 1;
                    _db.Entry(setting).State = EntityState.Modified;

                    entity.OrderNumber = "DG" + setting.DryGoodOrderNumber.ToString("00000");

                    try
                    {
                        base.Create(entity);
                        transaction.Commit();
                        isUnique = true;
                    }
                    catch(Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }
        }  
    }
}