﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ProductCategory : IEntity
    {
        public ProductCategory() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid? AisleID { get; set; }
        public Guid? ParentID { get; set; }
        [Required]
        public string Name { get; set; }
        public int SortPos { get; set; }
        public string InternalName { get; set; }

        public void GetChildrenCategory(List<ProductCategory> list)
        {
            list.Add(this);
            foreach (var child in Children)
            {
                child.GetChildrenCategory(list);
            }
        }

        //Foreign References
        public virtual ProductCategory Parent { get; set; }
        public virtual ICollection<ProductCategory> Children { get; set; }
        public virtual Aisle Aisle { get; set; }
    }




    public class ProductCategoryViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ParentID { get; set; }
        public string Name { get; set; }
        public int SortPos { get; set; }
        public string AisleID { get; set; }
    }



    public class ProductCategoryMapper : ModelMapper<ProductCategory, ProductCategoryViewModel>
    {
        public override void MapToViewModel(ProductCategory model, ProductCategoryViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.ParentID = model.ParentID.HasValue? model.ParentID.ToString() : "";
            viewModel.AisleID = model.AisleID.ToString();
            viewModel.SortPos = model.SortPos;
        }

        public override void MapToModel(ProductCategoryViewModel viewModel, ProductCategory model)
        {
            model.Name = viewModel.Name;
            model.ParentID = string.IsNullOrWhiteSpace(viewModel.ParentID) ? (Guid?)null : Guid.Parse(viewModel.ParentID);
            model.AisleID = Guid.Parse(viewModel.AisleID);
            model.SortPos = viewModel.SortPos;
        }

    }
}
