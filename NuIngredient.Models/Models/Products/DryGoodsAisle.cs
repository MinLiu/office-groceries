﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class DryGoodsAisle : IEntity
    {
        public DryGoodsAisle() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid AisleID { get; set; }
        public Guid ProductID { get; set; }

        // Navigations
        public virtual Aisle Aisle { get; set; }
        public virtual Product Product { get; set; }
    }

}
