﻿
namespace NuIngredient.Models
{
    public enum OneOffOrderStatus
    {
        Draft = 0,
        Accepted = 1,
        Delivered = 2,
        Invoiced = 3,
        CreditOrder = 4,
        Canceled = 5,
        Proforma = 6,
    }

}
