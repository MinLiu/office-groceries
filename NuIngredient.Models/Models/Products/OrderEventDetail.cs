﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class OrderEventDetail : IEntity
    {
        public OrderEventDetail() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid OneOffOrderID { get; set; }
        public Guid SupplierID { get; set; }
        public string FunctionName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string RoomName { get; set; }
        public string NumberOfGuests { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime StartTime { get; set; }
        public string DeliveryDetails { get; set; }
        public string SpecialRequirements { get; set; }

        public virtual OneOffOrder OneOffOrder { get; set; }
        public virtual Supplier Supplier { get; set; }
    }

    public class OrderEventDetailViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string EventID { get; set; }
        public string OneOffOrderID { get; set; }
        public string EventSupplierID { get; set; }
        public string FunctionName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string RoomName { get; set; }
        public string NumberOfGuests { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime StartTime { get; set; }
        public string DeliveryDetails { get; set; }
        public string SpecialRequirements { get; set; }

    }

    public class OrderEventDetailMapper : ModelMapper<OrderEventDetail, OrderEventDetailViewModel>
    {
        public override void MapToModel(OrderEventDetailViewModel viewModel, OrderEventDetail model)
        {
            model.SupplierID = Guid.Parse(viewModel.EventSupplierID);
            model.OneOffOrderID = Guid.Parse(viewModel.OneOffOrderID);
            model.FunctionName = viewModel.FunctionName;
            model.AddressLine1 = viewModel.AddressLine1;
            model.AddressLine2 = viewModel.AddressLine2;
            model.RoomName = viewModel.RoomName;
            model.Postcode = viewModel.Postcode;
            model.Town = viewModel.Town;
            model.NumberOfGuests = viewModel.NumberOfGuests;
            model.EventDate = viewModel.EventDate;
            model.StartTime = viewModel.StartTime;
            model.DeliveryDetails = viewModel.DeliveryDetails;
            model.SpecialRequirements = viewModel.SpecialRequirements;
        }
        public override void MapToViewModel(OrderEventDetail model, OrderEventDetailViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.OneOffOrderID = model.OneOffOrderID.ToString();
            viewModel.EventID = model.ID.ToString();
            viewModel.EventSupplierID = model.SupplierID.ToString();
            viewModel.FunctionName = model.FunctionName;
            viewModel.AddressLine1 = model.AddressLine1;
            viewModel.AddressLine2 = model.AddressLine2;
            viewModel.RoomName = model.RoomName;
            viewModel.Postcode = model.Postcode;
            viewModel.Town = model.Town;
            viewModel.NumberOfGuests = model.NumberOfGuests;
            viewModel.EventDate = model.EventDate;
            viewModel.StartTime = model.StartTime;
            viewModel.DeliveryDetails = model.DeliveryDetails;
            viewModel.SpecialRequirements = model.SpecialRequirements;
        }
    }

}
