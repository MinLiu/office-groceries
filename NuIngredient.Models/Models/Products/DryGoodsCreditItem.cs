﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class DryGoodsCreditItem : IEntity
    {
        public DryGoodsCreditItem() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid DryGoodsCreditHeaderID { get; set; }
        public Guid DryGoodsOrderItemID { get; set; }
        public int Qty { get; set; }

        public virtual DryGoodsCreditHeader DryGoodsCreditHeader { get; set; }
        public virtual OneOffOrderItem DryGoodsOrderItem { get; set; }
    }

    public class DryGoodsCreditItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductName { get; set; }
        public string ImageURL { get; set; }
        public int Qty { get; set; }
        public string CreateBy { get; set; }
        public DateTime Created { get; set; }
        public string Narrative { get; set; }
    }

    public class DryGoodsCreditItemMapper : ModelMapper<DryGoodsCreditItem, DryGoodsCreditItemViewModel>
    {
        public override void MapToModel(DryGoodsCreditItemViewModel viewModel, DryGoodsCreditItem model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(DryGoodsCreditItem model, DryGoodsCreditItemViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CreateBy = model.DryGoodsCreditHeader.CreateBy.Email;
            viewModel.Created = model.DryGoodsCreditHeader.Created;
            viewModel.Narrative = model.DryGoodsCreditHeader.Narrative;
            viewModel.ImageURL = model.DryGoodsOrderItem.Product != null ? model.DryGoodsOrderItem.Product.ImageURL : "";
            viewModel.ProductName = model.DryGoodsOrderItem.ProductName;
            viewModel.Qty = model.Qty;
        }
    }
}
