﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class OneOffOrder : IEntity
    {
        public OneOffOrder() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? SupplierID { get; set; }
        public string CompanyName { get; set; }
        [MaxLength(50)]
        [Index("OrderNumberAndSequence", 1, IsUnique = true)]
        public string OrderNumber { get; set; }
        // This is used to differentiate between orders with the same order number
        [Index("OrderNumberAndSequence", 2, IsUnique = true)]
        public int OrderNumberSequence { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        public decimal TotalGross { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryCounty { get; set; }
        public string DeliveryPostCode { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCounty { get; set; }
        public string BillingPostCode { get; set; }
        public string OrderNotes { get; set; }
        public string PONumber { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? OriginalCreated { get; set; } // Used to store the original created date when a proforma order is approved and the created date is updated

        public OneOffOrderStatus Status { get; set; }

        // Navigations
        public virtual Company Company { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<OneOffOrderItem> OneOffOrderItems { get; set; }
        public virtual ICollection<OrderEventDetail> OrderEventDetails { get; set; }

        public string GetChecksum()
        {
            return LastUpdated.ToString("yyMMddHHmmss");
        }

        public void CalculateTotal()
        {
            TotalNet = OneOffOrderItems.Any() ? OneOffOrderItems.ToList().Sum(x => x.Total) : 0;
            TotalVAT = OneOffOrderItems.Any() ? OneOffOrderItems.ToList().Sum(x => x.Total * x.ProductVAT) : 0;
            TotalGross = OneOffOrderItems.Any() ? OneOffOrderItems.ToList().Sum(x => x.Total + x.Total * x.ProductVAT) : 0;
        }
    }

    public class OneOffOrderViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        [Display(Name = "Company")]
        public string CompanyName { get; set; }
        [Display(Name = "Order Number")]
        public string OrderNumber { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        public decimal TotalGross { get; set; }
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        [Display(Name = "Delivery Address")]
        public string DeliveryAddress { get; set; }
        [Display(Name = "Delivery County")]
        public string DeliveryCounty { get; set; }
        [Display(Name = "Delivery Postcode")]
        public string DeliveryPostCode { get; set; }
        [Display(Name = "Billing Address")]
        public string BillingAddress { get; set; }
        [Display(Name = "Billing County")]
        public string BillingCounty { get; set; }
        [Display(Name = "Billing Postcode")]
        public string BillingPostCode { get; set; }
        [Display(Name = "Order Notes")]
        public string OrderNotes { get; set; }
        [Display(Name = "PO Number")]
        public string PONumber { get; set; }
        [Display(Name = "Date")]
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public OneOffOrderStatus Status { get; set; }
        public string SupplierID { get; set; }
        public List<OneOffOrderItem> OneOffOrderItems { get; set; }

    }

    public class OneOffOrderMapper : ModelMapper<OneOffOrder, OneOffOrderViewModel>
    {
        public override void MapToModel(OneOffOrderViewModel viewModel, OneOffOrder model)
        {
            throw new NotImplementedException();
        }
        public override void MapToViewModel(OneOffOrder model, OneOffOrderViewModel viewModel)
        {
            viewModel.CompanyName = model.CompanyName;
            viewModel.BillingAddress = model.BillingAddress;
            viewModel.BillingCounty = model.BillingCounty;
            viewModel.BillingPostCode = model.BillingPostCode;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Created = model.Created;
            viewModel.DeliveryAddress = model.DeliveryAddress;
            viewModel.DeliveryCounty = model.DeliveryCounty;
            viewModel.DeliveryPostCode = model.DeliveryPostCode;
            viewModel.EmailAddress = model.EmailAddress;
            viewModel.ID = model.ID.ToString();
            viewModel.LastUpdated = model.LastUpdated;
            viewModel.OrderNumber = model.OrderNumber;
            viewModel.OrderNotes = model.OrderNotes;
            viewModel.PONumber = model.PONumber;
            viewModel.Phone = model.Phone;
            viewModel.Status = model.Status;
            viewModel.TotalNet = model.TotalNet;
            viewModel.TotalGross = model.TotalGross;
            viewModel.TotalVAT= model.TotalVAT;
            viewModel.DeliveryDate = model.DeliveryDate;
            viewModel.SupplierID = model.SupplierID != null ? model.SupplierID.ToString() : "";
            viewModel.OneOffOrderItems = model.OneOffOrderItems?.ToList() ?? new List<OneOffOrderItem>();
        }
    }

    public class OneOffOrderGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string OrderNumber { get; set; }
        public decimal Total { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
        public OneOffOrderStatus Status { get; set; }
    }

    public class OneOffOrderGridMapper: ModelMapper<OneOffOrder, OneOffOrderGridViewModel>
    {
        public override void MapToModel(OneOffOrderGridViewModel viewModel, OneOffOrder model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(OneOffOrder model, OneOffOrderGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CompanyName = model.CompanyName;
            viewModel.OrderNumber = model.OrderNumber;
            viewModel.Total = model.TotalGross;
            viewModel.TotalNet = model.TotalNet;
            viewModel.TotalVAT = model.TotalVAT;
            viewModel.Status = model.Status;
            viewModel.Created = model.Created;
            viewModel.LastUpdated = model.LastUpdated;
        }
    }

    public class ProformaOrderViewModel : OneOffOrderViewModel
    {
        public string ApproverID { get; set; }
        public string ApproverName { get; set; }
    }

    public class ProformaOrderMapper : OneOffOrderMapper
    {
        public new ProformaOrderViewModel MapToViewModel(OneOffOrder model)
        {
            var viewModel = new ProformaOrderViewModel();
            MapToViewModel(model, viewModel);
            viewModel.ApproverID = model.Company.ProformaApproverID.ToString();
            viewModel.ApproverName = model.Company.ProformaApprover.Name;
            return viewModel;
        }
    }

}
