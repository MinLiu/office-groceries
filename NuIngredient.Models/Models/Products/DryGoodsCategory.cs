﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class DryGoodsCategory : IEntity
    {
        public DryGoodsCategory() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid ProductID { get; set; }
        public int SortPos { get; set; }

        // Navigations
        public virtual ProductCategory Category { get; set; }
        public virtual Product Product { get; set; }
    }

}
