﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class OneOffOrderItem : IEntity
    {
        public OneOffOrderItem() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        [ForeignKey("OrderHeader")]
        public Guid OrderHeaderID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? ProductID { get; set; }
        public string ProductName { get; set; }
        public string ImageURL { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProductCode { get; set; }
        public decimal ProductVAT { get; set; }
        public int Qty { get; set; }
        public decimal Total { get; set; }
        [NotMapped]
        public decimal TotalVAT { get { return Math.Round(ProductVAT * ProductPrice * Qty, 2, MidpointRounding.AwayFromZero); } private set { } }
        [NotMapped]
        public decimal TotalGross { get { return Math.Round(TotalVAT + Total, 2, MidpointRounding.AwayFromZero); } private set { } }

        // Navigations
        public virtual OneOffOrder OrderHeader { get; set; }
        public virtual Company Company { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<DryGoodsCreditItem> Credits { get; set; }

        public void CalculateTotal()
        {
            Total = Qty * ProductPrice;
        }
    }

    public class OneOffOrderItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductName { get; set; }
        public string ImageURL { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal ProductVAT { get; set; }
        public string ProductCode { get; set; }
        public int Qty { get; set; }
        public decimal Net { get; set; }
        public decimal VAT { get; set; }
        public decimal Gross { get; set; }
        public string Unit { get; set; }
    }

    public class OneOffOrderItemMapper : ModelMapper<OneOffOrderItem, OneOffOrderItemViewModel>
    {
        public override void MapToModel(OneOffOrderItemViewModel viewModel, OneOffOrderItem model)
        {
            model.ProductPrice = viewModel.ProductPrice;
            model.Qty = viewModel.Qty;
            model.Total = viewModel.Qty * viewModel.ProductPrice;
            model.ProductName = viewModel.ProductName;
        }
        public override void MapToViewModel(OneOffOrderItem model, OneOffOrderItemViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ImageURL = model.ImageURL;
            viewModel.ProductCode = model.ProductCode;
            viewModel.ProductName = model.ProductName;
            viewModel.ProductPrice = model.ProductPrice;
            viewModel.ProductVAT = model.ProductVAT;
            viewModel.Qty = model.Qty;
            viewModel.Net = model.Total;
            viewModel.VAT = model.ProductVAT * model.Total;
            viewModel.Gross = viewModel.Net + viewModel.VAT;
            viewModel.Unit = model.Product?.Unit;
        }
    }

}
