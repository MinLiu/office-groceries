﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ShoppingCartItem : IEntity
    {
        public ShoppingCartItem() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid? CompanyID { get; set; }
        public Guid ProductID { get; set; }
        [MaxLength(50)]
        [Index("IX_ProspectToken", IsUnique = false)]
        public string ProspectToken { get; set; }
        public int Qty { get; set; }
        public DateTime UpdateTime { get; set; }
        // Navigations
        public virtual Company Company { get; set; }
        public virtual Product Product { get; set; }
    }

    public class ShoppingCartItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ImageURL { get; set; }
        public string ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal ProductVAT { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalVAT { get; set; }
        public decimal TotalGross { get; set; }
        public int Qty { get; set; }
        public string SupplierName { get; set; }
        public string SupplierID { get; set; }
    }

    public class ShoppingCartItemMapper : ModelMapper<ShoppingCartItem, ShoppingCartItemViewModel>
    {
        public override void MapToModel(ShoppingCartItemViewModel viewModel, ShoppingCartItem model)
        {
            throw new NotImplementedException();
        }
        public override void MapToViewModel(ShoppingCartItem model, ShoppingCartItemViewModel viewModel)
        {
            var price = model.Product.ExclusivePrice(model.CompanyID);
            
            viewModel.ID = model.ID.ToString();
            viewModel.ImageURL = model.Product.ImageURL;
            viewModel.ProductID = model.Product.ID.ToString();
            viewModel.ProductCode = model.Product.ProductCode;
            viewModel.ProductName = model.Product.Name;
            viewModel.ProductPrice = price;
            viewModel.ProductVAT = model.Product.VAT;
            viewModel.Qty = model.Qty;
            viewModel.TotalNet = price * model.Qty;
            viewModel.TotalVAT = model.Product.VAT * viewModel.TotalNet;
            viewModel.TotalGross = viewModel.TotalNet + viewModel.TotalVAT;
            viewModel.SupplierID = model.Product.SupplierID.ToString();
            viewModel.SupplierName = model.Product.Supplier != null ? model.Product.Supplier.Name : "";
        }

        public ShoppingCartItemViewModel MapToViewModel(ShoppingCartItem model, Company company)
        {
            var viewModel = new ShoppingCartItemViewModel();

            model.Product.GetPrice(company);
            MapToViewModel(model, viewModel);

            return viewModel;
        }

        public ShoppingCartItemViewModel MapToViewModel(ShoppingCartItem model, Guid companyID)
        {
            var company = new CompanyRepository().Find(companyID);

            return MapToViewModel(model, company);
        }
    }

}
