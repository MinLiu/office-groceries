﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class Product : IEntity
    {
        public Product() { ID = Guid.NewGuid(); IsActive = true; IsProspectsVisible = true; }
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool Deleted { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }        
        public decimal VAT { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        public Guid? SupplierID { get; set; }
        public int SortPosition { get; set; }
        public string InternalName { get; set; }
        

        // Navigations
        public virtual ICollection<DryGoodsCategory> DryGoodsCategories { get; set; }
        public virtual ICollection<DryGoodsAisle> DryGoodsAisles { get; set; }
        public virtual ICollection<ProductExclusivePrice> ExclusivePrices { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<DryGoodsExcludeCompany> ExcludeCompanies { get; set; }
        public virtual ICollection<ProductTagPrice> TagPrices { get; set; }

        // Methods
        public void GetPrice(Company company)
        {
            if (company == null)
                return;

            if (TryGetCustomPrice(out var customPrice))
            {
                Price = customPrice.Value;
                return;
            }

            if (TryGetTagPrice(out var tagPrice))
            {
                Price = tagPrice.Value;
                return;
            }

            bool TryGetCustomPrice(out decimal? price)
            {
                var exclusivePrice = ExclusivePrices.FirstOrDefault(x => x.CusomterID == company.ID);
                price = exclusivePrice?.Price;
                return exclusivePrice != null;
            }

            bool TryGetTagPrice(out decimal? price)
            {
                price = null;
                if (company.TagItems == null || !company.TagItems.Any())
                    return false;

                var companyTagIds = company.TagItems.Select(t => t.TagID).ToList();
                var tPrice = TagPrices?.Where(t => companyTagIds.Contains(t.TagID))
                    .OrderByDescending(x => x.Price)
                    .FirstOrDefault();

                price = tPrice?.Price;

                return tPrice != null;
            }
        }

        public decimal ExclusivePrice(Company company)
        {
            if (company == null)
                return Price;

            if (TryGetCustomPrice(out var customPrice))
                return customPrice.Value;

            if (TryGetTagPrice(out var tagPrice))
                return tagPrice.Value;

            return Price;

            bool TryGetCustomPrice(out decimal? price)
            {
                var exclusivePrice = ExclusivePrices.FirstOrDefault(x => x.CusomterID == company.ID);
                price = exclusivePrice?.Price;
                return exclusivePrice != null;
            }

            bool TryGetTagPrice(out decimal? price)
            {
                price = null;
                if (company.TagItems == null || !company.TagItems.Any())
                    return false;

                var companyTagIds = company.TagItems.Select(t => t.TagID).ToList();
                var tPrice = TagPrices?.Where(t => companyTagIds.Contains(t.TagID))
                    .OrderByDescending(x => x.Price)
                    .FirstOrDefault();

                price = tPrice?.Price;

                return tPrice != null;
            }
        }

        public decimal ExclusivePrice(Guid? companyID)
        {
            Company company = null;
            if (companyID != null)
            {
                company = new CompanyRepository().Find(companyID.Value);
            }
            return ExclusivePrice(company);
        }
    }

    #region EditProductViewModel

    public class EditProductViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        [Required(ErrorMessage = "* The Product Name field is required.")]
        [Display(Name = "Product Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "* The Product Code field is required.")]
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        public string ShortDescription { get; set; }

        [AllowHtml]
        public string Description { get; set; }
        public string Keywords { get; set; }

        public string CategoryID { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public decimal Cost { get; set; }
        public decimal VAT { get; set; }
        [Required(ErrorMessage = "* The Supplier is required.")]
        public Guid? SupplierID { get; set; }

        public List<string> DryGoodsCategoryIDs { get; set; }
        public List<string> DryGoodsAisleIDs { get; set; }
        public List<DryGoodsAisle> DryGoodsAisles { get; set; }
    }

    public class EditProductModelMapper : ModelMapper<Product, EditProductViewModel>
    {
        public override void MapToModel(EditProductViewModel viewModel, Product model)
        {
            model.ProductCode = viewModel.ProductCode;
            model.Name = viewModel.Name;
            model.ShortDescription = viewModel.ShortDescription;
            model.Description = RemoveTableStyle(viewModel.Description ?? "");
            model.IsExclusive = viewModel.IsExclusive;
            model.IsProspectsVisible = viewModel.IsProspectsVisible;
            model.IsActive = viewModel.IsActive;
            model.Price = viewModel.Price;
            model.Cost = viewModel.Cost;
            model.Unit = viewModel.Unit;
            model.VAT = viewModel.VAT;
            model.SupplierID = viewModel.SupplierID;
            model.Keywords = viewModel.Keywords;

        }

        private string RemoveTableStyle(string str = "")
        {
            var pattern = "<table[^>]*>";
            str = Regex.Replace(str, pattern, "<table>");
            return str;
        }

        public override void MapToViewModel(Product model, EditProductViewModel viewModel)
        {
            viewModel.Cost = model.Cost;
            viewModel.ShortDescription = model.ShortDescription;
            viewModel.Description = model.Description;
            viewModel.ImageURL = model.ImageURL;
            viewModel.ThumbnailImageURL = model.ThumbnailImageURL;
            viewModel.ID = model.ID.ToString();
            viewModel.IsActive = model.IsActive;
            viewModel.IsExclusive = model.IsExclusive;
            viewModel.IsProspectsVisible = model.IsProspectsVisible;
            viewModel.Name = model.Name;
            viewModel.Price = model.Price; 
            viewModel.ProductCode = model.ProductCode; 
            viewModel.Unit = model.Unit;
            viewModel.VAT = model.VAT;
            viewModel.DryGoodsCategoryIDs = model.DryGoodsCategories != null ? model.DryGoodsCategories.Select(x => x.Category.ID.ToString()).ToList() : new List<string>();
            viewModel.DryGoodsAisleIDs = model.DryGoodsAisles != null ? model.DryGoodsAisles.Select(x => x.Aisle.ID.ToString()).ToList() : new List<string>();
            viewModel.DryGoodsAisles = model.DryGoodsAisles != null ? model.DryGoodsAisles.ToList() : new List<DryGoodsAisle>();
            viewModel.SupplierID = model.SupplierID;
            viewModel.Keywords = model.Keywords;
        }
    }

    #endregion

    #region ProductGridViewModel

    public class ProductGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string CategoryName { get; set; }
        public string AisleName { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool IsProspectsVisible { get; set; }
        public bool IsExclusive { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
        public decimal Cost { get; set; }
        public Guid? SupplierID { get; set; }
        public string SupplierName { get; set; }
    }

    public class ProductGridMapper : ModelMapper<Product, ProductGridViewModel>
    {
        public override void MapToViewModel(Product model, ProductGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductCode = model.ProductCode;
            viewModel.Name = model.Name;
            viewModel.ShortDescription = model.ShortDescription;
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.ThumbnailImageURL = model.ThumbnailImageURL ?? "/Product Images/NotFound.png";
            viewModel.Price = model.Price;
            viewModel.VAT = model.VAT;
            viewModel.Cost = model.Cost;
            viewModel.IsActive = model.IsActive;
            viewModel.IsExclusive = model.IsExclusive;
            viewModel.IsProspectsVisible = model.IsProspectsVisible;
            viewModel.CategoryName = model.DryGoodsCategories.Any()? string.Join(", ", model.DryGoodsCategories.Select(x => x.Category.Name)) : "";
            viewModel.AisleName = model.DryGoodsAisles.Any()? string.Join(", ", model.DryGoodsAisles.Select(x => x.Aisle.Name)) : "";
            viewModel.SupplierID = model.SupplierID;
            viewModel.SupplierName = model.Supplier != null ? model.Supplier.Name : "";
        }


        public override void MapToModel(ProductGridViewModel viewModel, Product model)
        {
            throw new NotImplementedException();
        }

    }

    #endregion

    #region ProductListViewModel

    public class ProductListViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string InternalName { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailImageURL { get; set; }
        public string ShortDescription { get; set; }
        public decimal Price { get; set; }
        public string Unit { get; set; }
        public decimal VAT { get; set; }
    }

    public class ProductListViewMapper : ModelMapper<Product, ProductListViewModel>
    {
        public override void MapToModel(ProductListViewModel viewModel, Product model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(Product model, ProductListViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name.Trim();
            viewModel.InternalName = model.InternalName;
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.ThumbnailImageURL = model.ThumbnailImageURL ?? "/Product Images/NotFound.png";
            viewModel.ShortDescription = model.ShortDescription ?? "";
            viewModel.Price = model.Price;
            viewModel.Unit = model.Unit;
            viewModel.VAT = model.VAT;
        }

        public ProductListViewModel MapToViewModel(Product model, Company company)
        {
            var viewModel = new ProductListViewModel();
            model.GetPrice(company); // Get Exclusive price
            MapToViewModel(model, viewModel);

            return viewModel;
        }
    }

    #endregion

    public class ProductViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductName { get; set; }

        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailImageURL { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
    }
    public class ProductMapper : ModelMapper<Product, ProductViewModel>
    {
        public override void MapToViewModel(Product model, ProductViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductName = model.Name;
            viewModel.ProductCode = model.ProductCode;
            viewModel.ShortDescription = model.ShortDescription ?? "";
            viewModel.Description = model.Description ?? "";
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.ThumbnailImageURL = model.ThumbnailImageURL ?? "/Product Images/NotFound.png";
            viewModel.Price = model.Price;
            viewModel.VAT = model.VAT;
        }
        public override void MapToModel(ProductViewModel viewModel, Product model)
        {
            throw new NotImplementedException();
        }
        public ProductViewModel MapToViewModel(Product model, Company company)
        {
            var viewModel = new ProductViewModel();
            model.GetPrice(company); // Get Exclusive price
            MapToViewModel(model, viewModel);

            return viewModel;
        }
    }

    public class ProductKeywordGridVm
    {
        public Guid ID { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailImageURL { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string Keywords { get; set; }
    }


}
