﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NuIngredient.Models
{
    public class ProductExclusivePrice : IEntity
    {
        public ProductExclusivePrice() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        [ForeignKey("Customer")]
        public Guid CusomterID { get; set; }
        public decimal Price { get; set; }
        public virtual Product Product { get; set; }
        public virtual Company Customer { get; set; }
    }




    public class ProductExclusivePriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public decimal Price { get; set; }
        public SelectItemViewModel Customer { get; set; }
    }



    public class ProductExclusivePriceMapper : ModelMapper<ProductExclusivePrice, ProductExclusivePriceViewModel>
    {
        public override void MapToViewModel(ProductExclusivePrice model, ProductExclusivePriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Price = model.Price;
            viewModel.Customer = new SelectItemViewModel() { ID = model.Customer.ID.ToString(), Name = model.Customer.Name };
        }

        public override void MapToModel(ProductExclusivePriceViewModel viewModel, ProductExclusivePrice model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.CusomterID = Guid.Parse(viewModel.Customer.ID);
            model.Price = viewModel.Price;
        }

    }
}
