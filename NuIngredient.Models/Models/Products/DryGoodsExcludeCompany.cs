﻿using System;

namespace NuIngredient.Models
{
    public class DryGoodsExcludeCompany : ProductExcludeCompany
    {
        public Guid DryGoodProductID { get; set; }
        public virtual Product DryGoodProduct { get; set; }
    }
    
    public class DryGoodsExcludeCompanyGridMapper : ModelMapper<DryGoodsExcludeCompany, ProductExcludeCompanyGridViewModel>
    {
        public override void MapToViewModel(DryGoodsExcludeCompany model, ProductExcludeCompanyGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Customer = new SelectItemViewModel(model.CompanyID.ToString(), model.Company?.Name);
            viewModel.ProductID = model.DryGoodProductID.ToString();
        }

        public override void MapToModel(ProductExcludeCompanyGridViewModel viewModel, DryGoodsExcludeCompany model)
        {
            model.DryGoodProductID = Guid.Parse(viewModel.ProductID);
            model.CompanyID = Guid.Parse(viewModel.Customer.ID);
        }
    }
}