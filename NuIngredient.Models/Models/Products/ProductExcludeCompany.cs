﻿using System;

namespace NuIngredient.Models
{
    public abstract class ProductExcludeCompany : IEntity
    {
        protected ProductExcludeCompany()
        {
            ID = Guid.NewGuid();
        }

        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public virtual Company Company { get; set; }
    }

    public class ProductExcludeCompanyGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public SelectItemViewModel Customer { get; set; }
        public string ProductID { get; set; }
    }
}