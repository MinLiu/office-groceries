﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{
    public class ProductTagPrice : IEntity
    {
        public ProductTagPrice() { ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public Guid TagID { get; set; }
        public decimal Price { get; set; }

        public virtual Product Product { get; set; }
        public virtual Tag Tag { get; set; }
    }

    public class ProductTagPriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public TagGridViewModel Tag { get; set; }
        public decimal Price { get; set; }
    }

    public class ProductTagPriceMapper : ModelMapper<ProductTagPrice, ProductTagPriceViewModel>
    {
        private static readonly TagGridMapper TagMapper = new TagGridMapper();
        public override void MapToViewModel(ProductTagPrice model, ProductTagPriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Tag = TagMapper.MapToViewModel(model.Tag);
            viewModel.Price = model.Price;
        }

        public override void MapToModel(ProductTagPriceViewModel viewModel, ProductTagPrice model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.TagID = Guid.Parse(viewModel.Tag.ID);
            model.Price = viewModel.Price;

        }
    }
}
