﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SupAcctRequestLog : IEntity
    {
        public SupAcctRequestLog() {  ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid SupplierID { get; set; }
        public DateTime RequestTime { get; set; }

        public virtual Company Company { get; set; }
        public virtual Supplier Supplier { get; set; } 
       
    }



    public class SupAcctRequestLogViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public DateTime RequestTime { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
    }

    public class CompanyDryGoodsSupStatusViewModel
    {
        public bool Live { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public DateTime? RequestTime { get; set; }
    }



    public class SupAcctRequestLogMapper : ModelMapper<SupAcctRequestLog, SupAcctRequestLogViewModel>
    {
        public override void MapToViewModel(SupAcctRequestLog model, SupAcctRequestLogViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierID = model.SupplierID.ToString();
            viewModel.SupplierName = model.Supplier.Name;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CompanyName = model.Company.Name;
        }

        public override void MapToModel(SupAcctRequestLogViewModel viewModel, SupAcctRequestLog model)
        {
            throw new NotImplementedException();
        }

    }


}
