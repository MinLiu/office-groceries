﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class Supplier : IEntity, IEqualityComparer<Supplier>
    {
        public Supplier() {  ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public SupplierType SupplierType { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        public string SecondaryEmail { get; set; }
        public string Label { get; set; }
        public string Telephone { get; set; }
        public decimal MinimumValue { get; set; }
        public bool UseMasterAccount { get; set; }
        public bool CalendarNeeded { get; set; }
        public string RequestCenterEmail { get; set; }
        public bool Deleted { get; set; }
        public bool EmailGoStraight { get; set; }
        public bool WeeklyEmailReminderEnabled { get; set; }
        public DayOfWeek? WeeklyEmailReminderDay { get; set; } = DayOfWeek.Friday;
        public int? WeeklyEmailReminderHour { get; set; } = 15;
        public int WeeklyEmailReminderMinute { get; set; } = 0;
        public bool DailyEmailReminderEnabled { get; set; }
       
        public virtual ICollection<DeliveryCharge> DeliveryCharge { get; set; }
        public virtual ICollection<Aisle> DefaultAisles { get; set; }
        public virtual ICollection<SupplierPostcode> SupplierPostcode { get; set; }
        public virtual ICollection<SupplierDepot> SupplierDepots { get; set; }
        
        public virtual ICollection<SupplierMilkProductCode> SupplierMilkProductCodes { get; set; }
        public virtual ICollection<SupplierFruitProductCode> SupplierFruitProductCodes { get; set; }
        public virtual ICollection<IssueEmail> IssueEmails { get; set; }
        public virtual ICollection<SupplierOpenDay> SupplierOpenDays { get; set; }

        [NotMapped]
        public string DefaultAisleNames
        {
            get {
                return DefaultAisles != null ? DefaultAisles.Select(a => a.InternalName).FirstOrDefault() : "";
                //return DefaultAisles != null ? String.Join(", ", DefaultAisles.Select(a => a.Name)) : "";
            }
            private set { }
        }
            
        [NotMapped]
        public string DefaultAisleLabels
        {
            get {
                return DefaultAisles != null ? String.Join(", ", DefaultAisles.Select(a => a.Label)) : "";
            }
            private set { }
}

        public bool Equals(Supplier x, Supplier y)
        {
            if (x == null || y == null)
                return false;

            return x.ID == y.ID;
        }

        public int GetHashCode(Supplier obj)
        {
            return obj.ID.GetHashCode();
        }
    }



    public class SupplierViewModel : IEntityViewModel, IValidatableObject
    {
        public string ID { get; set; }
        [Required]
        public SelectItemViewModel SupplierType { get; set; }
        [Required(ErrorMessage = "* The Supplier Name field is required.")]
        [Display(Name = "Supplier Name")]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string SecondaryEmail { get; set; }
        public string Label { get; set; }
        public string Telephone { get; set; }
        public decimal MinimumValue { get; set; }
        public bool UseMasterAccount { get; set; }
        public bool CalendarNeeded { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.        
        public string DefaultAisleID { get; set; }
        public string DefaultAisleLabel { get; set; }
        public string DefaultAisleName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string RequestCenterEmail { get; set; }
        public bool EmailGoStraight { get; set; }
        public bool WeeklyEmailReminderEnabled { get; set; }
        public bool DailyEmailReminderEnabled { get; set; }
        public DayOfWeek? WeeklyReminderDay { get; set; }
        public int WeeklyReminderHourSetting { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            if (SupplierType == null || SupplierType.ID == null)
            {
                results.Add(new ValidationResult("Must select a supplier type", new[] { "SupplierType.ID" }));
            }
            else if (SupplierType.ID != null && (SupplierType)Enum.Parse(typeof(SupplierType), SupplierType.ID) == Models.SupplierType.DryGoods && UseMasterAccount == false)
            {
                if (RequestCenterEmail == null || RequestCenterEmail == "")
                {
                    results.Add(new ValidationResult("Must have request account email", new[] { "RequestCenterEmail" }));
                }
            }
            return results;
        }
    }

    public class SupplierGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public SelectItemViewModel SupplierType { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Label { get; set; }
        public string Telephone { get; set; }
        public decimal MinimumValue { get; set; }
        public bool UseMasterAccount { get; set; }
        public bool CalendarNeeded { get; set; }
        public bool Deleted { get; set; }    
        public string DefaultAisleID { get; set; }
        public string DefaultAisleLabel { get; set; }
        public string DefaultAisleName { get; set; }
        public string RequestCenterEmail { get; set; }
        public bool EmailGoStraight { get; set; }
    }





    public class SupplierMapper : ModelMapper<Supplier, SupplierViewModel>
    {
        public override void MapToViewModel(Supplier model, SupplierViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierType = new SelectItemViewModel() { ID = model.SupplierType.ToString(), Name = model.SupplierType.ToString() };
            viewModel.Name = model.Name;
            viewModel.Email = model.Email;
            viewModel.SecondaryEmail = model.SecondaryEmail;
            viewModel.Label = model.Label;
            viewModel.Telephone = model.Telephone;
            viewModel.Deleted = model.Deleted;
            viewModel.MinimumValue = model.MinimumValue;
            viewModel.UseMasterAccount = model.UseMasterAccount;
            viewModel.CalendarNeeded = model.CalendarNeeded;
            viewModel.DefaultAisleLabel = model.DefaultAisleLabels;
            viewModel.DefaultAisleName = model.DefaultAisleNames;
            viewModel.RequestCenterEmail = model.RequestCenterEmail;
            viewModel.EmailGoStraight = model.EmailGoStraight;
            viewModel.WeeklyEmailReminderEnabled = model.WeeklyEmailReminderEnabled;
            viewModel.WeeklyReminderDay = model.WeeklyEmailReminderDay;
            viewModel.WeeklyReminderHourSetting = (model.WeeklyEmailReminderHour ?? 15) * 100 + model.WeeklyEmailReminderMinute;
            viewModel.DailyEmailReminderEnabled = model.DailyEmailReminderEnabled;
        }

        public override void MapToModel(SupplierViewModel viewModel, Supplier model)
        {
            model.SupplierType = (SupplierType)Enum.Parse(typeof(SupplierType), viewModel.SupplierType.ID);
            model.Name = viewModel.Name;
            model.Email = viewModel.Email;
            model.SecondaryEmail = viewModel.SecondaryEmail;
            model.Label = viewModel.Label;
            model.Telephone = viewModel.Telephone;
            model.MinimumValue = viewModel.MinimumValue;
            model.UseMasterAccount = viewModel.UseMasterAccount;
            model.CalendarNeeded = viewModel.CalendarNeeded;
            model.RequestCenterEmail = viewModel.RequestCenterEmail;
            model.EmailGoStraight = viewModel.EmailGoStraight;
            model.WeeklyEmailReminderEnabled = viewModel.WeeklyEmailReminderEnabled;
            model.DailyEmailReminderEnabled = viewModel.DailyEmailReminderEnabled;
            model.WeeklyEmailReminderDay = viewModel.WeeklyReminderDay;
            if (model.WeeklyEmailReminderDay == 0 || model.WeeklyEmailReminderDay == null) // set default to Friday
            {
                model.WeeklyEmailReminderDay = DayOfWeek.Friday;
            }
            // WeeklyReminderHourSetting is a 4 digit number, first 2 digits are hour, last 2 digits are minute
            model.WeeklyEmailReminderHour = viewModel.WeeklyReminderHourSetting / 100;
            if (model.WeeklyEmailReminderHour == 0) // set default to 15
            {
                model.WeeklyEmailReminderHour = 15;
            }
            model.WeeklyEmailReminderMinute = viewModel.WeeklyReminderHourSetting % 100;
            model.WeeklyEmailReminderMinute = (model.WeeklyEmailReminderMinute / 30) * 30; // round to nearest
        }

    }

    public class SupplierGridMapper : ModelMapper<Supplier, SupplierGridViewModel>
    {
        private Setting _settings { get; set; }

        public SupplierGridMapper()
        {
            _settings = new SnapDbContext().Settings.FirstOrDefault();
        }

        public override void MapToViewModel(Supplier model, SupplierGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierType = new SelectItemViewModel() { ID = model.SupplierType.ToString(), Name = model.SupplierType.ToString() };
            viewModel.Name = model.Name;
            viewModel.Email = model.Email;
            viewModel.Label = model.Label;
            viewModel.Telephone = model.Telephone;
            viewModel.Deleted = model.Deleted;
            viewModel.MinimumValue = model.MinimumValue;
            viewModel.UseMasterAccount = model.UseMasterAccount;
            viewModel.CalendarNeeded = model.CalendarNeeded;
            viewModel.DefaultAisleLabel = model.DefaultAisleLabels;
            viewModel.DefaultAisleName = model.DefaultAisleNames;
            viewModel.RequestCenterEmail = model.RequestCenterEmail;
            viewModel.EmailGoStraight = model.EmailGoStraight;

            SetRequestCenterEmail(viewModel, model);

            
        }

        private void SetRequestCenterEmail(SupplierGridViewModel viewModel, Supplier model)
        {
            if (model.SupplierType == SupplierType.DryGoods)
            {
                viewModel.RequestCenterEmail = model.RequestCenterEmail;
            }
            else if (model.SupplierType == SupplierType.Milk)
            {
                if (model.EmailGoStraight == false)
                {
                    viewModel.RequestCenterEmail = _settings.MilkSupplierCenterEmail;
                }
                else
                {
                    viewModel.RequestCenterEmail = null;
                }
            }
            else if (model.SupplierType == SupplierType.Fruits)
            {
                if (model.EmailGoStraight == false)
                {
                    viewModel.RequestCenterEmail = _settings.FruitSupplierCenterEmail;
                }
                else
                {
                    viewModel.RequestCenterEmail = null;
                }
            }
        }

        public override void MapToModel(SupplierGridViewModel viewModel, Supplier model)
        {
            throw new NotImplementedException();
        }

    }


}
