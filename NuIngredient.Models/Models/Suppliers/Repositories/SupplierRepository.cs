﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SupplierRepository : EntityRespository<Supplier>
    {
        public SupplierRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Supplier entity)
        {
            //var oneOffOrderRepo = new OneOffOrderRepository();
            //var supplierOrders = oneOffOrderRepo.Read().Where(x => x.SupplierID == entity.ID).ToList();
            //supplierOrders.ForEach(x => x.SupplierID = null);
            //oneOffOrderRepo.Update(supplierOrders);

            var productRepo = new ProductRepository();
            var supplierProducts = productRepo.Read().Where(x => x.SupplierID == entity.ID).ToList();
            productRepo.Delete(supplierProducts);

            //var set = _db.Set<Supplier>().Where(a => a.ID == entity.ID);
            //_db.Set<Supplier>().RemoveRange(set);
            entity.Deleted = true;
            Update(entity, new string[] { "Deleted" });

            var aisleRepository = new AisleRepository();
            var supplierAisles = aisleRepository.Read().Where(x => x.DefaultSupplierID == entity.ID).ToList();
            foreach(var aisle in supplierAisles)
            {
                aisle.DefaultSupplierID = null;
                aisleRepository.Update(aisle);
            }

            var companySupRepo = new CompanySupplierRepository();
            var companySuppliers = companySupRepo.Read().Where(x => x.SupplierID == entity.ID).ToList();
            companySupRepo.Delete(companySuppliers);

            _db.Set<SupAcctRequestLog>().RemoveRange(_db.Set<SupAcctRequestLog>().Where(x => x.SupplierID == entity.ID));
            _db.SaveChanges();
        }


    }

}
