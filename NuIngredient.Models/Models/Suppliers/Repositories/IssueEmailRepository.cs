﻿
namespace NuIngredient.Models
{
    public class IssueEmailRepository : EntityRespository<IssueEmail>
    {
        public IssueEmailRepository()
            : this(new SnapDbContext())
        {

        }

        public IssueEmailRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
