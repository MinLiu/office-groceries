﻿
namespace NuIngredient.Models
{
    public class SupplierMilkProductCodeRepository : EntityRespository<SupplierMilkProductCode>
    {
        public SupplierMilkProductCodeRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierMilkProductCodeRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
