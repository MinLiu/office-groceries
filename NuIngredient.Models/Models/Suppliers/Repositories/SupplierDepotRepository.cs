﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SupplierDepotRepository : EntityRespository<SupplierDepot>
    {
        public SupplierDepotRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierDepotRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
