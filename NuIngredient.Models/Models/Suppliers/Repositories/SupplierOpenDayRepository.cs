﻿using System;
using System.Linq;

namespace NuIngredient.Models
{
    public class SupplierOpenDayRepository : EntityRespository<SupplierOpenDay>
    {
        public SupplierOpenDayRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierOpenDayRepository(SnapDbContext db)
            : base(db)
        {

        }
        
        public IQueryable<SupplierOpenDay> ReadSupplierOpenDays(Guid? supplierID)
        {
            return Read().Where(x => x.SupplierID == supplierID);
        }
    }

}
