﻿
namespace NuIngredient.Models
{
    public class SupplierFruitProductCodeRepository : EntityRespository<SupplierFruitProductCode>
    {
        public SupplierFruitProductCodeRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierFruitProductCodeRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
