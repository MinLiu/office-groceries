﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SupAcctRequestLogRepository : EntityRespository<SupAcctRequestLog>
    {
        public SupAcctRequestLogRepository()
            : this(new SnapDbContext())
        {

        }

        public SupAcctRequestLogRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
