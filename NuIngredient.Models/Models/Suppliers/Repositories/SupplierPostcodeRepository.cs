﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SupplierPostcodeRepository : EntityRespository<SupplierPostcode>
    {
        public SupplierPostcodeRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierPostcodeRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
