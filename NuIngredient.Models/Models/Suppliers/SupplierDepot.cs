﻿using System;

namespace NuIngredient.Models
{
    public class SupplierDepot : IEntity
    {
        public SupplierDepot()
        {
            ID = Guid.NewGuid();
        }
        
        public Guid ID { get; set; }
        public Guid SupplierID { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        
        public virtual Supplier Supplier { get; set; }
    }

    public class SupplierDepotGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string SupplierID { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }

    public class SupplierDepotGridMapper : ModelMapper<SupplierDepot, SupplierDepotGridViewModel>
    {
        public override void MapToViewModel(SupplierDepot model, SupplierDepotGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierID = model.SupplierID.ToString();
            viewModel.Name = model.Name;
            viewModel.Telephone = model.Telephone;
            viewModel.Email = model.Email;
        }

        public override void MapToModel(SupplierDepotGridViewModel viewModel, SupplierDepot model)
        {
            model.SupplierID = Guid.Parse(viewModel.SupplierID);
            model.Name = viewModel.Name;
            model.Telephone = viewModel.Telephone;
            model.Email = viewModel.Email;
        }
    }
}