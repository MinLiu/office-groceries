﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class SupplierPostcode : IEntity
    {
        public SupplierPostcode() {  ID = Guid.NewGuid(); }
        [Key]
        public Guid ID { get; set; }
        public Guid SupplierID { get; set; }
        [Required, MaxLength(10)]
        public string Postcode { get; set; }

        public virtual Supplier Supplier { get; set; }
    }



    public class SupplierPostcodeGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Postcode { get; set; }
        public string SupplierID { get; set; }
    }





    public class SupplierPostcodeGridMapper : ModelMapper<SupplierPostcode, SupplierPostcodeGridViewModel>
    {
        public override void MapToViewModel(SupplierPostcode model, SupplierPostcodeGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierID = model.SupplierID.ToString();
            viewModel.Postcode = model.Postcode;
        }

        public override void MapToModel(SupplierPostcodeGridViewModel viewModel, SupplierPostcode model)
        {
            model.Postcode = (viewModel.Postcode ?? "").ToUpper().Trim().Replace(" ", "");
            model.SupplierID = Guid.Parse(viewModel.SupplierID);
        }

    }


}
