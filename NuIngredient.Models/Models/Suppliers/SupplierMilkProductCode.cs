﻿using System;

namespace NuIngredient.Models
{
    public class SupplierMilkProductCode : IEntity, ISupplierProductCode
    {
        public Guid ID { get; set; } = Guid.NewGuid();
        public Guid ProductID => MilkProductID;
        public Guid MilkProductID { get; set; }
        public Guid SupplierID { get; set; }
        public string Code { get; set; }
        
        public virtual Supplier Supplier { get; set; }
        public virtual MilkProduct MilkProduct { get; set; }
    }
    
    public class SupplierMilkProductCodeGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public SelectItemViewModel Product { get; set; }
        public SelectItemViewModel Supplier { get; set; }
        public string Code { get; set; }
    }
    
    public class SupplierMilkProductCodeGridMapper : ModelMapper<SupplierMilkProductCode, SupplierMilkProductCodeGridViewModel>
    {
        public override void MapToViewModel(SupplierMilkProductCode model, SupplierMilkProductCodeGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Product = new SelectItemViewModel(model.ProductID.ToString(), model.MilkProduct?.Name);
            viewModel.Supplier = new SelectItemViewModel(model.SupplierID.ToString(), model.Supplier?.Name);
            viewModel.Code = model.Code;
        }

        public override void MapToModel(SupplierMilkProductCodeGridViewModel viewModel, SupplierMilkProductCode model)
        {
            model.MilkProductID = Guid.Parse(viewModel.Product.ID);
            model.SupplierID = Guid.Parse(viewModel.Supplier.ID);
            model.Code = viewModel.Code;
        }
    }
}