﻿using System;

namespace NuIngredient.Models
{
    public class IssueEmail : IEntity
    {
        public Guid ID { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public Guid CompanyID { get; set; }
        public Guid SupplierID { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedDateUtc { get; set; } = DateTime.UtcNow;
        
        public virtual Company Company { get; set; }
        public virtual Supplier Supplier { get; set; }
    }

    public class IssueEmailGridViewModel : IEntityViewModel
    { 
        public string ID { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class IssueEmailGridMapper : ModelMapper<IssueEmail, IssueEmailGridViewModel>
    {
        public override void MapToViewModel(IssueEmail model, IssueEmailGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Subject = model.Subject;
            viewModel.Message = model.Message;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CompanyName = model.Company.Name;
            viewModel.SupplierID = model.SupplierID.ToString();
            viewModel.SupplierName = model.Supplier.Name;
            viewModel.UserName = model.UserName;
            viewModel.CreatedDate = model.CreatedDateUtc.ToLocalTime();
        }

        public override void MapToModel(IssueEmailGridViewModel viewModel, IssueEmail model)
        {
            throw new NotSupportedException();
        }
    }

}