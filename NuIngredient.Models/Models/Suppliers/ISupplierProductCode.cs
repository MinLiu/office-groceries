﻿using System;

namespace NuIngredient.Models
{
    public interface ISupplierProductCode
    {
        Guid ID { get; set; }
        Guid ProductID { get; }
        Guid SupplierID { get; set; }
        string Code { get; set; }
    }
}