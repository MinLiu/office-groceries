﻿namespace NuIngredient.Models
{
    public enum SupplierType
    {
        Milk = 1,
        Fruits = 2,
        DryGoods = 3,
        Snacks = 4,
    }
}
