﻿using System;

namespace NuIngredient.Models
{
    public class SupplierFruitProductCode : IEntity, ISupplierProductCode
    {
        public Guid ID { get; set; } = Guid.NewGuid();
        public Guid ProductID => FruitProductID;
        public Guid FruitProductID { get; set; }
        public Guid SupplierID { get; set; }
        public string Code { get; set; }
        
        public virtual Supplier Supplier { get; set; }
        public virtual FruitProduct FruitProduct { get; set; }
    }
    
    public class SupplierFruitProductCodeGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public SelectItemViewModel Product { get; set; }
        public SelectItemViewModel Supplier { get; set; }
        public string Code { get; set; }
    }
    
    public class SupplierFruitProductCodeGridMapper : ModelMapper<SupplierFruitProductCode, SupplierFruitProductCodeGridViewModel>
    {
        public override void MapToViewModel(SupplierFruitProductCode model, SupplierFruitProductCodeGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Product = new SelectItemViewModel(model.ProductID.ToString(), model.FruitProduct?.Name);
            viewModel.Supplier = new SelectItemViewModel(model.SupplierID.ToString(), model.Supplier?.Name);
            viewModel.Code = model.Code;
        }

        public override void MapToModel(SupplierFruitProductCodeGridViewModel viewModel, SupplierFruitProductCode model)
        {
            model.FruitProductID = Guid.Parse(viewModel.Product.ID);
            model.SupplierID = Guid.Parse(viewModel.Supplier.ID);
            model.Code = viewModel.Code;
        }
    }
}