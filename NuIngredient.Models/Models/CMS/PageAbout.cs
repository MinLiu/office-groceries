﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class StaticPage : IEntity
    {
        public StaticPage() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Content { get; set; }
    }

    public class StaticPageViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        [AllowHtml]
        public string Content { get; set; }
    }

    public class StaticPageMapper : ModelMapper<StaticPage, StaticPageViewModel>
    {
        public override void MapToModel(StaticPageViewModel viewModel, StaticPage model)
        {
            model.MetaTitle = viewModel.MetaTitle;
            model.MetaKeyword = viewModel.MetaKeyword;
            model.MetaDescription = viewModel.MetaDescription;
            model.Title = viewModel.Title;
            model.Subtitle = viewModel.Subtitle;
            model.Content = viewModel.Content;
        }

        public override void MapToViewModel(StaticPage model, StaticPageViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.MetaTitle = model.MetaTitle;
            viewModel.MetaKeyword = model.MetaKeyword;
            viewModel.MetaDescription = model.MetaDescription;
            viewModel.Title = model.Title;
            viewModel.Subtitle = model.Subtitle;
            viewModel.Content = model.Content;
        }
    }
}
