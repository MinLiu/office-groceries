﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{
    public class PageSetting : IEntity
    {
        public PageSetting() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string Label { get; set; }
        public string FilePath { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public bool Active { get; set; }
    }

    public class PageSettingViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Label { get; set; }
        public string FilePath { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public bool Active { get; set; }
    }

    public class PageSettingGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Label { get; set; }
        public string FilePath { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public bool Active { get; set; }
    }

    public class PageSettingGridMapper : ModelMapper<PageSetting, PageSettingGridViewModel>
    {
        public override void MapToModel(PageSettingGridViewModel viewModel, PageSetting model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(PageSetting model, PageSettingGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Label = model.Label;
            viewModel.FilePath = model.FilePath;
            viewModel.MetaTitle = model.MetaTitle;
            viewModel.MetaKeyword = model.MetaKeyword;
            viewModel.MetaDescription = model.MetaDescription;
            viewModel.Active = model.Active;
        }
    }

    public class PageSettingMapper : ModelMapper<PageSetting, PageSettingViewModel>
    {
        public override void MapToModel(PageSettingViewModel viewModel, PageSetting model)
        {
            model.Label = viewModel.Label;
            model.FilePath = viewModel.FilePath;
            model.MetaTitle = viewModel.MetaTitle;
            model.MetaKeyword = viewModel.MetaKeyword;
            model.MetaDescription = viewModel.MetaDescription;
            model.Active = viewModel.Active;
        }

        public override void MapToViewModel(PageSetting model, PageSettingViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Label = model.Label;
            viewModel.FilePath = model.FilePath;
            viewModel.MetaTitle = model.MetaTitle;
            viewModel.MetaKeyword = model.MetaKeyword;
            viewModel.MetaDescription = model.MetaDescription;
            viewModel.Active = model.Active;
        }
    }
}
