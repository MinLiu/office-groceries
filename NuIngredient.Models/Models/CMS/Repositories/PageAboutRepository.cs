﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class StaticPageRepository : EntityRespository<StaticPage>
    {
        public StaticPageRepository()
            : this(new SnapDbContext())
        {

        }

        public StaticPageRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
