﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class PageSettingRepository : EntityRespository<PageSetting>
    {
        public PageSettingRepository()
            : this(new SnapDbContext())
        {

        }

        public PageSettingRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
