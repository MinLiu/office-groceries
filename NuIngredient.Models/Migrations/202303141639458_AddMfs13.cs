﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMfs13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanySuppliers", "IsMfs13", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanySuppliers", "IsMfs13");
        }
    }
}
