namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitProducts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitProductCategories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ParentID = c.Guid(),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitProductCategories", t => t.ParentID)
                .Index(t => t.ParentID);
            
            CreateTable(
                "dbo.FruitProductExclusivePrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CusomterID = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CusomterID)
                .ForeignKey("dbo.FruitProducts", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CusomterID);
            
            CreateTable(
                "dbo.FruitProducts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Name = c.String(),
                        ShortDescription = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        ImageURL = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        IsProspectsVisible = c.Boolean(nullable: false),
                        IsExclusive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitProductCategories", t => t.CategoryID)
                .Index(t => t.CategoryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitProductExclusivePrices", "ProductID", "dbo.FruitProducts");
            DropForeignKey("dbo.FruitProducts", "CategoryID", "dbo.FruitProductCategories");
            DropForeignKey("dbo.FruitProductExclusivePrices", "CusomterID", "dbo.Companies");
            DropForeignKey("dbo.FruitProductCategories", "ParentID", "dbo.FruitProductCategories");
            DropIndex("dbo.FruitProducts", new[] { "CategoryID" });
            DropIndex("dbo.FruitProductExclusivePrices", new[] { "CusomterID" });
            DropIndex("dbo.FruitProductExclusivePrices", new[] { "ProductID" });
            DropIndex("dbo.FruitProductCategories", new[] { "ParentID" });
            DropTable("dbo.FruitProducts");
            DropTable("dbo.FruitProductExclusivePrices");
            DropTable("dbo.FruitProductCategories");
        }
    }
}
