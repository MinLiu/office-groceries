namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeliveryTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanySuppliers", "DeliveryTimeFrom", c => c.DateTime());
            AddColumn("dbo.CompanySuppliers", "DeliveryTimeTo", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanySuppliers", "DeliveryTimeTo");
            DropColumn("dbo.CompanySuppliers", "DeliveryTimeFrom");
        }
    }
}
