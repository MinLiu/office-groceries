﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExcludeCompanies : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AisleExcludeCompanies",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AisleID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Aisles", t => t.AisleID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.AisleID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.ProductExcludeCompanies",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        DryGoodProductID = c.Guid(),
                        FruitProductID = c.Guid(),
                        MilkProductID = c.Guid(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Products", t => t.DryGoodProductID)
                .ForeignKey("dbo.FruitProducts", t => t.FruitProductID)
                .ForeignKey("dbo.MilkProducts", t => t.MilkProductID)
                .Index(t => t.CompanyID)
                .Index(t => t.DryGoodProductID)
                .Index(t => t.FruitProductID)
                .Index(t => t.MilkProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductExcludeCompanies", "MilkProductID", "dbo.MilkProducts");
            DropForeignKey("dbo.ProductExcludeCompanies", "FruitProductID", "dbo.FruitProducts");
            DropForeignKey("dbo.ProductExcludeCompanies", "DryGoodProductID", "dbo.Products");
            DropForeignKey("dbo.ProductExcludeCompanies", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.AisleExcludeCompanies", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.AisleExcludeCompanies", "AisleID", "dbo.Aisles");
            DropIndex("dbo.ProductExcludeCompanies", new[] { "MilkProductID" });
            DropIndex("dbo.ProductExcludeCompanies", new[] { "FruitProductID" });
            DropIndex("dbo.ProductExcludeCompanies", new[] { "DryGoodProductID" });
            DropIndex("dbo.ProductExcludeCompanies", new[] { "CompanyID" });
            DropIndex("dbo.AisleExcludeCompanies", new[] { "CompanyID" });
            DropIndex("dbo.AisleExcludeCompanies", new[] { "AisleID" });
            DropTable("dbo.ProductExcludeCompanies");
            DropTable("dbo.AisleExcludeCompanies");
        }
    }
}
