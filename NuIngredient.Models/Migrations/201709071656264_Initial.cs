namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        ModuleTypeID = c.Int(nullable: false),
                        Description = c.String(),
                        ActivityByUserID = c.String(maxLength: 128),
                        LinkID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ActivityByUserID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.ModuleTypes", t => t.ModuleTypeID)
                .Index(t => t.CompanyID)
                .Index(t => t.ModuleTypeID)
                .Index(t => t.ActivityByUserID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        CompanyID = c.Guid(nullable: false),
                        Token = c.String(),
                        AccessCRM = c.Boolean(nullable: false),
                        AccessProducts = c.Boolean(nullable: false),
                        AccessQuotations = c.Boolean(nullable: false),
                        AccessJobs = c.Boolean(nullable: false),
                        AccessInvoices = c.Boolean(nullable: false),
                        AccessDeliveryNotes = c.Boolean(nullable: false),
                        AccessPurchaseOrders = c.Boolean(nullable: false),
                        AccessStockControl = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.ClientEvents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        AssignedUserID = c.String(maxLength: 128),
                        CreatorID = c.String(maxLength: 128),
                        ClientID = c.Guid(),
                        ClientContactID = c.Guid(),
                        Timestamp = c.DateTime(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.ClientContacts", t => t.ClientContactID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorID)
                .Index(t => t.CompanyID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.CreatorID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientContactID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientTypeID = c.Guid(),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Website = c.String(),
                        Notes = c.String(),
                        SageAccountReference = c.String(),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.ClientTypes", t => t.ClientTypeID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientTypeID);
            
            CreateTable(
                "dbo.ClientAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        County = c.String(),
                        Town = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.DeliveryNotes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        JobID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        Number = c.Int(nullable: false),
                        ClientReference = c.String(),
                        Description = c.String(),
                        Note = c.String(),
                        InternalNote = c.String(),
                        PickedBy = c.String(),
                        PoNumber = c.String(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        EstimatedDelivery = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .ForeignKey("dbo.ClientAddresses", t => t.DeliveryAddressID)
                .ForeignKey("dbo.DeliveryNoteStatus", t => t.StatusID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.StatusID)
                .Index(t => t.JobID)
                .Index(t => t.DeliveryAddressID);
            
            CreateTable(
                "dbo.DeliveryNoteAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNotes", t => t.DeliveryNoteID)
                .Index(t => t.DeliveryNoteID);
            
            CreateTable(
                "dbo.DeliveryNoteComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNotes", t => t.DeliveryNoteID)
                .Index(t => t.DeliveryNoteID);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AccountNo = c.String(),
                        SignupDate = c.DateTime(nullable: false),
                        AccountClosed = c.Boolean(nullable: false),
                        DeleteData = c.Boolean(nullable: false),
                        Name = c.String(nullable: false),
                        Motto = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        VatNumber = c.String(),
                        LogoImageURL = c.String(),
                        DefaultVatRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                        DefaultCurrencyID = c.Int(nullable: false),
                        UseTeams = c.Boolean(nullable: false),
                        UseHierarchy = c.Boolean(nullable: false),
                        AccessSameLevelHierarchy = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.DefaultCurrencyID)
                .Index(t => t.DefaultCurrencyID);
            
            CreateTable(
                "dbo.CompanyAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.ClientTags",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 3),
                        Name = c.String(nullable: false, maxLength: 128),
                        Symbol = c.String(nullable: false, maxLength: 3),
                        UseInCommerce = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNotePreviews",
                c => new
                    {
                        DeliveryNoteID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.DeliveryNoteID)
                .ForeignKey("dbo.DeliveryNoteTemplates", t => t.BackTemplateID)
                .ForeignKey("dbo.DeliveryNoteTemplates", t => t.BodyTemplateID)
                .ForeignKey("dbo.DeliveryNoteTemplates", t => t.FrontTemplateID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.DeliveryNotes", t => t.DeliveryNoteID)
                .Index(t => t.DeliveryNoteID)
                .Index(t => t.CompanyID)
                .Index(t => t.FrontTemplateID)
                .Index(t => t.BodyTemplateID)
                .Index(t => t.BackTemplateID);
            
            CreateTable(
                "dbo.DeliveryNoteTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.DeliveryNoteSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.DefaultDeliveryNoteAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        DeliveryNoteSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNoteSettings", t => t.DeliveryNoteSettings_CompanyID)
                .Index(t => t.DeliveryNoteSettings_CompanyID);
            
            CreateTable(
                "dbo.DefaultDeliveryNoteNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        DeliveryNoteSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNoteSettings", t => t.DeliveryNoteSettings_CompanyID)
                .Index(t => t.DeliveryNoteSettings_CompanyID);
            
            CreateTable(
                "dbo.HierarchyMembers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ReportsToID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.HierarchyMembers", t => t.ReportsToID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.CompanyID)
                .Index(t => t.UserId)
                .Index(t => t.ReportsToID);
            
            CreateTable(
                "dbo.InvoicePreviews",
                c => new
                    {
                        InvoiceID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.InvoiceID)
                .ForeignKey("dbo.InvoiceTemplates", t => t.BackTemplateID)
                .ForeignKey("dbo.InvoiceTemplates", t => t.BodyTemplateID)
                .ForeignKey("dbo.InvoiceTemplates", t => t.FrontTemplateID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Invoices", t => t.InvoiceID)
                .Index(t => t.InvoiceID)
                .Index(t => t.CompanyID)
                .Index(t => t.FrontTemplateID)
                .Index(t => t.BodyTemplateID)
                .Index(t => t.BackTemplateID);
            
            CreateTable(
                "dbo.InvoiceTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        QuotationID = c.Guid(),
                        JobID = c.Guid(),
                        DeliveryNoteID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        ClientReference = c.String(),
                        IsProForma = c.Boolean(nullable: false),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        PoNumber = c.String(),
                        AccountCode = c.String(),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        InvoiceDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.ClientAddresses", t => t.DeliveryAddressID)
                .ForeignKey("dbo.DeliveryNotes", t => t.DeliveryNoteID)
                .ForeignKey("dbo.ClientAddresses", t => t.InvoiceAddressID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .ForeignKey("dbo.InvoiceStatus", t => t.StatusID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.StatusID)
                .Index(t => t.CurrencyID)
                .Index(t => t.QuotationID)
                .Index(t => t.JobID)
                .Index(t => t.DeliveryNoteID)
                .Index(t => t.InvoiceAddressID)
                .Index(t => t.DeliveryAddressID);
            
            CreateTable(
                "dbo.InvoiceAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Invoices", t => t.InvoiceID)
                .Index(t => t.InvoiceID);
            
            CreateTable(
                "dbo.InvoiceComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Invoices", t => t.InvoiceID)
                .Index(t => t.InvoiceID);
            
            CreateTable(
                "dbo.InvoiceItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Invoices", t => t.InvoiceID)
                .ForeignKey("dbo.InvoiceItems", t => t.ParentItemID)
                .ForeignKey("dbo.InvoiceSections", t => t.SectionID)
                .Index(t => t.InvoiceID)
                .Index(t => t.SectionID)
                .Index(t => t.ParentItemID);
            
            CreateTable(
                "dbo.InvoiceSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Invoices", t => t.InvoiceID)
                .Index(t => t.InvoiceID);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        QuotationID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        ClientReference = c.String(),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        PoNumber = c.String(),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        EstimatedDelivery = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.ClientAddresses", t => t.DeliveryAddressID)
                .ForeignKey("dbo.ClientAddresses", t => t.InvoiceAddressID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .ForeignKey("dbo.JobStatus", t => t.StatusID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.StatusID)
                .Index(t => t.CurrencyID)
                .Index(t => t.QuotationID)
                .Index(t => t.InvoiceAddressID)
                .Index(t => t.DeliveryAddressID);
            
            CreateTable(
                "dbo.JobAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.JobComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.JobItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .ForeignKey("dbo.JobItems", t => t.ParentItemID)
                .ForeignKey("dbo.JobSections", t => t.SectionID)
                .Index(t => t.JobID)
                .Index(t => t.SectionID)
                .Index(t => t.ParentItemID);
            
            CreateTable(
                "dbo.JobSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.JobNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.JobPreviews",
                c => new
                    {
                        JobID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.JobID)
                .ForeignKey("dbo.JobTemplates", t => t.BackTemplateID)
                .ForeignKey("dbo.JobTemplates", t => t.BodyTemplateID)
                .ForeignKey("dbo.JobTemplates", t => t.FrontTemplateID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.JobID)
                .Index(t => t.CompanyID)
                .Index(t => t.FrontTemplateID)
                .Index(t => t.BodyTemplateID)
                .Index(t => t.BackTemplateID);
            
            CreateTable(
                "dbo.JobTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Quotations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        ClientReference = c.String(),
                        Number = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        PoNumber = c.String(),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalDiscount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalMargin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        EstimatedDelivery = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.ClientAddresses", t => t.DeliveryAddressID)
                .ForeignKey("dbo.ClientAddresses", t => t.InvoiceAddressID)
                .ForeignKey("dbo.QuotationStatus", t => t.StatusID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.StatusID)
                .Index(t => t.CurrencyID)
                .Index(t => t.InvoiceAddressID)
                .Index(t => t.DeliveryAddressID);
            
            CreateTable(
                "dbo.QuotationAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Markup = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ListPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MarginPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationItems", t => t.ParentItemID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .ForeignKey("dbo.QuotationSections", t => t.SectionID)
                .Index(t => t.QuotationID)
                .Index(t => t.SectionID)
                .Index(t => t.ParentItemID);
            
            CreateTable(
                "dbo.QuotationSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalDiscount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalMargin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationPreviews",
                c => new
                    {
                        QuotationID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.QuotationID)
                .ForeignKey("dbo.QuotationTemplates", t => t.BackTemplateID)
                .ForeignKey("dbo.QuotationTemplates", t => t.BodyTemplateID)
                .ForeignKey("dbo.QuotationTemplates", t => t.FrontTemplateID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID)
                .Index(t => t.CompanyID)
                .Index(t => t.FrontTemplateID)
                .Index(t => t.BodyTemplateID)
                .Index(t => t.BackTemplateID);
            
            CreateTable(
                "dbo.QuotationTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.QuotationStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Invoices", t => t.InvoiceID)
                .Index(t => t.InvoiceID);
            
            CreateTable(
                "dbo.InvoiceStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        DefaultProFormaReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        QueryEmail = c.String(),
                        DefaultPaymentDays = c.Int(nullable: false),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.DefaultCurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.DefaultCurrencyID);
            
            CreateTable(
                "dbo.DefaultInvoiceAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        InvoiceSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.InvoiceSettings", t => t.InvoiceSettings_CompanyID)
                .Index(t => t.InvoiceSettings_CompanyID);
            
            CreateTable(
                "dbo.DefaultInvoiceNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        InvoiceSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.InvoiceSettings", t => t.InvoiceSettings_CompanyID)
                .Index(t => t.InvoiceSettings_CompanyID);
            
            CreateTable(
                "dbo.JobSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.DefaultCurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.DefaultCurrencyID);
            
            CreateTable(
                "dbo.DefaultJobAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        JobSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.JobSettings", t => t.JobSettings_CompanyID)
                .Index(t => t.JobSettings_CompanyID);
            
            CreateTable(
                "dbo.DefaultJobNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        JobSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.JobSettings", t => t.JobSettings_CompanyID)
                .Index(t => t.JobSettings_CompanyID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Category = c.String(),
                        Unit = c.String(),
                        VendorCode = c.String(),
                        Manufacturer = c.String(),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        UsePriceBreaks = c.Boolean(nullable: false),
                        IsBought = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MinimumQty = c.Int(nullable: false),
                        MaximumQty = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.ProductCosts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.ProductKitItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        KitID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Product_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.KitID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.Products", t => t.Product_ID)
                .Index(t => t.KitID)
                .Index(t => t.ProductID)
                .Index(t => t.Product_ID);
            
            CreateTable(
                "dbo.ProductPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.SupplierProducts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.ProductID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        SupplierTypeID = c.Guid(),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Website = c.String(),
                        Notes = c.String(),
                        SageAccountReference = c.String(),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.SupplierTypes", t => t.SupplierTypeID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierTypeID);
            
            CreateTable(
                "dbo.SupplierAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.SupplierAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.SupplierContacts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Position = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.SupplierTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.PurchaseOrderPreviews",
                c => new
                    {
                        PurchaseOrderID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.PurchaseOrderID)
                .ForeignKey("dbo.PurchaseOrderTemplates", t => t.BackTemplateID)
                .ForeignKey("dbo.PurchaseOrderTemplates", t => t.BodyTemplateID)
                .ForeignKey("dbo.PurchaseOrderTemplates", t => t.FrontTemplateID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .Index(t => t.PurchaseOrderID)
                .Index(t => t.CompanyID)
                .Index(t => t.FrontTemplateID)
                .Index(t => t.BodyTemplateID)
                .Index(t => t.BackTemplateID);
            
            CreateTable(
                "dbo.PurchaseOrderTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        SupplierID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        QuotationID = c.Guid(),
                        JobID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        AccountCode = c.String(),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierInvoiceNo = c.String(),
                        SupplierInvoiceVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierInvoiceNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierInvoiceTotal = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierDeliveryNote = c.String(),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        ClientDeliveryAddressID = c.Guid(),
                        DeliverToClient = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        RequiredDate = c.DateTime(nullable: false),
                        SentDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientAddresses", t => t.ClientDeliveryAddressID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.CompanyAddresses", t => t.DeliveryAddressID)
                .ForeignKey("dbo.CompanyAddresses", t => t.InvoiceAddressID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .ForeignKey("dbo.PurchaseOrderStatus", t => t.StatusID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientID)
                .Index(t => t.SupplierID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.StatusID)
                .Index(t => t.CurrencyID)
                .Index(t => t.QuotationID)
                .Index(t => t.JobID)
                .Index(t => t.InvoiceAddressID)
                .Index(t => t.DeliveryAddressID)
                .Index(t => t.ClientDeliveryAddressID);
            
            CreateTable(
                "dbo.PurchaseOrderAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .Index(t => t.PurchaseOrderID);
            
            CreateTable(
                "dbo.PurchaseOrderComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .Index(t => t.PurchaseOrderID);
            
            CreateTable(
                "dbo.PurchaseOrderItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        QuantityReceived = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .ForeignKey("dbo.PurchaseOrderSections", t => t.SectionID)
                .Index(t => t.PurchaseOrderID)
                .Index(t => t.SectionID);
            
            CreateTable(
                "dbo.PurchaseOrderSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .Index(t => t.PurchaseOrderID);
            
            CreateTable(
                "dbo.PurchaseOrderNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .Index(t => t.PurchaseOrderID);
            
            CreateTable(
                "dbo.PurchaseOrderStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.DefaultCurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.DefaultCurrencyID);
            
            CreateTable(
                "dbo.DefaultPurchaseOrderAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        PurchaseOrderSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrderSettings", t => t.PurchaseOrderSettings_CompanyID)
                .Index(t => t.PurchaseOrderSettings_CompanyID);
            
            CreateTable(
                "dbo.DefaultPurchaseOrderNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        PurchaseOrderSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrderSettings", t => t.PurchaseOrderSettings_CompanyID)
                .Index(t => t.PurchaseOrderSettings_CompanyID);
            
            CreateTable(
                "dbo.QuotationSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowMarkup = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        ShowDiscount = c.Boolean(nullable: false),
                        ShowListPrice = c.Boolean(nullable: false),
                        ShowCost = c.Boolean(nullable: false),
                        ShowMargin = c.Boolean(nullable: false),
                        ShowMarginPercentage = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.DefaultCurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.DefaultCurrencyID);
            
            CreateTable(
                "dbo.DefaultQuotationAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        QuotationSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationSettings", t => t.QuotationSettings_CompanyID)
                .Index(t => t.QuotationSettings_CompanyID);
            
            CreateTable(
                "dbo.DefaultQuotationNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        QuotationSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationSettings", t => t.QuotationSettings_CompanyID)
                .Index(t => t.QuotationSettings_CompanyID);
            
            CreateTable(
                "dbo.SageExportSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        ExchangeRateGBP = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ExchangeRateUSD = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ExchangeRateEUR = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SubscriptionPlans",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Created = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        InitialAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        RegularAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AmountPerUser = c.Decimal(nullable: false, precision: 28, scale: 5),
                        NumberOfUsers = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Trial = c.Boolean(nullable: false),
                        AccessCRM = c.Boolean(nullable: false),
                        AccessProducts = c.Boolean(nullable: false),
                        AccessQuotations = c.Boolean(nullable: false),
                        AccessJobs = c.Boolean(nullable: false),
                        AccessInvoices = c.Boolean(nullable: false),
                        AccessDeliveryNotes = c.Boolean(nullable: false),
                        AccessPurchaseOrders = c.Boolean(nullable: false),
                        AccessStockControl = c.Boolean(nullable: false),
                        FuturePayID = c.String(),
                        InternalCartID = c.Int(nullable: false),
                        CartID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.TeamMembers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        TeamID = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Teams", t => t.TeamID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.TeamID)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DeliveryNoteItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        JobItemID = c.Guid(),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNotes", t => t.DeliveryNoteID)
                .ForeignKey("dbo.JobItems", t => t.JobItemID)
                .ForeignKey("dbo.DeliveryNoteItems", t => t.ParentItemID)
                .ForeignKey("dbo.DeliveryNoteSections", t => t.SectionID)
                .Index(t => t.DeliveryNoteID)
                .Index(t => t.SectionID)
                .Index(t => t.JobItemID)
                .Index(t => t.ParentItemID);
            
            CreateTable(
                "dbo.DeliveryNoteItemCollectLocations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteItemID = c.Guid(nullable: false),
                        StockLocationID = c.Guid(nullable: false),
                        LocationName = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNoteItems", t => t.DeliveryNoteItemID)
                .ForeignKey("dbo.StockLocations", t => t.StockLocationID)
                .Index(t => t.DeliveryNoteItemID)
                .Index(t => t.StockLocationID);
            
            CreateTable(
                "dbo.StockLocations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ParentLocationID = c.Guid(),
                        Name = c.String(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.StockLocations", t => t.ParentLocationID)
                .Index(t => t.CompanyID)
                .Index(t => t.ParentLocationID);
            
            CreateTable(
                "dbo.DeliveryNoteSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNotes", t => t.DeliveryNoteID)
                .Index(t => t.DeliveryNoteID);
            
            CreateTable(
                "dbo.DeliveryNoteNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryNotes", t => t.DeliveryNoteID)
                .Index(t => t.DeliveryNoteID);
            
            CreateTable(
                "dbo.DeliveryNoteStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.ClientContacts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Position = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.ClientTagItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        ClientTagID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientTags", t => t.ClientTagID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientTagID);
            
            CreateTable(
                "dbo.ClientTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ModuleTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        IndexURL = c.String(),
                        EditURL = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmailSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Email = c.String(),
                        Password = c.String(),
                        Host = c.String(),
                        Port = c.Int(nullable: false),
                        EnableSSL = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.ProductStockLocations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        StockLocationID = c.Guid(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.StockLocations", t => t.StockLocationID)
                .Index(t => t.CompanyID)
                .Index(t => t.StockLocationID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.StockLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        ProductCode = c.String(),
                        ProductDescription = c.String(),
                        LocationName = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ActionType = c.Int(nullable: false),
                        Narrative = c.String(),
                        ActivityByUserID = c.String(maxLength: 128),
                        ModuleTypeID = c.Int(nullable: false),
                        LinkID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ActivityByUserID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID)
                .Index(t => t.ActivityByUserID);
            
            CreateTable(
                "dbo.SubscriptionCartItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        AccessCRM = c.Boolean(nullable: false),
                        AccessProducts = c.Boolean(nullable: false),
                        AccessQuotations = c.Boolean(nullable: false),
                        AccessJobs = c.Boolean(nullable: false),
                        AccessInvoices = c.Boolean(nullable: false),
                        AccessDeliveryNotes = c.Boolean(nullable: false),
                        AccessPurchaseOrders = c.Boolean(nullable: false),
                        AccessStockControl = c.Boolean(nullable: false),
                        InitialAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        RegularAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AmountPerUser = c.Decimal(nullable: false, precision: 28, scale: 5),
                        NumberOfUsers = c.Int(nullable: false),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SubscriptionPlanPayments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Description = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Currency = c.String(maxLength: 3),
                        FuturePayID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SubscriptionRequests",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Email = c.String(nullable: false),
                        CompanyName = c.String(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierDeliveryItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierDeliveryNoteID = c.Guid(nullable: false),
                        PurchaseOrderItemID = c.Guid(nullable: false),
                        QuantityReceived = c.Decimal(nullable: false, precision: 28, scale: 5),
                        NetValue = c.Double(nullable: false),
                        PurchaseOrder_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrder_ID)
                .ForeignKey("dbo.PurchaseOrderItems", t => t.PurchaseOrderItemID)
                .ForeignKey("dbo.SupplierDeliveryNotes", t => t.SupplierDeliveryNoteID)
                .Index(t => t.SupplierDeliveryNoteID)
                .Index(t => t.PurchaseOrderItemID)
                .Index(t => t.PurchaseOrder_ID);
            
            CreateTable(
                "dbo.SupplierDeliveryNotes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        NoteNumber = c.String(),
                        PurchaseOrderID = c.Guid(nullable: false),
                        DefaultStockLocationID = c.Guid(nullable: false),
                        SupplierInvoiceID = c.Guid(),
                        Note = c.String(),
                        Created = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.StockLocations", t => t.DefaultStockLocationID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .ForeignKey("dbo.SupplierInvoices", t => t.SupplierInvoiceID)
                .Index(t => t.PurchaseOrderID)
                .Index(t => t.DefaultStockLocationID)
                .Index(t => t.SupplierInvoiceID);
            
            CreateTable(
                "dbo.SupplierInvoices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        SupplierDelieveryNoteID = c.Guid(),
                        TotalNet = c.Double(nullable: false),
                        TotalVat = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID)
                .Index(t => t.PurchaseOrderID);
            
            CreateTable(
                "dbo.WorldPayResponses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        InstID = c.String(),
                        CartID = c.String(),
                        Desc = c.String(),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Amount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AmountString = c.String(),
                        Currency = c.String(maxLength: 3),
                        AuthMode = c.String(),
                        TestMode = c.String(),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        Region = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        CountryString = c.String(),
                        Tel = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        DelvName = c.String(),
                        DelvAddress1 = c.String(),
                        DelvAddress2 = c.String(),
                        DelvAddress3 = c.String(),
                        DelvTown = c.String(),
                        DelvRegion = c.String(),
                        DelvPostcode = c.String(),
                        DelvCountry = c.String(),
                        DelvCountryString = c.String(),
                        CompName = c.String(),
                        TransID = c.String(),
                        TransStatus = c.String(),
                        TransTime = c.String(),
                        AuthAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AuthCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AuthCurrency = c.String(),
                        AuthAmountString = c.String(),
                        RawAuthMessage = c.String(),
                        RawAuthCode = c.String(),
                        CallbackPW = c.String(),
                        CardType = c.String(),
                        CountryMatch = c.String(),
                        AVS = c.String(),
                        WafMerchMessage = c.String(),
                        Authentication = c.String(),
                        IpAddress = c.String(),
                        Charenc = c.String(),
                        _spCharEnc = c.String(),
                        FuturePayID = c.String(),
                        FuturePayStatusChange = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierDeliveryItems", "SupplierDeliveryNoteID", "dbo.SupplierDeliveryNotes");
            DropForeignKey("dbo.SupplierDeliveryNotes", "SupplierInvoiceID", "dbo.SupplierInvoices");
            DropForeignKey("dbo.SupplierInvoices", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.SupplierDeliveryNotes", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.SupplierDeliveryNotes", "DefaultStockLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.SupplierDeliveryItems", "PurchaseOrderItemID", "dbo.PurchaseOrderItems");
            DropForeignKey("dbo.SupplierDeliveryItems", "PurchaseOrder_ID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.SubscriptionPlanPayments", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SubscriptionCartItems", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.StockLogs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.StockLogs", "ActivityByUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ProductStockLocations", "StockLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.ProductStockLocations", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ProductCategories", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ActivityLogs", "ModuleTypeID", "dbo.ModuleTypes");
            DropForeignKey("dbo.ActivityLogs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientEvents", "CreatorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientEvents", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientTypes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Clients", "ClientTypeID", "dbo.ClientTypes");
            DropForeignKey("dbo.ClientTagItems", "ClientTagID", "dbo.ClientTags");
            DropForeignKey("dbo.ClientTagItems", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "ClientContactID", "dbo.ClientContacts");
            DropForeignKey("dbo.ClientContacts", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientAttachments", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.DeliveryNotes", "StatusID", "dbo.DeliveryNoteStatus");
            DropForeignKey("dbo.DeliveryNoteNotifications", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNoteItems", "SectionID", "dbo.DeliveryNoteSections");
            DropForeignKey("dbo.DeliveryNoteSections", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNoteItems", "ParentItemID", "dbo.DeliveryNoteItems");
            DropForeignKey("dbo.DeliveryNoteItems", "JobItemID", "dbo.JobItems");
            DropForeignKey("dbo.DeliveryNoteItems", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNoteItemCollectLocations", "StockLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.StockLocations", "ParentLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.StockLocations", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNoteItemCollectLocations", "DeliveryNoteItemID", "dbo.DeliveryNoteItems");
            DropForeignKey("dbo.DeliveryNotes", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.TeamMembers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TeamMembers", "TeamID", "dbo.Teams");
            DropForeignKey("dbo.Teams", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SubscriptionPlans", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SageExportSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultQuotationNotifications", "QuotationSettings_CompanyID", "dbo.QuotationSettings");
            DropForeignKey("dbo.QuotationSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultQuotationAttachments", "QuotationSettings_CompanyID", "dbo.QuotationSettings");
            DropForeignKey("dbo.QuotationSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultPurchaseOrderNotifications", "PurchaseOrderSettings_CompanyID", "dbo.PurchaseOrderSettings");
            DropForeignKey("dbo.PurchaseOrderSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultPurchaseOrderAttachments", "PurchaseOrderSettings_CompanyID", "dbo.PurchaseOrderSettings");
            DropForeignKey("dbo.PurchaseOrderSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrderPreviews", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseOrders", "StatusID", "dbo.PurchaseOrderStatus");
            DropForeignKey("dbo.PurchaseOrders", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.PurchaseOrderNotifications", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.PurchaseOrderSections", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderItems", "SectionID", "dbo.PurchaseOrderSections");
            DropForeignKey("dbo.PurchaseOrderItems", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "InvoiceAddressID", "dbo.CompanyAddresses");
            DropForeignKey("dbo.PurchaseOrders", "DeliveryAddressID", "dbo.CompanyAddresses");
            DropForeignKey("dbo.PurchaseOrders", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.PurchaseOrders", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrderComments", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "ClientDeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.PurchaseOrders", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.PurchaseOrderAttachments", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrderPreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrderPreviews", "FrontTemplateID", "dbo.PurchaseOrderTemplates");
            DropForeignKey("dbo.PurchaseOrderTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrderPreviews", "BodyTemplateID", "dbo.PurchaseOrderTemplates");
            DropForeignKey("dbo.PurchaseOrderPreviews", "BackTemplateID", "dbo.PurchaseOrderTemplates");
            DropForeignKey("dbo.Suppliers", "SupplierTypeID", "dbo.SupplierTypes");
            DropForeignKey("dbo.SupplierTypes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SupplierProducts", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierContacts", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierAttachments", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierAddresses", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.Suppliers", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SupplierProducts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductPrices", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductPrices", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductKitItems", "Product_ID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "KitID", "dbo.Products");
            DropForeignKey("dbo.Products", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductCosts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductCosts", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Products", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultJobNotifications", "JobSettings_CompanyID", "dbo.JobSettings");
            DropForeignKey("dbo.JobSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultJobAttachments", "JobSettings_CompanyID", "dbo.JobSettings");
            DropForeignKey("dbo.JobSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultInvoiceNotifications", "InvoiceSettings_CompanyID", "dbo.InvoiceSettings");
            DropForeignKey("dbo.InvoiceSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultInvoiceAttachments", "InvoiceSettings_CompanyID", "dbo.InvoiceSettings");
            DropForeignKey("dbo.InvoiceSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InvoicePreviews", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "StatusID", "dbo.InvoiceStatus");
            DropForeignKey("dbo.InvoiceNotifications", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Jobs", "StatusID", "dbo.JobStatus");
            DropForeignKey("dbo.Quotations", "StatusID", "dbo.QuotationStatus");
            DropForeignKey("dbo.QuotationPreviews", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationPreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationPreviews", "FrontTemplateID", "dbo.QuotationTemplates");
            DropForeignKey("dbo.QuotationTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationPreviews", "BodyTemplateID", "dbo.QuotationTemplates");
            DropForeignKey("dbo.QuotationPreviews", "BackTemplateID", "dbo.QuotationTemplates");
            DropForeignKey("dbo.QuotationNotifications", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Jobs", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationSections", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationItems", "SectionID", "dbo.QuotationSections");
            DropForeignKey("dbo.QuotationItems", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationItems", "ParentItemID", "dbo.QuotationItems");
            DropForeignKey("dbo.Invoices", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "InvoiceAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Quotations", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Quotations", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Quotations", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationComments", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.QuotationAttachments", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.JobPreviews", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobPreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.JobPreviews", "FrontTemplateID", "dbo.JobTemplates");
            DropForeignKey("dbo.JobTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.JobPreviews", "BodyTemplateID", "dbo.JobTemplates");
            DropForeignKey("dbo.JobPreviews", "BackTemplateID", "dbo.JobTemplates");
            DropForeignKey("dbo.JobNotifications", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobSections", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobItems", "SectionID", "dbo.JobSections");
            DropForeignKey("dbo.JobItems", "ParentItemID", "dbo.JobItems");
            DropForeignKey("dbo.JobItems", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Invoices", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "InvoiceAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.DeliveryNotes", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Jobs", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Jobs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.JobComments", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.JobAttachments", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.InvoiceItems", "SectionID", "dbo.InvoiceSections");
            DropForeignKey("dbo.InvoiceSections", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.InvoiceItems", "ParentItemID", "dbo.InvoiceItems");
            DropForeignKey("dbo.InvoiceItems", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "InvoiceAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Invoices", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.Invoices", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Invoices", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Invoices", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InvoiceComments", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.InvoiceAttachments", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.InvoicePreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InvoicePreviews", "FrontTemplateID", "dbo.InvoiceTemplates");
            DropForeignKey("dbo.InvoiceTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InvoicePreviews", "BodyTemplateID", "dbo.InvoiceTemplates");
            DropForeignKey("dbo.InvoicePreviews", "BackTemplateID", "dbo.InvoiceTemplates");
            DropForeignKey("dbo.HierarchyMembers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.HierarchyMembers", "ReportsToID", "dbo.HierarchyMembers");
            DropForeignKey("dbo.HierarchyMembers", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultDeliveryNoteNotifications", "DeliveryNoteSettings_CompanyID", "dbo.DeliveryNoteSettings");
            DropForeignKey("dbo.DefaultDeliveryNoteAttachments", "DeliveryNoteSettings_CompanyID", "dbo.DeliveryNoteSettings");
            DropForeignKey("dbo.DeliveryNoteSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotePreviews", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNotePreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotePreviews", "FrontTemplateID", "dbo.DeliveryNoteTemplates");
            DropForeignKey("dbo.DeliveryNoteTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotePreviews", "BodyTemplateID", "dbo.DeliveryNoteTemplates");
            DropForeignKey("dbo.DeliveryNotePreviews", "BackTemplateID", "dbo.DeliveryNoteTemplates");
            DropForeignKey("dbo.Companies", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ClientTags", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Clients", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.AspNetUsers", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.CompanyAddresses", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNoteComments", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNotes", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.DeliveryNoteAttachments", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNotes", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientAddresses", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ActivityLogs", "ActivityByUserID", "dbo.AspNetUsers");
            DropIndex("dbo.SupplierInvoices", new[] { "PurchaseOrderID" });
            DropIndex("dbo.SupplierDeliveryNotes", new[] { "SupplierInvoiceID" });
            DropIndex("dbo.SupplierDeliveryNotes", new[] { "DefaultStockLocationID" });
            DropIndex("dbo.SupplierDeliveryNotes", new[] { "PurchaseOrderID" });
            DropIndex("dbo.SupplierDeliveryItems", new[] { "PurchaseOrder_ID" });
            DropIndex("dbo.SupplierDeliveryItems", new[] { "PurchaseOrderItemID" });
            DropIndex("dbo.SupplierDeliveryItems", new[] { "SupplierDeliveryNoteID" });
            DropIndex("dbo.SubscriptionPlanPayments", new[] { "CompanyID" });
            DropIndex("dbo.SubscriptionCartItems", new[] { "CompanyID" });
            DropIndex("dbo.StockLogs", new[] { "ActivityByUserID" });
            DropIndex("dbo.StockLogs", new[] { "CompanyID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ProductStockLocations", new[] { "StockLocationID" });
            DropIndex("dbo.ProductStockLocations", new[] { "CompanyID" });
            DropIndex("dbo.ProductCategories", new[] { "CompanyID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.ClientTypes", new[] { "CompanyID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientTagID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientID" });
            DropIndex("dbo.ClientContacts", new[] { "ClientID" });
            DropIndex("dbo.ClientAttachments", new[] { "ClientID" });
            DropIndex("dbo.DeliveryNoteNotifications", new[] { "DeliveryNoteID" });
            DropIndex("dbo.DeliveryNoteSections", new[] { "DeliveryNoteID" });
            DropIndex("dbo.StockLocations", new[] { "ParentLocationID" });
            DropIndex("dbo.StockLocations", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNoteItemCollectLocations", new[] { "StockLocationID" });
            DropIndex("dbo.DeliveryNoteItemCollectLocations", new[] { "DeliveryNoteItemID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "ParentItemID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "JobItemID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "SectionID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "DeliveryNoteID" });
            DropIndex("dbo.TeamMembers", new[] { "UserId" });
            DropIndex("dbo.TeamMembers", new[] { "TeamID" });
            DropIndex("dbo.Teams", new[] { "CompanyID" });
            DropIndex("dbo.SubscriptionPlans", new[] { "CompanyID" });
            DropIndex("dbo.SageExportSettings", new[] { "CompanyID" });
            DropIndex("dbo.DefaultQuotationNotifications", new[] { "QuotationSettings_CompanyID" });
            DropIndex("dbo.DefaultQuotationAttachments", new[] { "QuotationSettings_CompanyID" });
            DropIndex("dbo.QuotationSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.QuotationSettings", new[] { "CompanyID" });
            DropIndex("dbo.DefaultPurchaseOrderNotifications", new[] { "PurchaseOrderSettings_CompanyID" });
            DropIndex("dbo.DefaultPurchaseOrderAttachments", new[] { "PurchaseOrderSettings_CompanyID" });
            DropIndex("dbo.PurchaseOrderSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.PurchaseOrderSettings", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrderNotifications", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderSections", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderItems", new[] { "SectionID" });
            DropIndex("dbo.PurchaseOrderItems", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderComments", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderAttachments", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrders", new[] { "ClientDeliveryAddressID" });
            DropIndex("dbo.PurchaseOrders", new[] { "DeliveryAddressID" });
            DropIndex("dbo.PurchaseOrders", new[] { "InvoiceAddressID" });
            DropIndex("dbo.PurchaseOrders", new[] { "JobID" });
            DropIndex("dbo.PurchaseOrders", new[] { "QuotationID" });
            DropIndex("dbo.PurchaseOrders", new[] { "CurrencyID" });
            DropIndex("dbo.PurchaseOrders", new[] { "StatusID" });
            DropIndex("dbo.PurchaseOrders", new[] { "AssignedUserID" });
            DropIndex("dbo.PurchaseOrders", new[] { "SupplierID" });
            DropIndex("dbo.PurchaseOrders", new[] { "ClientID" });
            DropIndex("dbo.PurchaseOrders", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrderTemplates", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "PurchaseOrderID" });
            DropIndex("dbo.SupplierTypes", new[] { "CompanyID" });
            DropIndex("dbo.SupplierContacts", new[] { "SupplierID" });
            DropIndex("dbo.SupplierAttachments", new[] { "SupplierID" });
            DropIndex("dbo.SupplierAddresses", new[] { "SupplierID" });
            DropIndex("dbo.Suppliers", new[] { "SupplierTypeID" });
            DropIndex("dbo.Suppliers", new[] { "CompanyID" });
            DropIndex("dbo.SupplierProducts", new[] { "SupplierID" });
            DropIndex("dbo.SupplierProducts", new[] { "ProductID" });
            DropIndex("dbo.ProductPrices", new[] { "CurrencyID" });
            DropIndex("dbo.ProductPrices", new[] { "ProductID" });
            DropIndex("dbo.ProductKitItems", new[] { "Product_ID" });
            DropIndex("dbo.ProductKitItems", new[] { "ProductID" });
            DropIndex("dbo.ProductKitItems", new[] { "KitID" });
            DropIndex("dbo.ProductCosts", new[] { "CurrencyID" });
            DropIndex("dbo.ProductCosts", new[] { "ProductID" });
            DropIndex("dbo.Products", new[] { "CurrencyID" });
            DropIndex("dbo.Products", new[] { "CompanyID" });
            DropIndex("dbo.DefaultJobNotifications", new[] { "JobSettings_CompanyID" });
            DropIndex("dbo.DefaultJobAttachments", new[] { "JobSettings_CompanyID" });
            DropIndex("dbo.JobSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.JobSettings", new[] { "CompanyID" });
            DropIndex("dbo.DefaultInvoiceNotifications", new[] { "InvoiceSettings_CompanyID" });
            DropIndex("dbo.DefaultInvoiceAttachments", new[] { "InvoiceSettings_CompanyID" });
            DropIndex("dbo.InvoiceSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.InvoiceSettings", new[] { "CompanyID" });
            DropIndex("dbo.InvoiceNotifications", new[] { "InvoiceID" });
            DropIndex("dbo.QuotationTemplates", new[] { "CompanyID" });
            DropIndex("dbo.QuotationPreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.QuotationPreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.QuotationPreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.QuotationPreviews", new[] { "CompanyID" });
            DropIndex("dbo.QuotationPreviews", new[] { "QuotationID" });
            DropIndex("dbo.QuotationNotifications", new[] { "QuotationID" });
            DropIndex("dbo.QuotationSections", new[] { "QuotationID" });
            DropIndex("dbo.QuotationItems", new[] { "ParentItemID" });
            DropIndex("dbo.QuotationItems", new[] { "SectionID" });
            DropIndex("dbo.QuotationItems", new[] { "QuotationID" });
            DropIndex("dbo.QuotationComments", new[] { "QuotationID" });
            DropIndex("dbo.QuotationAttachments", new[] { "QuotationID" });
            DropIndex("dbo.Quotations", new[] { "DeliveryAddressID" });
            DropIndex("dbo.Quotations", new[] { "InvoiceAddressID" });
            DropIndex("dbo.Quotations", new[] { "CurrencyID" });
            DropIndex("dbo.Quotations", new[] { "StatusID" });
            DropIndex("dbo.Quotations", new[] { "AssignedUserID" });
            DropIndex("dbo.Quotations", new[] { "ClientID" });
            DropIndex("dbo.Quotations", new[] { "CompanyID" });
            DropIndex("dbo.JobTemplates", new[] { "CompanyID" });
            DropIndex("dbo.JobPreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.JobPreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.JobPreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.JobPreviews", new[] { "CompanyID" });
            DropIndex("dbo.JobPreviews", new[] { "JobID" });
            DropIndex("dbo.JobNotifications", new[] { "JobID" });
            DropIndex("dbo.JobSections", new[] { "JobID" });
            DropIndex("dbo.JobItems", new[] { "ParentItemID" });
            DropIndex("dbo.JobItems", new[] { "SectionID" });
            DropIndex("dbo.JobItems", new[] { "JobID" });
            DropIndex("dbo.JobComments", new[] { "JobID" });
            DropIndex("dbo.JobAttachments", new[] { "JobID" });
            DropIndex("dbo.Jobs", new[] { "DeliveryAddressID" });
            DropIndex("dbo.Jobs", new[] { "InvoiceAddressID" });
            DropIndex("dbo.Jobs", new[] { "QuotationID" });
            DropIndex("dbo.Jobs", new[] { "CurrencyID" });
            DropIndex("dbo.Jobs", new[] { "StatusID" });
            DropIndex("dbo.Jobs", new[] { "AssignedUserID" });
            DropIndex("dbo.Jobs", new[] { "ClientID" });
            DropIndex("dbo.Jobs", new[] { "CompanyID" });
            DropIndex("dbo.InvoiceSections", new[] { "InvoiceID" });
            DropIndex("dbo.InvoiceItems", new[] { "ParentItemID" });
            DropIndex("dbo.InvoiceItems", new[] { "SectionID" });
            DropIndex("dbo.InvoiceItems", new[] { "InvoiceID" });
            DropIndex("dbo.InvoiceComments", new[] { "InvoiceID" });
            DropIndex("dbo.InvoiceAttachments", new[] { "InvoiceID" });
            DropIndex("dbo.Invoices", new[] { "DeliveryAddressID" });
            DropIndex("dbo.Invoices", new[] { "InvoiceAddressID" });
            DropIndex("dbo.Invoices", new[] { "DeliveryNoteID" });
            DropIndex("dbo.Invoices", new[] { "JobID" });
            DropIndex("dbo.Invoices", new[] { "QuotationID" });
            DropIndex("dbo.Invoices", new[] { "CurrencyID" });
            DropIndex("dbo.Invoices", new[] { "StatusID" });
            DropIndex("dbo.Invoices", new[] { "AssignedUserID" });
            DropIndex("dbo.Invoices", new[] { "ClientID" });
            DropIndex("dbo.Invoices", new[] { "CompanyID" });
            DropIndex("dbo.InvoiceTemplates", new[] { "CompanyID" });
            DropIndex("dbo.InvoicePreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.InvoicePreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.InvoicePreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.InvoicePreviews", new[] { "CompanyID" });
            DropIndex("dbo.InvoicePreviews", new[] { "InvoiceID" });
            DropIndex("dbo.HierarchyMembers", new[] { "ReportsToID" });
            DropIndex("dbo.HierarchyMembers", new[] { "UserId" });
            DropIndex("dbo.HierarchyMembers", new[] { "CompanyID" });
            DropIndex("dbo.DefaultDeliveryNoteNotifications", new[] { "DeliveryNoteSettings_CompanyID" });
            DropIndex("dbo.DefaultDeliveryNoteAttachments", new[] { "DeliveryNoteSettings_CompanyID" });
            DropIndex("dbo.DeliveryNoteSettings", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNoteTemplates", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "DeliveryNoteID" });
            DropIndex("dbo.ClientTags", new[] { "CompanyID" });
            DropIndex("dbo.CompanyAddresses", new[] { "CompanyID" });
            DropIndex("dbo.Companies", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.DeliveryNoteComments", new[] { "DeliveryNoteID" });
            DropIndex("dbo.DeliveryNoteAttachments", new[] { "DeliveryNoteID" });
            DropIndex("dbo.DeliveryNotes", new[] { "DeliveryAddressID" });
            DropIndex("dbo.DeliveryNotes", new[] { "JobID" });
            DropIndex("dbo.DeliveryNotes", new[] { "StatusID" });
            DropIndex("dbo.DeliveryNotes", new[] { "AssignedUserID" });
            DropIndex("dbo.DeliveryNotes", new[] { "ClientID" });
            DropIndex("dbo.DeliveryNotes", new[] { "CompanyID" });
            DropIndex("dbo.ClientAddresses", new[] { "ClientID" });
            DropIndex("dbo.Clients", new[] { "ClientTypeID" });
            DropIndex("dbo.Clients", new[] { "CompanyID" });
            DropIndex("dbo.ClientEvents", new[] { "ClientContactID" });
            DropIndex("dbo.ClientEvents", new[] { "ClientID" });
            DropIndex("dbo.ClientEvents", new[] { "CreatorID" });
            DropIndex("dbo.ClientEvents", new[] { "AssignedUserID" });
            DropIndex("dbo.ClientEvents", new[] { "CompanyID" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "CompanyID" });
            DropIndex("dbo.ActivityLogs", new[] { "ActivityByUserID" });
            DropIndex("dbo.ActivityLogs", new[] { "ModuleTypeID" });
            DropIndex("dbo.ActivityLogs", new[] { "CompanyID" });
            DropTable("dbo.WorldPayResponses");
            DropTable("dbo.SupplierInvoices");
            DropTable("dbo.SupplierDeliveryNotes");
            DropTable("dbo.SupplierDeliveryItems");
            DropTable("dbo.SubscriptionRequests");
            DropTable("dbo.SubscriptionPlanPayments");
            DropTable("dbo.SubscriptionCartItems");
            DropTable("dbo.StockLogs");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.ProductStockLocations");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.EmailSettings");
            DropTable("dbo.ModuleTypes");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.ClientTypes");
            DropTable("dbo.ClientTagItems");
            DropTable("dbo.ClientContacts");
            DropTable("dbo.ClientAttachments");
            DropTable("dbo.DeliveryNoteStatus");
            DropTable("dbo.DeliveryNoteNotifications");
            DropTable("dbo.DeliveryNoteSections");
            DropTable("dbo.StockLocations");
            DropTable("dbo.DeliveryNoteItemCollectLocations");
            DropTable("dbo.DeliveryNoteItems");
            DropTable("dbo.TeamMembers");
            DropTable("dbo.Teams");
            DropTable("dbo.SubscriptionPlans");
            DropTable("dbo.SageExportSettings");
            DropTable("dbo.DefaultQuotationNotifications");
            DropTable("dbo.DefaultQuotationAttachments");
            DropTable("dbo.QuotationSettings");
            DropTable("dbo.DefaultPurchaseOrderNotifications");
            DropTable("dbo.DefaultPurchaseOrderAttachments");
            DropTable("dbo.PurchaseOrderSettings");
            DropTable("dbo.PurchaseOrderStatus");
            DropTable("dbo.PurchaseOrderNotifications");
            DropTable("dbo.PurchaseOrderSections");
            DropTable("dbo.PurchaseOrderItems");
            DropTable("dbo.PurchaseOrderComments");
            DropTable("dbo.PurchaseOrderAttachments");
            DropTable("dbo.PurchaseOrders");
            DropTable("dbo.PurchaseOrderTemplates");
            DropTable("dbo.PurchaseOrderPreviews");
            DropTable("dbo.SupplierTypes");
            DropTable("dbo.SupplierContacts");
            DropTable("dbo.SupplierAttachments");
            DropTable("dbo.SupplierAddresses");
            DropTable("dbo.Suppliers");
            DropTable("dbo.SupplierProducts");
            DropTable("dbo.ProductPrices");
            DropTable("dbo.ProductKitItems");
            DropTable("dbo.ProductCosts");
            DropTable("dbo.Products");
            DropTable("dbo.DefaultJobNotifications");
            DropTable("dbo.DefaultJobAttachments");
            DropTable("dbo.JobSettings");
            DropTable("dbo.DefaultInvoiceNotifications");
            DropTable("dbo.DefaultInvoiceAttachments");
            DropTable("dbo.InvoiceSettings");
            DropTable("dbo.InvoiceStatus");
            DropTable("dbo.InvoiceNotifications");
            DropTable("dbo.JobStatus");
            DropTable("dbo.QuotationStatus");
            DropTable("dbo.QuotationTemplates");
            DropTable("dbo.QuotationPreviews");
            DropTable("dbo.QuotationNotifications");
            DropTable("dbo.QuotationSections");
            DropTable("dbo.QuotationItems");
            DropTable("dbo.QuotationComments");
            DropTable("dbo.QuotationAttachments");
            DropTable("dbo.Quotations");
            DropTable("dbo.JobTemplates");
            DropTable("dbo.JobPreviews");
            DropTable("dbo.JobNotifications");
            DropTable("dbo.JobSections");
            DropTable("dbo.JobItems");
            DropTable("dbo.JobComments");
            DropTable("dbo.JobAttachments");
            DropTable("dbo.Jobs");
            DropTable("dbo.InvoiceSections");
            DropTable("dbo.InvoiceItems");
            DropTable("dbo.InvoiceComments");
            DropTable("dbo.InvoiceAttachments");
            DropTable("dbo.Invoices");
            DropTable("dbo.InvoiceTemplates");
            DropTable("dbo.InvoicePreviews");
            DropTable("dbo.HierarchyMembers");
            DropTable("dbo.DefaultDeliveryNoteNotifications");
            DropTable("dbo.DefaultDeliveryNoteAttachments");
            DropTable("dbo.DeliveryNoteSettings");
            DropTable("dbo.DeliveryNoteTemplates");
            DropTable("dbo.DeliveryNotePreviews");
            DropTable("dbo.Currencies");
            DropTable("dbo.ClientTags");
            DropTable("dbo.CompanyAddresses");
            DropTable("dbo.Companies");
            DropTable("dbo.DeliveryNoteComments");
            DropTable("dbo.DeliveryNoteAttachments");
            DropTable("dbo.DeliveryNotes");
            DropTable("dbo.ClientAddresses");
            DropTable("dbo.Clients");
            DropTable("dbo.ClientEvents");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ActivityLogs");
        }
    }
}
