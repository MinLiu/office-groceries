﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDpdFruitAmendments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "DpdFruitAmendments", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "DpdFruitAmendments");
        }
    }
}
