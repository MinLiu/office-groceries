namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPageSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PageSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Label = c.String(),
                        FilePath = c.String(),
                        MetaTitle = c.String(),
                        MetaKeyword = c.String(),
                        MetaDescription = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PageSettings");
        }
    }
}
