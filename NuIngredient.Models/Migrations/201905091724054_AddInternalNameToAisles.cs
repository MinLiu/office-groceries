namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInternalNameToAisles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aisles", "InternalName", c => c.String());
            AddColumn("dbo.ProductCategories", "InternalName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductCategories", "InternalName");
            DropColumn("dbo.Aisles", "InternalName");
        }
    }
}
