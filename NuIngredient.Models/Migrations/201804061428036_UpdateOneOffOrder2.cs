namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOneOffOrder2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OneOffOrders", "SupplierID", c => c.Guid());
            CreateIndex("dbo.OneOffOrders", "SupplierID");
            AddForeignKey("dbo.OneOffOrders", "SupplierID", "dbo.Suppliers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OneOffOrders", "SupplierID", "dbo.Suppliers");
            DropIndex("dbo.OneOffOrders", new[] { "SupplierID" });
            DropColumn("dbo.OneOffOrders", "SupplierID");
        }
    }
}
