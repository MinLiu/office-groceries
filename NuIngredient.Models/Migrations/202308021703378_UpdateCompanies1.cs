﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCompanies1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "InternalDeliveryInstruction", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "InternalDeliveryInstruction");
        }
    }
}
