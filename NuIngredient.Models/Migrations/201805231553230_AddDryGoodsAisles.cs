namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDryGoodsAisles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DryGoodsAisles",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AisleID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Aisles", t => t.AisleID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.AisleID)
                .Index(t => t.ProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DryGoodsAisles", "ProductID", "dbo.Products");
            DropForeignKey("dbo.DryGoodsAisles", "AisleID", "dbo.Aisles");
            DropIndex("dbo.DryGoodsAisles", new[] { "ProductID" });
            DropIndex("dbo.DryGoodsAisles", new[] { "AisleID" });
            DropTable("dbo.DryGoodsAisles");
        }
    }
}
