namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCompanySupplier : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CompanySuppliers", "StartDate", c => c.DateTime());
            AlterColumn("dbo.CompanySuppliers", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CompanySuppliers", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CompanySuppliers", "StartDate", c => c.DateTime(nullable: false));
        }
    }
}
