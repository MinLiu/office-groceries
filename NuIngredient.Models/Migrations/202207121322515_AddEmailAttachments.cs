﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailAttachments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ReferenceID = c.Guid(nullable: false),
                        FileName = c.String(),
                        FilePath = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.ReferenceID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.EmailAttachments", new[] { "ReferenceID" });
            DropTable("dbo.EmailAttachments");
        }
    }
}
