namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductCatetory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCategories", "AisleID", c => c.Guid());
            CreateIndex("dbo.ProductCategories", "AisleID");
            AddForeignKey("dbo.ProductCategories", "AisleID", "dbo.Aisles", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductCategories", "AisleID", "dbo.Aisles");
            DropIndex("dbo.ProductCategories", new[] { "AisleID" });
            DropColumn("dbo.ProductCategories", "AisleID");
        }
    }
}
