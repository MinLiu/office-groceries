namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addInvoiceHeader : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitInvoiceHeaders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        CompanyID = c.Guid(nullable: false),
                        FromDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            AddColumn("dbo.FruitInvoices", "FruitInvoiceHeaderID", c => c.Guid(nullable: false));
            CreateIndex("dbo.FruitInvoices", "FruitInvoiceHeaderID");
            AddForeignKey("dbo.FruitInvoices", "FruitInvoiceHeaderID", "dbo.FruitInvoiceHeaders", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitInvoices", "FruitInvoiceHeaderID", "dbo.FruitInvoiceHeaders");
            DropForeignKey("dbo.FruitInvoiceHeaders", "CompanyID", "dbo.Companies");
            DropIndex("dbo.FruitInvoiceHeaders", new[] { "CompanyID" });
            DropIndex("dbo.FruitInvoices", new[] { "FruitInvoiceHeaderID" });
            DropColumn("dbo.FruitInvoices", "FruitInvoiceHeaderID");
            DropTable("dbo.FruitInvoiceHeaders");
        }
    }
}
