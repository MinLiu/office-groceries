﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMonthlyPoNumbers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MonthlyPoNumbers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        InvoiceMonth = c.DateTime(nullable: false),
                        InvoiceNumber = c.String(),
                        InvoiceAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        PoNumber = c.String(),
                        IsExported = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MonthlyPoNumbers", "CompanyID", "dbo.Companies");
            DropIndex("dbo.MonthlyPoNumbers", new[] { "CompanyID" });
            DropTable("dbo.MonthlyPoNumbers");
        }
    }
}
