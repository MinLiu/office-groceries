﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDryGoodOrderNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OneOffOrders", "OrderNumberSequence", c => c.Int(nullable: false));
            AddColumn("dbo.Settings", "DryGoodOrderNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.OneOffOrders", "OrderNumber", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.OneOffOrders", "OrderNumber", c => c.String());
            DropColumn("dbo.Settings", "DryGoodOrderNumber");
            DropColumn("dbo.OneOffOrders", "OrderNumberSequence");
        }
    }
}
