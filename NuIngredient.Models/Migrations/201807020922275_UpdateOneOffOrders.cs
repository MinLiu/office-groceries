namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOneOffOrders : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OneOffOrders", "TotalVAT", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            RenameColumn("dbo.OneOffOrders", "Total", "TotalNet");
            
        }
        
        public override void Down()
        {
            RenameColumn("dbo.OneOffOrders", "TotalNet", "Total");
            DropColumn("dbo.OneOffOrders", "TotalVAT");
        }
    }
}
