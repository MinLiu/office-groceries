namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAisleMoreInfoContents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AisleMoreInfoContents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AisleID = c.Guid(nullable: false),
                        Label = c.String(),
                        Content = c.String(),
                        SortPosition = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Aisles", t => t.AisleID)
                .Index(t => t.AisleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AisleMoreInfoContents", "AisleID", "dbo.Aisles");
            DropIndex("dbo.AisleMoreInfoContents", new[] { "AisleID" });
            DropTable("dbo.AisleMoreInfoContents");
        }
    }
}
