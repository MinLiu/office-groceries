namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFlagForEmailSent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "TransferEmailSent", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "TransferEmailSent");
        }
    }
}
