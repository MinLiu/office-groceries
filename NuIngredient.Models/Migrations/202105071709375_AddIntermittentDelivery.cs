namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIntermittentDelivery : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanySuppliers", "IsIntermittentDelivery", c => c.Boolean(nullable: false));
            AddColumn("dbo.CompanySuppliers", "DeliveryFrequency", c => c.Int());
            AddColumn("dbo.CompanySuppliers", "NextDeliveryWeek", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanySuppliers", "NextDeliveryWeek");
            DropColumn("dbo.CompanySuppliers", "DeliveryFrequency");
            DropColumn("dbo.CompanySuppliers", "IsIntermittentDelivery");
        }
    }
}
