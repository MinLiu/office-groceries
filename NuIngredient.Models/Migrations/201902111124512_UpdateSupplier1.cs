namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSupplier1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "CalendarNeeded", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "CalendarNeeded");
        }
    }
}
