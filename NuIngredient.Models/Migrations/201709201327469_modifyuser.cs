namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyuser : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "AccessCRM");
            DropColumn("dbo.AspNetUsers", "AccessProducts");
            DropColumn("dbo.AspNetUsers", "AccessQuotations");
            DropColumn("dbo.AspNetUsers", "AccessJobs");
            DropColumn("dbo.AspNetUsers", "AccessInvoices");
            DropColumn("dbo.AspNetUsers", "AccessDeliveryNotes");
            DropColumn("dbo.AspNetUsers", "AccessPurchaseOrders");
            DropColumn("dbo.AspNetUsers", "AccessStockControl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "AccessStockControl", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccessPurchaseOrders", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccessDeliveryNotes", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccessInvoices", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccessJobs", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccessQuotations", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccessProducts", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "AccessCRM", c => c.Boolean(nullable: false));
        }
    }
}
