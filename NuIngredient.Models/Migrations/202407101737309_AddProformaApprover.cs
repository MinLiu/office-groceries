﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProformaApprover : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProformaApprovers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Companies", "ProformaApproverID", c => c.Guid());
            CreateIndex("dbo.Companies", "ProformaApproverID");
            AddForeignKey("dbo.Companies", "ProformaApproverID", "dbo.ProformaApprovers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "ProformaApproverID", "dbo.ProformaApprovers");
            DropIndex("dbo.Companies", new[] { "ProformaApproverID" });
            DropColumn("dbo.Companies", "ProformaApproverID");
            DropTable("dbo.ProformaApprovers");
        }
    }
}
