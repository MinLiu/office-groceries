namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupAcctRequestLogs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupAcctRequestLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        RequestTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupAcctRequestLogs", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupAcctRequestLogs", "CompanyID", "dbo.Companies");
            DropIndex("dbo.SupAcctRequestLogs", new[] { "SupplierID" });
            DropIndex("dbo.SupAcctRequestLogs", new[] { "CompanyID" });
            DropTable("dbo.SupAcctRequestLogs");
        }
    }
}
