namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEnquiry : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Enquiries",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        BusinessName = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        InterestOfMilk = c.Boolean(nullable: false),
                        InterestOfFruits = c.Boolean(nullable: false),
                        InterestOfDryGoods = c.Boolean(nullable: false),
                        Note = c.String(),
                        NoGo = c.Boolean(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Enquiries");
        }
    }
}
