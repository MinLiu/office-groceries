namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProspect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enquiries", "Postcode", c => c.String());
            AddColumn("dbo.Prospects", "Postcode", c => c.String());
            AddColumn("dbo.Prospects", "Note", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "Note");
            DropColumn("dbo.Prospects", "Postcode");
            DropColumn("dbo.Enquiries", "Postcode");
        }
    }
}
