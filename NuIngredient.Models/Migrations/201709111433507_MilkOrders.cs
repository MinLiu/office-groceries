namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MilkOrders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MilkOrders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductName = c.String(),
                        MilkID = c.Guid(nullable: false),
                        Monday = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Tuesday = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Wednesday = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Thursday = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Friday = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Saturday = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Sunday = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Milk", t => t.MilkID)
                .Index(t => t.MilkID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkOrders", "MilkID", "dbo.Milk");
            DropIndex("dbo.MilkOrders", new[] { "MilkID" });
            DropTable("dbo.MilkOrders");
        }
    }
}
