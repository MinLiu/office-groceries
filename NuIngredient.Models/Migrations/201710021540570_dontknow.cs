namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dontknow : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CompanySuppliers", "AccountNo", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CompanySuppliers", "AccountNo", c => c.String());
        }
    }
}
