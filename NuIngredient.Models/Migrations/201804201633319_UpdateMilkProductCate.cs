namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilkProductCate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitProductCategories", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.MilkProductCategories", "Deleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkProductCategories", "Deleted");
            DropColumn("dbo.FruitProductCategories", "Deleted");
        }
    }
}
