namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCarousel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carousels",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        productID = c.Guid(),
                        ImageURL = c.String(),
                        LinkURL = c.String(),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.productID)
                .Index(t => t.productID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Carousels", "productID", "dbo.Products");
            DropIndex("dbo.Carousels", new[] { "productID" });
            DropTable("dbo.Carousels");
        }
    }
}
