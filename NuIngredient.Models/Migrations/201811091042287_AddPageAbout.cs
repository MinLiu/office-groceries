namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPageAbout : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PageAbouts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        MetaTitle = c.String(),
                        MetaKeyword = c.String(),
                        MetaDescription = c.String(),
                        Title = c.String(),
                        Subtitle = c.String(),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PageAbouts");
        }
    }
}
