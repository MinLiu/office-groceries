﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProspect2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "CountQuoteEmail", c => c.Int(nullable: false));
            AddColumn("dbo.Prospects", "CountPromotionEmail", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "CountPromotionEmail");
            DropColumn("dbo.Prospects", "CountQuoteEmail");
        }
    }
}
