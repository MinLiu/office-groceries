namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNiComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Complaints", "NIComment", c => c.String());
            AddColumn("dbo.Complaints", "ReplierID", c => c.String(maxLength: 128));
            AddColumn("dbo.Complaints", "ResponseTime", c => c.DateTime());
            CreateIndex("dbo.Complaints", "ReplierID");
            AddForeignKey("dbo.Complaints", "ReplierID", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Complaints", "ReplierID", "dbo.AspNetUsers");
            DropIndex("dbo.Complaints", new[] { "ReplierID" });
            DropColumn("dbo.Complaints", "ResponseTime");
            DropColumn("dbo.Complaints", "ReplierID");
            DropColumn("dbo.Complaints", "NIComment");
        }
    }
}
