namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilkCategories : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MilkProductCategories", "AisleID");
            AddForeignKey("dbo.MilkProductCategories", "AisleID", "dbo.Aisles", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkProductCategories", "AisleID", "dbo.Aisles");
            DropIndex("dbo.MilkProductCategories", new[] { "AisleID" });
        }
    }
}
