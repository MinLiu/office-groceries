﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSecondaryEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "SecondaryEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "SecondaryEmail");
        }
    }
}
