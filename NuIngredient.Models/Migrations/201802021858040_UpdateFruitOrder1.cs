namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFruitOrder1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.FruitOrders", name: "FruitID", newName: "FruitProductID");
            RenameIndex(table: "dbo.FruitOrders", name: "IX_FruitID", newName: "IX_FruitProductID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.FruitOrders", name: "IX_FruitProductID", newName: "IX_FruitID");
            RenameColumn(table: "dbo.FruitOrders", name: "FruitProductID", newName: "FruitID");
        }
    }
}
