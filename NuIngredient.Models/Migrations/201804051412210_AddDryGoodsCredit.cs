namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDryGoodsCredit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DryGoodsCreditHeaders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        SupplierID = c.Guid(),
                        CreateByID = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        OrderHeaderID = c.Guid(nullable: false),
                        Narrative = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreateByID)
                .ForeignKey("dbo.OneOffOrders", t => t.OrderHeaderID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierID)
                .Index(t => t.CreateByID)
                .Index(t => t.OrderHeaderID);
            
            CreateTable(
                "dbo.DryGoodsCreditItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DryGoodsCreditHeaderID = c.Guid(nullable: false),
                        DryGoodsOrderItemID = c.Guid(nullable: false),
                        Qty = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DryGoodsCreditHeaders", t => t.DryGoodsCreditHeaderID)
                .ForeignKey("dbo.OneOffOrderItems", t => t.DryGoodsOrderItemID)
                .Index(t => t.DryGoodsCreditHeaderID)
                .Index(t => t.DryGoodsOrderItemID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DryGoodsCreditHeaders", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.DryGoodsCreditHeaders", "OrderHeaderID", "dbo.OneOffOrders");
            DropForeignKey("dbo.DryGoodsCreditItems", "DryGoodsOrderItemID", "dbo.OneOffOrderItems");
            DropForeignKey("dbo.DryGoodsCreditItems", "DryGoodsCreditHeaderID", "dbo.DryGoodsCreditHeaders");
            DropForeignKey("dbo.DryGoodsCreditHeaders", "CreateByID", "dbo.AspNetUsers");
            DropForeignKey("dbo.DryGoodsCreditHeaders", "CompanyID", "dbo.Companies");
            DropIndex("dbo.DryGoodsCreditItems", new[] { "DryGoodsOrderItemID" });
            DropIndex("dbo.DryGoodsCreditItems", new[] { "DryGoodsCreditHeaderID" });
            DropIndex("dbo.DryGoodsCreditHeaders", new[] { "OrderHeaderID" });
            DropIndex("dbo.DryGoodsCreditHeaders", new[] { "CreateByID" });
            DropIndex("dbo.DryGoodsCreditHeaders", new[] { "SupplierID" });
            DropIndex("dbo.DryGoodsCreditHeaders", new[] { "CompanyID" });
            DropTable("dbo.DryGoodsCreditItems");
            DropTable("dbo.DryGoodsCreditHeaders");
        }
    }
}
