namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitCategoryLink : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitCategoryLinks",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        FruitProductID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitProductCategories", t => t.CategoryID)
                .ForeignKey("dbo.FruitProducts", t => t.FruitProductID)
                .Index(t => t.CategoryID)
                .Index(t => t.FruitProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitCategoryLinks", "FruitProductID", "dbo.FruitProducts");
            DropForeignKey("dbo.FruitCategoryLinks", "CategoryID", "dbo.FruitProductCategories");
            DropIndex("dbo.FruitCategoryLinks", new[] { "FruitProductID" });
            DropIndex("dbo.FruitCategoryLinks", new[] { "CategoryID" });
            DropTable("dbo.FruitCategoryLinks");
        }
    }
}
