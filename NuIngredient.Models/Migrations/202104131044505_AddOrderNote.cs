namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderNote : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrderHeaders", "Note", c => c.String());
            AddColumn("dbo.MilkOrderHeaders", "Note", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkOrderHeaders", "Note");
            DropColumn("dbo.FruitOrderHeaders", "Note");
        }
    }
}
