namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierIntoAisle : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Suppliers", "DefaultAisleID", "dbo.Aisles");
            DropIndex("dbo.Suppliers", new[] { "DefaultAisleID" });
            AddColumn("dbo.Aisles", "DefaultSupplierID", c => c.Guid());
            CreateIndex("dbo.Aisles", "DefaultSupplierID");
            AddForeignKey("dbo.Aisles", "DefaultSupplierID", "dbo.Suppliers", "ID");
            DropColumn("dbo.Suppliers", "DefaultAisleID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Suppliers", "DefaultAisleID", c => c.Guid());
            DropForeignKey("dbo.Aisles", "DefaultSupplierID", "dbo.Suppliers");
            DropIndex("dbo.Aisles", new[] { "DefaultSupplierID" });
            DropColumn("dbo.Aisles", "DefaultSupplierID");
            CreateIndex("dbo.Suppliers", "DefaultAisleID");
            AddForeignKey("dbo.Suppliers", "DefaultAisleID", "dbo.Aisles", "ID");
        }
    }
}
