namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMilkCategoryLink : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MilkCategoryLinks",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        MilkProductID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkProductCategories", t => t.CategoryID)
                .ForeignKey("dbo.MilkProducts", t => t.MilkProductID)
                .Index(t => t.CategoryID)
                .Index(t => t.MilkProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkCategoryLinks", "MilkProductID", "dbo.MilkProducts");
            DropForeignKey("dbo.MilkCategoryLinks", "CategoryID", "dbo.MilkProductCategories");
            DropIndex("dbo.MilkCategoryLinks", new[] { "MilkProductID" });
            DropIndex("dbo.MilkCategoryLinks", new[] { "CategoryID" });
            DropTable("dbo.MilkCategoryLinks");
        }
    }
}
