namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAisleCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aisles", "AisleType", c => c.Int(nullable: false, defaultValueSql: "'3'"));
            AddColumn("dbo.MilkProductCategories", "AisleID", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkProductCategories", "AisleID");
            DropColumn("dbo.Aisles", "AisleType");
        }
    }
}
