namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFeedback : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        UserID = c.String(maxLength: 128),
                        Message = c.String(),
                        Created = c.DateTime(nullable: false),
                        NIComment = c.String(),
                        ReplierID = c.String(maxLength: 128),
                        ResponseTime = c.DateTime(),
                        DryGoodsOrderID = c.Guid(),
                        FeedbackTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.FeedbackTypes", t => t.FeedbackTypeID)
                .ForeignKey("dbo.AspNetUsers", t => t.ReplierID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.CompanyID)
                .Index(t => t.UserID)
                .Index(t => t.ReplierID)
                .Index(t => t.FeedbackTypeID);
            
            CreateTable(
                "dbo.FeedbackTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Colour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Feedbacks", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Feedbacks", "ReplierID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Feedbacks", "FeedbackTypeID", "dbo.FeedbackTypes");
            DropForeignKey("dbo.Feedbacks", "CompanyID", "dbo.Companies");
            DropIndex("dbo.Feedbacks", new[] { "FeedbackTypeID" });
            DropIndex("dbo.Feedbacks", new[] { "ReplierID" });
            DropIndex("dbo.Feedbacks", new[] { "UserID" });
            DropIndex("dbo.Feedbacks", new[] { "CompanyID" });
            DropTable("dbo.FeedbackTypes");
            DropTable("dbo.Feedbacks");
        }
    }
}
