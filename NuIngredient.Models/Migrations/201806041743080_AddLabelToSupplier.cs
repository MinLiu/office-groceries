namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLabelToSupplier : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "Label", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "Label");
        }
    }
}
