﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMultiSites : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "HasMultiSites", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "HasMultiSites");
        }
    }
}
