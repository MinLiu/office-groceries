namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAisles1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aisles", "IsMain", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Aisles", "IsMain");
        }
    }
}
