namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateComplaints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Complaints", "OneOffOrderID", c => c.Guid());
            CreateIndex("dbo.Complaints", "OneOffOrderID");
            AddForeignKey("dbo.Complaints", "OneOffOrderID", "dbo.OneOffOrders", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Complaints", "OneOffOrderID", "dbo.OneOffOrders");
            DropIndex("dbo.Complaints", new[] { "OneOffOrderID" });
            DropColumn("dbo.Complaints", "OneOffOrderID");
        }
    }
}
