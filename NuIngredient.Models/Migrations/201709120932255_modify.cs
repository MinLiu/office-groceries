namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MilkTypes", "ImageURL", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkTypes", "ImageURL");
        }
    }
}
