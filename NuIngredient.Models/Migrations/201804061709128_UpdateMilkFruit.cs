namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilkFruit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrderHeaders", "AccountNo", c => c.String());
            AddColumn("dbo.MilkOrderHeaders", "AccountNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkOrderHeaders", "AccountNo");
            DropColumn("dbo.FruitOrderHeaders", "AccountNo");
        }
    }
}
