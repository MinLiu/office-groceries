namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBillingAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "BillingAddress", c => c.String());
            AddColumn("dbo.Companies", "BillingPostcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "BillingPostcode");
            DropColumn("dbo.Companies", "BillingAddress");
        }
    }
}
