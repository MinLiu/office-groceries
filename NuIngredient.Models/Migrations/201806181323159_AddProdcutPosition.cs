namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProdcutPosition : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "SortPosition", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "SortPosition");
        }
    }
}
