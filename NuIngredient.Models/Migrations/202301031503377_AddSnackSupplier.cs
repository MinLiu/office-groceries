﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSnackSupplier : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "AskingForSnackSupplier", c => c.DateTime());
            AddColumn("dbo.FruitProducts", "FruitProductType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FruitProducts", "FruitProductType");
            DropColumn("dbo.Companies", "AskingForSnackSupplier");
        }
    }
}
