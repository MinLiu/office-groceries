namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateStaticPage : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PageAbouts", newName: "StaticPages");
            AddColumn("dbo.StaticPages", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StaticPages", "Name");
            RenameTable(name: "dbo.StaticPages", newName: "PageAbouts");
        }
    }
}
