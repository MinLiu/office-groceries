namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifySupplier : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SupplierProducts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Suppliers", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SupplierAddresses", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierAttachments", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierContacts", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierProducts", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierTypes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Suppliers", "SupplierTypeID", "dbo.SupplierTypes");
            DropIndex("dbo.SupplierProducts", new[] { "ProductID" });
            DropIndex("dbo.SupplierProducts", new[] { "SupplierID" });
            DropIndex("dbo.Suppliers", new[] { "CompanyID" });
            DropIndex("dbo.Suppliers", new[] { "SupplierTypeID" });
            DropIndex("dbo.SupplierAddresses", new[] { "SupplierID" });
            DropIndex("dbo.SupplierAttachments", new[] { "SupplierID" });
            DropIndex("dbo.SupplierContacts", new[] { "SupplierID" });
            DropIndex("dbo.SupplierTypes", new[] { "CompanyID" });
            DropColumn("dbo.Suppliers", "CompanyID");
            DropColumn("dbo.Suppliers", "SupplierTypeID");
            DropColumn("dbo.Suppliers", "Address1");
            DropColumn("dbo.Suppliers", "Address2");
            DropColumn("dbo.Suppliers", "Address3");
            DropColumn("dbo.Suppliers", "Town");
            DropColumn("dbo.Suppliers", "County");
            DropColumn("dbo.Suppliers", "Postcode");
            DropColumn("dbo.Suppliers", "Country");
            DropColumn("dbo.Suppliers", "Email");
            DropColumn("dbo.Suppliers", "Telephone");
            DropColumn("dbo.Suppliers", "Mobile");
            DropColumn("dbo.Suppliers", "Website");
            DropColumn("dbo.Suppliers", "Notes");
            DropColumn("dbo.Suppliers", "SageAccountReference");
            DropColumn("dbo.Suppliers", "Created");
            DropTable("dbo.SupplierProducts");
            DropTable("dbo.SupplierAddresses");
            DropTable("dbo.SupplierAttachments");
            DropTable("dbo.SupplierContacts");
            DropTable("dbo.SupplierTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SupplierTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierContacts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Position = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierProducts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Suppliers", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Suppliers", "SageAccountReference", c => c.String());
            AddColumn("dbo.Suppliers", "Notes", c => c.String());
            AddColumn("dbo.Suppliers", "Website", c => c.String());
            AddColumn("dbo.Suppliers", "Mobile", c => c.String());
            AddColumn("dbo.Suppliers", "Telephone", c => c.String());
            AddColumn("dbo.Suppliers", "Email", c => c.String());
            AddColumn("dbo.Suppliers", "Country", c => c.String());
            AddColumn("dbo.Suppliers", "Postcode", c => c.String());
            AddColumn("dbo.Suppliers", "County", c => c.String());
            AddColumn("dbo.Suppliers", "Town", c => c.String());
            AddColumn("dbo.Suppliers", "Address3", c => c.String());
            AddColumn("dbo.Suppliers", "Address2", c => c.String());
            AddColumn("dbo.Suppliers", "Address1", c => c.String());
            AddColumn("dbo.Suppliers", "SupplierTypeID", c => c.Guid());
            AddColumn("dbo.Suppliers", "CompanyID", c => c.Guid(nullable: false));
            CreateIndex("dbo.SupplierTypes", "CompanyID");
            CreateIndex("dbo.SupplierContacts", "SupplierID");
            CreateIndex("dbo.SupplierAttachments", "SupplierID");
            CreateIndex("dbo.SupplierAddresses", "SupplierID");
            CreateIndex("dbo.Suppliers", "SupplierTypeID");
            CreateIndex("dbo.Suppliers", "CompanyID");
            CreateIndex("dbo.SupplierProducts", "SupplierID");
            CreateIndex("dbo.SupplierProducts", "ProductID");
            AddForeignKey("dbo.Suppliers", "SupplierTypeID", "dbo.SupplierTypes", "ID");
            AddForeignKey("dbo.SupplierTypes", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.SupplierProducts", "SupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.SupplierContacts", "SupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.SupplierAttachments", "SupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.SupplierAddresses", "SupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.Suppliers", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.SupplierProducts", "ProductID", "dbo.Products", "ID");
        }
    }
}
