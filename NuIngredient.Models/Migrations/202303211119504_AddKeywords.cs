﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKeywords : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Keywords", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Keywords");
        }
    }
}
