namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilkProduct : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.MilkProductExclusivePrices", new[] { "ProductID" });
            DropIndex("dbo.MilkProductExclusivePrices", new[] { "MilkProduct_ID" });
            DropColumn("dbo.MilkProductExclusivePrices", "ProductID");
            RenameColumn(table: "dbo.MilkProductExclusivePrices", name: "MilkProduct_ID", newName: "ProductID");
            AlterColumn("dbo.MilkProductExclusivePrices", "ProductID", c => c.Guid(nullable: false));
            CreateIndex("dbo.MilkProductExclusivePrices", "ProductID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MilkProductExclusivePrices", new[] { "ProductID" });
            AlterColumn("dbo.MilkProductExclusivePrices", "ProductID", c => c.Guid());
            RenameColumn(table: "dbo.MilkProductExclusivePrices", name: "ProductID", newName: "MilkProduct_ID");
            AddColumn("dbo.MilkProductExclusivePrices", "ProductID", c => c.Guid(nullable: false));
            CreateIndex("dbo.MilkProductExclusivePrices", "MilkProduct_ID");
            CreateIndex("dbo.MilkProductExclusivePrices", "ProductID");
        }
    }
}
