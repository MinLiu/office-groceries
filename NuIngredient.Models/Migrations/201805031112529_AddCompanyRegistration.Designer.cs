// <auto-generated />
namespace NuIngredient.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddCompanyRegistration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCompanyRegistration));
        
        string IMigrationMetadata.Id
        {
            get { return "201805031112529_AddCompanyRegistration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
