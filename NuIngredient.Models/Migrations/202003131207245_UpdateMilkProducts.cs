namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilkProducts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MilkProducts", "AisleID", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkProducts", "AisleID");
        }
    }
}
