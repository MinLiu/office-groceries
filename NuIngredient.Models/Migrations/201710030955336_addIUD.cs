namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIUD : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.CompanySuppliers");
            AddColumn("dbo.CompanySuppliers", "ID", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.CompanySuppliers", "ID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.CompanySuppliers");
            DropColumn("dbo.CompanySuppliers", "ID");
            AddPrimaryKey("dbo.CompanySuppliers", new[] { "CompanyID", "SupplierID" });
        }
    }
}
