namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanDB : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FruitInvoiceHeaders", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.FruitInvoices", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.FruitInvoices", "FruitInvoiceHeaderID", "dbo.FruitInvoiceHeaders");
            DropForeignKey("dbo.Complaints", "FruitInvoiceHeaderID", "dbo.FruitInvoiceHeaders");
            DropForeignKey("dbo.Fruits", "FruitTypeID", "dbo.FruitTypes");
            DropIndex("dbo.Complaints", new[] { "FruitInvoiceHeaderID" });
            DropIndex("dbo.FruitInvoiceHeaders", new[] { "CompanyID" });
            DropIndex("dbo.FruitInvoices", new[] { "FruitInvoiceHeaderID" });
            DropIndex("dbo.FruitInvoices", new[] { "CompanyID" });
            DropIndex("dbo.Fruits", new[] { "FruitTypeID" });
            DropColumn("dbo.Complaints", "FruitInvoiceHeaderID");
            DropTable("dbo.FruitInvoiceHeaders");
            DropTable("dbo.FruitInvoices");
            DropTable("dbo.Fruits");
            DropTable("dbo.FruitTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FruitTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ImageURL = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Fruits",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Unit = c.String(),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VATRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageUrl = c.String(),
                        Deleted = c.Boolean(nullable: false),
                        FruitTypeID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FruitInvoices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FruitInvoiceHeaderID = c.Guid(nullable: false),
                        ImageURL = c.String(),
                        ProductName = c.String(),
                        Unit = c.String(),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Monday = c.Int(nullable: false),
                        Tuesday = c.Int(nullable: false),
                        Wednesday = c.Int(nullable: false),
                        Thursday = c.Int(nullable: false),
                        Friday = c.Int(nullable: false),
                        Saturday = c.Int(nullable: false),
                        Sunday = c.Int(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FruitID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FruitInvoiceHeaders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        CompanyID = c.Guid(nullable: false),
                        FromDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Complaints", "FruitInvoiceHeaderID", c => c.Guid());
            CreateIndex("dbo.Fruits", "FruitTypeID");
            CreateIndex("dbo.FruitInvoices", "CompanyID");
            CreateIndex("dbo.FruitInvoices", "FruitInvoiceHeaderID");
            CreateIndex("dbo.FruitInvoiceHeaders", "CompanyID");
            CreateIndex("dbo.Complaints", "FruitInvoiceHeaderID");
            AddForeignKey("dbo.Fruits", "FruitTypeID", "dbo.FruitTypes", "ID");
            AddForeignKey("dbo.Complaints", "FruitInvoiceHeaderID", "dbo.FruitInvoiceHeaders", "ID");
            AddForeignKey("dbo.FruitInvoices", "FruitInvoiceHeaderID", "dbo.FruitInvoiceHeaders", "ID");
            AddForeignKey("dbo.FruitInvoices", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.FruitInvoiceHeaders", "CompanyID", "dbo.Companies", "ID");
        }
    }
}
