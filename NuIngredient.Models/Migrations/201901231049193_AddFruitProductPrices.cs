namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitProductPrices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitProductPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        BreakPoint = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitProducts", t => t.ProductID)
                .Index(t => t.ProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitProductPrices", "ProductID", "dbo.FruitProducts");
            DropIndex("dbo.FruitProductPrices", new[] { "ProductID" });
            DropTable("dbo.FruitProductPrices");
        }
    }
}
