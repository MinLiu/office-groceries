namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortPosInCategories : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCategories", "SortPos", c => c.Int(nullable: false));
            AddColumn("dbo.FruitProductCategories", "SortPos", c => c.Int(nullable: false));
            AddColumn("dbo.MilkProductCategories", "SortPos", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkProductCategories", "SortPos");
            DropColumn("dbo.FruitProductCategories", "SortPos");
            DropColumn("dbo.ProductCategories", "SortPos");
        }
    }
}
