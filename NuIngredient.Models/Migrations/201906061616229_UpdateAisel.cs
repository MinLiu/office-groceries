namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAisel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aisles", "H1Tag", c => c.String());
            AddColumn("dbo.Aisles", "AisleImageUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Aisles", "AisleImageUrl");
            DropColumn("dbo.Aisles", "H1Tag");
        }
    }
}
