namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilkCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitCategoryLinks", "SortPos", c => c.Int(nullable: false));
            AddColumn("dbo.MilkCategoryLinks", "SortPos", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkCategoryLinks", "SortPos");
            DropColumn("dbo.FruitCategoryLinks", "SortPos");
        }
    }
}
