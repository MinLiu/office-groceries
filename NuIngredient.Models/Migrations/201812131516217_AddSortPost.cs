namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortPost : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DryGoodsCategories", "SortPos", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DryGoodsCategories", "SortPos");
        }
    }
}
