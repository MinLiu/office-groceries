﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitExclusivePrice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitProductTagPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        TagID = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitProducts", t => t.ProductID)
                .ForeignKey("dbo.Tags", t => t.TagID)
                .Index(t => t.ProductID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitProductTagPrices", "TagID", "dbo.Tags");
            DropForeignKey("dbo.FruitProductTagPrices", "ProductID", "dbo.FruitProducts");
            DropIndex("dbo.FruitProductTagPrices", new[] { "TagID" });
            DropIndex("dbo.FruitProductTagPrices", new[] { "ProductID" });
            DropTable("dbo.FruitProductTagPrices");
        }
    }
}
