namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyCarousel : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Carousels", new[] { "productID" });
            CreateIndex("dbo.Carousels", "ProductID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Carousels", new[] { "ProductID" });
            CreateIndex("dbo.Carousels", "productID");
        }
    }
}
