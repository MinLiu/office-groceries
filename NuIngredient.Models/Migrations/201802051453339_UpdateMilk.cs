namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilk : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Milk", "MilkTypeID", "dbo.MilkTypes");
            DropIndex("dbo.Milk", new[] { "MilkTypeID" });
            DropForeignKey("dbo.MilkOrders", "MilkID", "dbo.Milk");
            DropIndex("dbo.MilkOrders", new[] { "MilkID" });
            RenameColumn(table: "dbo.MilkOrders", name: "CompanyID", newName: "Company_ID");
            RenameIndex(table: "dbo.MilkOrders", name: "IX_CompanyID", newName: "IX_Company_ID");
            CreateTable(
                "dbo.MilkOrderHeaders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        SupplierID = c.Guid(),
                        FileURL = c.String(),
                        ProspectToken = c.String(),
                        OrderNumber = c.String(),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalVAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        FromDate = c.DateTime(),
                        ToDate = c.DateTime(),
                        OrderMode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierID);
            
            AddColumn("dbo.Complaints", "MilkOrderHeaderID", c => c.Guid());
            AddColumn("dbo.MilkOrders", "VATRate", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.MilkOrders", "TotalNet", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.MilkOrders", "TotalVAT", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.MilkOrders", "OrderHeaderID", c => c.Guid(nullable: false));
            AddColumn("dbo.MilkOrders", "MilkProductID", c => c.Guid(nullable: false));
            CreateIndex("dbo.Complaints", "MilkOrderHeaderID");
            CreateIndex("dbo.MilkOrders", "MilkProductID");
            CreateIndex("dbo.MilkOrders", "OrderHeaderID");
            AddForeignKey("dbo.MilkOrders", "MilkProductID", "dbo.MilkProducts", "ID");
            AddForeignKey("dbo.MilkOrders", "OrderHeaderID", "dbo.MilkOrderHeaders", "ID");
            AddForeignKey("dbo.Complaints", "MilkOrderHeaderID", "dbo.MilkOrderHeaders", "ID");
            DropColumn("dbo.MilkOrders", "ProspectToken");
            DropColumn("dbo.MilkOrders", "MilkID");
            DropTable("dbo.Milk");
            DropTable("dbo.MilkTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MilkTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ImageURL = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Milk",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Unit = c.String(),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageUrl = c.String(),
                        Deleted = c.Boolean(nullable: false),
                        MilkTypeID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.MilkOrders", "MilkID", c => c.Guid(nullable: false));
            AddColumn("dbo.MilkOrders", "ProspectToken", c => c.String());
            DropForeignKey("dbo.Complaints", "MilkOrderHeaderID", "dbo.MilkOrderHeaders");
            DropForeignKey("dbo.MilkOrderHeaders", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.MilkOrders", "OrderHeaderID", "dbo.MilkOrderHeaders");
            DropForeignKey("dbo.MilkOrders", "MilkProductID", "dbo.MilkProducts");
            DropForeignKey("dbo.MilkOrderHeaders", "CompanyID", "dbo.Companies");
            DropIndex("dbo.MilkOrders", new[] { "OrderHeaderID" });
            DropIndex("dbo.MilkOrders", new[] { "MilkProductID" });
            DropIndex("dbo.MilkOrderHeaders", new[] { "SupplierID" });
            DropIndex("dbo.MilkOrderHeaders", new[] { "CompanyID" });
            DropIndex("dbo.Complaints", new[] { "MilkOrderHeaderID" });
            DropColumn("dbo.MilkOrders", "MilkProductID");
            DropColumn("dbo.MilkOrders", "OrderHeaderID");
            DropColumn("dbo.MilkOrders", "TotalVAT");
            DropColumn("dbo.MilkOrders", "TotalNet");
            DropColumn("dbo.MilkOrders", "VATRate");
            DropColumn("dbo.Complaints", "MilkOrderHeaderID");
            DropTable("dbo.MilkOrderHeaders");
            RenameIndex(table: "dbo.MilkOrders", name: "IX_Company_ID", newName: "IX_CompanyID");
            RenameColumn(table: "dbo.MilkOrders", name: "Company_ID", newName: "CompanyID");
            CreateIndex("dbo.MilkOrders", "MilkID");
            AddForeignKey("dbo.MilkOrders", "MilkID", "dbo.Milk", "ID");
            CreateIndex("dbo.Milk", "MilkTypeID");
            AddForeignKey("dbo.Milk", "MilkTypeID", "dbo.MilkTypes", "ID");
        }
    }
}
