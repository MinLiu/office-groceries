namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateVAT : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OneOffOrders", "TotalGross", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.OneOffOrderItems", "ProductVAT", c => c.Decimal(nullable: false, precision: 28, scale: 5));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OneOffOrderItems", "ProductVAT");
            DropColumn("dbo.OneOffOrders", "TotalGross");
        }
    }
}
