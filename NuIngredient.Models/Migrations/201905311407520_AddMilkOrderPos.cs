namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMilkOrderPos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrders", "SortPos", c => c.Int(nullable: false));
            AddColumn("dbo.MilkOrders", "SortPos", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkOrders", "SortPos");
            DropColumn("dbo.FruitOrders", "SortPos");
        }
    }
}
