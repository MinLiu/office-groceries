namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSupPostcode : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SupplierPostcodes", "Postcode", c => c.String(nullable: false, maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SupplierPostcodes", "Postcode", c => c.String(nullable: false, maxLength: 2));
        }
    }
}
