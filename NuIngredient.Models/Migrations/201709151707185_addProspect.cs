namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addProspect : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Prospects",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        CompanyName = c.String(),
                        ProspectToken = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Prospects");
        }
    }
}
