﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeOrderNumberUnique : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.OneOffOrders", new[] { "OrderNumber", "OrderNumberSequence" }, unique: true, name: "OrderNumberAndSequence");
        }
        
        public override void Down()
        {
            DropIndex("dbo.OneOffOrders", "OrderNumberAndSequence");
        }
    }
}
