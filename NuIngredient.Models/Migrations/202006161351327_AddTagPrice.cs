namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTagPrice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyTagItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        TagID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Tags", t => t.TagID)
                .Index(t => t.CompanyID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MilkProductTagPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        TagID = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkProducts", t => t.ProductID)
                .ForeignKey("dbo.Tags", t => t.TagID)
                .Index(t => t.ProductID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkProductTagPrices", "TagID", "dbo.Tags");
            DropForeignKey("dbo.MilkProductTagPrices", "ProductID", "dbo.MilkProducts");
            DropForeignKey("dbo.CompanyTagItems", "TagID", "dbo.Tags");
            DropForeignKey("dbo.CompanyTagItems", "CompanyID", "dbo.Companies");
            DropIndex("dbo.MilkProductTagPrices", new[] { "TagID" });
            DropIndex("dbo.MilkProductTagPrices", new[] { "ProductID" });
            DropIndex("dbo.CompanyTagItems", new[] { "TagID" });
            DropIndex("dbo.CompanyTagItems", new[] { "CompanyID" });
            DropTable("dbo.MilkProductTagPrices");
            DropTable("dbo.Tags");
            DropTable("dbo.CompanyTagItems");
        }
    }
}
