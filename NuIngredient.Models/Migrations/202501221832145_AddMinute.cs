﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMinute : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "WeeklyEmailReminderMinute", c => c.Int(nullable: false));
            AddColumn("dbo.OneOffOrders", "OriginalCreated", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OneOffOrders", "OriginalCreated");
            DropColumn("dbo.Suppliers", "WeeklyEmailReminderMinute");
        }
    }
}
