namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFeedback : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Feedbacks", "DryGoodsOrderID");
            AddForeignKey("dbo.Feedbacks", "DryGoodsOrderID", "dbo.OneOffOrders", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Feedbacks", "DryGoodsOrderID", "dbo.OneOffOrders");
            DropIndex("dbo.Feedbacks", new[] { "DryGoodsOrderID" });
        }
    }
}
