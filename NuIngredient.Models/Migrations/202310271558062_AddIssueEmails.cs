﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIssueEmails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IssueEmails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Subject = c.String(),
                        Message = c.String(),
                        CompanyID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        UserName = c.String(),
                        CreatedDateUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IssueEmails", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.IssueEmails", "CompanyID", "dbo.Companies");
            DropIndex("dbo.IssueEmails", new[] { "SupplierID" });
            DropIndex("dbo.IssueEmails", new[] { "CompanyID" });
            DropTable("dbo.IssueEmails");
        }
    }
}
