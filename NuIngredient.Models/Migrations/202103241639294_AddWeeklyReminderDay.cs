namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWeeklyReminderDay : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "WeeklyEmailReminderDay", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "WeeklyEmailReminderDay");
        }
    }
}
