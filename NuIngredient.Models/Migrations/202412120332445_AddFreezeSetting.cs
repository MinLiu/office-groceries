﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFreezeSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "IsFreezeActivated", c => c.Boolean(nullable: false));
            AddColumn("dbo.Settings", "FreezeStartDate", c => c.DateTime());
            AddColumn("dbo.Settings", "FreezeEndDate", c => c.DateTime());
            AddColumn("dbo.Settings", "FreezeMessage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "FreezeMessage");
            DropColumn("dbo.Settings", "FreezeEndDate");
            DropColumn("dbo.Settings", "FreezeStartDate");
            DropColumn("dbo.Settings", "IsFreezeActivated");
        }
    }
}
