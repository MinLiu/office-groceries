namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifySupplier3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanySuppliers",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        AccountNo = c.String(),
                        StartDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.CompanyID, t.SupplierID })
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CompanySuppliers", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.CompanySuppliers", "CompanyID", "dbo.Companies");
            DropIndex("dbo.CompanySuppliers", new[] { "SupplierID" });
            DropIndex("dbo.CompanySuppliers", new[] { "CompanyID" });
            DropTable("dbo.CompanySuppliers");
        }
    }
}
