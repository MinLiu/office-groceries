﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeliveryCharge : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.MilkOrders", new[] { "MilkProductID" });
            CreateTable(
                "dbo.DeliveryCharges",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(),
                        Name = c.String(),
                        Code = c.String(),
                        Charge = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        IsActive = c.Boolean(nullable: false),
                        Type = c.Int(nullable: false),
                        Category = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.DeliveryChargePostcodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryChargeID = c.Guid(nullable: false),
                        Postcode = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DeliveryCharges", t => t.DeliveryChargeID)
                .Index(t => t.DeliveryChargeID);
            
            AddColumn("dbo.Companies", "NoDeliveryCharge", c => c.Boolean(nullable: false));
            AddColumn("dbo.MilkOrderHeaders", "DeliveryUnitCharge", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.MilkOrderHeaders", "DeliveryChargeVAT", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.MilkOrderHeaders", "DeliveryChargeName", c => c.String());
            AddColumn("dbo.MilkOrderHeaders", "DeliveryChargeCode", c => c.String());
            AddColumn("dbo.MilkOrders", "LineItemType", c => c.Int(nullable: false));
            AlterColumn("dbo.MilkOrders", "MilkProductID", c => c.Guid());
            CreateIndex("dbo.MilkOrders", "MilkProductID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DeliveryCharges", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.DeliveryChargePostcodes", "DeliveryChargeID", "dbo.DeliveryCharges");
            DropIndex("dbo.MilkOrders", new[] { "MilkProductID" });
            DropIndex("dbo.DeliveryChargePostcodes", new[] { "DeliveryChargeID" });
            DropIndex("dbo.DeliveryCharges", new[] { "SupplierID" });
            AlterColumn("dbo.MilkOrders", "MilkProductID", c => c.Guid(nullable: false));
            DropColumn("dbo.MilkOrders", "LineItemType");
            DropColumn("dbo.MilkOrderHeaders", "DeliveryChargeCode");
            DropColumn("dbo.MilkOrderHeaders", "DeliveryChargeName");
            DropColumn("dbo.MilkOrderHeaders", "DeliveryChargeVAT");
            DropColumn("dbo.MilkOrderHeaders", "DeliveryUnitCharge");
            DropColumn("dbo.Companies", "NoDeliveryCharge");
            DropTable("dbo.DeliveryChargePostcodes");
            DropTable("dbo.DeliveryCharges");
            CreateIndex("dbo.MilkOrders", "MilkProductID");
        }
    }
}
