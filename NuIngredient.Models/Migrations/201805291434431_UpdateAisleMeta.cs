namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAisleMeta : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aisles", "MetaTitle", c => c.String());
            AddColumn("dbo.Aisles", "MetaKeyword", c => c.String());
            AddColumn("dbo.Aisles", "MetaDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Aisles", "MetaDescription");
            DropColumn("dbo.Aisles", "MetaKeyword");
            DropColumn("dbo.Aisles", "MetaTitle");
        }
    }
}
