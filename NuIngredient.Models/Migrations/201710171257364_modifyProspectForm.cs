namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyProspectForm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prospects", "FirstName", c => c.String());
            AddColumn("dbo.Prospects", "LastName", c => c.String());
            DropColumn("dbo.Prospects", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prospects", "Name", c => c.String());
            DropColumn("dbo.Prospects", "LastName");
            DropColumn("dbo.Prospects", "FirstName");
        }
    }
}
