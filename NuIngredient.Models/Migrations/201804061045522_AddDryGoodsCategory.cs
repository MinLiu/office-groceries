namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDryGoodsCategory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DryGoodsCategories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProductCategories", t => t.CategoryID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.CategoryID)
                .Index(t => t.ProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DryGoodsCategories", "ProductID", "dbo.Products");
            DropForeignKey("dbo.DryGoodsCategories", "CategoryID", "dbo.ProductCategories");
            DropIndex("dbo.DryGoodsCategories", new[] { "ProductID" });
            DropIndex("dbo.DryGoodsCategories", new[] { "CategoryID" });
            DropTable("dbo.DryGoodsCategories");
        }
    }
}
