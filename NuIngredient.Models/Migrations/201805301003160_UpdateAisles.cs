namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAisles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aisles", "Label", c => c.String());
            AddColumn("dbo.Aisles", "LandingPageFilePath", c => c.String());
            AddColumn("dbo.Aisles", "ProspectContentFilePath", c => c.String());
            AddColumn("dbo.Aisles", "CustomerContentFilePath", c => c.String());
            AddColumn("dbo.Aisles", "IsConstant", c => c.Boolean(nullable: false));
            AddColumn("dbo.Aisles", "SortPosition", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Aisles", "SortPosition");
            DropColumn("dbo.Aisles", "IsConstant");
            DropColumn("dbo.Aisles", "CustomerContentFilePath");
            DropColumn("dbo.Aisles", "ProspectContentFilePath");
            DropColumn("dbo.Aisles", "LandingPageFilePath");
            DropColumn("dbo.Aisles", "Label");
        }
    }
}
