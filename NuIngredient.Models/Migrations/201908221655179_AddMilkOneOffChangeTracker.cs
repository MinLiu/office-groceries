namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMilkOneOffChangeTracker : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MilkOneOffChangeTrackers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        MilkProductID = c.Guid(nullable: false),
                        Monday = c.Boolean(nullable: false),
                        Tuesday = c.Boolean(nullable: false),
                        Wednesday = c.Boolean(nullable: false),
                        Thursday = c.Boolean(nullable: false),
                        Friday = c.Boolean(nullable: false),
                        Saturday = c.Boolean(nullable: false),
                        OrderHeaderID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkProducts", t => t.MilkProductID)
                .ForeignKey("dbo.MilkOrderHeaders", t => t.OrderHeaderID)
                .Index(t => t.MilkProductID)
                .Index(t => t.OrderHeaderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkOneOffChangeTrackers", "OrderHeaderID", "dbo.MilkOrderHeaders");
            DropForeignKey("dbo.MilkOneOffChangeTrackers", "MilkProductID", "dbo.MilkProducts");
            DropIndex("dbo.MilkOneOffChangeTrackers", new[] { "OrderHeaderID" });
            DropIndex("dbo.MilkOneOffChangeTrackers", new[] { "MilkProductID" });
            DropTable("dbo.MilkOneOffChangeTrackers");
        }
    }
}
