namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialProduct : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductKitItems", "Product_ID", "dbo.Products");
            DropIndex("dbo.ProductKitItems", new[] { "Product_ID" });
            DropColumn("dbo.Products", "CurrencyID");
            DropColumn("dbo.Products", "IsKit");
            DropColumn("dbo.Products", "UseKitPrice");
            DropColumn("dbo.Products", "UsePriceBreaks");
            DropColumn("dbo.Products", "IsBought");
            DropColumn("dbo.Products", "IsService");
            DropColumn("dbo.Products", "MinimumQty");
            DropColumn("dbo.Products", "MaximumQty");
            DropColumn("dbo.ProductKitItems", "Product_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductKitItems", "Product_ID", c => c.Guid());
            AddColumn("dbo.Products", "MaximumQty", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "MinimumQty", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "IsService", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "IsBought", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "UsePriceBreaks", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "UseKitPrice", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "IsKit", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "CurrencyID", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductKitItems", "Product_ID");
            AddForeignKey("dbo.ProductKitItems", "Product_ID", "dbo.Products", "ID");
        }
    }
}
