namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArchivedUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Archived", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Archived");
        }
    }
}
