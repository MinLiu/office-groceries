// <auto-generated />
namespace NuIngredient.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddSortPosInCategories : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddSortPosInCategories));
        
        string IMigrationMetadata.Id
        {
            get { return "201807180528422_AddSortPosInCategories"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
