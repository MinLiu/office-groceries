namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCookieID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrders", "CookieID", c => c.String());
            AddColumn("dbo.FruitOrders", "CompanyID", c => c.Guid());
            AddColumn("dbo.MilkOrders", "CookieID", c => c.String());
            AddColumn("dbo.MilkOrders", "CompanyID", c => c.Guid());
            CreateIndex("dbo.FruitOrders", "CompanyID");
            CreateIndex("dbo.MilkOrders", "CompanyID");
            AddForeignKey("dbo.FruitOrders", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.MilkOrders", "CompanyID", "dbo.Companies", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkOrders", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.FruitOrders", "CompanyID", "dbo.Companies");
            DropIndex("dbo.MilkOrders", new[] { "CompanyID" });
            DropIndex("dbo.FruitOrders", new[] { "CompanyID" });
            DropColumn("dbo.MilkOrders", "CompanyID");
            DropColumn("dbo.MilkOrders", "CookieID");
            DropColumn("dbo.FruitOrders", "CompanyID");
            DropColumn("dbo.FruitOrders", "CookieID");
        }
    }
}
