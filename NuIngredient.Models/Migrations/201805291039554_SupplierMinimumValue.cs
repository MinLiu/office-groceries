namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SupplierMinimumValue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "MinimumValue", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.Suppliers", "UseMasterAccount", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "UseMasterAccount");
            DropColumn("dbo.Suppliers", "MinimumValue");
        }
    }
}
