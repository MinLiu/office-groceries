namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Milk : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Milk",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Unit = c.String(),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageUrl = c.String(),
                        Deleted = c.Boolean(nullable: false),
                        MilkTypeID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkTypes", t => t.MilkTypeID)
                .Index(t => t.MilkTypeID);
            
            CreateTable(
                "dbo.MilkTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Milk", "MilkTypeID", "dbo.MilkTypes");
            DropIndex("dbo.Milk", new[] { "MilkTypeID" });
            DropTable("dbo.MilkTypes");
            DropTable("dbo.Milk");
        }
    }
}
