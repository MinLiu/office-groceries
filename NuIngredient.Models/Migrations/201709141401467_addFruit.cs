namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFruit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fruits",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Unit = c.String(),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageUrl = c.String(),
                        Deleted = c.Boolean(nullable: false),
                        FruitTypeID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitTypes", t => t.FruitTypeID)
                .Index(t => t.FruitTypeID);
            
            CreateTable(
                "dbo.FruitTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ImageURL = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FruitOrders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductName = c.String(),
                        Unit = c.String(),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        FruitID = c.Guid(nullable: false),
                        Monday = c.Int(nullable: false),
                        Tuesday = c.Int(nullable: false),
                        Wednesday = c.Int(nullable: false),
                        Thursday = c.Int(nullable: false),
                        Friday = c.Int(nullable: false),
                        Saturday = c.Int(nullable: false),
                        Sunday = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Fruits", t => t.FruitID)
                .Index(t => t.FruitID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitOrders", "FruitID", "dbo.Fruits");
            DropForeignKey("dbo.Fruits", "FruitTypeID", "dbo.FruitTypes");
            DropIndex("dbo.FruitOrders", new[] { "FruitID" });
            DropIndex("dbo.Fruits", new[] { "FruitTypeID" });
            DropTable("dbo.FruitOrders");
            DropTable("dbo.FruitTypes");
            DropTable("dbo.Fruits");
        }
    }
}
