namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyCollection : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyCollections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Companies", "CompanyCollectionID", c => c.Guid());
            CreateIndex("dbo.Companies", "CompanyCollectionID");
            AddForeignKey("dbo.Companies", "CompanyCollectionID", "dbo.CompanyCollections", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "CompanyCollectionID", "dbo.CompanyCollections");
            DropIndex("dbo.Companies", new[] { "CompanyCollectionID" });
            DropColumn("dbo.Companies", "CompanyCollectionID");
            DropTable("dbo.CompanyCollections");
        }
    }
}
