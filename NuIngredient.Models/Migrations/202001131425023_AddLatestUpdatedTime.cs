namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLatestUpdatedTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrderHeaders", "LatestUpdated", c => c.DateTime());
            AddColumn("dbo.MilkOrderHeaders", "LatestUpdated", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkOrderHeaders", "LatestUpdated");
            DropColumn("dbo.FruitOrderHeaders", "LatestUpdated");
        }
    }
}
