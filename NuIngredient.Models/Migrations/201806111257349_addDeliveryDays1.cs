namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDeliveryDays1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanySuppliers", "DeliveryMon", c => c.Boolean(nullable: false));
            AddColumn("dbo.CompanySuppliers", "DeliveryTue", c => c.Boolean(nullable: false));
            AddColumn("dbo.CompanySuppliers", "DeliveryWed", c => c.Boolean(nullable: false));
            AddColumn("dbo.CompanySuppliers", "DeliveryThu", c => c.Boolean(nullable: false));
            AddColumn("dbo.CompanySuppliers", "DeliveryFri", c => c.Boolean(nullable: false));
            AddColumn("dbo.CompanySuppliers", "DeliverySat", c => c.Boolean(nullable: false));
            AddColumn("dbo.CompanySuppliers", "DeliverySun", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanySuppliers", "DeliverySun");
            DropColumn("dbo.CompanySuppliers", "DeliverySat");
            DropColumn("dbo.CompanySuppliers", "DeliveryFri");
            DropColumn("dbo.CompanySuppliers", "DeliveryThu");
            DropColumn("dbo.CompanySuppliers", "DeliveryWed");
            DropColumn("dbo.CompanySuppliers", "DeliveryTue");
            DropColumn("dbo.CompanySuppliers", "DeliveryMon");
        }
    }
}
