namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShoppingCartItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShoppingCartItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        ProductID = c.Guid(nullable: false),
                        ProspectToken = c.String(),
                        Qty = c.Int(nullable: false),
                        UpdateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.CompanyID)
                .Index(t => t.ProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShoppingCartItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ShoppingCartItems", "CompanyID", "dbo.Companies");
            DropIndex("dbo.ShoppingCartItems", new[] { "ProductID" });
            DropIndex("dbo.ShoppingCartItems", new[] { "CompanyID" });
            DropTable("dbo.ShoppingCartItems");
        }
    }
}
