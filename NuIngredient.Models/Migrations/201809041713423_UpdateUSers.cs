namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUSers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "AgreeToGetEmails", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "AgreeToGetEmails");
        }
    }
}
