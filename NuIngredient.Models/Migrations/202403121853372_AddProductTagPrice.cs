﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductTagPrice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductTagPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        TagID = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.Tags", t => t.TagID)
                .Index(t => t.ProductID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductTagPrices", "TagID", "dbo.Tags");
            DropForeignKey("dbo.ProductTagPrices", "ProductID", "dbo.Products");
            DropIndex("dbo.ProductTagPrices", new[] { "TagID" });
            DropIndex("dbo.ProductTagPrices", new[] { "ProductID" });
            DropTable("dbo.ProductTagPrices");
        }
    }
}
