namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOneOffORder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OneOffOrders", "DeliveryDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OneOffOrders", "DeliveryDate");
        }
    }
}
