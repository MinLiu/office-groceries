namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "AskingForFruitSupplier", c => c.DateTime());
            AddColumn("dbo.Companies", "AskingForMilkSupplier", c => c.DateTime());
            AddColumn("dbo.Companies", "AskingForDryGoodsSupplier", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "AskingForDryGoodsSupplier");
            DropColumn("dbo.Companies", "AskingForMilkSupplier");
            DropColumn("dbo.Companies", "AskingForFruitSupplier");
        }
    }
}
