namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        MilkSupplierCenterEmail = c.String(),
                        FruitSupplierCenterEmail = c.String(),
                        DryGoodsSupplierCenterEmail = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Settings");
        }
    }
}
