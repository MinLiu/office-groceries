namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCompany5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "IsMultiTenantedBuilding", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "IsMultiTenantedBuilding");
        }
    }
}
