namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierPostcode : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierPostcodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Postcode = c.String(nullable: false, maxLength: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
            AddColumn("dbo.Suppliers", "EmailGoStraight", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierPostcodes", "SupplierID", "dbo.Suppliers");
            DropIndex("dbo.SupplierPostcodes", new[] { "SupplierID" });
            DropColumn("dbo.Suppliers", "EmailGoStraight");
            DropTable("dbo.SupplierPostcodes");
        }
    }
}
