﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplyPriceBreakToExclusive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitProducts", "ApplyPriceBreakToExclusive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FruitProducts", "ApplyPriceBreakToExclusive");
        }
    }
}
