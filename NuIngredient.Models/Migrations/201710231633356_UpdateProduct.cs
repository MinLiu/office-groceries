namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProduct : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ProductCategories", "CompanyID", "dbo.Companies");
            DropIndex("dbo.Products", new[] { "CompanyID" });
            DropIndex("dbo.ProductCategories", new[] { "CompanyID" });
            CreateTable(
                "dbo.ProductExclusivePrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CusomterID = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CusomterID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CusomterID);
            
            AddColumn("dbo.Products", "CategoryID", c => c.Guid(nullable: false));
            AddColumn("dbo.Products", "Name", c => c.String());
            AddColumn("dbo.Products", "ShortDescription", c => c.String());
            AddColumn("dbo.Products", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "IsProspectsVisible", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "IsExclusive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductCategories", "ParentID", c => c.Guid());
            CreateIndex("dbo.ProductCategories", "ParentID");
            CreateIndex("dbo.Products", "CategoryID");
            AddForeignKey("dbo.ProductCategories", "ParentID", "dbo.ProductCategories", "ID");
            AddForeignKey("dbo.Products", "CategoryID", "dbo.ProductCategories", "ID");
            DropColumn("dbo.Products", "CompanyID");
            DropColumn("dbo.Products", "Category");
            DropColumn("dbo.Products", "VendorCode");
            DropColumn("dbo.Products", "Manufacturer");
            DropColumn("dbo.Products", "VAT");
            DropColumn("dbo.ProductCategories", "CompanyID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductCategories", "CompanyID", c => c.Guid(nullable: false));
            AddColumn("dbo.Products", "VAT", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AddColumn("dbo.Products", "Manufacturer", c => c.String());
            AddColumn("dbo.Products", "VendorCode", c => c.String());
            AddColumn("dbo.Products", "Category", c => c.String());
            AddColumn("dbo.Products", "CompanyID", c => c.Guid(nullable: false));
            DropForeignKey("dbo.ProductExclusivePrices", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Products", "CategoryID", "dbo.ProductCategories");
            DropForeignKey("dbo.ProductExclusivePrices", "CusomterID", "dbo.Companies");
            DropForeignKey("dbo.ProductCategories", "ParentID", "dbo.ProductCategories");
            DropIndex("dbo.Products", new[] { "CategoryID" });
            DropIndex("dbo.ProductExclusivePrices", new[] { "CusomterID" });
            DropIndex("dbo.ProductExclusivePrices", new[] { "ProductID" });
            DropIndex("dbo.ProductCategories", new[] { "ParentID" });
            DropColumn("dbo.ProductCategories", "ParentID");
            DropColumn("dbo.Products", "IsExclusive");
            DropColumn("dbo.Products", "IsProspectsVisible");
            DropColumn("dbo.Products", "IsActive");
            DropColumn("dbo.Products", "ShortDescription");
            DropColumn("dbo.Products", "Name");
            DropColumn("dbo.Products", "CategoryID");
            DropTable("dbo.ProductExclusivePrices");
            CreateIndex("dbo.ProductCategories", "CompanyID");
            CreateIndex("dbo.Products", "CompanyID");
            AddForeignKey("dbo.ProductCategories", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.Products", "CompanyID", "dbo.Companies", "ID");
        }
    }
}
