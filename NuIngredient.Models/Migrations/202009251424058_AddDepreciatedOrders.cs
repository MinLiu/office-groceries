namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepreciatedOrders : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrderHeaders", "Depreciated", c => c.Boolean(nullable: false));
            AddColumn("dbo.MilkOrderHeaders", "Depreciated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkOrderHeaders", "Depreciated");
            DropColumn("dbo.FruitOrderHeaders", "Depreciated");
        }
    }
}
