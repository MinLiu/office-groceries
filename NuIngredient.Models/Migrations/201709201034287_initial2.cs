namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ActivityLogs", "ActivityByUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientEvents", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientAddresses", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.DeliveryNotes", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.DeliveryNoteAttachments", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNotes", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.DeliveryNoteComments", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.Clients", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientTags", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotePreviews", "BackTemplateID", "dbo.DeliveryNoteTemplates");
            DropForeignKey("dbo.DeliveryNotePreviews", "BodyTemplateID", "dbo.DeliveryNoteTemplates");
            DropForeignKey("dbo.DeliveryNoteTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotePreviews", "FrontTemplateID", "dbo.DeliveryNoteTemplates");
            DropForeignKey("dbo.DeliveryNotePreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotePreviews", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNotes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNoteSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultDeliveryNoteAttachments", "DeliveryNoteSettings_CompanyID", "dbo.DeliveryNoteSettings");
            DropForeignKey("dbo.DefaultDeliveryNoteNotifications", "DeliveryNoteSettings_CompanyID", "dbo.DeliveryNoteSettings");
            DropForeignKey("dbo.InvoicePreviews", "BackTemplateID", "dbo.InvoiceTemplates");
            DropForeignKey("dbo.InvoicePreviews", "BodyTemplateID", "dbo.InvoiceTemplates");
            DropForeignKey("dbo.InvoiceTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InvoicePreviews", "FrontTemplateID", "dbo.InvoiceTemplates");
            DropForeignKey("dbo.InvoicePreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Invoices", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.InvoiceAttachments", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.InvoiceComments", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Invoices", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Invoices", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Invoices", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.Invoices", "InvoiceAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.InvoiceItems", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.InvoiceItems", "ParentItemID", "dbo.InvoiceItems");
            DropForeignKey("dbo.InvoiceSections", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.InvoiceItems", "SectionID", "dbo.InvoiceSections");
            DropForeignKey("dbo.Jobs", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.JobAttachments", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.JobComments", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Jobs", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Jobs", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.DeliveryNotes", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "InvoiceAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Invoices", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobItems", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobItems", "ParentItemID", "dbo.JobItems");
            DropForeignKey("dbo.JobItems", "SectionID", "dbo.JobSections");
            DropForeignKey("dbo.JobSections", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobNotifications", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobPreviews", "BackTemplateID", "dbo.JobTemplates");
            DropForeignKey("dbo.JobPreviews", "BodyTemplateID", "dbo.JobTemplates");
            DropForeignKey("dbo.JobTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.JobPreviews", "FrontTemplateID", "dbo.JobTemplates");
            DropForeignKey("dbo.JobPreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.JobPreviews", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Quotations", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.QuotationAttachments", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.QuotationComments", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Quotations", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Quotations", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Quotations", "InvoiceAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Invoices", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationItems", "ParentItemID", "dbo.QuotationItems");
            DropForeignKey("dbo.QuotationItems", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationItems", "SectionID", "dbo.QuotationSections");
            DropForeignKey("dbo.QuotationSections", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Jobs", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationNotifications", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationPreviews", "BackTemplateID", "dbo.QuotationTemplates");
            DropForeignKey("dbo.QuotationPreviews", "BodyTemplateID", "dbo.QuotationTemplates");
            DropForeignKey("dbo.QuotationTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationPreviews", "FrontTemplateID", "dbo.QuotationTemplates");
            DropForeignKey("dbo.QuotationPreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationPreviews", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "StatusID", "dbo.QuotationStatus");
            DropForeignKey("dbo.Jobs", "StatusID", "dbo.JobStatus");
            DropForeignKey("dbo.InvoiceNotifications", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "StatusID", "dbo.InvoiceStatus");
            DropForeignKey("dbo.InvoicePreviews", "InvoiceID", "dbo.Invoices");
            DropForeignKey("dbo.InvoiceSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultInvoiceAttachments", "InvoiceSettings_CompanyID", "dbo.InvoiceSettings");
            DropForeignKey("dbo.InvoiceSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultInvoiceNotifications", "InvoiceSettings_CompanyID", "dbo.InvoiceSettings");
            DropForeignKey("dbo.JobSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultJobAttachments", "JobSettings_CompanyID", "dbo.JobSettings");
            DropForeignKey("dbo.JobSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultJobNotifications", "JobSettings_CompanyID", "dbo.JobSettings");
            DropForeignKey("dbo.PurchaseOrderPreviews", "BackTemplateID", "dbo.PurchaseOrderTemplates");
            DropForeignKey("dbo.PurchaseOrderPreviews", "BodyTemplateID", "dbo.PurchaseOrderTemplates");
            DropForeignKey("dbo.PurchaseOrderTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrderPreviews", "FrontTemplateID", "dbo.PurchaseOrderTemplates");
            DropForeignKey("dbo.PurchaseOrderPreviews", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrders", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrderAttachments", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.PurchaseOrders", "ClientDeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.PurchaseOrderComments", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrders", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.PurchaseOrders", "DeliveryAddressID", "dbo.CompanyAddresses");
            DropForeignKey("dbo.PurchaseOrders", "InvoiceAddressID", "dbo.CompanyAddresses");
            DropForeignKey("dbo.PurchaseOrderItems", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderItems", "SectionID", "dbo.PurchaseOrderSections");
            DropForeignKey("dbo.PurchaseOrderSections", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.PurchaseOrderNotifications", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.PurchaseOrders", "StatusID", "dbo.PurchaseOrderStatus");
            DropForeignKey("dbo.PurchaseOrders", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseOrderPreviews", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultPurchaseOrderAttachments", "PurchaseOrderSettings_CompanyID", "dbo.PurchaseOrderSettings");
            DropForeignKey("dbo.PurchaseOrderSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultPurchaseOrderNotifications", "PurchaseOrderSettings_CompanyID", "dbo.PurchaseOrderSettings");
            DropForeignKey("dbo.QuotationSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultQuotationAttachments", "QuotationSettings_CompanyID", "dbo.QuotationSettings");
            DropForeignKey("dbo.QuotationSettings", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.DefaultQuotationNotifications", "QuotationSettings_CompanyID", "dbo.QuotationSettings");
            DropForeignKey("dbo.SubscriptionPlans", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DeliveryNotes", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.DeliveryNoteItemCollectLocations", "DeliveryNoteItemID", "dbo.DeliveryNoteItems");
            DropForeignKey("dbo.StockLocations", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.StockLocations", "ParentLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.DeliveryNoteItemCollectLocations", "StockLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.DeliveryNoteItems", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNoteItems", "JobItemID", "dbo.JobItems");
            DropForeignKey("dbo.DeliveryNoteItems", "ParentItemID", "dbo.DeliveryNoteItems");
            DropForeignKey("dbo.DeliveryNoteSections", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNoteItems", "SectionID", "dbo.DeliveryNoteSections");
            DropForeignKey("dbo.DeliveryNoteNotifications", "DeliveryNoteID", "dbo.DeliveryNotes");
            DropForeignKey("dbo.DeliveryNotes", "StatusID", "dbo.DeliveryNoteStatus");
            DropForeignKey("dbo.ClientAttachments", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientContacts", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "ClientContactID", "dbo.ClientContacts");
            DropForeignKey("dbo.ClientEvents", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientTagItems", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientTagItems", "ClientTagID", "dbo.ClientTags");
            DropForeignKey("dbo.Clients", "ClientTypeID", "dbo.ClientTypes");
            DropForeignKey("dbo.ClientTypes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientEvents", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientEvents", "CreatorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ActivityLogs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ActivityLogs", "ModuleTypeID", "dbo.ModuleTypes");
            DropForeignKey("dbo.ProductStockLocations", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ProductStockLocations", "StockLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.StockLogs", "ActivityByUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.StockLogs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SubscriptionCartItems", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SubscriptionPlanPayments", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SupplierDeliveryItems", "PurchaseOrder_ID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.SupplierDeliveryItems", "PurchaseOrderItemID", "dbo.PurchaseOrderItems");
            DropForeignKey("dbo.SupplierDeliveryNotes", "DefaultStockLocationID", "dbo.StockLocations");
            DropForeignKey("dbo.SupplierDeliveryNotes", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.SupplierInvoices", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.SupplierDeliveryNotes", "SupplierInvoiceID", "dbo.SupplierInvoices");
            DropForeignKey("dbo.SupplierDeliveryItems", "SupplierDeliveryNoteID", "dbo.SupplierDeliveryNotes");
            DropIndex("dbo.ActivityLogs", new[] { "CompanyID" });
            DropIndex("dbo.ActivityLogs", new[] { "ModuleTypeID" });
            DropIndex("dbo.ActivityLogs", new[] { "ActivityByUserID" });
            DropIndex("dbo.ClientEvents", new[] { "CompanyID" });
            DropIndex("dbo.ClientEvents", new[] { "AssignedUserID" });
            DropIndex("dbo.ClientEvents", new[] { "CreatorID" });
            DropIndex("dbo.ClientEvents", new[] { "ClientID" });
            DropIndex("dbo.ClientEvents", new[] { "ClientContactID" });
            DropIndex("dbo.Clients", new[] { "CompanyID" });
            DropIndex("dbo.Clients", new[] { "ClientTypeID" });
            DropIndex("dbo.ClientAddresses", new[] { "ClientID" });
            DropIndex("dbo.DeliveryNotes", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNotes", new[] { "ClientID" });
            DropIndex("dbo.DeliveryNotes", new[] { "AssignedUserID" });
            DropIndex("dbo.DeliveryNotes", new[] { "StatusID" });
            DropIndex("dbo.DeliveryNotes", new[] { "JobID" });
            DropIndex("dbo.DeliveryNotes", new[] { "DeliveryAddressID" });
            DropIndex("dbo.DeliveryNoteAttachments", new[] { "DeliveryNoteID" });
            DropIndex("dbo.DeliveryNoteComments", new[] { "DeliveryNoteID" });
            DropIndex("dbo.ClientTags", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "DeliveryNoteID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.DeliveryNotePreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.DeliveryNoteTemplates", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNoteSettings", new[] { "CompanyID" });
            DropIndex("dbo.DefaultDeliveryNoteAttachments", new[] { "DeliveryNoteSettings_CompanyID" });
            DropIndex("dbo.DefaultDeliveryNoteNotifications", new[] { "DeliveryNoteSettings_CompanyID" });
            DropIndex("dbo.InvoicePreviews", new[] { "InvoiceID" });
            DropIndex("dbo.InvoicePreviews", new[] { "CompanyID" });
            DropIndex("dbo.InvoicePreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.InvoicePreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.InvoicePreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.InvoiceTemplates", new[] { "CompanyID" });
            DropIndex("dbo.Invoices", new[] { "CompanyID" });
            DropIndex("dbo.Invoices", new[] { "ClientID" });
            DropIndex("dbo.Invoices", new[] { "AssignedUserID" });
            DropIndex("dbo.Invoices", new[] { "StatusID" });
            DropIndex("dbo.Invoices", new[] { "CurrencyID" });
            DropIndex("dbo.Invoices", new[] { "QuotationID" });
            DropIndex("dbo.Invoices", new[] { "JobID" });
            DropIndex("dbo.Invoices", new[] { "DeliveryNoteID" });
            DropIndex("dbo.Invoices", new[] { "InvoiceAddressID" });
            DropIndex("dbo.Invoices", new[] { "DeliveryAddressID" });
            DropIndex("dbo.InvoiceAttachments", new[] { "InvoiceID" });
            DropIndex("dbo.InvoiceComments", new[] { "InvoiceID" });
            DropIndex("dbo.InvoiceItems", new[] { "InvoiceID" });
            DropIndex("dbo.InvoiceItems", new[] { "SectionID" });
            DropIndex("dbo.InvoiceItems", new[] { "ParentItemID" });
            DropIndex("dbo.InvoiceSections", new[] { "InvoiceID" });
            DropIndex("dbo.Jobs", new[] { "CompanyID" });
            DropIndex("dbo.Jobs", new[] { "ClientID" });
            DropIndex("dbo.Jobs", new[] { "AssignedUserID" });
            DropIndex("dbo.Jobs", new[] { "StatusID" });
            DropIndex("dbo.Jobs", new[] { "CurrencyID" });
            DropIndex("dbo.Jobs", new[] { "QuotationID" });
            DropIndex("dbo.Jobs", new[] { "InvoiceAddressID" });
            DropIndex("dbo.Jobs", new[] { "DeliveryAddressID" });
            DropIndex("dbo.JobAttachments", new[] { "JobID" });
            DropIndex("dbo.JobComments", new[] { "JobID" });
            DropIndex("dbo.JobItems", new[] { "JobID" });
            DropIndex("dbo.JobItems", new[] { "SectionID" });
            DropIndex("dbo.JobItems", new[] { "ParentItemID" });
            DropIndex("dbo.JobSections", new[] { "JobID" });
            DropIndex("dbo.JobNotifications", new[] { "JobID" });
            DropIndex("dbo.JobPreviews", new[] { "JobID" });
            DropIndex("dbo.JobPreviews", new[] { "CompanyID" });
            DropIndex("dbo.JobPreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.JobPreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.JobPreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.JobTemplates", new[] { "CompanyID" });
            DropIndex("dbo.Quotations", new[] { "CompanyID" });
            DropIndex("dbo.Quotations", new[] { "ClientID" });
            DropIndex("dbo.Quotations", new[] { "AssignedUserID" });
            DropIndex("dbo.Quotations", new[] { "StatusID" });
            DropIndex("dbo.Quotations", new[] { "CurrencyID" });
            DropIndex("dbo.Quotations", new[] { "InvoiceAddressID" });
            DropIndex("dbo.Quotations", new[] { "DeliveryAddressID" });
            DropIndex("dbo.QuotationAttachments", new[] { "QuotationID" });
            DropIndex("dbo.QuotationComments", new[] { "QuotationID" });
            DropIndex("dbo.QuotationItems", new[] { "QuotationID" });
            DropIndex("dbo.QuotationItems", new[] { "SectionID" });
            DropIndex("dbo.QuotationItems", new[] { "ParentItemID" });
            DropIndex("dbo.QuotationSections", new[] { "QuotationID" });
            DropIndex("dbo.QuotationNotifications", new[] { "QuotationID" });
            DropIndex("dbo.QuotationPreviews", new[] { "QuotationID" });
            DropIndex("dbo.QuotationPreviews", new[] { "CompanyID" });
            DropIndex("dbo.QuotationPreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.QuotationPreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.QuotationPreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.QuotationTemplates", new[] { "CompanyID" });
            DropIndex("dbo.InvoiceNotifications", new[] { "InvoiceID" });
            DropIndex("dbo.InvoiceSettings", new[] { "CompanyID" });
            DropIndex("dbo.InvoiceSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.DefaultInvoiceAttachments", new[] { "InvoiceSettings_CompanyID" });
            DropIndex("dbo.DefaultInvoiceNotifications", new[] { "InvoiceSettings_CompanyID" });
            DropIndex("dbo.JobSettings", new[] { "CompanyID" });
            DropIndex("dbo.JobSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.DefaultJobAttachments", new[] { "JobSettings_CompanyID" });
            DropIndex("dbo.DefaultJobNotifications", new[] { "JobSettings_CompanyID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "FrontTemplateID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "BodyTemplateID" });
            DropIndex("dbo.PurchaseOrderPreviews", new[] { "BackTemplateID" });
            DropIndex("dbo.PurchaseOrderTemplates", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrders", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrders", new[] { "ClientID" });
            DropIndex("dbo.PurchaseOrders", new[] { "SupplierID" });
            DropIndex("dbo.PurchaseOrders", new[] { "AssignedUserID" });
            DropIndex("dbo.PurchaseOrders", new[] { "StatusID" });
            DropIndex("dbo.PurchaseOrders", new[] { "CurrencyID" });
            DropIndex("dbo.PurchaseOrders", new[] { "QuotationID" });
            DropIndex("dbo.PurchaseOrders", new[] { "JobID" });
            DropIndex("dbo.PurchaseOrders", new[] { "InvoiceAddressID" });
            DropIndex("dbo.PurchaseOrders", new[] { "DeliveryAddressID" });
            DropIndex("dbo.PurchaseOrders", new[] { "ClientDeliveryAddressID" });
            DropIndex("dbo.PurchaseOrderAttachments", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderComments", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderItems", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderItems", new[] { "SectionID" });
            DropIndex("dbo.PurchaseOrderSections", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderNotifications", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrderSettings", new[] { "CompanyID" });
            DropIndex("dbo.PurchaseOrderSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.DefaultPurchaseOrderAttachments", new[] { "PurchaseOrderSettings_CompanyID" });
            DropIndex("dbo.DefaultPurchaseOrderNotifications", new[] { "PurchaseOrderSettings_CompanyID" });
            DropIndex("dbo.QuotationSettings", new[] { "CompanyID" });
            DropIndex("dbo.QuotationSettings", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.DefaultQuotationAttachments", new[] { "QuotationSettings_CompanyID" });
            DropIndex("dbo.DefaultQuotationNotifications", new[] { "QuotationSettings_CompanyID" });
            DropIndex("dbo.SubscriptionPlans", new[] { "CompanyID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "DeliveryNoteID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "SectionID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "JobItemID" });
            DropIndex("dbo.DeliveryNoteItems", new[] { "ParentItemID" });
            DropIndex("dbo.DeliveryNoteItemCollectLocations", new[] { "DeliveryNoteItemID" });
            DropIndex("dbo.DeliveryNoteItemCollectLocations", new[] { "StockLocationID" });
            DropIndex("dbo.StockLocations", new[] { "CompanyID" });
            DropIndex("dbo.StockLocations", new[] { "ParentLocationID" });
            DropIndex("dbo.DeliveryNoteSections", new[] { "DeliveryNoteID" });
            DropIndex("dbo.DeliveryNoteNotifications", new[] { "DeliveryNoteID" });
            DropIndex("dbo.ClientAttachments", new[] { "ClientID" });
            DropIndex("dbo.ClientContacts", new[] { "ClientID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientTagID" });
            DropIndex("dbo.ClientTypes", new[] { "CompanyID" });
            DropIndex("dbo.ProductStockLocations", new[] { "CompanyID" });
            DropIndex("dbo.ProductStockLocations", new[] { "StockLocationID" });
            DropIndex("dbo.StockLogs", new[] { "CompanyID" });
            DropIndex("dbo.StockLogs", new[] { "ActivityByUserID" });
            DropIndex("dbo.SubscriptionCartItems", new[] { "CompanyID" });
            DropIndex("dbo.SubscriptionPlanPayments", new[] { "CompanyID" });
            DropIndex("dbo.SupplierDeliveryItems", new[] { "SupplierDeliveryNoteID" });
            DropIndex("dbo.SupplierDeliveryItems", new[] { "PurchaseOrderItemID" });
            DropIndex("dbo.SupplierDeliveryItems", new[] { "PurchaseOrder_ID" });
            DropIndex("dbo.SupplierDeliveryNotes", new[] { "PurchaseOrderID" });
            DropIndex("dbo.SupplierDeliveryNotes", new[] { "DefaultStockLocationID" });
            DropIndex("dbo.SupplierDeliveryNotes", new[] { "SupplierInvoiceID" });
            DropIndex("dbo.SupplierInvoices", new[] { "PurchaseOrderID" });
            DropTable("dbo.ActivityLogs");
            DropTable("dbo.ClientEvents");
            DropTable("dbo.Clients");
            DropTable("dbo.ClientAddresses");
            DropTable("dbo.DeliveryNotes");
            DropTable("dbo.DeliveryNoteAttachments");
            DropTable("dbo.DeliveryNoteComments");
            DropTable("dbo.ClientTags");
            DropTable("dbo.DeliveryNotePreviews");
            DropTable("dbo.DeliveryNoteTemplates");
            DropTable("dbo.DeliveryNoteSettings");
            DropTable("dbo.DefaultDeliveryNoteAttachments");
            DropTable("dbo.DefaultDeliveryNoteNotifications");
            DropTable("dbo.InvoicePreviews");
            DropTable("dbo.InvoiceTemplates");
            DropTable("dbo.Invoices");
            DropTable("dbo.InvoiceAttachments");
            DropTable("dbo.InvoiceComments");
            DropTable("dbo.InvoiceItems");
            DropTable("dbo.InvoiceSections");
            DropTable("dbo.Jobs");
            DropTable("dbo.JobAttachments");
            DropTable("dbo.JobComments");
            DropTable("dbo.JobItems");
            DropTable("dbo.JobSections");
            DropTable("dbo.JobNotifications");
            DropTable("dbo.JobPreviews");
            DropTable("dbo.JobTemplates");
            DropTable("dbo.Quotations");
            DropTable("dbo.QuotationAttachments");
            DropTable("dbo.QuotationComments");
            DropTable("dbo.QuotationItems");
            DropTable("dbo.QuotationSections");
            DropTable("dbo.QuotationNotifications");
            DropTable("dbo.QuotationPreviews");
            DropTable("dbo.QuotationTemplates");
            DropTable("dbo.QuotationStatus");
            DropTable("dbo.JobStatus");
            DropTable("dbo.InvoiceNotifications");
            DropTable("dbo.InvoiceStatus");
            DropTable("dbo.InvoiceSettings");
            DropTable("dbo.DefaultInvoiceAttachments");
            DropTable("dbo.DefaultInvoiceNotifications");
            DropTable("dbo.JobSettings");
            DropTable("dbo.DefaultJobAttachments");
            DropTable("dbo.DefaultJobNotifications");
            DropTable("dbo.PurchaseOrderPreviews");
            DropTable("dbo.PurchaseOrderTemplates");
            DropTable("dbo.PurchaseOrders");
            DropTable("dbo.PurchaseOrderAttachments");
            DropTable("dbo.PurchaseOrderComments");
            DropTable("dbo.PurchaseOrderItems");
            DropTable("dbo.PurchaseOrderSections");
            DropTable("dbo.PurchaseOrderNotifications");
            DropTable("dbo.PurchaseOrderStatus");
            DropTable("dbo.PurchaseOrderSettings");
            DropTable("dbo.DefaultPurchaseOrderAttachments");
            DropTable("dbo.DefaultPurchaseOrderNotifications");
            DropTable("dbo.QuotationSettings");
            DropTable("dbo.DefaultQuotationAttachments");
            DropTable("dbo.DefaultQuotationNotifications");
            DropTable("dbo.SubscriptionPlans");
            DropTable("dbo.DeliveryNoteItems");
            DropTable("dbo.DeliveryNoteItemCollectLocations");
            DropTable("dbo.StockLocations");
            DropTable("dbo.DeliveryNoteSections");
            DropTable("dbo.DeliveryNoteNotifications");
            DropTable("dbo.DeliveryNoteStatus");
            DropTable("dbo.ClientAttachments");
            DropTable("dbo.ClientContacts");
            DropTable("dbo.ClientTagItems");
            DropTable("dbo.ClientTypes");
            DropTable("dbo.ModuleTypes");
            DropTable("dbo.ProductStockLocations");
            DropTable("dbo.StockLogs");
            DropTable("dbo.SubscriptionCartItems");
            DropTable("dbo.SubscriptionPlanPayments");
            DropTable("dbo.SubscriptionRequests");
            DropTable("dbo.SupplierDeliveryItems");
            DropTable("dbo.SupplierDeliveryNotes");
            DropTable("dbo.SupplierInvoices");
            DropTable("dbo.WorldPayResponses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.WorldPayResponses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        InstID = c.String(),
                        CartID = c.String(),
                        Desc = c.String(),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Amount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AmountString = c.String(),
                        Currency = c.String(maxLength: 3),
                        AuthMode = c.String(),
                        TestMode = c.String(),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        Region = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        CountryString = c.String(),
                        Tel = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        DelvName = c.String(),
                        DelvAddress1 = c.String(),
                        DelvAddress2 = c.String(),
                        DelvAddress3 = c.String(),
                        DelvTown = c.String(),
                        DelvRegion = c.String(),
                        DelvPostcode = c.String(),
                        DelvCountry = c.String(),
                        DelvCountryString = c.String(),
                        CompName = c.String(),
                        TransID = c.String(),
                        TransStatus = c.String(),
                        TransTime = c.String(),
                        AuthAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AuthCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AuthCurrency = c.String(),
                        AuthAmountString = c.String(),
                        RawAuthMessage = c.String(),
                        RawAuthCode = c.String(),
                        CallbackPW = c.String(),
                        CardType = c.String(),
                        CountryMatch = c.String(),
                        AVS = c.String(),
                        WafMerchMessage = c.String(),
                        Authentication = c.String(),
                        IpAddress = c.String(),
                        Charenc = c.String(),
                        _spCharEnc = c.String(),
                        FuturePayID = c.String(),
                        FuturePayStatusChange = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierInvoices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        SupplierDelieveryNoteID = c.Guid(),
                        TotalNet = c.Double(nullable: false),
                        TotalVat = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierDeliveryNotes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        NoteNumber = c.String(),
                        PurchaseOrderID = c.Guid(nullable: false),
                        DefaultStockLocationID = c.Guid(nullable: false),
                        SupplierInvoiceID = c.Guid(),
                        Note = c.String(),
                        Created = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SupplierDeliveryItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierDeliveryNoteID = c.Guid(nullable: false),
                        PurchaseOrderItemID = c.Guid(nullable: false),
                        QuantityReceived = c.Decimal(nullable: false, precision: 28, scale: 5),
                        NetValue = c.Double(nullable: false),
                        PurchaseOrder_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubscriptionRequests",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Email = c.String(nullable: false),
                        CompanyName = c.String(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubscriptionPlanPayments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Description = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Currency = c.String(maxLength: 3),
                        FuturePayID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubscriptionCartItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        AccessCRM = c.Boolean(nullable: false),
                        AccessProducts = c.Boolean(nullable: false),
                        AccessQuotations = c.Boolean(nullable: false),
                        AccessJobs = c.Boolean(nullable: false),
                        AccessInvoices = c.Boolean(nullable: false),
                        AccessDeliveryNotes = c.Boolean(nullable: false),
                        AccessPurchaseOrders = c.Boolean(nullable: false),
                        AccessStockControl = c.Boolean(nullable: false),
                        InitialAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        RegularAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AmountPerUser = c.Decimal(nullable: false, precision: 28, scale: 5),
                        NumberOfUsers = c.Int(nullable: false),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StockLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        ProductCode = c.String(),
                        ProductDescription = c.String(),
                        LocationName = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ActionType = c.Int(nullable: false),
                        Narrative = c.String(),
                        ActivityByUserID = c.String(maxLength: 128),
                        ModuleTypeID = c.Int(nullable: false),
                        LinkID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductStockLocations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        StockLocationID = c.Guid(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ModuleTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        IndexURL = c.String(),
                        EditURL = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientTagItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        ClientTagID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientContacts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Position = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StockLocations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ParentLocationID = c.Guid(),
                        Name = c.String(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteItemCollectLocations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteItemID = c.Guid(nullable: false),
                        StockLocationID = c.Guid(nullable: false),
                        LocationName = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        JobItemID = c.Guid(),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubscriptionPlans",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Created = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        InitialAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        RegularAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AmountPerUser = c.Decimal(nullable: false, precision: 28, scale: 5),
                        NumberOfUsers = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Trial = c.Boolean(nullable: false),
                        AccessCRM = c.Boolean(nullable: false),
                        AccessProducts = c.Boolean(nullable: false),
                        AccessQuotations = c.Boolean(nullable: false),
                        AccessJobs = c.Boolean(nullable: false),
                        AccessInvoices = c.Boolean(nullable: false),
                        AccessDeliveryNotes = c.Boolean(nullable: false),
                        AccessPurchaseOrders = c.Boolean(nullable: false),
                        AccessStockControl = c.Boolean(nullable: false),
                        FuturePayID = c.String(),
                        InternalCartID = c.Int(nullable: false),
                        CartID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DefaultQuotationNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        QuotationSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DefaultQuotationAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        QuotationSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowMarkup = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        ShowDiscount = c.Boolean(nullable: false),
                        ShowListPrice = c.Boolean(nullable: false),
                        ShowCost = c.Boolean(nullable: false),
                        ShowMargin = c.Boolean(nullable: false),
                        ShowMarginPercentage = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.DefaultPurchaseOrderNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        PurchaseOrderSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DefaultPurchaseOrderAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        PurchaseOrderSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.PurchaseOrderStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        QuantityReceived = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PurchaseOrderID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        SupplierID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        QuotationID = c.Guid(),
                        JobID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        AccountCode = c.String(),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierInvoiceNo = c.String(),
                        SupplierInvoiceVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierInvoiceNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierInvoiceTotal = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SupplierDeliveryNote = c.String(),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        ClientDeliveryAddressID = c.Guid(),
                        DeliverToClient = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        RequiredDate = c.DateTime(nullable: false),
                        SentDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderPreviews",
                c => new
                    {
                        PurchaseOrderID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.PurchaseOrderID);
            
            CreateTable(
                "dbo.DefaultJobNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        JobSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DefaultJobAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        JobSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.DefaultInvoiceNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        InvoiceSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DefaultInvoiceAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        InvoiceSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        DefaultProFormaReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        QueryEmail = c.String(),
                        DefaultPaymentDays = c.Int(nullable: false),
                        DefaultCurrencyID = c.Int(nullable: false),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.InvoiceStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationPreviews",
                c => new
                    {
                        QuotationID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalDiscount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalMargin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Markup = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ListPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MarginPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Quotations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        ClientReference = c.String(),
                        Number = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        PoNumber = c.String(),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalDiscount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalMargin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        EstimatedDelivery = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobPreviews",
                c => new
                    {
                        JobID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.JobID);
            
            CreateTable(
                "dbo.JobNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        QuotationID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        ClientReference = c.String(),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        PoNumber = c.String(),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        EstimatedDelivery = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        IsService = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        InvoiceID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        QuotationID = c.Guid(),
                        JobID = c.Guid(),
                        DeliveryNoteID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        ClientReference = c.String(),
                        IsProForma = c.Boolean(nullable: false),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        PoNumber = c.String(),
                        AccountCode = c.String(),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        InvoiceDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoiceTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InvoicePreviews",
                c => new
                    {
                        InvoiceID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.InvoiceID);
            
            CreateTable(
                "dbo.DefaultDeliveryNoteNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        DeliveryNoteSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DefaultDeliveryNoteAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteSettingsID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                        DeliveryNoteSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.DeliveryNoteTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        FilePath = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNotePreviews",
                c => new
                    {
                        DeliveryNoteID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FrontTemplateID = c.Guid(),
                        BodyTemplateID = c.Guid(),
                        BackTemplateID = c.Guid(),
                    })
                .PrimaryKey(t => t.DeliveryNoteID);
            
            CreateTable(
                "dbo.ClientTags",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNoteAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DeliveryNoteID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DeliveryNotes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        JobID = c.Guid(),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        Number = c.Int(nullable: false),
                        ClientReference = c.String(),
                        Description = c.String(),
                        Note = c.String(),
                        InternalNote = c.String(),
                        PickedBy = c.String(),
                        PoNumber = c.String(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        EstimatedDelivery = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        County = c.String(),
                        Town = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientTypeID = c.Guid(),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Website = c.String(),
                        Notes = c.String(),
                        SageAccountReference = c.String(),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientEvents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        AssignedUserID = c.String(maxLength: 128),
                        CreatorID = c.String(maxLength: 128),
                        ClientID = c.Guid(),
                        ClientContactID = c.Guid(),
                        Timestamp = c.DateTime(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        ModuleTypeID = c.Int(nullable: false),
                        Description = c.String(),
                        ActivityByUserID = c.String(maxLength: 128),
                        LinkID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.SupplierInvoices", "PurchaseOrderID");
            CreateIndex("dbo.SupplierDeliveryNotes", "SupplierInvoiceID");
            CreateIndex("dbo.SupplierDeliveryNotes", "DefaultStockLocationID");
            CreateIndex("dbo.SupplierDeliveryNotes", "PurchaseOrderID");
            CreateIndex("dbo.SupplierDeliveryItems", "PurchaseOrder_ID");
            CreateIndex("dbo.SupplierDeliveryItems", "PurchaseOrderItemID");
            CreateIndex("dbo.SupplierDeliveryItems", "SupplierDeliveryNoteID");
            CreateIndex("dbo.SubscriptionPlanPayments", "CompanyID");
            CreateIndex("dbo.SubscriptionCartItems", "CompanyID");
            CreateIndex("dbo.StockLogs", "ActivityByUserID");
            CreateIndex("dbo.StockLogs", "CompanyID");
            CreateIndex("dbo.ProductStockLocations", "StockLocationID");
            CreateIndex("dbo.ProductStockLocations", "CompanyID");
            CreateIndex("dbo.ClientTypes", "CompanyID");
            CreateIndex("dbo.ClientTagItems", "ClientTagID");
            CreateIndex("dbo.ClientTagItems", "ClientID");
            CreateIndex("dbo.ClientContacts", "ClientID");
            CreateIndex("dbo.ClientAttachments", "ClientID");
            CreateIndex("dbo.DeliveryNoteNotifications", "DeliveryNoteID");
            CreateIndex("dbo.DeliveryNoteSections", "DeliveryNoteID");
            CreateIndex("dbo.StockLocations", "ParentLocationID");
            CreateIndex("dbo.StockLocations", "CompanyID");
            CreateIndex("dbo.DeliveryNoteItemCollectLocations", "StockLocationID");
            CreateIndex("dbo.DeliveryNoteItemCollectLocations", "DeliveryNoteItemID");
            CreateIndex("dbo.DeliveryNoteItems", "ParentItemID");
            CreateIndex("dbo.DeliveryNoteItems", "JobItemID");
            CreateIndex("dbo.DeliveryNoteItems", "SectionID");
            CreateIndex("dbo.DeliveryNoteItems", "DeliveryNoteID");
            CreateIndex("dbo.SubscriptionPlans", "CompanyID");
            CreateIndex("dbo.DefaultQuotationNotifications", "QuotationSettings_CompanyID");
            CreateIndex("dbo.DefaultQuotationAttachments", "QuotationSettings_CompanyID");
            CreateIndex("dbo.QuotationSettings", "DefaultCurrencyID");
            CreateIndex("dbo.QuotationSettings", "CompanyID");
            CreateIndex("dbo.DefaultPurchaseOrderNotifications", "PurchaseOrderSettings_CompanyID");
            CreateIndex("dbo.DefaultPurchaseOrderAttachments", "PurchaseOrderSettings_CompanyID");
            CreateIndex("dbo.PurchaseOrderSettings", "DefaultCurrencyID");
            CreateIndex("dbo.PurchaseOrderSettings", "CompanyID");
            CreateIndex("dbo.PurchaseOrderNotifications", "PurchaseOrderID");
            CreateIndex("dbo.PurchaseOrderSections", "PurchaseOrderID");
            CreateIndex("dbo.PurchaseOrderItems", "SectionID");
            CreateIndex("dbo.PurchaseOrderItems", "PurchaseOrderID");
            CreateIndex("dbo.PurchaseOrderComments", "PurchaseOrderID");
            CreateIndex("dbo.PurchaseOrderAttachments", "PurchaseOrderID");
            CreateIndex("dbo.PurchaseOrders", "ClientDeliveryAddressID");
            CreateIndex("dbo.PurchaseOrders", "DeliveryAddressID");
            CreateIndex("dbo.PurchaseOrders", "InvoiceAddressID");
            CreateIndex("dbo.PurchaseOrders", "JobID");
            CreateIndex("dbo.PurchaseOrders", "QuotationID");
            CreateIndex("dbo.PurchaseOrders", "CurrencyID");
            CreateIndex("dbo.PurchaseOrders", "StatusID");
            CreateIndex("dbo.PurchaseOrders", "AssignedUserID");
            CreateIndex("dbo.PurchaseOrders", "SupplierID");
            CreateIndex("dbo.PurchaseOrders", "ClientID");
            CreateIndex("dbo.PurchaseOrders", "CompanyID");
            CreateIndex("dbo.PurchaseOrderTemplates", "CompanyID");
            CreateIndex("dbo.PurchaseOrderPreviews", "BackTemplateID");
            CreateIndex("dbo.PurchaseOrderPreviews", "BodyTemplateID");
            CreateIndex("dbo.PurchaseOrderPreviews", "FrontTemplateID");
            CreateIndex("dbo.PurchaseOrderPreviews", "CompanyID");
            CreateIndex("dbo.PurchaseOrderPreviews", "PurchaseOrderID");
            CreateIndex("dbo.DefaultJobNotifications", "JobSettings_CompanyID");
            CreateIndex("dbo.DefaultJobAttachments", "JobSettings_CompanyID");
            CreateIndex("dbo.JobSettings", "DefaultCurrencyID");
            CreateIndex("dbo.JobSettings", "CompanyID");
            CreateIndex("dbo.DefaultInvoiceNotifications", "InvoiceSettings_CompanyID");
            CreateIndex("dbo.DefaultInvoiceAttachments", "InvoiceSettings_CompanyID");
            CreateIndex("dbo.InvoiceSettings", "DefaultCurrencyID");
            CreateIndex("dbo.InvoiceSettings", "CompanyID");
            CreateIndex("dbo.InvoiceNotifications", "InvoiceID");
            CreateIndex("dbo.QuotationTemplates", "CompanyID");
            CreateIndex("dbo.QuotationPreviews", "BackTemplateID");
            CreateIndex("dbo.QuotationPreviews", "BodyTemplateID");
            CreateIndex("dbo.QuotationPreviews", "FrontTemplateID");
            CreateIndex("dbo.QuotationPreviews", "CompanyID");
            CreateIndex("dbo.QuotationPreviews", "QuotationID");
            CreateIndex("dbo.QuotationNotifications", "QuotationID");
            CreateIndex("dbo.QuotationSections", "QuotationID");
            CreateIndex("dbo.QuotationItems", "ParentItemID");
            CreateIndex("dbo.QuotationItems", "SectionID");
            CreateIndex("dbo.QuotationItems", "QuotationID");
            CreateIndex("dbo.QuotationComments", "QuotationID");
            CreateIndex("dbo.QuotationAttachments", "QuotationID");
            CreateIndex("dbo.Quotations", "DeliveryAddressID");
            CreateIndex("dbo.Quotations", "InvoiceAddressID");
            CreateIndex("dbo.Quotations", "CurrencyID");
            CreateIndex("dbo.Quotations", "StatusID");
            CreateIndex("dbo.Quotations", "AssignedUserID");
            CreateIndex("dbo.Quotations", "ClientID");
            CreateIndex("dbo.Quotations", "CompanyID");
            CreateIndex("dbo.JobTemplates", "CompanyID");
            CreateIndex("dbo.JobPreviews", "BackTemplateID");
            CreateIndex("dbo.JobPreviews", "BodyTemplateID");
            CreateIndex("dbo.JobPreviews", "FrontTemplateID");
            CreateIndex("dbo.JobPreviews", "CompanyID");
            CreateIndex("dbo.JobPreviews", "JobID");
            CreateIndex("dbo.JobNotifications", "JobID");
            CreateIndex("dbo.JobSections", "JobID");
            CreateIndex("dbo.JobItems", "ParentItemID");
            CreateIndex("dbo.JobItems", "SectionID");
            CreateIndex("dbo.JobItems", "JobID");
            CreateIndex("dbo.JobComments", "JobID");
            CreateIndex("dbo.JobAttachments", "JobID");
            CreateIndex("dbo.Jobs", "DeliveryAddressID");
            CreateIndex("dbo.Jobs", "InvoiceAddressID");
            CreateIndex("dbo.Jobs", "QuotationID");
            CreateIndex("dbo.Jobs", "CurrencyID");
            CreateIndex("dbo.Jobs", "StatusID");
            CreateIndex("dbo.Jobs", "AssignedUserID");
            CreateIndex("dbo.Jobs", "ClientID");
            CreateIndex("dbo.Jobs", "CompanyID");
            CreateIndex("dbo.InvoiceSections", "InvoiceID");
            CreateIndex("dbo.InvoiceItems", "ParentItemID");
            CreateIndex("dbo.InvoiceItems", "SectionID");
            CreateIndex("dbo.InvoiceItems", "InvoiceID");
            CreateIndex("dbo.InvoiceComments", "InvoiceID");
            CreateIndex("dbo.InvoiceAttachments", "InvoiceID");
            CreateIndex("dbo.Invoices", "DeliveryAddressID");
            CreateIndex("dbo.Invoices", "InvoiceAddressID");
            CreateIndex("dbo.Invoices", "DeliveryNoteID");
            CreateIndex("dbo.Invoices", "JobID");
            CreateIndex("dbo.Invoices", "QuotationID");
            CreateIndex("dbo.Invoices", "CurrencyID");
            CreateIndex("dbo.Invoices", "StatusID");
            CreateIndex("dbo.Invoices", "AssignedUserID");
            CreateIndex("dbo.Invoices", "ClientID");
            CreateIndex("dbo.Invoices", "CompanyID");
            CreateIndex("dbo.InvoiceTemplates", "CompanyID");
            CreateIndex("dbo.InvoicePreviews", "BackTemplateID");
            CreateIndex("dbo.InvoicePreviews", "BodyTemplateID");
            CreateIndex("dbo.InvoicePreviews", "FrontTemplateID");
            CreateIndex("dbo.InvoicePreviews", "CompanyID");
            CreateIndex("dbo.InvoicePreviews", "InvoiceID");
            CreateIndex("dbo.DefaultDeliveryNoteNotifications", "DeliveryNoteSettings_CompanyID");
            CreateIndex("dbo.DefaultDeliveryNoteAttachments", "DeliveryNoteSettings_CompanyID");
            CreateIndex("dbo.DeliveryNoteSettings", "CompanyID");
            CreateIndex("dbo.DeliveryNoteTemplates", "CompanyID");
            CreateIndex("dbo.DeliveryNotePreviews", "BackTemplateID");
            CreateIndex("dbo.DeliveryNotePreviews", "BodyTemplateID");
            CreateIndex("dbo.DeliveryNotePreviews", "FrontTemplateID");
            CreateIndex("dbo.DeliveryNotePreviews", "CompanyID");
            CreateIndex("dbo.DeliveryNotePreviews", "DeliveryNoteID");
            CreateIndex("dbo.ClientTags", "CompanyID");
            CreateIndex("dbo.DeliveryNoteComments", "DeliveryNoteID");
            CreateIndex("dbo.DeliveryNoteAttachments", "DeliveryNoteID");
            CreateIndex("dbo.DeliveryNotes", "DeliveryAddressID");
            CreateIndex("dbo.DeliveryNotes", "JobID");
            CreateIndex("dbo.DeliveryNotes", "StatusID");
            CreateIndex("dbo.DeliveryNotes", "AssignedUserID");
            CreateIndex("dbo.DeliveryNotes", "ClientID");
            CreateIndex("dbo.DeliveryNotes", "CompanyID");
            CreateIndex("dbo.ClientAddresses", "ClientID");
            CreateIndex("dbo.Clients", "ClientTypeID");
            CreateIndex("dbo.Clients", "CompanyID");
            CreateIndex("dbo.ClientEvents", "ClientContactID");
            CreateIndex("dbo.ClientEvents", "ClientID");
            CreateIndex("dbo.ClientEvents", "CreatorID");
            CreateIndex("dbo.ClientEvents", "AssignedUserID");
            CreateIndex("dbo.ClientEvents", "CompanyID");
            CreateIndex("dbo.ActivityLogs", "ActivityByUserID");
            CreateIndex("dbo.ActivityLogs", "ModuleTypeID");
            CreateIndex("dbo.ActivityLogs", "CompanyID");
            AddForeignKey("dbo.SupplierDeliveryItems", "SupplierDeliveryNoteID", "dbo.SupplierDeliveryNotes", "ID");
            AddForeignKey("dbo.SupplierDeliveryNotes", "SupplierInvoiceID", "dbo.SupplierInvoices", "ID");
            AddForeignKey("dbo.SupplierInvoices", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.SupplierDeliveryNotes", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.SupplierDeliveryNotes", "DefaultStockLocationID", "dbo.StockLocations", "ID");
            AddForeignKey("dbo.SupplierDeliveryItems", "PurchaseOrderItemID", "dbo.PurchaseOrderItems", "ID");
            AddForeignKey("dbo.SupplierDeliveryItems", "PurchaseOrder_ID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.SubscriptionPlanPayments", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.SubscriptionCartItems", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.StockLogs", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.StockLogs", "ActivityByUserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ProductStockLocations", "StockLocationID", "dbo.StockLocations", "ID");
            AddForeignKey("dbo.ProductStockLocations", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.ActivityLogs", "ModuleTypeID", "dbo.ModuleTypes", "ID");
            AddForeignKey("dbo.ActivityLogs", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.ClientEvents", "CreatorID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ClientEvents", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.ClientTypes", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.Clients", "ClientTypeID", "dbo.ClientTypes", "ID");
            AddForeignKey("dbo.ClientTagItems", "ClientTagID", "dbo.ClientTags", "ID");
            AddForeignKey("dbo.ClientTagItems", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.ClientEvents", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.ClientEvents", "ClientContactID", "dbo.ClientContacts", "ID");
            AddForeignKey("dbo.ClientContacts", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.ClientAttachments", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.DeliveryNotes", "StatusID", "dbo.DeliveryNoteStatus", "ID");
            AddForeignKey("dbo.DeliveryNoteNotifications", "DeliveryNoteID", "dbo.DeliveryNotes", "ID");
            AddForeignKey("dbo.DeliveryNoteItems", "SectionID", "dbo.DeliveryNoteSections", "ID");
            AddForeignKey("dbo.DeliveryNoteSections", "DeliveryNoteID", "dbo.DeliveryNotes", "ID");
            AddForeignKey("dbo.DeliveryNoteItems", "ParentItemID", "dbo.DeliveryNoteItems", "ID");
            AddForeignKey("dbo.DeliveryNoteItems", "JobItemID", "dbo.JobItems", "ID");
            AddForeignKey("dbo.DeliveryNoteItems", "DeliveryNoteID", "dbo.DeliveryNotes", "ID");
            AddForeignKey("dbo.DeliveryNoteItemCollectLocations", "StockLocationID", "dbo.StockLocations", "ID");
            AddForeignKey("dbo.StockLocations", "ParentLocationID", "dbo.StockLocations", "ID");
            AddForeignKey("dbo.StockLocations", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DeliveryNoteItemCollectLocations", "DeliveryNoteItemID", "dbo.DeliveryNoteItems", "ID");
            AddForeignKey("dbo.DeliveryNotes", "DeliveryAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.SubscriptionPlans", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DefaultQuotationNotifications", "QuotationSettings_CompanyID", "dbo.QuotationSettings", "CompanyID");
            AddForeignKey("dbo.QuotationSettings", "DefaultCurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.DefaultQuotationAttachments", "QuotationSettings_CompanyID", "dbo.QuotationSettings", "CompanyID");
            AddForeignKey("dbo.QuotationSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DefaultPurchaseOrderNotifications", "PurchaseOrderSettings_CompanyID", "dbo.PurchaseOrderSettings", "CompanyID");
            AddForeignKey("dbo.PurchaseOrderSettings", "DefaultCurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.DefaultPurchaseOrderAttachments", "PurchaseOrderSettings_CompanyID", "dbo.PurchaseOrderSettings", "CompanyID");
            AddForeignKey("dbo.PurchaseOrderSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.PurchaseOrderPreviews", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.PurchaseOrders", "SupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.PurchaseOrders", "StatusID", "dbo.PurchaseOrderStatus", "ID");
            AddForeignKey("dbo.PurchaseOrders", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.PurchaseOrderNotifications", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.PurchaseOrders", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.PurchaseOrderSections", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.PurchaseOrderItems", "SectionID", "dbo.PurchaseOrderSections", "ID");
            AddForeignKey("dbo.PurchaseOrderItems", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.PurchaseOrders", "InvoiceAddressID", "dbo.CompanyAddresses", "ID");
            AddForeignKey("dbo.PurchaseOrders", "DeliveryAddressID", "dbo.CompanyAddresses", "ID");
            AddForeignKey("dbo.PurchaseOrders", "CurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.PurchaseOrders", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.PurchaseOrderComments", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.PurchaseOrders", "ClientDeliveryAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.PurchaseOrders", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.PurchaseOrderAttachments", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            AddForeignKey("dbo.PurchaseOrders", "AssignedUserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.PurchaseOrderPreviews", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.PurchaseOrderPreviews", "FrontTemplateID", "dbo.PurchaseOrderTemplates", "ID");
            AddForeignKey("dbo.PurchaseOrderTemplates", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.PurchaseOrderPreviews", "BodyTemplateID", "dbo.PurchaseOrderTemplates", "ID");
            AddForeignKey("dbo.PurchaseOrderPreviews", "BackTemplateID", "dbo.PurchaseOrderTemplates", "ID");
            AddForeignKey("dbo.DefaultJobNotifications", "JobSettings_CompanyID", "dbo.JobSettings", "CompanyID");
            AddForeignKey("dbo.JobSettings", "DefaultCurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.DefaultJobAttachments", "JobSettings_CompanyID", "dbo.JobSettings", "CompanyID");
            AddForeignKey("dbo.JobSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DefaultInvoiceNotifications", "InvoiceSettings_CompanyID", "dbo.InvoiceSettings", "CompanyID");
            AddForeignKey("dbo.InvoiceSettings", "DefaultCurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.DefaultInvoiceAttachments", "InvoiceSettings_CompanyID", "dbo.InvoiceSettings", "CompanyID");
            AddForeignKey("dbo.InvoiceSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.InvoicePreviews", "InvoiceID", "dbo.Invoices", "ID");
            AddForeignKey("dbo.Invoices", "StatusID", "dbo.InvoiceStatus", "ID");
            AddForeignKey("dbo.InvoiceNotifications", "InvoiceID", "dbo.Invoices", "ID");
            AddForeignKey("dbo.Jobs", "StatusID", "dbo.JobStatus", "ID");
            AddForeignKey("dbo.Quotations", "StatusID", "dbo.QuotationStatus", "ID");
            AddForeignKey("dbo.QuotationPreviews", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.QuotationPreviews", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.QuotationPreviews", "FrontTemplateID", "dbo.QuotationTemplates", "ID");
            AddForeignKey("dbo.QuotationTemplates", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.QuotationPreviews", "BodyTemplateID", "dbo.QuotationTemplates", "ID");
            AddForeignKey("dbo.QuotationPreviews", "BackTemplateID", "dbo.QuotationTemplates", "ID");
            AddForeignKey("dbo.QuotationNotifications", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.Jobs", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.QuotationSections", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.QuotationItems", "SectionID", "dbo.QuotationSections", "ID");
            AddForeignKey("dbo.QuotationItems", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.QuotationItems", "ParentItemID", "dbo.QuotationItems", "ID");
            AddForeignKey("dbo.Invoices", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.Quotations", "InvoiceAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.Quotations", "DeliveryAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.Quotations", "CurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.Quotations", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.QuotationComments", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.Quotations", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.QuotationAttachments", "QuotationID", "dbo.Quotations", "ID");
            AddForeignKey("dbo.Quotations", "AssignedUserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.JobPreviews", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.JobPreviews", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.JobPreviews", "FrontTemplateID", "dbo.JobTemplates", "ID");
            AddForeignKey("dbo.JobTemplates", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.JobPreviews", "BodyTemplateID", "dbo.JobTemplates", "ID");
            AddForeignKey("dbo.JobPreviews", "BackTemplateID", "dbo.JobTemplates", "ID");
            AddForeignKey("dbo.JobNotifications", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.JobSections", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.JobItems", "SectionID", "dbo.JobSections", "ID");
            AddForeignKey("dbo.JobItems", "ParentItemID", "dbo.JobItems", "ID");
            AddForeignKey("dbo.JobItems", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.Invoices", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.Jobs", "InvoiceAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.DeliveryNotes", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.Jobs", "DeliveryAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.Jobs", "CurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.Jobs", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.JobComments", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.Jobs", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.JobAttachments", "JobID", "dbo.Jobs", "ID");
            AddForeignKey("dbo.Jobs", "AssignedUserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.InvoiceItems", "SectionID", "dbo.InvoiceSections", "ID");
            AddForeignKey("dbo.InvoiceSections", "InvoiceID", "dbo.Invoices", "ID");
            AddForeignKey("dbo.InvoiceItems", "ParentItemID", "dbo.InvoiceItems", "ID");
            AddForeignKey("dbo.InvoiceItems", "InvoiceID", "dbo.Invoices", "ID");
            AddForeignKey("dbo.Invoices", "InvoiceAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.Invoices", "DeliveryNoteID", "dbo.DeliveryNotes", "ID");
            AddForeignKey("dbo.Invoices", "DeliveryAddressID", "dbo.ClientAddresses", "ID");
            AddForeignKey("dbo.Invoices", "CurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.Invoices", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.InvoiceComments", "InvoiceID", "dbo.Invoices", "ID");
            AddForeignKey("dbo.Invoices", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.InvoiceAttachments", "InvoiceID", "dbo.Invoices", "ID");
            AddForeignKey("dbo.Invoices", "AssignedUserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.InvoicePreviews", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.InvoicePreviews", "FrontTemplateID", "dbo.InvoiceTemplates", "ID");
            AddForeignKey("dbo.InvoiceTemplates", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.InvoicePreviews", "BodyTemplateID", "dbo.InvoiceTemplates", "ID");
            AddForeignKey("dbo.InvoicePreviews", "BackTemplateID", "dbo.InvoiceTemplates", "ID");
            AddForeignKey("dbo.DefaultDeliveryNoteNotifications", "DeliveryNoteSettings_CompanyID", "dbo.DeliveryNoteSettings", "CompanyID");
            AddForeignKey("dbo.DefaultDeliveryNoteAttachments", "DeliveryNoteSettings_CompanyID", "dbo.DeliveryNoteSettings", "CompanyID");
            AddForeignKey("dbo.DeliveryNoteSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DeliveryNotes", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DeliveryNotePreviews", "DeliveryNoteID", "dbo.DeliveryNotes", "ID");
            AddForeignKey("dbo.DeliveryNotePreviews", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DeliveryNotePreviews", "FrontTemplateID", "dbo.DeliveryNoteTemplates", "ID");
            AddForeignKey("dbo.DeliveryNoteTemplates", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DeliveryNotePreviews", "BodyTemplateID", "dbo.DeliveryNoteTemplates", "ID");
            AddForeignKey("dbo.DeliveryNotePreviews", "BackTemplateID", "dbo.DeliveryNoteTemplates", "ID");
            AddForeignKey("dbo.ClientTags", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.Clients", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.DeliveryNoteComments", "DeliveryNoteID", "dbo.DeliveryNotes", "ID");
            AddForeignKey("dbo.DeliveryNotes", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.DeliveryNoteAttachments", "DeliveryNoteID", "dbo.DeliveryNotes", "ID");
            AddForeignKey("dbo.DeliveryNotes", "AssignedUserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ClientAddresses", "ClientID", "dbo.Clients", "ID");
            AddForeignKey("dbo.ClientEvents", "AssignedUserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ActivityLogs", "ActivityByUserID", "dbo.AspNetUsers", "Id");
        }
    }
}
