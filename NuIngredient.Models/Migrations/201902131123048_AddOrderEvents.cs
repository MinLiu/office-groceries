namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderEvents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderEventDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OneOffOrderID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        FunctionName = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        Town = c.String(),
                        Postcode = c.String(),
                        RoomName = c.String(),
                        NumberOfGuests = c.String(),
                        EventDate = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        DeliveryDetails = c.String(),
                        SpecialRequirements = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.OneOffOrders", t => t.OneOffOrderID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.OneOffOrderID)
                .Index(t => t.SupplierID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderEventDetails", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.OrderEventDetails", "OneOffOrderID", "dbo.OneOffOrders");
            DropIndex("dbo.OrderEventDetails", new[] { "SupplierID" });
            DropIndex("dbo.OrderEventDetails", new[] { "OneOffOrderID" });
            DropTable("dbo.OrderEventDetails");
        }
    }
}
