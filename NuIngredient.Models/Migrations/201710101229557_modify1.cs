namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.FruitInvoices", "FromDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FruitInvoices", "FromDate", c => c.DateTime(nullable: false));
        }
    }
}
