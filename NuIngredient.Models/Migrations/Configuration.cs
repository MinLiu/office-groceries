﻿namespace NuIngredient.Models.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NuIngredient.Models.SnapDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(NuIngredient.Models.SnapDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //Add roles            
            context.Roles.AddOrUpdate(
                new IdentityRole { Id = "-1", Name = "NI-User" },
                new IdentityRole { Id = "1", Name = "Admin" },
                new IdentityRole { Id = "2", Name = "User" }
            );
            context.SaveChanges();



            //Add Company     

            if (context.Companies.Count() <= 0)
            {
                context.Companies.AddOrUpdate(
                    new Company
                    {
                        ID = Guid.Parse("bff01aac-69fe-e611-bf05-e03f495631f0"),
                        Name = "NuIngredient",
                        Email = "developer@fruitfulgroup.com",
                        SignupDate = DateTime.Now,
                        AccountClosed = false,
                        DefaultCurrencyID = 1,
                    }
                );
                context.SaveChanges();
            }

            var companyGiud = context.Companies.First().ID;


            //context.SubscriptionPlans.AddOrUpdate(
            //    new SubscriptionPlan
            //    {
            //        ID = companyGiud,
            //        Name = "Developer Subscription Plan",
            //        CompanyID = companyGiud,
            //        NumberOfUsers = 9999,
            //        StartDate = DateTime.Today.AddYears(-1),
            //        EndDate = DateTime.Today.AddYears(10),
            //        AccessCRM = true,
            //        AccessDeliveryNotes = true,
            //        AccessInvoices = true,
            //        AccessProducts = true,
            //        AccessQuotations = true,
            //        AccessJobs = true,
            //        Active = false,
            //        Created = DateTime.Today.AddYears(-1),
            //    }
            //);
            //context.SaveChanges();

            //Add Default Users Passwords : "Dev123!" 
            if (context.Users.Where(u => u.Id == "abe7ab8c-0063-48c2-9130-060ffc8169e1").LongCount() <= 0)
            {
                context.Users.Add(
                    new User
                    {
                        Id = "abe7ab8c-0063-48c2-9130-060ffc8169e1",
                        Email = "developer@fruitfulgroup.com",
                        UserName = "developer@fruitfulgroup.com",
                        PasswordHash = "AIh69MUSNlJKo1QGyT2tC1OSZK8NX3VM/x3gd6LLIRbhHO3WVNF+8FqVjv3XKnPN1A==",
                        SecurityStamp = "b75b58c7-4318-4cbf-99c7-8429856c9d0b",
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = false,
                        AccessFailedCount = 0,
                        CompanyID = companyGiud,
                        Roles = { new IdentityUserRole { UserId = "abe7ab8c-0063-48c2-9130-060ffc8169e1", RoleId = "-1" } },
                    }
                );
            }

            //if (context.Users.Where(u => u.Id == "dbe0447f-9dd6-4e7f-8759-9bb37454ea2d").LongCount() <= 0)
            //{
            //    context.Users.Add(
            //        new User
            //        {
            //            Id = "dbe0447f-9dd6-4e7f-8759-9bb37454ea2d",
            //            Email = "user@fruitfulgroup.com",
            //            UserName = "user@fruitfulgroup.com",
            //            PasswordHash = "AEWJ+U6SRhVCeB1cgPI7R+/8ioCgmiYXfhx7FNzVGud9XQTer7IfTLVPxH4LUdf11A==",
            //            SecurityStamp = "915105c3-6f47-40b1-a88a-aeb74846cf5c",
            //            EmailConfirmed = true,
            //            PhoneNumberConfirmed = false,
            //            TwoFactorEnabled = false,
            //            LockoutEnabled = false,
            //            AccessFailedCount = 0,
            //            CompanyID = companyGiud,
            //            Roles = { new IdentityUserRole { UserId = "dbe0447f-9dd6-4e7f-8759-9bb37454ea2d", RoleId = "1" } },
            //            AccessCRM = true,
            //            AccessDeliveryNotes = true,
            //            AccessInvoices = true,
            //            AccessProducts = true,
            //            AccessQuotations = true,
            //            AccessJobs = true,
            //        }
            //    );
            //}

            //context.SaveChanges();

            if (context.EmailSettings.Count() <= 0)
            {
                context.EmailSettings.AddOrUpdate(
                    new Fruitful.Email.EmailSetting
                    {
                        Email = "noreply@snap-suite.com",
                        Password = "SDb&4*2chh",
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSSL = true
                    }
                );
            }

            if (context.Settings.Count() <= 0)
            {
                context.Settings.AddOrUpdate(
                    new Setting
                    {
                        MilkSupplierCenterEmail = "min@fruitfulgroup.com",
                        FruitSupplierCenterEmail = "min@fruitfulgroup.com",
                        DryGoodsSupplierCenterEmail = "min@fruitfulgroup.com",
                        DefaultReference = "OG",
                        StartingNumber = 1000
                    }
                );
            }


            context.FeedbackTypes.AddOrUpdate(
                new FeedbackType { ID = FeedbackTypeValues.Milk, Name = "Milk", Colour = "#4286f4" },
                new FeedbackType { ID = FeedbackTypeValues.Fruits, Name = "Fruits", Colour = "#7e4bc1" },
                new FeedbackType { ID = FeedbackTypeValues.DryGoods, Name = "Dry Goods", Colour = "#37a7c6" },
                new FeedbackType { ID = FeedbackTypeValues.Others, Name = "Others", Colour = "#c48236" }
            );

            context.Target.AddOrUpdate(
                new Target { ID = TargetValues.Milk, Name = "Milk", Colour = "#4286f4", Active = true },
                new Target { ID = TargetValues.Fruits, Name = "Fruits", Colour = "#7e4bc1", Active = true },
                new Target { ID = TargetValues.DryGoods, Name = "Dry Goods", Colour = "#37a7c6", Active = true }
            );

            if (!context.Aisles.Where(x => x.Name == "Milk").Any())
            { 
                context.Aisles.AddOrUpdate(
                    new Aisle { ID = Guid.Parse("31651989-9F45-455C-97D2-9870B162C723"), Name = "Milk", Label = "Fresh Milk Aisle", IsConstant = true, StartDate = new DateTime(2018, 01, 01), Deleted = false, SortPosition = 0, MetaTitle = "Fresh Milk", MetaKeyword = "Milk, Delivery, Fresh", MetaDescription = "Office-Groceries provides fresh milk delivery to your office everyday", LandingPageFilePath = "/Aisles/Milk.html", ProspectContentFilePath = "/Milk Aisle/_MilkContent_Prospects.html", CustomerContentFilePath = "/Milk Aisle/_MilkContent.html" },
                    new Aisle { ID = Guid.Parse("83A43007-5D66-4D14-A446-C64ABBB794FA"), Name = "Fruit", Label = "Fresh Fruit Aisle", IsConstant = true, StartDate = new DateTime(2018, 01, 01), Deleted = false, SortPosition = 1, MetaTitle = "Fresh Fruits", MetaKeyword = "Fruits, Delivery, Fresh", MetaDescription = "Office-Groceries provides fresh fruit delivery to your office everyday", LandingPageFilePath = "/Aisles/Fruit.html", ProspectContentFilePath = "/Fruit Aisle/_FruitContent_Prospects.html", CustomerContentFilePath = "/Fruit Aisle/_FruitContent.html" },
                    new Aisle { ID = Guid.Parse("15201A14-3FF2-4E36-9FC8-6477C75C7A7A"), Name = "Dry Goods", Label = "Dry Goods Aisle", IsConstant = true, StartDate = new DateTime(2018, 01, 01), Deleted = false, SortPosition = 2, MetaTitle = "Dry Goods", MetaKeyword = "Dry Goods, Delivery", MetaDescription = "Office-Groceries provides delivery to your office everyday", LandingPageFilePath = "/Aisles/Dry Goods.html", ProspectContentFilePath = "/Dry Goods Aisle/_DryGoodsContent_Prospects.html", CustomerContentFilePath = "/Dry Goods Aisle/_DryGoodsContent.html" }
                );
            }

            context.SaveChanges();
            /*
            context.SubscriptionPlans.AddOrUpdate(
                new SubscriptionPlan { ID = 1, Name = "Personal", Description = "SNAP Suite Personal with single user", DaysValid = 30, Price = 15, NumberOfUsers = 1 },
                new SubscriptionPlan { ID = 2, Name = "Professional 5", Description = "SNAP Suite Professional 5 with five users", DaysValid = 30, Price = 70, NumberOfUsers = 5 },
                new SubscriptionPlan { ID = 3, Name = "Professional 10", Description = "SNAP Suite Professional 10 with ten users", DaysValid = 30, Price = 130, NumberOfUsers = 10 }
            );
            */

        }
    }
}
