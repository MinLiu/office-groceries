﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFruitHeader : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrderHeaders", "SnackSupplierID", c => c.Guid());
            AddColumn("dbo.FruitOrderHeaders", "SnackAccountNo", c => c.String());
            CreateIndex("dbo.FruitOrderHeaders", "SnackSupplierID");
            AddForeignKey("dbo.FruitOrderHeaders", "SnackSupplierID", "dbo.Suppliers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitOrderHeaders", "SnackSupplierID", "dbo.Suppliers");
            DropIndex("dbo.FruitOrderHeaders", new[] { "SnackSupplierID" });
            DropColumn("dbo.FruitOrderHeaders", "SnackAccountNo");
            DropColumn("dbo.FruitOrderHeaders", "SnackSupplierID");
        }
    }
}
