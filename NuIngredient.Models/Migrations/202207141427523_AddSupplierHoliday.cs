﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierHoliday : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Holidays", "SupplierID", c => c.Guid());
            CreateIndex("dbo.Holidays", "SupplierID");
            AddForeignKey("dbo.Holidays", "SupplierID", "dbo.Suppliers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Holidays", "SupplierID", "dbo.Suppliers");
            DropIndex("dbo.Holidays", new[] { "SupplierID" });
            DropColumn("dbo.Holidays", "SupplierID");
        }
    }
}
