namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifySupplier2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "HasFruitSupplier", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "HasMilkSupplier", c => c.Boolean(nullable: false));
            AddColumn("dbo.Suppliers", "SupplierType", c => c.Int(nullable: false));
            AddColumn("dbo.Suppliers", "Email", c => c.String(nullable: false));
            AddColumn("dbo.Suppliers", "Telephone", c => c.String());
            DropColumn("dbo.Companies", "HasSupplier");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Companies", "HasSupplier", c => c.Boolean(nullable: false));
            DropColumn("dbo.Suppliers", "Telephone");
            DropColumn("dbo.Suppliers", "Email");
            DropColumn("dbo.Suppliers", "SupplierType");
            DropColumn("dbo.Companies", "HasMilkSupplier");
            DropColumn("dbo.Companies", "HasFruitSupplier");
        }
    }
}
