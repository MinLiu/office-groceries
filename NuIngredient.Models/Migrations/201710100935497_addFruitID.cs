namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFruitID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitInvoices", "FruitID", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FruitInvoices", "FruitID");
        }
    }
}
