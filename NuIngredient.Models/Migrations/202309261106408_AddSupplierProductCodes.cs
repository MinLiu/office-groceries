﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierProductCodes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierFruitProductCodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FruitProductID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitProducts", t => t.FruitProductID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.FruitProductID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.SupplierMilkProductCodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        MilkProductID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkProducts", t => t.MilkProductID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.MilkProductID)
                .Index(t => t.SupplierID);
            
            AddColumn("dbo.Suppliers", "DailyEmailReminderEnabled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierMilkProductCodes", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierMilkProductCodes", "MilkProductID", "dbo.MilkProducts");
            DropForeignKey("dbo.SupplierFruitProductCodes", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierFruitProductCodes", "FruitProductID", "dbo.FruitProducts");
            DropIndex("dbo.SupplierMilkProductCodes", new[] { "SupplierID" });
            DropIndex("dbo.SupplierMilkProductCodes", new[] { "MilkProductID" });
            DropIndex("dbo.SupplierFruitProductCodes", new[] { "SupplierID" });
            DropIndex("dbo.SupplierFruitProductCodes", new[] { "FruitProductID" });
            DropColumn("dbo.Suppliers", "DailyEmailReminderEnabled");
            DropTable("dbo.SupplierMilkProductCodes");
            DropTable("dbo.SupplierFruitProductCodes");
        }
    }
}
