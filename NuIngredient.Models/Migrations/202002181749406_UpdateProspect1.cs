namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProspect1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enquiries", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Enquiries", "DateDeleted", c => c.DateTime());
            AddColumn("dbo.Prospects", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Prospects", "DateDeleted", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prospects", "DateDeleted");
            DropColumn("dbo.Prospects", "Deleted");
            DropColumn("dbo.Enquiries", "DateDeleted");
            DropColumn("dbo.Enquiries", "Deleted");
        }
    }
}
