namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMessageAttachments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MessageAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        MessageID = c.Guid(),
                        FileName = c.String(),
                        FilePath = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Messages", t => t.MessageID)
                .Index(t => t.MessageID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MessageAttachments", "MessageID", "dbo.Messages");
            DropIndex("dbo.MessageAttachments", new[] { "MessageID" });
            DropTable("dbo.MessageAttachments");
        }
    }
}
