﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierDepots : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierDepots",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Name = c.String(),
                        Telephone = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
            AddColumn("dbo.CompanySuppliers", "SupplierDepotID", c => c.Guid());
            CreateIndex("dbo.CompanySuppliers", "SupplierDepotID");
            AddForeignKey("dbo.CompanySuppliers", "SupplierDepotID", "dbo.SupplierDepots", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CompanySuppliers", "SupplierDepotID", "dbo.SupplierDepots");
            DropForeignKey("dbo.SupplierDepots", "SupplierID", "dbo.Suppliers");
            DropIndex("dbo.SupplierDepots", new[] { "SupplierID" });
            DropIndex("dbo.CompanySuppliers", new[] { "SupplierDepotID" });
            DropColumn("dbo.CompanySuppliers", "SupplierDepotID");
            DropTable("dbo.SupplierDepots");
        }
    }
}
