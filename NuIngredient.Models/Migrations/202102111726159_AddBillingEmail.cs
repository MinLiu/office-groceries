namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBillingEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "BillingCompanyName", c => c.String());
            AddColumn("dbo.Companies", "BillingEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "BillingEmail");
            DropColumn("dbo.Companies", "BillingCompanyName");
        }
    }
}
