namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "FruitSupplierID", c => c.Guid());
            AddColumn("dbo.Companies", "MilkSupplierID", c => c.Guid());
            AddColumn("dbo.Companies", "HasSupplier", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "Live", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Companies", "FruitSupplierID");
            CreateIndex("dbo.Companies", "MilkSupplierID");
            AddForeignKey("dbo.Companies", "FruitSupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.Companies", "MilkSupplierID", "dbo.Suppliers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "MilkSupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.Companies", "FruitSupplierID", "dbo.Suppliers");
            DropIndex("dbo.Companies", new[] { "MilkSupplierID" });
            DropIndex("dbo.Companies", new[] { "FruitSupplierID" });
            DropColumn("dbo.Companies", "Live");
            DropColumn("dbo.Companies", "HasSupplier");
            DropColumn("dbo.Companies", "MilkSupplierID");
            DropColumn("dbo.Companies", "FruitSupplierID");
        }
    }
}
