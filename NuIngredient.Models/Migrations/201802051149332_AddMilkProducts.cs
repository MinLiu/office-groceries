namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMilkProducts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MilkProductCategories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ParentID = c.Guid(),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkProductCategories", t => t.ParentID)
                .Index(t => t.ParentID);
            
            CreateTable(
                "dbo.MilkProductExclusivePrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CusomterID = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MilkProduct_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CusomterID)
                .ForeignKey("dbo.FruitProducts", t => t.ProductID)
                .ForeignKey("dbo.MilkProducts", t => t.MilkProduct_ID)
                .Index(t => t.ProductID)
                .Index(t => t.CusomterID)
                .Index(t => t.MilkProduct_ID);
            
            CreateTable(
                "dbo.MilkProducts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Name = c.String(),
                        ShortDescription = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        ImageURL = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        IsProspectsVisible = c.Boolean(nullable: false),
                        IsExclusive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkProductCategories", t => t.CategoryID)
                .Index(t => t.CategoryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkProductExclusivePrices", "MilkProduct_ID", "dbo.MilkProducts");
            DropForeignKey("dbo.MilkProducts", "CategoryID", "dbo.MilkProductCategories");
            DropForeignKey("dbo.MilkProductExclusivePrices", "ProductID", "dbo.FruitProducts");
            DropForeignKey("dbo.MilkProductExclusivePrices", "CusomterID", "dbo.Companies");
            DropForeignKey("dbo.MilkProductCategories", "ParentID", "dbo.MilkProductCategories");
            DropIndex("dbo.MilkProducts", new[] { "CategoryID" });
            DropIndex("dbo.MilkProductExclusivePrices", new[] { "MilkProduct_ID" });
            DropIndex("dbo.MilkProductExclusivePrices", new[] { "CusomterID" });
            DropIndex("dbo.MilkProductExclusivePrices", new[] { "ProductID" });
            DropIndex("dbo.MilkProductCategories", new[] { "ParentID" });
            DropTable("dbo.MilkProducts");
            DropTable("dbo.MilkProductExclusivePrices");
            DropTable("dbo.MilkProductCategories");
        }
    }
}
