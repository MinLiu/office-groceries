namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitOrderFeedback : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Complaints", "FruitOrderHeaderID", c => c.Guid());
            CreateIndex("dbo.Complaints", "FruitOrderHeaderID");
            AddForeignKey("dbo.Complaints", "FruitOrderHeaderID", "dbo.FruitOrderHeaders", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Complaints", "FruitOrderHeaderID", "dbo.FruitOrderHeaders");
            DropIndex("dbo.Complaints", new[] { "FruitOrderHeaderID" });
            DropColumn("dbo.Complaints", "FruitOrderHeaderID");
        }
    }
}
