namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMarkToTransferredCustomers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "IsTransferred", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "TempPlainPassword", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "TempPlainPassword");
            DropColumn("dbo.Companies", "IsTransferred");
        }
    }
}
