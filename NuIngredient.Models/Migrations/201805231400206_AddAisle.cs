namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAisle : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Complaints", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Complaints", "ReplierID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Complaints", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Complaints", "FruitOrderHeaderID", "dbo.FruitOrderHeaders");
            DropForeignKey("dbo.Complaints", "MilkOrderHeaderID", "dbo.MilkOrderHeaders");
            DropForeignKey("dbo.Complaints", "OneOffOrderID", "dbo.OneOffOrders");
            DropIndex("dbo.Complaints", new[] { "CompanyID" });
            DropIndex("dbo.Complaints", new[] { "UserID" });
            DropIndex("dbo.Complaints", new[] { "ReplierID" });
            DropIndex("dbo.Complaints", new[] { "FruitOrderHeaderID" });
            DropIndex("dbo.Complaints", new[] { "MilkOrderHeaderID" });
            DropIndex("dbo.Complaints", new[] { "OneOffOrderID" });
            CreateTable(
                "dbo.Aisles",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.Complaints");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Complaints",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        UserID = c.String(maxLength: 128),
                        Message = c.String(),
                        Created = c.DateTime(nullable: false),
                        NIComment = c.String(),
                        ReplierID = c.String(maxLength: 128),
                        ResponseTime = c.DateTime(),
                        FruitOrderHeaderID = c.Guid(),
                        MilkOrderHeaderID = c.Guid(),
                        OneOffOrderID = c.Guid(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.Aisles");
            CreateIndex("dbo.Complaints", "OneOffOrderID");
            CreateIndex("dbo.Complaints", "MilkOrderHeaderID");
            CreateIndex("dbo.Complaints", "FruitOrderHeaderID");
            CreateIndex("dbo.Complaints", "ReplierID");
            CreateIndex("dbo.Complaints", "UserID");
            CreateIndex("dbo.Complaints", "CompanyID");
            AddForeignKey("dbo.Complaints", "OneOffOrderID", "dbo.OneOffOrders", "ID");
            AddForeignKey("dbo.Complaints", "MilkOrderHeaderID", "dbo.MilkOrderHeaders", "ID");
            AddForeignKey("dbo.Complaints", "FruitOrderHeaderID", "dbo.FruitOrderHeaders", "ID");
            AddForeignKey("dbo.Complaints", "UserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Complaints", "ReplierID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Complaints", "CompanyID", "dbo.Companies", "ID");
        }
    }
}
