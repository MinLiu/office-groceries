namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitOrderTracker : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitOneOffChangeTrackers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FruitProductID = c.Guid(nullable: false),
                        Monday = c.Boolean(nullable: false),
                        Tuesday = c.Boolean(nullable: false),
                        Wednesday = c.Boolean(nullable: false),
                        Thursday = c.Boolean(nullable: false),
                        Friday = c.Boolean(nullable: false),
                        Saturday = c.Boolean(nullable: false),
                        OrderHeaderID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitProducts", t => t.FruitProductID)
                .ForeignKey("dbo.FruitOrderHeaders", t => t.OrderHeaderID)
                .Index(t => t.FruitProductID)
                .Index(t => t.OrderHeaderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitOneOffChangeTrackers", "OrderHeaderID", "dbo.FruitOrderHeaders");
            DropForeignKey("dbo.FruitOneOffChangeTrackers", "FruitProductID", "dbo.FruitProducts");
            DropIndex("dbo.FruitOneOffChangeTrackers", new[] { "OrderHeaderID" });
            DropIndex("dbo.FruitOneOffChangeTrackers", new[] { "FruitProductID" });
            DropTable("dbo.FruitOneOffChangeTrackers");
        }
    }
}
