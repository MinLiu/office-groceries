namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FruitInvoice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitInvoices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ImageURL = c.String(),
                        ProductName = c.String(),
                        Unit = c.String(),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Monday = c.Int(nullable: false),
                        Tuesday = c.Int(nullable: false),
                        Wednesday = c.Int(nullable: false),
                        Thursday = c.Int(nullable: false),
                        Friday = c.Int(nullable: false),
                        Saturday = c.Int(nullable: false),
                        Sunday = c.Int(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        FromDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitInvoices", "CompanyID", "dbo.Companies");
            DropIndex("dbo.FruitInvoices", new[] { "CompanyID" });
            DropTable("dbo.FruitInvoices");
        }
    }
}
