namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSupplier2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "WeeklyEmailReminderEnabled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "WeeklyEmailReminderEnabled");
        }
    }
}
