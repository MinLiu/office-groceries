namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInternalNAme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "InternalName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "InternalName");
        }
    }
}
