﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierOpenDay : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierOpenDays",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierOpenDays", "SupplierID", "dbo.Suppliers");
            DropIndex("dbo.SupplierOpenDays", new[] { "SupplierID" });
            DropTable("dbo.SupplierOpenDays");
        }
    }
}
