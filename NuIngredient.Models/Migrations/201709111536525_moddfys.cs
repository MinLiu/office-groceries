namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moddfys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MilkOrders", "Unit", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkOrders", "Unit");
        }
    }
}
