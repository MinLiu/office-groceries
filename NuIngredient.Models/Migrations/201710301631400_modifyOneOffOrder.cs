namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyOneOffOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OneOffOrders", "CompanyName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OneOffOrders", "CompanyName");
        }
    }
}
