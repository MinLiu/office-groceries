namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionOffer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PromotionOffers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Subject = c.String(),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PromotionOffers");
        }
    }
}
