﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIndex : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FruitOrderHeaders", "ProspectToken", c => c.String(maxLength: 50));
            AlterColumn("dbo.MilkOrderHeaders", "ProspectToken", c => c.String(maxLength: 50));
            AlterColumn("dbo.ShoppingCartItems", "ProspectToken", c => c.String(maxLength: 50));
            CreateIndex("dbo.FruitOrderHeaders", "ProspectToken");
            CreateIndex("dbo.MilkOrderHeaders", "ProspectToken");
            CreateIndex("dbo.ShoppingCartItems", "ProspectToken");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ShoppingCartItems", new[] { "ProspectToken" });
            DropIndex("dbo.MilkOrderHeaders", new[] { "ProspectToken" });
            DropIndex("dbo.FruitOrderHeaders", new[] { "ProspectToken" });
            AlterColumn("dbo.ShoppingCartItems", "ProspectToken", c => c.String());
            AlterColumn("dbo.MilkOrderHeaders", "ProspectToken", c => c.String());
            AlterColumn("dbo.FruitOrderHeaders", "ProspectToken", c => c.String());
        }
    }
}
