namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deleteCurrency : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Companies", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductCosts", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Products", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductPrices", "CurrencyID", "dbo.Currencies");
            DropIndex("dbo.Companies", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.Products", new[] { "CurrencyID" });
            DropIndex("dbo.ProductCosts", new[] { "CurrencyID" });
            DropIndex("dbo.ProductPrices", new[] { "CurrencyID" });
            DropColumn("dbo.ProductCosts", "CurrencyID");
            DropColumn("dbo.ProductPrices", "CurrencyID");
            DropTable("dbo.Currencies");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 3),
                        Name = c.String(nullable: false, maxLength: 128),
                        Symbol = c.String(nullable: false, maxLength: 3),
                        UseInCommerce = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.ProductPrices", "CurrencyID", c => c.Int(nullable: false));
            AddColumn("dbo.ProductCosts", "CurrencyID", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductPrices", "CurrencyID");
            CreateIndex("dbo.ProductCosts", "CurrencyID");
            CreateIndex("dbo.Products", "CurrencyID");
            CreateIndex("dbo.Companies", "DefaultCurrencyID");
            AddForeignKey("dbo.ProductPrices", "CurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.Products", "CurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.ProductCosts", "CurrencyID", "dbo.Currencies", "ID");
            AddForeignKey("dbo.Companies", "DefaultCurrencyID", "dbo.Currencies", "ID");
        }
    }
}
