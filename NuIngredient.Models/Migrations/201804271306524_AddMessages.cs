namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMessages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Title = c.String(),
                        Content = c.String(),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MessageTargets",
                c => new
                    {
                        MessageID = c.Guid(nullable: false),
                        TargetID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MessageID, t.TargetID })
                .ForeignKey("dbo.Messages", t => t.MessageID)
                .ForeignKey("dbo.Targets", t => t.TargetID)
                .Index(t => t.MessageID)
                .Index(t => t.TargetID);
            
            CreateTable(
                "dbo.Targets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Colour = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MessageTargets", "TargetID", "dbo.Targets");
            DropForeignKey("dbo.MessageTargets", "MessageID", "dbo.Messages");
            DropIndex("dbo.MessageTargets", new[] { "TargetID" });
            DropIndex("dbo.MessageTargets", new[] { "MessageID" });
            DropTable("dbo.Targets");
            DropTable("dbo.MessageTargets");
            DropTable("dbo.Messages");
        }
    }
}
