﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWeeklyReminderHour : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "WeeklyEmailReminderHour", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "WeeklyEmailReminderHour");
        }
    }
}
