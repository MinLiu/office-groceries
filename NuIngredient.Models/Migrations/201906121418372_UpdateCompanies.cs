namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCompanies : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "EasySuppliersActivated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "EasySuppliersActivated");
        }
    }
}
