namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSetting1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "FeedbackCenterEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "FeedbackCenterEmail");
        }
    }
}
