namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addOrderMode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrders", "FromDate", c => c.DateTime());
            AddColumn("dbo.FruitOrders", "OrderMode", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FruitOrders", "OrderMode");
            DropColumn("dbo.FruitOrders", "FromDate");
        }
    }
}
