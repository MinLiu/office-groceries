namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAisle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aisles", "LandingPageContent", c => c.String());
            AddColumn("dbo.Aisles", "ProspectContent", c => c.String());
            AddColumn("dbo.Aisles", "CustomerContent", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Aisles", "CustomerContent");
            DropColumn("dbo.Aisles", "ProspectContent");
            DropColumn("dbo.Aisles", "LandingPageContent");
        }
    }
}
