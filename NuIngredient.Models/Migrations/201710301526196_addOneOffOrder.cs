namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addOneOffOrder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OneOffOrderItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrderHeaderID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ProductID = c.Guid(),
                        ProductName = c.String(),
                        ImageURL = c.String(),
                        ProductPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ProductCode = c.String(),
                        Qty = c.Int(nullable: false),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.OneOffOrders", t => t.OrderHeaderID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.OrderHeaderID)
                .Index(t => t.CompanyID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.OneOffOrders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        OrderNumber = c.String(),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        EmailAddress = c.String(),
                        Phone = c.String(),
                        DeliveryAddress = c.String(),
                        DeliveryCounty = c.String(),
                        DeliveryPostCode = c.String(),
                        BillingAddress = c.String(),
                        BillingCounty = c.String(),
                        BillingPostCode = c.String(),
                        OrderNotes = c.String(),
                        Created = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OneOffOrderItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.OneOffOrderItems", "OrderHeaderID", "dbo.OneOffOrders");
            DropForeignKey("dbo.OneOffOrders", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.OneOffOrderItems", "CompanyID", "dbo.Companies");
            DropIndex("dbo.OneOffOrders", new[] { "CompanyID" });
            DropIndex("dbo.OneOffOrderItems", new[] { "ProductID" });
            DropIndex("dbo.OneOffOrderItems", new[] { "CompanyID" });
            DropIndex("dbo.OneOffOrderItems", new[] { "OrderHeaderID" });
            DropTable("dbo.OneOffOrders");
            DropTable("dbo.OneOffOrderItems");
        }
    }
}
