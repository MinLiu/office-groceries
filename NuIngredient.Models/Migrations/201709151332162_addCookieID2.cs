namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCookieID2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrders", "ProspectToken", c => c.String());
            AddColumn("dbo.MilkOrders", "ProspectToken", c => c.String());
            DropColumn("dbo.FruitOrders", "CookieID");
            DropColumn("dbo.MilkOrders", "CookieID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MilkOrders", "CookieID", c => c.String());
            AddColumn("dbo.FruitOrders", "CookieID", c => c.String());
            DropColumn("dbo.MilkOrders", "ProspectToken");
            DropColumn("dbo.FruitOrders", "ProspectToken");
        }
    }
}
