namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCompany1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "DryGoodsDeliveryMonday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "DryGoodsDeliveryTuesday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "DryGoodsDeliveryWednesday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "DryGoodsDeliveryThursday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "DryGoodsDeliveryFriday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "DryGoodsDeliverySaturday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "PermanentFruiPONo", c => c.String());
            AddColumn("dbo.Companies", "PermanentMilkPONo", c => c.String());
            AddColumn("dbo.Companies", "PermanentDryGoodsPONo", c => c.String());
            DropColumn("dbo.Companies", "Motto");
            DropColumn("dbo.Companies", "Country");
            DropColumn("dbo.Companies", "Mobile");
            DropColumn("dbo.Companies", "Fax");
            DropColumn("dbo.Companies", "Website");
            DropColumn("dbo.Companies", "LogoImageURL");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Companies", "LogoImageURL", c => c.String());
            AddColumn("dbo.Companies", "Website", c => c.String());
            AddColumn("dbo.Companies", "Fax", c => c.String());
            AddColumn("dbo.Companies", "Mobile", c => c.String());
            AddColumn("dbo.Companies", "Country", c => c.String());
            AddColumn("dbo.Companies", "Motto", c => c.String());
            DropColumn("dbo.Companies", "PermanentDryGoodsPONo");
            DropColumn("dbo.Companies", "PermanentMilkPONo");
            DropColumn("dbo.Companies", "PermanentFruiPONo");
            DropColumn("dbo.Companies", "DryGoodsDeliverySaturday");
            DropColumn("dbo.Companies", "DryGoodsDeliveryFriday");
            DropColumn("dbo.Companies", "DryGoodsDeliveryThursday");
            DropColumn("dbo.Companies", "DryGoodsDeliveryWednesday");
            DropColumn("dbo.Companies", "DryGoodsDeliveryTuesday");
            DropColumn("dbo.Companies", "DryGoodsDeliveryMonday");
        }
    }
}
