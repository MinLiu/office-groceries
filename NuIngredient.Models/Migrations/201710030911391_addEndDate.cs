namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEndDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanySuppliers", "EndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanySuppliers", "EndDate");
        }
    }
}
