namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMessageRead : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MessageReads",
                c => new
                    {
                        MessageID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.MessageID, t.CompanyID })
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Messages", t => t.MessageID)
                .Index(t => t.MessageID)
                .Index(t => t.CompanyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MessageReads", "MessageID", "dbo.Messages");
            DropForeignKey("dbo.MessageReads", "CompanyID", "dbo.Companies");
            DropIndex("dbo.MessageReads", new[] { "CompanyID" });
            DropIndex("dbo.MessageReads", new[] { "MessageID" });
            DropTable("dbo.MessageReads");
        }
    }
}
