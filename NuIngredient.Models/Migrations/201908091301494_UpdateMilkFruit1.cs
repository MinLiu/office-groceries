namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMilkFruit1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.FruitProducts", new[] { "CategoryID" });
            DropIndex("dbo.MilkProducts", new[] { "CategoryID" });
            AlterColumn("dbo.FruitProducts", "CategoryID", c => c.Guid());
            AlterColumn("dbo.MilkProducts", "CategoryID", c => c.Guid());
            CreateIndex("dbo.FruitProducts", "CategoryID");
            CreateIndex("dbo.MilkProducts", "CategoryID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MilkProducts", new[] { "CategoryID" });
            DropIndex("dbo.FruitProducts", new[] { "CategoryID" });
            AlterColumn("dbo.MilkProducts", "CategoryID", c => c.Guid(nullable: false));
            AlterColumn("dbo.FruitProducts", "CategoryID", c => c.Guid(nullable: false));
            CreateIndex("dbo.MilkProducts", "CategoryID");
            CreateIndex("dbo.FruitProducts", "CategoryID");
        }
    }
}
