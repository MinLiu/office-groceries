namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moddfy : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MilkOrders", "Monday", c => c.Int(nullable: false));
            AlterColumn("dbo.MilkOrders", "Tuesday", c => c.Int(nullable: false));
            AlterColumn("dbo.MilkOrders", "Wednesday", c => c.Int(nullable: false));
            AlterColumn("dbo.MilkOrders", "Thursday", c => c.Int(nullable: false));
            AlterColumn("dbo.MilkOrders", "Friday", c => c.Int(nullable: false));
            AlterColumn("dbo.MilkOrders", "Saturday", c => c.Int(nullable: false));
            AlterColumn("dbo.MilkOrders", "Sunday", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MilkOrders", "Sunday", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AlterColumn("dbo.MilkOrders", "Saturday", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AlterColumn("dbo.MilkOrders", "Friday", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AlterColumn("dbo.MilkOrders", "Thursday", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AlterColumn("dbo.MilkOrders", "Wednesday", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AlterColumn("dbo.MilkOrders", "Tuesday", c => c.Decimal(nullable: false, precision: 28, scale: 5));
            AlterColumn("dbo.MilkOrders", "Monday", c => c.Decimal(nullable: false, precision: 28, scale: 5));
        }
    }
}
