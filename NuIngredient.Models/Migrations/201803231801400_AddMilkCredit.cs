namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMilkCredit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MilkCreditHeaders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        SupplierID = c.Guid(),
                        CreateByID = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        OrderHeaderID = c.Guid(nullable: false),
                        Narrative = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreateByID)
                .ForeignKey("dbo.MilkOrderHeaders", t => t.OrderHeaderID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierID)
                .Index(t => t.CreateByID)
                .Index(t => t.OrderHeaderID);
            
            CreateTable(
                "dbo.MilkCreditItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        MilkCreditHeaderID = c.Guid(nullable: false),
                        MilkOrderItemID = c.Guid(nullable: false),
                        Monday = c.Int(nullable: false),
                        Tuesday = c.Int(nullable: false),
                        Wednesday = c.Int(nullable: false),
                        Thursday = c.Int(nullable: false),
                        Friday = c.Int(nullable: false),
                        Saturday = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MilkCreditHeaders", t => t.MilkCreditHeaderID)
                .ForeignKey("dbo.MilkOrders", t => t.MilkOrderItemID)
                .Index(t => t.MilkCreditHeaderID)
                .Index(t => t.MilkOrderItemID);
            
            AddColumn("dbo.MilkOrders", "MilkCreditHeader_ID", c => c.Guid());
            CreateIndex("dbo.MilkOrders", "MilkCreditHeader_ID");
            AddForeignKey("dbo.MilkOrders", "MilkCreditHeader_ID", "dbo.MilkCreditHeaders", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MilkCreditItems", "MilkOrderItemID", "dbo.MilkOrders");
            DropForeignKey("dbo.MilkCreditItems", "MilkCreditHeaderID", "dbo.MilkCreditHeaders");
            DropForeignKey("dbo.MilkCreditHeaders", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.MilkCreditHeaders", "OrderHeaderID", "dbo.MilkOrderHeaders");
            DropForeignKey("dbo.MilkOrders", "MilkCreditHeader_ID", "dbo.MilkCreditHeaders");
            DropForeignKey("dbo.MilkCreditHeaders", "CreateByID", "dbo.AspNetUsers");
            DropForeignKey("dbo.MilkCreditHeaders", "CompanyID", "dbo.Companies");
            DropIndex("dbo.MilkCreditItems", new[] { "MilkOrderItemID" });
            DropIndex("dbo.MilkCreditItems", new[] { "MilkCreditHeaderID" });
            DropIndex("dbo.MilkCreditHeaders", new[] { "OrderHeaderID" });
            DropIndex("dbo.MilkCreditHeaders", new[] { "CreateByID" });
            DropIndex("dbo.MilkCreditHeaders", new[] { "SupplierID" });
            DropIndex("dbo.MilkCreditHeaders", new[] { "CompanyID" });
            DropIndex("dbo.MilkOrders", new[] { "MilkCreditHeader_ID" });
            DropColumn("dbo.MilkOrders", "MilkCreditHeader_ID");
            DropTable("dbo.MilkCreditItems");
            DropTable("dbo.MilkCreditHeaders");
        }
    }
}
