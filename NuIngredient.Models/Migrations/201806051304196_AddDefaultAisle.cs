namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultAisle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "DefaultAisleID", c => c.Guid());
            CreateIndex("dbo.Suppliers", "DefaultAisleID");
            AddForeignKey("dbo.Suppliers", "DefaultAisleID", "dbo.Aisles", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Suppliers", "DefaultAisleID", "dbo.Aisles");
            DropIndex("dbo.Suppliers", new[] { "DefaultAisleID" });
            DropColumn("dbo.Suppliers", "DefaultAisleID");
        }
    }
}
