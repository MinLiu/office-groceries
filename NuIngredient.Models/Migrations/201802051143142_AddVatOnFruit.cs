namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddVatOnFruit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitProducts", "VAT", c => c.Decimal(nullable: false, precision: 28, scale: 5));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FruitProducts", "VAT");
        }
    }
}
