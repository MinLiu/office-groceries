namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductSortPos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitProducts", "SortPosition", c => c.Int(nullable: false));
            AddColumn("dbo.MilkProducts", "SortPosition", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MilkProducts", "SortPosition");
            DropColumn("dbo.FruitProducts", "SortPosition");
        }
    }
}
