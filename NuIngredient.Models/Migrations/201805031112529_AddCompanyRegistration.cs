namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyRegistration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "CompanyRegistration", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "CompanyRegistration");
        }
    }
}
