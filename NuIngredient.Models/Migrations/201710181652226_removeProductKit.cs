namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeProductKit : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductCosts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "KitID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductPrices", "ProductID", "dbo.Products");
            DropIndex("dbo.ProductCosts", new[] { "ProductID" });
            DropIndex("dbo.ProductKitItems", new[] { "KitID" });
            DropIndex("dbo.ProductKitItems", new[] { "ProductID" });
            DropIndex("dbo.ProductPrices", new[] { "ProductID" });
            DropTable("dbo.ProductCosts");
            DropTable("dbo.ProductKitItems");
            DropTable("dbo.ProductPrices");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductKitItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        KitID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductCosts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.ProductPrices", "ProductID");
            CreateIndex("dbo.ProductKitItems", "ProductID");
            CreateIndex("dbo.ProductKitItems", "KitID");
            CreateIndex("dbo.ProductCosts", "ProductID");
            AddForeignKey("dbo.ProductPrices", "ProductID", "dbo.Products", "ID");
            AddForeignKey("dbo.ProductKitItems", "ProductID", "dbo.Products", "ID");
            AddForeignKey("dbo.ProductKitItems", "KitID", "dbo.Products", "ID");
            AddForeignKey("dbo.ProductCosts", "ProductID", "dbo.Products", "ID");
        }
    }
}
