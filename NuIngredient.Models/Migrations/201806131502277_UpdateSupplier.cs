namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSupplier : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "RequestCenterEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "RequestCenterEmail");
        }
    }
}
