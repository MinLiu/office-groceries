// <auto-generated />
namespace NuIngredient.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddMilkOneOffChangeTracker : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddMilkOneOffChangeTracker));
        
        string IMigrationMetadata.Id
        {
            get { return "201908221655179_AddMilkOneOffChangeTracker"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
