namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFruitOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FruitOrderHeaders", "SupplierID", c => c.Guid());
            AddColumn("dbo.FruitOrderHeaders", "FileURL", c => c.String());
            CreateIndex("dbo.FruitOrderHeaders", "SupplierID");
            AddForeignKey("dbo.FruitOrderHeaders", "SupplierID", "dbo.Suppliers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitOrderHeaders", "SupplierID", "dbo.Suppliers");
            DropIndex("dbo.FruitOrderHeaders", new[] { "SupplierID" });
            DropColumn("dbo.FruitOrderHeaders", "FileURL");
            DropColumn("dbo.FruitOrderHeaders", "SupplierID");
        }
    }
}
