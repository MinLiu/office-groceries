namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addComplaint : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Complaints",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        UserID = c.String(maxLength: 128),
                        Message = c.String(),
                        Created = c.DateTime(nullable: false),
                        FruitInvoiceHeaderID = c.Guid(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .ForeignKey("dbo.FruitInvoiceHeaders", t => t.FruitInvoiceHeaderID)
                .Index(t => t.CompanyID)
                .Index(t => t.UserID)
                .Index(t => t.FruitInvoiceHeaderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Complaints", "FruitInvoiceHeaderID", "dbo.FruitInvoiceHeaders");
            DropForeignKey("dbo.Complaints", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Complaints", "CompanyID", "dbo.Companies");
            DropIndex("dbo.Complaints", new[] { "FruitInvoiceHeaderID" });
            DropIndex("dbo.Complaints", new[] { "UserID" });
            DropIndex("dbo.Complaints", new[] { "CompanyID" });
            DropTable("dbo.Complaints");
        }
    }
}
