namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitCredit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FruitCreditHeaders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        SupplierID = c.Guid(),
                        CreateByID = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        OrderHeaderID = c.Guid(nullable: false),
                        Narrative = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreateByID)
                .ForeignKey("dbo.FruitOrderHeaders", t => t.OrderHeaderID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.CompanyID)
                .Index(t => t.SupplierID)
                .Index(t => t.CreateByID)
                .Index(t => t.OrderHeaderID);
            
            CreateTable(
                "dbo.FruitCreditItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FruitCreditHeaderID = c.Guid(nullable: false),
                        FruitOrderItemID = c.Guid(nullable: false),
                        Monday = c.Int(nullable: false),
                        Tuesday = c.Int(nullable: false),
                        Wednesday = c.Int(nullable: false),
                        Thursday = c.Int(nullable: false),
                        Friday = c.Int(nullable: false),
                        Saturday = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FruitCreditHeaders", t => t.FruitCreditHeaderID)
                .ForeignKey("dbo.FruitOrders", t => t.FruitOrderItemID)
                .Index(t => t.FruitCreditHeaderID)
                .Index(t => t.FruitOrderItemID);
            
            AddColumn("dbo.FruitOrders", "FruitCreditHeader_ID", c => c.Guid());
            CreateIndex("dbo.FruitOrders", "FruitCreditHeader_ID");
            AddForeignKey("dbo.FruitOrders", "FruitCreditHeader_ID", "dbo.FruitCreditHeaders", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FruitCreditItems", "FruitOrderItemID", "dbo.FruitOrders");
            DropForeignKey("dbo.FruitCreditItems", "FruitCreditHeaderID", "dbo.FruitCreditHeaders");
            DropForeignKey("dbo.FruitCreditHeaders", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.FruitCreditHeaders", "OrderHeaderID", "dbo.FruitOrderHeaders");
            DropForeignKey("dbo.FruitOrders", "FruitCreditHeader_ID", "dbo.FruitCreditHeaders");
            DropForeignKey("dbo.FruitCreditHeaders", "CreateByID", "dbo.AspNetUsers");
            DropForeignKey("dbo.FruitCreditHeaders", "CompanyID", "dbo.Companies");
            DropIndex("dbo.FruitCreditItems", new[] { "FruitOrderItemID" });
            DropIndex("dbo.FruitCreditItems", new[] { "FruitCreditHeaderID" });
            DropIndex("dbo.FruitCreditHeaders", new[] { "OrderHeaderID" });
            DropIndex("dbo.FruitCreditHeaders", new[] { "CreateByID" });
            DropIndex("dbo.FruitCreditHeaders", new[] { "SupplierID" });
            DropIndex("dbo.FruitCreditHeaders", new[] { "CompanyID" });
            DropIndex("dbo.FruitOrders", new[] { "FruitCreditHeader_ID" });
            DropColumn("dbo.FruitOrders", "FruitCreditHeader_ID");
            DropTable("dbo.FruitCreditItems");
            DropTable("dbo.FruitCreditHeaders");
        }
    }
}
