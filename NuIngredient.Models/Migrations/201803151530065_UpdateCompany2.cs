namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCompany2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "DeliveryInstruction", c => c.String());
            AddColumn("dbo.Companies", "Depot", c => c.String());
            AddColumn("dbo.Companies", "Round", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Round");
            DropColumn("dbo.Companies", "Depot");
            DropColumn("dbo.Companies", "DeliveryInstruction");
        }
    }
}
