namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFruitOrderHeader : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.FruitOrders", name: "CompanyID", newName: "Company_ID");
            RenameIndex(table: "dbo.FruitOrders", name: "IX_CompanyID", newName: "IX_Company_ID");
            CreateTable(
                "dbo.FruitOrderHeaders",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        ProspectToken = c.String(),
                        OrderNumber = c.String(),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 2),
                        TotalVAT = c.Decimal(nullable: false, precision: 28, scale: 2),
                        FromDate = c.DateTime(),
                        ToDate = c.DateTime(),
                        OrderMode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            AddColumn("dbo.Fruits", "VATRate", c => c.Decimal(nullable: false, precision: 28, scale: 2));
            AddColumn("dbo.FruitOrders", "VATRate", c => c.Decimal(nullable: false, precision: 28, scale: 2));
            AddColumn("dbo.FruitOrders", "TotalNet", c => c.Decimal(nullable: false, precision: 28, scale: 2));
            AddColumn("dbo.FruitOrders", "TotalVAT", c => c.Decimal(nullable: false, precision: 28, scale: 2));
            AddColumn("dbo.FruitOrders", "OrderHeaderID", c => c.Guid(nullable: false));
            CreateIndex("dbo.FruitOrders", "OrderHeaderID");
            AddForeignKey("dbo.FruitOrders", "OrderHeaderID", "dbo.FruitOrderHeaders", "ID");
            DropColumn("dbo.FruitOrders", "ProspectToken");
            DropColumn("dbo.FruitOrders", "FromDate");
            DropColumn("dbo.FruitOrders", "OrderMode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FruitOrders", "OrderMode", c => c.Int(nullable: false));
            AddColumn("dbo.FruitOrders", "FromDate", c => c.DateTime());
            AddColumn("dbo.FruitOrders", "ProspectToken", c => c.String());
            DropForeignKey("dbo.FruitOrders", "OrderHeaderID", "dbo.FruitOrderHeaders");
            DropForeignKey("dbo.FruitOrderHeaders", "CompanyID", "dbo.Companies");
            DropIndex("dbo.FruitOrders", new[] { "OrderHeaderID" });
            DropIndex("dbo.FruitOrderHeaders", new[] { "CompanyID" });
            DropColumn("dbo.FruitOrders", "OrderHeaderID");
            DropColumn("dbo.FruitOrders", "TotalVAT");
            DropColumn("dbo.FruitOrders", "TotalNet");
            DropColumn("dbo.FruitOrders", "VATRate");
            DropColumn("dbo.Fruits", "VATRate");
            DropTable("dbo.FruitOrderHeaders");
            RenameIndex(table: "dbo.FruitOrders", name: "IX_Company_ID", newName: "IX_CompanyID");
            RenameColumn(table: "dbo.FruitOrders", name: "Company_ID", newName: "CompanyID");
        }
    }
}
