﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyAttachments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CompanyAttachments", "CompanyID", "dbo.Companies");
            DropIndex("dbo.CompanyAttachments", new[] { "CompanyID" });
            DropTable("dbo.CompanyAttachments");
        }
    }
}
