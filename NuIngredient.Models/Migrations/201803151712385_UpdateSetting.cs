namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "DefaultReference", c => c.String());
            AddColumn("dbo.Settings", "StartingNumber", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "StartingNumber");
            DropColumn("dbo.Settings", "DefaultReference");
        }
    }
}
