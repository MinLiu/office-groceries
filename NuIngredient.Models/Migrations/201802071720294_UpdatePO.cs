namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePO : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OneOffOrders", "PONumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OneOffOrders", "PONumber");
        }
    }
}
