﻿namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierSecondaryEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "SecondaryEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "SecondaryEmail");
        }
    }
}
