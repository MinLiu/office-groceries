namespace NuIngredient.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDeliveryDays : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Companies", "FruitSupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.Companies", "MilkSupplierID", "dbo.Suppliers");
            DropIndex("dbo.Companies", new[] { "FruitSupplierID" });
            DropIndex("dbo.Companies", new[] { "MilkSupplierID" });
            AddColumn("dbo.Companies", "MilkDeliveryMonday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "MilkDeliveryTuesday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "MilkDeliveryWednesday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "MilkDeliveryThursday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "MilkDeliveryFriday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "MilkDeliverySaturday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "MilkDeliverySunday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "FruitsDeliveryMonday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "FruitsDeliveryTuesday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "FruitsDeliveryWednesday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "FruitsDeliveryThursday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "FruitsDeliveryFriday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "FruitsDeliverySaturday", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.Companies", "FruitsDeliverySunday", c => c.Boolean(nullable: false, defaultValue: true));
            DropColumn("dbo.Companies", "FruitSupplierID");
            DropColumn("dbo.Companies", "MilkSupplierID");
            DropColumn("dbo.Companies", "HasFruitSupplier");
            DropColumn("dbo.Companies", "HasMilkSupplier");
            DropColumn("dbo.Companies", "Live");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Companies", "Live", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "HasMilkSupplier", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "HasFruitSupplier", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "MilkSupplierID", c => c.Guid());
            AddColumn("dbo.Companies", "FruitSupplierID", c => c.Guid());
            DropColumn("dbo.Companies", "FruitsDeliverySunday");
            DropColumn("dbo.Companies", "FruitsDeliverySaturday");
            DropColumn("dbo.Companies", "FruitsDeliveryFriday");
            DropColumn("dbo.Companies", "FruitsDeliveryThursday");
            DropColumn("dbo.Companies", "FruitsDeliveryWednesday");
            DropColumn("dbo.Companies", "FruitsDeliveryTuesday");
            DropColumn("dbo.Companies", "FruitsDeliveryMonday");
            DropColumn("dbo.Companies", "MilkDeliverySunday");
            DropColumn("dbo.Companies", "MilkDeliverySaturday");
            DropColumn("dbo.Companies", "MilkDeliveryFriday");
            DropColumn("dbo.Companies", "MilkDeliveryThursday");
            DropColumn("dbo.Companies", "MilkDeliveryWednesday");
            DropColumn("dbo.Companies", "MilkDeliveryTuesday");
            DropColumn("dbo.Companies", "MilkDeliveryMonday");
            CreateIndex("dbo.Companies", "MilkSupplierID");
            CreateIndex("dbo.Companies", "FruitSupplierID");
            AddForeignKey("dbo.Companies", "MilkSupplierID", "dbo.Suppliers", "ID");
            AddForeignKey("dbo.Companies", "FruitSupplierID", "dbo.Suppliers", "ID");
        }
    }
}
