﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace NuIngredient.Models
{
    public class SnapDbContext : IdentityDbContext<User>
    {
        public SnapDbContext() : base("name=DefaultConnection", throwIfV1Schema: false)
        {
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Disable cascade delete by default for one-to-many relationships.
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            // Override default convertion for Decimals.
            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            modelBuilder.Conventions.Add(new DecimalPropertyConvention(28, 5)); //Set max decimal places to 5

            // Specify entities which will cascade delete.
            modelBuilder.Entity<User>().HasMany(q => q.Claims).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Logins).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Roles).WithRequired().WillCascadeOnDelete();

            //Add Cascade Delete Roles Here

            /*             
                DropForeignKey("dbo.AspNetUsers", "CompanyID", "dbo.Companies");
                DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");             
             */

            base.OnModelCreating(modelBuilder);
        }

        public static SnapDbContext Create() {  return new SnapDbContext();  }

        //Company Entities
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyAddress> CompanyAddresses { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<TeamMember> TeamMembers { get; set; }
        public virtual DbSet<HierarchyMember> HierarchyMembers { get; set; }

        //Suppliers Entities
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<CompanySupplier> CompanySuppliers { get; set; }
        
        //Products Entities
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductExclusivePrice> ProductExclusivePrices { get; set; }
        public virtual DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }
        public virtual DbSet<OneOffOrder> OneOffOrders { get; set; }
        public virtual DbSet<OneOffOrderItem> OneOffOrderItems { get; set; }
        public virtual DbSet<OrderEventDetail> OrderEventDetails { get; set; }
        public virtual DbSet<DryGoodsCreditHeader> DryGoodsCreditHeaders { get; set; }
        public virtual DbSet<DryGoodsCreditItem> DryGoodsCreditItems { get; set; }
        public virtual DbSet<DryGoodsCategory> DryGoodsCategories { get; set; }
        public virtual DbSet<DryGoodsAisle> DryGoodsAisles { get; set; }

        //Settings & Others Entities
        public virtual DbSet<Fruitful.Email.EmailSetting> EmailSettings { get; set; }
        public virtual DbSet<SupAcctRequestLog> SupAcctRequestLogs { get; set; }

        public virtual DbSet<SageExportSettings> SageExportSettings { get; set; }
        public virtual DbSet<Holiday> Holidays { get; set; }
        public virtual DbSet<ProductExcludeCompany> ProductExcludeCompanies { get; set; }
        public virtual DbSet<EmailAttachment> EmailAttachments { get; set; }

        // Milk Entities
        public virtual DbSet<MilkOrder> MilkOrders { get; set; }
        public virtual DbSet<MilkOrderHeader> MilkOrderHeaders { get; set; }
        public virtual DbSet<MilkCreditHeader> MilkCreditHeaders { get; set; }
        public virtual DbSet<MilkCreditItem> MilkCreditItems { get; set; }
        public virtual DbSet<MilkOneOffChangeTracker> MilkOneOffChangeTrackers { get; set; }

        // Milk Product Entities
        public virtual DbSet<MilkProduct> MilkProducts { get; set; }
        public virtual DbSet<MilkProductCategory> MilkProductCategories { get; set; }
        public virtual DbSet<MilkProductExclusivePrice> MilkProductExclusivePrices { get; set; }
        public virtual DbSet<MilkCategoryLink> MilkCategoryLinks { get; set; }

        // Fruit Entities
        public virtual DbSet<FruitOrder> FruitOrders { get; set; }
        public virtual DbSet<FruitOrderHeader> FruitOrderHeaders { get; set; }
        public virtual DbSet<FruitCategory> FruitCategories { get; set; }
        public virtual DbSet<FruitCreditHeader> FruitCreditHeaders { get; set; }
        public virtual DbSet<FruitCreditItem> FruitCreditItems { get; set; }
        public virtual DbSet<FruitCategoryLink> FruitCategoryLinks { get; set; }
        public virtual DbSet<FruitOneOffChangeTracker> FruitOneOffChangeTrackers { get; set; }

        // Fruit Product Entities
        public virtual DbSet<FruitProduct> FruitProducts { get; set; }
        public virtual DbSet<FruitProductCategory> FruitProductCategories { get; set; }
        public virtual DbSet<FruitProductExclusivePrice> FruitProductExclusivePrices { get; set; }
        public virtual DbSet<FruitProductPrice> FruitProductPrices { get; set; }

        // Prospect Entities
        public virtual DbSet<Prospect> Prospects { get; set; }
        public virtual DbSet<PromotionOffer> PromotionOffers { get; set; }

        // Carousel Entities
        public virtual DbSet<Carousel> Carousels { get; set; }

        // Settings Entities
        public virtual DbSet<Setting> Settings { get; set; }

        // Enquiries
        public virtual DbSet<Enquiry> Enquiries { get; set; }

        // Feedbacks
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<FeedbackType> FeedbackTypes { get; set; }

        // Message Entities
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<MessageTarget> MessageTargets { get; set; }
        public virtual DbSet<Target> Target { get; set; }
        public virtual DbSet<MessageRead> MessageReads { get; set; }

        // CMS Entities
        public virtual DbSet<PageSetting> PageSettings { get; set; }
        public virtual DbSet<StaticPage> PageAbout { get; set; }

        // Direct Entities
        public virtual DbSet<Aisle> Aisles { get; set; }
        public virtual DbSet<AisleMoreInfoContent> AisleMoreInfoContents { get; set; }
        public virtual DbSet<AisleExcludeCompany> AisleExcludeCompanies { get; set; }

        // Other Entities
        public virtual DbSet<InternalNote> InternalNotes { get; set; }
        public virtual DbSet<Testimonial> Testimonials { get; set; }

        public static List<User> GetCompanyUsers(User CurrentUser) {

            using (var context = new SnapDbContext())
            {
                if (CurrentUser.Company.UseTeams)
                {
                    //Get the teams where the user is in
                    var teamIDs = context.TeamMembers.Where(t => t.UserId == CurrentUser.Id).Select(t => t.TeamID);

                    return context.TeamMembers
                            .Include(u => u.User)
                            .Where(u => teamIDs.Contains(u.TeamID))
                            .OrderBy(u => u.User.Email)
                            .Select(u => u.User).ToList();             
                }
                else if (CurrentUser.Company.UseHierarchy)
                {
                    var ids = new List<Guid>();
                    var allHiers = context.HierarchyMembers.Where(t => t.CompanyID == CurrentUser.CompanyID).Include(i => i.Underlings).Include(i => i.User).ToList();
                    var userHiers = allHiers.Where(i => i.UserId == CurrentUser.Id);

                    foreach (var h in userHiers)
                    {
                        HierarchyMemberExtensions.GetUnderlingIDs(h, ids, allHiers);

                        if(CurrentUser.Company.AccessSameLevelHierarchy)
                            HierarchyMemberExtensions.GetPeerIDs(h, ids, allHiers);
                    }

                    return allHiers.Where(i => ids.Contains(i.ID)).Select(u => u.User).ToList();  


                }
                else
                {
                    return context.Users.Where(u => u.CompanyID == CurrentUser.CompanyID)
                                        .OrderBy(u => u.Email).ToList();

                                        
                }
            }
        }
    }
}
