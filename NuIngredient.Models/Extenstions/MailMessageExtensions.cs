﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models.Extenstions
{
    public static class MailMessageExtensions
    {
        public static void AddToEmails(this MailMessage mailMsg, IEnumerable<string> emailList)
        {
            if (emailList == null)
                return;
            
            foreach (var email in emailList)
            {
                mailMsg.AddToEmails(email);
            }
        }
        
        public static void AddToEmails(this MailMessage mailMsg, string emails)
        {
            emails = emails ?? "";

            var emailList = emails.Split(';').Select(x => x.Trim()).ToList();

            foreach (var email in emailList)
            {
                if (Fruitful.Email.Emailer.IsValidEmail(email))
                {
                    mailMsg.To.Add(email);
                }
            }
        }

        public static void AddCCEmails(this MailMessage mailMsg, IEnumerable<string> emailList)
        {
            if (emailList == null)
                return;

            foreach (var email in emailList)
            {
                mailMsg.AddCCEmails(email);
            }
        }
        
        public static void AddCCEmails(this MailMessage mailMsg, string emails)
        {
            emails = emails ?? "";

            var emailList = emails.Split(';').Select(x => x.Trim()).ToList();

            foreach (var email in emailList)
            {
                if (Fruitful.Email.Emailer.IsValidEmail(email))
                {
                    mailMsg.CC.Add(email);
                }
            }
        }
        
        public static void AddBccEmails(this MailMessage mailMsg, IEnumerable<string> emailList)
        {
            if (emailList == null)
                return;

            foreach (var email in emailList)
            {
                mailMsg.AddBccEmails(email);
            }
        }

        public static void AddBccEmails(this MailMessage mailMsg, string emails)
        {
            emails = emails ?? "";

            var emailList = emails.Split(';').Select(x => x.Trim()).ToList();

            foreach (var email in emailList)
            {
                if (Fruitful.Email.Emailer.IsValidEmail(email))
                {
                    mailMsg.Bcc.Add(email);
                }
            }
        }
    }
}
