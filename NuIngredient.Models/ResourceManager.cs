﻿using System.IO;
using System.Reflection;

namespace NuIngredient.Models
{
    public class ResourceManager
    {
        public static string GetPromotionEmailTemplate()
        {
            const string resourceName = "NuIngredient.Models.EmailHelpers.Templates.PromoteEmailTemplate.cshtml";

            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}