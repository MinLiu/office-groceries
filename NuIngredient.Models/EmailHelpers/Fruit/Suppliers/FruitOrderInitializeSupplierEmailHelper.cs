﻿using System;
using System.IO;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class FruitOrderInitializeSupplierEmailHelper : FruitSupplierBaseEmailHelper
    {
        public static MailMessage GetEmail(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            var mailMessage = new MailMessage();
            mailMessage.AddToEmails(companySupplier.Supplier.Email);

            var type = companySupplier.Supplier.SupplierType == SupplierType.Snacks
                ? "Snack"
                : "Fruit";
            
            mailMessage.Subject = $"{type} Order Confirmation from Office-Groceries Ltd";
            var startDate = DateTime.Now >= DateTime.Today.AddHours(CutOffTime.TimeOfDay)
                ? DateTime.Today.AddDays(2)
                : DateTime.Today.AddDays(1);
            if (startDate < companySupplier.StartDate.Value)
                startDate = companySupplier.StartDate.Value;

            mailMessage.Body = String.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                            "Hello<br /><br />" +
                                            "Please accept this e-mail as confirmation for a new Office-Groceries Ltd account due to start on <br />" +
                                            "<br />" +
                                            "<strong>Please check final order on attachment as the original request may have changed.</strong>" +
                                            "<hr />" +
                                            "<table>" +
                                                "<tr>" +
                                                    "<td><label>Start Date:</label></td><td>{0}</td>" +
                                                "</tr>" +
                                                (companySupplier.DeliveryTimeFrom != null ? "<tr><td><label>From:</label></td><td>" + companySupplier.DeliveryTimeFrom.Value.ToString("HH:mm") + "</td></tr>" : "" ) +
                                                (companySupplier.DeliveryTimeTo != null ? "<tr><td><label>To:</label></td><td>" + companySupplier.DeliveryTimeTo.Value.ToString("HH:mm") + "</td></tr>" : "" ) +
                                                "<tr>" +
                                                    "<td><label>Account Number:</label></td><td>{1}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Company:</label></td><td>{2}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Delivery Address:</label></td><td>{3}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Postcode:</label></td><td>{4}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Delivery Instructions:</label><td>{5}</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<hr />" +
                                            "<br />" +
                                            "Should there be any variation to the above information please contact Orders@office-groceries.com ASAP<br />" +
                                        "</div>", startFrom.ToString("dd/MM/yyyy")
                                                , companySupplier.AccountNo
                                                , company.Name
                                                , company.Address1
                                                , company.Postcode
                                                , company.DeliveryInstruction);

            var productType = companySupplier.Supplier.SupplierType == SupplierType.Snacks
                ? FruitProductType.Snack
                : FruitProductType.Fruit;

            try
            {
                string filePath = GenerateExcel(company, OrderMode.Regular, "", companySupplier.AccountNo, startDate,
                    productType);
                mailMessage.Attachments.Add(new Attachment(filePath) { Name = Path.GetFileName(filePath) });
            }
            catch (Exception e)
            {
                
            }
            mailMessage.IsBodyHtml = true;

            return mailMessage;
        }

    }
}