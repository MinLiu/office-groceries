﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class FruitOneOffOrderChangeSupplierEmailHelper : FruitSupplierBaseEmailHelper
    {
        public static MailMessage GetEmail(Company company, CompanySupplier companySupplier, DateTime fromDate, DateTime toDate)
        {
            var mailMessage = new MailMessage();
            if (companySupplier.Supplier.DailyEmailReminderEnabled)
            {
                mailMessage.AddToEmails("support@office-groceries.com");
            }
            else if (!string.IsNullOrWhiteSpace(companySupplier.Supplier.SecondaryEmail))
            {
                mailMessage.AddToEmails(companySupplier.Supplier.SecondaryEmail);
            }
            else
            {
                mailMessage.AddToEmails(companySupplier.Supplier.Email);
            }

            var startDate = DateTime.Today.StartOfWeek(DayOfWeek.Monday) < fromDate
                ? fromDate
                : DateTime.Now >= DateTime.Today.AddHours(CutOffTime.TimeOfDay)
                    ? DateTime.Today.AddDays(2)
                    : DateTime.Today.AddDays(1);

            var productType = companySupplier.Supplier.SupplierType == SupplierType.Fruits
                ? FruitProductType.Fruit
                : FruitProductType.Snack;
            
            mailMessage.Subject = "One Off change to " + productType + " Order (" + fromDate.ToString("dd/MM/yyyy") + " - " + toDate.ToString("dd/MM/yyyy") + ") Confirmation for " + company.Name + "(" + companySupplier.AccountNo + ") from Office-Groceries Ltd";
            var companyDetails = string.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                            "Hello<br /><br />" +
                                            "Please accept this e-mail as confirmation for a <strong>One Off Change</strong> for the Office-Groceries Ltd account below: <br />" +
                                            "<br />" +
                                            "<hr />" +
                                            "<table>" +
                                                "<tr>" +
                                                    "<td><label>Week:</label></td><td>" + fromDate.ToString("dd/MM/yyyy") + " - " + toDate.ToString("dd/MM/yyyy") + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Account Number:</label></td><td>{0}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Company:</label></td><td>{1}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Delivery Address:</label></td><td>{2}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Postcode:</label></td><td>{3}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Delivery Instructions:</label><td>{4}</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<hr />" +
                                            "<br />" +
                                        "</div>", companySupplier.AccountNo
                                                , company.Name
                                                , company.Address1
                                                , company.Postcode
                                                , company.DeliveryInstruction);

            
            var weeklyOrder = GetWeeklyOrderBody(company, productType, fromDate);
            var regularOrder = GetRegularOrderBody(company, productType);

            mailMessage.Body = companyDetails + "" +
                string.Format("<strong>Please action the One Off Change below for the {0} - {1} trading week</strong>", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy")) +
                weeklyOrder +
                "<br/><br/>" +
                "The client's normal standing order is:" +
                regularOrder;

            try
            {
                string filePath = GenerateExcel(company, OrderMode.OneOff, fromDate.ToString(), companySupplier.AccountNo, startDate, productType); // test
                mailMessage.Attachments.Add(new Attachment(filePath) { Name = Path.GetFileName(filePath) });
            }
            catch (Exception e)
            {
                
            }
            
            mailMessage.IsBodyHtml = true;
                
            return mailMessage;
        }
    }
}