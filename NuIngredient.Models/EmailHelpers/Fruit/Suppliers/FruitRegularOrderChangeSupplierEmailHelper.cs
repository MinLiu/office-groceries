﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class FruitRegularOrderChangeSupplierEmailHelper : FruitSupplierBaseEmailHelper
    {
        public static MailMessage GetEmail(Company company, CompanySupplier companySupplier)
        {
            var mailMessage = new MailMessage();
            if (companySupplier.Supplier.DailyEmailReminderEnabled)
            {
                mailMessage.AddToEmails("support@office-groceries.com");
            }
            else if (!string.IsNullOrWhiteSpace(companySupplier.Supplier.SecondaryEmail))
            {
                mailMessage.AddToEmails(companySupplier.Supplier.SecondaryEmail);
            }
            else
            {
                mailMessage.AddToEmails(companySupplier.Supplier.Email);
            }
            
            var productType = companySupplier.Supplier.SupplierType == SupplierType.Fruits
                ? FruitProductType.Fruit
                : FruitProductType.Snack;
            
            var startDate = DateTime.Now >= DateTime.Today.AddHours(CutOffTime.TimeOfDay)
                ? DateTime.Today.AddDays(2)
                : DateTime.Today.AddDays(1);
            // Permanent change
            if (startDate < companySupplier.StartDate.Value)
                startDate = companySupplier.StartDate.Value;

            var intermittentDeliveryMessage = "";
            if (companySupplier.IsIntermittentDelivery && companySupplier.NextDeliveryWeek != null)
            {
                intermittentDeliveryMessage =
                    $"<br/><strong>Please note: this customer's next delivery week is {companySupplier.NextDeliveryWeek:dd/MM/yyyy} - {companySupplier.NextDeliveryWeek.Value.AddDays(6):dd/MM/yyyy} trading week</strong>";
            }
            
            mailMessage.Subject = $"Permanent change to {productType} Order Confirmation for {company.Name}({companySupplier.AccountNo}) from Office-Groceries Ltd";
            var companyDetails = string.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                            "Hello<br /><br />" +
                                            "Please accept this e-mail as confirmation for a <strong>Permanent Change</strong> for the Office-Groceries Ltd account below: <br />" +
                                            "<br />" +
                                            "<strong>Any Permanent Changes received after " + CutOffTime.ToTimeString() + " should only be actioned the day after next.</strong>" +
                                            intermittentDeliveryMessage +
                                            "<hr />" +
                                            "<table>" +
                                                "<tr>" +
                                                    "<td><label>Account Number:</label></td><td>{0}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Company:</label></td><td>{1}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Delivery Address:</label></td><td>{2}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Postcode:</label></td><td>{3}</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td><label>Delivery Instructions:</label><td>{4}</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<hr />" +
                                            "<br />" +
                                        "</div>", companySupplier.AccountNo
                                                , company.Name
                                                , company.Address1
                                                , company.Postcode
                                                , company.DeliveryInstruction);

            var regularOrderBody = GetRegularOrderBody(company, productType);

            mailMessage.Body = companyDetails +
                "<strong>Please action the Permanent Change to the client’s normal standing order below</strong>" +
                regularOrderBody;

            try
            {
                var lastRegularOrderBody = GetLastRegularOrderBody(company, productType);
                if (!string.IsNullOrWhiteSpace(lastRegularOrderBody))
                {
                    mailMessage.Body += "<br/><br/>" +
                        "The client's last standing order was:" +
                        lastRegularOrderBody;
                }
            }
            catch (Exception ex)
            {
                // ignored
            }

            try
            {
                string filePath = GenerateExcel(company, OrderMode.Regular, "", companySupplier.AccountNo, startDate, productType); // test
                mailMessage.Attachments.Add(new Attachment(filePath) { Name = Path.GetFileName(filePath) });
            }
            catch (Exception ex)
            {
                // ignored
            }
            mailMessage.IsBodyHtml = true;
                
            return mailMessage;
        }
    }
}