﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace NuIngredient.Models
{
    public class FruitSupplierBaseEmailHelper
    {
        protected static string GetWeeklyOrderBody(Company company, FruitProductType productType, DateTime dateFrom)
        {
            return GetOrderBody(company, OrderMode.OneOff, productType, dateFrom);
        }

        protected static string GetRegularOrderBody(Company company, FruitProductType productType)
        {
            return GetOrderBody(company, OrderMode.Regular, productType);
        }
        
        protected static string GetLastRegularOrderBody(Company company, FruitProductType productType)
        {
            return GetOrderBody(company, OrderMode.Regular, productType, null, true);
        }

        private static string GetOrderBody(Company company, OrderMode orderMode, FruitProductType productType, DateTime? dateFrom = null, bool depreciated = false)
        {
            try
            {
                var service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), new SnapDbContext());
                var weeklyOrder = service.GetCustomerOrder(company.ID, orderMode, dateFrom, depreciated);

                var tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                        "<thead>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<th>Product</th>" +
                                                "<th>Unit</th>" +
                                                "<th>Mon</th>" +
                                                "<th>Tue</th>" +
                                                "<th>Wed</th>" +
                                                "<th>Thu</th>" +
                                                "<th>Fri</th>" +
                                                "<th>Sat</th>" +
                                                "<th>Weekly Volume</th>" +
                                            "</tr>" +
                                        "</thead>";

                var tableBody = "";
                var validItems = weeklyOrder.Items.Where(x => x.Fruit.FruitProductType == productType);
                foreach (var lineItem in validItems.OrderBy(i => i.ProductName))
                {
                    var str = string.Format("<tr style='background-color: #c1c4be;'>" +
                                                "<td style='text-align: center;'>{0}</td>" +
                                                "<td style='text-align: center;'>{1}</td>" +
                                                "<td style='text-align: center;'>{2}</td>" +
                                                "<td style='text-align: center;'>{3}</td>" +
                                                "<td style='text-align: center;'>{4}</td>" +
                                                "<td style='text-align: center;'>{5}</td>" +
                                                "<td style='text-align: center;'>{6}</td>" +
                                                "<td style='text-align: center;'>{7}</td>" +
                                                "<td style='text-align: center;'>{8}</td>" +
                                            "</tr>", lineItem.ProductName
                                                   , lineItem.Unit
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Monday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Tuesday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Wednesday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Thursday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Friday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Saturday)
                                                   , lineItem.WeeklyVolume);
                    tableBody += str;
                }

                tableBody += string.Format("<tr style='background-color: #c1c4be;'>" +
                                                "<td style='text-align: center;' colspan = '8'>Total</td>" +
                                                "<td style='text-align: center;'>{0}</td>" +
                                            "</tr>", validItems.Sum(x => x.WeeklyVolume));
                var tableFooter = "</table>";

                return tableHeader + tableBody + tableFooter;
            }
            catch(Exception e)
            {
                return "";
            }
        }
        
        protected static string GenerateExcel(Company company, OrderMode orderMode, string date, string acctNum, DateTime StartDate, FruitProductType fruitProductType)
        {
            DateTime? FromDate = !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;

            var orderHelper = new CustomerOrderFormHelper();

            orderHelper.SetCustName(company.Name);
            orderHelper.SetCustInfo("");
            orderHelper.SetAcctNumber(acctNum);
            orderHelper.SetDateDeliveryRequired(orderMode == OrderMode.Regular ? String.Format("From {0}", StartDate.ToString("dd/MM/yyyy")) 
                                                                               : String.Format("{0} - {1}", FromDate.Value.ToString("dd/MM/yyyy"), FromDate.Value.AddDays(6).ToString("dd/MM/yyyy")));
            var orderHeaderRepo = new FruitOrderHeaderRepository();
            var orderHeader = orderHeaderRepo.Read()
                                             .Where(x => x.CompanyID == company.ID)
                                             .Where(x => x.OrderMode == orderMode)
                                             .Where(x => orderMode == OrderMode.Regular || x.FromDate == FromDate)
                                             .Include(x => x.Items)
                                             .FirstOrDefault();
            //var lineItems = new FruitOrderRepository().Read()
            //                                          .Where(x => x.OrderHeader.CompanyID == company.ID)
            //                                          .Where(x => x.OrderHeader.OrderMode == orderMode)
            //                                          .Where(x => x.OrderHeader.FromDate == FromDate)
            //                                          .ToList();

            foreach (var item in orderHeader.Items.Where(x => x.Fruit.FruitProductType == fruitProductType))
            {
                orderHelper.NewLineItem(item.ProductName, item.ProductName, item.Monday, item.Tuesday, item.Wednesday, item.Thursday, item.Friday, item.Saturday, item.OrderHeader.OrderMode);
            }

            //Write the workbook to a memory stream
            const string contentFolderRoot = "/FruitOrders/";
            var virtualPath = string.Format("{0}{1}", contentFolderRoot, company.ID.ToString());


            var physicalPath = HostingEnvironment.MapPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var fileName = "";
            if (orderMode == OrderMode.Regular)
                fileName =
                    $"/Regular_Order (Updated {DateTime.Now:dd-MM-yy HH-mm-ss}) ({fruitProductType}).xls";
            else
                fileName =
                    $"/OneOff_Order({FromDate.Value:dd-MM-yyyy} ~ {FromDate.Value.AddDays(6):dd-MM-yyyy}) (Updated {DateTime.Now:dd-MM-yy HH-mm-ss}) ({fruitProductType}).xls";

            string physicalSavePath = HostingEnvironment.MapPath(virtualPath) + fileName;
            var workbook = orderHelper.Workbook;

            FileStream file = new FileStream(physicalSavePath, FileMode.Create, FileAccess.ReadWrite);
            workbook.Write(file);
            file.Close();

            orderHeader.FileURL = virtualPath + fileName;
            orderHeaderRepo.Update(orderHeader);

            return physicalSavePath;
        }
    }
}