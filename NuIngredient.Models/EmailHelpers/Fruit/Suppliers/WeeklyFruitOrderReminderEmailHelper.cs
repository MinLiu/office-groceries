﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class CompanyFruitOrder
    {
        public CompanyFruitOrder(FruitOrderHeader orderHeader, string accountNo, Company company)
        {
            OrderHeader = orderHeader;
            AccountNo = accountNo;
            Company = company;
        }

        public FruitOrderHeader OrderHeader { get; private set; }
        public string AccountNo { get; private set; }
        public Company Company { get; private set; }
    }

    public class WeeklyFruitOrderReminderEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Supplier supplier, List<CompanyFruitOrder> companyWithOrders, DateTime monday)
        {
            var saturday = monday.AddDays(5);

            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(supplier.Email);
            //mailMessage.AddToEmails("min.liu@zigaflow.com");
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = $"Weekly Order Reminder from Office Groceries ({monday:dd/MM/yyyy} ~ {saturday:dd/MM/yyyy})";
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(supplier, monday, saturday) +
                                                AppendBody(companyWithOrders, supplier.SupplierType) + 
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(Supplier supplier, DateTime monday, DateTime saturday)
        {
            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + supplier.Name + "</span>,<br />" +
                                    "<br />" +
                                    $"Please accept this e-mail confirming our customer’s standing orders for next week. ({monday:dd/MM/yyyy} ~ {saturday:dd/MM/yyyy})<br />" +
                                    "<br />" +
                                    "If you have any questions, send us an e-mail at <a href='mailto:orders@office-groceries.com'>Orders@office-groceries.com</a> or give us a bell on <br />0345 463 8863 and the team will be happy to help! <br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team!" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";

            return message;
        }

        private static string AppendBody(List<CompanyFruitOrder> companyOrders, SupplierType supplierType)
        {
            var content = "";
            foreach (var companyOrder in companyOrders.OrderBy(c => c.Company.Name))
            {
                if (companyOrder.OrderHeader?.Items == null) continue;
                content += "<tr><td><hr /></td></tr>";
                content += AppendCompanyDetails(companyOrder.Company, companyOrder.AccountNo);
                content += AppendOrderBody(companyOrder.OrderHeader, supplierType);
                content += "<tr><td><br /></td></tr>";
                content += "<tr><td><br /></td></tr>";
            }

            return content;
        }

        private static string AppendCompanyDetails(Company company, string accountNo)
        {
            var companyDetails = "<tr>" +
                                    "<td>" +
                                        "<table style='border: 1px solid white; width: 700px;'>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td style='width:130px;'><label>Account Number:</label></td><td>" + accountNo + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Company:</label></td><td>" + company.Name + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Delivery Address:</label></td><td>" + company.Address1 + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Postcode:</label></td><td>" + company.Postcode + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Delivery Instructions:</label><td>" + company.DeliveryInstruction + "</td>" +
                                            "</tr>" +
                                        "</table>" +
                                    "</td>" +
                                "</tr>";
            return companyDetails;
        }

        private static string AppendOrderBody(FruitOrderHeader fruitOrderHeader, SupplierType supplierType)
        {
            var body = GetFruitOrderTable(fruitOrderHeader, supplierType);

            var quoteBody = "<tr>" +
                                "<td style='" + _style_font_family + "'>" +
                                    body +
                                "</td>" +
                            "</tr>";
            return quoteBody;
        }

        private static string GetFruitOrderTable(FruitOrderHeader orderHeader, SupplierType supplierType)
        {
            var fruitType = supplierType == SupplierType.Snacks
                ? FruitProductType.Snack
                : FruitProductType.Fruit;
            
            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th style='width: 220px'>Product</th>" +
                                            "<th>Unit</th>" +
                                            "<th>Mon</th>" +
                                            "<th>Tue</th>" +
                                            "<th>Wed</th>" +
                                            "<th>Thu</th>" +
                                            "<th>Fri</th>" +
                                            "<th>Sat</th>" +
                                            "<th>Weekly Volume</th>" +
                                        "</tr>" +
                                    "</thead>";

            string tableBody = "";
            foreach (var order in orderHeader.Items.Where(x => x.Fruit.FruitProductType == fruitType))
            {
                var str = "<tr style='background-color: #c1c4be;'>" +
                          $"<td style='text-align: center;'>{order.ProductName}</td>" +
                          $"<td style='text-align: center;'>{order.Unit}</td>" +
                          $"<td style='text-align: center;'>{order.Monday:0.######;-0.######;\"\"}</td>" +
                          $"<td style='text-align: center;'>{order.Tuesday:0.######;-0.######;\"\"}</td>" +
                          $"<td style='text-align: center;'>{order.Wednesday:0.######;-0.######;\"\"}</td>" +
                          $"<td style='text-align: center;'>{order.Thursday:0.######;-0.######;\"\"}</td>" +
                          $"<td style='text-align: center;'>{order.Friday:0.######;-0.######;\"\"}</td>" +
                          $"<td style='text-align: center;'>{order.Saturday:0.######;-0.######;\"\"}</td>" +
                          $"<td style='text-align: center;'>{order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday}</td>" +
                          "</tr>";
                tableBody += str;
            }

            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;' colspan = '8'>Total</td>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                        "</tr>", orderHeader.Items.Where(x => x.Fruit.FruitProductType == fruitType).Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday));

            string tableFooter = "</table>";

            return tableHeader + tableBody + tableFooter;
        }

    }
}
