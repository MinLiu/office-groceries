﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class FruitOrderIssueEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Supplier supplier, string subject, string message, List<EmailAttachment> emailAttachments)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            if (!string.IsNullOrWhiteSpace(supplier.SecondaryEmail))
            {
                mailMessage.AddToEmails(supplier.SecondaryEmail);
            }
            else
            {
                mailMessage.AddToEmails(supplier.Email);
            }
            //mailMessage.AddToEmails("min.liu@zigaflow.com");
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = subject;
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(message) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }
                        
            foreach (var attachment in emailAttachments)
            {
                try
                { 
                    var newAttachment = new Attachment(_serverPath + attachment.FilePath);
                    newAttachment.Name = attachment.FileName;
                    mailMessage.Attachments.Add(newAttachment);
                }
                catch(Exception e) { }
            }

            return mailMessage;
        }

        private static string AppendMessage(string message)
        {
            var result = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    message?.Replace("\n", "<br />") +
                                    "<br />" +
                                    "<br />" +
                                    "If you have any questions, send us an e-mail at <a href='mailto:orders@office-groceries.com'>Orders@office-groceries.com</a> or give us a bell on <br />0345 463 8863 and the team will be happy to help! <br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team!" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";

            return result;
        }

    }
}
