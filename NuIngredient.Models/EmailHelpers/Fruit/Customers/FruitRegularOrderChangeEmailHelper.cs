﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class FruitRegularOrderChangeEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Company company)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(company.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = "Permanent change to Fruit/Snack Order Confirmation from Office Groceries";
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(company) +
                                                AppendCompanyDetails(company) +
                                                AppendOrderBody(company, imgAttachments) +
                                                AppendFooter() +
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(Company company)
        {
            var callBackUrl = String.Format("{0}{1}", _customerSiteUrl, "Dashboard/Index");

            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + company.Name + "</span>,<br />" +
                                    "<br />" +
                                    "Thanks for getting in contact!<br />" +
                                    "<br />" +
                                    "Please accept this e-mail as confirmation of the <strong>Permanent Change</strong> you have made to your fruit/snack order.<br />" +
                                    "<br />" +
                                    "If you need any help or support or would like to leave feedback regarding your permanent change, please click <a href='" + callBackUrl + "'>Here</a>.<br />" +
                                    "<br />" +
                                    "Alternatively, send us an e-mail at <a href='mailto:orders@office-groceries.com'>Orders@office-groceries.com</a> or give us a bell on 0345 463 8863 and the team will be happy to help! <br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team!" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";

            return message;
        }
        private static string AppendOrderBody(Company company, List<Attachment> imgAttachments)
        {
            var body = GetFruitOrderTable(company, OrderMode.Regular, "", imgAttachments);

            var quoteBody = "<tr>" +
                                "<td style='" + _style_font_family + "'>" +
                                    body +
                                "</td>" +
                            "</tr>";
            return quoteBody;
        }

    }
}
