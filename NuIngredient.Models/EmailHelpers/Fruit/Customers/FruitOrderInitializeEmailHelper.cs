﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class FruitOrderInitializeEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(company.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = "Fruit/Snack Account Confirmation from Office Groceries";
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(company, startFrom, companySupplier) +
                                                AppendCompanyDetails(company) +
                                                AppendOrderBody(company, imgAttachments) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            var callBackUrl = String.Format("{0}{1}", _customerSiteUrl, "Dashboard/Index");

            var deliveryDays = new List<string>();
            if (company.FruitsDeliveryMonday) deliveryDays.Add("<strong>Monday</strong>");
            if (company.FruitsDeliveryTuesday) deliveryDays.Add("<strong>Tuesday</strong>");
            if (company.FruitsDeliveryWednesday) deliveryDays.Add("<strong>Wednesday</strong>");
            if (company.FruitsDeliveryThursday) deliveryDays.Add("<strong>Thursday</strong>");
            if (company.FruitsDeliveryFriday) deliveryDays.Add("<strong>Friday</strong>");
            if (company.FruitsDeliverySaturday) deliveryDays.Add("<strong>Saturday</strong>");

            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + company.Name + "</span>,<br />" +
                                    "<br />" +
                                    "Congratulations- your account is now complete!<br />" +
                                    "<br />" +
                                    "Please accept this e-mail as confirmation that your Office Groceries Fruit Account is now set up. We have scheduled your first delivery to take place week commencing " + startFrom.ToString("dd/MM/yyyy") + ", however if you would like to change this, please don’t hesitate to contact us.<br />" +
                                    "<br />" +
                                    "Available delivery days to your business are: " + String.Join(", ", deliveryDays) + "<br/>" +
                                    "<br />" +
                                    (companySupplier.DeliveryTimeFrom != null && companySupplier.DeliveryTimeTo != null ? "And the delivery time is between " + companySupplier.DeliveryTimeFrom.Value.ToString("HH:mm") + " - " + companySupplier.DeliveryTimeTo.Value.ToString("HH:mm") + "<br /><br />" : "") +
                                    "Moving forward, if you would like to make any changes or place further orders, please login to our <a href='" + callBackUrl + "'>client area</a> using the login details you created during the account set up process.<br />" +
                                    "<br />" +
                                    "If you have any questions or require support, please don’t hesitate to get in contact. Either reply to this e-mail or contact us at <a href='mailto:orders@office-groceries.com'>Orders@office-groceries.com</a>, alternatively give us call on 0345 463 8863 and one of the team will be happy to help!<br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team!" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";

            return message;
        }
        private static string AppendOrderBody(Company company, List<Attachment> imgAttachments)
        {
            var body = GetFruitOrderTable(company, OrderMode.Regular, "", imgAttachments);

            var quoteBody = "<tr>" +
                                "<td style='" + _style_font_family + "'>" +
                                    body +
                                "</td>" +
                            "</tr>";
            return quoteBody;
        }

    }
}
