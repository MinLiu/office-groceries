﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class AccountSetupWithPasswordEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(User user, string password, string category)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(user.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = String.Format("{0} Account set up from Office Groceries", category);
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(user, password, category) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(User user, string password, string category)
        {
            var resetPswdURL = String.Format("{0}Users/ManageAccount", _customerSiteUrl);
            var loginURL = String.Format("{0}Account/Login", _customerSiteUrl);

            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + user.Company.Name + "</span>,<br />" +
                                    "<br />" +
                                    "Thank you for choosing to open a " + category + " Account with Office Groceries!<br>" +
                                    "<br />" +
                                    "We are currently in the process of setting up your account. Please note, it can take up to 6 working days to set up your account and delivery days cannot be guaranteed until the account is complete.<br />" +
                                    "<br />" +
                                         "<table>" +
                                            "<tr>" +
                                                "<td>" +
                                                    "username: " +
                                                "</td>" +
                                                "<td>" +
                                                    user.UserName +
                                                "</td>" +
                                            "</tr>" +
                                            "<tr>" +
                                                "<td>" +
                                                    "password: " +
                                                "</td>" +
                                                "<td>" +
                                                    password +
                                                "</td>" +
                                            "</tr>" +
                                            "<tr>" +
                                                "<td colspan='2'>" +
                                                    "If you would like to reset your Password, click <a href='" + resetPswdURL + "'>here</a>" +
                                                "</td>" +
                                            "</tr>" +
                                        "</table>" +
                                    "<br />" +
                                    
                                    "Once your account has been completed, we will be in contact to confirm the delivery days available to you, the date of your first order and your account number.<br />" +
                                    "<br />" +
                                    "In the meantime, if you have any questions at all, please don’t hesitate to give us a bell on <span style='white-space: nowrap;'>0345 463 8863</span>, or email <a href=\"mailto:Orders@office-groceries.com\">Orders@office-groceries.com</a> and we will be happy to assist.<br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";
            return message;
        }
    }
}
