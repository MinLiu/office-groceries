﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class OfficeDropTransferEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Company company, User user, string password)
        {
            var setting = new SnapDbContext().EmailSettings.First();
            
            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(user.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = "Welcome to Office-Groceries";
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(user, password, company, imgAttachments) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            try
            {
                var termsConditions = new Attachment(_serverPath + "/Content/OG Terms & Conditions.pdf");
                termsConditions.Name = "Terms & Conditions.pdf";
                mailMessage.Attachments.Add(termsConditions);
            }
            catch { }

            try
            {
                var brochure = new Attachment(_serverPath + "/Content/OG Brochure.pdf");
                brochure.Name = "OG Brochure.pdf";
                mailMessage.Attachments.Add(brochure);
            }
            catch { }

            try
            {
                var keyinfo = new Attachment(_serverPath + "/Content/Office-Groceries Key Info Customers.pdf");
                keyinfo.Name = "Key Info.pdf";
                mailMessage.Attachments.Add(keyinfo);
            }
            catch { }

            return mailMessage;
        }

        private static string AppendMessage(User user, string password, Company company,List<Attachment> imgAttachments )
        {
            var resetPswdURL = String.Format("{0}Users/ManageAccount", _customerSiteUrl);
            var loginURL = String.Format("{0}Account/Login", _customerSiteUrl);

            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + company.Name + "</span>,<br />" +
                                    "<br />" +
                                    "Welcome to Office-Groceries Ltd!<br>" +
                                    "<br />" +
                                    "Please find below your login details to our website <a href='https://www.office-groceries.com'>www.office-groceries.com</a>.<br />" +
                                    "<br />" +
                                    "By logging in you can make all the necessary changes to your Milk and Fruit standing orders, view previous orders and place orders from any of the aisles we have on offer." +
                                    "<br />" +
                                         "<table>" +
                                            "<tr>" +
                                                "<td>" +
                                                    "username: " +
                                                "</td>" +
                                                "<td>" +
                                                    user.UserName +
                                                "</td>" +
                                            "</tr>" +
                                            "<tr>" +
                                                "<td>" +
                                                    "password: " +
                                                "</td>" +
                                                "<td>" +
                                                    password +
                                                "</td>" +
                                            "</tr>" +
                                            "<tr>" +
                                                "<td colspan='2'>" +
                                                    "If you would like to reset your Password, click <a href='" + resetPswdURL + "'>here</a>" +
                                                "</td>" +
                                            "</tr>" +
                                        "</table>" +
                                    "<br />" +
                                    "In the meantime, if you have any questions at all, please don’t hesitate to give us a bell on <span style='white-space: nowrap;'>0345 463 8863</span>, or email <a href=\"mailto:Orders@office-groceries.com\">Orders@office-groceries.com</a> and we will be happy to assist.<br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";
            return message;
        }
    }
}
