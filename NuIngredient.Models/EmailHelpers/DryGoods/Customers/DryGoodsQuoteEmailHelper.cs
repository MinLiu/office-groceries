﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class DryGoodsQuoteEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Company company)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(company.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = String.Format("Dry Goods Quote from Office Groceries");
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(company) +
                                                AppendCompanyDetails(company) +
                                                AppendOrderBody(company, imgAttachments) +
                                                AppendFooter() +
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(Company company)
        {
            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + company.Name + "</span>,<br />" +
                                    "<br />" +
                                    "This is your Dry Goods quote from Office Groceries.<br />" +
                                    "<br />" +
                                    "The Office Groceries Team" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";

            return message;
        }

        private static string AppendOrderBody(Company company, List<Attachment> imgAttachments)
        {
            var body = GetDryGoodsOrderTable(company, imgAttachments);

            var orderBody = "<tr>" +
                                "<td style='" + _style_font_family + "'>" +
                                    body +
                                "</td>" +
                            "</tr>";
            return orderBody;
        }

        private static string GetDryGoodsOrderTable(Company company, List<Attachment> imgAttachments)
        {
            var shoppingCartItemRepo = new ShoppingCartItemRepository();

            var quote = shoppingCartItemRepo
                .Read()
                .Where(x => x.CompanyID == company.ID)
                .Include(x => x.Product)
                .ToList()
                .Where(x => (x.Product.Supplier.UseMasterAccount && company.EasySuppliersActivated) || company.Method_IsLive(x.Product.Supplier))
                .ToList();

            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th>Product</th>" +
                                            "<th>Product Code</th>" +
                                            "<th>Cost</th>" +
                                            "<th>Qty</th>" +
                                            "<th>Net</th>" +
                                            "<th>VAT</th>" +
                                            "<th>Gross</th>" +
                                        "</tr>" +
                                    "</thead>";

            var tableBody = "";
            var totalNet = 0m;
            var totalVAT = 0m;
            var totalGross = 0m;

            foreach (var item in quote)
            {
                var str = String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                        "</tr>", item.Product.Name
                                               , item.Product.ProductCode
                                               , item.Product.ExclusivePrice(company).ToString("C")
                                               , item.Qty
                                               , (item.Product.ExclusivePrice(company) * item.Qty).ToString("C")
                                               , (item.Product.ExclusivePrice(company) * item.Qty * item.Product.VAT).ToString("C")
                                               , ((item.Product.ExclusivePrice(company) * item.Qty) + (item.Product.ExclusivePrice(company) * item.Qty * item.Product.VAT)).ToString("C"));
                tableBody += str;

                totalNet += (item.Product.ExclusivePrice(company) * item.Qty);
                totalVAT += ((item.Product.ExclusivePrice(company) * item.Qty) * item.Product.VAT);
                totalGross += ((item.Product.ExclusivePrice(company) * item.Qty) + ((item.Product.ExclusivePrice(company) * item.Qty) * item.Product.VAT));
            }

            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;' colspan='4'>Order Total</td>" +
                                            "<td style='text-align: center; color: red;'>{0}</td>" +
                                            "<td style='text-align: center; color: red;'>{1}</td>" +
                                            "<td style='text-align: center; color: red;'>{2}</td>" +
                                        "</tr>", totalNet.ToString("C")
                                               , totalVAT.ToString("C")
                                               , totalGross.ToString("C"));

            string tableFooter = "</table>";

            return tableHeader + tableBody + tableFooter;
        }

    }
}
