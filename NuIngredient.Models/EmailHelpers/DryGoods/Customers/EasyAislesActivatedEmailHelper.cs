﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class EasyAislesActivatedEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Company company)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(company.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = GetSubject(company);
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(company) +
                                                AppendCompanyDetails(company) + 
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string GetAllEasyAisleNames()
        {
            var easyAisles = new AisleRepository()
                 .Read()
                 .Where(x => x.Deleted == false)
                 .Where(x => x.StartDate <= DateTime.Today)
                 .Where(x => x.EndDate == null || x.EndDate > DateTime.Today)
                 .Where(x => x.Supplier.UseMasterAccount);

            var aisleNames = String.Join(", ", easyAisles.Select(x => x.Label).ToList());

            return aisleNames;
        }

        private static string AppendMessage(Company company)
        {
            var callBackUrl = String.Format("{0}{1}", _customerSiteUrl, "Dashboard/Index");
            var aisleNames = GetAllEasyAisleNames();

            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + company.Name + "</span>,<br />" +
                                    "<br />" +
                                    "Congratulations- your " + aisleNames + " aisle is now activated!<br />" +
                                    "<br />" +
                                    "Please accept this e-mail as confirmation that your Office Groceries " + aisleNames + " aisles are now activated. You can start placing orders from now on.<br />" +
                                    "<br />" +
                                    "Moving forward, if you would like to make any changes or place further orders, please login to our <a href='" + callBackUrl + "'>client area</a> using the login details you created during the account set up process.<br />" +
                                    "<br />" +
                                    "If you have any questions or require support, please don’t hesitate to get in contact. Either reply to this e-mail or contact us at <a href='mailto:Orders@office-groceries.com'>Orders@office-groceries.com</a>, alternatively give us call on 0345 463 8863 and one of the team will be happy to help!<br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";
            return message;
        }
        private static string GetSubject(Company company)
        {
            return String.Format("{0} Account Confirmation from Office Groceries", GetAllEasyAisleNames());
        }

    }
}
