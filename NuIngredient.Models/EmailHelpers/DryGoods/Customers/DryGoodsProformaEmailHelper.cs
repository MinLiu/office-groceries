﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    /// <summary>
    /// This is the email that is sent to the customer's approver when they place an order
    /// </summary>
    public class DryGoodsProformaEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Guid companyID, Guid orderID, DateTime? deliveryDate)
        {
            var company = new CompanyRepository().Find(companyID);
            var approver = company.ProformaApprover;
            var order = new OneOffOrderRepository().Find(orderID);

            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(approver.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = $"Proforma Order {order.OrderNumber} from Office Groceries – Please action ASAP";
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(company, approver, deliveryDate) +
                                                AppendCompanyDetails(company, order) +
                                                AppendEventBody(company, order, imgAttachments) +
                                                AppendOrderBody(company, order, imgAttachments) +
                                                AppendButtons(company, order, imgAttachments) +
                                                AppendFooter() +
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(Company company, ProformaApprover approver, DateTime? deliveryDate)
        {
            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + approver.Name + "</span>,<br />" +
                                    "<br />" +
                                    $"{company.Name} has placed an order for approval, please action ASAP in order to avoid delays in delivery date ({deliveryDate?.ToString("dd/MM/yyyy")}).<br />" +
                                    "<br />" +
                                    "Please find the details of the order below.<br />" +
                                    "<br />" +
                                    "If you have any questions, or we can help with anything at all, please don’t hesitate to contact us on 0345 463 8863 / <a href='mailto:orders@office-groceries.com'>Orders@office-groceries.com</a> and we will be happy to assist.<br />" +
                                    "<br />" +
                                    "Thanks again! <br />" +
                                    "<br />" +
                                    "The Office Groceries Team" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";

            return message;
        }

        private static string AppendCompanyDetails(Company company, OneOffOrder order)
        {
            if (IsEventOrder(order))
            {
                var companyDetails = "<tr>" +
                                    "<td>" +
                                        "<table style='border: 1px solid white;'>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Account Number:</label></td><td>" + company.AccountNo + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Company:</label></td><td>" + company.Name + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Delivery Instructions:</label><td>" + company.DeliveryInstruction + "</td>" +
                                            "</tr>" +
                                        "</table>" +
                                    "</td>" +
                                "</tr>";
                return companyDetails;
            }
            else
            {
                return AppendCompanyDetails(company);
            }
        }

        private static string AppendEventBody(Company company, OneOffOrder order, List<Attachment> imgAttachments)
        {
            if (!IsEventOrder(order))
            {
                return "";
            }

            var body = GetEventDetailsTable(company, order, imgAttachments);

            var eventBody = "<tr>" +
                                "<td style='" + _style_font_family + "'>" +
                                    body +
                                "</td>" +
                            "</tr>";
            return eventBody;
        }

        private static string GetEventDetailsTable(Company company, OneOffOrder order, List<Attachment> imgAttachments)
        {
            var eventDetailTemplate = "";
            foreach (var eventDetail in order.OrderEventDetails)
            {
                eventDetailTemplate += String.Format(
                    $"<table style='width: 700px; border: 1px solid white;'>" +
                        "<tr><td><label>Function Name</label></td><td>{0}</td></tr>" +
                        "<tr><td><label>Address Line 1</label></td><td>{1}</td></tr>" +
                        "<tr><td><label>Address Line 2</label></td><td>{2}</td></tr>" +
                        "<tr><td><label>Town</label></td><td>{3}</td></tr>" +
                        "<tr><td><label>Postcode</label></td><td>{4}</td></tr>" +
                        "<tr><td><label>Room Name</label></td><td>{5}</td></tr>" +
                        "<tr><td><label>Event Date</label></td><td>{6}</td></tr>" +
                        "<tr><td><label>Start Time</label></td><td>{7}</td></tr>" +
                        "<tr><td><label>Delivery Details</label></td><td>{8}</td></tr>" +
                        "<tr><td><label>Special Requirements</label></td><td>{9}</td></tr>" +
                    "</table>"
                    , eventDetail.FunctionName
                    , eventDetail.AddressLine1
                    , eventDetail.AddressLine2
                    , eventDetail.Town
                    , eventDetail.Postcode
                    , eventDetail.RoomName
                    , eventDetail.EventDate.ToString("dd/MM/yyyy")
                    , eventDetail.StartTime.ToString("HH:mm")
                    , eventDetail.DeliveryDetails
                    , eventDetail.SpecialRequirements);
            }
            return eventDetailTemplate;
        }

        private static string AppendOrderBody(Company company, OneOffOrder order, List<Attachment> imgAttachments)
        {
            var body = GetDryGoodsOrderTable(company, order, imgAttachments);

            var orderBody = "<tr>" +
                                "<td style='" + _style_font_family + "'>" +
                                    body +
                                "</td>" +
                            "</tr>";
            return orderBody;
        }

        private static string GetDryGoodsOrderTable(Company company, OneOffOrder order, List<Attachment> imgAttachments)
        {
            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th>Product</th>" +
                                            "<th>Product Code</th>" +
                                            "<th>Cost</th>" +
                                            "<th>Qty</th>" +
                                            "<th>Net</th>" +
                                            "<th>VAT</th>" +
                                            "<th>Gross</th>" +
                                        "</tr>" +
                                    "</thead>";

            var tableBody = "";
            var totalNet = 0m;
            var totalVAT = 0m;
            var totalGross = 0m;

            foreach (var item in order.OneOffOrderItems)
            {
                var str = String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                        "</tr>", item.ProductName
                                               , item.ProductCode
                                               , item.ProductPrice.ToString("C")
                                               , item.Qty
                                               , item.Total.ToString("C")
                                               , (item.Total * item.ProductVAT).ToString("C")
                                               , (item.Total + item.Total * item.ProductVAT).ToString("C"));
                tableBody += str;

                totalNet += item.Total;
                totalVAT += (item.Total * item.ProductVAT);
                totalGross += (item.Total + item.Total * item.ProductVAT);
            }

            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;' colspan='4'>Order Total</td>" +
                                            "<td style='text-align: center; color: red;'>{0}</td>" +
                                            "<td style='text-align: center; color: red;'>{1}</td>" +
                                            "<td style='text-align: center; color: red;'>{2}</td>" +
                                        "</tr>", totalNet.ToString("C")
                                               , totalVAT.ToString("C")
                                               , totalGross.ToString("C"));

            string tableFooter = "</table>";

            return tableHeader + tableBody + tableFooter;
        }
        
        private static string AppendButtons(Company company, OneOffOrder order, List<Attachment> imgAttachments)
        {
            var approveUrl = $"{_customerSiteUrl}Proforma/ActionOrder?orderId={order.ID}&accept=true&checksum={order.GetChecksum()}";
            var rejectUrl = $"{_customerSiteUrl}Proforma/ActionOrder?orderId={order.ID}&accept=false&checksum={order.GetChecksum()}";
            
            var orderBody = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "You can approve or reject the order by clicking the links below:<br />" +
                                "<a href='" + approveUrl + "' style=''>Approve</a><br/>" +
                                "<a href='" + rejectUrl + "' style=''>Reject</a>" +
                            "</td>" +
                            "</tr>";
            return orderBody;
        }

        private static bool IsEventOrder(OneOffOrder order)
        {
            return order.OrderEventDetails != null && order.OrderEventDetails.Any();
        }
    }
}
