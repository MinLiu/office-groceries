﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class DifficultAislesActivatedEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(CompanySupplier companySupplier)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(companySupplier.Company.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = String.Format("{0} Account Confirmation from Office Groceries", companySupplier.Supplier.DefaultAisleLabels);
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(companySupplier) +
                                                AppendCompanyDetails(companySupplier.Company) + 
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(CompanySupplier companySupplier)
        {
            var callBackUrl = String.Format("{0}{1}", _customerSiteUrl, "Dashboard/Index");

            var deliveryDays = new List<string>();
            if (companySupplier.DeliveryMon) deliveryDays.Add("<strong>Monday</strong>");
            if (companySupplier.DeliveryTue) deliveryDays.Add("<strong>Tuesday</strong>");
            if (companySupplier.DeliveryWed) deliveryDays.Add("<strong>Wednesday</strong>");
            if (companySupplier.DeliveryThu) deliveryDays.Add("<strong>Thursday</strong>");
            if (companySupplier.DeliveryFri) deliveryDays.Add("<strong>Friday</strong>");
            if (companySupplier.DeliverySat) deliveryDays.Add("<strong>Saturday</strong>");

            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + companySupplier.Company.Name + "</span>,<br />" +
                                    "<br />" +
                                    "Congratulations- your " + companySupplier.Supplier.DefaultAisleLabels + " aisle is now activated!<br />" +
                                    "<br />" +
                                    "Please accept this e-mail as confirmation that your Office Groceries " + companySupplier.Supplier.DefaultAisleLabels + " aisle is now activated. You can start placing orders from " + companySupplier.StartDate.Value.ToString("dd/MM/yyyy") + ".<br />" +
                                    "<br />" +
                                    "Available delivery days to your business are: " + String.Join(", ", deliveryDays) + "<br/>" +
                                    "<br />" +
                                    "Moving forward, if you would like to make any changes or place further orders, please login to our <a href='" + callBackUrl + "'>client area</a> using the login details you created during the account set up process.<br />" +
                                    "<br />" +
                                    "If you have any questions or require support, please don’t hesitate to get in contact. Either reply to this e-mail or contact us at <a href='mailto:Orders@office-groceries.com'>Orders@office-groceries.com</a>, alternatively give us call on 0345 463 8863 and one of the team will be happy to help!<br />" +
                                    "<br />" +
                                    "Thank you from the Office Groceries Team" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";
            return message;
        }
    }
}
