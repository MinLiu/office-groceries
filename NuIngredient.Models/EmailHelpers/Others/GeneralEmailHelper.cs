﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class GeneralEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(string title, string content, List<string> emailsTo, List<string> emailsBcc, List<EmailAttachment> emailAttachments)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(emailsTo);
            mailMessage.AddBccEmails(emailsBcc);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = title;
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(content) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            foreach (var attachment in emailAttachments)
            {
                try
                { 
                    var newAttachment = new Attachment(_serverPath + attachment.FilePath);
                    newAttachment.Name = attachment.FileName;
                    mailMessage.Attachments.Add(newAttachment);
                }
                catch(Exception e) { }
            }

            return mailMessage;
        }

        private static string AppendMessage(string content)
        {
            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    content +    
                                "</p>" +
                            "</td>" +
                        "</tr>";
            return message;
        }

    }
}
