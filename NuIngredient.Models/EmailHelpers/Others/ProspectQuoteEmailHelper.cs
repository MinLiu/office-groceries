﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ProspectQuoteEmailHelper: DefaultEmailHelper
    {
        public static MailMessage GetEmail(Prospect prospect, out List<string> categoryList)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            QuoteExist(prospect.ProspectToken, out bool milk, out bool fruit, out bool dryGoods, out categoryList);

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(prospect.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = GetQuoteEmailSubject(milk, fruit, dryGoods);
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(prospect, categoryList) +
                                                AppendQuoteBody(prospect, milk, fruit, dryGoods, imgAttachments) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            try
            {
                var termsConditions = new Attachment(_serverPath + "/Content/OG Terms & Conditions.pdf");
                termsConditions.Name = "Terms & Conditions.pdf";
                mailMessage.Attachments.Add(termsConditions);
            }
            catch { }

            try
            { 
                var brochure = new Attachment(_serverPath + "/Content/OG Brochure.pdf");
                brochure.Name = "OG Brochure.pdf";
                mailMessage.Attachments.Add(brochure);
            }
            catch { }
            
            try
            { 
                var poManagement = new Attachment(_serverPath + "/Content/Purchase Order Management.pdf");
                poManagement.Name = "Purchase Order Management.pdf";
                mailMessage.Attachments.Add(poManagement);
            }
            catch { }
            
            return mailMessage;
        }

        private static string AppendMessage(Prospect prospect, List<string> categoryList)
        {
            var localDepotTerm = categoryList.Contains("Milk") ? "dairy" : "depot";
            
            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + prospect.CompanyName + "</span>,<br />" +
                                    "<br />" +
                                    $"Thank you so much for your enquiry regarding our {string.Join(", ", categoryList)} delivery service.<br />" +
                                    "<br />" +
                                    $"Please find below your bespoke quote for our {string.Join(", ", categoryList)} deliveries to your business. If you would like to go ahead with these deliveries, you just need to complete your account set up. Please do note that any requested delivery days will not be fully confirmed until we receive final confirmation from your local {localDepotTerm}.<br />" +
                                    "<br />" +
                                    "<strong>Setting up an account is easy:</strong><br />" +
                                "</p>" +
                                "<ul style='margin-bottom:0;'>" +
                                    "<li>" +
                                        "Simply click the link below to complete account set up and create your password!" +
                                    "</li>" +
                                    "<li>" +
                                        "Alternatively, give us a bell on <strong style='color: #32b24a;'>0345 463 8863</strong> or email <a href='mailto:Orders@office-groceries.com' style='color: #f57f20; font-weight: 700; text-decoration: none;'>Orders@office-groceries.com</a> and one of our key account managers will be in touch! We will be happy set up your account and answer any questions you may have regarding your account with Office Groceries." +
                                    "</li>" +
                                "</ul>" +
                            "</td>" +
                        "</tr>";
            return message;
        }

        private static string AppendQuoteBody(Prospect prospect, bool milk, bool fruit, bool dryGoods, List<Attachment> imgAttachments)
        {
            var body = GetQuoteEmailBody(prospect, milk, fruit, dryGoods, imgAttachments);

            var quoteBody = "<tr>" +
                                "<td style='" + _style_font_family + "'>" +
                                    body +
                                "</td>" +
                            "</tr>";
            return quoteBody;
        }

        private static void QuoteExist(string prospectToken, out bool milk, out bool fruit, out bool dryGoods, out List<string> categoryList)
        {
            using (var context = new SnapDbContext())
            {
                milk = context.MilkOrderHeaders.Where(x => x.ProspectToken == prospectToken).Any();
                fruit = context.FruitOrderHeaders.Where(x => x.ProspectToken == prospectToken).Any();
                dryGoods = context.ShoppingCartItems.Where(x => x.ProspectToken == prospectToken).Any();
                categoryList = new List<string>();
                if (milk) categoryList.Add("Milk");
                if (fruit) categoryList.Add("Fruit/Snacks");
                if (dryGoods) categoryList.Add("Dry Goods");
            }
        }

        private static string GetQuoteEmailSubject(bool milk, bool fruit, bool dryGoods)
        {
            var items = new List<string>();
            if (milk) items.Add("Milk");
            if (fruit) items.Add("Fruit/Snacks");
            if (dryGoods) items.Add("Dry Goods");

            return String.Format("{0} Quote from Office Groceries", String.Join(", ", items));
        }

        private static string GetQuoteEmailBody(Prospect prospect, bool milk, bool fruit, bool dryGoods, List<Attachment> attachments)
        {
            string fruitQuote = "";
            string milkQuote = "";
            string dryGoodsQuote = "";

            if (milk) milkQuote = GetMilkQuote(prospect, attachments);
            if (fruit) fruitQuote = GetFruitQuote(prospect, attachments);
            if (dryGoods) dryGoodsQuote = GetOneOffQuote(prospect, attachments);

            string link = AppendLink(prospect, milk, fruit, dryGoods, attachments);


            return "<tr><td>" + fruitQuote + "</td></tr>"
                 + "<tr><td>" + milkQuote + "</td></tr>" 
                 + "<tr><td>" + dryGoodsQuote + "</td></tr>"
                 + "<tr><td>" + link + "</td></tr>"
                 + "<tr><td></td></tr>"
                 + "<tr><td></td></tr>";
        }

        private static string AppendLink(Prospect prospect, bool milk, bool fruit, bool dryGoods, List<Attachment> attachments)
        {
            var categoryList = new List<string>();
            if (milk) categoryList.Add("Milk");
            if (fruit) categoryList.Add("Fruit/Snacks");
            if (dryGoods) categoryList.Add("Dry Goods");

            var activeBtnBase64 = "";

            try
            {
                var activeBtnBytes = File.ReadAllBytes(_serverPath + "/Content/OG_Activate.png");
                activeBtnBase64 = Convert.ToBase64String(activeBtnBytes);
            }
            catch { }
            
            var content = String.Format("<table style='width:100%;'>" +
                                            "<tr>" +
                                                "<td>" +
                                                    "<strong><a href='{0}Account/Register?prospectToken={1}'>Please click here to set up your {2} account today.</a></strong>" +
                                                "</td>" +
                                                "<td align='right'>" +
                                                    "<a href='{0}Account/Register?prospectToken={1}'>" +
                                                        $"<img src='data:image/jpeg;base64,{activeBtnBase64}' alt='Activate Now'/>" +
                                                    "</a>" +
                                                "</td>" +
                                            "</tr>" +
                                        "</table>"
                                        , _customerSiteUrl
                                        , prospect.ProspectToken
                                        , String.Join(", ", categoryList));

            return content;
        }

        private static string GetFruitQuote(Prospect prospect, List<Attachment> attachments)
        {
            var fruitOrderHeaderRepo = new FruitOrderHeaderRepository();
            var quoteOrderHeader = fruitOrderHeaderRepo.Read()
                                                       .Where(x => x.ProspectToken == prospect.ProspectToken)
                                                       .Include(x => x.Items)
                                                       .FirstOrDefault();
            if (quoteOrderHeader.Items.Count() == 0)
                return "";

            string title = "Regular Fruit Order";

            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th>Product</th>" +
                                            "<th>Unit</th>" +
                                            "<th>Cost</th>" +
                                            "<th style='width: 32px;'>Mon</th>" +
                                            "<th style='width: 32px;'>Tue</th>" +
                                            "<th style='width: 32px;'>Wed</th>" +
                                            "<th style='width: 32px;'>Thu</th>" +
                                            "<th style='width: 32px;'>Fri</th>" +
                                            "<th style='width: 32px;'>Sat</th>" +
                                            "<th>Weekly Volume</th>" +
                                            "<th>Weekly Net</th>" +
                                            "<th>Weekly VAT</th>" +
                                            "<th>Weekly Gross</th>" +
                                        "</tr>" +
                                    "</thead>";

            string tableBody = "";

            var fruitPdtService = new FruitProductService(new SnapDbContext());
            foreach (var order in quoteOrderHeader.Items)
            {
                var subtotalResult = fruitPdtService.CalculateSubtotal(order.FruitProductID, order.OrderHeader.CompanyID, order.Monday, order.Tuesday, order.Wednesday, order.Thursday, order.Friday, order.Saturday);
                var weeklyNet = subtotalResult.Subtotal;
                var unitPrice = subtotalResult.UnitPrice;
                var weeklyVat = weeklyNet * order.VATRate;
                var weeklyGross = weeklyNet + weeklyVat;
                var str = String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                            "<td style='text-align: center;'>{7}</td>" +
                                            "<td style='text-align: center;'>{8}</td>" +
                                            "<td style='text-align: center;'>{9}</td>" +
                                            "<td style='text-align: center;'>{10}</td>" +
                                            "<td style='text-align: center;'>{11}</td>" +
                                            "<td style='text-align: center;'>{12}</td>" +
                                        "</tr>", order.ProductName
                                               , order.Unit
                                               , unitPrice.ToString("C")
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Monday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Tuesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Wednesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Thursday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Friday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Saturday)
                                               , order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday
                                               , weeklyNet.ToString("C")
                                               , weeklyVat.ToString("C")
                                               , weeklyGross.ToString("C"));
                tableBody += str;
            }

            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;' colspan = '9'>Total</td>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'><strong>{3}</strong></td>" +
                                        "</tr>", quoteOrderHeader.Items.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday)
                                               , quoteOrderHeader.TotalNet.ToString("C")
                                               , quoteOrderHeader.TotalVAT.ToString("C")
                                               , quoteOrderHeader.TotalGross.ToString("C"));

            string tableFooter = "</table>";

            return title + tableHeader + tableBody + tableFooter;
        }

        private static string GetMilkQuote(Prospect prospect, List<Attachment> attachments)
        {
            var milkOrderHeaderRepo = new MilkOrderHeaderRepository();
            var quoteOrderHeader = milkOrderHeaderRepo.Read()
                                                      .Where(x => x.ProspectToken == prospect.ProspectToken)
                                                      .Include(x => x.Items)
                                                      .FirstOrDefault();
            if (quoteOrderHeader.Items.Count() == 0)
                return "";

            string title = "Regular Milk Order";

            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th>Product</th>" +
                                            "<th>Unit</th>" +
                                            "<th>Cost</th>" +
                                            "<th style='width: 32px;'>Mon</th>" +
                                            "<th style='width: 32px;'>Tue</th>" +
                                            "<th style='width: 32px;'>Wed</th>" +
                                            "<th style='width: 32px;'>Thu</th>" +
                                            "<th style='width: 32px;'>Fri</th>" +
                                            "<th style='width: 32px;'>Sat</th>" +
                                            "<th>Weekly Volume</th>" +
                                            "<th>Weekly Total</th>" +
                                        "</tr>" +
                                    "</thead>";

            string tableBody = "";
            var monDelivery = false;
            var tueDelivery = false;
            var wedDelivery = false;
            var thuDelivery = false;
            var friDelivery = false;
            var satDelivery = false;
            foreach (var order in quoteOrderHeader.Items)
            {
                var str = String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                            "<td style='text-align: center;'>{7}</td>" +
                                            "<td style='text-align: center;'>{8}</td>" +
                                            "<td style='text-align: center;'>{9}</td>" +
                                            "<td style='text-align: center;'>{10}</td>" +
                                        "</tr>", order.ProductName
                                               , order.Unit
                                               , order.Price.ToString("C")
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Monday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Tuesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Wednesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Thursday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Friday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Saturday)
                                               , order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday
                                               , ((order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday) * order.Price).ToString("C"));
                tableBody += str;
                
                if (order.Monday > 0) monDelivery = true;
                if (order.Tuesday > 0) tueDelivery = true;
                if (order.Wednesday > 0) wedDelivery = true;
                if (order.Thursday > 0) thuDelivery = true;
                if (order.Friday > 0) friDelivery = true;
                if (order.Saturday > 0) satDelivery = true;
            }

            var weeklyTotal = quoteOrderHeader.TotalNet;
            var deliveryCharge = DeliveryChargeHelper.GetMilkDeliveryCharge(prospect.Postcode);
            if (deliveryCharge != null)
            {
                var totalDrops = new List<bool>()
                    { monDelivery, tueDelivery, wedDelivery, thuDelivery, friDelivery, satDelivery }.Count(x => x);

                var totalDeliveryCharge = totalDrops * deliveryCharge.Charge;
                
                tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2:C}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                            "<td style='text-align: center;'>{7}</td>" +
                                            "<td style='text-align: center;'>{8}</td>" +
                                            "<td style='text-align: center;'>{9}</td>" +
                                            "<td style='text-align: center;'>{10:C}</td>" +
                                        "</tr>", deliveryCharge.Name
                                               , "Per Drop"
                                               , deliveryCharge.Charge
                                               , monDelivery ? "v" : ""
                                               , tueDelivery ? "v" : ""
                                               , wedDelivery ? "v" : ""
                                               , thuDelivery ? "v" : ""
                                               , friDelivery ? "v" : ""
                                               , satDelivery ? "v" : ""
                                               , totalDrops
                                               , totalDeliveryCharge);

                weeklyTotal += totalDeliveryCharge;
            }
            
            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                "<td style='text-align: center;' colspan = '9'>Total</td>" +
                                "<td style='text-align: center;'>{0}</td>" +
                                "<td style='text-align: center;'><strong>{1:C}</strong></td>" +
                            "</tr>", quoteOrderHeader.Items.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday)
                                   , weeklyTotal);

            string tableFooter = "</table>" + "<br/>*Please note that there may be a small delivery fee for milk, applicable in some postcodes. Please ask the team for more details.";

            return title + tableHeader + tableBody + tableFooter;
        }

        private static string GetOneOffQuote(Prospect prospect, List<Attachment> attachments)
        {
            var shoppingCartItemRepo = new ShoppingCartItemRepository();
            var shoppingCartItems = shoppingCartItemRepo.Read()
                                                        .Where(x => x.ProspectToken == prospect.ProspectToken)
                                                        .Where(x => x.Product.IsActive == true)
                                                        .Where(x => x.Product.IsExclusive == false)
                                                        .Where(x => x.Product.IsProspectsVisible == true)
                                                        .Where(x => x.Product.Deleted == false)
                                                        .ToList();
            if (shoppingCartItems.Count() == 0)
                return "";

            string title = "Dry Goods Products" +
                           "<p>" +
                               "<i>All prices quoted are exclusive of any applicable VAT.</i>" +
                           "</p>";

            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th>Product</th>" +
                                            "<th>Unit</th>" +
                                            "<th>Price</th>" +
                                            "<th>Qty</th>" +
                                            "<th>Total</th>" +
                                        "</tr>" +
                                    "</thead>";

            string tableBody = "";
            foreach (var item in shoppingCartItems)
            {
                var str = String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                        "</tr>", item.Product.Name
                                               , item.Product.Unit
                                               , item.Product.Price.ToString("C")
                                               , item.Qty
                                               , (item.Qty * item.Product.Price).ToString("C"));
                tableBody += str;
            }

            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                    "<td style='text-align: center;' colspan = '4'>Total</td>" +
                    "<td style='text-align: center;'><strong>{0}</strong></td>" +
                "</tr>", shoppingCartItems.Sum(x => x.Qty * x.Product.Price).ToString("C"));

            string tableFooter = "</table>";

            return title + tableHeader + tableBody + tableFooter;
        }

    }
}
