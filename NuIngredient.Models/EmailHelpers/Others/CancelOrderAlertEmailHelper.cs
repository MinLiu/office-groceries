﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class CancelOrderAlertEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Company company, string type)
        {
            var setting = new SnapDbContext().EmailSettings.First();
            var emalsTo = new SnapDbContext().Settings.FirstOrDefault()?.FeedbackCenterEmail;
            
            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(emalsTo);
            //mailMessage.To.Add("min.liu@zigaflow.com");
            mailMessage.To.Add("dustin@office-groceries.com");
            // mailMessage.To.Add("Lily@office-groceries.com");
            mailMessage.To.Add("stephen@office-groceries.com");
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = $"{company.Name} is zeroing {type} standing order";
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(company, type) +
                                                AppendCompanyDetails(company) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(Company company, string type)
        {
            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    $"<span>{company.Name} is zeroing their {type} standing order</span>, please follow." +
                                    "<br />" +
                                "</p>" +
                                " <hr />" +
                            "</td>" +
                        "</tr>";

            return message;
        }

    }
}
