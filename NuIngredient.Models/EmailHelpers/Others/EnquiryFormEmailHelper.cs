﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class EnquiryFormEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(string email, string firstName, string categories)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = "Your Office-Groceries Enquiry Has Been Received";
            mailMessage.Body = "<html>" +
                                    "<body>" +
                                        "<table align='center' style='" + _email_table_style + "'>" +
                                            "<tbody>" +
                                                AppendLogo(imgAttachments) +
                                                AppendMessage(firstName, categories) +
                                                AppendFooter() + 
                                            "</tbody>" +
                                        "</table>" +
                                    "</body>" +
                                "</html>";
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            return mailMessage;
        }

        private static string AppendMessage(string firstName, string categories)
        {
            var message = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span>Hello " + firstName + "</span>,<br />" +
                                    "<br />" +
                                    "Welcome to Office-Groceries.<br />" +
                                    "<br />" +
                                    "Thank you for your enquiry regarding our " + categories + " delivery service for your business!<br />" +
                                    "<br />" +
                                    "One of our procurement specialists will be in contact with you shortly, so that we can get a better understanding of your needs and ensure your account is tailor-made for your business.<br />" +
                                    "<br />" +
                                    "In the meantime, if you have any questions at all, please don’t hesitate to give us a bell on <span style = 'white-space: nowrap;'> 0345 463 8863</span >, or email <a href='mailto:orders@office-groceries.com'>orders@office-groceries.com</a> and we will be happy to assist.<br />" +
                                    "<br />" +
                                    "Once again, thank you for your enquiry, " + firstName + ".<br />" +
                                    "<br />" +
                                    "The Office-Groceries Team" +
                                "</p>" +
                            "</td>" +
                        "</tr>";
            return message;
        }

    }
}
