﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using RazorEngine;
using RazorEngine.Templating;
using NuIngredient.Models.EmailTemplateVms;

namespace NuIngredient.Models
{
    public class ProspectQuotePromotionalEmailHelper : DefaultEmailHelper
    {
        public static MailMessage GetEmail(Prospect prospect, out List<string> categoryList)
        {
            var setting = new SnapDbContext().EmailSettings.First();
            var promotionOffer = new PromotionOfferRepository().Read().FirstOrDefault();
            QuoteExist(prospect.ProspectToken, out bool milk, out bool fruit, out bool dryGoods, out categoryList);

            var imgAttachments = new List<Attachment>();

            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(prospect.Email);
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.Subject = GetQuoteEmailSubject(milk, fruit, dryGoods, promotionOffer);
            mailMessage.Body = AppendBody(prospect, milk, fruit, dryGoods, categoryList);
            mailMessage.IsBodyHtml = true;

            foreach (var attachement in imgAttachments)
            {
                try
                {
                    mailMessage.Attachments.Add(attachement);
                }
                catch { }
            }

            try
            {
                var termsConditions = new Attachment(_serverPath + "/Content/OG Terms & Conditions.pdf");
                termsConditions.Name = "Terms & Conditions.pdf";
                mailMessage.Attachments.Add(termsConditions);
            }
            catch { }

            try
            { 
                var brochure = new Attachment(_serverPath + "/Content/OG Brochure.pdf");
                brochure.Name = "OG Brochure.pdf";
                mailMessage.Attachments.Add(brochure);
            }
            catch { }
            
            try
            { 
                var poManagement = new Attachment(_serverPath + "/Content/Purchase Order Management.pdf");
                poManagement.Name = "Purchase Order Management.pdf";
                mailMessage.Attachments.Add(poManagement);
            }
            catch { }
            
            return mailMessage;
        }

        private static string AppendBody(Prospect prospect, bool milk, bool fruit, bool dryGoods, List<string> categoryList)
        {
            var vm = new EmailTemplateVm
            {
                CompanyName = prospect.CompanyName,
                Category = string.Join(", ", categoryList),
                CallBackUrl = $"{_customerSiteUrl}Account/Register?prospectToken={prospect.ProspectToken}",
                UnsubscribeUrl = $"{_customerSiteUrl}prospects/unsubscribe?prospectToken={prospect.ProspectToken}",
            };

            if (milk)
            {
                vm.MilkWeeklyQuote = GetMilkQuote(prospect);
            }

            if (fruit)
            {
                vm.FruitWeeklyQuote = GetFruitQuote(prospect);
            }

            if (dryGoods)
            {
                vm.DryGoodQuote = GetOneOffQuote(prospect);
            }
            
            var isTemplateCached = Engine.Razor.IsTemplateCached("promoteEmail", typeof(EmailTemplateVm));

            if (isTemplateCached)
            {
                return Engine.Razor.Run("promoteEmail", typeof(EmailTemplateVm), vm);
            }
            
            var template = ResourceManager.GetPromotionEmailTemplate();

            return Engine.Razor.RunCompile(template, "promoteEmail", typeof(EmailTemplateVm), vm);
        }
        
        private static void QuoteExist(string prospectToken, out bool milk, out bool fruit, out bool dryGoods, out List<string> categoryList)
        {
            using (var context = new SnapDbContext())
            {
                milk = context.MilkOrderHeaders.Any(x => x.ProspectToken == prospectToken);
                fruit = context.FruitOrderHeaders.Any(x => x.ProspectToken == prospectToken);
                dryGoods = context.ShoppingCartItems.Any(x => x.ProspectToken == prospectToken);
                categoryList = new List<string>();
                if (milk) categoryList.Add("Milk");
                if (fruit) categoryList.Add("Fruit/Snacks");
                if (dryGoods) categoryList.Add("Dry Goods");
            }
        }

        private static string GetQuoteEmailSubject(bool milk, bool fruit, bool dryGoods, PromotionOffer promotionOffer)
        {
            if (!string.IsNullOrWhiteSpace(promotionOffer.Subject))
            {
                return promotionOffer.Subject;
            }
            
            var items = new List<string>();
            if (milk) items.Add("Milk");
            if (fruit) items.Add("Fruit/Snacks");
            if (dryGoods) items.Add("Dry Goods");

            return String.Format("{0} Quote from Office Groceries", String.Join(", ", items));
        }

        private static WeeklyOrderVm GetFruitQuote(Prospect prospect)
        {
            var fruitOrderHeaderRepo = new FruitOrderHeaderRepository();
            var quoteOrderHeader = fruitOrderHeaderRepo.Read()
                .Where(x => x.ProspectToken == prospect.ProspectToken)
                .Include(x => x.Items)
                .FirstOrDefault();

            if (quoteOrderHeader == null)
                return null;
            
            if (!quoteOrderHeader.Items.Any())
                return null;

            var result = new WeeklyOrderVm();
            
            var fruitPdtService = new FruitProductService(new SnapDbContext());

            foreach (var item in quoteOrderHeader.Items)
            {
                var subtotalResult = fruitPdtService.CalculateSubtotal(item.FruitProductID, null, item.Monday, item.Tuesday, item.Wednesday, item.Thursday, item.Friday, item.Saturday);
                var weeklyNet = subtotalResult.Subtotal;
                var unitPrice = subtotalResult.UnitPrice;
                var weeklyVat = Math.Round(weeklyNet * item.VATRate, 2, MidpointRounding.AwayFromZero);
                
                var lineItem = new WeeklyOrderLineItemVm
                {
                    ProductName = item.ProductName,
                    Unit = item.Unit,
                    UnitPrice = unitPrice,
                    Mon = item.Monday,
                    Tue = item.Tuesday,
                    Wed = item.Wednesday,
                    Thu = item.Thursday,
                    Fri = item.Friday,
                    Sat = item.Saturday,
                    WeeklyPrice = weeklyNet,
                    WeeklyVAT = weeklyVat
                };

                result.LineItems.Add(lineItem);
            }
            
            return result;
        }
        
        private static WeeklyOrderVm GetMilkQuote(Prospect prospect)
        {
            var milkOrderHeaderRepo = new MilkOrderHeaderRepository();
            var quoteOrderHeader = milkOrderHeaderRepo.Read()
                .Where(x => x.ProspectToken == prospect.ProspectToken)
                .Include(x => x.Items)
                .FirstOrDefault();

            if (quoteOrderHeader == null)
                return null;
            
            if (!quoteOrderHeader.Items.Any())
                return null;
            
            var result = new WeeklyOrderVm
            {
                LineItems = quoteOrderHeader.Items.Select(x => new WeeklyOrderLineItemVm
                {
                    ProductName = x.ProductName,
                    Unit = x.Unit,
                    UnitPrice = x.Price,
                    Mon = x.Monday,
                    Tue = x.Tuesday,
                    Wed = x.Wednesday,
                    Thu = x.Thursday,
                    Fri = x.Friday,
                    Sat = x.Saturday,
                    WeeklyPrice = Math.Round((x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday) * x.Price, 2, MidpointRounding.AwayFromZero),
                }).ToList(),
            };
            
            var deliveryCharge = DeliveryChargeHelper.GetMilkDeliveryCharge(prospect.Postcode);
            if (deliveryCharge == null)
                return result;
            
            result.DeliveryCharges = new DeliveryChargeVm
            {
                ProductName = deliveryCharge.Name,
                Unit = "Per Drop",
                UnitPrice = deliveryCharge.Charge,
                Mon = result.MondayDrop,
                Tue = result.TuesdayDrop,
                Wed = result.WednesdayDrop,
                Thu = result.ThursdayDrop,
                Fri = result.FridayDrop,
                Sat = result.SaturdayDrop,
            };

            return result;
        }
        
        private static DryGoodQuoteVm GetOneOffQuote(Prospect prospect)
        {
            var shoppingCartItemRepo = new ShoppingCartItemRepository();
            var shoppingCartItems = shoppingCartItemRepo.Read()
                .Where(x => x.ProspectToken == prospect.ProspectToken)
                .Where(x => x.Product.IsActive == true)
                .Where(x => x.Product.IsExclusive == false)
                .Where(x => x.Product.IsProspectsVisible == true)
                .Where(x => x.Product.Deleted == false)
                .ToList();
            if (!shoppingCartItems.Any())
                return null;
            
            var dryGoodQuoteVm = new DryGoodQuoteVm();

            foreach (var item in shoppingCartItems)
            {
                dryGoodQuoteVm.LineItems.Add(new DryGoodQuoteLineItemVm
                {
                    ProductName = item.Product.Name,
                    Unit = item.Product.Unit,
                    Price = item.Product.Price,
                    Quantity = item.Qty
                });
            }

            return dryGoodQuoteVm;
        }
    }
}
