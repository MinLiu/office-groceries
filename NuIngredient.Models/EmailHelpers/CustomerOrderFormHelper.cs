﻿using System;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using System.Web.Hosting;

namespace NuIngredient.Models
{
    public class CustomerOrderFormHelper
    {
        public HSSFWorkbook Workbook { get; set; }

        private IFont DefaultFont { get; set; }

        private int ItemCount { get; set; }

        public CustomerOrderFormHelper()
        {
            ItemCount = 0;
            NewCustomerOrderForm();
            SetLogo("og-logo-for-suppier.png");
            SetSupplierInfo("Telephone: 0345 463 8863");
            SetNumberOfPage("NUMBER OF PAGES (INCLUDING THIS PAGE): 1");
            SetIsAdditional("Is this in addition to your current order? No");
            SetDateDeliveryRequired("Date delivery required: ");
            SetTodaysDate("Todays Date: " + DateTime.Today.ToString("dd/MM/yyyy"));
            SetCustName("Customer Name: ");
            SetAcctNumber("Account Number: ");
            SetPONumber("PO Number (If applicable): ");
            SetCustInfo("");
            SetDefaultFont("Verdana", 12);
        }

        public void SetDefaultFont(string fontName = "Arial", short fontSize = 12)
        {
            DefaultFont.FontName = fontName;
            DefaultFont.FontHeightInPoints = fontSize;
            Workbook.GetSheetAt(0).GetRow(0).GetCell(0).CellStyle.SetFont(DefaultFont);
        }

        public void SetCellValue(string value, int rowIndex, int cellIndex, int sheetIndex = 0)
        {
            if (Workbook.GetSheetAt(sheetIndex).GetRow(rowIndex) == null)
            {
                var cell = Workbook.GetSheetAt(sheetIndex).CreateRow(rowIndex).CreateCell(cellIndex);
                cell.SetCellValue(value);
            }
            else
            {
                if (Workbook.GetSheetAt(sheetIndex).GetRow(rowIndex).GetCell(cellIndex) == null)
                {
                    var cell = Workbook.GetSheetAt(sheetIndex).GetRow(rowIndex).CreateCell(cellIndex);
                    cell.SetCellValue(cell.StringCellValue + value);
                }
                else
                {
                    var cell = Workbook.GetSheetAt(sheetIndex).GetRow(rowIndex).GetCell(cellIndex);
                    cell.SetCellValue(cell.StringCellValue + value);
                }
            }
        }
        public void SetLogo(string imageName)
        {
            string imagePath = HostingEnvironment.MapPath("/Content/" + imageName);
            SetCellValue("", 0, 0);
            if (!string.IsNullOrWhiteSpace(imagePath))
            {
                using (FileStream fs = new FileStream(imagePath, FileMode.Open))
                {
                    byte[] bytes = new byte[fs.Length];
                    fs.Read(bytes, 0, (int)fs.Length);
                    int pictureIndex;
                    switch (Path.GetExtension(imagePath).ToLower())
                    {
                        case ".bmp":
                            pictureIndex = Workbook.AddPicture(bytes, PictureType.BMP);
                            break;
                        case ".jpeg":
                            pictureIndex = Workbook.AddPicture(bytes, PictureType.JPEG);
                            break;
                        case ".png":
                            pictureIndex = Workbook.AddPicture(bytes, PictureType.PNG);
                            break;
                        default:
                            return;
                    }
                    HSSFSheet sheet = Workbook.GetSheetAt(0) as HSSFSheet;
                    HSSFPatriarch patriarch = sheet.CreateDrawingPatriarch() as HSSFPatriarch;
                    HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0, 0, 0, 2, 4);
                    anchor.AnchorType = AnchorType.MoveDontResize;
                    HSSFPicture pic = (HSSFPicture)patriarch.CreatePicture(anchor, pictureIndex);

                }
            }

        }
        public void SetSupplierInfo(string value)
        {
            SetCellValue(value, 4, 0);
        }

        public void SetNumberOfPage(string value)
        {
            SetCellValue(value, 5, 0);
        }

        public void SetIsAdditional(string value)
        {
            SetCellValue(value, 6, 0);
        }

        public void SetDateDeliveryRequired(string value)
        {
            SetCellValue(value, 7, 0);
        }

        public void SetTodaysDate(string value)
        {
            SetCellValue(value, 8, 0);
        }

        public void SetCustName(string value)
        {
            SetCellValue(value, 9, 0);
        }

        public void SetAcctNumber(string value)
        {
            SetCellValue(value, 10, 0);
        }

        public void SetPONumber(string value)
        {
            SetCellValue(value, 11, 0);
        }

        public void SetCustInfo(string value)
        {
            SetCellValue(value, 12, 0);
        }

        public void NewLineItem(string pdtCode, string pdtDesc, int mon, int tue, int wed, int thu, int fri, int sat, OrderMode mode)
        {
            int baseRow = 15;

            SetCellValue(pdtCode, baseRow + ItemCount, 0);
            SetCellValue(pdtDesc, baseRow + ItemCount, 1);
            SetCellValue(mon != 0 ? mon.ToString() : "", baseRow + ItemCount, 2);
            SetCellValue(tue != 0 ? tue.ToString() : "", baseRow + ItemCount, 3);
            SetCellValue(wed != 0 ? wed.ToString() : "", baseRow + ItemCount, 4);
            SetCellValue(thu != 0 ? thu.ToString() : "", baseRow + ItemCount, 5);
            SetCellValue(fri != 0 ? fri.ToString() : "", baseRow + ItemCount, 6);
            SetCellValue(sat != 0 ? sat.ToString() : "", baseRow + ItemCount, 7);
            SetCellValue(mode == OrderMode.OneOff ? "v" : "", baseRow + ItemCount, 8);
            SetCellValue(mode == OrderMode.Regular ? "v" : "", baseRow + ItemCount, 9);

            ItemCount++;
        }

        public void NewCustomerOrderForm()
        {
            Workbook = new HSSFWorkbook();
            DefaultFont = Workbook.CreateFont();
            var sheet = Workbook.CreateSheet();

            var styleBoldLarge = Workbook.CreateCellStyle();
            var fontBoldLarge = Workbook.CreateFont();
            fontBoldLarge.Boldweight = (short)FontBoldWeight.Bold;
            fontBoldLarge.FontHeightInPoints = 16;
            fontBoldLarge.FontName = "Verdana";
            styleBoldLarge.SetFont(fontBoldLarge);

            var styleRed = Workbook.CreateCellStyle();
            var fontRed = Workbook.CreateFont();
            fontRed.Color = (short)FontColor.Red;
            fontRed.Boldweight = (short)FontBoldWeight.Bold;
            fontRed.FontName = "Verdana";
            styleRed.SetFont(fontRed);

            // Image Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 3, 0, 9));

            // Supplier Info Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(4, 4, 0, 9));

            // Number of Page Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(5, 5, 0, 9));
            sheet.CreateRow(9).CreateCell(0).CellStyle = styleBoldLarge;

            // "Is this in addition to your current order" Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(6, 6, 0, 9));

            // "Date delivery required" Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(7, 7, 0, 9));

            // "Today's Date" Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(8, 8, 0, 9));

            // "Customer Name" Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(9, 9, 0, 9));

            // "Account Number" Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(10, 10, 0, 9));

            // "PO Number" Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(11, 11, 0, 9));

            // "Customer Info" Area
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(12, 12, 0, 9));

            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(13, 14, 0, 0));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(13, 14, 1, 1));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(13, 13, 2, 7));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(13, 13, 8, 9));

            // Product Code Area
            var cell_ProductCode = sheet.CreateRow(13).CreateCell(0);
            cell_ProductCode.SetCellValue("Product Code");
            cell_ProductCode.CellStyle = styleRed;

            // Product Desc Area
            var cell_ProductDescription = sheet.GetRow(13).CreateCell(1);
            cell_ProductDescription.SetCellValue("Product Description");
            cell_ProductDescription.CellStyle = styleRed;

            var cell_QtyDesc = sheet.GetRow(13).CreateCell(2);
            cell_QtyDesc.SetCellValue("Please enter quantity as the number of single units/pots/bottles required");
            cell_QtyDesc.CellStyle = styleRed;

            // Mon
            var cell_Mon = sheet.CreateRow(14).CreateCell(2);
            cell_Mon.SetCellValue("Mon");
            cell_Mon.CellStyle = styleRed;

            // Tue
            var cell_Tue = sheet.GetRow(14).CreateCell(3);
            cell_Tue.SetCellValue("Tue");
            cell_Tue.CellStyle = styleRed;

            // Wed
            var cell_Wed = sheet.GetRow(14).CreateCell(4);
            cell_Wed.SetCellValue("Wed");
            cell_Wed.CellStyle = styleRed;

            // Thu
            var cell_Thu = sheet.GetRow(14).CreateCell(5);
            cell_Thu.SetCellValue("Thu");
            cell_Thu.CellStyle = styleRed;

            // Fri
            var cell_Fri = sheet.GetRow(14).CreateCell(6);
            cell_Fri.SetCellValue("Fri");
            cell_Fri.CellStyle = styleRed;

            // Sat
            var cell_Sat = sheet.GetRow(14).CreateCell(7);
            cell_Sat.SetCellValue("Sat");
            cell_Sat.CellStyle = styleRed;

            var cell_ChangeType = sheet.GetRow(13).CreateCell(8);
            cell_ChangeType.SetCellValue("Change Type");

            // One Off
            var cell_OneOff = sheet.GetRow(14).CreateCell(8);
            cell_OneOff.SetCellValue("One-Off");
            cell_OneOff.CellStyle = styleRed;

            // Base Order
            var cell_BaseOrder = sheet.GetRow(14).CreateCell(9);
            cell_BaseOrder.SetCellValue("Standing Order");
            cell_BaseOrder.CellStyle = styleRed;

            sheet.SetColumnWidth(0, 4500);
            sheet.SetColumnWidth(1, 5500);
            sheet.SetColumnWidth(2, 3500);
            sheet.SetColumnWidth(3, 3500);
            sheet.SetColumnWidth(4, 3500);
            sheet.SetColumnWidth(5, 3500);
            sheet.SetColumnWidth(6, 3500);
            sheet.SetColumnWidth(7, 3500);
            sheet.SetColumnWidth(8, 3500);
            sheet.SetColumnWidth(9, 3500);
        }
    }
}