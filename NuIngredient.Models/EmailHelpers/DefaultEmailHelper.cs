﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public abstract class DefaultEmailHelper
    {
        protected static string _serverPath = ConfigurationManager.AppSettings["ServerPath"];
        protected const string _style_font_family = "font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;";
        protected const string _email_table_style = "max-width: 700px; border-top: 5px solid #898989; border-bottom: 5px solid #f57f20; font-size: 15px;";
        protected static string _customerSiteUrl = ConfigurationManager.AppSettings["BaseURL"];

        protected static string AppendLogo(List<Attachment> imgAttachments)
        {
            var logoBase64 = "";
            
            try
            {
                // read logo image to byte array
                var logoBytes = File.ReadAllBytes(_serverPath + "/Content/og-logo-order-banner.png");
                logoBase64 = Convert.ToBase64String(logoBytes);
                //
                // var logoImg = new Attachment(_serverPath + "/Content/og-logo-order-banner.png");
                // logoImg.ContentId = "og-logo";
                // logoImg.Name = "og-logo.png";
                // imgAttachments.Add(logoImg);
            }
            catch { }
            
            
            var logo = "<tr>" +
                            "<td></td>" +
                       "</tr>" +
                       "<tr>" +
                            "<td></td>" +
                       "</tr>" +
                       "<tr>" +
                           "<td>" +
                               $"<img src='data:image/jpeg;base64,{logoBase64}' style='height: 55px; width: 137px;'></img>" +
                           "</td>" +
                       "</tr>" +
                       "<tr>" +
                            "<td></td>" +
                       "</tr>" +
                       "<tr>" +
                            "<td></td>" +
                       "</tr>";

            return logo;
        }

        protected static string AppendFooter()
        {
            var footer = "<tr>" +
                            "<td style='" + _style_font_family + "'>" +
                                "<p>" +
                                    "<span style='color: #32b24a;'>Thank You From</span><br />" +
                                    "The Office-Groceries Team" +
                                "</p>" +
                                "<p>" +
                                    "0345 463 8863 <span style='color: #f57f20;'>|</span> <a href='mailto:Orders@office-groceries.com' style=' text-decoration: none;'>Orders@office-groceries.com</a> <br />" +
                                    "Office-Groceries, 43 Church Street, Great Burstead, CM11 2SX <br/>" +
                                    "<a href='https://www.office-groceries.com' style='color: #32b24a;'>www.office-groceries.com</a>" +
                                "</p>" +
                            "</td>" +
                        "</tr>";

            return footer;
        }

        protected static string AppendCompanyDetails(Company company)
        {
            var companyDetails = "<tr>" +
                                    "<td>" +
                                        "<table style='border: 1px solid white;'>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Account Number:</label></td><td>" + company.AccountNo + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Company:</label></td><td>" + company.Name + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Delivery Address:</label></td><td>" + company.Address1 + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Postcode:</label></td><td>" + company.Postcode + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Telephone:</label><td>" + company.Telephone + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Email:</label><td>" + company.Email + "</td>" +
                                            "</tr>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<td><label>Delivery Instructions:</label><td>" + company.DeliveryInstruction + "</td>" +
                                            "</tr>" +
                                        "</table>" +
                                    "</td>" +
                                "</tr>";
            return companyDetails;
        }

        protected static string GetMilkOrderTable(Company company, OrderMode orderMode, string date, List<Attachment> attachments)
        {
            DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;

            var milkOrderHeaderRepo = new MilkOrderHeaderRepository();
            var orderHeader = milkOrderHeaderRepo.Read()
                                           .Where(f => f.CompanyID == company.ID)
                                           .Where(f => f.OrderMode == orderMode)
                                           .Where(f => f.FromDate == fromDate)
                                           .Include(f => f.Items)
                                           .FirstOrDefault();

            if (orderHeader == null)
                return "";

            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th>Product</th>" +
                                            "<th>Unit</th>" +
                                            "<th>Cost</th>" +
                                            "<th>Mon</th>" +
                                            "<th>Tue</th>" +
                                            "<th>Wed</th>" +
                                            "<th>Thu</th>" +
                                            "<th>Fri</th>" +
                                            "<th>Sat</th>" +
                                            "<th>Weekly Volume</th>" +
                                            "<th>Weekly Total</th>" +
                                        "</tr>" +
                                    "</thead>";

            string tableBody = "";
            var monDelivery = false;
            var tueDelivery = false;
            var wedDelivery = false;
            var thuDelivery = false;
            var friDelivery = false;
            var satDelivery = false;
            foreach (var order in orderHeader.Items)
            {
                var str = String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                            "<td style='text-align: center;'>{7}</td>" +
                                            "<td style='text-align: center;'>{8}</td>" +
                                            "<td style='text-align: center;'>{9}</td>" +
                                            "<td style='text-align: center;'>{10}</td>" +
                                        "</tr>", order.ProductName
                                               , order.Unit
                                               , order.Price.ToString("C")
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Monday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Tuesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Wednesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Thursday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Friday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Saturday)
                                               , order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday
                                               , ((order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday) * order.Price).ToString("C"));
                tableBody += str;

                if (order.Monday > 0) monDelivery = true;
                if (order.Tuesday > 0) tueDelivery = true;
                if (order.Wednesday > 0) wedDelivery = true;
                if (order.Thursday > 0) thuDelivery = true;
                if (order.Friday > 0) friDelivery = true;
                if (order.Saturday > 0) satDelivery = true;
            }

            var weeklyTotal = orderHeader.TotalNet;
            var deliveryCharge = DeliveryChargeHelper.GetMilkDeliveryCharge(company);
            if (deliveryCharge != null)
            {
                var totalDrops = new List<bool>()
                    { monDelivery, tueDelivery, wedDelivery, thuDelivery, friDelivery, satDelivery }.Count(x => x);

                var totalDeliveryCharge = totalDrops * deliveryCharge.Charge;
                
                tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2:C}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                            "<td style='text-align: center;'>{7}</td>" +
                                            "<td style='text-align: center;'>{8}</td>" +
                                            "<td style='text-align: center;'>{9}</td>" +
                                            "<td style='text-align: center;'>{10:C}</td>" +
                                        "</tr>", deliveryCharge.Name
                                               , "Per Drop"
                                               , deliveryCharge.Charge
                                               , monDelivery ? "v" : ""
                                               , tueDelivery ? "v" : ""
                                               , wedDelivery ? "v" : ""
                                               , thuDelivery ? "v" : ""
                                               , friDelivery ? "v" : ""
                                               , satDelivery ? "v" : ""
                                               , totalDrops
                                               , totalDeliveryCharge);

                weeklyTotal += totalDeliveryCharge;
            }
            
            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;' colspan = '9'>Total</td>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'><strong>{1:C}</strong></td>" +
                                        "</tr>", orderHeader.Items.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday)
                                               , weeklyTotal);

            string tableFooter = "</table>";

            return tableHeader + tableBody + tableFooter;
        }

        protected static string GetFruitOrderTable(Company company, OrderMode orderMode, string date, List<Attachment> attachments)
        {
            DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;

            var fruitOrderHeaderRepo = new FruitOrderHeaderRepository();
            var OrderHeader = fruitOrderHeaderRepo.Read()
                                           .Where(f => f.CompanyID == company.ID)
                                           .Where(f => f.OrderMode == orderMode)
                                           .Where(f => f.FromDate == fromDate)
                                           .Include(f => f.Items)
                                           .FirstOrDefault();

            if (OrderHeader == null)
                return "";

            string tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                    "<thead>" +
                                        "<tr style='background-color: #c1c4be;'>" +
                                            "<th>Product</th>" +
                                            "<th>Unit</th>" +
                                            "<th>Cost</th>" +
                                            "<th>Mon</th>" +
                                            "<th>Tue</th>" +
                                            "<th>Wed</th>" +
                                            "<th>Thu</th>" +
                                            "<th>Fri</th>" +
                                            "<th>Sat</th>" +
                                            "<th>Weekly Volume</th>" +
                                            "<th>Net</th>" +
                                            "<th>VAT</th>" +
                                            "<th>Total</th>" +
                                        "</tr>" +
                                    "</thead>";

            string tableBody = "";
            var fruitPdtService = new FruitProductService(new SnapDbContext());
            foreach (var order in OrderHeader.Items)
            {
                var subtotalResult = fruitPdtService.CalculateSubtotal(order.FruitProductID, order.OrderHeader.CompanyID, order.Monday, order.Tuesday, order.Wednesday, order.Thursday, order.Friday, order.Saturday);
                var weeklyNet = subtotalResult.Subtotal;
                var unitPrice = subtotalResult.UnitPrice;
                var weeklyVat = weeklyNet * order.VATRate;
                var weeklyGross = weeklyNet + weeklyVat;
                var str = String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                            "<td style='text-align: center;'>{7}</td>" +
                                            "<td style='text-align: center;'>{8}</td>" +
                                            "<td style='text-align: center;'>{9}</td>" +
                                            "<td style='text-align: center;'>{10}</td>" +
                                            "<td style='text-align: center;'>{11}</td>" +
                                            "<td style='text-align: center;'>{12}</td>" +
                                        "</tr>", order.ProductName
                                               , order.Unit
                                               , unitPrice.ToString("C")
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Monday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Tuesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Wednesday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Thursday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Friday)
                                               , String.Format("{0:0.######;-0.######;\"\"}", order.Saturday)
                                               , order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday
                                               , weeklyNet.ToString("C")
                                               , weeklyVat.ToString("C")
                                               , weeklyGross.ToString("C"));
                tableBody += str;
            }

            tableBody += String.Format("<tr style='background-color: #c1c4be;'>" +
                                            "<td style='text-align: center;' colspan = '9'>Total</td>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'><strong>{1}</strong></td>" +
                                            "<td style='text-align: center;'><strong>{2}</strong></td>" +
                                            "<td style='text-align: center;'><strong>{3}</strong></td>" +
                                        "</tr>", OrderHeader.Items.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday)
                                               , OrderHeader.TotalNet.ToString("C")
                                               , OrderHeader.TotalVAT.ToString("C")
                                               , OrderHeader.TotalGross.ToString("C"));

            string tableFooter = "</table>";

            return tableHeader + tableBody + tableFooter;
        }
    }
}
