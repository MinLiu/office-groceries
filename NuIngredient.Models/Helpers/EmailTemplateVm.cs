﻿using System.Collections.Generic;
using System.Linq;

namespace NuIngredient.Models.EmailTemplateVms
{
    public class EmailTemplateVm
    {
        public string CompanyName { get; set; }
        public string Category { get; set; }
        public string CallBackUrl { get; set; }
        public string UnsubscribeUrl { get; set; }
        public WeeklyOrderVm MilkWeeklyQuote { get; set; }
        public WeeklyOrderVm FruitWeeklyQuote { get; set; }
        public DryGoodQuoteVm DryGoodQuote { get; set; }
        public bool HasMilkQuote => MilkWeeklyQuote != null;
        public bool HasFruitQuote => FruitWeeklyQuote != null;
        public bool HasDryGoodQuote => DryGoodQuote != null;
        public string LocalDepotTerm => HasMilkQuote ? "dairy" : "depot";
    }

    public class WeeklyOrderVm
    {
        public List<WeeklyOrderLineItemVm> LineItems { get; set; } = new List<WeeklyOrderLineItemVm>();
        public DeliveryChargeVm DeliveryCharges { get; set; }
        public bool MondayDrop => LineItems.Any(x => x.Mon > 0);
        public bool TuesdayDrop => LineItems.Any(x => x.Tue > 0);
        public bool WednesdayDrop => LineItems.Any(x => x.Wed > 0);
        public bool ThursdayDrop => LineItems.Any(x => x.Thu > 0);
        public bool FridayDrop => LineItems.Any(x => x.Fri > 0);
        public bool SaturdayDrop => LineItems.Any(x => x.Sat > 0);
        public bool HasDeliveryCharges => DeliveryCharges != null;
        public decimal NetTotal => LineItems.Sum(x => x.WeeklyPrice) + (HasDeliveryCharges ? DeliveryCharges.WeeklyPrice : 0);
        public decimal VATTotal => LineItems.Sum(x => x.WeeklyVAT);
        public decimal GrossTotal => LineItems.Sum(x => x.WeeklyGross) + (HasDeliveryCharges ? DeliveryCharges.WeeklyPrice : 0);
    }
    
    public class WeeklyOrderLineItemVm
    {
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public decimal UnitPrice { get; set; }
        public int Mon { get; set; }
        public int Tue { get; set; }
        public int Wed { get; set; }
        public int Thu { get; set; }
        public int Fri { get; set; }
        public int Sat { get; set; }
        public int WeeklyVolume => Mon + Tue + Wed + Thu + Fri + Sat;
        public decimal WeeklyPrice { get; set; }//=> WeeklyVolume* Price;
        public decimal WeeklyVAT { get; set; }
        public decimal WeeklyGross => WeeklyPrice + WeeklyVAT;
        public string ConvertToString(int qty) => qty > 0 ? qty.ToString() : "-";
    }

    public class DeliveryChargeVm
    {
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public decimal UnitPrice { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public int TotalDrop => new List<bool>(){ Mon, Tue, Wed, Thu, Fri, Sat }.Count(x => x);
        public decimal WeeklyPrice => TotalDrop * UnitPrice;
        public string ConvertToString(bool hasDelivery) => hasDelivery ? "v" : "-";
    }

    public class DryGoodQuoteVm
    {
        public decimal NetTotal => LineItems.Sum(x => x.TotalPrice);
        public List<DryGoodQuoteLineItemVm> LineItems { get; set; } = new List<DryGoodQuoteLineItemVm>();
    }

    public class DryGoodQuoteLineItemVm
    {
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice => Price * Quantity;
    }
}