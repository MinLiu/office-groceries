﻿using System;
using System.Linq;

namespace NuIngredient.Models
{
    public static class SupplierHelper
    {
        public static Supplier GetSupplier(Guid companyId, SupplierType type, DateTime? dateTime)
        {
            dateTime = dateTime ?? DateTime.Today;
            
            return new CompanySupplierRepository()
                .Read()
                .Where(x => x.CompanyID == companyId)
                .Where(x => x.Supplier.SupplierType == type)
                .Where(x => x.StartDate <= dateTime && (x.EndDate == null || x.EndDate >= dateTime))
                .Select(x => x.Supplier)
                .FirstOrDefault();
        }
    }
}