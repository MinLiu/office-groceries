﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using Fruitful.Email;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class RequestSnackAcctHelper : BaseRequestAcctHelper
    {
        public bool RequestSnackSupplier(Guid companyID, bool sendToCustomer = true, Guid? orderHeaderId = null)
        {
            var companyRepo = new CompanyRepository();
            var company = companyRepo.Find(companyID);
            
            var orderHeaderRepo = new FruitOrderHeaderRepository();

            var query = orderHeaderRepo.Read()
                .Where(x => x.CompanyID == company.ID)
                .Where(x => x.Depreciated == false);
            
            // If orderHeaderId is null, then we want to get the latest order header that is a draft or regular order
            query = orderHeaderId != null
                ? query.Where(x => x.ID == orderHeaderId)
                : query.Where(x => x.OrderMode == OrderMode.Draft || x.OrderMode == OrderMode.Regular);
            
            var fruitOrderHeader = query
                .Include(x => x.Items)
                .OrderByDescending(x => x.OrderMode == OrderMode.Regular)
                .FirstOrDefault();
            if (fruitOrderHeader == null || fruitOrderHeader.Items.All(x => x.Fruit.FruitProductType != FruitProductType.Snack))
                return false;

            string greeting = "Hello<br />" +
                              "<br />" +
                              "Please accept this e-mail as a formal request to open a Snack account from Office-Groceries for our client below.<br />" +
                              "<br />" +
                              "<strong>Could you please forward on account details along with a confirmation of delivery days to: <a href='mailto:Orders@office-groceries.com'>Orders@office-groceries.com</a></strong><br />" +
                              "<br />";

            string customerDetails = String.Format("<table>" +
                                                        "<tr><td><label>Company:</label></td><td>{0}</td></tr>" +
                                                        "<tr><td><label>Delivery Address:</label></td><td>{1}</td></tr>" +
                                                        "<tr><td><label>Postcode:</label></td><td>{2}</td></tr>" +
                                                        "<tr><td><label>Delivery Instructions:</label></td><td>{3}</td></tr>" +
                                                    "</table><br />", company.Name
                                                                    , company.Address1
                                                                    , company.Postcode
                                                                    , company.DeliveryInstruction);
            string title = "Regular Snack Order";

            string tableHeader = "<table border='1' style='width: 700px;'>" +
                                    "<thead>" +
                                        "<th>Product</th>" +
                                        "<th>Unit</th>" +
                                        "<th>Mon</th>" +
                                        "<th>Tue</th>" +
                                        "<th>Wed</th>" +
                                        "<th>Thu</th>" +
                                        "<th>Fri</th>" +
                                        "<th>Sat</th>" +
                                        "<th>Weekly Volume</th>" +
                                    "</thead>";

            string tableBody = "";
            foreach (var order in fruitOrderHeader.Items.Where(x => x.Fruit.FruitProductType == FruitProductType.Snack))
            {
                var str = String.Format("<tr>" +
                                            "<td style='text-align: center;'>{0}</td>" +
                                            "<td style='text-align: center;'>{1}</td>" +
                                            "<td style='text-align: center;'>{2}</td>" +
                                            "<td style='text-align: center;'>{3}</td>" +
                                            "<td style='text-align: center;'>{4}</td>" +
                                            "<td style='text-align: center;'>{5}</td>" +
                                            "<td style='text-align: center;'>{6}</td>" +
                                            "<td style='text-align: center;'>{7}</td>" +
                                            "<td style='text-align: center;'>{8}</td>" +
                                        "</tr>", order.ProductName
                                                , order.Unit
                                                , String.Format("{0:0.######;-0.######;\"\"}", order.Monday)
                                                , String.Format("{0:0.######;-0.######;\"\"}", order.Tuesday)
                                                , String.Format("{0:0.######;-0.######;\"\"}", order.Wednesday)
                                                , String.Format("{0:0.######;-0.######;\"\"}", order.Thursday)
                                                , String.Format("{0:0.######;-0.######;\"\"}", order.Friday)
                                                , String.Format("{0:0.######;-0.######;\"\"}", order.Saturday)
                                                , order.Monday + order.Tuesday + order.Wednesday + order.Thursday + order.Friday + order.Saturday);
                tableBody += str;
            }
            string tableFooter = "</table>";
            var body = greeting + customerDetails + title + tableHeader + tableBody + tableFooter;

            var toEmails = GetRequestSnackEmailAddrs(company.Postcode);

            var emailSetting = new SnapDbContext().EmailSettings.First();
            
            var success = false;
            foreach (var toEmail in toEmails.Split(';'))
            {
                var mailMessage = new MailMessage();
                mailMessage.To.Add(toEmail.Trim());
                mailMessage.From = new MailAddress(emailSetting.Email);
                mailMessage.Subject = "Request for an account from Office-Groceries";
                mailMessage.Body = "<div class=\"row\"> " +
                                   "<div class=\"col-sm-12\">" +
                                   "<br />" +
                                   body +
                                   "</div> " +
                                   "</div>";
                mailMessage.IsBodyHtml = true;
                success = new Emailer(new SnapDbContext()).SendEmail(mailMessage) || success;
            }

            if (success)
            {
                using (var db = new SnapDbContext())
                {
                    db.Companies.Where(x => x.ID == company.ID).First().AskingForSnackSupplier = DateTime.Now;
                    db.SaveChanges();

                    if (sendToCustomer && company.IsTransferred == false) // remove later
                    {
                        SendAccountSetUpEmail(company, "Snack");
                    }
                }
            }

            return success;
        }
        
        private string GetRequestSnackEmailAddrs(string deliveryPostcode)
        {
            var emails = GetRequestEmailAddrs(deliveryPostcode, SupplierType.Snacks);

            if (string.IsNullOrEmpty(emails))
            {
                var overallSetting = new SnapDbContext().Settings.First();
                emails = overallSetting.FruitSupplierCenterEmail;
            }

            return emails;
        }
    }
}