﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NuIngredient.Models
{
    public class FruitSnackSupplierChecker
    {
        public static void CheckAndInitialize(Guid fruitOrderHeaderId, Guid companyId)
        {
            var company = new CompanyRepository().Find(companyId);
            var fruitOrderItems = new FruitOrderRepository().Read()
                .Where(x => x.OrderHeaderID == fruitOrderHeaderId)
                .ToList();

            CheckFruitSupplier(company, fruitOrderItems, fruitOrderHeaderId);
            CheckSnackSupplier(company, fruitOrderItems, fruitOrderHeaderId);
        }

        private static void CheckFruitSupplier(Company company, IEnumerable<FruitOrder> items, Guid fruitOrderHeaderId)
        {
            if (items.All(x => x.Fruit.FruitProductType != FruitProductType.Fruit))
                return;
            
            if (company.Method_HasSupplier(SupplierType.Fruits) || company.AskingForFruitSupplier != null)
                return;
            
            var helper = new RequestFruitAcctHelper();
            helper.RequestFruitSupplier(company.ID, false, fruitOrderHeaderId);
        }

        private static void CheckSnackSupplier(Company company, IEnumerable<FruitOrder> items, Guid fruitOrderHeaderId)
        {
            if (items.All(x => x.Fruit.FruitProductType != FruitProductType.Snack))
                return;
            
            if (company.Method_HasSupplier(SupplierType.Snacks) || company.AskingForSnackSupplier != null)
                return;
            
            var helper = new RequestSnackAcctHelper();
            helper.RequestSnackSupplier(company.ID, false, fruitOrderHeaderId);
        }
    }
}