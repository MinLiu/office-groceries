﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace NuIngredient.Models
{
    public class SaveFruitOrderHelper
    {
        private SnapDbContext _dbContext {get;set;}
        private FruitOrderService<FruitOrderHeaderViewModel> _service { get; set; }
        private FruitOrderMapper _mapper { get; set; }
        private List<FruitOneOffChangeTracker> trackersToCreate { get; set; }
        private List<FruitOneOffChangeTracker> trackersToUpdate { get; set; }
        private Guid _companyID { get; set; }
        private string _prospectToken { get; set; }
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private SaveFruitOrderHelper()
        {
            _dbContext = new SnapDbContext();
            _service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), _dbContext);
            _mapper = new FruitOrderMapper();
            trackersToCreate = new List<FruitOneOffChangeTracker>();
            trackersToUpdate = new List<FruitOneOffChangeTracker>();
        }

        public static SaveFruitOrderHelper New()
        {
            return new SaveFruitOrderHelper();
        }

        public SaveFruitOrderHelper SetCompanyID(Guid companyID)
        {
            this._companyID = companyID;
            return this;
        }

        public SaveFruitOrderHelper SetProspectToken(string prospectToken)
        {
            this._prospectToken = prospectToken;
            return this;
        }

        public SaveFruitOrderResult SaveCustomOrder(OrderMode orderMode, DateTime? fromDate, List<FruitOrderViewModel> orderItems)
        {
            if (_companyID == null)
                throw new Exception("Must set companyID");

            var orderHeader = GetCustomOrder(orderMode, fromDate);

            if (orderMode == OrderMode.OneOff)
            {
                if (!CheckValidFruitChangeHelper.IsValidChange(orderItems, orderHeader, fromDate.Value))
                {
                    throw new Exception(string.Format("Changes to next day must be done before {0} pm today.", CutOffTime.TimeOfDayPm));
                }
                SetChangeTracker(orderItems, orderHeader);
            }

            var changedProductTypes = GetChangedProductTypes(orderItems, orderHeader);

            if (orderHeader != null && orderHeader.OrderMode == orderMode)
            {
                if (orderHeader.OrderMode == OrderMode.Regular)
                {
                    orderHeader.Depreciated = true;
                    _service.Update(orderHeader);
                    orderHeader = CreateNewOrder(orderMode, fromDate);
                }
                else
                {
                    DeleteAllOrderItems(orderHeader);
                    orderHeader.LatestUpdated = DateTime.Now;
                    _service.Update(orderHeader);
                }
            }
            else
            {
                orderHeader = CreateNewOrder(orderMode, fromDate);
            }

            AddOrderItems(orderHeader.ID, orderItems);

            try
            {
                if (orderMode == OrderMode.Regular)
                {
                    AdjustFutureOneOffOrders(orderItems);
                }
                else if (orderMode == OrderMode.OneOff)
                {
                    UpdateTracker(orderHeader.ID);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }

            orderHeader = _service.CalculateTotal(orderHeader.ID);

            return new SaveFruitOrderResult
            {
                OrderHeader = orderHeader,
                ChangedProductTypes = changedProductTypes
            };
        }

        private List<FruitProductType> GetChangedProductTypes(List<FruitOrderViewModel> newOrderItems, FruitOrderHeader oldOrder)
        {
            var changedProductTypes = new List<FruitProductType>();
            foreach (var newOrderLine in newOrderItems)
            {
                var oldItem = oldOrder?.Items?.FirstOrDefault(x => x.FruitProductID.ToString() == newOrderLine.FruitID);
                if (oldItem == null)
                {
                    var productType = new FruitProductRepository().Find(Guid.Parse(newOrderLine.FruitID)).FruitProductType;
                    changedProductTypes.Add(productType);
                }
                else if (oldItem.Monday != newOrderLine.Monday ||
                         oldItem.Tuesday != newOrderLine.Tuesday ||
                         oldItem.Wednesday != newOrderLine.Wednesday ||
                         oldItem.Thursday != newOrderLine.Thursday ||
                         oldItem.Friday != newOrderLine.Friday ||
                         oldItem.Saturday != newOrderLine.Saturday)
                {
                    var productType = new FruitProductRepository().Find(Guid.Parse(newOrderLine.FruitID)).FruitProductType;
                    changedProductTypes.Add(productType);
                }
            }

            foreach (var oldOrderItem in oldOrder?.Items ?? new List<FruitOrder>())
            {
                if(newOrderItems.Any(x => x.FruitID == oldOrderItem.FruitProductID.ToString()))
                    continue;
                
                var productType = new FruitProductRepository().Find(oldOrderItem.FruitProductID).FruitProductType;
                changedProductTypes.Add(productType);
            }

            return changedProductTypes.Distinct().ToList();
        }

        public bool SaveProspectOrder(List<FruitOrderViewModel> orderItems)
        {
            if (string.IsNullOrEmpty(_prospectToken))
                throw new Exception("Must set prospect token");

            var orderHeader = GetProspectOrder();
            if (orderItems.Any())
            {
                if (orderHeader != null)
                {
                    _service.DeleteAllItems(orderHeader);
                    orderHeader.LatestUpdated = DateTime.Now;
                    _service.Update(orderHeader);
                }
                else
                {
                    orderHeader = new FruitOrderHeader();
                    orderHeader.ProspectToken = _prospectToken;
                    orderHeader.OrderMode = OrderMode.Draft;
                    orderHeader.OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff"));
                    orderHeader.LatestUpdated = DateTime.Now;
                    _service.Create(orderHeader);
                }

                foreach (var item in orderItems)
                {
                    var product = new FruitProductRepository().Find(Guid.Parse(item.FruitID));
                    item.Price = product.Price;
                    item.ProductName = product.Name;
                    item.Unit = product.Unit;
                    var model = _mapper.MapToModel(item);
                    _service.AddItem(orderHeader.ID, model, null);
                }

                _service.CalculateTotal(orderHeader.ID);
            }
            else
            {
                _service.DeleteOrder(orderHeader);
            }

            return true;
        }

        private FruitOrderHeader GetCustomOrder(OrderMode orderMode, DateTime? fromDate)
        {
            return _service.GetCustomerOrder(_companyID, orderMode, fromDate);
        }

        private FruitOrderHeader GetProspectOrder()
        {
            return _service.GetProspectOrder(_prospectToken);
        }

        private void SetChangeTracker(List<FruitOrderViewModel> newOrders, FruitOrderHeader oldOrder)
        {
            var repo = new FruitOneOffChangeTrackerRepository(_dbContext);
            var trackers = repo.Read().Where(x => x.OrderHeaderID == oldOrder.ID).ToList();
            trackersToUpdate = trackers.ToList();
            foreach (var newOrderLine in newOrders)
            {
                var oldOrderLine = oldOrder
                    .Items
                    .Where(x => x.FruitProductID.ToString() == newOrderLine.FruitID)
                    .FirstOrDefault();

                if (oldOrderLine != null)
                {
                    var tracker = trackers.Where(x => x.FruitProductID == oldOrderLine.FruitProductID).FirstOrDefault();
                    if (tracker == null)
                    {
                        tracker = new FruitOneOffChangeTracker()
                        {
                            FruitProductID = oldOrderLine.FruitProductID,
                            //OrderHeaderID = oldOrder.ID,
                        };
                        trackersToCreate.Add(tracker);
                    }

                    if (oldOrderLine.Monday != newOrderLine.Monday) tracker.Monday = true;
                    if (oldOrderLine.Tuesday != newOrderLine.Tuesday) tracker.Tuesday = true;
                    if (oldOrderLine.Wednesday != newOrderLine.Wednesday) tracker.Wednesday = true;
                    if (oldOrderLine.Thursday != newOrderLine.Thursday) tracker.Thursday = true;
                    if (oldOrderLine.Friday != newOrderLine.Friday) tracker.Friday = true;
                    if (oldOrderLine.Saturday != newOrderLine.Saturday) tracker.Saturday = true;
                }
                else
                {
                    var tracker = trackers.Where(x => x.FruitProductID == Guid.Parse(newOrderLine.FruitID)).FirstOrDefault();
                    if (tracker == null)
                    {
                        tracker = new FruitOneOffChangeTracker()
                        {
                            FruitProductID = Guid.Parse(newOrderLine.FruitID),
                            //OrderHeaderID = oldOrder.ID,
                        };
                        trackersToCreate.Add(tracker);
                    }

                    if (newOrderLine.Monday != 0) tracker.Monday = true;
                    if (newOrderLine.Tuesday != 0) tracker.Tuesday = true;
                    if (newOrderLine.Wednesday != 0) tracker.Wednesday = true;
                    if (newOrderLine.Thursday != 0) tracker.Thursday = true;
                    if (newOrderLine.Friday != 0) tracker.Friday = true;
                    if (newOrderLine.Saturday != 0) tracker.Saturday = true;
                }
            }
        }

        private void AdjustFutureOneOffOrders(List<FruitOrderViewModel> latestItemViewModels)
        {
            var latestItems = latestItemViewModels
                .Select(x => _mapper.MapToModel(x))
                .ToList();

            var repo = new FruitOrderHeaderRepository(_dbContext);
            var fruitOrderItemRepo = new FruitOrderRepository(_dbContext);
            var fruitProductService = new FruitProductService(_dbContext);

            var futureOneOffOrders = repo
                .Read()
                .Where(x => x.CompanyID == _companyID)
                .Where(x => x.OrderMode == OrderMode.OneOff)
                .Where(x => x.ToDate != null && x.ToDate > DateTime.Today)
                .Include(x => x.Items)
                .AsNoTracking()
                .ToList();

            foreach (var futureOrder in futureOneOffOrders)
            {
                var supplier =
                    SupplierHelper.GetSupplier(_companyID, SupplierType.Fruits, futureOrder.FromDate.Value);
                
                var weekdayEnable = GetCanBeChanged(futureOrder.FromDate.Value, supplier?.ID);

                var origItems = futureOrder.Items.ToList();

                var toRemoveItems = origItems
                    .Where(x => !latestItems.Select(li => li.FruitProductID).Contains(x.FruitProductID))
                    .ToList();
                var toAddItems = latestItems
                    .Where(x => !origItems.Select(oi => oi.FruitProductID).Contains(x.FruitProductID))
                    .ToList();
                var toAdjustItems = origItems
                    .Where(x => latestItems.Select(li => li.FruitProductID).Contains(x.FruitProductID))
                    .ToList();

                foreach (var toAdjustItem in toAdjustItems)
                {
                    var latestItem = latestItems.Where(x => x.FruitProductID == toAdjustItem.FruitProductID).FirstOrDefault();
                    
                    var trackerRepo = new FruitOneOffChangeTrackerRepository();
                    var tracker = trackerRepo
                        .Read()
                        .Where(x => x.OrderHeaderID == futureOrder.ID)
                        .Where(x => x.FruitProductID == toAdjustItem.FruitProductID)
                        .FirstOrDefault() ?? new FruitOneOffChangeTracker()
                        {
                            Monday = false,
                            Tuesday = false,
                            Wednesday = false,
                            Thursday = false,
                            Friday = false,
                            Saturday = false,
                        };
                    
                    var subtotalResult = fruitProductService.CalculateSubtotal(toAdjustItem.FruitProductID, toAdjustItem.OrderHeader.CompanyID, toAdjustItem.Monday, toAdjustItem.Tuesday, toAdjustItem.Wednesday, toAdjustItem.Thursday, toAdjustItem.Friday, toAdjustItem.Saturday);
                    
                    toAdjustItem.Monday = weekdayEnable[DayOfWeek.Monday] && !tracker.Monday ? latestItem.Monday : toAdjustItem.Monday;
                    toAdjustItem.Tuesday = weekdayEnable[DayOfWeek.Tuesday] && !tracker.Tuesday ? latestItem.Tuesday : toAdjustItem.Tuesday;
                    toAdjustItem.Wednesday = weekdayEnable[DayOfWeek.Wednesday] && !tracker.Wednesday ? latestItem.Wednesday : toAdjustItem.Wednesday;
                    toAdjustItem.Thursday = weekdayEnable[DayOfWeek.Thursday] && !tracker.Thursday ? latestItem.Thursday : toAdjustItem.Thursday;
                    toAdjustItem.Friday = weekdayEnable[DayOfWeek.Friday] && !tracker.Friday ? latestItem.Friday : toAdjustItem.Friday;
                    toAdjustItem.Saturday = weekdayEnable[DayOfWeek.Saturday] && !tracker.Saturday ? latestItem.Saturday : toAdjustItem.Saturday;
                    toAdjustItem.TotalNet = subtotalResult.Subtotal;
                    toAdjustItem.TotalVAT = toAdjustItem.TotalNet * toAdjustItem.VATRate;
                    toAdjustItem.Price = subtotalResult.UnitPrice;

                    fruitOrderItemRepo.Update(toAdjustItem);
                }

                foreach (var toRemoveItem in toRemoveItems)
                {
                    if (weekdayEnable[DayOfWeek.Monday]
                        && weekdayEnable[DayOfWeek.Tuesday]
                        && weekdayEnable[DayOfWeek.Wednesday]
                        && weekdayEnable[DayOfWeek.Thursday]
                        && weekdayEnable[DayOfWeek.Friday]
                        && weekdayEnable[DayOfWeek.Saturday])
                    {
                        _service.DeleteItem(futureOrder, toRemoveItem.ID);
                    }
                    else
                    {
                        toRemoveItem.Monday = weekdayEnable[DayOfWeek.Monday] ? 0 : toRemoveItem.Monday;
                        toRemoveItem.Tuesday = weekdayEnable[DayOfWeek.Tuesday] ? 0 : toRemoveItem.Tuesday;
                        toRemoveItem.Wednesday = weekdayEnable[DayOfWeek.Wednesday] ? 0 : toRemoveItem.Wednesday;
                        toRemoveItem.Thursday = weekdayEnable[DayOfWeek.Thursday] ? 0 : toRemoveItem.Thursday;
                        toRemoveItem.Friday = weekdayEnable[DayOfWeek.Friday] ? 0 : toRemoveItem.Friday;
                        toRemoveItem.Saturday = weekdayEnable[DayOfWeek.Saturday] ? 0 : toRemoveItem.Saturday;

                        if (toRemoveItem.Monday == 0
                            && toRemoveItem.Tuesday == 0
                            && toRemoveItem.Wednesday == 0
                            && toRemoveItem.Thursday == 0
                            && toRemoveItem.Friday == 0
                            && toRemoveItem.Saturday == 0)
                        {
                            _service.DeleteItem(futureOrder, toRemoveItem.ID);
                        }
                        else
                        {
                            var subtotalResult = fruitProductService.CalculateSubtotal(toRemoveItem.FruitProductID, toRemoveItem.OrderHeader.CompanyID, toRemoveItem.Monday, toRemoveItem.Tuesday, toRemoveItem.Wednesday, toRemoveItem.Thursday, toRemoveItem.Friday, toRemoveItem.Saturday);
                            toRemoveItem.TotalNet = subtotalResult.Subtotal;
                            toRemoveItem.Price = subtotalResult.UnitPrice;
                            toRemoveItem.TotalVAT = toRemoveItem.TotalNet * toRemoveItem.VATRate;
                            fruitOrderItemRepo.Update(toRemoveItem);
                        }
                    }
                }

                foreach (var toAddItem in toAddItems)
                {
                    var newItem = (FruitOrder)toAddItem.Clone();
                    
                    var subtotalResult = fruitProductService.CalculateSubtotal(newItem.FruitProductID, _companyID, newItem.Monday, newItem.Tuesday, newItem.Wednesday, newItem.Thursday, newItem.Friday, newItem.Saturday);
                    
                    newItem.ID = Guid.NewGuid();
                    newItem.Monday = weekdayEnable[DayOfWeek.Monday] ? newItem.Monday : 0;
                    newItem.Tuesday = weekdayEnable[DayOfWeek.Tuesday] ? newItem.Tuesday : 0;
                    newItem.Wednesday = weekdayEnable[DayOfWeek.Wednesday] ? newItem.Wednesday : 0;
                    newItem.Thursday = weekdayEnable[DayOfWeek.Thursday] ? newItem.Thursday : 0;
                    newItem.Friday = weekdayEnable[DayOfWeek.Friday] ? newItem.Friday : 0;
                    newItem.Saturday = weekdayEnable[DayOfWeek.Saturday] ? newItem.Saturday : 0;
                    newItem.TotalNet = subtotalResult.Subtotal;
                    newItem.TotalVAT = newItem.TotalNet * newItem.VATRate;
                    newItem.Price = subtotalResult.UnitPrice;
                    _service.AddItem(futureOrder.ID, newItem, futureOrder.CompanyID);
                }

                _service.CalculateTotal(futureOrder.ID);
            }
        }

        private void UpdateTracker(Guid orderHeaderID)
        {
            var trackerRepo = new FruitOneOffChangeTrackerRepository(_dbContext);
            trackersToCreate.ForEach(x => x.OrderHeaderID = orderHeaderID);
            trackerRepo.Create(trackersToCreate);
            trackerRepo.Update(trackersToUpdate);
        }

        private void DeleteAllOrderItems(FruitOrderHeader orderHeader)
        {
            _service.DeleteAllItems(orderHeader);
        }

        private FruitOrderHeader CreateNewOrder(OrderMode orderMode, DateTime? fromDate)
        {
            var orderHeader = new FruitOrderHeader();

            orderHeader.CompanyID = _companyID;
            orderHeader.OrderMode = orderMode;
            orderHeader.OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff"));
            orderHeader.LatestUpdated = DateTime.Now;
            if (orderMode == OrderMode.OneOff)
            {
                orderHeader.FromDate = fromDate;
                orderHeader.ToDate = fromDate.Value.AddDays(6);
            }
            _service.Create(orderHeader);

            return orderHeader;
        }

        private void AddOrderItems(Guid orderHeaderID, List<FruitOrderViewModel> orderItems)
        {
            using (var db = new SnapDbContext())
            {
                var fruitPriceService = new FruitProductService(db);
                foreach (var item in orderItems)
                {
                    var product = new FruitProductRepository(db).Find(Guid.Parse(item.FruitID));
                    item.ProductName = product.Name;
                    item.Unit = product.Unit;
                    var model = _mapper.MapToModel(item);
                    //model.TotalNet = fruitPriceService.CalculateSubtotal(product, _companyID, model.Monday,
                    //    model.Tuesday, model.Wednesday, model.Thursday, model.Friday, model.Saturday);
                    //model.VATRate = product.VAT;
                    //model.TotalVAT = model.TotalVAT * model.VATRate;
                    _service.AddItem(orderHeaderID, model, _companyID);
                }
            }
        }

        private Dictionary<DayOfWeek, bool> GetCanBeChanged(DateTime fromDate, Guid? supplierID)
        {
            fromDate = fromDate.Date;
            var toDate = fromDate.AddDays(6);

            var holidays = new HolidayService(_dbContext).ReadSupplierHolidays(supplierID)
                .Where(x => fromDate <= x.Date)
                .Where(x => toDate >= x.Date)
                .Select(x => x.Date)
                .ToList();
            
            var openDays = new SupplierOpenDayRepository(_dbContext).ReadSupplierOpenDays(supplierID)
                .Where(x => fromDate <= x.Date)
                .Where(x => toDate >= x.Date)
                .Select(x => x.Date)
                .ToList();

            var results = new Dictionary<DayOfWeek, bool>();
            var cutOffDate = DateTime.Now.AddHours(CutOffTime.RestTime).Date;

            var dateMon = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Monday);
            var dateTue = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Tuesday);
            var dateWed = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Wednesday);
            var dateThu = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Thursday);
            var dateFri = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Friday);
            var dateSat = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Saturday);

            results.Add(DayOfWeek.Monday, cutOffDate < dateMon && (NotHoliday(dateMon) || IsOpenDay(dateMon)));
            results.Add(DayOfWeek.Tuesday, cutOffDate < dateTue && (NotHoliday(dateTue) || IsOpenDay(dateTue)));
            results.Add(DayOfWeek.Wednesday, cutOffDate < dateWed && (NotHoliday(dateWed) || IsOpenDay(dateWed)));
            results.Add(DayOfWeek.Thursday, cutOffDate < dateThu && (NotHoliday(dateThu) || IsOpenDay(dateThu)));
            results.Add(DayOfWeek.Friday, cutOffDate < dateFri && (NotHoliday(dateFri) || IsOpenDay(dateFri)));
            results.Add(DayOfWeek.Saturday, cutOffDate < dateSat && (NotHoliday(dateSat) || IsOpenDay(dateSat)));

            return results;

            bool NotHoliday(DateTime date)
            {
                return !holidays.Contains(date);
            }
            
            bool IsOpenDay(DateTime date)
            {
                return openDays.Contains(date);
            }
        }
    }
}
