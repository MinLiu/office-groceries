﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using NLog;

namespace NuIngredient.Models
{
    public class FruitOrderRescheduleHelper
    {
        private readonly Guid _supplierId;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly FruitProductType _productType;
        
        public FruitOrderRescheduleHelper(Guid supplierId)
        {
            _supplierId = supplierId;
            var supplierType = new SupplierRepository().Read().FirstOrDefault(s => s.ID == supplierId)?.SupplierType ?? SupplierType.Fruits;
            _productType = supplierType == SupplierType.Fruits
                ? FruitProductType.Fruit
                : FruitProductType.Snack;
            
        }
        
        public async Task RescheduleOrderAsync(DateTime oldDate, DateTime newDate, OrderReschedulePolicy policy = OrderReschedulePolicy.ReplaceExisting)
        {
            if (oldDate == newDate)
                return;

            var companyIds = await GetCompanies().ToListAsync();

            Logger.Info($"Rescheduling fruit orders - Supplier: {_supplierId} - From: {oldDate:d} - To: {newDate:d}  - Total companies: {companyIds.Count}");
            var count = 0;
            foreach (var companyId in companyIds)
            {
                var sw = Stopwatch.StartNew();
                RescheduleCompanyOrder(companyId, oldDate, newDate, policy);
                sw.Stop();
                Logger.Info($"{++count}: {sw.ElapsedMilliseconds}ms - {companyId}");
            }
            Logger.Info("Reschedule completed");

        }
        
        private IQueryable<Guid> GetCompanies()
        {
            var repo = new CompanySupplierRepository();
            return repo.Read()
                .Where(cs => cs.SupplierID == _supplierId)
                .Where(cs => cs.EndDate == null || cs.EndDate >= DateTime.Now)
                .Select(cs => cs.CompanyID);
        }
        
        private void RescheduleCompanyOrder(Guid companyId, DateTime oldDate, DateTime newDate, OrderReschedulePolicy policy)
        {
            if (oldDate == newDate)
                return;
            
            var fromOrderItems = GetFromOrder(companyId, oldDate);
            
            if (fromOrderItems.Count == 0)
                return;

            if (oldDate.StartOfWeek(DayOfWeek.Monday) == newDate.StartOfWeek(DayOfWeek.Monday))
            {
                RescheduleAndRemoveOrderItems(companyId, oldDate, newDate, fromOrderItems, policy);
            }
            else
            {
                RescheduleWeeklyOrderItems(companyId, oldDate, newDate, fromOrderItems, policy);
                RemoveAndSaveUpdatedOrderItems(companyId, oldDate);
            }
        }
        
        private List<FruitOrder> GetFromOrder(Guid companyId, DateTime fromDate)
        {
            using (var db = new SnapDbContext())
            {
                var orderService = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), db);
                var standingOrder = orderService.GetCustomerOrder(companyId, OrderMode.OneOff, fromDate.StartOfWeek(DayOfWeek.Monday), false, true);
                return standingOrder.Items.ToList();
            }
        }

        private List<FruitOrder> GetWeeklyOrder(Guid companyId, DateTime startDate, SnapDbContext db)
        {
            var orderService = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), db);
            var orderHeader = orderService.GetCustomerOrder(companyId, OrderMode.OneOff, startDate.StartOfWeek(DayOfWeek.Monday));
            return orderHeader.Items.ToList();
        }

        private void RescheduleAndRemoveOrderItems(Guid companyId, DateTime oldDate, DateTime newDate, List<FruitOrder> fromOrderItems, OrderReschedulePolicy policy)
        {
            using (var db = new SnapDbContext())
            {
                try
                {
                    var weeklyOrderItems = GetWeeklyOrder(companyId, newDate, db);
                    
                    // Update target weekly order
                    RescheduleOrderItems(fromOrderItems, weeklyOrderItems, oldDate, newDate, policy);
                    
                    // Update original weekly order by removing the old date
                    RemoveOrderItems(weeklyOrderItems, oldDate);
                    
                    SaveOrder(companyId, newDate, weeklyOrderItems);
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Failed to reschedule order for company {companyId}");
                }
            }
        }
        
        private void RescheduleWeeklyOrderItems(Guid companyId, DateTime oldDate, DateTime newDate, List<FruitOrder> fromOrderItems, OrderReschedulePolicy policy)
        {
            using (var db = new SnapDbContext())
            {
                try
                {
                    var weeklyOrderItems = GetWeeklyOrder(companyId, newDate, db);
                    
                    // Update target weekly order
                    RescheduleOrderItems(fromOrderItems, weeklyOrderItems, oldDate, newDate, policy);
                    
                    SaveOrder(companyId, newDate, weeklyOrderItems);
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Failed to reschedule order for company {companyId}");
                }
            }
        }
        
        private void RemoveAndSaveUpdatedOrderItems(Guid companyId, DateTime oldDate)
        {
            using (var db = new SnapDbContext())
            {
                try
                {
                    // Update original weekly order by removing the old date
                    var weeklyOrderItems = GetWeeklyOrder(companyId, oldDate, db);
                    
                    RemoveOrderItems(weeklyOrderItems, oldDate);
                    
                    SaveOrder(companyId, oldDate, weeklyOrderItems);
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Failed to reschedule order for company {companyId}");
                }
            }
        }

        private void RescheduleOrderItems(List<FruitOrder> standingOrderItems, List<FruitOrder> weeklyOrderItems, DateTime oldDate, DateTime newDate, OrderReschedulePolicy policy)
        {
            var oldDateWeekDay = oldDate.DayOfWeek;
            var newDateWeekDay = newDate.DayOfWeek;
            
            foreach (var item in weeklyOrderItems.Where(x => x.Fruit.FruitProductType == _productType))
            {
                var standingQty = standingOrderItems.FirstOrDefault(x => x.FruitProductID == item.FruitProductID)?.GetAmountByDayOfWeek(oldDateWeekDay) ?? 0;

                switch (newDateWeekDay)
                {
                    case DayOfWeek.Monday:
                        item.Monday = policy == OrderReschedulePolicy.ReplaceExisting ? standingQty : item.Monday + standingQty;
                        break;
                    case DayOfWeek.Tuesday:
                        item.Tuesday = policy == OrderReschedulePolicy.ReplaceExisting ? standingQty : item.Tuesday + standingQty;
                        break;
                    case DayOfWeek.Wednesday:
                        item.Wednesday = policy == OrderReschedulePolicy.ReplaceExisting ? standingQty : item.Wednesday + standingQty;
                        break;
                    case DayOfWeek.Thursday:
                        item.Thursday = policy == OrderReschedulePolicy.ReplaceExisting ? standingQty : item.Thursday + standingQty;
                        break;
                    case DayOfWeek.Friday:
                        item.Friday = policy == OrderReschedulePolicy.ReplaceExisting ? standingQty : item.Friday + standingQty;
                        break;
                    case DayOfWeek.Saturday:
                        item.Saturday = policy == OrderReschedulePolicy.ReplaceExisting ? standingQty : item.Saturday + standingQty;
                        break;
                }
            }
        }
        
        private void RemoveOrderItems(List<FruitOrder> weeklyOrderItems, DateTime oldDate)
        {
            var oldDateWeekDay = oldDate.DayOfWeek;
            
            foreach (var item in weeklyOrderItems.Where(x => x.Fruit.FruitProductType == _productType))
            {
                switch (oldDateWeekDay)
                {
                    case DayOfWeek.Monday:
                        item.Monday = 0;
                        break;
                    case DayOfWeek.Tuesday:
                        item.Tuesday = 0;
                        break;
                    case DayOfWeek.Wednesday:
                        item.Wednesday = 0;
                        break;
                    case DayOfWeek.Thursday:
                        item.Thursday = 0;
                        break;
                    case DayOfWeek.Friday:
                        item.Friday = 0;
                        break;
                    case DayOfWeek.Saturday:
                        item.Saturday = 0;
                        break;
                }
            }
        }
        
        private void SaveOrder(Guid companyId, DateTime startDate, List<FruitOrder> orderItems)
        {
            SaveFruitOrderHelper.New()
                .SetCompanyID(companyId)
                .SaveCustomOrder(
                    OrderMode.OneOff,
                    startDate.StartOfWeek(DayOfWeek.Monday),
                    orderItems.Select(f => new FruitOrderMapper().MapToViewModel(f)).ToList());
        }
        
    }
}