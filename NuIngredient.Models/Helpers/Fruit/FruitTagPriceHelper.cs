using System;
using System.Linq;

namespace NuIngredient.Models
{
    public class FruitTagPriceHelper
    {
        public static bool TryGetTagPrice(Guid companyId, Guid fruitProductId, out decimal price)
        {
            price = 0;

            var companyTagIds = new CompanyTagItemRepository().Read()
                .Where(x => x.CompanyID == companyId)
                .Select(x => x.TagID)
                .ToList();

            if (!companyTagIds.Any())
                return false;
            
            var fruitTagPriceRepo = new FruitProductTagPriceRepository();
            var fruitTagPrice = fruitTagPriceRepo.Read()
                .Where(x =>
                    x.ProductID == fruitProductId &&
                    companyTagIds.Contains(x.TagID))
                .Select(x => x.Price)
                .OrderBy(p => p)
                .FirstOrDefault();
            
            if (fruitTagPrice == 0)
                return false;
            
            price = fruitTagPrice;
            return true;
        }
    }
}