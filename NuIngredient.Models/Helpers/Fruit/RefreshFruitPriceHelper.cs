﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class RefreshFruitPriceHelper
    {
        private SnapDbContext _db;
        private FruitProduct _fruit { get; set; }
        private FruitOrderRepository _fruitOrderRepo {get;set;}
        private FruitOrderHeaderRepository _fruitOrderHeaderRepo {get;set; }

        private RefreshFruitPriceHelper(SnapDbContext db)
        {
            _db = db;
            _fruitOrderRepo = new FruitOrderRepository(db);
            _fruitOrderHeaderRepo = new FruitOrderHeaderRepository(db);
        }

        public static RefreshFruitPriceHelper New(SnapDbContext db)
        {
            return new RefreshFruitPriceHelper(db);
        }

        public static RefreshFruitPriceHelper New()
        {
            return new RefreshFruitPriceHelper(new SnapDbContext());
        }

        public void RefreshOrderPrice(Guid fruitPdtId, Guid? companyId = null)
        {
            _fruit = _db.FruitProducts.Find(fruitPdtId);

            var affectedOrders = GetAffectedOrders(companyId).ToList();

            foreach(var affectedOrder in affectedOrders)
            { 
                foreach(var orderItem in affectedOrder.Items)
                {
                    if (orderItem.FruitProductID == _fruit.ID)
                    {
                        UpdateItemPrice(orderItem, affectedOrder.CompanyID);
                    }
                }

                UpdateOrderHeaderPrice(affectedOrder);
            }

        }

        private IEnumerable<FruitOrderHeader> GetAffectedOrders(Guid? companyId)
        {
            var orders = _fruitOrderHeaderRepo
                .Read()
                .Where(h => companyId == null || h.CompanyID == companyId.Value)
                .Where(h => h.OrderMode == OrderMode.Draft
                         || h.OrderMode == OrderMode.Regular
                         || h.OrderMode == OrderMode.OneOff && h.ToDate >= DateTime.Now)
                .Where(h => h.Depreciated == false)
                .Where(h => h.Items.Any(i => i.FruitProductID == _fruit.ID));

            return orders;
        }

        private void UpdateItemPrice(FruitOrder orderItem, Guid? companyID)
        {
            orderItem.VATRate = _fruit.VAT;
            orderItem.TotalNet = _fruit.GetPrice(orderItem.Monday, companyID)
                + _fruit.GetPrice(orderItem.Tuesday, companyID)
                + _fruit.GetPrice(orderItem.Wednesday, companyID)
                + _fruit.GetPrice(orderItem.Thursday, companyID)
                + _fruit.GetPrice(orderItem.Friday, companyID)
                + _fruit.GetPrice(orderItem.Saturday, companyID);
            orderItem.TotalVAT = orderItem.TotalNet * orderItem.VATRate;

            orderItem.Price = orderItem.WeeklyVolume > 0
                ? Math.Round(orderItem.TotalNet / orderItem.WeeklyVolume, 2, MidpointRounding.AwayFromZero)
                : 0m;

            _fruitOrderRepo.Update(orderItem);
        }

        private void UpdateOrderHeaderPrice(FruitOrderHeader orderHeader)
        {
            orderHeader.TotalNet = 0m;
            orderHeader.TotalVAT = 0m;
            foreach (var item in orderHeader.Items)
            {
                orderHeader.TotalNet += item.TotalNet;
                orderHeader.TotalVAT += item.TotalVAT;
            }
            _fruitOrderHeaderRepo.Update(orderHeader);
        }
    }
}
