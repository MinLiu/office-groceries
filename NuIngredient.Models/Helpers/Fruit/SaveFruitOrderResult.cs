﻿using System.Collections.Generic;

namespace NuIngredient.Models
{
    public class SaveFruitOrderResult
    {
        public FruitOrderHeader OrderHeader { get; set; }
        public List<FruitProductType> ChangedProductTypes { get; set; }
    }
}