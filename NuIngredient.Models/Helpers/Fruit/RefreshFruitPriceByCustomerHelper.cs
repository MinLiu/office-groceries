﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class RefreshFruitPriceByCustomerHelper
    {
        private SnapDbContext _db;
        private FruitOrderRepository _fruitOrderRepo {get;set;}
        private FruitOrderHeaderRepository _fruitOrderHeaderRepo {get;set; }

        private RefreshFruitPriceByCustomerHelper(SnapDbContext db)
        {
            _db = db;
            _fruitOrderRepo = new FruitOrderRepository(db);
            _fruitOrderHeaderRepo = new FruitOrderHeaderRepository(db);
        }

        public static RefreshFruitPriceByCustomerHelper New(SnapDbContext db)
        {
            return new RefreshFruitPriceByCustomerHelper(db);
        }

        public static RefreshFruitPriceByCustomerHelper New()
        {
            return new RefreshFruitPriceByCustomerHelper(new SnapDbContext());
        }

        public void RefreshOrderPrice(Guid companyId)
        {
            var affectedOrders = GetAffectedOrders(companyId).ToList();

            foreach(var affectedOrder in affectedOrders)
            { 
                foreach(var orderItem in affectedOrder.Items)
                {
                    UpdateItemPrice(orderItem, affectedOrder.CompanyID);
                }

                UpdateOrderHeaderPrice(affectedOrder);
            }

        }

        private IEnumerable<FruitOrderHeader> GetAffectedOrders(Guid? companyId)
        {
            var orders = _fruitOrderHeaderRepo
                .Read()
                .Where(h => companyId == null || h.CompanyID == companyId.Value)
                .Where(h => h.OrderMode == OrderMode.Draft
                         || h.OrderMode == OrderMode.Regular
                         || h.OrderMode == OrderMode.OneOff && h.ToDate >= DateTime.Now)
                .Where(h => h.Depreciated == false);

            return orders;
        }

        private void UpdateItemPrice(FruitOrder orderItem, Guid? companyID)
        {
            var fruit = new SnapDbContext().FruitProducts.Find(orderItem.FruitProductID);
            
            orderItem.VATRate = fruit.VAT;
            orderItem.TotalNet = fruit.GetPrice(orderItem.Monday, companyID)
                                 + fruit.GetPrice(orderItem.Tuesday, companyID)
                                 + fruit.GetPrice(orderItem.Wednesday, companyID)
                                 + fruit.GetPrice(orderItem.Thursday, companyID)
                                 + fruit.GetPrice(orderItem.Friday, companyID)
                                 + fruit.GetPrice(orderItem.Saturday, companyID);
            orderItem.TotalVAT = orderItem.TotalNet * orderItem.VATRate;

            orderItem.Price = orderItem.WeeklyVolume > 0
                ? Math.Round(orderItem.TotalNet / orderItem.WeeklyVolume, 2, MidpointRounding.AwayFromZero)
                : 0m;

            _fruitOrderRepo.Update(orderItem);
        }

        private void UpdateOrderHeaderPrice(FruitOrderHeader orderHeader)
        {
            orderHeader.TotalNet = 0m;
            orderHeader.TotalVAT = 0m;
            foreach (var item in orderHeader.Items)
            {
                orderHeader.TotalNet += item.TotalNet;
                orderHeader.TotalVAT += item.TotalVAT;
            }
            _fruitOrderHeaderRepo.Update(orderHeader);
        }
    }
}
