﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    
    public abstract class EntityMapper <E, M>
                        where E : new()
                        where M : new()
    {
        public abstract M MapToViewModel(E entity);
        public abstract E MapToModel(M model);
    }
}
