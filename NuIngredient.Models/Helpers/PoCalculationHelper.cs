﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class PoCalculationHelper
    {
        private DbContext _dbContext;
        
        public PoCalculationHelper(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<WeeklyAverageReport> Calculate(Guid companyId, DateTime startDate, DateTime endDate)
        {
            var averageMilk = await CalculateMilkAverageAsync(companyId);
            var averageFruit = await CalculateFruitAverageAsync(companyId);

            var totalDays = (endDate - startDate).TotalDays;
            var totalWeeks = (int)Math.Round(totalDays / 7, 0, MidpointRounding.AwayFromZero);

            var result = new WeeklyAverageReport
            {
                Weeks = totalWeeks,
                WeeklyTotal = (averageMilk.Value + averageFruit.Value),
                MilkWeeklyAverage = averageMilk,
                FruitWeeklyAverage = averageFruit,
                CompanyID = companyId,
                FromDate = startDate,
                ToDate = endDate,
            };

            return result;
        }

        private async Task<WeeklyAverageResult> CalculateMilkAverageAsync(Guid companyId)
        {
            var result = new WeeklyAverageResult();
            
            var standingOrder = await _dbContext.Set<MilkOrderHeader>()
                .Where(x => x.CompanyID == companyId)
                .Where(x => x.OrderMode == OrderMode.Regular)
                .OrderByDescending(x => x.LatestUpdated)
                .FirstOrDefaultAsync();

            if (standingOrder == null)
                return result;

            result.Value = standingOrder.TotalGross;
            result.LineItems = standingOrder.Items.Where(x => x.TotalNet != 0)
                .Select(x => new LineItemViewModel
                {
                    Name = x.ProductName,
                    Mon = x.Monday,
                    Tue = x.Tuesday,
                    Wed = x.Wednesday,
                    Thu = x.Thursday,
                    Fri = x.Friday,
                    Sat = x.Saturday,
                    WeeklyVolume = x.WeeklyVolume,
                    Unit = x.Unit,
                    UnitCost = x.Price,
                    Total = x.TotalNet + x.TotalVAT
                }).ToList();

            AddDeliveryCharge(standingOrder.Company, result);
            
            return result;
        }
        
        private void AddDeliveryCharge(Company company, WeeklyAverageResult result)
        {
            var deliveryCharge = DeliveryChargeHelper.GetMilkDeliveryCharge(company);
            
            if (deliveryCharge == null) return;
            
            // Create a line item for delivery charge
            var deliveryChargeLineItem = new LineItemViewModel
            {
                Name = deliveryCharge.Name,
                Mon = result.LineItems.Any(x => x.Mon != 0) ? 1 : 0,
                Tue = result.LineItems.Any(x => x.Tue != 0) ? 1 : 0,
                Wed = result.LineItems.Any(x => x.Wed != 0) ? 1 : 0,
                Thu = result.LineItems.Any(x => x.Thu != 0) ? 1 : 0,
                Fri = result.LineItems.Any(x => x.Fri != 0) ? 1 : 0,
                Sat = result.LineItems.Any(x => x.Sat != 0) ? 1 : 0,
                Unit = "Per Drop",
                UnitCost = deliveryCharge.Charge,
            };

            deliveryChargeLineItem.WeeklyVolume =
                deliveryChargeLineItem.Mon + deliveryChargeLineItem.Tue + deliveryChargeLineItem.Wed +
                deliveryChargeLineItem.Thu + deliveryChargeLineItem.Fri + deliveryChargeLineItem.Sat;

            deliveryChargeLineItem.Total = deliveryChargeLineItem.UnitCost * deliveryChargeLineItem.WeeklyVolume;

            // Add delivery charge to the line items
            result.LineItems.Add(deliveryChargeLineItem);

            // Add delivery charge to the total
            result.Value += deliveryChargeLineItem.Total;
        }
        
        private async Task<WeeklyAverageResult> CalculateFruitAverageAsync(Guid companyId)
        {
            var result = new WeeklyAverageResult();
            
            var standingOrder = await _dbContext.Set<FruitOrderHeader>()
                .Where(x => x.CompanyID == companyId)
                .Where(x => x.OrderMode == OrderMode.Regular)
                .OrderByDescending(x => x.LatestUpdated)
                .FirstOrDefaultAsync();

            if (standingOrder == null)
                return result;

            result.Value = standingOrder.TotalGross;
            result.LineItems = standingOrder.Items.Where(x => x.TotalNet != 0)
                .Select(x => new LineItemViewModel
                {
                    Name = x.ProductName,
                    Mon = x.Monday,
                    Tue = x.Tuesday,
                    Wed = x.Wednesday,
                    Thu = x.Thursday,
                    Fri = x.Friday,
                    Sat = x.Saturday,
                    WeeklyVolume = x.WeeklyVolume,
                    Unit = x.Unit,
                    UnitCost = x.Price,
                    Total = x.TotalNet + x.TotalVAT
                }).ToList();

            return result;
        }

    }

    public class WeeklyAverageResult
    {
        public decimal Value { get; set; }
        public List<LineItemViewModel> LineItems { get; set; } = new List<LineItemViewModel>();
    }

    public class LineItemViewModel
    {
        public string Name { get; set; }
        public int Mon { get; set; }
        public int Tue { get; set; }
        public int Wed { get; set; }
        public int Thu { get; set; }
        public int Fri { get; set; }
        public int Sat { get; set; }
        public int WeeklyVolume { get; set; }
        public string Unit { get; set; }
        public decimal UnitCost { get; set; }
        public decimal Total { get; set; }
    }
    
    public class WeeklyAverageReport
    {
        public Guid CompanyID { get; set; }
        public Company Company { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Weeks { get; set; }
        public decimal WeeklyTotal { get; set; }
        public decimal Total => Weeks * WeeklyTotal;
        public WeeklyAverageResult MilkWeeklyAverage { get; set; }
        public WeeklyAverageResult FruitWeeklyAverage { get; set; }
        public List<string> ToEmailList { get; set; }
        public string OtherEmails { get; set; }
    }
}