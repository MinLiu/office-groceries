﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace NuIngredient.Models
{
    public class ExportMultiSitesCompanyHelper
    {
        private ExcelPackage _package { get; set; }
        private CompanyRepository _repo { get; set; }
        private static List<string> _headerList = new List<string>()
        {
            "Company Name", "Company Address", "Postcode", "Quote YES", "Quote No", "Chase Up", "https://www.192.com/businesses/search/",
        };

        private ExportMultiSitesCompanyHelper()
        {
            _repo = new CompanyRepository();
        }

        public static ExportMultiSitesCompanyHelper New()
        {
            return new ExportMultiSitesCompanyHelper();
        }

        public ExportMultiSitesCompanyHelper BuildSpreadSheet()
        {
            CreateWorksheet();
            SetHeaders();
            SetData();
            return this;
        }

        public byte[] Export()
        {
            var output = new System.IO.MemoryStream();
            _package.SaveAs(output);
            return output.ToArray();
        }

        private void CreateWorksheet()
        {
            _package = new ExcelPackage();
            _package.Workbook.Worksheets.Add("Customers");
        }

        private void SetHeaders()
        {
            var worksheet = _package.Workbook.Worksheets.First();
            var row = 1;
            var column = 1;
            foreach(var headerStr in _headerList)
            {
                worksheet.Cells[row, column].Value = headerStr;
                if (Uri.IsWellFormedUriString(headerStr, UriKind.Absolute))
                {
                    worksheet.Cells[row, column].Hyperlink = new Uri(headerStr);
                }
                column++;
            }
        }
    
        private void SetData()
        {
            var companies = GetCompanies();
            SetData(companies);
        }

        private List<Company> GetCompanies()
        {
            var companies = _repo
                .Read()
                .Where(c => c.HasMultiSites)
                .ToList()
                .OrderByDescending(c => c.AccountNo)
                .ToList();
            return companies;
        }

        private void SetData(List<Company> companies)
        {
            var worksheet = _package.Workbook.Worksheets.First();
            var row = 2;
            foreach (var company in companies)
            {
                Set(worksheet, row, "Company Name", company.Name);
                Set(worksheet, row, "Company Address", company.Address1);
                Set(worksheet, row, "Postcode", (company.Postcode ?? "").Trim().ToUpper());
                row++;
            }

            worksheet.Cells.AutoFitColumns(0);
        }

        private void Set(ExcelWorksheet worksheet, int row, string key, string value)
        {
            var index = _headerList.IndexOf(key);
            if (index == -1) throw new Exception("header index not found: " + key);
            worksheet.Cells[row, ++index].Value = value;
        }

        //private void SetExported(List<Company> companies)
        //{
        //    var toUpdate = companies
        //        .Where(c => c.Exported == false)
        //        .ToList();
        //    toUpdate.ForEach(c => c.Exported = true);
        //    _repo.Update(toUpdate, new string[] { "Exported" });
        //}
    }

}
