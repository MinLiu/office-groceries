﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace NuIngredient.Models
{
    public class DeliveryChargeHelper
    {
        public static DeliveryCharge GetMilkDeliveryCharge(Company company)
        {
            if (company.NoDeliveryCharge)
                return null;
            
            return GetChargeBySupplier(company) ?? GetChargeByPostcode(company.Postcode);
        }

        public static DeliveryCharge GetMilkDeliveryCharge(string postcode)
        {
            return GetChargeByPostcode(postcode);
        }

        private static DeliveryCharge GetChargeBySupplier(Company company)
        {
            var dbContext = new SnapDbContext();
            var supplier = dbContext.Set<CompanySupplier>()
                .Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                .Where(x => x.CompanyID == company.ID)
                .Where(x => x.EndDate == null || x.EndDate >= DateTime.Now)
                .OrderBy(x => x.Supplier.Name)
                .Select(x => x.Supplier)
                .FirstOrDefault();

            return supplier?.DeliveryCharge?.FirstOrDefault(x => x.IsActive);
        }

        private static DeliveryCharge GetChargeByPostcode(string postcode)
        {
            postcode = (postcode ?? "").ToUpper().Trim().Replace(" ", "");
            var postcodePrefix = Regex.Split(postcode, @"[0-9]+")[0];
            
            var dbContext = new SnapDbContext();
            var charge = dbContext.Set<DeliveryCharge>()
                .Where(x => x.Category == DeliveryChargeCategory.General)
                .Where(x => x.Type == DeliveryChargeType.Milk)
                .Where(x => x.IsActive)
                .Where(x => x.Postcodes.Any(p => postcode.StartsWith(p.Postcode) && p.Postcode.StartsWith(postcodePrefix)))
                .FirstOrDefault();

            return charge;
        }
    }
}