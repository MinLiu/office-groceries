﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace NuIngredient.Models
{
    public class ExportCompanyHelper
    {
        private ExcelPackage _package { get; set; }
        private CompanyRepository _repo { get; set; }
        private static List<string> _headerList = new List<string>()
        {
            "Account Reference", "Account Name", "Customer Invoice Name", "Street 1", "Street 2", "Town", "County", "Postcode", "Contact Name", "Telephone Number",
            "Fax Number", "Analysis 1", "Analysis 2", "Analysis 3", "Department", "VAT Reg No", "MTD Turnover", "YTD Turnover", "Last Year",
            "Credit Limit", "Terms Text", "Due Days", "Settlement Discount", "Default Nominal", "Tax Code", "Trade Contact", "Telephone 2",
            "EMail", "WWW", "Discount Rate", "Payment Due Days", "Terms Agreed?", "Bank Name", "Bank Address 1", "Bank Address 2",
            "Bank Address 3", "Bank Address 4", "Bank Address 5", "Bank Account Name", "Bank Sort Code", "Bank Account No", "Bank BACS Ref",
            "Online Payments?", "Currency No", "Restrict Mailing?", "Date Account Opened", "Next Credit Review", "Last Credit Review",
            "Account Status", "Can Apply Charges?", "Country Code", "Priority Trader?", "Override Stock Tax?", "Override Stock Nom?",
            "Bank Additional 1", "Bank Additional 2", "Bank Additional 3", "Bank IBAN", "Bank BIC Swift", "Bank Roll Number", "Report Password",
            "DUNS Number", "Payment Method", "Letters Via Email?", "EMail 2", "EMail 3", "Donor Title", "Donor Forename", "Donor Surname",
            "Gift Aid Declaration Received?", "Declaration Valid From", "Inactive Account", "Payment Due From",
            "Direct Debit Email", "Twitter Address", "LinkedIn Address", "Facebook Address", "Delivery Postcode",
        };

        private ExportCompanyHelper()
        {
            _repo = new CompanyRepository();
        }

        public static ExportCompanyHelper New()
        {
            return new ExportCompanyHelper();
        }

        public ExportCompanyHelper BuildSpreadSheet(bool includesExported)
        {
            CreateWorksheet();
            SetHeaders();
            SetDataAndSetExported(includesExported);
            return this;
        }

        public byte[] Export()
        {
            var output = new System.IO.MemoryStream();
            _package.SaveAs(output);
            return output.ToArray();
        }

        private void CreateWorksheet()
        {
            _package = new ExcelPackage();
            _package.Workbook.Worksheets.Add("Customers");
        }

        private void SetHeaders()
        {
            var worksheet = _package.Workbook.Worksheets.First();
            var row = 1;
            var column = 1;
            foreach(var headerStr in _headerList)
            {
                worksheet.Cells[row, column++].Value = headerStr;
            }
        }
    
        private void SetDataAndSetExported(bool includesExported)
        {
            var companies = GetCompanies(includesExported);
            SetData(companies);
            if (!includesExported)
            {
                // Only set exported flag when exporting new customers
                SetExported(companies);
            }
        }

        private List<Company> GetCompanies(bool includesExported)
        {
            var companies = _repo
                .Read()
                .Where(c => includesExported == true || (includesExported == false && c.Exported == false))
                .ToList()
                .Where(c => c.CanBeExported)
                .OrderByDescending(c => c.AccountNo)
                .ToList();
            return companies;
        }

        private void SetData(List<Company> companies)
        {
            var worksheet = _package.Workbook.Worksheets.First();
            var row = 2;

            foreach (var company in companies)
            {
                var contactName = company.AllRelatedCompanies()?.SelectMany(c => c.ApplicationUsers).Select(u => u.FirstName + " " + u.LastName).FirstOrDefault()
                    ?? company.Name;

                Set(worksheet, row, "Account Reference", company.AccountNo);
                Set(worksheet, row, "Account Name", company.Name);
                Set(worksheet, row, "Customer Invoice Name", string.IsNullOrWhiteSpace(company.BillingCompanyName) ? company.Name : company.BillingCompanyName);
                Set(worksheet, row, "Street 1", company.BillingAddress);
                Set(worksheet, row, "Postcode", (company.BillingPostcode ?? "").Trim().ToUpper());
                Set(worksheet, row, "Contact Name", contactName.Trim());
                Set(worksheet, row, "Telephone Number", company.Telephone);
                Set(worksheet, row, "Department", "0");
                Set(worksheet, row, "Default Nominal", "4000");
                Set(worksheet, row, "Tax Code", "T1");
                Set(worksheet, row, "EMail", string.IsNullOrWhiteSpace(company.BillingEmail) ? company.Email : company.BillingEmail);
                Set(worksheet, row, "Terms Agreed?", "1");
                Set(worksheet, row, "Delivery Postcode", company.Postcode);
                row++;
            }

            worksheet.Cells.AutoFitColumns(0);
        }

        private void Set(ExcelWorksheet worksheet, int row, string key, string value)
        {
            var index = _headerList.IndexOf(key);
            if (index == -1) throw new Exception("header index not found: " + key);
            worksheet.Cells[row, ++index].Value = value;
        }

        private void SetExported(List<Company> companies)
        {
            var toUpdate = companies
                .Where(c => c.Exported == false)
                .ToList();
            toUpdate.ForEach(c => c.Exported = true);
            _repo.Update(toUpdate, new string[] { "Exported" });
        }
    }

}
