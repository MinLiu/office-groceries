﻿using System;
using System.Linq;
using Fruitful.Email;

namespace NuIngredient.Models
{
    public class BaseRequestAcctHelper
    {
        protected string GetRequestEmailAddrs(string deliveryPostcode, SupplierType supplierType)
        {
            var result = "";

            try
            {
                deliveryPostcode = (deliveryPostcode ?? "").ToUpper().Trim().Replace(" ", "");

                var helper = new PotentialSupplierHelper();
                var suppliers = helper.GetPotentialSuppliers(supplierType, deliveryPostcode)
                    .Where(s => s.EmailGoStraight)
                    .ToList();

                if (suppliers.Any())
                {
                    var supplierEmailList = suppliers.Select(x => x.Email).ToList();
                    result = String.Join(";", supplierEmailList);
                }
            }
            catch(Exception e) { }

            return result;
        }
        
        protected void SendAccountSetUpEmail(Company company, string category)
        {
            var mailMessage = AccountSetupEmailHelper.GetEmail(company, category);
            new Emailer(new SnapDbContext()).SendEmail(mailMessage);
        }
    }
}