﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;

namespace NuIngredient.Models
{
    public class WeeklyOrderReminderHelper
    {
        private readonly WeeklyOrderReminderConfig _config;

        public WeeklyOrderReminderHelper()
        {
            _config = new WeeklyOrderReminderConfig();
        }

        public WeeklyOrderReminderHelper(WeeklyOrderReminderConfig config)
        {
            _config = config;
        }

        public void Execute()
        {
            var query = new CompanyRepository().Read()
                .Where(c => c.DeleteData == false)
                .Include(c => c.CompanySuppliers);

            query = _config.IsSupplierSpecified
                ? query.Where(x => x.CompanySuppliers.Any(c => c.SupplierID == _config.SupplierId))
                : query;
            
            var companyList = query.ToList();
            var activeMilk = companyList.Where(ActiveOnMilk).ToList();
            var activeFruit = companyList.Where(ActiveOnFruit).ToList();
            var activeSnack = companyList.Where(ActiveOnSnack).ToList();
            
            PrepareMilkEmail(activeMilk);
            PrepareFruitEmail(activeFruit);
            PrepareSnackEmail(activeSnack);
        }
        
        private void PrepareMilkEmail(List<Company> companyList)
        {
            var dayOfWeek = DateTime.Now.DayOfWeek;
            var monday = DateTime.Now.AddDays(7).StartOfWeek(DayOfWeek.Monday);
            var service = new MilkOrderService<MilkOrderHeaderViewModel>(null, new SnapDbContext());
            var group = new List<GP>();
            foreach (var company in companyList)
            {
                var companySupplier = company.SupplierWithAccountNo(SupplierType.Milk, monday).FirstOrDefault();
                if (companySupplier == null) continue;

                if (!IsAccepted(companySupplier.Supplier, dayOfWeek))
                    continue;
                
                var orderNextWeek = service.GetWeeklyOrderNoTracking(OrderMode.OneOff, company.ID, monday);
                group.Add(new GP(company, companySupplier.Supplier, companySupplier.AccountNo, orderNextWeek));
            }

            var ts = group.GroupBy(g => g.Supplier);
            foreach (var t in ts)
            {
                var supplier = t.Key;
                
                try
                {
                    var shit = t.Select(x => new CompanyMilkOrder(x.MilkOrderHeader, x.AccountNo, x.Company)).ToList();
                    var email = WeeklyMilkOrderReminderEmailHelper.GetEmail(supplier, shit, monday);
                    SendEmail(email);
                }
                catch (Exception e)
                {
                    SendErrorEmail(e, supplier.Name);
                }
            }
        }
        
        private void PrepareFruitEmail(List<Company> companyList)
        {
            PrepareFruitAisleEmail(companyList, SupplierType.Fruits);
        }
        
        private void PrepareSnackEmail(List<Company> companyList)
        {
            PrepareFruitAisleEmail(companyList, SupplierType.Snacks);
        }
        
        private void PrepareFruitAisleEmail(List<Company> companyList, SupplierType supplierType)
        {
            var dayOfWeek = DateTime.Now.DayOfWeek;
            var monday = DateTime.Now.AddDays(7).StartOfWeek(DayOfWeek.Monday);
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(null, new SnapDbContext());
            var group = new List<GpFruit>();
            foreach (var company in companyList)
            {
                var companySupplier = company.SupplierWithAccountNo(supplierType, monday).FirstOrDefault();
                if (companySupplier == null) continue;
                
                if (!IsAccepted(companySupplier.Supplier, dayOfWeek))
                    continue;
                
                var orderNextWeek = service.GetWeeklyOrderAsNoTracking(OrderMode.OneOff, company.ID, monday);
                group.Add(new GpFruit(company, companySupplier.Supplier, companySupplier.AccountNo, orderNextWeek));
            }

            var ts = group.GroupBy(g => g.Supplier);
            foreach (var t in ts)
            {
                var supplier = t.Key;
                
                try
                {
                    var companyWithOrders = t.Select(x => new CompanyFruitOrder(x.FruitOrderHeader, x.AccountNo, x.Company)).ToList();
                    var email = WeeklyFruitOrderReminderEmailHelper.GetEmail(supplier, companyWithOrders, monday);
                    SendEmail(email);
                }
                catch (Exception e)
                {
                    SendErrorEmail(e, supplier.Name);
                }
            }
        }
        
        private static bool ActiveOnMilk(Company company)
        {
            // find the companies if it's active next week
            return company.IsLiveWeek(SupplierType.Milk, DateTime.Now.AddDays(7));
        }

        private static bool ActiveOnFruit(Company company)
        {
            // find the companies if it's active next week
            return company.IsLiveWeek(SupplierType.Fruits, DateTime.Now.AddDays(7));
        }
        
        private static bool ActiveOnSnack(Company company)
        {
            // find the companies if it's active next week
            return company.IsLiveWeek(SupplierType.Snacks, DateTime.Now.AddDays(7));
        }

        private bool IsAccepted(Supplier supplier, DayOfWeek dayOfWeek)
        {
            if (_config.IsSupplierSpecified)
            {
                return supplier.ID == _config.SupplierId;
            }
            
            if (supplier.WeeklyEmailReminderEnabled == false)
            {
                return false;
            }

            if (_config.IgnoreWeekday)
            {
                return true;
            }

            return supplier.WeeklyEmailReminderDay == dayOfWeek &&
                   supplier.WeeklyEmailReminderHour == DateTime.Now.Hour &&
                   supplier.WeeklyEmailReminderMinute == (DateTime.Now.Minute / 30) * 30;
        }
        
        private void SendErrorEmail(Exception e, string supplierName)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add("min.liu@zigaflow.com");
            mailMessage.From = new MailAddress("orders@office-groceries.com");
            mailMessage.Subject = $"Failed to Generate Weekly Order Reminder from Office Groceries";
            mailMessage.Body = $"Supplier: {supplierName}<br />" +
                               $"Message: {e.Message}<br/>" +
                               $"StackTrace: {e.StackTrace}";
            mailMessage.IsBodyHtml = true;
            SendEmail(mailMessage);
        }
        
        private bool SendEmail(MailMessage mailMessage)
        {
            try
            {

                using (var context = new SnapDbContext())
                {
                    var emailer = new Fruitful.Email.Emailer(context);
                    emailer.SendEmail(mailMessage);
                    return true;
                }
            }
            catch
            {
                return false;
            }

        }
        
        private class GP
        {
            public Company Company { get; set; }
            public Supplier Supplier { get; set; }
            public string AccountNo { get; set; }
            public MilkOrderHeader MilkOrderHeader { get; set; }

            public GP(Company company, Supplier supplier, string accountNo, MilkOrderHeader milkOrderHeader)
            {
                Company = company;
                Supplier = supplier;
                AccountNo = accountNo;
                MilkOrderHeader = milkOrderHeader;
            }
        }
        
        private class GpFruit
        {
            public Company Company { get; set; }
            public Supplier Supplier { get; set; }
            public string AccountNo { get; set; }
            public FruitOrderHeader FruitOrderHeader { get; set; }

            public GpFruit(Company company, Supplier supplier, string accountNo, FruitOrderHeader fruitOrderHeader)
            {
                Company = company;
                Supplier = supplier;
                AccountNo = accountNo;
                FruitOrderHeader = fruitOrderHeader;
            }
        }

        public class WeeklyOrderReminderConfig
        {
            public bool IgnoreWeekday { get; set; }
            public Guid? SupplierId { get; set; }
            public bool IsSupplierSpecified => SupplierId != null;
        }
    }
}