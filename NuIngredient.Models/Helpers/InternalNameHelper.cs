﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace NuIngredient.Models
{
    
    public class InternalNameHelper
    {
        private readonly static Regex _rgxNoneAlphanumeric = new Regex("[^a-zA-Z0-9-]");
        private readonly static Regex _rgxDuplicateDashes = new Regex("[-]+");

        public static string Convert(string name)
        {
            var result = name.ToLower();

            result = _rgxNoneAlphanumeric.Replace(result, "-");

            result = _rgxDuplicateDashes.Replace(result, "-");

            result = result.Trim(' ', '-');

            return result;
        }
    }
}
