﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models.Helpers.Reports
{
    /// <summary>
    /// Helper class for generating the customer list report with whether they are a fruit or milk customer
    /// </summary>
    public class CustomerListReportHelper
    {
        public async Task<string> GetReportCsvAsync()
        {
            var bidFoodSupplierID = Guid.Parse("06f2e6c6-74e5-46fd-80be-95c4874bd0ab");
            var brakesSupplierID = Guid.Parse("c83a90f7-1cf9-4c9c-aac5-5e74f21f7bc1");
            
            var companies = await new CompanyRepository()
                .Read()
                .Include(x => x.CompanySuppliers)
                .Include(x => x.ApplicationUsers)
                .Select(x => new Entity
                {
                    AccountNo = x.AccountNo,
                    Name = x.Name,
                    Email = x.Email,
                    Telephone = x.Telephone,
                    ContactName = x.CompanyRegistration,
                    User = x.ApplicationUsers.FirstOrDefault(u => u.Roles.Any(r => r.RoleId == "1")),
                    Fruit = x.CompanySuppliers.Any(c => c.Supplier.SupplierType == SupplierType.Fruits && c.EndDate == null),
                    Milk = x.CompanySuppliers.Any(c => c.Supplier.SupplierType == SupplierType.Milk && c.EndDate == null),
                    Bidfood = x.CompanySuppliers.Any(c => c.Supplier.ID == bidFoodSupplierID && c.EndDate == null),
                    BrakesCateringEquipment = x.CompanySuppliers.Any(c => c.Supplier.ID == brakesSupplierID && c.EndDate == null)
                })
                .OrderBy(x => x.Name)
                .ToListAsync();

            // generate csv
            var csv = new StringBuilder();
            csv.AppendLine("Name,AccountNo,Email,Telephone,ContactName,Fruit,Milk,Bidfood,BrakesCateringEquipment");
            foreach (var company in companies)
            {
                var contactName = string.IsNullOrWhiteSpace(company.ContactName)
                    ? company.User?.ToFullName() ?? ""
                    : company.ContactName;
                csv.AppendLine(
                    $"{EscapeCsvString(company.Name)},{EscapeCsvString(company.AccountNo)},{EscapeCsvString(company.Email)},{EscapeCsvString(company.Telephone)},{EscapeCsvString(contactName)},{(company.Fruit ? "v" : "")},{(company.Milk ? "v" : "")},{(company.Bidfood ? "v" : "")},{(company.BrakesCateringEquipment ? "v" : "")}");
            }
            
            return csv.ToString();
        }

        private string EscapeCsvString(string input)
        {
            input = $"\"{input}\"";
            return input;
        }
        
        private class Entity
        {
            public string AccountNo { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Telephone { get; set; }
            public string ContactName { get; set; }
            public bool Fruit { get; set; }
            public bool Milk { get; set; }
            public bool Bidfood { get; set; }
            public bool BrakesCateringEquipment { get; set; }
            public User User { get; set; }
        }
    }
}