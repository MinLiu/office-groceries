﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Fruitful.Import;

namespace NuIngredient.Models
{
    public class ImportDryGoodsPriceHelper
    {
        private ImportDryGoodsPriceHelper()
        {
        }

        public static ImportDryGoodsPriceHelper New()
        {
            return new ImportDryGoodsPriceHelper();
        }

        public ImportResult ImportExcelFile(string filePath, string sheetName = null, bool deleteFile = true)
        {
            var dataTable = ImportService.CopyExcelFileToTable(filePath, deleteFile, sheetName);

            return ImportDataTable(dataTable);
        }

        private ImportResult ImportDataTable(DataTable dataTable)
        {
            var pdtRepo = new ProductRepository();
            var pdtsToUpdate = new List<Product>();
            var pdtsNotFound = new List<string>();

            foreach (DataRow row in dataTable.Rows)
            {
                try
                { 
                    var supplierName = row["Supplier"].ToString().ToLower();
                    var pdtCode = row["Product Code"].ToString();
                    var price = Math.Round(decimal.Parse(row["Retail"].ToString()), 2, MidpointRounding.AwayFromZero);
                    var cost = Math.Round(decimal.Parse(row["Cost"].ToString()), 2, MidpointRounding.AwayFromZero);

                    var pdt = pdtRepo.Read()
                        .Where(p => p.Supplier.Name.ToLower() == supplierName)
                        .Where(p => p.ProductCode == pdtCode)
                        .FirstOrDefault();

                    if (pdt == null)
                    {
                        pdtsNotFound.Add(pdtCode);
                        continue;
                    }

                    pdt.Price = price;
                    pdt.Cost = cost;

                    pdtsToUpdate.Add(pdt);
                }
                catch(Exception e)
                {
                    throw new Exception(string.Format("Something wrong with row {0}", dataTable.Rows.IndexOf(row) + 2));
                }
            }

            pdtRepo.Update(pdtsToUpdate, new string[] { "Price", "Cost" });

            var result = new ImportResult()
            {
                TotalSuccess = pdtsToUpdate.Count,
                TotalFail = pdtsNotFound.Count,
                Message = string.Format("Product Not Found: {0}", string.Join(", ", pdtsNotFound)),
            };

            return result;
        }

        public class ImportResult
        {
            public int TotalSuccess { get; set; }
            public int TotalFail { get; set; }
            public string Message { get; set; }
        }
    }
}