﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class SelectItemViewModel
    {
        public SelectItemViewModel()
        {
        }

        public SelectItemViewModel(string id, string name)
        {
            ID = id;
            Name = name;
        }

        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class UserItemViewModel
    {
        public string ID { get; set; }
        public string Email { get; set; }
    }

    public enum TemplateType { Front, Body, Back }
    public enum TemplateFormat { Pdf, Docx }

}