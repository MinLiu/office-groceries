﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using Fruitful.Email;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class DryGoodsOrderPlacedMailHelper
    {
        public bool SendMail(OneOffOrder order)
        {
            var company = new SnapDbContext().Companies.FirstOrDefault(x => x.ID == order.CompanyID);
            DateTime? nextDeliveryDate = company.NextDryGoodsDeliveryDate(order.SupplierID.Value);

            if (order.Status == OneOffOrderStatus.Proforma)
            {
                SendProformaOrderMailToApprover(company, order, nextDeliveryDate);
            }
            else
            {
                
                SendOrderMailToCustomer(company, order, nextDeliveryDate);
                SendOrderMailToSupplier(company, order, nextDeliveryDate);
            }

            return true;
        }
        
        private void SendProformaOrderMailToApprover(Company company, OneOffOrder order, DateTime? deliveryDate)
        {
            var mailMessage = DryGoodsProformaEmailHelper.GetEmail(company.ID, order.ID, deliveryDate);
            new Emailer(new SnapDbContext()).SendEmail(mailMessage);
        }
        
        private void SendOrderMailToCustomer(Company company, OneOffOrder order, DateTime? deliveryDate)
        {
            var mailMessage = DryGoodsOrderConfirmEmailHelper.GetEmail(company, order, deliveryDate);
            new Emailer(new SnapDbContext()).SendEmail(mailMessage);
        }
        
        private void SendOrderMailToSupplier(Company company, OneOffOrder order, DateTime? deliveryDate)
        {
            string supplierEmail = "";
            string accountDetail = "";

            var deliveryPrompt = deliveryDate != null ? "The customer's anticipated delivery day is: <strong>" + deliveryDate.Value.ToString("dd/MM/yyyy") + "</strong><br />"
                                                      : "The customer's anticipated delivery day is within next 3 - 4 working days <br />";

            deliveryPrompt += $"<br/><strong>PLEASE ENSURE YOU ADD ORDER NUMBER {order.OrderNumber} TO YOUR BILLING/INVOICE FOR OFFICE GROCERIES</strong><br/>";

            var setting = new SnapDbContext().EmailSettings.First();

            var supplier = new SupplierRepository().Find(order.SupplierID.Value);
            if (supplier.UseMasterAccount)
            {
                supplierEmail = supplier.Email;
                accountDetail = company.AccountNo;
            }
            else
            {
                var difficultSupplier = company.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.DryGoods)
                                                                .Where(x => x.SupplierID == order.SupplierID)
                                                                .Where(x => x.EndDate == null || x.EndDate >= DateTime.Today)
                                                                .Where(x => x.StartDate <= DateTime.Today)
                                                                .First();
                supplierEmail = difficultSupplier?.SupplierDepot?.Email ?? difficultSupplier?.Supplier.Email;
                accountDetail = difficultSupplier.AccountNo;

            }

            var imgAttachments = new List<Attachment>();
            var body = GetSupplierOrderMailBody(order, out imgAttachments);

            var orderRepo = new OneOffOrderRepository();
            order = orderRepo.Read().Where(x => x.ID == order.ID).Include(x => x.OrderEventDetails).FirstOrDefault();

            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(setting.Email);
            mailMessage.AddToEmails(supplierEmail);
            mailMessage.Subject = String.Format("Order #{0} Confirmation from Office-Groceries Ltd", order.OrderNumber);
            if (order.OrderEventDetails.Any())
            {
                var eventDetailTemplate = "";
                foreach(var eventDetail in order.OrderEventDetails)
                {
                    eventDetailTemplate += String.Format(
                        $"<table>" +
                            "<tr><td><label>Function Name</label></td><td>{0}</td></tr>" +
                            "<tr><td><label>Address Line 1</label></td><td>{1}</td></tr>" +
                            "<tr><td><label>Address Line 2</label></td><td>{2}</td></tr>" +
                            "<tr><td><label>Town</label></td><td>{3}</td></tr>" +
                            "<tr><td><label>Postcode</label></td><td>{4}</td></tr>" +
                            "<tr><td><label>Room Name</label></td><td>{5}</td></tr>" +
                            "<tr><td><label>Event Date</label></td><td>{6}</td></tr>" +
                            "<tr><td><label>Start Time</label></td><td>{7}</td></tr>" +
                            "<tr><td><label>Delivery Details</label></td><td>{8}</td></tr>" +
                            "<tr><td><label>Special Requirements</label></td><td>{9}</td></tr>" +
                        "</table>"
                        , eventDetail.FunctionName
                        , eventDetail.AddressLine1
                        , eventDetail.AddressLine2
                        , eventDetail.Town
                        , eventDetail.Postcode
                        , eventDetail.RoomName
                        , eventDetail.EventDate.ToString("dd/MM/yyyy")
                        , eventDetail.StartTime.ToString("HH:mm")
                        , eventDetail.DeliveryDetails
                        , eventDetail.SpecialRequirements);
                }
                mailMessage.Body = String.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                                     //"Hello {1}<br /><br />" +
                                                     "Please accept this e-mail as confirmation for the following order from Office-Groceries Ltd:<br />" +
                                                     "<br />" +
                                                     "<hr />" +
                                                        "<table>" +
                                                            "<tr>" +
                                                                "<td><label>Account Number:</label></td><td>{0}</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td><label>Company:</label></td><td>{1}</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td><label>Delivery Instructions:</label><td>{2}</td>" +
                                                            "</tr>" +
                                                        "</table>" +
                                                        "<hr />" +
                                                        "{3}" +
                                                        "<hr />" +
                                                        "<br />" +
                                                        "{4}" +
                                                     "</div> " +
                                                 "</div>", accountDetail
                                                         , company.Name
                                                         , order.OrderNotes
                                                         , eventDetailTemplate
                                                         , body);
            }
            else
            { 
                mailMessage.Body = String.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                                     //"Hello {1}<br /><br />" +
                                                     "Please accept this e-mail as confirmation for the following order from Office-Groceries Ltd:<br />" +
                                                     "<br />" +
                                                     deliveryPrompt +
                                                     "<br />" +
                                                     "<hr />" +
                                                        "<table>" +
                                                            "<tr>" +
                                                                "<td><label>Account Number:</label></td><td>{0}</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td><label>Company:</label></td><td>{1}</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td><label>Delivery Address:</label></td><td>{2}</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td><label>Postcode:</label></td><td>{3}</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td><label>Delivery Instructions:</label><td>{4}</td>" +
                                                            "</tr>" +
                                                        "</table>" +
                                                        "<hr />" +
                                                        "<br />" +
                                                        "{5}" +
                                                     "</div> " +
                                                 "</div>", accountDetail
                                                         , company.Name
                                                         , company.Address1
                                                         , company.Postcode
                                                         , order.OrderNotes
                                                         , body);
            }
            mailMessage.IsBodyHtml = true;

            new Emailer(new SnapDbContext()).SendEmail(mailMessage);
        }
        
        private string GetSupplierOrderMailBody(OneOffOrder order, out List<Attachment> attachments)
        {
            attachments = new List<Attachment>();
            var oneoffOrderRepo = new OneOffOrderRepository();

            order = oneoffOrderRepo.Read().Where(x => x.ID == order.ID).Include(x => x.OneOffOrderItems).FirstOrDefault();

            var tableHeader = "<table border='1' style='width: 700px;'>" +
                              "<thead>" +
                              "<th>Product</th>" +
                              "<th>Product Code</th>" +
                              "<th>Unit Size</th>" +
                              "<th>Qty</th>" +
                              "</thead>";

            var tableBody = "";
            var subtotal = 0m;
            foreach (var item in order.OneOffOrderItems.Where(x => x.Qty != 0))
            {
                var str = String.Format("<tr>" +
                                        "<td style='text-align: center;'>{0}</td>" +
                                        "<td style='text-align: center;'>{1}</td>" +
                                        "<td style='text-align: center;'>{2}</td>" +
                                        "<td style='text-align: center;'>{3}</td>" +
                                        "</tr>", item.ProductName
                    , item.ProductCode
                    , item.Product.Unit
                    , item.Qty);
                tableBody += str;
                // attach img
                //try
                //{
                //    attachments.Add(new Attachment(Server.MapPath(item.ImageURL)) { ContentId = order.ID + ".png" });
                //}
                //catch { }
                subtotal += item.Total;
            }

            string tableFooter = "</table>";

            return tableHeader + tableBody + tableFooter;
        }
    }
}