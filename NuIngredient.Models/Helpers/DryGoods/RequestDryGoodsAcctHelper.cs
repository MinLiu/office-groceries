﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Fruitful.Email;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    public class RequestDryGoodsAcctHelper : BaseRequestAcctHelper
    {
        public bool RequestDryGoodsSupplier(Guid companyID, List<Guid> supplierIDs = null, bool sendToCustomer = true)
        {
            var companyRepo = new CompanyRepository();
            var company = companyRepo.Find(companyID);
            
            var supplierRepo = new SupplierRepository();
            var requestedSuppliers = new List<Supplier>();
            if (supplierIDs != null)
            {
                requestedSuppliers = supplierRepo.Read()
                                                 .Where(x => supplierIDs.Contains(x.ID))
                                                 .ToList();
            }
            else
            {
                requestedSuppliers = supplierRepo.Read()
                                                 .Where(x => x.Deleted == false)
                                                 .Where(x => x.SupplierType == SupplierType.DryGoods)
                                                 .Where(x => x.UseMasterAccount == false)
                                                 .Where(x => x.DefaultAisles.Any())
                                                 .ToList();
            }

            using (var db = new SnapDbContext())
            {
                var emailSetting = db.EmailSettings.First();

                string greeting = "Hello<br />" +
                                  "<br />" +
                                  "Please accept this e-mail as a formal request to open a Dry Goods account from Office-Groceries for our client below.<br />" +
                                  "<br />" +
                                  "<strong>Could you please forward on account details along with a confirmation of delivery days to: <a href='mailto:Orders@office-groceries.com'>Orders@office-groceries.com</a></strong><br />" +
                                  "<br />";

                string customerDetails = String.Format("<table>" +
                                                            "<tr><td><label>Company:</label></td><td>{0}</td></tr>" +
                                                            "<tr><td><label>Delivery Address:</label></td><td>{1}</td></tr>" +
                                                            "<tr><td><label>Postcode:</label></td><td>{2}</td></tr>" +
                                                            "<tr><td><label>Delivery Instructions:</label></td><td>{3}</td></tr>" +
                                                        "</table><br />", company.Name
                                                                        , company.Address1
                                                                        , company.Postcode
                                                                        , company.DeliveryInstruction);
                
                var customerDetailsForBidFood = String.Format("<table>" +
                                                              "<tr><td><label>Company:</label></td><td>{0}</td></tr>" +
                                                              "<tr><td><label>Delivery Address:</label></td><td>{1}</td></tr>" +
                                                              "<tr><td><label>Postcode:</label></td><td>{2}</td></tr>" +
                                                              "<tr><td><label>Delivery Instructions:</label></td><td>{3}</td></tr>" +
                                                              "<tr><td><label>Contact:</label></td><td>{4}</td></tr>" +
                                                              "<tr><td><label>Contact Phone:</label></td><td>{5}</td></tr>" +
                                                              "</table><br />"
                    , company.Name
                    , company.Address1
                    , company.Postcode
                    , company.DeliveryInstruction
                    , company.ApplicationUsers?.FirstOrDefault()?.ToFullName() ?? company.CompanyCollection?.Companies?.SelectMany(c => c.ApplicationUsers)?.FirstOrDefault()?.ToFullName() 
                    , company.Telephone);
                
                var body = greeting + customerDetails;
                var bodyForBidFood = greeting + customerDetailsForBidFood;

                foreach (var supplier in requestedSuppliers.Where(x => !x.UseMasterAccount))
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.AddToEmails(supplier.RequestCenterEmail);
                    mailMessage.From = new MailAddress(emailSetting.Email);
                    mailMessage.Subject = "Request for a Dry Goods account from Office-Groceries";
                    mailMessage.Body = "<div class=\"row\"> " +
                                            "<div class=\"col-sm-12\">" +
                                                "<br />" +
                                                (supplier.ID.ToString() == "06f2e6c6-74e5-46fd-80be-95c4874bd0ab" /*Bidfood*/ ? bodyForBidFood : body) +
                                            "</div> " +
                                        "</div>";
                    mailMessage.IsBodyHtml = true;

                    bool success = new Emailer(new SnapDbContext()).SendEmail(mailMessage);

                    if (success)
                    {
                        //db.Companies.Where(x => x.ID == company.ID).First().AskingForDryGoodsSupplier = DateTime.Now;
                        //db.SaveChanges();
                        var supAcctRequestLogRepo = new SupAcctRequestLogRepository();
                        supAcctRequestLogRepo.Create(new SupAcctRequestLog
                        {
                            CompanyID = company.ID,
                            SupplierID = supplier.ID,
                            RequestTime = DateTime.Now
                        });
                    }
                }

                if (sendToCustomer)
                { 
                    SendAccountSetUpEmail(company, String.Join(", ", requestedSuppliers.Select(x => x.DefaultAisleLabels)));
                }
                return true;
            }
        }
    }
}