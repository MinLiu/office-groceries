﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuIngredient.Models.Helpers;

namespace NuIngredient.Models
{
    
    public static class CutOffTime
    {
        public const int TimeOfDay = 14;
        public const int TimeOfDayPm = 2;
        public const int RestTime = 24 - TimeOfDay;
        public static string ToTimeString()
        {
            return string.Format("{0}pm", TimeOfDayPm);
        }
    }

    public static class AppSettings
    {
        private const int StartHour = 7; // 07:00
        private const int EndHour = 14; // 14:00

        /// <summary>
        /// Users can only change order between 7am to 2pm weekdays
        /// </summary>
        /// <returns></returns>
        public static bool IsEditableTime()
        {
            var result = IsFrozenByAdmin();
            if (result.IsFrozen)
            {
                FreezingTimeMessage = result.Message;
                return false;
            }
            
            FreezingTimeMessage = DefaultFreezingTimeMessage;
            
            if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday
                || DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                return false;

            var currentHour = DateTime.Now.Hour;

            return currentHour >= StartHour && currentHour < EndHour;
        }
        
        private static readonly string DefaultFreezingTimeMessage = "Please note permanent/one-off changes to your standing order can only be made between 7:00 am and 2:00 pm Monday to Friday";

        public static string FreezingTimeMessage = DefaultFreezingTimeMessage;

        public static readonly List<string> DeliveryAreas = new List<string>()
        {
            "Edinburgh",
            "Cardiff",
            "Birmingham",
            "Glasgow",
            "Bristol",
            "Manchester",
            "Leeds",
            "Southampton",
            "London",
            "Liverpool",
            "Reading",
            "Newcastle",
            "Sheffield",
            "Northampton",
            "Nottingham"
        };

        private static FreezeSettingHelper.FrozenSettingResult IsFrozenByAdmin()
        {
            var freezeSetting = new FreezeSettingHelper();
            return freezeSetting.IsFrozenByAdmin();
        }
    }
}
