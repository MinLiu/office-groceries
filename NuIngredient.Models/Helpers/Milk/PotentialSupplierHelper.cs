﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace NuIngredient.Models
{
    public class PotentialSupplierHelper
    {
        private readonly SupplierRepository _repo = new SupplierRepository();

        public List<Supplier> GetPotentialMilkSuppliers(string postcode)
        {
            return GetPotentialSuppliers(SupplierType.Milk, postcode);
        }

        public List<Supplier> GetPotentialFruitSuppliers(string postcode)
        {
            return GetPotentialSuppliers(SupplierType.Fruits, postcode);
        }
        
        public List<Supplier> GetPotentialSnackSuppliers(string postcode)
        {
            return GetPotentialSuppliers(SupplierType.Snacks, postcode);
        }

        public List<Supplier> GetPotentialSuppliers(SupplierType type, string postcode)
        {
            postcode = (postcode ?? "").ToUpper().Trim().Replace(" ", "");
            var postcodePrefix = Regex.Split(postcode, @"[0-9]+")[0];

            var suppliers = _repo.Read()
                .Where(x => x.Deleted == false)
                .Where(x => x.SupplierType == type)
                .Where(x => x.SupplierPostcode.Any(p => postcode.StartsWith(p.Postcode) && p.Postcode.StartsWith(postcodePrefix)))
                .OrderBy(x => x.Name)
                .ToList();

            return suppliers;
        }
    }
}
