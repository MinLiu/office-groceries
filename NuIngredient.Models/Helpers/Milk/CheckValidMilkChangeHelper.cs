﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class CheckValidMilkChangeHelper
    {
        public static bool IsValidChange(List<MilkOrderViewModel> newOrders, MilkOrderHeader oldOrder, DateTime fromDate)
        {
            var weekdays = new bool[7] { false, false, false, false, false, false, false };
            var weekdates = new DateTime[7]
            {
                fromDate.AddDays(-1),
                fromDate.AddDays(0),
                fromDate.AddDays(1),
                fromDate.AddDays(2),
                fromDate.AddDays(3),
                fromDate.AddDays(4),
                fromDate.AddDays(5)
            };
            foreach (var newOrderLine in newOrders)
            {
                var oldOrderLine = oldOrder.Items.Where(x => x.MilkProductID.ToString() == newOrderLine.MilkID).FirstOrDefault();
                if (oldOrderLine != null)
                {
                    if (oldOrderLine.Monday != newOrderLine.Monday) weekdays[(int)DayOfWeek.Monday] = true;
                    if (oldOrderLine.Tuesday != newOrderLine.Tuesday) weekdays[(int)DayOfWeek.Tuesday] = true;
                    if (oldOrderLine.Wednesday != newOrderLine.Wednesday) weekdays[(int)DayOfWeek.Wednesday] = true;
                    if (oldOrderLine.Thursday != newOrderLine.Thursday) weekdays[(int)DayOfWeek.Thursday] = true;
                    if (oldOrderLine.Friday != newOrderLine.Friday) weekdays[(int)DayOfWeek.Friday] = true;
                    if (oldOrderLine.Saturday != newOrderLine.Saturday) weekdays[(int)DayOfWeek.Saturday] = true;
                }
                else
                {
                    if (newOrderLine.Monday != 0) weekdays[(int)DayOfWeek.Monday] = true;
                    if (newOrderLine.Tuesday != 0) weekdays[(int)DayOfWeek.Tuesday] = true;
                    if (newOrderLine.Wednesday != 0) weekdays[(int)DayOfWeek.Wednesday] = true;
                    if (newOrderLine.Thursday != 0) weekdays[(int)DayOfWeek.Thursday] = true;
                    if (newOrderLine.Friday != 0) weekdays[(int)DayOfWeek.Friday] = true;
                    if (newOrderLine.Saturday != 0) weekdays[(int)DayOfWeek.Saturday] = true;
                }
            }
            foreach (var oldOrderLine in oldOrder.Items.Where(x => !newOrders.Select(n => Guid.Parse(n.MilkID)).Contains(x.MilkProductID.Value)))
            {
                if (oldOrderLine.Monday != 0) weekdays[(int)DayOfWeek.Monday] = true;
                if (oldOrderLine.Tuesday != 0) weekdays[(int)DayOfWeek.Tuesday] = true;
                if (oldOrderLine.Wednesday != 0) weekdays[(int)DayOfWeek.Wednesday] = true;
                if (oldOrderLine.Thursday != 0) weekdays[(int)DayOfWeek.Thursday] = true;
                if (oldOrderLine.Friday != 0) weekdays[(int)DayOfWeek.Friday] = true;
                if (oldOrderLine.Saturday != 0) weekdays[(int)DayOfWeek.Saturday] = true;
            }

            var notAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date;

            for (var i = 0; i < weekdays.Length; i++)
            {
                if (weekdays[i] == true)
                {
                    if (weekdates[i] <= notAllowedBefore)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
