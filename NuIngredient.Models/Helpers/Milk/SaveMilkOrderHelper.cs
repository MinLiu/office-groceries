﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace NuIngredient.Models
{
    public class SaveMilkOrderHelper
    {
        private SnapDbContext _dbContext {get;set;}
        private MilkOrderService<MilkOrderHeaderViewModel> _service { get; set; }
        private MilkOrderMapper _mapper { get; set; }
        private List<MilkOneOffChangeTracker> trackersToCreate { get; set; }
        private List<MilkOneOffChangeTracker> trackersToUpdate { get; set; }
        private Guid _companyID { get; set; }
        private string _prospectToken { get; set; }
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private SaveMilkOrderHelper()
        {
            _dbContext = new SnapDbContext();
            _service = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), _dbContext);
            _mapper = new MilkOrderMapper();
            trackersToCreate = new List<MilkOneOffChangeTracker>();
            trackersToUpdate = new List<MilkOneOffChangeTracker>();
        }

        public static SaveMilkOrderHelper New()
        {
            return new SaveMilkOrderHelper();
        }

        public SaveMilkOrderHelper SetCompanyID(Guid companyID)
        {
            this._companyID = companyID;
            return this;
        }

        public SaveMilkOrderHelper SetProspectToken(string prospectToken)
        {
            this._prospectToken = prospectToken;
            return this;
        }

        public MilkOrderHeader SaveCustomOrder(OrderMode orderMode, DateTime? fromDate, List<MilkOrderViewModel> orderItems)
        {
            if (_companyID == null)
                throw new Exception("Must set companyID");

            var orderHeader = GetCustomOrder(orderMode, fromDate);

            if (orderMode == OrderMode.OneOff)
            {
                if (!CheckValidMilkChangeHelper.IsValidChange(orderItems, orderHeader, fromDate.Value))
                {
                    throw new Exception("Changes to next day must be done before 2 pm today.");
                }
                SetChangeTracker(orderItems, orderHeader);
            }

            if (orderHeader != null && orderHeader.OrderMode == orderMode)
            {
                if (orderHeader.OrderMode == OrderMode.Regular)
                {
                    orderHeader.Depreciated = true;
                    _service.Update(orderHeader);
                    orderHeader = CreateNewOrder(orderMode, fromDate);
                }
                else
                {
                    DeleteAllOrderItems(orderHeader);
                    orderHeader.LatestUpdated = DateTime.Now;
                    _service.Update(orderHeader);
                }
            }
            else
            {
                orderHeader = CreateNewOrder(orderMode, fromDate);
            }

            AddOrderItems(orderHeader.ID, orderItems);

            try
            {
                if (orderMode == OrderMode.Regular)
                {
                    AdjustFutureOneOffOrders(orderItems);
                }
                else if (orderMode == OrderMode.OneOff)
                {
                    UpdateTracker(orderHeader.ID);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            orderHeader = _service.CalculateTotal(orderHeader.ID);

            return orderHeader;
        }

        public bool SaveProspectOrder(List<MilkOrderViewModel> orderItems)
        {
            if (string.IsNullOrEmpty(_prospectToken))
                throw new Exception("Must set prospect token");

            var orderHeader = GetProspectOrder();
            if (orderItems.Any())
            {
                if (orderHeader != null)
                {
                    _service.DeleteAllItems(orderHeader);
                    orderHeader.LatestUpdated = DateTime.Now;
                    _service.Update(orderHeader);
                }
                else
                {
                    orderHeader = new MilkOrderHeader();
                    orderHeader.ProspectToken = _prospectToken;
                    orderHeader.OrderMode = OrderMode.Draft;
                    orderHeader.OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff"));
                    orderHeader.LatestUpdated = DateTime.Now;
                    _service.Create(orderHeader);
                }

                foreach (var item in orderItems)
                {
                    var product = new MilkProductRepository().Find(Guid.Parse(item.MilkID));
                    item.Price = product.Price;
                    item.ProductName = product.Name;
                    item.Unit = product.Unit;
                    var model = _mapper.MapToModel(item);
                    _service.AddItem(orderHeader.ID, model);
                }

                _service.CalculateTotal(orderHeader.ID);
            }
            else
            {
                _service.DeleteOrder(orderHeader);
            }

            return true;
        }

        private MilkOrderHeader GetCustomOrder(OrderMode orderMode, DateTime? fromDate)
        {
            return _service.GetCustomerOrder(_companyID, orderMode, fromDate);
        }

        private MilkOrderHeader GetProspectOrder()
        {
            return _service.GetProspectOrder(_prospectToken);
        }

        private void SetChangeTracker(List<MilkOrderViewModel> newOrders, MilkOrderHeader oldOrder)
        {
            var repo = new MilkOneOffChangeTrackerRepository(_dbContext);
            var trackers = repo.Read().Where(x => x.OrderHeaderID == oldOrder.ID).ToList();
            trackersToUpdate = trackers.ToList();
            foreach (var newOrderLine in newOrders)
            {
                var oldOrderLine = oldOrder
                    .Items
                    .FirstOrDefault(x => x.MilkProductID != null && x.MilkProductID.ToString() == newOrderLine.MilkID);

                if (oldOrderLine != null)
                {
                    var tracker = trackers.FirstOrDefault(x => x.MilkProductID == oldOrderLine.MilkProductID);
                    if (tracker == null)
                    {
                        tracker = new MilkOneOffChangeTracker()
                        {
                            MilkProductID = oldOrderLine.MilkProductID.Value,
                            //OrderHeaderID = oldOrder.ID,
                        };
                        trackersToCreate.Add(tracker);
                    }

                    if (oldOrderLine.Monday != newOrderLine.Monday) tracker.Monday = true;
                    if (oldOrderLine.Tuesday != newOrderLine.Tuesday) tracker.Tuesday = true;
                    if (oldOrderLine.Wednesday != newOrderLine.Wednesday) tracker.Wednesday = true;
                    if (oldOrderLine.Thursday != newOrderLine.Thursday) tracker.Thursday = true;
                    if (oldOrderLine.Friday != newOrderLine.Friday) tracker.Friday = true;
                    if (oldOrderLine.Saturday != newOrderLine.Saturday) tracker.Saturday = true;
                }
                else
                {
                    var tracker = trackers.Where(x => x.MilkProductID == Guid.Parse(newOrderLine.MilkID)).FirstOrDefault();
                    if (tracker == null)
                    {
                        tracker = new MilkOneOffChangeTracker()
                        {
                            MilkProductID = Guid.Parse(newOrderLine.MilkID),
                            //OrderHeaderID = oldOrder.ID,
                        };
                        trackersToCreate.Add(tracker);
                    }

                    if (newOrderLine.Monday != 0) tracker.Monday = true;
                    if (newOrderLine.Tuesday != 0) tracker.Tuesday = true;
                    if (newOrderLine.Wednesday != 0) tracker.Wednesday = true;
                    if (newOrderLine.Thursday != 0) tracker.Thursday = true;
                    if (newOrderLine.Friday != 0) tracker.Friday = true;
                    if (newOrderLine.Saturday != 0) tracker.Saturday = true;
                }
            }
        }

        private void AdjustFutureOneOffOrders(List<MilkOrderViewModel> latestItemViewModels)
        {
            var latestItems = latestItemViewModels
                .Select(x => _mapper.MapToModel(x))
                .ToList();

            var repo = new MilkOrderHeaderRepository(_dbContext);
            var milkOrderItemRepo = new MilkOrderRepository(_dbContext);

            var futureOneOffOrders = repo
                .Read()
                .Where(x => x.CompanyID == _companyID)
                .Where(x => x.OrderMode == OrderMode.OneOff)
                .Where(x => x.ToDate != null && x.ToDate > DateTime.Today)
                .Include(x => x.Items)
                .AsNoTracking()
                .ToList();
            
            foreach (var futureOrder in futureOneOffOrders)
            {
                var supplier =
                    SupplierHelper.GetSupplier(_companyID, SupplierType.Milk, futureOrder.FromDate.Value);
                
                var weekdayEnable = GetCanBeChanged(futureOrder.FromDate.Value, supplier?.ID);

                var origItems = futureOrder.Items.ToList();

                var toRemoveItems = origItems
                    .Where(x => !latestItems.Select(li => li.MilkProductID).Contains(x.MilkProductID))
                    .ToList();
                var toAddItems = latestItems
                    .Where(x => !origItems.Select(oi => oi.MilkProductID).Contains(x.MilkProductID))
                    .ToList();
                var toAdjustItems = origItems
                    .Where(x => latestItems.Select(li => li.MilkProductID).Contains(x.MilkProductID))
                    .ToList();

                foreach (var toAdjustItem in toAdjustItems)
                {
                    var latestItem = latestItems.Where(x => x.MilkProductID == toAdjustItem.MilkProductID).FirstOrDefault();
                    
                    var trackerRepo = new MilkOneOffChangeTrackerRepository();
                    var tracker = trackerRepo
                        .Read()
                        .Where(x => x.OrderHeaderID == futureOrder.ID)
                        .Where(x => x.MilkProductID == toAdjustItem.MilkProductID)
                        .FirstOrDefault() ?? new MilkOneOffChangeTracker()
                        {
                            Monday = false,
                            Tuesday = false,
                            Wednesday = false,
                            Thursday = false,
                            Friday = false,
                            Saturday = false,
                        };

                    toAdjustItem.Monday = weekdayEnable[DayOfWeek.Monday] && !tracker.Monday ? latestItem.Monday : toAdjustItem.Monday;
                    toAdjustItem.Tuesday = weekdayEnable[DayOfWeek.Tuesday] && !tracker.Tuesday ? latestItem.Tuesday : toAdjustItem.Tuesday;
                    toAdjustItem.Wednesday = weekdayEnable[DayOfWeek.Wednesday] && !tracker.Wednesday ? latestItem.Wednesday : toAdjustItem.Wednesday;
                    toAdjustItem.Thursday = weekdayEnable[DayOfWeek.Thursday] && !tracker.Thursday ? latestItem.Thursday : toAdjustItem.Thursday;
                    toAdjustItem.Friday = weekdayEnable[DayOfWeek.Friday] && !tracker.Friday ? latestItem.Friday : toAdjustItem.Friday;
                    toAdjustItem.Saturday = weekdayEnable[DayOfWeek.Saturday] && !tracker.Saturday ? latestItem.Saturday : toAdjustItem.Saturday;
                    toAdjustItem.CalculateTotal();

                    milkOrderItemRepo.Update(toAdjustItem);
                }

                foreach (var toRemoveItem in toRemoveItems)
                {
                    if (weekdayEnable[DayOfWeek.Monday]
                        && weekdayEnable[DayOfWeek.Tuesday]
                        && weekdayEnable[DayOfWeek.Wednesday]
                        && weekdayEnable[DayOfWeek.Thursday]
                        && weekdayEnable[DayOfWeek.Friday]
                        && weekdayEnable[DayOfWeek.Saturday])
                    {
                        _service.DeleteItem(futureOrder, toRemoveItem.ID);
                    }
                    else
                    {
                        toRemoveItem.Monday = weekdayEnable[DayOfWeek.Monday] ? 0 : toRemoveItem.Monday;
                        toRemoveItem.Tuesday = weekdayEnable[DayOfWeek.Tuesday] ? 0 : toRemoveItem.Tuesday;
                        toRemoveItem.Wednesday = weekdayEnable[DayOfWeek.Wednesday] ? 0 : toRemoveItem.Wednesday;
                        toRemoveItem.Thursday = weekdayEnable[DayOfWeek.Thursday] ? 0 : toRemoveItem.Thursday;
                        toRemoveItem.Friday = weekdayEnable[DayOfWeek.Friday] ? 0 : toRemoveItem.Friday;
                        toRemoveItem.Saturday = weekdayEnable[DayOfWeek.Saturday] ? 0 : toRemoveItem.Saturday;

                        if (toRemoveItem.Monday == 0
                            && toRemoveItem.Tuesday == 0
                            && toRemoveItem.Wednesday == 0
                            && toRemoveItem.Thursday == 0
                            && toRemoveItem.Friday == 0
                            && toRemoveItem.Saturday == 0)
                        {
                            _service.DeleteItem(futureOrder, toRemoveItem.ID);
                        }
                        else
                        {
                            toRemoveItem.CalculateTotal();
                            milkOrderItemRepo.Update(toRemoveItem);
                        }
                    }
                }

                foreach (var toAddItem in toAddItems)
                {
                    var newItem = (MilkOrder)toAddItem.Clone();
                    newItem.ID = Guid.NewGuid();
                    newItem.Monday = weekdayEnable[DayOfWeek.Monday] ? newItem.Monday : 0;
                    newItem.Tuesday = weekdayEnable[DayOfWeek.Tuesday] ? newItem.Tuesday : 0;
                    newItem.Wednesday = weekdayEnable[DayOfWeek.Wednesday] ? newItem.Wednesday : 0;
                    newItem.Thursday = weekdayEnable[DayOfWeek.Thursday] ? newItem.Thursday : 0;
                    newItem.Friday = weekdayEnable[DayOfWeek.Friday] ? newItem.Friday : 0;
                    newItem.Saturday = weekdayEnable[DayOfWeek.Saturday] ? newItem.Saturday : 0;
                    newItem.CalculateTotal();
                    _service.AddItem(futureOrder.ID, newItem);
                }

                _service.CalculateTotal(futureOrder.ID);
            }
        }

        private void UpdateTracker(Guid orderHeaderID)
        {
            var trackerRepo = new MilkOneOffChangeTrackerRepository(_dbContext);
            trackersToCreate.ForEach(x => x.OrderHeaderID = orderHeaderID);
            trackerRepo.Create(trackersToCreate);
            trackerRepo.Update(trackersToUpdate);
        }

        private void DeleteAllOrderItems(MilkOrderHeader orderHeader)
        {
            _service.DeleteAllItems(orderHeader);
        }

        private MilkOrderHeader CreateNewOrder(OrderMode orderMode, DateTime? fromDate)
        {
            var orderHeader = new MilkOrderHeader();

            orderHeader.CompanyID = _companyID;
            orderHeader.OrderMode = orderMode;
            orderHeader.OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff"));
            orderHeader.LatestUpdated = DateTime.Now;
            if (orderMode == OrderMode.OneOff)
            {
                orderHeader.FromDate = fromDate;
                orderHeader.ToDate = fromDate.Value.AddDays(6);
            }
            _service.Create(orderHeader);

            return orderHeader;
        }

        private void AddOrderItems(Guid orderHeaderID, List<MilkOrderViewModel> orderItems)
        {
            foreach (var item in orderItems)
            {
                var product = new MilkProductRepository().Find(Guid.Parse(item.MilkID));
                item.ProductName = product.Name;
                item.Unit = product.Unit;
                var model = _mapper.MapToModel(item);
                _service.AddItem(orderHeaderID, model);
            }
        }

        private Dictionary<DayOfWeek, bool> GetCanBeChanged(DateTime fromDate, Guid? supplierID)
        {
            fromDate = fromDate.Date;
            var toDate = fromDate.AddDays(6);

            var holidays = new HolidayService(_dbContext).ReadSupplierHolidays(supplierID)
                .Where(x => fromDate <= x.Date)
                .Where(x => toDate >= x.Date)
                .Select(x => x.Date)
                .ToList();
            
            var openDays = new SupplierOpenDayRepository(_dbContext).ReadSupplierOpenDays(supplierID)
                .Where(x => fromDate <= x.Date)
                .Where(x => toDate >= x.Date)
                .Select(x => x.Date)
                .ToList();

            var results = new Dictionary<DayOfWeek, bool>();
            var cutOffDate = DateTime.Now.AddHours(CutOffTime.RestTime).Date;

            var dateMon = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Monday);
            var dateTue = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Tuesday);
            var dateWed = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Wednesday);
            var dateThu = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Thursday);
            var dateFri = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Friday);
            var dateSat = fromDate.AddDays(-(int)fromDate.DayOfWeek).AddDays((int)DayOfWeek.Saturday);

            results.Add(DayOfWeek.Monday, cutOffDate < dateMon && (NotHoliday(dateMon) || IsOpenDay(dateMon)));
            results.Add(DayOfWeek.Tuesday, cutOffDate < dateTue && (NotHoliday(dateTue) || IsOpenDay(dateTue)));
            results.Add(DayOfWeek.Wednesday, cutOffDate < dateWed && (NotHoliday(dateWed) || IsOpenDay(dateWed)));
            results.Add(DayOfWeek.Thursday, cutOffDate < dateThu && (NotHoliday(dateThu) || IsOpenDay(dateThu)));
            results.Add(DayOfWeek.Friday, cutOffDate < dateFri && (NotHoliday(dateFri) || IsOpenDay(dateFri)));
            results.Add(DayOfWeek.Saturday, cutOffDate < dateSat && (NotHoliday(dateSat) || IsOpenDay(dateSat)));

            return results;

            bool NotHoliday(DateTime date)
            {
                return !holidays.Contains(date);
            }
            
            bool IsOpenDay(DateTime date)
            {
                return openDays.Contains(date);
            }
        }
    }
}
