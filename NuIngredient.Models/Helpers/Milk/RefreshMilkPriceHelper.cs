﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class RefreshMilkPriceHelper
    {
        private SnapDbContext _db;
        private MilkProduct _milk { get; set; }
        private MilkOrderRepository _milkOrderRepo {get;set;}
        private MilkOrderHeaderRepository _milkOrderHeaderRepo {get;set; }

        private RefreshMilkPriceHelper(SnapDbContext db)
        {
            _db = db;
            _milkOrderRepo = new MilkOrderRepository(db);
            _milkOrderHeaderRepo = new MilkOrderHeaderRepository(db);
        }

        public static RefreshMilkPriceHelper New(SnapDbContext db)
        {
            return new RefreshMilkPriceHelper(db);
        }

        public static RefreshMilkPriceHelper New()
        {
            return new RefreshMilkPriceHelper(new SnapDbContext());
        }

        public void RefreshOrderPrice(Guid milkPdtId, Guid? companyId = null)
        {
            _milk = _db.MilkProducts.Find(milkPdtId);

            var affectedOrders = GetAffectedOrders(companyId).ToList();

            foreach(var affectedOrder in affectedOrders)
            { 
                foreach(var orderItem in affectedOrder.Items)
                {
                    if (orderItem.MilkProductID == _milk.ID)
                    {
                        UpdateItemPrice(orderItem);
                    }
                }

                UpdateOrderHeaderPrice(affectedOrder);
            }

        }

        private IEnumerable<MilkOrderHeader> GetAffectedOrders(Guid? companyId)
        {
            var orders = _milkOrderHeaderRepo
                .Read()
                .Where(h => h.OrderMode == OrderMode.Draft
                         || h.OrderMode == OrderMode.Regular
                         || h.OrderMode == OrderMode.OneOff && h.ToDate >= DateTime.Now)
                .Where(h => h.Depreciated == false)
                .Where(h => h.Items.Any(i => i.MilkProductID == _milk.ID));

            if (companyId != null)
                orders = orders.Where(h => h.CompanyID == companyId.Value);

            return orders;
        }

        private void UpdateItemPrice(MilkOrder orderItem)
        {
            orderItem.Price = _milk.ExclusivePrice(orderItem.OrderHeader.CompanyID);
            orderItem.VATRate = _milk.VAT;
            orderItem.TotalNet = (orderItem.Monday + orderItem.Tuesday + orderItem.Wednesday + orderItem.Thursday + orderItem.Friday + orderItem.Saturday) * orderItem.Price;
            orderItem.TotalVAT = orderItem.TotalNet * orderItem.VATRate;
            _milkOrderRepo.Update(orderItem);
        }

        private void UpdateOrderHeaderPrice(MilkOrderHeader orderHeader)
        {
            orderHeader.TotalNet = 0m;
            orderHeader.TotalVAT = 0m;
            foreach (var item in orderHeader.Items)
            {
                orderHeader.TotalNet += item.TotalNet;
                orderHeader.TotalVAT += item.TotalVAT;
            }
            _milkOrderHeaderRepo.Update(orderHeader);
        }
    }
}
