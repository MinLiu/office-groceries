﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class RefreshMilkPriceByCustomerHelper
    {
        private SnapDbContext _db;
        private Company _company { get; set; }
        private CompanyRepository _companyRepo { get; set; }
        private MilkOrderRepository _milkOrderRepo {get;set;}
        private MilkOrderHeaderRepository _milkOrderHeaderRepo {get;set; }

        private RefreshMilkPriceByCustomerHelper(SnapDbContext db)
        {
            _db = db;
            _companyRepo = new CompanyRepository(db);
            _milkOrderRepo = new MilkOrderRepository(db);
            _milkOrderHeaderRepo = new MilkOrderHeaderRepository(db);
        }

        public static RefreshMilkPriceByCustomerHelper New(SnapDbContext db)
        {
            return new RefreshMilkPriceByCustomerHelper(db);
        }

        public static RefreshMilkPriceByCustomerHelper New()
        {
            return new RefreshMilkPriceByCustomerHelper(new SnapDbContext());
        }

        public void RefreshOrderPrice(Guid companyId)
        {
            _company = _companyRepo.Find(companyId);

            var affectedOrders = GetAffectedOrders().ToList();

            foreach(var affectedOrder in affectedOrders)
            { 
                foreach(var orderItem in affectedOrder.Items)
                {
                    UpdateItemPrice(orderItem);
                }

                UpdateOrderHeaderPrice(affectedOrder);
            }

        }

        private IEnumerable<MilkOrderHeader> GetAffectedOrders()
        {
            var orders = _milkOrderHeaderRepo
                .Read()
                .Where(h => h.CompanyID == _company.ID)
                .Where(h => h.OrderMode == OrderMode.Draft
                         || h.OrderMode == OrderMode.Regular
                         || h.OrderMode == OrderMode.OneOff && h.ToDate >= DateTime.Now);

            return orders;
        }

        private void UpdateItemPrice(MilkOrder orderItem)
        {
            orderItem.Price = orderItem.Milk.ExclusivePrice(orderItem.OrderHeader.CompanyID);
            orderItem.VATRate = orderItem.Milk.VAT;
            orderItem.TotalNet = (orderItem.Monday + orderItem.Tuesday + orderItem.Wednesday + orderItem.Thursday + orderItem.Friday + orderItem.Saturday) * orderItem.Price;
            orderItem.TotalVAT = orderItem.TotalNet * orderItem.VATRate;
            _milkOrderRepo.Update(orderItem);
        }

        private void UpdateOrderHeaderPrice(MilkOrderHeader orderHeader)
        {
            orderHeader.TotalNet = 0m;
            orderHeader.TotalVAT = 0m;
            foreach (var item in orderHeader.Items)
            {
                orderHeader.TotalNet += item.TotalNet;
                orderHeader.TotalVAT += item.TotalVAT;
            }
            _milkOrderHeaderRepo.Update(orderHeader);
        }
    }
}
