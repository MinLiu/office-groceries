﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Models
{
    /// <summary>
    /// Get the next 7 day's orders for suppliers and send them an email
    /// </summary>
    public class DailyOrderReminderHelper
    {
        private readonly DailyOrderReminderConfig _config;

        public DailyOrderReminderHelper()
        {
            _config = new DailyOrderReminderConfig();
        }

        public DailyOrderReminderHelper(DailyOrderReminderConfig config)
        {
            _config = config;
        }

        public bool Execute()
        {
            var suppliers = GetSuppliers();

            var success = true;
            if (_config.SendMilkEmails)
            {
                var milkSuppliers = suppliers
                    .Where(x => x.SupplierType == SupplierType.Milk)
                    .ToList();
                success = success && MilkSuppliers(milkSuppliers);
            }
            
            if (_config.SendFruitEmails)
            {
                var fruitSnackSuppliers = suppliers
                    .Where(x => x.SupplierType == SupplierType.Fruits || x.SupplierType == SupplierType.Snacks)
                    .ToList();
                success = success && FruitSuppliers(fruitSnackSuppliers);
            }

            return success;
        }
        
        /// <summary>
        /// Get a list of suppliers that have daily email reminder enabled
        /// </summary>
        /// <returns></returns>
        private List<Supplier> GetSuppliers()
        {
            var query = new SupplierRepository().Read()
                .Where(x => x.Deleted == false)
                .Where(x => x.DailyEmailReminderEnabled);

            if (_config.IsSupplierSpecified)
            {
                query = query.Where(x => x.ID == _config.SupplierId);
            }
            
            var suppliers = query.ToList();
            return suppliers;
        }

        private bool MilkSuppliers(List<Supplier> suppliers)
        {
            var success = true;
            var dayList = GetDayList();
            
            var sunday = dayList.FirstOrDefault(IsSunday);
            
            foreach (var supplier in suppliers)
            {
                var errorList = new List<string>();
                var result = new List<Entity>();
                var supplierProductCodes = supplier.SupplierMilkProductCodes.ToList();
                var week1Orders = GetMilkOrders(supplier, dayList[0]);
                var week2Orders = GetMilkOrders(supplier, dayList[6]);
                
                foreach (var order in week1Orders)
                {
                    if (!int.TryParse(order.AccountNo?.Trim(), out var accountNo))
                    {
                        errorList.Add($"Account is empty or is not a number: {order.Company.AccountNo} - {order.Company.Name}");
                        continue;
                    }
                    
                    foreach (var item in order.OrderHeader.Items)
                    {
                        foreach (var date in dayList.Where(x => x < sunday))
                        {
                            var quantity = GetQuantityByDate(item, date);
                            
                            // if (quantity == 0) continue;

                            var pdtCode = supplierProductCodes
                                .FirstOrDefault(x => x.MilkProductID == item.MilkProductID)?.Code;

                            if (string.IsNullOrWhiteSpace(pdtCode))
                            {
                                errorList.Add($"Product code not found for product: {item.Milk.Name}, supplier: {supplier.Name}");
                                continue;
                            }
                            
                            result.Add(new Entity
                            {
                                Date = date,
                                ProductCode = pdtCode,
                                CompanyAccountNo = order.AccountNo,
                                CompanyName = order.Company.Name,
                                Quantity = quantity,
                            });
                        }
                    }
                }
                
                foreach (var order in week2Orders)
                {
                    if (!int.TryParse(order.AccountNo?.Trim(), out var accountNo))
                    {
                        errorList.Add($"Account is empty or is not a number: {order.Company.AccountNo} - {order.Company.Name}");
                        continue;
                    }
                    
                    foreach (var item in order.OrderHeader.Items)
                    {
                        foreach (var date in dayList.Where(x => x > sunday))
                        {
                            var quantity = GetQuantityByDate(item, date);
                            
                            // if (quantity == 0) continue;
                            
                            var pdtCode = supplierProductCodes
                                .FirstOrDefault(x => x.MilkProductID == item.MilkProductID)?.Code;

                            if (string.IsNullOrWhiteSpace(pdtCode))
                            {
                                errorList.Add($"Product code not found for product: {item.Milk.Name}, supplier: {supplier.Name}");
                                continue;
                            }
                            
                            result.Add(new Entity
                            {
                                Date = date,
                                ProductCode = pdtCode,
                                CompanyAccountNo = order.AccountNo,
                                CompanyName = order.Company.Name,
                                Quantity = quantity,
                            });
                        }
                    }
                }

                if (errorList.Any())
                {
                    SendValidationEmail(errorList, supplier.Name);
                    success = false;
                    continue;
                }
                
                SendEmail(supplier, result);
                // WriteToFile(result);
                
            }

            return success;
        }
        
        private bool FruitSuppliers(List<Supplier> suppliers)
        {
            var success = true;
            var dayList = GetDayList();
            
            var sunday = dayList.FirstOrDefault(IsSunday);
            
            foreach (var supplier in suppliers)
            {
                var errorList = new List<string>();
                var result = new List<Entity>();
                var supplierProductCodes = supplier.SupplierFruitProductCodes.ToList();
                var week1Orders = GetFruitOrders(supplier, dayList[0]);
                var week2Orders = GetFruitOrders(supplier, dayList[6]);
                
                foreach (var order in week1Orders)
                {
                    if (!int.TryParse(order.AccountNo?.Trim(), out var accountNo))
                    {
                        errorList.Add($"Account is empty or is not a number: {order.Company.AccountNo} - {order.Company.Name}");
                        continue;
                    }
                    
                    foreach (var item in order.OrderHeader.Items)
                    {
                        foreach (var date in dayList.Where(x => x < sunday))
                        {
                            var quantity = GetQuantityByDate(item, date);
                            
                            // if (quantity == 0) continue;
                            
                            var pdtCode = supplierProductCodes
                                .FirstOrDefault(x => x.FruitProductID == item.FruitProductID)?.Code;

                            if (string.IsNullOrWhiteSpace(pdtCode))
                            {
                                errorList.Add($"Product code not found for product: {item.Fruit.Name}, supplier: {supplier.Name}");
                                continue;
                            }
                            
                            result.Add(new Entity
                            {
                                Date = date,
                                ProductCode = pdtCode,
                                CompanyAccountNo = order.AccountNo,
                                CompanyName = order.Company.Name,
                                Quantity = quantity,
                            });
                        }
                    }
                }
                
                foreach (var order in week2Orders)
                {
                    if (!int.TryParse(order.AccountNo?.Trim(), out var accountNo))
                    {
                        errorList.Add($"Account is empty or is not a number: {order.Company.AccountNo} - {order.Company.Name}");
                        continue;
                    }
                    
                    foreach (var item in order.OrderHeader.Items)
                    {
                        foreach (var date in dayList.Where(x => x > sunday))
                        {
                            var quantity = GetQuantityByDate(item, date);
                            
                            // if (quantity == 0) continue;

                            var pdtCode = supplierProductCodes
                                .FirstOrDefault(x => x.FruitProductID == item.FruitProductID)?.Code;

                            if (string.IsNullOrWhiteSpace(pdtCode))
                            {
                                errorList.Add($"Product code not found for product: {item.Fruit.Name}, supplier: {supplier.Name}");
                                continue;
                            }
                            
                            result.Add(new Entity
                            {
                                Date = date,
                                ProductCode = pdtCode,
                                CompanyAccountNo = order.AccountNo,
                                CompanyName = order.Company.Name,
                                Quantity = quantity,
                            });
                        }
                    }
                }

                if (errorList.Any())
                {
                    SendValidationEmail(errorList, supplier.Name);
                    success = false;
                    continue;
                }
                
                SendEmail(supplier, result);
                // WriteToFile(result);
                
            }

            return success;
        }

        /// <summary>
        /// Get a list of days from tomorrow to the next 7 days
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        private List<DateTime> GetDayList(int days = 7)
        {
            return Enumerable.Range(1, days)
                .Select(day => DateTime.Now.AddDays(day))
                .ToList();
        }

        /// <summary>
        /// Check if the date is Sunday
        /// </summary>
        private bool IsSunday(DateTime dateTime)
        {
            return dateTime.DayOfWeek == DayOfWeek.Sunday;
        }
        
        /// <summary>
        /// Get the quantity of the order item by date
        /// </summary>
        private int GetQuantityByDate(MilkOrder orderItem, DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;
            var func = _dayDictionary[dayOfWeek];
            return func(orderItem);
        }

        /// <summary>
        /// A dictionary of day of week and the function to get the quantity of the order item
        /// </summary>
        private readonly Dictionary<DayOfWeek, Func<MilkOrder, int>> _dayDictionary =
            new Dictionary<DayOfWeek, Func<MilkOrder, int>>()
            {
                { DayOfWeek.Monday, x => x.Monday },
                { DayOfWeek.Tuesday, x => x.Tuesday },
                { DayOfWeek.Wednesday, x => x.Wednesday },
                { DayOfWeek.Thursday, x => x.Thursday },
                { DayOfWeek.Friday, x => x.Friday },
                { DayOfWeek.Saturday, x => x.Saturday },
                { DayOfWeek.Sunday, x => x.Sunday },
            };
        
        /// <summary>
        /// Get the quantity of the order item by date
        /// </summary>
        private int GetQuantityByDate(FruitOrder orderItem, DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;
            var func = _dayFruitDictionary[dayOfWeek];
            return func(orderItem);
        }
        
        /// <summary>
        /// A dictionary of day of week and the function to get the quantity of the order item
        /// </summary>
        private readonly Dictionary<DayOfWeek, Func<FruitOrder, int>> _dayFruitDictionary =
            new Dictionary<DayOfWeek, Func<FruitOrder, int>>()
            {
                { DayOfWeek.Monday, x => x.Monday },
                { DayOfWeek.Tuesday, x => x.Tuesday },
                { DayOfWeek.Wednesday, x => x.Wednesday },
                { DayOfWeek.Thursday, x => x.Thursday },
                { DayOfWeek.Friday, x => x.Friday },
                { DayOfWeek.Saturday, x => x.Saturday },
                { DayOfWeek.Sunday, x => x.Sunday },
            };
        
        /// <summary>
        /// Get the milk orders for the supplier
        /// </summary>
        private List<CompanyMilkOrder> GetMilkOrders(Supplier supplier, DateTime date)
        {
            var query = new CompanyRepository().Read()
                .Where(c => c.DeleteData == false)
                .Where(x => x.CompanySuppliers.Any(c => c.SupplierID == supplier.ID && (c.EndDate == null || c.EndDate > DateTime.Now)))
                .Include(c => c.CompanySuppliers);
            
            var companyList = query.ToList();
            var activeMilk = companyList.Where(ActiveOnMilk).ToList();
            
            var monday = date.StartOfWeek(DayOfWeek.Monday);
            var service = new MilkOrderService<MilkOrderHeaderViewModel>(null, new SnapDbContext());
            var group = new List<GP>();
            foreach (var company in activeMilk)
            {
                var companySupplier = company.SupplierWithAccountNo(SupplierType.Milk, monday).FirstOrDefault();
                if (companySupplier == null) continue;
                if (companySupplier.SupplierID != supplier.ID) continue;

                var orderNextWeek = service.GetWeeklyOrderNoTracking(OrderMode.OneOff, company.ID, monday);
                group.Add(new GP(company, companySupplier.Supplier, companySupplier.AccountNo, orderNextWeek));
            }
            
            var shit = group.Select(x => new CompanyMilkOrder(x.MilkOrderHeader, x.AccountNo, x.Company)).ToList();
            return shit;
        }
        
        /// <summary>
        /// Get the fruit orders for the supplier
        /// </summary>
        private List<CompanyFruitOrder> GetFruitOrders(Supplier supplier, DateTime date)
        {
            var query = new CompanyRepository().Read()
                .Where(c => c.DeleteData == false)
                .Where(x => x.CompanySuppliers.Any(c => c.SupplierID == supplier.ID && (c.EndDate == null || c.EndDate > DateTime.Now)))
                .Include(c => c.CompanySuppliers);
            
            var companyList = query.ToList();
            var activeFruit = companyList.Where(ActiveOnFruitOrSnack).ToList();
            
            var monday = date.StartOfWeek(DayOfWeek.Monday);
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(null, new SnapDbContext());
            var group = new List<GpFruit>();
            foreach (var company in activeFruit)
            {
                var companySupplier = company.SupplierWithAccountNo(SupplierType.Fruits, monday).FirstOrDefault();
                if (companySupplier == null) continue;
                if (companySupplier.SupplierID != supplier.ID) continue;

                var orderNextWeek = service.GetWeeklyOrderAsNoTracking(OrderMode.OneOff, company.ID, monday);
                group.Add(new GpFruit(company, companySupplier.Supplier, companySupplier.AccountNo, orderNextWeek));
            }
            
            var shit = group.Select(x => new CompanyFruitOrder(x.FruitOrderHeader, x.AccountNo, x.Company)).ToList();
            return shit;
        }
        
        private void WriteToFile(List<Entity> entities)
        {
            var orderedEntities = entities.OrderBy(x => x.CompanyAccountNo)
                .ThenBy(x => x.Date)
                .ToList();

            var fileContent = CreateFileContent(orderedEntities);

            var filePath = "D:\\WorkSpace\\office-groceries\\output2.csv";
            File.WriteAllText(filePath, fileContent);
        }

        private string CreateFileContent(List<Entity> orderedEntities)
        {
            var stringBuilder = new StringBuilder();

            foreach (var entity in orderedEntities)
            {
                CreateEntityRecord(stringBuilder, entity);
            }

            return stringBuilder.ToString();
        }

        private void CreateEntityRecord(StringBuilder stringBuilder, Entity entity)
        {
            stringBuilder
                .Append($"{entity.Date:ddMMyyyy},")
                .Append($"{entity.CompanyAccountNo?.Replace(",", " ").Trim()},")
                .Append($"{entity.CompanyName?.Replace(",", " ").Trim()},")
                .Append($"{entity.ProductCode},")
                .AppendLine($"{entity.Quantity},");
        }
        
        private static bool ActiveOnMilk(Company company)
        {
            // find the companies if it's active next week
            return company.IsLiveWeek(SupplierType.Milk, DateTime.Now.AddDays(7));
        }

        private static bool ActiveOnFruitOrSnack(Company company)
        {
            // find the companies if it's active next week
            return company.IsLiveWeek(SupplierType.Fruits, DateTime.Now.AddDays(7)) || 
                   company.IsLiveWeek(SupplierType.Snacks, DateTime.Now.AddDays(7));
        }
        
        private void SendErrorEmail(Exception e, string supplierName)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add("min.liu@zigaflow.com");
            mailMessage.From = new MailAddress("orders@office-groceries.com");
            mailMessage.Subject = $"Failed to Generate Daily Order Reminder from Office Groceries";
            mailMessage.Body = $"Supplier: {supplierName}<br />" +
                               $"Message: {e.Message}<br/>" +
                               $"StackTrace: {e.StackTrace}";
            mailMessage.IsBodyHtml = true;
            SendEmail(mailMessage);
        }
        
        private void SendValidationEmail(List<string> errors, string supplierName)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add("dustin@office-groceries.com");
            mailMessage.To.Add("ben@office-groceries.com");
            mailMessage.From = new MailAddress("orders@office-groceries.com");
            mailMessage.Subject = $"Failed to Generate Daily Order Reminder for {supplierName} from Office Groceries";
            mailMessage.Body = $"Supplier: {supplierName}<br />" +
                               string.Join("<br/>", errors.Distinct().OrderBy(x => x));
            mailMessage.IsBodyHtml = true;
            SendEmail(mailMessage);
        }

        private bool SendEmail(Supplier supplier, List<Entity> entities)
        {
            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("orders@office-groceries.com");
            mailMessage.AddToEmails(supplier.Email);
            // mailMessage.To.Add("min.liu@zigaflow.com");
            // mailMessage.To.Add("dustin@office-groceries.com");
            mailMessage.Subject = $"Order Reminder from Office Groceries";
            mailMessage.Attachments.Add(new Attachment(CreateFile(entities), "order.txt"));
            return SendEmail(mailMessage);
        }

        private bool SendEmail(MailMessage mailMessage)
        {
            try
            {
                using (var context = new SnapDbContext())
                {
                    var emailer = new Fruitful.Email.Emailer(context);
                    emailer.SendEmail(mailMessage);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        
        private Stream CreateFile(List<Entity> entities)
        {
            var orderedEntities = entities.OrderBy(x => x.CompanyAccountNo)
                .ThenBy(x => x.Date)
                .ToList();

            var fileContent = CreateFileContent(orderedEntities);

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(fileContent);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        
        private class GP
        {
            public Company Company { get; set; }
            public Supplier Supplier { get; set; }
            public string AccountNo { get; set; }
            public MilkOrderHeader MilkOrderHeader { get; set; }

            public GP(Company company, Supplier supplier, string accountNo, MilkOrderHeader milkOrderHeader)
            {
                Company = company;
                Supplier = supplier;
                AccountNo = accountNo;
                MilkOrderHeader = milkOrderHeader;
            }
        }
        
        private class GpFruit
        {
            public Company Company { get; set; }
            public Supplier Supplier { get; set; }
            public string AccountNo { get; set; }
            public FruitOrderHeader FruitOrderHeader { get; set; }

            public GpFruit(Company company, Supplier supplier, string accountNo, FruitOrderHeader fruitOrderHeader)
            {
                Company = company;
                Supplier = supplier;
                AccountNo = accountNo;
                FruitOrderHeader = fruitOrderHeader;
            }
        }

        public class DailyOrderReminderConfig
        {
            public Guid? SupplierId { get; set; }
            public bool IsSupplierSpecified => SupplierId != null;
            public bool SendMilkEmails { get; set; } = true;
            public bool SendFruitEmails { get; set; } = true;
        }
        
        private struct Entity
        {
            public DateTime Date { get; set; }
            public string ProductCode { get; set; }
            public string CompanyAccountNo { get; set; }
            public string CompanyName { get; set; }
            public int Quantity { get; set; }
        }
    }
}