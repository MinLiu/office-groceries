﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class IsCompanyDeletableHelper
    {
        private const int VALID_MONTH_AGO = -2;
        private SnapDbContext _db;
        private MilkOrderHeaderRepository _milkOrderHeaderRepo {get;set; }
        private FruitOrderHeaderRepository _fruitOrderHeaderRepo {get;set; }
        private OneOffOrderRepository _oneOffOrderRepo {get;set; }
        private CompanySupplierRepository _companySupplierRepo {get;set; }

        private IsCompanyDeletableHelper(SnapDbContext db)
        {
            _db = db;
            _milkOrderHeaderRepo = new MilkOrderHeaderRepository(db);
            _fruitOrderHeaderRepo = new FruitOrderHeaderRepository(db);
            _oneOffOrderRepo = new OneOffOrderRepository(db);
            _companySupplierRepo = new CompanySupplierRepository(db);
        }

        public static IsCompanyDeletableHelper New(SnapDbContext db)
        {
            return new IsCompanyDeletableHelper(db);
        }

        public static IsCompanyDeletableHelper New()
        {
            return new IsCompanyDeletableHelper(new SnapDbContext());
        }

        public IsCompanyDeletableResult IsDeletable(Guid companyID)
        {
            var withinDate = DateTime.Today.AddMonths(VALID_MONTH_AGO);

            var result = new IsCompanyDeletableResult();
            try
            { 
                if (IsAnyMilkOrdersWithinDate(companyID, withinDate))
                {
                    result.IsDeletable = false;
                    result.Message += "\nThere are still milk orders in 2 past months.";
                }

                if (IsAnyFruitOrdersWithinDate(companyID, withinDate))
                {
                    result.IsDeletable = false;
                    result.Message += "\nThere are still fruit orders in 2 past months.";
                }

                if (IsAnyDryGoodsOrderWithinDate(companyID, withinDate))
                {
                    result.IsDeletable = false;
                    result.Message += "\nThere are still dry goods orders in 2 past months.";
                }

                if (IsAnyActiveSupplier(companyID))
                {
                    result.IsDeletable = false;
                    result.Message += "\nThere are still active suppliers.";
                }
            }
            catch(Exception e)
            {
                result.IsDeletable = false;
                result.Message += "\n" + e.Message;
            }

            return result;
        }

        private bool IsAnyMilkOrdersWithinDate(Guid companyID, DateTime withinDate)
        {
            var query = _milkOrderHeaderRepo
                .Read()
                .Where(x => x.CompanyID == companyID)
                .Where(x => x.OrderMode == OrderMode.History)
                .Where(x => x.FromDate > withinDate);

            return query.Any();
        }

        private bool IsAnyFruitOrdersWithinDate(Guid companyID, DateTime withinDate)
        {
            var query = _fruitOrderHeaderRepo
                .Read()
                .Where(x => x.CompanyID == companyID)
                .Where(x => x.OrderMode == OrderMode.History)
                .Where(x => x.FromDate > withinDate);

            return query.Any();
        }

        private bool IsAnyDryGoodsOrderWithinDate(Guid companyID, DateTime withinDate)
        {
            var query = _oneOffOrderRepo
                .Read()
                .Where(x => x.CompanyID == companyID)
                .Where(x => x.Created > withinDate);

            return query.Any();
        }

        private bool IsAnyActiveSupplier(Guid companyID)
        {
            var today = DateTime.Today;

            var query = _companySupplierRepo
                .Read()
                .Where(x => x.CompanyID == companyID)
                .Where(x => x.EndDate == null || x.EndDate >= today);

            return query.Any();
        }
    }

    public class IsCompanyDeletableResult
    {
        public bool IsDeletable { get; set; }
        public string Message { get; set; }
        public IsCompanyDeletableResult()
        {
            IsDeletable = true;
            Message = "Can't delete.";
        }
    }

}
