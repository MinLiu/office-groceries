using System;
using System.Linq;

namespace NuIngredient.Models.Helpers
{
    public class FreezeSettingHelper
    {
        private static FrozenSettingResult _cachedFreezeSetting;
        private static DateTime _cachedTime;

        public FrozenSettingResult IsFrozenByAdmin()
        {
            if (_cachedFreezeSetting != null && _cachedTime.AddMinutes(10) > DateTime.Now)
            {
                return _cachedFreezeSetting;
            }

            using (var db = new SnapDbContext())
            {
                _cachedTime = DateTime.Now;
                
                var setting = db.Settings.FirstOrDefault();
                
                if (setting == null)
                {
                    _cachedFreezeSetting = new FrozenSettingResult(false);
                    return _cachedFreezeSetting;
                }
                
                if (setting.IsFreezeActivated)
                {
                    if (DateTime.Now >= setting.FreezeStartDate && DateTime.Now <= setting.FreezeEndDate)
                    {
                        _cachedFreezeSetting = new FrozenSettingResult(true, setting.FreezeMessage);
                        return _cachedFreezeSetting;
                    }
                }
                
                _cachedFreezeSetting = new FrozenSettingResult(false);
                return _cachedFreezeSetting;
            }
        }

        public class FrozenSettingResult
        {
            public FrozenSettingResult(bool isFrozen, string message = "")
            {
                IsFrozen = isFrozen;
                Message = message;
            }

            public bool IsFrozen { get; set; }
            public string Message { get; set; }
        }
    }
}