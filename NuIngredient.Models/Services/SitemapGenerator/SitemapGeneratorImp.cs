﻿using NuIngredient.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace Boilerplate.Web.Mvc.Sitemap
{
    /// <summary>
    /// Generates sitemap XML.
    /// </summary>
    public class SitemapGeneratorImp : SitemapGenerator
    {
        private List<string> _sitemapUrls = new List<string>();
        private static Uri BaseUri = new Uri(ConfigurationManager.AppSettings["BaseURL"]);

        protected override string GetSitemapUrl(int index)
        {
            return _sitemapUrls[index];
        }

        public List<XDocument> Generate()
        {
            InitializeSitemapUrls();
            var sitemapNodes = GetSitemapNodes();
            return GetSitemapDocuments(sitemapNodes);
        }

        protected void InitializeSitemapUrls()
        {
            AddDefaultPages();
            AddAislePages();
            AddDryGoodsProducts();
            AddDeliveryAreas();
        }

        protected List<SitemapNode> GetSitemapNodes()
        {
            var sitemapNodes = new List<SitemapNode>();
            foreach (var sitemapUrl in _sitemapUrls)
            {
                sitemapNodes.Add(new SitemapNode(sitemapUrl));
            }
            return sitemapNodes;
        }

        protected void AddDefaultPages()
        {
            _sitemapUrls.Add(MapUrlPath(""));
            _sitemapUrls.Add(MapUrlPath("/FAQs"));
            _sitemapUrls.Add(MapUrlPath("/Process"));
            _sitemapUrls.Add(MapUrlPath("/About"));
            _sitemapUrls.Add(MapUrlPath("/Account/Login"));
            _sitemapUrls.Add(MapUrlPath("/Account/EnquiryForm"));
        }

        protected void AddAislePages()
        {
            var repo = new AisleRepository();
            var mapper = new AisleMapper();
            var aisles = repo.Read().Where(x => x.StartDate <= DateTime.Now)
                                    .Where(x => x.EndDate == null || x.EndDate >= DateTime.Now)
                                    .OrderBy(x => x.SortPosition)
                                    .ToList()
                                    .Select(x => mapper.MapToViewModel(x))
                                    .ToList();
            foreach(var aisle in aisles)
            {
                if (aisle.Name == "Milk")
                    _sitemapUrls.Add(MapUrlPath("office-milk-deliveries"));
                else if (aisle.Name == "Fruit")
                    _sitemapUrls.Add(MapUrlPath("office-fruit-basket-deliveries"));
                else
                    _sitemapUrls.Add(MapUrlPath("Aisle/" + aisle.InternalName));
            }
        }
        
        protected void AddDryGoodsProducts()
        {
            var service = new ProductService<ProductViewModel>(new ProductMapper());
            var products = service.Read();
            foreach(var product in products)
            {
                //var parameters = HttpUtility.ParseQueryString(string.Empty);
                //parameters["id"] = product.Name;
                //_sitemapUrls.Add(MapUrlPath("Products/Detail", parameters));
                _sitemapUrls.Add(MapUrlPath("Products/Detail/" + product.InternalName));
            }
        }
        
        private void AddDeliveryAreas()
        {
            foreach(var location in AppSettings.DeliveryAreas)
            {
                _sitemapUrls.Add(MapUrlPath("delivery-areas/" + location));
            }
        }

        private string MapUrlPath(string path, NameValueCollection parameters = null)
        {
            var uriBuilder = new UriBuilder(BaseUri);
            uriBuilder.Path += path;
            if (parameters != null) uriBuilder.Query = parameters.ToString();
            return uriBuilder.Uri.AbsoluteUri;
        }
    }
}