﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class MilkOrderService<TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        protected SnapDbContext _db;
        protected ModelMapper<MilkOrderHeader, TViewModel> _mapper;
        protected MilkOrderHeaderRepository _repo;
        public MilkOrderService(ModelMapper<MilkOrderHeader, TViewModel> mapper, SnapDbContext db)
        {
            _mapper = mapper;
            _db = db;
            _repo = new MilkOrderHeaderRepository(_db);
        }

        public List<MilkOrder> GetWeeklyOrderItems(OrderMode orderMode, Guid? companyID, string prospectToken, DateTime? fromDate, bool ignoreHolidays = false)
        {
            var orderHeader = GetWeeklyOrder(orderMode, companyID, prospectToken, fromDate, depreciated: false, ignoreHolidays);

            if (orderHeader == null)
                return new List<MilkOrder>();
            else
                return orderHeader.Items.ToList();
        }

        public MilkOrderHeader GetWeeklyOrder(OrderMode orderMode, Guid? companyID, string prospectToken, DateTime? fromDate, bool depreciated = false, bool ignoreHolidays = false)
        {
            MilkOrderHeader milkOrder;
            if (companyID != null)
            {
                if (orderMode == OrderMode.Regular && depreciated)
                {
                    milkOrder = _repo.Read(true)
                        .Where(f => f.CompanyID == companyID
                                    && f.ProspectToken == null
                                    && f.OrderMode == orderMode
                                    && f.Depreciated == true)
                        .Include(f => f.Items)
                        .FirstOrDefault();

                    return milkOrder;
                }
                
                milkOrder = _repo.Read()
                                 .Where(f => f.CompanyID == companyID
                                             && f.ProspectToken == null
                                             && f.OrderMode == orderMode
                                             && f.FromDate == fromDate)
                                 .Include(f => f.Items)
                                 .FirstOrDefault();

                // Default with regular order
                if (orderMode == OrderMode.OneOff && milkOrder == null)
                {
                    milkOrder = _repo.Read()
                                     .Where(f => f.CompanyID == companyID
                                             && f.ProspectToken == null
                                             && f.OrderMode == OrderMode.Regular)
                                     .AsNoTracking()
                                     .Include(f => f.Items)
                                     .FirstOrDefault();
                }
            }
            else
            {
                // Prospects
                // Get Orders
                milkOrder = _repo.Read()
                                 .Where(f => f.ProspectToken == prospectToken
                                          && f.OrderMode == orderMode
                                          && f.FromDate == fromDate)
                                 .Include(f => f.Items)
                                 .FirstOrDefault();
            }

            if (orderMode == OrderMode.OneOff &&
                ignoreHolidays == false)
            {
                var supplierId = companyID != null
                    ? SupplierHelper.GetSupplier(companyID.Value, SupplierType.Milk, fromDate.Value)?.ID
                    : null;
                SetNonDeliveryOnHolidays(milkOrder, fromDate.Value, supplierId);
            }

            return milkOrder;
        }

        public MilkOrderHeader GetWeeklyOrderNoTracking(OrderMode orderMode, Guid companyID, DateTime? fromDate)
        {
            fromDate = fromDate?.Date;

            var milkOrder = _repo.Read()
                .Where(f => f.CompanyID == companyID
                            && f.ProspectToken == null
                            && f.OrderMode == orderMode
                            && f.FromDate == fromDate)
                .Include(f => f.Items)
                .AsNoTracking()
                .FirstOrDefault();

            // Default with regular order
            if (orderMode == OrderMode.OneOff && milkOrder == null)
            {
                milkOrder = _repo.Read()
                    .Where(f => f.CompanyID == companyID
                                && f.ProspectToken == null
                                && f.OrderMode == OrderMode.Regular)
                    .Include(f => f.Items)
                    .AsNoTracking()
                    .FirstOrDefault();
            }

            if (orderMode == OrderMode.OneOff)
            {
                var supplierId = SupplierHelper.GetSupplier(companyID, SupplierType.Milk, fromDate.Value)?.ID;
                SetNonDeliveryOnHolidays(milkOrder, fromDate.Value, supplierId);
            }

            return milkOrder;
        }

        public MilkOrderHeader GetProspectOrder(string prospectToken)
        {
            return GetWeeklyOrder(OrderMode.Draft, null, prospectToken, null);
        }

        public MilkOrderHeader GetCustomerOrder(Guid companyID, OrderMode orderMode, DateTime? fromDate, bool depreciated = false, bool ignoreHolidays = false)
        {
            return GetWeeklyOrder(orderMode, companyID, null, fromDate, depreciated, ignoreHolidays);
        }

        public MilkOrderHeader GetOrderByID(Guid ID)
        {
            var order = _repo.Read()
                .Where(f => f.ID == ID)
                .Include(f => f.Items)
                .FirstOrDefault();
            return order;
        }

        public List<MilkOrder> GetProspectOrderItems(string prospectToken)
        {
            return GetWeeklyOrderItems(OrderMode.Draft, null, prospectToken, null);
        }

        public List<MilkOrder> GetCustomerOrderItems(Guid companyID, OrderMode orderMode, DateTime? fromDate)
        {
            return GetWeeklyOrderItems(orderMode, companyID, null, fromDate);
        }

        public MilkOrderHeader DeleteOrder(MilkOrderHeader orderHeader)
        {
            _repo.Delete(orderHeader);
            return orderHeader;
        }

        public MilkOrderHeader DeleteItem(MilkOrderHeader orderHeader, Guid milkOrderID)
        {
            // Delete product
            var repo = new MilkOrderRepository(_db);
            var item = repo.Read().Where(x => x.ID == milkOrderID).FirstOrDefault();

            // Delete change tracker
            var trackerRepo = new MilkOneOffChangeTrackerRepository(_db);
            var trackers = trackerRepo.Read()
                .Where(x => x.OrderHeaderID == orderHeader.ID)
                .Where(x => x.MilkProductID == item.MilkProductID);

            repo.Delete(item);
            trackerRepo.Delete(trackers);

            return orderHeader;
        }

        public MilkOrderHeader DeleteAllItems(MilkOrderHeader orderHeader)
        {
            var repo = new MilkOrderRepository(_db);
            var items = repo.Read().Where(x => x.OrderHeaderID == orderHeader.ID);
            repo.Delete(items);
            return orderHeader;
        }

        public MilkOrder AddItem(Guid orderHeaderID, MilkOrder item)
        {
            var repo = new MilkOrderRepository(_db);

            var lastSortPos = repo.Read()
                .Where(x => x.OrderHeaderID == orderHeaderID)
                .Select(x => x.SortPos)
                .ToList()
                .OrderByDescending(x => x)
                .DefaultIfEmpty(-1)
                .FirstOrDefault();

            item.OrderHeaderID = orderHeaderID;
            item.SortPos = ++lastSortPos;

            repo.Create(item);

            return item;
        }

        public MilkOrder AddItem(Guid orderHeaderID, MilkProduct product)
        {
            var milkorder = new MilkOrder()
            {
                ProductName = product.Name,
                Unit = product.Unit,
                Monday = 0,
                Tuesday = 0,
                Wednesday = 0,
                Thursday = 0,
                Friday = 0,
                Saturday = 0,
                Sunday = 0,
                MilkProductID = product.ID,
                VATRate = product.VAT,
                Price = product.Price,
                TotalNet = 0,
                TotalVAT = 0,
            };
            return AddItem(orderHeaderID, milkorder);
        }

        public MilkOrderHeader CalculateTotal(Guid ID)
        {
            var orderHeader = _repo.Find(ID);
            return CalculateTotal(orderHeader);
        }

        public MilkOrderHeader CalculateTotal(MilkOrderHeader orderHeader)
        {
            orderHeader.TotalNet = 0m;
            orderHeader.TotalVAT = 0m;
            
            if (orderHeader.Items != null)
            {
                foreach (var item in orderHeader.Items)
                {
                    orderHeader.TotalNet += item.TotalNet;
                    orderHeader.TotalVAT += item.TotalVAT;
                }
            }
            
            orderHeader = Update(orderHeader);
            return orderHeader;
        }

        private void SetNonDeliveryOnHolidays(MilkOrderHeader orderHeader, DateTime orderFromDate, Guid? supplierId)
        {
            var orderToDate = orderFromDate.AddDays(6);
            var holidays = new HolidayService(_db).ReadSupplierHolidays(supplierId)
                .Where(x => orderFromDate <= x.Date && x.Date <= orderToDate)
                .ToList()
                .Select(x => x.Date.Date)
                .ToList();
            
            var openDays = new SupplierOpenDayRepository(_db).ReadSupplierOpenDays(supplierId)
                .Where(x => orderFromDate <= x.Date && x.Date <= orderToDate)
                .ToList()
                .Select(x => x.Date.Date)
                .ToList();
            
            var mon = orderFromDate.Date;
            var tue = orderFromDate.Date.AddDays(1);
            var wed = orderFromDate.Date.AddDays(2);
            var thu = orderFromDate.Date.AddDays(3);
            var fri = orderFromDate.Date.AddDays(4);
            var sat = orderFromDate.Date.AddDays(5);
            if (holidays.Any() && orderHeader?.Items != null)
            {
                foreach(var orderItem in orderHeader.Items)
                {
                    foreach (var holiday in holidays.Where(x => !openDays.Contains(x)))
                    {
                        if (holiday == mon)
                        {
                            orderItem.Monday = 0;
                        }
                        else if (holiday == tue)
                        {
                            orderItem.Tuesday = 0;
                        }
                        else if (holiday == wed)
                        {
                            orderItem.Wednesday = 0;
                        }
                        else if (holiday == thu)
                        {
                            orderItem.Thursday = 0;
                        }
                        else if (holiday == fri)
                        {
                            orderItem.Friday = 0;
                        }
                        else if (holiday == sat)
                        {
                            orderItem.Saturday = 0;
                        }
                    }

                    orderItem.CalculateTotal();
                }

                orderHeader.CalculateTotal();
            }
        }

        public MilkOrderHeader Update(MilkOrderHeader model)
        {
            _repo.Update(model);
            return model;
        }

        public MilkOrderHeader Create(MilkOrderHeader model)
        {
            _repo.Create(model);
            return model;
        }

        public TViewModel MapToViewModel(MilkOrderHeader model)
        {
            return _mapper.MapToViewModel(model);
        }

    }
}
