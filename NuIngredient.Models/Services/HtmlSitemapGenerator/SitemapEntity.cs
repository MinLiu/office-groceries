﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models.Services.HtmlSitemapGenerator
{
    public class SitemapEntity
    {
        public string Url { get; set; }
        public string Label { get; set; }
        public SitemapEntity()
        {

        }

        public SitemapEntity(string label, string url)
        {
            Label = label;
            Url = url;
        }
    }
}
