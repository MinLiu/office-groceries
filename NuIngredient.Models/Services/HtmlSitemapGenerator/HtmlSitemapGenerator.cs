﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models.Services.HtmlSitemapGenerator
{
    public class HtmlSitemapGenerator
    {
        private SitemapPageViewModel _sitemapPageViewModel { get; set; }

        private HtmlSitemapGenerator()
        {
            _sitemapPageViewModel = new SitemapPageViewModel();
        }

        public static HtmlSitemapGenerator Initialize()
        {
            return new HtmlSitemapGenerator();
        }

        public SitemapPageViewModel Generate()
        {
            AddPages();

            return _sitemapPageViewModel;
        }

        private void AddPages()
        {
            AddHomePage();
            AddFAQs();
            AddProcess();
            AddAbout();
            AddTestimonials();
            AddLogin();
            AddEnquiryForm();
            AddDeliveryAreas();
            AddAisles();
        }

        private void AddHomePage()
        {
            var page = new SitemapContainer()
            {
                Heading = new SitemapEntity("Home", "/")
            };
            AddToSitemap(page);
        }

        private void AddFAQs()
        {
            var page = new SitemapContainer()
            {
                Heading = new SitemapEntity("FAQs", "/FAQs")
            };
            AddToSitemap(page);
        }

        private void AddProcess()
        {
            var page = new SitemapContainer()
            {
                Heading = new SitemapEntity("Process", "/Process")
            };
            AddToSitemap(page);
        }

        private void AddAbout()
        {
            var page = new SitemapContainer()
            {
                Heading = new SitemapEntity("About", "/About")
            };
            AddToSitemap(page);
        }

        private void AddTestimonials()
        {
            var page = new SitemapContainer()
            {
                Heading = new SitemapEntity("Testimonials", "/Testimonials")
            };
            AddToSitemap(page);
        }

        private void AddLogin()
        {
            var page = new SitemapContainer()
            {
                Heading = new SitemapEntity("Login", "/Account/Login")
            };
            AddToSitemap(page);
        }

        private void AddEnquiryForm()
        {
            var page = new SitemapContainer()
            {
                Heading = new SitemapEntity("Enquiry Form", "/Account/EnquiryForm")
            };
            AddToSitemap(page);
        }

        private void AddAisles()
        {
            var repo = new AisleRepository();
            var aisles = repo.Read().Where(x => x.StartDate <= DateTime.Now)
                                    .Where(x => x.EndDate == null || x.EndDate >= DateTime.Now)
                                    .OrderBy(x => x.SortPosition)
                                    .ToList();

            foreach(var aisle in aisles)
            {
                if (aisle.Name == "Milk")
                {
                    var mainPage = new SitemapContainer()
                    {
                        Heading = new SitemapEntity(aisle.Label, "/office-milk-deliveries"),
                    };

                    var categories = new MilkProductCategoryRepository().Read().Where(x => x.Deleted == false && x.ParentID == null).OrderBy(x => x.SortPos).ThenBy(x => x.Name).ToList();
                    foreach (var category in categories)
                    {
                        mainPage.SubSitemaps.Add(new SitemapContainer()
                        {
                            Heading = new SitemapEntity(category.Name, "/office-milk-deliveries")
                        });
                    }
                    AddToSitemap(mainPage);
                }
                else if (aisle.Name == "Fruit")
                {
                    var mainPage = new SitemapContainer()
                    {
                        Heading = new SitemapEntity(aisle.Label, "/office-fruit-basket-deliveries"),
                    };
                    var categories = new FruitProductCategoryRepository().Read().Where(x => x.Deleted == false && x.ParentID == null).OrderBy(x => x.SortPos).ThenBy(x => x.Name).ToList();
                    foreach (var category in categories)
                    {
                        mainPage.SubSitemaps.Add(new SitemapContainer()
                        {
                            Heading = new SitemapEntity(category.Name, "/office-fruit-basket-deliveries")
                        });
                    }
                    AddToSitemap(mainPage);
                }
                else
                {
                    var mainPage = new SitemapContainer()
                    {
                        Heading = new SitemapEntity(aisle.Label, String.Format("/Aisle/{0}", aisle.InternalName)),
                    };

                    var pdtCatService = new ProductCategoryService<ProductCategoryViewModel>(null);
                    var categories = pdtCatService.GetAllTopCategories(aisle.ID);
                    foreach(var category in categories)
                    {
                        mainPage.SubSitemaps.Add(new SitemapContainer()
                        {
                            Heading = new SitemapEntity(category.Name, String.Format("/Aisle/{0}/{1}", aisle.InternalName, category.InternalName))
                        });
                    }
                    AddToSitemap(mainPage);
                }
            }
        }
        
        private void AddDeliveryAreas()
        {
            var mainPage = new SitemapContainer()
            {
                Heading = new SitemapEntity("Delivery Areas", "/delivery-areas"),
            };

            mainPage.SubSitemaps.AddRange(AppSettings.DeliveryAreas.Select(location => new SitemapContainer()
            {
                Heading = new SitemapEntity(location, $"/delivery-areas/{location}")

            }));
                
            AddToSitemap(mainPage);
        }

        private void AddToSitemap(SitemapContainer page)
        {
            _sitemapPageViewModel.Sitemaps.Add(page);
        }
    }
}
