﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models.Services.HtmlSitemapGenerator
{
    public class SitemapContainer
    {
        public SitemapEntity Heading { get; set; }
        public List<SitemapContainer> SubSitemaps { get; set; }
        public SitemapContainer()
        {
            Heading = new SitemapEntity();
            SubSitemaps = new List<SitemapContainer>();
        }
    }
}
