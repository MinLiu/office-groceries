﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models.Services.HtmlSitemapGenerator
{
    public class SitemapPageViewModel
    {
        public List<SitemapContainer> Sitemaps { get; set; }

        public SitemapPageViewModel()
        {
            Sitemaps = new List<SitemapContainer>();
        }
    }
}
