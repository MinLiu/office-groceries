﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class HolidayService
    {
        protected SnapDbContext _db;
        protected HolidayRepository _repo;
        public HolidayService(SnapDbContext db)
        {
            _db = db;
            _repo = new HolidayRepository(_db);
        }

        public IQueryable<Holiday> Read()
        {
            return _repo.Read();
        }

        public IQueryable<Holiday> ReadSupplierHolidays(Guid? supplierID)
        {
            return _repo.Read().Where(x => x.SupplierID == supplierID || x.SupplierID == null);
        }

        public Holiday Create(Holiday entity)
        {
            _repo.Create(entity);
            return entity;
        }

        public Holiday Update(Holiday entity)
        {
            _repo.Update(entity);
            return entity;
        }

        public Holiday Destroy(Holiday entity)
        {
            _repo.Delete(entity);
            return entity;
        }
    }
}
