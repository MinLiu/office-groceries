﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ProductService<TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        protected ModelMapper<Product, TViewModel> _mapper;
        protected ProductRepository _repo;
        public ProductService(ModelMapper<Product, TViewModel> mapper)
        {
            _mapper = mapper;
            _repo = new ProductRepository();
        }

        public IQueryable<Product> Read()
        {
            return Read(true, false);
        }

        public IQueryable<Product> Read(bool isActive, bool deleted)
        {
            return _repo.Read().Where(x => x.IsActive == isActive).Where(x => x.Deleted == deleted);
        }

        public IQueryable<Product> ReadByAisle(Guid aisleID, string filterText, string selectedCategoryName)
        {
            var pdtCategoryService = new ProductCategoryService<ProductCategoryViewModel>(new ProductCategoryMapper());

            ProductCategory selectedCategory = null;
            var categoryIds = new List<Guid>();
            if (!string.IsNullOrEmpty(selectedCategoryName))
            {
                selectedCategory = pdtCategoryService.FindByInternalName(aisleID, selectedCategoryName);
                categoryIds = pdtCategoryService.GetAllSubCategories(selectedCategory);
            }
            var results = Read()
                    .Where(x => !categoryIds.Any() || categoryIds.Intersect(x.DryGoodsCategories.Select(c => c.CategoryID)).Any())
                    .Where(x => x.DryGoodsAisles.Select(d => d.AisleID).Contains(aisleID));
                    //.Where(x => string.IsNullOrEmpty(filterText) || x.Name.ToLower().Contains(filterText));
                               //.OrderBy(x => x.DryGoodsCategories.Where(dc => dc.Category.AisleID == aisleID).FirstOrDefault().Category.SortPos).ThenBy(x => x.SortPosition);
            
            var filterDes = GetFilterDescription(filterText);

            try
            {
                if (!string.IsNullOrWhiteSpace(filterDes))
                    results = results.Where(filterDes);
            }
            catch(Exception e)
            {
                results = results.Where(x => string.IsNullOrEmpty(filterText) || x.Name.ToLower().Contains(filterText));
            }

            if (selectedCategory != null)
            {
                results = results.OrderBy(x => x.DryGoodsCategories
                                                .Where(dc => dc.Category.AisleID == aisleID)
                                                .Where(dc => dc.CategoryID == selectedCategory.ID)
                                                .OrderBy(dc => dc.Category.SortPos)
                                                .FirstOrDefault()
                                                .Category
                                                .SortPos)
                                 .ThenBy(x => x.DryGoodsCategories
                                               .Where(dc => dc.Category.AisleID == aisleID)
                                               .Where(dc => dc.CategoryID == selectedCategory.ID)
                                               .OrderBy(dc => dc.Category.SortPos)
                                               .FirstOrDefault()
                                               .SortPos);
            }
            else
            {
                results = results.OrderBy(x => x.DryGoodsCategories
                                                .Where(dc => dc.Category.AisleID == aisleID)
                                                .OrderBy(dc => dc.Category.SortPos)
                                                .FirstOrDefault()
                                                .Category
                                                .SortPos)
                                 .ThenBy(x => x.DryGoodsCategories
                                               .Where(dc => dc.Category.AisleID == aisleID)
                                               .OrderBy(dc => dc.Category.SortPos)
                                               .FirstOrDefault()
                                               .SortPos);
            }
            return results;

            string GetFilterDescription(string filterStr)
            {
                var filterList = ParseFilterString(filterStr);
                var result = string.Join(" and ", filterList.Select(f =>
                    $"(Name.ToLower().Contains(\"{f}\") or Description.ToLower().Contains(\"{f}\") or Keywords.ToLower().Contains(\"{f}\"))"));
                return result;
            }

            List<string> ParseFilterString(string filterStr)
            {
                filterStr = (filterStr ?? "").ToLower();
                var result = filterStr
                    .Split(' ')
                    .Select(f => f.Trim())
                    .Where(f => !string.IsNullOrWhiteSpace(f))
                    .Select(f => f.Replace("\"", ""))
                    .ToList();
                return result;
            }
        }

        public IQueryable<Product> CustomerReadByAisle(string aisleID, Guid companyID, string filterText, string selectedCategoryName)
        {
            Guid guid;
            if (Guid.TryParse(aisleID, out guid))
                return CustomerReadByAisle(guid, companyID, filterText, selectedCategoryName);
            else
                return null;
        }

        public IQueryable<Product> CustomerReadByAisle(Guid aisleID, Guid companyID, string filterText, string selectedCategoryName)
        {
            var results = ReadByAisle(aisleID, filterText, selectedCategoryName)
                            .Where(x => x.IsExclusive != true || (x.IsExclusive && x.ExclusivePrices.Any(ex => ex.CusomterID == companyID)))
                            .Where(x => x.ExcludeCompanies.All(c => c.CompanyID != companyID));
            return results;
        }

        public IQueryable<Product> ProspectReadByAisle(string aisleID, string filterText, string selectedCategoryName)
        {
            Guid guid;
            if (Guid.TryParse(aisleID, out guid))
                return ProspectReadByAisle(guid, filterText, selectedCategoryName);
            else
                return null;
        }

        public IQueryable<Product> ProspectReadByAisle(Guid aisleID, string filterText, string selectedCategoryName)
        {
            var results = ReadByAisle(aisleID, filterText, selectedCategoryName)
                            .Where(x => x.IsProspectsVisible == true)
                            .Where(x => x.IsExclusive != true);
            return results;
        }

        public Product FindByName(string name)
        {
            var product = Read().Where(x => x.Name == name).FirstOrDefault();
            return product;
        }

        public Product FindByInternalName(string internalName)
        {
            var product = Read().Where(x => x.InternalName == internalName).FirstOrDefault();
            return product;
        }

        public bool IsUniquePdtCode(string pdtCode)
        {
            pdtCode = pdtCode.Trim().ToLower();
            var any = _repo
                .Read()
                .Where(x => x.Deleted == false)
                .Where(x => x.ProductCode.ToLower() == pdtCode)
                .Any();

            return !any;
        }

        public TViewModel MapToViewModel(Product model)
        {
            return _mapper.MapToViewModel(model);
        }

        public ProductListViewModel MapToViewModel(Product model, Company company)
        {
            return (_mapper as ProductListViewMapper).MapToViewModel(model, company);
        }
    }
}
