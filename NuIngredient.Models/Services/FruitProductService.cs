﻿using System;

namespace NuIngredient.Models
{
    public class FruitProductService
    {
        protected SnapDbContext _db;
        protected FruitProductRepository _repo;
        public FruitProductService(SnapDbContext db)
        {
            _db = db;
            _repo = new FruitProductRepository(_db);
        }

        public FruitSubtotalResult CalculateSubtotal(Guid id, Guid? companyID, int mon, int tue, int wed, int thu, int fri, int sat)
        {
            var fruit = _repo.Find(id);
            
            return CalculateSubtotal(fruit, companyID, mon, tue, wed, thu, fri, sat);
        }

        private FruitSubtotalResult CalculateSubtotal(FruitProduct fruit, Guid? companyID, int mon, int tue, int wed, int thu, int fri, int sat)
        {
            var subtotal = 0m;
            subtotal += fruit.GetPrice(mon, companyID);
            subtotal += fruit.GetPrice(tue, companyID);
            subtotal += fruit.GetPrice(wed, companyID);
            subtotal += fruit.GetPrice(thu, companyID);
            subtotal += fruit.GetPrice(fri, companyID);
            subtotal += fruit.GetPrice(sat, companyID);

            var totalVolume = mon + tue + wed + thu + fri + sat;
            var result = new FruitSubtotalResult
            {
                Subtotal = subtotal,
                UnitPrice = totalVolume == 0
                    ? 0
                    : Math.Round(subtotal / totalVolume, 2, MidpointRounding.AwayFromZero)
            };

            return result;
        }

        public class FruitSubtotalResult
        {
            public decimal Subtotal { get; set; }
            public decimal UnitPrice { get; set; }
        }
    }
}
