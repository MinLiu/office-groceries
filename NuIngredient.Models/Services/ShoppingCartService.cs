﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class ShoppingCartService
    {
        protected SnapDbContext _db;
        protected ShoppingCartItemRepository _repo;
        public ShoppingCartService(SnapDbContext db)
        {
            _db = db;
            _repo = new ShoppingCartItemRepository(_db);
        }

        public IQueryable<ShoppingCartItem> GetProspectQuote(string prospectToken)
        {
            return _repo.Read().Where(x => x.ProspectToken == prospectToken)
                               .Where(x => x.Product.IsActive == true)
                               .Where(x => x.Product.Deleted == false);
        }

        public void ClearShoppingCart(Guid companyID)
        {
            var items = _repo.Read().Where(x => x.CompanyID == companyID);
            _repo.Delete(items);
        }

    }
}
