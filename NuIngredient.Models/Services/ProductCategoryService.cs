﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NuIngredient.Models
{
    public class ProductCategoryService<TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        protected ModelMapper<ProductCategory, TViewModel> _mapper;
        protected ProductCategoryRepository _repo;
        public ProductCategoryService(ModelMapper<ProductCategory, TViewModel> mapper)
        {
            _mapper = mapper;
            _repo = new ProductCategoryRepository();
        }

        public IQueryable<ProductCategory> GetAllTopCategories(Guid aisleID)
        {
            var results = _repo.Read().Where(x => x.AisleID == aisleID)
                                      .Where(x => x.ParentID == null)
                                      .OrderBy(x => x.SortPos)
                                      .ThenBy(x => x.Name);
            return results;
        }

        public List<Guid> GetAllSubCategories(ProductCategory category)
        {
            // Get all the sub-categories
            List<Guid> categoryIDs = new List<Guid>();
            try
            {
                var childrenCategories = new List<ProductCategory>();
                category.GetChildrenCategory(childrenCategories);
                categoryIDs = childrenCategories.Select(x => x.ID).ToList();
            }
            catch { }

            return categoryIDs;
        }

        public ProductCategory FindByInternalName(Guid aisleID, string categoryName)
        {
            categoryName = HttpUtility.HtmlDecode(categoryName?? "");
            var model = _repo.Read().Where(x => x.AisleID == aisleID)
                                    .Where(x => x.InternalName == categoryName)
                                    .FirstOrDefault();

            return model;
        }

    }
}
