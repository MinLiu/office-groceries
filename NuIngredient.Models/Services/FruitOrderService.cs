﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Models
{
    public class FruitOrderService<TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        protected SnapDbContext _db;
        protected ModelMapper<FruitOrderHeader, TViewModel> _mapper;
        protected FruitOrderHeaderRepository _repo;
        public FruitOrderService(ModelMapper<FruitOrderHeader, TViewModel> mapper, SnapDbContext db)
        {
            _mapper = mapper;
            _db = db;
            _repo = new FruitOrderHeaderRepository(_db);
        }

        public List<FruitOrder> GetWeeklyOrderItems(OrderMode orderMode, Guid? companyID, string prospectToken, DateTime? fromDate)
        {
            var orderHeader = GetWeeklyOrder(orderMode, companyID, prospectToken, fromDate);

            if (orderHeader == null)
                return new List<FruitOrder>();
            else
                return orderHeader.Items.ToList();
        }

        public FruitOrderHeader GetWeeklyOrder(OrderMode orderMode, Guid? companyID, string prospectToken, DateTime? fromDate, bool depreciated = false, bool ignoreHolidays = false)
        {
            FruitOrderHeader fruitOrder;
            if (companyID != null)
            {
                if (orderMode == OrderMode.Regular && depreciated)
                {
                    fruitOrder = _repo.Read(true)
                        .Where(f => f.CompanyID == companyID
                                    && f.ProspectToken == null
                                    && f.OrderMode == orderMode
                                    && f.Depreciated == true)
                        .Include(f => f.Items)
                        .FirstOrDefault();

                    return fruitOrder;
                }

                fruitOrder = _repo.Read()
                                 .Where(f => f.CompanyID == companyID
                                             && f.ProspectToken == null
                                             && f.OrderMode == orderMode
                                             && f.FromDate == fromDate)
                                 .Include(f => f.Items)
                                 .FirstOrDefault();

                // Default with regular order
                if (orderMode == OrderMode.OneOff && fruitOrder == null)
                {
                    fruitOrder = _repo.Read()
                                     .Where(f => f.CompanyID == companyID
                                             && f.ProspectToken == null
                                             && f.OrderMode == OrderMode.Regular)
                                     .AsNoTracking()
                                     .Include(f => f.Items)
                                     .FirstOrDefault();
                }
            }
            else
            {
                // Prospects
                // Get Orders
                fruitOrder = _repo.Read()
                                 .Where(f => f.ProspectToken == prospectToken
                                          && f.OrderMode == orderMode
                                          && f.FromDate == fromDate)
                                 .Include(f => f.Items)
                                 .FirstOrDefault();
            }

            if (orderMode == OrderMode.OneOff &&
                ignoreHolidays == false)
            {
                var fruitSupplierId = companyID != null
                    ? SupplierHelper.GetSupplier(companyID.Value, SupplierType.Fruits, fromDate.Value)?.ID
                    : null;
                var snackSupplierId = companyID != null
                    ? SupplierHelper.GetSupplier(companyID.Value, SupplierType.Snacks, fromDate.Value)?.ID
                    : null;
                SetNonDeliveryOnHolidays(fruitOrder, fromDate.Value, fruitSupplierId, snackSupplierId);
            }

            return fruitOrder;
        }

        public FruitOrderHeader GetWeeklyOrderAsNoTracking(OrderMode orderMode, Guid companyID, DateTime? fromDate)
        {
            var fruitOrder = _repo.Read()
                .Where(f => f.CompanyID == companyID
                            && f.ProspectToken == null
                            && f.OrderMode == orderMode
                            && f.FromDate == fromDate)
                .Include(f => f.Items)
                .AsNoTracking()
                .FirstOrDefault();

            // Default with regular order
            if (orderMode == OrderMode.OneOff && fruitOrder == null)
            {
                fruitOrder = _repo.Read()
                    .Where(f => f.CompanyID == companyID
                                && f.ProspectToken == null
                                && f.OrderMode == OrderMode.Regular)
                    .Include(f => f.Items)
                .AsNoTracking()
                    .FirstOrDefault();
            }

            if (orderMode == OrderMode.OneOff)
            {
                var fruitSupplierId = SupplierHelper.GetSupplier(companyID, SupplierType.Fruits, fromDate.Value)?.ID;
                var snackSupplierId = SupplierHelper.GetSupplier(companyID, SupplierType.Snacks, fromDate.Value)?.ID;
                SetNonDeliveryOnHolidays(fruitOrder, fromDate.Value, fruitSupplierId, snackSupplierId);
            }

            return fruitOrder;
        }

        public FruitOrderHeader GetProspectOrder(string prospectToken)
        {
            return GetWeeklyOrder(OrderMode.Draft, null, prospectToken, null);
        }

        public FruitOrderHeader GetCustomerOrder(Guid companyID, OrderMode orderMode, DateTime? fromDate, bool depreciated = false, bool ignoreHolidays = false)
        {
            return GetWeeklyOrder(orderMode, companyID, null, fromDate, depreciated, ignoreHolidays);
        }

        public FruitOrderHeader GetOrderByID(Guid ID)
        {
            var order = _repo.Read()
                .Where(f => f.ID == ID)
                .Include(f => f.Items)
                .FirstOrDefault();
            return order;
        }

        public List<FruitOrder> GetProspectOrderItems(string prospectToken)
        {
            return GetWeeklyOrderItems(OrderMode.Draft, null, prospectToken, null);
        }

        public List<FruitOrder> GetCustomerOrderItems(Guid companyID, OrderMode orderMode, DateTime? fromDate)
        {
            return GetWeeklyOrderItems(orderMode, companyID, null, fromDate);
        }

        public FruitOrderHeader DeleteOrder(FruitOrderHeader orderHeader)
        {
            _repo.Delete(orderHeader);
            return orderHeader;
        }

        public FruitOrderHeader DeleteItem(FruitOrderHeader orderHeader, Guid fruitOrderID)
        {
            // Delete product
            var repo = new FruitOrderRepository(_db);
            var item = repo.Read().Where(x => x.ID == fruitOrderID).FirstOrDefault();

            // Delete change tracker
            var trackerRepo = new FruitOneOffChangeTrackerRepository(_db);
            var trackers = trackerRepo.Read()
                .Where(x => x.OrderHeaderID == orderHeader.ID)
                .Where(x => x.FruitProductID == item.FruitProductID);

            repo.Delete(item);
            trackerRepo.Delete(trackers);

            return orderHeader;
        }

        public FruitOrderHeader DeleteAllItems(FruitOrderHeader orderHeader)
        {
            var repo = new FruitOrderRepository(_db);
            var items = repo.Read().Where(x => x.OrderHeaderID == orderHeader.ID);
            repo.Delete(items);
            return orderHeader;
        }

        public FruitOrder AddItem(Guid orderHeaderID, FruitOrder item, Guid? companyID)
        {
            var fruitProductService = new FruitProductService(_db);
            var repo = new FruitOrderRepository(_db);

            var lastSortPos = repo.Read()
               .Where(x => x.OrderHeaderID == orderHeaderID)
               .Select(x => x.SortPos)
               .ToList()
               .OrderByDescending(x => x)
               .DefaultIfEmpty(-1)
               .FirstOrDefault();

            var subtotalResult = fruitProductService.CalculateSubtotal(item.FruitProductID, companyID, item.Monday, item.Tuesday, item.Wednesday, item.Thursday, item.Friday, item.Saturday);

            item.Price = subtotalResult.UnitPrice;
            item.TotalNet = subtotalResult.Subtotal;
            item.TotalVAT = item.TotalNet * item.VATRate;
            item.OrderHeaderID = orderHeaderID;
            item.SortPos = ++lastSortPos;

            repo.Create(item);

            return item;
        }

        public FruitOrder AddItem(Guid orderHeaderID, FruitProduct product, Guid? companyID)
        {
            var fruitOrder = new FruitOrder()
            {
                ProductName = product.Name,
                Unit = product.Unit,
                Monday = 0,
                Tuesday = 0,
                Wednesday = 0,
                Thursday = 0,
                Friday = 0,
                Saturday = 0,
                Sunday = 0,
                FruitProductID = product.ID,
                VATRate = product.VAT,
                Price = product.GetBasicPrice(),
                TotalNet = 0,
                TotalVAT = 0,
            };
            return AddItem(orderHeaderID, fruitOrder, companyID);
        }

        public FruitOrderHeader CalculateTotal(Guid ID)
        {
            var orderHeader = _repo.Find(ID);
            return CalculateTotal(orderHeader);
        }

        public FruitOrderHeader CalculateTotal(FruitOrderHeader orderHeader)
        {
            orderHeader.TotalNet = 0m;
            orderHeader.TotalVAT = 0m;

            if (orderHeader.Items != null)
            { 
                foreach (var item in orderHeader.Items)
                {
                    orderHeader.TotalNet += item.TotalNet;
                    orderHeader.TotalVAT += item.TotalVAT;
                }
            }

            orderHeader = Update(orderHeader);
            return orderHeader;
        }

        private void SetNonDeliveryOnHolidays(FruitOrderHeader orderHeader, DateTime orderFromDate, Guid? fruitSupplierId, Guid? snackSupplierId)
        {
            var orderToDate = orderFromDate.AddDays(6);
            var fruitHolidays = new HolidayService(_db).ReadSupplierHolidays(fruitSupplierId)
                .Where(x => orderFromDate <= x.Date && x.Date <= orderToDate)
                .ToList()
                .Select(x => x.Date.Date)
                .ToList();
            var snackHolidays = new HolidayService(_db).ReadSupplierHolidays(snackSupplierId)
                .Where(x => orderFromDate <= x.Date && x.Date <= orderToDate)
                .ToList()
                .Select(x => x.Date.Date)
                .ToList();
            
            var fruitOpenDays = new SupplierOpenDayRepository(_db).ReadSupplierOpenDays(fruitSupplierId)
                .Where(x => orderFromDate <= x.Date && x.Date <= orderToDate)
                .ToList()
                .Select(x => x.Date.Date)
                .ToList();
            
            var snackOpenDays = new SupplierOpenDayRepository(_db).ReadSupplierOpenDays(snackSupplierId)
                .Where(x => orderFromDate <= x.Date && x.Date <= orderToDate)
                .ToList()
                .Select(x => x.Date.Date)
                .ToList();

            
            var mon = orderFromDate.Date;
            var tue = orderFromDate.Date.AddDays(1);
            var wed = orderFromDate.Date.AddDays(2);
            var thu = orderFromDate.Date.AddDays(3);
            var fri = orderFromDate.Date.AddDays(4);
            var sat = orderFromDate.Date.AddDays(5);
            
            if ((fruitHolidays.Any() || snackHolidays.Any())
                && orderHeader?.Items != null)
            {
                var service = new FruitProductService(new SnapDbContext());

                foreach (var orderItem in orderHeader.Items.Where(x => x.Fruit.FruitProductType == FruitProductType.Fruit))
                {
                    foreach (var holiday in fruitHolidays.Where(x => !fruitOpenDays.Contains(x)))
                    {
                        if (holiday == mon)
                        {
                            orderItem.Monday = 0;
                        }
                        else if (holiday == tue)
                        {
                            orderItem.Tuesday = 0;
                        }
                        else if (holiday == wed)
                        {
                            orderItem.Wednesday = 0;
                        }
                        else if (holiday == thu)
                        {
                            orderItem.Thursday = 0;
                        }
                        else if (holiday == fri)
                        {
                            orderItem.Friday = 0;
                        }
                        else if (holiday == sat)
                        {
                            orderItem.Saturday = 0;
                        }
                    }

                    var subtotalResult = service.CalculateSubtotal(orderItem.FruitProductID, orderHeader.CompanyID, orderItem.Monday, orderItem.Tuesday, orderItem.Wednesday, orderItem.Thursday, orderItem.Friday, orderItem.Saturday);
                    orderItem.TotalNet = subtotalResult.Subtotal;
                    orderItem.Price = subtotalResult.UnitPrice;
                    //orderItem.CalculateTotal();
                }
                
                foreach (var orderItem in orderHeader.Items.Where(x => x.Fruit.FruitProductType == FruitProductType.Snack))
                {
                    foreach (var holiday in snackHolidays.Where(x => !snackOpenDays.Contains(x)))
                    {
                        if (holiday == mon)
                        {
                            orderItem.Monday = 0;
                        }
                        else if (holiday == tue)
                        {
                            orderItem.Tuesday = 0;
                        }
                        else if (holiday == wed)
                        {
                            orderItem.Wednesday = 0;
                        }
                        else if (holiday == thu)
                        {
                            orderItem.Thursday = 0;
                        }
                        else if (holiday == fri)
                        {
                            orderItem.Friday = 0;
                        }
                        else if (holiday == sat)
                        {
                            orderItem.Saturday = 0;
                        }
                    }

                    var subtotalResult = service.CalculateSubtotal(orderItem.FruitProductID, orderHeader.CompanyID, orderItem.Monday, orderItem.Tuesday, orderItem.Wednesday, orderItem.Thursday, orderItem.Friday, orderItem.Saturday);
                    orderItem.TotalNet = subtotalResult.Subtotal;
                    orderItem.Price = subtotalResult.UnitPrice;
                    //orderItem.CalculateTotal();
                }

                orderHeader.CalculateTotal();
            }
        }

        public FruitOrderHeader Update(FruitOrderHeader model)
        {
            _repo.Update(model);
            return model;
        }

        public FruitOrderHeader Create(FruitOrderHeader model)
        {
            _repo.Create(model);
            return model;
        }

        public TViewModel MapToViewModel(FruitOrderHeader model)
        {
            return _mapper.MapToViewModel(model);
        }

    }
}
