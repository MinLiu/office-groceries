﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class SuppliersController : EntityController<Supplier, SupplierViewModel>
    {
      
        public SuppliersController()
            : base(new SupplierRepository(), new SupplierMapper())
        {

        }

        // GET: Suppliers
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult Deleted()
        //{
        //    return View();
        //}

        //#region Edit Supplier Panel

        // GET: /New Supplier
        public ActionResult _NewSupplier()
        {
            return PartialView(new SupplierViewModel());
        }


        // POST: /New Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewSupplier(SupplierViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            return Json(new { ID = model.ID });

        }

        // GET: /Edit Supplier
        public ActionResult _EditSupplier(Guid ID)
        {
            var model = _repo.Find(ID);
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }


        // POST: /New Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditSupplier(SupplierViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.

            return PartialView(viewModel);

        }



        //// GET: /Edit Supplier
        //public ActionResult _EditSupplier(string ID)
        //{
        //    try
        //    {
        //        //Make sure the supplier being returned belongs to the user of the user
        //        Supplier clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
        //        SupplierViewModel model = _mapper.MapToViewModel(clt);

        //        ViewBag.CurrentUserID = CurrentUser.Id;
        //        ViewBag.CurrentUserEmail = CurrentUser.Email;
        //        return PartialView(model);
        //    }
        //    catch {
        //        return PartialView("_NewSupplier", new SupplierViewModel());
        //    }

        //}

        public JsonResult DropDownSuppliers(SupplierType? supplierType)
        {
            var viewModels = _repo.Read()
                                  .Where(s => (supplierType == null || s.SupplierType == supplierType))
                                  .Where(s => s.Deleted == false)
                                  .ToList()
                                  .Select(s => new DropDownListItem()
                                  {
                                      Text = s.Name,
                                      Value = s.ID.ToString()
                                  });

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownDifficultSuppliers(SupplierType? supplierType)
        {
            var viewModels = _repo.Read()
                                  .Where(s => (supplierType == null || s.SupplierType == supplierType))
                                  .Where(s => s.UseMasterAccount == false)
                                  .Where(s => s.Deleted == false)
                                  .ToList()
                                  .Select(s => new DropDownListItem()
                                  {
                                      Text = s.Name,
                                      Value = s.ID.ToString()
                                  });

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<Supplier> GetSuppliers()
        {
            var suppliers = _repo.Read()
                               .Where(c => c.Deleted == false)
                               .OrderBy(c => c.Name)
                               .AsQueryable();

            return suppliers;
        }

    }
    
}