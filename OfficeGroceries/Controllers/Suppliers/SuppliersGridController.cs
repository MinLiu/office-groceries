﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class SuppliersGridController : GridController<Supplier, SupplierViewModel>
    {
      
        public SuppliersGridController()
            : base(new SupplierRepository(), new SupplierMapper())
        {

        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _repo.Read().Where(x => x.Deleted == false).OrderBy(x => x.SupplierType).ThenBy(x => x.UseMasterAccount).ThenBy(x => x.Name);
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, SupplierViewModel viewModel)
        {

            // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
    }
    
}