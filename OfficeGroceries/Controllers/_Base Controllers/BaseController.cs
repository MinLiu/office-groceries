﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using System.Web.Routing;
using Kendo.Mvc.UI;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Data.Entity;
using System;
using System.IO;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using System.Drawing;

namespace NuIngredient.Controllers
{
    public class BaseController : Controller
    {
        public UserManager UserManager;
        public User CurrentUser;  //This is the current user logged in.
        public string ProspectToken;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            GZipEncodePage();

            try
            {
                SetDefaultConfiguration();

                if (!User.Identity.IsAuthenticated)
                {
                    SetAisles();
                    
                    ViewBag.Milk = false;
                    ViewBag.Fruits = false;
                    ViewBag.DryGoods = false;

                    // Save Prospect's token
                    HttpCookie cookie = Request.Cookies["ProspectToken"];
                    if (cookie == null)
                    {
                        cookie = new HttpCookie("ProspectToken");
                        cookie.Value = Guid.NewGuid().ToString();
                    }
                    cookie.Expires = DateTime.Now.AddHours(1);

                    ProspectToken = cookie.Value;
                    Response.Cookies.Add(cookie);

                    return;
                }

                Response.Cookies["ProspectToken"].Expires = DateTime.Now.AddDays(-1);

                UserManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();

                var userId = User.Identity.GetUserId().ToString();
                var userRepo = new UserRepository();
                CurrentUser = userRepo.Read().Where(u => u.Id == userId).Include(u => u.Company).First();

                if (CurrentUser.CanSwitchCompany)
                {
                        
                    ViewBag.OtherCompanies = CurrentUser.Company.AllRelatedCompanies()?.Where(c => c.ID != CurrentUser.CompanyID).OrderBy(c => c.Name).Select(c => new SelectItemViewModel() { ID = c.ID.ToString(), Name = c.Name }).ToList()
                        ?? new List<SelectItemViewModel>();
                }
                else
                {
                    ViewBag.OtherCompanies = new List<SelectItemViewModel>(); 
                }

                SetAisles(CurrentUser.CompanyID);

                //Check Login Misuse by looking at the cookies
                //if (CurrentUser.Token != (Request.Cookies["GG"] ?? new HttpCookie("GG")).Value)
                //{
                //    ControllerContext.HttpContext.GetOwinContext().Authentication.SignOut();
                //    Response.Redirect("/Home/LoginMisuse");
                //}

                //Used for navbar settings
                ViewBag.ColSize = 4;
                ViewBag.AccessCount = 0;

                SetCustomerStatus();

            }
            catch {

                ViewBag.ColSize = 4;
                ViewBag.AccessCount = 0;
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }
            }

            base.Dispose(disposing);
        }


        public void CheckSubscription() {

            return;

        }

        protected void SetCustomerStatus()
        {
            ViewBag.Milk = false;
            ViewBag.Fruits = false;
            ViewBag.DryGoods = false;

            if (User.IsInRole("Admin") || User.IsInRole("User"))
            {
                ViewBag.Milk = CurrentUser.Company.Method_HasSupplier(SupplierType.Milk) || CurrentUser.Company.AskingForMilkSupplier != null;
                ViewBag.Fruits = CurrentUser.Company.Method_HasSupplier(SupplierType.Fruits) || CurrentUser.Company.AskingForFruitSupplier != null;
                ViewBag.DryGoods = CurrentUser.Company.Method_HasSupplier(SupplierType.DryGoods) || CurrentUser.Company.AskingForDryGoodsSupplier != null;
                SetShoppingCartStatus();
            }
        }

        protected void SetDefaultConfiguration()
        {
            using (var context = new SnapDbContext())
            { 
                ViewBag.MilkMinimumValue = context.Suppliers.Where(x => x.SupplierType == SupplierType.Milk)
                                                            .Where(x => x.Deleted == false)
                                                            .Select(x => x.MinimumValue)
                                                            .DefaultIfEmpty(0)
                                                            .OrderByDescending(x => x)
                                                            .FirstOrDefault();
                ViewBag.FruitMinimumValue = context.Suppliers.Where(x => x.SupplierType == SupplierType.Fruits)
                                                            .Where(x => x.Deleted == false)
                                                            .Select(x => x.MinimumValue)
                                                            .DefaultIfEmpty(0)
                                                            .OrderByDescending(x => x)
                                                            .FirstOrDefault();
            }
        }

        protected void SetCustomerStatus(string companyID)
        {
            var company = new CompanyRepository().Find(Guid.Parse(companyID));

            ViewBag.Milk = company.Method_HasSupplier(SupplierType.Milk) || CurrentUser.Company.AskingForMilkSupplier != null;
            ViewBag.Fruits = company.Method_HasSupplier(SupplierType.Fruits) || CurrentUser.Company.AskingForFruitSupplier != null;
            ViewBag.DryGoods = company.Method_HasSupplier(SupplierType.DryGoods) || CurrentUser.Company.AskingForDryGoodsSupplier != null;
        }

        protected void SetAisles(Guid? companyID = null)
        {
            var repo = new AisleRepository();
            var mapper = new AisleMapper();
            var aisles = repo.Read().Where(x => x.StartDate <= DateTime.Now)
                                    .Where(x => x.EndDate == null || x.EndDate >= DateTime.Now)
                                    .Where(x => companyID == null || x.ExcludeCompanies.All(c => c.CompanyID != companyID))
                                    .OrderBy(x => x.SortPosition)
                                    .ToList()
                                    .Select(x => mapper.MapToViewModel(x))
                                    .ToList();
            ViewBag.Aisles = aisles;
            ViewBag.MainDryGoodsAisleName = aisles
                .Where(x => x.IsMain)
                .Where(x => x.AisleType == AisleType.DryGoods)
                .OrderBy(x => x.SortPosition)
                .Select(x => x.InternalName)
                .DefaultIfEmpty("")
                .FirstOrDefault();
        }

        protected void SetShoppingCartStatus()
        {
            var result = GetShoppingCartStatus();
            ViewBag.ItemQty = result.Item1;
            ViewBag.Total = result.Item2;
        }

        /// <summary>
        /// return item qty, total
        /// </summary>
        /// <returns></returns>
        protected Tuple<int, string> GetShoppingCartStatus()
        {
            var repo = new ShoppingCartItemRepository();
            if (!User.Identity.IsAuthenticated)
            {
                return new Tuple<int, string>(0, 0.ToString("C"));
            }
            var shoppingCartItems = repo
                .Read()
                .Where(x => x.CompanyID == CurrentUser.CompanyID)
                .Where(x => x.Product.IsActive)
                .ToList()
                .Where(x => x.Product.Supplier.UseMasterAccount && (CurrentUser.Company.EasySuppliersActivated) || CurrentUser.Company.Method_IsLive(x.Product.Supplier))
                .ToList();

            var result = new Tuple<int, string>(
                shoppingCartItems.Sum(x => x.Qty),
                shoppingCartItems.Sum(x => x.Qty * x.Product.ExclusivePrice(CurrentUser.CompanyID) + x.Qty * x.Product.Price * x.Product.VAT)
                    .ToString("C"));
            return result;
        }

        public string SaveFile(HttpPostedFileBase file, string subFolder = "/", string filename = "", string companyID = null)
        {
            if(file == null) return null;

            const string contentFolderRoot = "/Uploads/";
            var virtualPath = string.Format("{0}{1}{2}", contentFolderRoot, companyID ?? CurrentUser.CompanyID.ToString(), subFolder);
            

            var physicalPath = Server.MapPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var extension = Path.GetExtension(file.FileName);
            var str = (string.IsNullOrEmpty(filename)) ? Guid.NewGuid().ToString() + extension : filename + extension;
            var physicalSavePath = Server.MapPath(virtualPath) + str;
            file.SaveAs(physicalSavePath);
            return virtualPath + str; 
        }

        public bool DeleteFile(string filepath)
        {
            try
            {
                var physicalPath = Server.MapPath(filepath);
                if (System.IO.File.Exists(physicalPath)) System.IO.File.Delete(physicalPath);
                return true;
            }
            catch { return false; }
        }

        public bool DoesFileExists(string subFolder = "/", string filename = "")
        {
            const string contentFolderRoot = "/Uploads/";
            var virtualPath = string.Format("{0}{1}{2}{3}", contentFolderRoot, CurrentUser.CompanyID.ToString(), subFolder, filename);

            return (System.IO.File.Exists(virtualPath));
        }

        public string GetUploadVirtualFilePath(string subFolder = "/", string filename = "", string companyID = null)
        {
            const string contentFolderRoot = "/Uploads/";
            return string.Format("{0}{1}{2}{3}", contentFolderRoot, companyID ?? CurrentUser.CompanyID.ToString(), subFolder, filename);
        }

        public string GetUploadPhysicalFilePath(string subFolder = "/", string filename = "", string companyID = null)
        {   
            return Server.MapPath(GetUploadVirtualFilePath(subFolder, filename, companyID));
        }

        public ImgLocation SaveImg(HttpPostedFileBase file, string subFolder = "/", string filename = "", string companyID = null)
        {
            var imgLocation = SaveFile(file, subFolder, filename, companyID);
            var thumbImgLocation = Path.GetDirectoryName(imgLocation) + "/" + Path.GetFileNameWithoutExtension(imgLocation) + "-thumb.jpg";

            var physicalSavePath = Server.MapPath(thumbImgLocation);

            Image image = Image.FromStream(file.InputStream);
            var thumbWidth = 300;
            var thumbHeight = (thumbWidth * image.Height) / image.Width;
            Image thumb = image.GetThumbnailImage(thumbWidth, thumbHeight, () => false, IntPtr.Zero);
            thumb.Save(physicalSavePath);

            return new ImgLocation(imgLocation, thumbImgLocation);
        }

        //public string CreateThumb(string filePath)
        //{
        //    var physicalPath = Server.MapPath(filePath);

        //    var thumbImgLocation = Path.GetDirectoryName(filePath) + "/" + Path.GetFileNameWithoutExtension(filePath) + "-thumb.jpg";

        //    var physicalSavePath = Server.MapPath(thumbImgLocation);

        //    Image image = Image.FromFile(physicalPath);
        //    var thumbWidth = 300;
        //    var thumbHeight = (thumbWidth * image.Height) / image.Width;
        //    Image thumb = image.GetThumbnailImage(thumbWidth, thumbHeight, () => false, IntPtr.Zero);
        //    thumb.Save(physicalSavePath);

        //    return thumbImgLocation;
        //}

        public class ImgLocation
        {
            public string Path { get; set; }
            public string ThumbnailPath { get; set; }
            public ImgLocation (string path, string thumbnailPath)
            {
                Path = path;
                ThumbnailPath = thumbnailPath;
            }
        }

        /// <summary>
        /// Determines if GZip is supported
        /// </summary>
        /// <returns></returns>
        private bool IsGZipSupported()
        {
            if (HttpContext.Response.Filter == null)
                return false;
            string AcceptEncoding = HttpContext.Request.Headers["Accept-Encoding"];
            if (!string.IsNullOrEmpty(AcceptEncoding) &&
                    (AcceptEncoding.Contains("gzip") || AcceptEncoding.Contains("deflate")))
                return true;
            return false;
        }

        /// <summary>
        /// Sets up the current page or handler to use GZip through a Response.Filter
        /// IMPORTANT:  
        /// You have to call this method before any output is generated!
        /// </summary>
        private void GZipEncodePage()
        {
            var Response = HttpContext.Response;

            if (IsGZipSupported())
            {
                string AcceptEncoding = HttpContext.Request.Headers["Accept-Encoding"];

                if (AcceptEncoding.Contains("deflate"))
                {
                    Response.Filter = new System.IO.Compression.DeflateStream(Response.Filter,
                                                System.IO.Compression.CompressionMode.Compress);
                    Response.Headers.Remove("Content-Encoding");
                    Response.AppendHeader("Content-Encoding", "deflate");
                }
                else
                {
                    Response.Filter = new System.IO.Compression.GZipStream(Response.Filter,
                                                System.IO.Compression.CompressionMode.Compress);
                    Response.Headers.Remove("Content-Encoding");
                    Response.AppendHeader("Content-Encoding", "gzip");
                }

            }
        }
    }
}