﻿using System;
using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    
    public class DeliveryAreasController : BaseController
    {
        public ActionResult Index(string locationName = null)
        {
            locationName = locationName ?? "";

            if (AppSettings.DeliveryAreas.Any(x =>
                    string.Equals(x, locationName, StringComparison.InvariantCultureIgnoreCase)))
            {
                ViewBag.MetaTitle = $"Milk & Fruit Deliveries for Offices in {locationName} | Fresh & Reliable Service";
                ViewBag.MetaDescription = $"Get fresh milk and fruit delivered to your office in {locationName} with our reliable, hassle-free service. Enjoy high-quality dairy, snacks, and more—perfect for keeping your team fuelled. Order today!";
                return View(locationName);
            }
            
            ViewBag.MetaTitle = "Milk & Fruit Deliveries for Offices Near You | Fresh & Reliable Service";
            ViewBag.MetaDescription = "Get fresh milk and fruit delivered to your office near you, with our reliable, hassle-free service. Enjoy high-quality dairy, snacks, and more—perfect for keeping your team fuelled. Order today!";

            return View();
        }

        public ActionResult Redirection(string locationName = null)
        {
            return Redirect("/delivery-areas/" + locationName);
        }

    }

}