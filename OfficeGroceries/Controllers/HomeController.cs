﻿using NuIngredient.Models;
using NuIngredient.Models.Services.HtmlSitemapGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;

namespace NuIngredient.Controllers
{
    [RoutePrefix(""), Route("{action}")]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var page = new PageSettingRepository().Read().Where(x => x.Label == "Home").FirstOrDefault();
            if (page != null)
            {
                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;
            }

            return View();
        }

        public ActionResult FAQs()
        {
            var page = new PageSettingRepository().Read().Where(x => x.Label == "FAQs").FirstOrDefault();
            if (page != null)
            {
                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;
            }
            return View();
        }

        public ActionResult Process()
        {
            var page = new PageSettingRepository().Read().Where(x => x.Label == "Process").FirstOrDefault();
            if (page != null)
            {
                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;
            }
            return View();
        }

        public ActionResult Testimonials()
        {
            var mapper = new TestimonialMapper();
            var testmonials = new TestimonialRepository()
                .Read()
                .Where(x => x.Deleted == false)
                .Where(x => x.Active)
                .OrderBy(x => x.SortPos)
                .ToList();

            var viewModels = testmonials.Select(x => mapper.MapToViewModel(x)).ToList();
            return View(viewModels);
        }

        public ActionResult Contact()
        {
            var page = new PageSettingRepository().Read().Where(x => x.Label == "Contact Details").FirstOrDefault();
            if (page != null)
            {
                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;
            }
            return View();
        }

        public ActionResult CopyOrder()
        {
            if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                return Json("Failed", JsonRequestBehavior.AllowGet);

            var companyList = new CompanyRepository().Read().Where(c => c.DeleteData == false).ToList();
            foreach (var company in companyList)
            {
                new Schedule.ScheduleCopyingOrders().GenerateFruitInvoices(company);
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult _404()
        {
            return View();
        }


        public ActionResult LoginMisuse()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return View();
        }

        public ActionResult Inactive()
        {
            return View();
        }

        public ActionResult Sitemap()
        {
            var viewModel = HtmlSitemapGenerator.Initialize().Generate();
            return View(viewModel);
        }

        public ActionResult About()
        {
            var pageAbout = new StaticPageMapper().MapToViewModel(new StaticPageRepository().Read().Where(x => x.Name == "About").First());
            ViewBag.MetaTitle = pageAbout.MetaTitle;
            ViewBag.MetaKeyword = pageAbout.MetaKeyword;
            ViewBag.MetaDescription = pageAbout.MetaDescription;
            return View(pageAbout);
        }

        public ActionResult PrivacyPolicy()
        {
            var pageAbout = new StaticPageMapper().MapToViewModel(new StaticPageRepository().Read().Where(x => x.Name == "PrivacyPolicy").First());
            ViewBag.MetaTitle = pageAbout.MetaTitle;
            ViewBag.MetaKeyword = pageAbout.MetaKeyword;
            ViewBag.MetaDescription = pageAbout.MetaDescription;
            return View(pageAbout);
        }

        public ActionResult TermsConditions()
        {
            var pageAbout = new StaticPageMapper().MapToViewModel(new StaticPageRepository().Read().Where(x => x.Name == "TermsConditions").First());
            ViewBag.MetaTitle = pageAbout.MetaTitle;
            ViewBag.MetaKeyword = pageAbout.MetaKeyword;
            ViewBag.MetaDescription = pageAbout.MetaDescription;
            return View(pageAbout);
        }

        public ActionResult GenerateSitemap()
        {
            var sitemapGenerator = new Boilerplate.Web.Mvc.Sitemap.SitemapGeneratorImp();
            var sitemapNodes = sitemapGenerator.Generate();
            sitemapNodes[0].Save(System.Web.Hosting.HostingEnvironment.MapPath("/sitemap.xml"));
            return Content("Success");
        }

        public ActionResult SayCoffee()
        {
            return View();
        }

        public ActionResult Ernie()
        {
            return View();
        }

        public ActionResult DeliveryAreas()
        {
            return View();
        }

        public ActionResult Location(string locationName)
        {
            return View("Location", null, locationName);
        }
    }

    

}