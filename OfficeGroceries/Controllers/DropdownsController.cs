﻿using NuIngredient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuIngredient.Controllers
{
    public class DropdownsController : BaseController
    {
      
        public JsonResult DropDownUsers()
        {
            //Get the teams where the user is in     
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersWithNotAssigned()
        {
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Add(new UserItemViewModel { ID = "-1", Email = "Not Assigned" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownEventUsers()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Where(u => u.Id != CurrentUser.Id)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersSearch()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            //users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersAll()
        {
            //Get the teams where the user is in  
            var userRepo = new UserRepository();

            var users = userRepo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownCompanyUsers(string companyID)
        {
            var userRepo = new UserRepository();

            if (string.IsNullOrWhiteSpace(companyID))
                return Json(new List<SelectItemViewModel>(), JsonRequestBehavior.AllowGet);

            Guid CompanyID = Guid.Parse(companyID);

            var users = userRepo.Read()
                                .Where(u => u.CompanyID == CompanyID)
                                .Select(u => new SelectItemViewModel()
                                {
                                    ID = u.Id,
                                    Name = u.FirstName + " " + u.LastName
                                })
                                .ToList();

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownSupplierTypes()
        {
            var supplierTypes = new List<SelectItemViewModel>();
            foreach (SupplierType type in Enum.GetValues(typeof(SupplierType)))
            {
                supplierTypes.Add(new SelectItemViewModel()
                {
                    ID = type.ToString(),
                    Name = Enum.GetName(typeof(SupplierType), type)
                });
            }

            return Json(supplierTypes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownProducts()
        {
            var repo = new ProductRepository();
            var products = repo.Read()
                               .Where(x => x.Deleted == false)
                               .Where(x => x.IsActive == true)
                               .ToList()
                               .Select(x => new SelectItemViewModel()
                               {
                                   ID = x.ID.ToString(),
                                   Name = x.Name
                               });
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownProspects()
        {
            using (var db = new SnapDbContext())
            {
                var prospects = db.Prospects.ToList()
                                            .Select(x => new SelectItemViewModel()
                                            {
                                                ID = x.ProspectToken,
                                                Name = x.CompanyName
                                            });
                return Json(prospects, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownCompanyDryGoodsOrders()
        {
            using (var db = new SnapDbContext())
            {
                var orders = db.OneOffOrders.Where(x => x.CompanyID == CurrentUser.CompanyID)
                                            .ToList()
                                            .Select(x => new SelectItemViewModel()
                                            {
                                                ID = x.ID.ToString(),
                                                Name = x.OrderNumber + " " + x.Created.ToString("dd/MM/yyyy")
                                            });
                return Json(orders, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownProductCategories(List<string> aisleIDs)
        {
            if (aisleIDs == null || aisleIDs.Count() == 0)
            {
                return Json(new List<SelectItemViewModel>(), JsonRequestBehavior.AllowGet);
            }
            using (var db = new SnapDbContext())
            {
                var orders = db.ProductCategories
                               .Where(x => aisleIDs.Contains(x.AisleID.ToString()))
                               .ToList()
                               .Select(x => new SelectItemViewModel()
                               {
                                   ID = x.ID.ToString(),
                                   Name = x.Aisle.Name + ": " + x.Name
                               })
                               .OrderBy(x => x.Name)
                               .ToList();
                return Json(orders, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownTargets()
        {
            using (var db = new SnapDbContext())
            {
                var targets = db.Target
                               .Where(x => x.Active == true)
                               .ToList()
                               .OrderBy(x => x.Name)
                               .Select(x => new SelectItemViewModel()
                               {
                                   ID = x.ID.ToString(),
                                   Name = x.Name
                               });
                return Json(targets, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownAisles()
        {
            using (var db = new SnapDbContext())
            {
                var orders = db.Aisles
                               .Where(x => x.Deleted == false)
                               .ToList()
                               .Select(x => new SelectItemViewModel()
                               {
                                   ID = x.ID.ToString(),
                                   Name = x.Name
                               });
                return Json(orders, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownDryGoodsAisles()
        {
            using (var db = new SnapDbContext())
            {
                var orders = db.Aisles
                               .Where(x => x.Deleted == false)
                               .Where(x => x.Name != "Milk" && x.Name != "Fruit")
                               .ToList()
                               .Select(x => new SelectItemViewModel()
                               {
                                   ID = x.ID.ToString(),
                                   Name = x.Name
                               });
                return Json(orders, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownDryGoodsSuppliers()
        {
            using (var db = new SnapDbContext())
            {
                var orders = db.Suppliers
                               .Where(x => x.SupplierType == SupplierType.DryGoods)
                               .Where(x => x.Deleted == false)
                               .ToList()
                               .Select(x => new SelectItemViewModel()
                               {
                                   ID = x.ID.ToString(),
                                   Name = x.Name
                               });
                return Json(orders, JsonRequestBehavior.AllowGet);
            }
        }
    }



}