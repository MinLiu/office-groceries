﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class HierarchyMembersController : GridController<HierarchyMember, HierarchyMemberViewModel>
    {

        public HierarchyMembersController()
            : base(new HierarchyMemberRepository(), new HierarchyMemberMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string reportTo)
        {
            var users = _repo.Read().Where(i => i.CompanyID == CurrentUser.CompanyID && (reportTo != null) ? i.ReportsToID.ToString() == reportTo : i.ReportsToID == null)
                                    .Include(i => i.Underlings)
                                    .ToList()
                                    .Select(i => _mapper.MapToViewModel(i));

            return Json(users.ToDataSourceResult(request));
        }



        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<HierarchyMember>().ToDataSourceResult(request));
        }

        public override JsonResult Create(DataSourceRequest request, HierarchyMemberViewModel viewModel)
        {
 	        throw new Exception(); 
        }

        public override JsonResult Update(DataSourceRequest request, HierarchyMemberViewModel viewModel)
        {
            throw new Exception();
        }

        public override JsonResult Destroy(DataSourceRequest request, HierarchyMemberViewModel viewModel)
        {
            throw new Exception();
        }


        public JsonResult Users_Read(string ID)
        {

            var users = _repo.Read().Where(i => i.CompanyID == CurrentUser.CompanyID && ((ID != null) ? i.ReportsToID.ToString() == ID : i.ReportsToID == null))
                                    .Include(i => i.Underlings)
                                    .ToList()
                                    .Select(i => _mapper.MapToViewModel(i));


            if (ID == null && users.Count() <= 0)
            {
                foreach(var i in _repo.Read().Where(i => i.CompanyID == CurrentUser.CompanyID).ToList()){
                    try { _repo.Delete(i); }
                    catch { }                    
                }
            }

            return Json(users, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddUser(string userId)
        {
            _repo.Create(new HierarchyMember 
                            { 
                                CompanyID = CurrentUser.CompanyID, 
                                ReportsToID = null,
                                UserId = userId
                            });

            return Json("Sucsess");
        }

        [HttpPost]
        public ActionResult DeleteUser(string id)
        {
            var model = _repo.Read().Where(i => i.ID.ToString() == id && i.CompanyID == CurrentUser.CompanyID).First();
            _repo.Delete(model);

            return Json("Sucsess");
        }


        [HttpPost]
        public ActionResult SwitchReportToUser(string id, string reportToID)
        {
            var model = _repo.Read().Where(i => i.ID.ToString() == id && i.CompanyID == CurrentUser.CompanyID).First();

            if (model.ReportsToID.ToString() != reportToID && !model.GetUnderlingIDs().Contains(Guid.Parse(reportToID)))
                model.ReportsToID = Guid.Parse(reportToID);

            _repo.Update(model, new string[] { "ReportsToID" });

            return Json("Sucsess");
        }

    }

}