﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ProspectsGridController : GridController<Prospect, ProspectGridViewModel>
    {
                
        public ProspectsGridController()
            : base(new ProspectRepository(), new ProspectGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText, int dateType, DateTime? fromDate, DateTime? toDate, bool existingCustomer = false)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            var exstingCustomers = new CompanyRepository().Read().Select(x => x.Email).ToList();

            var models = _repo.Read()
                                  .Where(x => filterText == "" || (x.CompanyName + " " + x.FirstName + " " + x.LastName).ToLower().Contains(filterText.ToLower()))
                                  .Where(x => x.Created >= fromDate && x.Created < toDate)
                                  .Where(x => existingCustomer == false || exstingCustomers.Contains(x.Email))
                                  .OrderByDescending(x => x.Created);


            var viewModels = models.Skip(request.PageSize * (request.Page - 1))
                                   .Take(request.PageSize)
                                   .ToList()
                                   .Select(s => _mapper.MapToViewModel(s));

            DataSourceResult result = new DataSourceResult() { Data = viewModels, Total = models.Count() };

            return Json(result);
        }


        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, ProspectGridViewModel viewModel)
        {

            if (ModelState.IsValid)
            {
                var model =  _repo.Find(Guid.Parse(viewModel.ID));

                _repo.Delete(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
    }

}