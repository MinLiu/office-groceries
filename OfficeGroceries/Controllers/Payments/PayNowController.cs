﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NLog;
using NuIngredient.Helpers;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    public class PayNowController : BaseController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        public async Task<ActionResult> Index(ZenPaymentInfoViewModel infoViewModel)
        {
            var sessionId = Guid.NewGuid();
            
            if (!infoViewModel.IsValid())
            {
                Logger.Info($"PaymentStart-InValid-{HttpContext?.Request?.Url?.PathAndQuery}");
                return Content("Not Valid");
            }
            
            Logger.Info($"PaymentStart-Valid-{sessionId}-{HttpContext?.Request?.Url?.PathAndQuery}");

            var opayoPayment = new OpayoPaymentViewModel(infoViewModel);
            var company =
                await new SnapDbContext().Companies.FirstOrDefaultAsync(x => x.AccountNo == opayoPayment.AccountNumber);

            if (company == null)
            {
                Logger.Info($"PaymentStart-{sessionId}-Couldn't find the company with account number {infoViewModel.AccountNumber}");
                return Content("Company Not Found");
            }

            opayoPayment.SetDeliveryInfo(company);
            opayoPayment.CryptField = OpayoHelper.RenderCryptField(opayoPayment);
            
            Logger.Info($"PaymentStart-{sessionId}-Crypt={opayoPayment.CryptField}");
            
            return View(opayoPayment);
        }

        public ActionResult PaymentSuccess(string crypt)
        {
            var info = OpayoHelper.Decrypt(crypt.Substring(1));
            
            Logger.Info($"PaymentSuccess-{info}");
            
            var viewModel = new OpayoPaymentResultViewModel(info);
            
            return View(viewModel);
        }

        public ActionResult PaymentFailure(string crypt)
        {
            var info = OpayoHelper.Decrypt(crypt.Substring(1));

            Logger.Info($"PaymentFailure-{info}");

            var viewModel = new OpayoPaymentResultViewModel(info);
            
            return View(viewModel);
        }

    }
}