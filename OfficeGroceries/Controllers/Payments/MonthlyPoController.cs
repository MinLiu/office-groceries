﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NLog;
using NuIngredient.Helpers;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    public class MonthlyPoController : BaseController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        public async Task<ActionResult> Index(ZenPoNumberViewModel infoViewModel)
        {
            var sessionId = Guid.NewGuid();
            
            if (!infoViewModel.IsValid())
            {
                Logger.Info($"MonthlyPoStart-InValid-{HttpContext?.Request?.Url?.PathAndQuery}");
                return Failed("Not Valid");
            }
            
            Logger.Info($"MonthlyPoStart-Valid-{sessionId}-{HttpContext?.Request?.Url?.PathAndQuery}");

            var company =
                await new SnapDbContext().Companies.FirstOrDefaultAsync(x => x.AccountNo == infoViewModel.AccountNumber);

            if (company == null)
            {
                Logger.Info($"MonthlyPoStart-{sessionId}-Couldn't find the company with account number {infoViewModel.AccountNumber}");
                return Failed("Company Not Found");
            }
            
            if (company.MonthlyPoNumbers.Any(x => x.InvoiceNumber == infoViewModel.InvoiceNumber))
            {
                Logger.Info($"MonthlyPoStart-{sessionId}-Already have a PO number for {infoViewModel.InvoiceNumber}");
                return Duplicate(company.Name);
            }

            var pageVm = new MonthlyPoPageViewModel
            {
                InvoiceNumber = infoViewModel.InvoiceNumber,
                Amount = infoViewModel.Amount,
                YearMonth = new DateTime(infoViewModel.Year, infoViewModel.Month, 1),
                CompanyName = company.Name,
                CompanyID = company.ID,
            };
            
            pageVm.SetHashCode();

            return View(pageVm);
        }

        public async Task<ActionResult> Submit(MonthlyPoPageViewModel pageVm)
        {
            if (!pageVm.IsValid())
            {
                Logger.Info($"MonthlyPoSubmit-InValid-{HttpContext?.Request?.Url?.PathAndQuery}");
                return Failed("Not Valid");
            }
            
            var sessionId = Guid.NewGuid();
            
            Logger.Info($"MonthlyPoSubmit-{sessionId}-{HttpContext?.Request?.Url?.PathAndQuery}");

            var company = await new SnapDbContext().Companies.FirstOrDefaultAsync(x => x.ID == pageVm.CompanyID);

            if (company == null)
            {
                Logger.Info($"MonthlyPoSubmit-{sessionId}-Couldn't find the company with ID {pageVm.CompanyID}");
                return Failed("Company Not Found");
            }
            
            if (company.MonthlyPoNumbers.Any(x => x.InvoiceNumber == pageVm.InvoiceNumber))
            {
                Logger.Info($"MonthlyPoSubmit-{sessionId}-Already have a PO number for {pageVm.InvoiceNumber}");
                return Duplicate(company.Name);
            }

            var poNumber = new MonthlyPoNumber
            {
                InvoiceMonth = pageVm.YearMonth,
                InvoiceNumber = pageVm.InvoiceNumber,
                InvoiceAmount = pageVm.Amount,
                PoNumber = pageVm.PoNumber,
                CompanyID = company.ID,
            };

            new MonthlyPoNumberRepository().Create(poNumber);
            
            Logger.Info($"MonthlyPoSubmit-{sessionId}-Success");
            
            return RedirectToAction("Success");
        }
        
        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Failed(string message)
        {
            return View("Failed", "", message);
        }

        public ActionResult Duplicate(string companyName)
        {
            return View("Duplicate", "", companyName);
        }
    }
}