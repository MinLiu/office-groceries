﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Threading.Tasks;
using NuIngredient.Models.Extenstions;
using System.Text.RegularExpressions;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User, Admin, User")]
    public class CompaniesController : EntityController<Company, CompanyViewModel>
    {

        public CompaniesController()
            : base(new CompanyRepository(), new CompanyMapper())
        {

        }

        // GET: Companies
        [Authorize(Roles = "NI-User")]
        public ActionResult Index(string ID)
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "NI-User")]
        public ActionResult _EditCompany(Guid id)
        {
            var viewModel = _mapper.MapToViewModel(_repo.Find(id));
            return PartialView(viewModel);
        }

        [HttpPost]
        [Authorize(Roles = "NI-User")]
        public ActionResult _EditCompany(CompanyViewModel viewModel)
        {
            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);
            ViewBag.Saved = true;

            return PartialView(_mapper.MapToViewModel(model));
        }

        [Authorize(Roles = "NI-User")]
        public ActionResult DropDownCompanies()
        {
            var viewModels = _repo.Read()
                                .Where(c => c.Name != "NuIngredient")
                                .ToList()
                                .Select(c => new SelectItemViewModel() { ID = c.ID.ToString(), Name = c.Name })
                                .ToList();


            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize(Roles = "NI-User")]
        public ActionResult _NewCompanySupplier(Guid companyID)
        {
            var viewModel = new CompanySupplierPageViewModel()
            {
                CompanyViewModel = _mapper.MapToViewModel(_repo.Find(companyID)),
                CompanySupplier = new CompanySupplierViewModel()
                {
                    CompanyID = companyID.ToString(),
                    DeliveryMon = true,
                    DeliveryTue = true,
                    DeliveryWed = true,
                    DeliveryThu = true,
                    DeliveryFri = true,
                    DeliverySat = true,
        }
            };
            return PartialView("_EditCompanySupplier", viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "NI-User")]
        public ActionResult _EditCompanySupplier(Guid id)
        {
            var companySupplierRepo = new CompanySupplierRepository();
            var companySuppliermapper = new CompanySupplierMapper();

            var model = companySupplierRepo.Find(id);
            var viewModel = new CompanySupplierPageViewModel()
            {
                CompanySupplier = companySuppliermapper.MapToViewModel(model),
                CompanyViewModel = _mapper.MapToViewModel(model.Company),
            };

            return PartialView("_EditCompanySupplier", viewModel);
        }

        [HttpPost]
        [Authorize(Roles = "NI-User")]
        public ActionResult _EditCompanySupplier(CompanySupplierPageViewModel viewModel)
        {
            var repo = new CompanySupplierRepository();
            var mapper = new CompanySupplierMapper();

            if (viewModel.CompanySupplier.ID == null)
            {
                // New 
                var model = new CompanySupplier();
                model = mapper.MapToModel(viewModel.CompanySupplier);
                repo.Create(model);


                // Create regular order (convert draft to regular)
                model = repo.Read()
                            .Where(x => x.ID == model.ID)
                            .Include(x => x.Supplier)
                            .FirstOrDefault();
                if (model.Supplier.SupplierType == SupplierType.Fruits)
                {
                    // Update Delivery Days
                    var company = _repo.Find(model.CompanyID);
                    company.FruitsDeliveryMonday = viewModel.CompanyViewModel.FruitsDeliveryMonday;
                    company.FruitsDeliveryTuesday = viewModel.CompanyViewModel.FruitsDeliveryTuesday;
                    company.FruitsDeliveryWednesday = viewModel.CompanyViewModel.FruitsDeliveryWednesday;
                    company.FruitsDeliveryThursday = viewModel.CompanyViewModel.FruitsDeliveryThursday;
                    company.FruitsDeliveryFriday = viewModel.CompanyViewModel.FruitsDeliveryFriday;
                    company.FruitsDeliverySaturday = viewModel.CompanyViewModel.FruitsDeliverySaturday;
                    _repo.Update(company);
                    UpdateDeliveryDays(company);

                    // Send initial order
                    if (model.StartDate.HasValue)
                    {
                        company.AskingForFruitSupplier = null;
                        _repo.Update(company);

                        var fruitOrderHeaderRepo = new FruitOrderHeaderRepository();
                        var fruitOrderHeader = fruitOrderHeaderRepo.Read()
                                                                   .Where(x => x.CompanyID == model.CompanyID && x.OrderMode == OrderMode.Draft)
                                                                   .FirstOrDefault();
                        if (fruitOrderHeader != null)
                        {
                            fruitOrderHeader.OrderMode = OrderMode.Regular;
                            fruitOrderHeaderRepo.Update(fruitOrderHeader);
                        }
                        var controller = new FruitOrdersController();
                        controller.SendOrderInitializeMail(model.CompanyID, model.StartDate.Value, model);
                    }
                }
                else if (model.Supplier.SupplierType == SupplierType.Milk)
                {
                    // Update Delivery Days
                    var company = _repo.Find(model.CompanyID);
                    company.MilkDeliveryMonday = viewModel.CompanyViewModel.MilkDeliveryMonday;
                    company.MilkDeliveryTuesday = viewModel.CompanyViewModel.MilkDeliveryTuesday;
                    company.MilkDeliveryWednesday = viewModel.CompanyViewModel.MilkDeliveryWednesday;
                    company.MilkDeliveryThursday = viewModel.CompanyViewModel.MilkDeliveryThursday;
                    company.MilkDeliveryFriday = viewModel.CompanyViewModel.MilkDeliveryFriday;
                    company.MilkDeliverySaturday = viewModel.CompanyViewModel.MilkDeliverySaturday;
                    _repo.Update(company);
                    UpdateDeliveryDays(company);

                    // Send initial order
                    if (model.StartDate.HasValue)
                    {
                        company.AskingForMilkSupplier = null;
                        _repo.Update(company);

                        var milkOrderHeaderRepo = new MilkOrderHeaderRepository();
                        var fruitOrderHeader = milkOrderHeaderRepo.Read()
                                                                   .Where(x => x.CompanyID == model.CompanyID && x.OrderMode == OrderMode.Draft)
                                                                   .FirstOrDefault();
                        if (fruitOrderHeader != null)
                        {
                            fruitOrderHeader.OrderMode = OrderMode.Regular;
                            milkOrderHeaderRepo.Update(fruitOrderHeader);
                        }
                        var controller = new MilkOrdersController();
                        controller.SendOrderInitializeMail(model.CompanyID, model.StartDate.Value, model);
                    }
                }
                else if (model.Supplier.SupplierType == SupplierType.DryGoods)
                {
                    // Update Delivery Days
                    //var company = _repo.Find(model.CompanyID);
                    //company.DryGoodsDeliveryMonday = viewModel.CompanyViewModel.DryGoodsDeliveryMonday;
                    //company.DryGoodsDeliveryTuesday = viewModel.CompanyViewModel.DryGoodsDeliveryTuesday;
                    //company.DryGoodsDeliveryWednesday = viewModel.CompanyViewModel.DryGoodsDeliveryWednesday;
                    //company.DryGoodsDeliveryThursday = viewModel.CompanyViewModel.DryGoodsDeliveryThursday;
                    //company.DryGoodsDeliveryFriday = viewModel.CompanyViewModel.DryGoodsDeliveryFriday;
                    //company.DryGoodsDeliverySaturday = viewModel.CompanyViewModel.DryGoodsDeliverySaturday;
                    //_repo.Update(company);
                    //UpdateDeliveryDays(company);

                    if (model.StartDate.HasValue)
                    {
                        //company.AskingForDryGoodsSupplier = null;
                        //_repo.Update(company);
                        var requestLogRepo = new SupAcctRequestLogRepository();
                        var logs = requestLogRepo.Read().Where(x => x.CompanyID == model.CompanyID && x.SupplierID == model.SupplierID);
                        requestLogRepo.Delete(logs);

                        var controller = new ProductsController();
                        controller.SendOrderInitializeMail(model.ID);
                    }

            }
        }
            else
            {
                // Edit
                var model = repo.Find(Guid.Parse(viewModel.CompanySupplier.ID));

                if (model.Supplier.SupplierType == SupplierType.Fruits)
                {
                    // Update Delivery Days
                    var company = _repo.Find(model.CompanyID);
                    company.FruitsDeliveryMonday = viewModel.CompanyViewModel.FruitsDeliveryMonday;
                    company.FruitsDeliveryTuesday = viewModel.CompanyViewModel.FruitsDeliveryTuesday;
                    company.FruitsDeliveryWednesday = viewModel.CompanyViewModel.FruitsDeliveryWednesday;
                    company.FruitsDeliveryThursday = viewModel.CompanyViewModel.FruitsDeliveryThursday;
                    company.FruitsDeliveryFriday = viewModel.CompanyViewModel.FruitsDeliveryFriday;
                    company.FruitsDeliverySaturday = viewModel.CompanyViewModel.FruitsDeliverySaturday;
                    _repo.Update(company);
                    UpdateDeliveryDays(company);

                    if (viewModel.CompanySupplier.StartDate.HasValue && (model.StartDate != viewModel.CompanySupplier.StartDate))
                    {
                        company.AskingForFruitSupplier = null;
                        _repo.Update(company);

                        mapper.MapToModel(viewModel.CompanySupplier, model);
                        repo.Update(model);

                        var fruitOrderHeaderRepo = new FruitOrderHeaderRepository();
                        var fruitOrderHeader = fruitOrderHeaderRepo.Read()
                                                                   .Where(x => x.CompanyID == model.CompanyID && x.OrderMode == OrderMode.Draft)
                                                                   .FirstOrDefault();

                        if (fruitOrderHeader != null)
                        {
                            fruitOrderHeader.OrderMode = OrderMode.Regular;
                            fruitOrderHeaderRepo.Update(fruitOrderHeader);
                        }
                        var controller = new FruitOrdersController();
                        controller.SendOrderInitializeMail(model.CompanyID, model.StartDate.Value, model);
                    }
                    else
                    {
                        mapper.MapToModel(viewModel.CompanySupplier, model);
                        repo.Update(model);
                    }
                }
                else if (model.Supplier.SupplierType == SupplierType.Milk)
                {
                    // Update Delivery Days
                    var company = _repo.Find(model.CompanyID);
                    company.MilkDeliveryMonday = viewModel.CompanyViewModel.MilkDeliveryMonday;
                    company.MilkDeliveryTuesday = viewModel.CompanyViewModel.MilkDeliveryTuesday;
                    company.MilkDeliveryWednesday = viewModel.CompanyViewModel.MilkDeliveryWednesday;
                    company.MilkDeliveryThursday = viewModel.CompanyViewModel.MilkDeliveryThursday;
                    company.MilkDeliveryFriday = viewModel.CompanyViewModel.MilkDeliveryFriday;
                    company.MilkDeliverySaturday = viewModel.CompanyViewModel.MilkDeliverySaturday;
                    _repo.Update(company);
                    UpdateDeliveryDays(company);

                    if (viewModel.CompanySupplier.StartDate.HasValue && (model.StartDate != viewModel.CompanySupplier.StartDate))
                    {
                        company.AskingForMilkSupplier = null;
                        _repo.Update(company);

                        mapper.MapToModel(viewModel.CompanySupplier, model);
                        repo.Update(model);

                        var milkOrderHeaderRepo = new MilkOrderHeaderRepository();
                        var milkOrderHeader = milkOrderHeaderRepo.Read()
                                                                   .Where(x => x.CompanyID == model.CompanyID && x.OrderMode == OrderMode.Draft)
                                                                   .FirstOrDefault();

                        if (milkOrderHeader != null)
                        {
                            milkOrderHeader.OrderMode = OrderMode.Regular;
                            milkOrderHeaderRepo.Update(milkOrderHeader);
                        }
                        var controller = new MilkOrdersController();
                        controller.SendOrderInitializeMail(model.CompanyID, model.StartDate.Value, model);
                    }
                    else
                    {
                        mapper.MapToModel(viewModel.CompanySupplier, model);
                        repo.Update(model);
                    }
                }
                else if (model.Supplier.SupplierType == SupplierType.DryGoods)
                {
                    // Update Delivery Days
                    var company = _repo.Find(model.CompanyID);
                    //company.DryGoodsDeliveryMonday = viewModel.CompanyViewModel.DryGoodsDeliveryMonday;
                    //company.DryGoodsDeliveryTuesday = viewModel.CompanyViewModel.DryGoodsDeliveryTuesday;
                    //company.DryGoodsDeliveryWednesday = viewModel.CompanyViewModel.DryGoodsDeliveryWednesday;
                    //company.DryGoodsDeliveryThursday = viewModel.CompanyViewModel.DryGoodsDeliveryThursday;
                    //company.DryGoodsDeliveryFriday = viewModel.CompanyViewModel.DryGoodsDeliveryFriday;
                    //company.DryGoodsDeliverySaturday = viewModel.CompanyViewModel.DryGoodsDeliverySaturday;
                    //_repo.Update(company);
                    //UpdateDeliveryDays(company);

                    if (viewModel.CompanySupplier.StartDate.HasValue && (model.StartDate != viewModel.CompanySupplier.StartDate))
                    {
                        //company.AskingForDryGoodsSupplier = null;
                        //_repo.Update(company);

                        var requestLogRepo = new SupAcctRequestLogRepository();
                        var logs = requestLogRepo.Read().Where(x => x.CompanyID == model.CompanyID && x.SupplierID == model.SupplierID);
                        requestLogRepo.Delete(logs);

                        mapper.MapToModel(viewModel.CompanySupplier, model);
                        repo.Update(model);
                        var controller = new ProductsController();
                        controller.SendOrderInitializeMail(model.ID);
                    }
                    else
                    {
                        mapper.MapToModel(viewModel.CompanySupplier, model);
                        repo.Update(model);
                    }
                }

            }

            return Json(new { status = "Success" });
        }

        [HttpPost]
        public ActionResult AskForDryGoodsSupplier(string supplierID = null)
        {
            var supplierIDs = supplierID != null ? new List<Guid> { Guid.Parse(supplierID) } : null;

            var helper = new RequestDryGoodsAcctHelper();
            var success = helper.RequestDryGoodsSupplier(CurrentUser.Company.ID, supplierIDs);
            
            return success ? Json(new { Success = true }) : Json(new { Fail = true });
        }

        [HttpPost]
        public ActionResult AskForFruitSnackSupplier()
        {
            var success = false;
            
            var requestFruitAcctHelper = new RequestFruitAcctHelper();
            if (requestFruitAcctHelper.RequestFruitSupplier(CurrentUser.Company.ID))
                success = true;
            
            var requestSnackAcctHelper = new RequestSnackAcctHelper();
            if (requestSnackAcctHelper.RequestSnackSupplier(CurrentUser.Company.ID))
                success = true;

            return success ? Json(new { Success = true }) : Json(new { Fail = true });
        }
        
        [HttpPost]
        public ActionResult AskForFruitSupplier()
        {
            var helper = new RequestFruitAcctHelper();
            var success = helper.RequestFruitSupplier(CurrentUser.Company.ID);

            return success ? Json(new { Success = true }) : Json(new { Fail = true });
        }

        [HttpPost]
        public ActionResult AskForSnackSupplier()
        {
            var helper = new RequestSnackAcctHelper();
            var success = helper.RequestSnackSupplier(CurrentUser.Company.ID);

            return success ? Json(new { Success = true }) : Json(new { Fail = true });
        }
        
        [HttpPost]
        public ActionResult AskForMilkSupplier(string id)
        {
            var helper = new RequestMilkAcctHelper();
            var success = helper.RequestMilkSupplier(CurrentUser.Company.ID);

            return success ? Json(new { Success = true }) : Json(new { Fail = true });
        }

        #region Helper

        protected void UpdateDeliveryDays(Company company)
        {
            UpdateFruitOrderDeliveryDays(company);
            UpdateMilkOrderDeliveryDays(company);
        }

        protected void UpdateFruitOrderDeliveryDays(Company company)
        {
            var headerRepo = new FruitOrderHeaderRepository();
            var itemRepo = new FruitOrderRepository();
            var orderHeaders = headerRepo.Read().Where(x => x.CompanyID == company.ID)
                                          .Where(x => x.OrderMode == OrderMode.Draft || x.OrderMode == OrderMode.Regular || (x.OrderMode == OrderMode.OneOff && x.ToDate > DateTime.Today))
                                          .ToList();

            foreach (var header in orderHeaders)
            {
                var items = itemRepo.Read()
                                    .Where(x => x.OrderHeaderID == header.ID)
                                    .ToList();
                foreach (var item in items)
                {
                    if (!company.FruitsDeliveryMonday) item.Monday = 0;
                    if (!company.FruitsDeliveryTuesday) item.Tuesday = 0;
                    if (!company.FruitsDeliveryWednesday) item.Wednesday = 0;
                    if (!company.FruitsDeliveryThursday) item.Thursday = 0;
                    if (!company.FruitsDeliveryFriday) item.Friday = 0;
                    if (!company.FruitsDeliverySaturday) item.Saturday = 0;
                    if (!company.FruitsDeliverySunday) item.Sunday = 0;

                    item.TotalNet = item.Price * (item.Monday + item.Tuesday + item.Wednesday + item.Thursday + item.Friday + item.Saturday + item.Sunday);
                    item.TotalVAT = item.VATRate * item.TotalNet;
                    itemRepo.Update(item);
                }

                new FruitOrdersController().RecalculateOrderHeader(header.ID);
            }
        }

        protected void UpdateMilkOrderDeliveryDays(Company company)
        {
            var headerRepo = new MilkOrderHeaderRepository();
            var itemRepo = new MilkOrderRepository();
            var orderHeaders = headerRepo.Read().Where(x => x.CompanyID == company.ID)
                                          .Where(x => x.OrderMode == OrderMode.Draft || x.OrderMode == OrderMode.Regular || (x.OrderMode == OrderMode.OneOff && x.ToDate > DateTime.Today))
                                          .ToList();

            foreach (var header in orderHeaders)
            {
                var items = itemRepo.Read()
                                    .Where(x => x.OrderHeaderID == header.ID)
                                    .ToList();
                foreach (var item in items)
                {
                    if (!company.MilkDeliveryMonday) item.Monday = 0;
                    if (!company.MilkDeliveryTuesday) item.Tuesday = 0;
                    if (!company.MilkDeliveryWednesday) item.Wednesday = 0;
                    if (!company.MilkDeliveryThursday) item.Thursday = 0;
                    if (!company.MilkDeliveryFriday) item.Friday = 0;
                    if (!company.MilkDeliverySaturday) item.Saturday = 0;
                    if (!company.MilkDeliverySunday) item.Sunday = 0;

                    item.TotalNet = item.Price * (item.Monday + item.Tuesday + item.Wednesday + item.Thursday + item.Friday + item.Saturday + item.Sunday);
                    item.TotalVAT = item.VATRate * item.TotalNet;
                    itemRepo.Update(item);
                }

                new MilkOrdersController().RecalculateOrderHeader(header.ID);
            }
        }

        public void SendAccountSetUpEmail(Company company, string category)
        {
            var mailMessage = AccountSetupEmailHelper.GetEmail(company, category);
            EmailService.SendEmail(mailMessage, Server);
        }

        #endregion

        public ActionResult CancelRequestingMilkSupplier(Guid id)
        {
            using (var db = new SnapDbContext())
            {
                db.Companies.Where(x => x.ID == id).First().AskingForMilkSupplier = null;
                db.SaveChanges();
            }
            return Json(new { Success = true });
        }

        public ActionResult CancelRequestingFruitSupplier(Guid id)
        {
            using (var db = new SnapDbContext())
            {
                db.Companies.Where(x => x.ID == id).First().AskingForFruitSupplier = null;
                db.SaveChanges();
            }
            return Json(new { Success = true });
        }
        
        public ActionResult CancelRequestingSnackSupplier(Guid id)
        {
            using (var db = new SnapDbContext())
            {
                db.Companies.Where(x => x.ID == id).First().AskingForSnackSupplier = null;
                db.SaveChanges();
            }
            return Json(new { Success = true });
        }

        public ActionResult CancelRequestingDryGoodsSupplier(Guid id, Guid SupplierID)
        {
            //using (var db = new SnapDbContext())
            //{
                //db.Companies.Where(x => x.ID == id).First().AskingForDryGoodsSupplier = null;
                //db.SaveChanges();
            //}
            var requestLogRepo = new SupAcctRequestLogRepository();
            var requestLogs = requestLogRepo.Read().Where(x => x.CompanyID == id && x.SupplierID == SupplierID);
            requestLogRepo.Delete(requestLogs);
            return Json(new { Success = true });
        }

        public async Task<ActionResult> SendResetPasswordMail(string id)
        {
            var user = UserManager.FindById(id);
            var controller = new AccountController();
            controller.InitializeController(this.ControllerContext.RequestContext);

            await controller.SendResetPasswordEmail(user);

            return Json(new { Success = true });
        }

    }
    
}