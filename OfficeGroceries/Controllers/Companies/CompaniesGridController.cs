﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class CompaniesGridController :GridController<Company, CompanyGridViewModel>
    {
      
        public CompaniesGridController()
            : base(new CompanyRepository(), new CompanyGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText)
        {
            var models = _repo.Read().Where(c => filterText == "" || c.Name.ToLower().Contains(filterText.ToLower()) || c.AccountNo.ToLower().Contains(filterText.ToLower()))
                                     .Where(c => c.Name != "NuIngredient");
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, CompanyGridViewModel viewModel)
    {
            // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
            var model = new Company { ID = Guid.Parse(viewModel.ID) };
            _repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

    }
}