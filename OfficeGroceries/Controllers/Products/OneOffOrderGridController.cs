﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class OneOffOrderGridController : GridController<OneOffOrder, OneOffOrderGridViewModel>
    {

        public OneOffOrderGridController()
            : base(new OneOffOrderRepository(), new OneOffOrderGridMapper())
        {

        }

        [HttpPost]
        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            if (User.IsInRole("NI-User"))
            {
                throw new NotImplementedException();
            }
            else
            {
                var viewModels = _repo.Read()
                                      .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                      .OrderByDescending(x => x.Created)
                                      .ToList()
                                      .Select(x => _mapper.MapToViewModel(x));
                return Json(viewModels.ToDataSourceResult(request));
            }
        }

        [HttpPost]
        [Authorize(Roles = "NI-User")]
        public JsonResult ReadAll([DataSourceRequest] DataSourceRequest request, string filterText, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate, List<OneOffOrderStatus> statuses)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            if (statuses == null)
                statuses = new List<OneOffOrderStatus>();

            var viewModels = _repo.Read()
                                  .Where(x => x.Created >= fromDate && x.Created < toDate)
                                  .Where(x => companyID == null || x.CompanyID == companyID)
                                  .Where(x => statuses.Contains(x.Status))
                                  .Where(x => filterText == null || filterText == "" || x.OrderNumber.ToLower().Contains(filterText.ToLower()))
                                  .OrderByDescending(x => x.Created)
                                  .ToList()
                                  .Select(x => _mapper.MapToViewModel(x));
            return Json(viewModels.ToDataSourceResult(request));
        }
    }
}