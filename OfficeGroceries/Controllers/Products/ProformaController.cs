﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Fruitful.Email;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    public class ProformaController : BaseController
    {
        public async Task<ActionResult> ActionOrder(Guid orderId, bool accept, string checksum)
        {
            var pageVm = new ActionOrderPageVm();
            
            var repo = new OneOffOrderRepository();
            var order = await repo.FindAsync(orderId);
            
            if (order == null)
            {
                return HttpNotFound();
            }
            
            // var orderChecksum = order.GetChecksum();
            //
            // if (orderChecksum != checksum)
            // {
            //     return HttpNotFound();
            // }

            pageVm.Order = new ProformaOrderMapper().MapToViewModel(order);
            pageVm.Order.DeliveryDate = order.Company.NextDryGoodsDeliveryDate(order.SupplierID);
            pageVm.Approver = new ProformaApproverMapper().MapToViewModel(order.Company.ProformaApprover);
            pageVm.Accept = accept;

            return View(pageVm);
        }

        [HttpPost]
        public async Task<ActionResult> Approve(Guid orderId, string poNumber)
        {
            var repo = new OneOffOrderRepository();
            var order = await repo.FindAsync(orderId);

            if (order.Status != OneOffOrderStatus.Proforma)
            {
                throw new InvalidOperationException("Order is not in proforma status");
            }
            
            order.PONumber = poNumber;
            order.Status = OneOffOrderStatus.Accepted;
            order.OriginalCreated = order.Created;
            order.Created = DateTime.Now;
            order.LastUpdated = DateTime.Now;
            order.DeliveryDate = order.Company.NextDryGoodsDeliveryDate(order.SupplierID);
            
            repo.Update(order);
            
            new DryGoodsOrderPlacedMailHelper().SendMail(order);
            
            TempData["OrderId"] = orderId;
            
            return RedirectToAction("Success");
        }
        
        [HttpPost]
        public async Task<ActionResult> Reject(Guid orderId, string reason)
        {
            var orderRepo = new OneOffOrderRepository();
            var order = await orderRepo.FindAsync(orderId);
            
            var mailMessage = ProformaRejectedEmailHelper.GetEmail(order.CompanyID, order.ID, order.Company.NextDryGoodsDeliveryDate(order.SupplierID), reason);
            new Emailer(new SnapDbContext()).SendEmail(mailMessage);
            
            TempData["OrderId"] = orderId;
            return RedirectToAction("Rejected");
        }
        
        public ActionResult Success()
        {
            if(TempData["OrderId"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            
            var orderId = (Guid)TempData["OrderId"];
            
            var repo = new OneOffOrderRepository();
            var order = repo.Find(orderId);
            
            if (order == null)
            {
                return HttpNotFound();
            }
            
            var vm = new OneOffOrderMapper().MapToViewModel(order);
            
            return View(vm);
        }

        public ActionResult Rejected()
        {
            if(TempData["OrderId"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }
        
    }
}