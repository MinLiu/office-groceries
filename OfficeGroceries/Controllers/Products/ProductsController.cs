﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Web.Hosting;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Controllers
{
    public class ProductsController : EntityController<Product, ProductViewModel>
    {
        public ProductsController()
            : base(new ProductRepository(), new ProductMapper())
        {

        }

        [HttpGet]
        public ActionResult Aisle(string companyID, string prospectToken, string id, int page = 1, string keywords = "", string category = "")
        {
            // for fixing google ads error
            if (id == "pantry")
            {
                id = "pantry-aisle";
            }

            var aisle = new AisleRepository().Read().Where(x => x.InternalName == id).FirstOrDefault();

            var viewModel = new AislePageViewModel(aisle, page, keywords, category);

            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    // NI User
                    if (!string.IsNullOrEmpty(companyID))
                    {
                        ViewBag.CompanyID = companyID;
                        viewModel.Set(new SnapDbContext().Companies.Find(Guid.Parse(companyID)));
                    }
                    else if (!string.IsNullOrEmpty(prospectToken))
                    {
                        ViewBag.ProspectToken = prospectToken;
                        viewModel.Set(null);
                    }
                    else
                    { 
                        viewModel.Set(CurrentUser.Company);
                    }
                }
                else
                {
                    // Member
                    viewModel.Set(CurrentUser.Company);
                }
            }
            else
            {
                viewModel.Set(null);
            }
            ViewBag.MetaTitle = viewModel.Aisle.MetaTitle;
            ViewBag.MetaKeyword = viewModel.Aisle.MetaKeyword;
            ViewBag.MetaDescription = viewModel.Aisle.MetaDescription;

            return View(viewModel);
        }

        public ActionResult Goto(string companyID, string prospectToken, string aisle)
        {
            //if (!string.IsNullOrEmpty(companyID))
            //{
            //    return RedirectToAction("Index", new { companyID = companyID });
            //}
            //else
            //{
            //    return RedirectToAction("Index", new { prospectToken = prospectToken });
            //}
            if (!string.IsNullOrEmpty(companyID))
            {
                return RedirectToAction("Aisle", new { companyID = companyID, id = aisle });
            }
            else
            {
                return RedirectToAction("Aisle", new { prospectToken = prospectToken, id = aisle });
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult OrderHistories()
        {
            var repo = new OneOffOrderRepository();
            var mapper = new OneOffOrderGridMapper();
            var orders = repo.Read()
                            .Where(x => x.CompanyID == CurrentUser.CompanyID)
                            .OrderByDescending(x => x.Created)
                            .ToList()
                            .Select(x => mapper.MapToViewModel(x)).ToList();
            return View(orders);
        }

        [HttpGet]
        [Authorize]
        public ActionResult _OneOffOrderDetails(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var mapper = new OneOffOrderMapper();
            var model = repo.Find(id);

            return PartialView(mapper.MapToViewModel(model));
        }

        [HttpGet]
        public ActionResult Detail(string id)
        {
            var service = new ProductService<ProductViewModel>(new ProductMapper());
            Product product = null;
            if (Guid.TryParse(id, out var guid))
            {
                product = _repo.Find(guid);
            }
            else
            { 
                product = service.FindByInternalName(id);
            }

            if (product == null)
            {
                return RedirectToAction("Index", "Home");
            }
            
            Company company = null;
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    // NI-Users
                    company = CurrentUser.Company;
                }
                else
                {
                    // Members
                    company = CurrentUser.Company;
                }
            }

            var viewModel = new ProductDetailPageViewModel(company, product)
            {
            };

            return View(viewModel);
        }

        #region Shopping Cart

        [HttpGet]
        public ActionResult _ShoppingCart(Guid? companyID)
        {
            var repo = new ShoppingCartItemRepository();
            var mapper = new ShoppingCartItemMapper();

            var cartItems = new List<ShoppingCartItemViewModel>();
            var cartItemsBySuppliers = new List<CartItemsBySuppliers>();
            Company company = null;
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    // NI-Users
                    if (companyID == null)
                        throw new NotImplementedException();
                    company = new CompanyRepository().Find(companyID);
                }
                else
                {
                    // Members
                    company = CurrentUser.Company;
                }
                cartItemsBySuppliers = repo.Read()
                                           .Where(x => x.CompanyID == company.ID)
                                           .Where(x => x.Product.Deleted == false)
                                           .Where(x => x.Product.IsActive == true)
                                           .Where(x => x.Product.IsExclusive == false || x.Product.ExclusivePrices.Where(ep => ep.CusomterID == company.ID).Count() > 0)
                                           .GroupBy(x => x.Product.Supplier)
                                           .ToList()
                                           .Select(g => new CartItemsBySuppliers
                                           {
                                               Supplier = new SupplierMapper().MapToViewModel(g.Key),
                                               CartItems = g.Select(i => mapper.MapToViewModel(i)).ToList(),
                                               SubNet = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalNet) : 0,
                                               SubVAT = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalVAT) : 0,
                                               SubGross = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalGross) : 0,
                                               Live = (g.Key.UseMasterAccount && company.EasySuppliersActivated) || company.Method_IsLive(g.Key)
                                           })
                                           .Where(x => x.Live)
                                           .ToList();
            }
            else
            {
                // Prospects
                cartItemsBySuppliers = repo.Read()
                                           .Where(x => x.ProspectToken == ProspectToken)
                                           .Where(x => x.Product.Deleted == false)
                                           .Where(x => x.Product.IsActive == true)
                                           .Where(x => x.Product.IsExclusive == false)
                                           .Where(x => x.Product.IsProspectsVisible == true)
                                           .GroupBy(x => x.Product.Supplier)
                                           .ToList()
                                           .Select(g => new CartItemsBySuppliers
                                           {
                                               Supplier = new SupplierMapper().MapToViewModel(g.Key),
                                               CartItems = g.Select(i => mapper.MapToViewModel(i)).ToList(),
                                               SubNet = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalNet) : 0,
                                               SubVAT = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalVAT) : 0,
                                               SubGross = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalGross) : 0,
                                           })
                                           .ToList();
            }


            var viewModel = new ShoppingCartPageViewModel()
            {
                CartItems = cartItems,
                CartItemsBySuppliers = cartItemsBySuppliers,
                Gross = cartItemsBySuppliers.Count() > 0 ? cartItemsBySuppliers.Sum(x => x.SubGross) : 0,
                Net = cartItemsBySuppliers.Count() > 0 ? cartItemsBySuppliers.Sum(x => x.SubNet) : 0,
                VAT = cartItemsBySuppliers.Count() > 0 ? cartItemsBySuppliers.Sum(x => x.SubVAT) : 0,
                //LiveOnDryGoods = company == null ? false : company.Method_IsLive(SupplierType.DryGoods),
                //AskingForDryGoodsSupplier = company == null ? false : company.AskingForDryGoodsSupplier != null,
            };
            viewModel.Set(company);
            
            return PartialView(viewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SendQuote(Guid? companyID)
        {
            Company company;
            if (User.IsInRole("NI-User"))
            {
                // NI-Users
                if (companyID == null)
                    throw new NotImplementedException();
                company = new CompanyRepository().Find(companyID);
            }
            else
            {
                // Members
                company = CurrentUser.Company;
            }

            SendQuoteMailToCustomer(company);
            //ClearShoppingCart(company);

            return Json(new { Success = true });
        }

        #region Quantity Change

        [HttpPost]
        public ActionResult AddToCart(QuoteProductViewModel quote, Guid? companyID)
        {
            var repo = new ShoppingCartItemRepository();

            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    // NI-User
                    if (companyID == null)
                        throw new NotImplementedException();

                    var cartItem = repo.Read()
                                       .Where(x => x.CompanyID == companyID)
                                       .Where(x => x.ProductID == quote.ProductID)
                                       .FirstOrDefault();
                    if (cartItem == null)
                    {
                        cartItem = new ShoppingCartItem()
                        {
                            CompanyID = companyID,
                            ProductID = quote.ProductID,
                            Qty = 0,
                            UpdateTime = DateTime.Now
                        };
                        repo.Create(cartItem);
                    }

                    cartItem.Qty += quote.Qty;
                    repo.Update(cartItem);
                }
                else
                {
                    // Members
                    var cartItem = repo.Read()
                                       .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                       .Where(x => x.ProductID == quote.ProductID)
                                       .FirstOrDefault();
                    if (cartItem == null)
                    {
                        cartItem = new ShoppingCartItem()
                        {
                            CompanyID = CurrentUser.CompanyID,
                            ProductID = quote.ProductID,
                            Qty = 0,
                            UpdateTime = DateTime.Now
                        };
                        repo.Create(cartItem);
                    }

                    cartItem.Qty += quote.Qty;
                    repo.Update(cartItem);
                }
            }
            else
            {
                // Prospects
                var cartItem = repo.Read()
                                   .Where(x => x.ProspectToken == ProspectToken)
                                   .Where(x => x.ProductID == quote.ProductID)
                                   .FirstOrDefault();
                if (cartItem == null)
                {
                    cartItem = new ShoppingCartItem()
                    {
                        ProspectToken = ProspectToken,
                        ProductID = quote.ProductID,
                        Qty = 0,
                        UpdateTime = DateTime.Now
                    };
                    repo.Create(cartItem);
                }

                cartItem.Qty += quote.Qty;
                repo.Update(cartItem);
            }

            return Json(new { Success = true });
        }

        [HttpPost]
        public ActionResult ChangeCartItemQty(QuoteProductViewModel quote, Guid? companyID)
        {
            var repo = new ShoppingCartItemRepository();
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    // NI-User
                    if (companyID == null)
                        throw new NotImplementedException();

                    var cartItem = repo.Read()
                                       .Where(x => x.CompanyID == companyID)
                                       .Where(x => x.ProductID == quote.ProductID)
                                       .FirstOrDefault();
                    if (cartItem == null)
                    {
                        cartItem = new ShoppingCartItem()
                        {
                            CompanyID = companyID,
                            ProductID = quote.ProductID,
                            Qty = 0,
                            UpdateTime = DateTime.Now
                        };
                        repo.Create(cartItem);
                    }

                    cartItem.Qty = quote.Qty;
                    repo.Update(cartItem);
                }
                else
                {
                    // Members
                    var cartItem = repo.Read()
                                       .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                       .Where(x => x.ProductID == quote.ProductID)
                                       .FirstOrDefault();
                    if (cartItem == null)
                    {
                        cartItem = new ShoppingCartItem()
                        {
                            CompanyID = CurrentUser.CompanyID,
                            ProductID = quote.ProductID,
                            Qty = 0,
                            UpdateTime = DateTime.Now
                        };
                        repo.Create(cartItem);
                    }

                    cartItem.Qty = quote.Qty;
                    repo.Update(cartItem);
                }
            }
            else
            {
                // Prospects
                var cartItem = repo.Read()
                                   .Where(x => x.ProspectToken == ProspectToken)
                                   .Where(x => x.ProductID == quote.ProductID)
                                   .FirstOrDefault();
                if (cartItem == null)
                {
                    cartItem = new ShoppingCartItem()
                    {
                        ProspectToken = ProspectToken,
                        ProductID = quote.ProductID,
                        Qty = 0,
                        UpdateTime = DateTime.Now
                    };
                    repo.Create(cartItem);
                }

                cartItem.Qty = quote.Qty;
                repo.Update(cartItem);
            }

            return Json(new { Success = true });
        }

        [HttpPost]
        public ActionResult RemoveCartItem(Guid productID, Guid? companyID)
        {
            var repo = new ShoppingCartItemRepository();
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    // NI-User
                    if (companyID == null)
                        throw new NotImplementedException();

                    var cartItem = repo.Read()
                                       .Where(x => x.CompanyID == companyID)
                                       .Where(x => x.ProductID == productID)
                                       .FirstOrDefault();
                    if (cartItem != null)
                    {
                        repo.Delete(cartItem);
                    }
                }
                else
                {
                    // Members
                    var cartItem = repo.Read()
                                       .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                       .Where(x => x.ProductID == productID)
                                       .FirstOrDefault();
                    if (cartItem != null)
                    {
                        repo.Delete(cartItem);
                    }
                }
            }
            else
            {
                // Prospects
                var cartItem = repo.Read()
                                   .Where(x => x.ProspectToken == ProspectToken)
                                   .Where(x => x.ProductID == productID)
                                   .FirstOrDefault();
                if (cartItem != null)
                {
                    repo.Delete(cartItem);
                }
            }

            return Json(new { Success = true });
        }

        [HttpPost]
        public ActionResult CopyToCart(Guid orderID, Guid? companyID)
        {
            var orderItemRepo = new OneOffOrderItemRepository();
            var cartItemRepo = new ShoppingCartItemRepository();

            if (User.IsInRole("NI-User"))
            {
                // NI-Users
                throw new NotImplementedException();
            }
            else
            {
                // Members
                var items = orderItemRepo.Read()
                                         .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                         .Where(x => x.OrderHeaderID == orderID)
                                         .ToList();
                foreach (var item in items)
                {
                    if (item.Product != null 
                        && item.Product.Deleted == false 
                        && item.Product.IsActive == true 
                        && (item.Product.IsExclusive == false || item.Product.ExclusivePrices.Where(x => x.CusomterID == CurrentUser.CompanyID).Count() > 0))
                    {
                        var cartItem = cartItemRepo.Read()
                                                   .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                                   .Where(x => x.ProductID == item.ProductID)
                                                   .FirstOrDefault();
                        if (cartItem == null)
                        {
                            cartItem = new ShoppingCartItem()
                            {
                                CompanyID = CurrentUser.CompanyID,
                                ProductID = item.ProductID.Value,
                                Qty = 0,
                                UpdateTime = DateTime.Now
                            };
                            cartItemRepo.Create(cartItem);
                        }

                        cartItem.Qty += item.Qty;
                        cartItemRepo.Update(cartItem);
                    }
                }
            }
            return Json(new { Success = true });
        }

        #endregion

        #region Check Out

        [HttpGet]
        [Authorize]
        public ActionResult CheckOut(Guid? companyID)
        {
            var repo = new ShoppingCartItemRepository();
            var mapper = new ShoppingCartItemMapper();

            //var cartItems = new List<ShoppingCartItemViewModel>();
            var cartItemsBySuppliers = new List<CartItemsBySuppliers>();
            Company company;
            if (User.IsInRole("NI-User"))
            {
                // NI-Users
                if (companyID == null)
                    throw new NotImplementedException();

                company = new CompanyRepository().Find(companyID);

                //cartItems = repo.Read()
                //                .Where(x => x.CompanyID == companyID)
                //                .Where(x => x.Product.Deleted == false)
                //                .Where(x => x.Product.IsActive == true)
                //                .Where(x => x.Product.IsExclusive == false || x.Product.ExclusivePrices.Where(ep => ep.CusomterID == companyID).Count() > 0)
                //                .ToList()
                //                .Select(x => mapper.MapToViewModel(x, company))
                //                .ToList();

            }
            else
            {
                // Members
                company = CurrentUser.Company;

                //cartItems = repo.Read()
                //                .Where(x => x.CompanyID == CurrentUser.CompanyID)
                //                .Where(x => x.Product.Deleted == false)
                //                .Where(x => x.Product.IsActive == true)
                //                .Where(x => x.Product.IsExclusive == false || x.Product.ExclusivePrices.Where(ep => ep.CusomterID == CurrentUser.CompanyID).Count() > 0)
                //                .ToList()
                //                .Select(x => mapper.MapToViewModel(x, company))
                //                .ToList();

            }

            cartItemsBySuppliers = repo.Read()
                                       .Where(x => x.CompanyID == company.ID)
                                       .Where(x => x.Product.Deleted == false)
                                       .Where(x => x.Product.IsActive == true)
                                       .Where(x => x.Product.IsExclusive == false || x.Product.ExclusivePrices.Where(ep => ep.CusomterID == company.ID).Count() > 0)
                                       .GroupBy(x => x.Product.Supplier)
                                       .ToList()
                                       .Select(g => new CartItemsBySuppliers
                                       {
                                           //Live = company.Method_IsLive(g.Key),
                                           Supplier = new SupplierMapper().MapToViewModel(g.Key),
                                           CartItems = g.Select(i => mapper.MapToViewModel(i)).ToList(),
                                           SubNet = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalNet) : 0,
                                           SubVAT = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalVAT) : 0,
                                           SubGross = g.Count() > 0 ? g.Select(i => mapper.MapToViewModel(i)).ToList().Sum(i => i.TotalGross) : 0,
                                           Live = (g.Key.UseMasterAccount && company.EasySuppliersActivated) || company.Method_IsLive(g.Key)
                                       })
                                        .Where(x => x.Live)
                                       .ToList();

            var viewModel = new CheckOutPageViewModel()
            {
                ShoppingCart = new ShoppingCartPageViewModel()
                {
                    //CartItems = cartItems,
                    CartItemsBySuppliers = cartItemsBySuppliers,
                    Net = cartItemsBySuppliers.Count() > 0 ? cartItemsBySuppliers.Sum(x => x.SubNet) : 0,
                    VAT = cartItemsBySuppliers.Count() > 0 ? cartItemsBySuppliers.Sum(x => x.SubVAT) : 0,
                    Gross = cartItemsBySuppliers.Count() > 0 ? cartItemsBySuppliers.Sum(x => x.SubGross) : 0
                },
                BillingDetails = new BillingDetailsViewModel()
                {
                    CompanyName = company.Name,
                    EmailAddress = company.Email,
                    Phone = company.Telephone,
                    DeliveryAddress = company.Address1,
                    DeliveryPostCode = company.Postcode,
                    DeliveryCounty = company.County,
                    BillingAddress = company.BillingAddress,
                    BillingPostCode = company.BillingPostcode,
                    BillingCounty = company.County,
                    PONumber = company.PermanentDryGoodsPONo
                },
                CompanyID = company.ID.ToString()
            };

            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CheckOut(CheckOutPageViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            var shoppingCartItemRepo = new ShoppingCartItemRepository();
            var shoppingCartItemMapper = new ShoppingCartItemMapper();

            var oneOffOrderRepo = new OneOffOrderRepository();
            var oneOffOrderItemRepo = new OneOffOrderItemRepository();

            Company company = CurrentUser.Company;
            //var cartItems = new List<ShoppingCartItemViewModel>();
            var cartItemsBySuppliers = new List<CartItemsBySuppliers>();
            //cartItems = shoppingCartItemRepo.Read()
            //                                .Where(x => x.CompanyID == company.ID)
            //                                .Where(x => x.Product.Deleted == false)
            //                                .Where(x => x.Product.IsActive == true)
            //                                .Where(x => x.Product.IsExclusive == false || x.Product.ExclusivePrices.Where(ep => ep.CusomterID == company.ID).Count() > 0)
            //                                .ToList()
            //                                .Select(x => shoppingCartItemMapper.MapToViewModel(x, company))
            //                                .ToList();
            cartItemsBySuppliers = shoppingCartItemRepo.Read()
                                                       .Where(x => x.CompanyID == company.ID)
                                                       .Where(x => x.Product.Deleted == false)
                                                       .Where(x => x.Product.IsActive == true)
                                                       .Where(x => x.Product.IsExclusive == false || x.Product.ExclusivePrices.Where(ep => ep.CusomterID == company.ID).Count() > 0)
                                                       .GroupBy(x => x.Product.Supplier)
                                                       .ToList()
                                                       .Select(g => new CartItemsBySuppliers
                                                       {
                                                           Supplier = new SupplierMapper().MapToViewModel(g.Key),
                                                           CartItems = g.Select(i => shoppingCartItemMapper.MapToViewModel(i)).ToList(),
                                                           SubNet = g.Count() > 0 ? g.Select(i => shoppingCartItemMapper.MapToViewModel(i)).ToList().Sum(i => i.TotalNet) : 0,
                                                           SubVAT = g.Count() > 0 ? g.Select(i => shoppingCartItemMapper.MapToViewModel(i)).ToList().Sum(i => i.TotalVAT) : 0,
                                                           SubGross = g.Count() > 0 ? g.Select(i => shoppingCartItemMapper.MapToViewModel(i)).ToList().Sum(i => i.TotalGross) : 0,
                                                           Live = (g.Key.UseMasterAccount && company.EasySuppliersActivated) || company.Method_IsLive(g.Key)
                                                       })
                                                       .Where(x => x.Live)
                                                       .ToList();

            foreach (var item in cartItemsBySuppliers)
            {
                if (item.SubNet < item.Supplier.MinimumValue)
                {
                    ViewBag.ErrorMessage = String.Format("A minimum of {0} under {1} is required", item.Supplier.MinimumValue.ToString("C"), item.Supplier.Name);
                    viewModel.ShoppingCart.CartItemsBySuppliers = cartItemsBySuppliers;
                    viewModel.ShoppingCart.Net = viewModel.ShoppingCart.CartItemsBySuppliers.Count() > 0 ? viewModel.ShoppingCart.CartItemsBySuppliers.Sum(x => x.SubNet) : 0;
                    viewModel.ShoppingCart.VAT = viewModel.ShoppingCart.CartItemsBySuppliers.Count() > 0 ? viewModel.ShoppingCart.CartItemsBySuppliers.Sum(x => x.SubVAT) : 0;
                    viewModel.ShoppingCart.Gross = viewModel.ShoppingCart.CartItemsBySuppliers.Count() > 0 ? viewModel.ShoppingCart.CartItemsBySuppliers.Sum(x => x.SubGross) : 0;
                    return PartialView(viewModel);
                }
            }
            //if (cartItems.Count() == 0 || cartItems.Sum(x => x.Total) < 120)
            //{
            //    ViewBag.ErrorMessage = "£120 minimum of order amount";
            //    return View(viewModel);
            //}

            var successViewModel = new CheckOutSuccessPageViewModel()
            {
                Approver = company.ProformaApproverID != null
                    ? new ProformaApproverMapper().MapToViewModel(new ProformaApproverRepository().Find(company.ProformaApproverID.Value))
                    : null,
                SubOrders = new List<SubOrder>()
            };
            foreach (var cartItemsBySupplier in cartItemsBySuppliers)
            {
                var newOrder = new OneOffOrder()
                {
                    CompanyID = company.ID,
                    CompanyName = viewModel.BillingDetails.CompanyName,
                    BillingAddress = viewModel.BillingDetails.BillingAddress,
                    BillingCounty = viewModel.BillingDetails.BillingCounty,
                    BillingPostCode = viewModel.BillingDetails.BillingPostCode,
                    DeliveryAddress = viewModel.BillingDetails.DeliveryAddress,
                    DeliveryCounty = viewModel.BillingDetails.DeliveryCounty,
                    DeliveryPostCode = viewModel.BillingDetails.DeliveryPostCode,
                    PONumber = viewModel.BillingDetails.PONumber,
                    Created = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    EmailAddress = viewModel.BillingDetails.EmailAddress,
                    OrderNotes = viewModel.BillingDetails.OrderNotes,
                    Phone = viewModel.BillingDetails.Phone,
                    Status = company.ProformaApproverID == null ? OneOffOrderStatus.Accepted : OneOffOrderStatus.Proforma,
                    TotalNet = cartItemsBySupplier.CartItems.Sum(x => x.TotalNet),
                    TotalVAT = cartItemsBySupplier.CartItems.Sum(x => x.TotalVAT),
                    TotalGross = cartItemsBySupplier.CartItems.Sum(x => x.TotalGross),
                    DeliveryDate = company.NextDryGoodsDeliveryDate(Guid.Parse(cartItemsBySupplier.Supplier.ID)),
                    SupplierID = Guid.Parse(cartItemsBySupplier.Supplier.ID),
                    OneOffOrderItems = new List<OneOffOrderItem>()
                };
                oneOffOrderRepo.Create(newOrder);

                shoppingCartItemRepo.Dispose();
                shoppingCartItemRepo = new ShoppingCartItemRepository();

                var newOrderItems = new List<OneOffOrderItem>();
                foreach (var item in cartItemsBySupplier.CartItems)
                {
                    var newOrderItem = new OneOffOrderItem()
                    {
                        OrderHeaderID = newOrder.ID,
                        CompanyID = company.ID,
                        ImageURL = item.ImageURL,
                        ProductCode = item.ProductCode,
                        ProductID = Guid.Parse(item.ProductID),
                        ProductName = item.ProductName,
                        ProductPrice = item.ProductPrice,
                        Qty = item.Qty,
                        Total = item.TotalNet,
                        ProductVAT = item.ProductVAT,
                    };
                    newOrderItems.Add(newOrderItem);
                    shoppingCartItemRepo.Delete(new ShoppingCartItem() { ID = Guid.Parse(item.ID) });
                }
                oneOffOrderItemRepo.Create(newOrderItems);

                if (cartItemsBySupplier.Supplier.CalendarNeeded)
                {
                    if (!viewModel.EventDetails.Any(x => x.EventSupplierID == cartItemsBySupplier.Supplier.ID))
                        throw new Exception("Need event details");

                    var orderEventRepo = new OrderEventDetailRepository();
                    var orderEventMapper = new OrderEventDetailMapper();
                    foreach (var eventDetails in viewModel.EventDetails.Where(x => x.EventSupplierID == cartItemsBySupplier.Supplier.ID))
                    {
                        eventDetails.OneOffOrderID = newOrder.ID.ToString();
                        var newEventDetail = orderEventMapper.MapToModel(eventDetails);
                        orderEventRepo.Create(newEventDetail);
                    }
                }

                new DryGoodsOrderPlacedMailHelper().SendMail(newOrder);

                successViewModel.SubOrders.Add(new SubOrder
                {
                    OrderHeader = new OneOffOrderMapper().MapToViewModel(newOrder),
                    OrderItems = newOrderItems.Select(x => new OneOffOrderItemMapper().MapToViewModel(x)).ToList(),
                    DeliveryDate = company.NextDryGoodsDeliveryDate(newOrder.SupplierID.Value)
                });
            }

            return PartialView("CheckOutSuccess", successViewModel);
        }

        #endregion

        #endregion

        #region Initial Order set up email
        public bool SendOrderInitializeMail(Guid companySupplierID)
        {
            var companySupplier = new CompanySupplierRepository().Read().Where(x => x.ID == companySupplierID).Include(x => x.Company).Include(x => x.Supplier).Include(x => x.Supplier.DefaultAisles).FirstOrDefault();
            return SendOrderInitializeMail(companySupplier);
        }

        // TO CUSTOMER 4
        public bool SendOrderInitializeMail(CompanySupplier companySupplier)
        {
            var mailMessage = DifficultAislesActivatedEmailHelper.GetEmail(companySupplier);
            EmailService.SendEmail(mailMessage, Server);

            return true;
        }

        #endregion

        private void SendQuoteMailToCustomer(Company company)
        {
            var mailMessage = DryGoodsQuoteEmailHelper.GetEmail(company);
            EmailService.SendEmail(mailMessage, Server);
        }

        public ActionResult _ShoppingCartTotal()
        {
            var result = GetShoppingCartStatus();
            return Json(new { ItemQty = result.Item1, Total = result.Item2 }, JsonRequestBehavior.AllowGet);
        }

        private void ClearShoppingCart(Company company)
        {
            var service = new ShoppingCartService(new SnapDbContext());
            service.ClearShoppingCart(company.ID);
        }
    }
}