﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    public class ProductListViewController : GridController<Product, ProductListViewModel>
    {

        public ProductListViewController()
            : base(new ProductRepository(), new ProductListViewMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? aisleID, Guid? categoryID, string filterText, Guid? companyID)
        {

            // Prepare the default parameters
            if (request.Page == 0) request.Page = 1;
            if (request.PageSize == 0) request.PageSize = 16;
            filterText = filterText.ToLower();

            // Get all the sub-categories
            List<Guid> categoryIDs = new List<Guid>();
            if (categoryID != null)
            {
                try
                {
                    var category = new ProductCategoryRepository().Find(categoryID);
                    var childrenCategories = new List<ProductCategory>();
                    category.GetChildrenCategory(childrenCategories);
                    categoryIDs = childrenCategories.Select(x => x.ID).ToList();
                }
                catch
                {
                    categoryID = null;
                }
            }

            var models = _repo.Read()
                              .Where(x => x.Deleted == false)
                              .Where(x => x.IsActive == true)
                              .Where(x => aisleID == null || x.DryGoodsAisles.Select(d => d.AisleID).Contains(aisleID.Value))
                              .Where(x => categoryID == null || categoryIDs.Intersect(x.DryGoodsCategories.Select(c => c.CategoryID)).Any())
                              .Where(x => filterText == "" || x.Name.ToLower().Contains(filterText))
                              .OrderBy(x => x.DryGoodsCategories.Where(dc => dc.Category.AisleID == aisleID).FirstOrDefault().Category.SortPos).ThenBy(x => x.SortPosition)
                              .AsEnumerable();

            Company company = null;
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    // NI-Users
                    if (companyID == null)
                    {
                        company = CurrentUser.Company;
                    }
                    else
                    {
                        company = new CompanyRepository().Find(companyID);
                        models = models.Where(x => !x.IsExclusive || (x.IsExclusive && x.ExclusivePrices.Any(ex => ex.CusomterID == company.ID)));
                    }
                }
                else
                {
                    // Members
                    company = CurrentUser.Company;
                    models = models.Where(x => !x.IsExclusive || (x.IsExclusive && x.ExclusivePrices.Any(ex => ex.CusomterID == company.ID)));
                }
            }
            else
            {
                // Prospects
                models = models.Where(x => x.IsProspectsVisible == true)
                               .Where(x => !x.IsExclusive);
            }

            var mapper = new ProductListViewMapper();
            var viewModels = models.Skip((request.Page - 1) * request.PageSize)
                                   .Take(request.PageSize)
                                   .ToList()
                                   .Select(s => mapper.MapToViewModel(s, company));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = models.Count()
            };

            return Json(result);
        }

    }
}