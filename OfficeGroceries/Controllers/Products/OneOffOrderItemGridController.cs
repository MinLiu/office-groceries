﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class OneOffOrderItemGridController : GridController<OneOffOrderItem, OneOffOrderItemViewModel>
    {

        public OneOffOrderItemGridController()
            : base(new OneOffOrderItemRepository(), new OneOffOrderItemMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid id)
        {
            var viewModels = _repo.Read()
                                    .Where(x => x.OrderHeaderID == id)
                                    .OrderBy(x => x.ProductName)
                                    .ToList()
                                    .Select(x => _mapper.MapToViewModel(x));
            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, OneOffOrderItemViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Find the existing entity.
                var model = _repo.Find(Guid.Parse(viewModel.ID));

                // Map the view model to the model and update the database.
                _mapper.MapToModel(viewModel, model);
                _repo.Update(model);

                UpdateOrderTotal(model.OrderHeaderID);

                _mapper.MapToViewModel(model, viewModel);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public void UpdateOrderTotal(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var model = repo.Find(id);

            var orderItems = _repo.Read()
                                  .Where(x => x.OrderHeaderID == id)
                                  .ToList();

            model.TotalNet = orderItems.Count() > 0 ? orderItems.Sum(x => x.Total) : 0;
            model.TotalVAT = orderItems.Count() > 0 ? orderItems.Sum(x => (x.Total * x.ProductVAT)) : 0;
            model.TotalGross = orderItems.Count() > 0 ? orderItems.Sum(x => (x.Total * x.ProductVAT) + x.Total) : 0;
            model.LastUpdated = DateTime.Now;
            repo.Update(model);

            return;
        }
    }
}