﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MessagesController : EntityController<Message, MessageViewModel>
    {
        public MessagesController()
            : base(new MessageRepository(), new MessageViewMapper())
        {

        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _New()
        {
            return PartialView(new MessageViewModel());
        }

        //[HttpPost]
        //public ActionResult _New(MessageViewModel viewModel)
        //{
        //    if (!ModelState.IsValid)
        //        return PartialView(viewModel);

        //    var model = _mapper.MapToModel(viewModel);
        //    model.CreatedDate = DateTime.Now;

        //    _repo.Create(model);

        //    viewModel.ID = model.ID.ToString();
        //    UpdateMessageTargets(viewModel);

        //    SendMessage(viewModel);

        //    return Json(new { ID = model.ID });
        //}

        [HttpGet]
        public ActionResult _Edit(Guid id)
        {
            var model = _repo.Find(id);
            var viewModel = _mapper.MapToViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _Edit(MessageViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return PartialView(viewModel);

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);

            _repo.Update(model);

            UpdateMessageTargets(viewModel);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
        }

        private void UpdateMessageTargets(MessageViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.MessageTargets)
                             .FirstOrDefault();

            var oldList = model != null ? model.MessageTargets.Select(x => x.TargetID.ToString()) : new List<string>();
            var newList = viewModel.MessageTargets ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new MessageTargetRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.TargetID.ToString() == id && x.MessageID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new MessageTarget() { TargetID = int.Parse(id), MessageID = Guid.Parse(viewModel.ID) });
            }
        }

        //private void SendMessage(MessageViewModel viewModel)
        //{
        //    var model = _repo.Read()
        //                     .Where(x => x.ID.ToString() == viewModel.ID)
        //                     .Include(x => x.MessageTargets)
        //                     .FirstOrDefault();

        //    if (model == null) return;

        //    var emailsToList = new List<string>();
        //    foreach (var target in model.MessageTargets)
        //    {
        //        emailsToList.AddRange(EmailsToList(target.TargetID));
        //    }

        //    SendMessage(viewModel.Title, viewModel.Content, emailsToList);
            
        //}

        //private void SendMessage(string title, string content, List<string> emailsTo)
        //{
        //    if (emailsTo.Count == 0) return;

        //    var mailMessage = SystemMessageEmailHelper.GetEmail(title, content, emailsTo);

        //    EmailService.SendEmail(mailMessage, Server);
        //}

        private List<string> EmailsToList(int target)
        {
            var results = new List<string>();

            var customerRepo = new CompanyRepository();
            var customerList = customerRepo.Read().ToList();

            switch (target)
            {
                case TargetValues.Milk:
                    results = customerList.Where(x => x.Method_HasSupplier(SupplierType.Milk)).Select(x => x.Email).ToList();
                    break;
                case TargetValues.Fruits:
                    results = customerList.Where(x => x.Method_HasSupplier(SupplierType.Fruits)).Select(x => x.Email).ToList();
                    break;
                case TargetValues.DryGoods:
                    results = customerList.Where(x => x.Method_HasSupplier(SupplierType.DryGoods)).Select(x => x.Email).ToList();
                    break;
                default:
                    break;
            }

            return results;
        }


    }

}