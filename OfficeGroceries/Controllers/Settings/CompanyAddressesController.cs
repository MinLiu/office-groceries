﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class CompanyAddressesController : GridController<CompanyAddress, CompanyAddressViewModel>
    {

        public CompanyAddressesController()
            : base(new CompanyAddressRepository(), new CompanyAddressMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string companyID)
        {
            var list = _repo.Read().Where(p => p.CompanyID.ToString() == companyID).ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = _repo.Read().Where(p => p.CompanyID == CurrentUser.CompanyID).ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }


        public JsonResult DropDownCompanyAddresses()
        {
            var addresses = _repo.Read().Where(a => a.CompanyID == CurrentUser.CompanyID)
                .OrderBy(t => t.Name).ToArray()
                .Select(a => new SelectItemViewModel
                {
                    ID = a.ID.ToString(),
                    Name = string.Format("{0} {1} {2}", a.Address1, a.Address2, a.Postcode)
                }
            ).ToList();

            addresses.Insert(0, new SelectItemViewModel { ID = null, Name = string.Format("{0} {1} {2}", CurrentUser.Company.Address1, CurrentUser.Company.Address2, CurrentUser.Company.Postcode) });
                
            return Json(addresses, JsonRequestBehavior.AllowGet);
        }

    }

}