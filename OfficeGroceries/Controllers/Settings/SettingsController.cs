﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class SettingsController : BaseController
    {
        CompanyRepository _repo = new CompanyRepository();
        CompanyMapper _mapper = new CompanyMapper();
        
        // GET: Settings
        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult _Suppliers()
        {
            return PartialView();
        }

        public ActionResult _ProductCategories()
        {
            return PartialView();
        }

        public ActionResult _FruitProductCategories()
        {
            return PartialView();
        }

        public ActionResult _MilkProductCategories()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult _OverallSetting()
        {
            var model = new SnapDbContext().Settings.First();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult _OverallSetting(Setting viewModel)
        {
            using (var db = new SnapDbContext())
            {
                var model = db.Settings.First();

                model.MilkSupplierCenterEmail = viewModel.MilkSupplierCenterEmail;
                model.FruitSupplierCenterEmail = viewModel.FruitSupplierCenterEmail;
                model.DryGoodsSupplierCenterEmail = viewModel.DryGoodsSupplierCenterEmail;
                model.FeedbackCenterEmail = viewModel.FeedbackCenterEmail;

                db.SaveChanges();

                ViewBag.Saved = true;

                return PartialView(model);
            }
        }
    }
}