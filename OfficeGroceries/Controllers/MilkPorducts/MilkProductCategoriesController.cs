﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    public class MilkProductCategoriesController : EntityController<MilkProductCategory, MilkProductCategoryViewModel>
    {

        public MilkProductCategoriesController()
            : base(new MilkProductCategoryRepository(), new MilkProductCategoryMapper())
        {

        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult DropDownAllCategories()
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.Deleted == false)
                                  .Select(x => new SelectItemViewModel
                                  {
                                      ID = x.ID.ToString(),
                                      Name = x.Name
                                  })
                                  .OrderBy(x => x.Name)
                                  .ToList();

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TreeViewMilkProductCategories(Guid? id)
        {
            var categories = _repo.Read()
                                  .Where(x => x.ParentID == id)
                                  .Where(x => x.Deleted == false)
                                  .OrderBy(x => x.Name)
                                  .Select(x => new
                                  {
                                      id = x.ID,
                                      Name = x.Name,
                                      hasChildren = x.Children.Any(),
                                  })
                                  .ToList();
            //if (!id.HasValue)
            //{
            //    categories.Insert(0, new { id = Guid.NewGuid(), Name = "All", hasChildren = false });
            //}
            return Json(categories, JsonRequestBehavior.AllowGet);
        }
    }
}