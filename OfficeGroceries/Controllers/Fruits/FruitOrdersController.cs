﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.Extensions;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Web.Hosting;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class FruitOrdersController : EntityController<FruitOrder, FruitOrderViewModel>
    {
        public FruitOrdersController()
            : base(new FruitOrderRepository(), new FruitOrderMapper())
        {

        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SaveOrders(string orders, OrderMode orderMode, bool submit, string date, string companyID, string prospectToken)
        {
            try
            {
                DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;
                List<FruitOrderViewModel> fruitOrderViewModels = new JavaScriptSerializer().Deserialize<List<FruitOrderViewModel>>(orders);

                if (User.Identity.IsAuthenticated)
                {
                    // Only allow user to edit orders when it's in editable time, except for the user does not yet activate fruit account
                    if (AppSettings.IsEditableTime() || orderMode == OrderMode.Draft)
                    {
                        // Members
                        var result = SaveFruitOrderHelper.New()
                            .SetCompanyID(CurrentUser.CompanyID)
                            .SaveCustomOrder(orderMode, fromDate, fruitOrderViewModels);

                        // if the user is already asking fruit or snack supplier, then initialize the fruit snack supplier checker,
                        // if not, means it's a pure draft order. So no need to initialize the fruit snack supplier checker
                        if (orderMode == OrderMode.Draft)
                        {
                            var company = new CompanyRepository().Find(CurrentUser.CompanyID);
                            if (company.AskingForSnackSupplier != null || company.AskingForFruitSupplier != null)
                            {
                                FruitSnackSupplierChecker.CheckAndInitialize(result.OrderHeader.ID, CurrentUser.CompanyID);
                            }
                        }
                        
                        // Send mails
                        if (orderMode == OrderMode.Regular || orderMode == OrderMode.OneOff)
                        {
                            FruitSnackSupplierChecker.CheckAndInitialize(result.OrderHeader.ID, CurrentUser.CompanyID);

                            SendOrderChangeMail(CurrentUser.Company, orderMode, date, result.ChangedProductTypes);
                        }

                        if (orderMode == OrderMode.Regular && result.OrderHeader.TotalNet == 0)
                        {
                            SendCancelOrderAlertEmail(CurrentUser.Company);
                        }
                    }
                    else
                    {
                        throw new Exception(AppSettings.FreezingTimeMessage);
                    }
                }
                else
                {
                    // Prospects
                    var result = SaveFruitOrderHelper.New()
                        .SetProspectToken(ProspectToken)
                        .SaveProspectOrder(fruitOrderViewModels);
                }
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, ErrorMessage = e.Message });
            }
        }

        private void AdjustFutureOneOffOrders(List<FruitOrderViewModel> latestItemViewModels)
        {
            var latestItems = latestItemViewModels
                .Select(x => _mapper.MapToModel(x))
                .ToList();

            var repo = new FruitOrderHeaderRepository();
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), new SnapDbContext());

            var futureOneOffOrders = repo
                .Read()
                .Where(x => x.CompanyID == CurrentUser.CompanyID)
                .Where(x => x.OrderMode == OrderMode.OneOff)
                .Where(x => x.ToDate != null && x.ToDate > DateTime.Today)
                .Include(x => x.Items)
                .AsNoTracking()
                .ToList();

            foreach (var futureOrder in futureOneOffOrders)
            {
                var origItems = futureOrder.Items.ToList();

                var toRemoveItems = origItems
                    .Where(x => !latestItems.Select(li => li.FruitProductID).Contains(x.FruitProductID))
                    .ToList();
                var toAddItems = latestItems
                    .Where(x => !origItems.Select(oi => oi.FruitProductID).Contains(x.FruitProductID))
                    .ToList();

                foreach (var toRemoveItem in toRemoveItems)
                {
                    service.DeleteItem(futureOrder, toRemoveItem.ID);
                }

                foreach (var toAddItem in toAddItems)
                {
                    var newItem = (FruitOrder)toAddItem.Clone();
                    newItem.ID = Guid.NewGuid();
                    service.AddItem(futureOrder.ID, newItem, futureOrder.CompanyID);
                }

                service.CalculateTotal(futureOrder.ID);
            }
        }

        

        // Get dates which has orders
        public ActionResult GetOrderWeekDates(string companyID)
        {
            var companyRepo = new CompanyRepository();

            Guid CompanyID = string.IsNullOrWhiteSpace(companyID)? CurrentUser.CompanyID : Guid.Parse(companyID);

            var company = companyRepo.Find(CompanyID);

            var orderWeeks = _repo.Read()
                                  .Where(fo => fo.OrderHeader.CompanyID == CompanyID)
                                  .Where(fo => fo.OrderHeader.OrderMode == OrderMode.OneOff)
                                  .Select(fo => fo.OrderHeader.FromDate);

            var viewModel = new FruitsPageViewModel();
            viewModel.Set(company);
            foreach (var startdates in orderWeeks)
            {
                for (int i = 0; i < 7; i++)
                    viewModel.OrderWeekDates.Add(startdates.Value.AddDays(i));
            }

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult _AddToQuote(Guid fruitProductID)
        {

            if (fruitProductID == Guid.Parse("1f05fbdc-b4e6-47a6-a0d3-55dd109b3620") ||
                fruitProductID == Guid.Parse("12b05cdd-aee5-464a-83de-f26e3164759d"))
                return Json("Invalid Product");
            
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), new SnapDbContext());

            var fruitProduct = new FruitProductRepository().Read().Where(f => f.ID == fruitProductID).FirstOrDefault();

            if (fruitProduct == null)
                return Json("No Content");

            var orderHeader = service.GetProspectOrder(ProspectToken);
            if (orderHeader == null)
            {
                orderHeader = new FruitOrderHeader()
                {
                    ProspectToken = ProspectToken,
                    OrderMode = OrderMode.Draft,
                    OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                };
                orderHeader = service.Create(orderHeader);
            }

            // Check if the product is already in the order
            if (orderHeader.Items != null && orderHeader.Items.Any(x => x.FruitProductID == fruitProduct.ID))
            {
                return Json("Already exists");
            }

            service.AddItem(orderHeader.ID, fruitProduct, null);

            return Json("Success");
        }

        #region Helper

        public void RecalculateOrderHeader(Guid orderHeaderID)
        {
            var repo = new FruitOrderHeaderRepository();
            var header = repo.Read()
                             .Where(x => x.ID == orderHeaderID)
                             .Include(x => x.Items)
                             .FirstOrDefault();

            var TotalVAT = 0m;
            var TotalNet = 0m;
            foreach(var item in header.Items)
            {
                TotalVAT += item.TotalVAT;
                TotalNet += item.TotalNet;
            }

            header.TotalVAT = TotalVAT;
            header.TotalNet = TotalNet;

            repo.Update(header, new[] { "TotalVAT", "TotalNet" });
        }

        #region Send Order Change Email

        public bool SendOrderChangeMail(Guid companyID, OrderMode orderMode, string date, List<FruitProductType> changedProductTypes)
        {
            var company = new CompanyRepository().Find(companyID);
            return SendOrderChangeMail(company, orderMode, date, changedProductTypes);
        }

        public bool SendOrderChangeMail(Company company, OrderMode orderMode, string date, List<FruitProductType> changedProductTypes)
        {
            SendOrderChangeMailToCustomer(company, orderMode, date);
            SendOrderChangeMailToSuppliers(company, orderMode, date, changedProductTypes);

            return true;
        }

        private void SendOrderChangeMailToCustomer(Company company, OrderMode orderMode, string date)
        {
            if (orderMode == OrderMode.OneOff)
                SendOneOffOrderChangeMailToCustomer(company, date);
            else if (orderMode == OrderMode.Regular)
                SendPermanentOrderChangeMailToCustomer(company);
        }

        // TO CUSTOMER 8
        private void SendOneOffOrderChangeMailToCustomer(Company company, string date)
        {
            var mailMessage = FruitOneOffOrderChangeEmailHelper.GetEmail(company, date);
            EmailService.SendEmail(mailMessage, Server);
        }

        // TO CUSTOMER 9
        private void SendPermanentOrderChangeMailToCustomer(Company company)
        {
            var mailMessage = FruitRegularOrderChangeEmailHelper.GetEmail(company);
            EmailService.SendEmail(mailMessage, Server);
        }

        private void SendOrderChangeMailToSuppliers(Company company, OrderMode orderMode, string date, List<FruitProductType> changedProductTypes)
        {
            if (orderMode == OrderMode.OneOff)
                SendOneOffOrderChangeMailToSuppliers(company, date, changedProductTypes);
            else if (orderMode == OrderMode.Regular)
                SendPermanentOrderChangeMailToSuppliers(company, changedProductTypes);
        }

        // TO SUPPLIER 6
        private void SendOneOffOrderChangeMailToSuppliers(Company company, string date, List<FruitProductType> changedProductTypes)
        {
            DateTime fromDate = DateTime.Parse(date);
            DateTime toDate = fromDate.AddDays(6);

            var supplierTypes = new List<SupplierType>();
            if (changedProductTypes.Contains(FruitProductType.Fruit))
                supplierTypes.Add(SupplierType.Fruits);
            if (changedProductTypes.Contains(FruitProductType.Snack))
                supplierTypes.Add(SupplierType.Snacks);
            
            List<CompanySupplier> companySuppliers = company.CompanySuppliers
                                                            .Where(x => supplierTypes.Contains(x.Supplier.SupplierType))
                                                            .Where(x => x.StartDate.HasValue)
                                                            .Where(x => !(toDate < x.StartDate))
                                                            .Where(x => !x.EndDate.HasValue || !(fromDate > x.EndDate))
                                                            .OrderBy(x => x.StartDate)
                                                            .ToList();
            foreach (var companySupplier in companySuppliers)
            {
                try
                {
                    var mailMessage = FruitOneOffOrderChangeSupplierEmailHelper.GetEmail(company, companySupplier, fromDate, toDate);
                    EmailService.SendEmail(mailMessage, Server);
                }
                catch (Exception e) { }
            }
        }

        // TO SUPPLIER 7
        private void SendPermanentOrderChangeMailToSuppliers(Company company, List<FruitProductType> changedProductTypes)
        {
            var supplierTypes = new List<SupplierType>();
            if (changedProductTypes.Contains(FruitProductType.Fruit))
                supplierTypes.Add(SupplierType.Fruits);
            if (changedProductTypes.Contains(FruitProductType.Snack))
                supplierTypes.Add(SupplierType.Snacks);
            
            List<CompanySupplier> companySuppliers = company.CompanySuppliers
                                                            .Where(x => supplierTypes.Contains(x.Supplier.SupplierType))
                                                            .Where(x => x.StartDate.HasValue)
                                                            .Where(x => !x.EndDate.HasValue || x.EndDate.Value >= DateTime.Today)
                                                            .OrderBy(x => x.StartDate)
                                                            .ToList();
            foreach (var companySupplier in companySuppliers)
            {
                try
                {
                    var mailMessage = FruitRegularOrderChangeSupplierEmailHelper.GetEmail(company, companySupplier);
                    EmailService.SendEmail(mailMessage, Server);
                }
                catch { }
            }
        }

        private void SendCancelOrderAlertEmail(Company company)
        {
            var mailMessage = CancelOrderAlertEmailHelper.GetEmail(company, "fruit");
            EmailService.SendEmail(mailMessage, Server);
        }

        #endregion

        #region Initial Order set up email
        public bool SendOrderInitializeMail(Guid companyID, DateTime startFrom, CompanySupplier companySupplier)
        {
            var company = new CompanyRepository().Find(companyID);
            return SendOrderInitializeMail(company, startFrom, companySupplier);
        }

        public bool SendOrderInitializeMail(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            SendOrderIntializeMailToCustomer(company, startFrom, companySupplier);
            SendOrderIntializeMailToSupplier(company, startFrom, companySupplier);
            return true;
        }

        // TO CUSTOMER 4
        public void SendOrderIntializeMailToCustomer(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            var mailMessage = FruitOrderInitializeEmailHelper.GetEmail(company, startFrom, companySupplier);
            EmailService.SendEmail(mailMessage, Server);
        }

        // TO SUPPLIER 5
        public void SendOrderIntializeMailToSupplier(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            try
            {
                var mailMessage = FruitOrderInitializeSupplierEmailHelper.GetEmail(company, startFrom, companySupplier);
                EmailService.SendEmail(mailMessage, Server);
            }
            catch (Exception e) { }
        }
        #endregion

        #endregion

    }

}