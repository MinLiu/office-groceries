﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class FruitsController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            var viewModel = new FruitsPageViewModel();
            var fruitOrderRepo = new FruitOrderRepository();
            if (User.Identity.IsAuthenticated)
            {
                viewModel.Set(CurrentUser.Company);
                var orderWeeks = fruitOrderRepo.Read()
                                                .Where(fo => fo.OrderHeader.CompanyID == CurrentUser.Company.ID)
                                                .Where(fo => fo.OrderHeader.OrderMode == OrderMode.OneOff)
                                                .Select(fo => fo.OrderHeader.FromDate);
                foreach (var startdates in orderWeeks)
                {
                    for (int i = 0; i < 7; i++)
                        viewModel.OrderWeekDates.Add(startdates.Value.AddDays(i));
                }
            }

            var page = new AisleRepository().Read().Where(x => x.Name == "Fruit").FirstOrDefault();
            if (page != null)
            {
                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;
            }

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Invoice()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult _FruitOrderRow(Guid fruitProductID, OrderMode orderMode, DateTime? date)
        {
            var viewModel = new FruitOrderRowPageViewModel(orderMode, date, CurrentUser?.CompanyID);

            var fruit = new FruitProductRepository().Read().Where(f => f.ID == fruitProductID).FirstOrDefault();
            var rowViewModel = new FruitOrderViewModel()
            {
                ID = Guid.NewGuid().ToString(),
                FruitID = fruit.ID.ToString(),
                ImageUrl = fruit.ImageURL,
                ProductType = fruit.FruitProductType,
                ProductName = fruit.Name,
                Unit = fruit.Unit,
                Monday = 0,
                Tuesday = 0,
                Wednesday = 0,
                Thursday = 0,
                Friday = 0,
                Saturday = 0,
                Sunday = 0,
                WeeklyVolume = 0,
                WeeklyTotal = 0,
                OrderMode = OrderMode.Draft,
            };

            if (User.Identity.IsAuthenticated)
            {
                //viewModel.Price = fruit.ExclusivePrice(CurrentUser.Company);
                    rowViewModel.Price = fruit.GetBasicPrice();
            }
            else
            {
                rowViewModel.Price = fruit.Price;
            }
            // Get Non-Delivery Days
            if (User.Identity.IsAuthenticated)
            {
                // Members
                SetNonDeliveryDays(CurrentUser.Company);
            }
            else
            {
                // Prospects
                SetNonDeliveryDays();
            }

            viewModel.FruitOrders.Add(rowViewModel);

            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult FruitOrderDetails(Guid id)
        {
            var repo = new FruitOrderHeaderRepository();
            var mapper = new FruitOrderHeaderMapper();

            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return View(viewModel);
        }

        [AllowAnonymous]
        public ActionResult _WeeklyOrder(OrderMode? orderMode, string date, string companyID, string prospectToken)
        {
            if (!orderMode.HasValue) orderMode = OrderMode.Draft;
            DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;

            var viewModel = new FruitWeeklyOrderPageViewModel(orderMode.Value, date, CurrentUser?.CompanyID);
            if (User.Identity.IsAuthenticated)
            {
                viewModel.Set(CurrentUser.Company);
            }

            viewModel.FruitOrders = GetOrders(orderMode.Value, fromDate, companyID);

            return PartialView(viewModel);
        }

        public ActionResult _CopyOrders(OrderMode orderMode, string date, string companyID)
        {
            DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date).AddDays(-7) : (DateTime?)null;

            List<FruitOrderViewModel> fruitOrders = GetOrders(orderMode, fromDate, companyID);

            fruitOrders.Each(f => f.ID = Guid.NewGuid().ToString());

            if (fruitOrders.Count > 0)
                return PartialView("_FruitOrderRow", fruitOrders);
            else
                return Json("");
        }

        public ActionResult _CalendarForm()
        {
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult _GetSubtotal(Guid fruitProductID, int mon = 0, int tue = 0, int wed = 0, int thu = 0, int fri = 0, int sat = 0)
        {
            var service = new FruitProductService(new SnapDbContext());
            var companyID = CurrentUser?.CompanyID;
            var result = service.CalculateSubtotal(fruitProductID, companyID, mon, tue, wed, thu, fri, sat);
            return Json(new { Subtotal = result.Subtotal, UnitPrice = result.UnitPrice });
        }

        #region Helper

        private List<FruitOrderViewModel> GetOrders(OrderMode orderMode, DateTime? fromDate, string companyID)
        {
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), new SnapDbContext());
            var fruitOrderMapper = new FruitOrderMapper();

            List<FruitOrderViewModel> fruitOrders = new List<FruitOrderViewModel>();
            if (User.Identity.IsAuthenticated)
            {
                // Members
                Guid CompanyID = CurrentUser.CompanyID;
                fruitOrders = service
                    .GetCustomerOrderItems(CompanyID, orderMode, fromDate)
                    .OrderBy(f => f.ProductName)
                    .ThenBy(f => f.Unit)
                    .Select(f => fruitOrderMapper.MapToViewModel(f))
                    .ToList();

                // Get Non-Delivery Days
                SetNonDeliveryDays(CompanyID);
            }
            else
            {
                // Prospects
                // Get Orders
                fruitOrders = service
                    .GetProspectOrderItems(ProspectToken)
                    .OrderBy(f => f.ProductName)
                    .ThenBy(f => f.Unit)
                    .Select(f => fruitOrderMapper.MapToViewModel(f))
                    .ToList();

                // Get Non-Delivery Days
                SetNonDeliveryDays();
            }

            return fruitOrders;
        }

        private List<FruitOrderViewModel> GetOrders(string prospectToken)
        {
            var fruitOrderRepo = new FruitOrderRepository();
            var fruitOrderMapper = new FruitOrderMapper();

            List<FruitOrderViewModel> fruitOrders = new List<FruitOrderViewModel>();
            fruitOrders = fruitOrderRepo
                .Read()
                .Where(f => f.OrderHeader.ProspectToken == prospectToken)
                .OrderBy(f => f.ProductName)
                .ThenBy(f => f.Unit)
                .ToList()
                .Select(f => fruitOrderMapper.MapToViewModel(f))
                .ToList();

            SetNonDeliveryDays();

            return fruitOrders;
        }

        private void SetNonDeliveryDays(string companyID)
        {
            SetNonDeliveryDays(Guid.Parse(companyID));
        }

        private void SetNonDeliveryDays()
        {
            SetNonDeliveryDays((Company)null);
        }

        private void SetNonDeliveryDays(Guid companyID)
        {
            var company = new CompanyRepository().Find(companyID);
            SetNonDeliveryDays(company);
            
        }

        private void SetNonDeliveryDays(Company company)
        {
            if (company != null)
            {
                ViewBag.Monday = company.FruitsDeliveryMonday;
                ViewBag.Tuesday = company.FruitsDeliveryTuesday;
                ViewBag.Wednesday = company.FruitsDeliveryWednesday;
                ViewBag.Thursday = company.FruitsDeliveryThursday;
                ViewBag.Friday = company.FruitsDeliveryFriday;
                ViewBag.Saturday = company.FruitsDeliverySaturday;
                ViewBag.Sunday = company.FruitsDeliverySunday;
            }
            else
            {
                ViewBag.Monday = true;
                ViewBag.Tuesday = true;
                ViewBag.Wednesday = true;
                ViewBag.Thursday = true;
                ViewBag.Friday = true;
                ViewBag.Saturday = true;
                ViewBag.Sunday = true;
            }
        }

        #endregion


    }

}