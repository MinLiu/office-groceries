﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    public class FruitOrdersGridController : GridController<FruitOrder, FruitOrderViewModel>
    {
        public FruitOrdersGridController()
            : base(new FruitOrderRepository(), new FruitOrderMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid fruitOrderHeaderID)
        {
            var models = _repo.Read()
                              .Where(x => x.OrderHeaderID == fruitOrderHeaderID)
                              .OrderBy(x => x.ProductName)
                              .ToList();

            var viewModels = models.Select(x => _mapper.MapToViewModel(x)).ToList();

            return Json(viewModels.ToDataSourceResult(request));
        }

        //public override JsonResult Create([DataSourceRequest] DataSourceRequest request, FruitOrderViewModel viewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var model = _mapper.MapToModel(viewModel);

        //        // Set ProspectToken or CompanyID
        //        if (CurrentUser == null)
        //            model.ProspectToken = ProspectToken;
        //        else
        //            model.CompanyID = CurrentUser.CompanyID;

        //        _repo.Create(model);
        //        viewModel.ID = model.ID.ToString();
        //    }

        //    return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        //}

        [Authorize(Roles = "NI-User")]
        public JsonResult ReadProspectQuotes([DataSourceRequest] DataSourceRequest request, string prospectToken)
        {
            var models = _repo.Read().Where(m => ProspectToken == m.OrderHeader.ProspectToken);
            var viewModels = _repo.Read()
                                  .Where(m => m.OrderHeader.ProspectToken == prospectToken)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

    }

}