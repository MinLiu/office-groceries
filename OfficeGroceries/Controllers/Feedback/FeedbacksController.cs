﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class FeedbacksController : BaseController
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _EditFeedback(Guid feedbackID)
        {
            var repo = new FeedbackRepository();
            var mapper = new FeedbackMapper();
            var model = repo.Find(feedbackID);
            var viewModel = mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditFeedback(FeedbackViewModel viewModel)
        {
            var repo = new FeedbackRepository();
            var mapper = new FeedbackMapper();

            var model = repo.Find(Guid.Parse(viewModel.ID));
            mapper.MapToModel(viewModel, model);
            model.ReplierID = CurrentUser.Id;
            repo.Update(model, new string[] { "NIComment", "ReplierID", "ResponseTime" });

            ViewBag.Saved = true;

            return PartialView(mapper.MapToViewModel(model));
        }

        [HttpPost]
        public ActionResult Feedback(int type, string message, string dryGoodsOrderID)
        {
            var feedback = new Feedback()
            {
                FeedbackTypeID = type,
                Message = message,
                CompanyID = CurrentUser.CompanyID,
                UserID = CurrentUser.Id,
                DryGoodsOrderID = (type == FeedbackTypeValues.DryGoods && !string.IsNullOrWhiteSpace(dryGoodsOrderID)) ? Guid.Parse(dryGoodsOrderID) : null as Guid?,
            };

            using (var context = new SnapDbContext())
            {
                context.Feedbacks.Add(feedback);
                context.SaveChanges();
            }

            SendFeedbackMail(feedback.ID);

            return Json(new { Success = true, Message = "Message has been sent successfully, one of our specialists will be in contact with you shortly." });
        }

        private bool SendFeedbackMail(Guid feedbackID)
        {
            using (var context = new SnapDbContext())
            {
                var feedback = context.Feedbacks.Where(x => x.ID == feedbackID).First();
                var mailServer = context.EmailSettings.First();
                var overallSetting = context.Settings.First();

                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = String.Format("Feedback from " + feedback.Company.Name);
                mailMessage.AddToEmails(overallSetting.FeedbackCenterEmail);
                mailMessage.From = new MailAddress(mailServer.Email);
                mailMessage.Body = String.Format("<div class=\"row\" style='max-width: 500px;'>" +
                                                    "Customer Feedback" +
                                                    "<hr />" +
                                                    "<table>" +
                                                        "<tr>" +
                                                            "<td><label>Company:</label></td><td>{0}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Name:</label></td><td>{1}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Email:</label><td>{2}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Telephone:</label><td>{3}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Message:</label><td>{4}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Timestamp:</label><td>{5}</td>" +
                                                        "</tr>" +
                                                    "</table>" +
                                                    "<hr />" +
                                                "</div>", feedback.Company.Name
                                                        , feedback.User.FirstName + " " + feedback.User.LastName
                                                        , feedback.User.Email
                                                        , feedback.User.PhoneNumber
                                                        , feedback.Message
                                                        , feedback.Created);
                mailMessage.IsBodyHtml = true;

                // Send to feedback center

                EmailService.SendEmail(mailMessage, Server);

                return true;
            }
        }
    }

}