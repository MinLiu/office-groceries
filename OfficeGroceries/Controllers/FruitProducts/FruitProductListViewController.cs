﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    public class FruitProductListViewController : GridController<FruitProduct, FruitProductListViewModel>
    {

        public FruitProductListViewController()
            : base(new FruitProductRepository(), new FruitProductListViewMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? categoryID, Guid? companyID, string filterText = "")
        {

            // Prepare the default parameters
            filterText = filterText.ToLower();

            // Get all the sub-categories
            List<Guid> categoryIDs = new List<Guid>();
            if (categoryID != null)
            {
                try
                {
                    var category = new FruitProductCategoryRepository().Find(categoryID);
                    var childrenCategories = new List<FruitProductCategory>();
                    category.GetChildrenCategory(childrenCategories);
                    categoryIDs = childrenCategories.Select(x => x.ID).ToList();
                }
                catch
                {
                    categoryID = null;
                }
            }

            var models = _repo.Read()
                              .Where(x => x.Deleted == false)
                              .Where(x => x.IsActive == true)
                              .Where(x => categoryID == null || x.Categories.Select(c => c.CategoryID).Intersect(categoryIDs).Any())
                              .Where(x => filterText == "" || x.Name.ToLower().Contains(filterText));

            Company company = null;
            if (User.Identity.IsAuthenticated)
            {
                // Members
                company = CurrentUser.Company;
                models = models.Where(x => x.ExcludeCompanies.All(c => c.CompanyID != company.ID));
                models = models.Where(x => !x.IsExclusive || (x.IsExclusive && x.ExclusivePrices.Any(ex => ex.CusomterID == company.ID)));
            }
            else
            {
                // Prospects
                models = models.Where(x => x.IsProspectsVisible == true)
                               .Where(x => !x.IsExclusive);
            }

            if (categoryID != null)
            {
                models = models
                    .OrderBy(x => x.Categories.Where(c => c.CategoryID == categoryID).Select(c => c.Category.SortPos).FirstOrDefault())
                    .ThenBy(x => x.Categories.Where(c => c.CategoryID == categoryID).Select(c => c.SortPos).FirstOrDefault());
            }
            else
            {
                models = models
                    .OrderBy(x => x.Categories.OrderBy(c => c.Category.SortPos).Select(c => c.Category.SortPos).FirstOrDefault())
                    .ThenBy(x => x.Categories.OrderBy(c => c.Category.SortPos).Select(c => c.SortPos).FirstOrDefault());
            }

            var mapper = new FruitProductListViewMapper();
            var viewModels = models
                .ToList()
                .Select(s => mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = models.Count()
            };

            return Json(result);
        }

    }
}