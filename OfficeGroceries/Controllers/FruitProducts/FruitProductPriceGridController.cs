﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    public class FruitProductPriceGridController : GridController<FruitProductPrice, FruitProductPriceViewModel>
    {

        public FruitProductPriceGridController()
            : base(new FruitProductPriceRepository(), new FruitProductPriceMapper())
        {
        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid productID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.ProductID == productID)
                                  .OrderBy(x => x.BreakPoint)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }
    }
}