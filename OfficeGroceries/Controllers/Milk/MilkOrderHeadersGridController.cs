﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MilkOrderHeadersGridController : GridController<MilkOrderHeader, MilkOrderHeaderViewModel>
    {
        public MilkOrderHeadersGridController()
            : base(new MilkOrderHeaderRepository(), new MilkOrderHeaderMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, int dateType, DateTime? fromDate, DateTime? toDate, List<OrderMode> statuses)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            fromDate = fromDate.Value.AddDays(-7);
            var viewModels = _repo.Read()
                                  .Where(x => !(x.FromDate <= fromDate) || x.OrderMode == OrderMode.Regular)
                                  .Where(x => !(x.FromDate > toDate) || x.OrderMode == OrderMode.Regular)
                                  .Where(x => statuses.Contains(x.OrderMode))
                                  .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                  .Where(x => x.Depreciated == false)
                                  .ToList()
                                  .OrderBy(x => x.OrderMode).ThenBy(x => x.FromDate)
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));


        }
    }

}