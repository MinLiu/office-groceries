﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Web.Script.Serialization;
using System.Net.Mail;
using NPOI.HSSF.UserModel;
using System.Web.Hosting;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MilkOrdersController : EntityController<MilkOrder, MilkOrderViewModel>
    {
        public MilkOrdersController()
            : base(new MilkOrderRepository(), new MilkOrderMapper())
        {

        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SaveOrders(string orders, OrderMode orderMode, bool submit, string date)
        {
            try
            { 
                DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;
                List<MilkOrderViewModel> milkOrderViewModels = new JavaScriptSerializer().Deserialize<List<MilkOrderViewModel>>(orders);

                if (User.Identity.IsAuthenticated)
                {
                    // Only allow user to edit orders when it's in editable time, except for the user does not yet activate milk account
                    if (AppSettings.IsEditableTime() || orderMode == OrderMode.Draft)
                    {
                        // Members
                        var result = SaveMilkOrderHelper.New()
                            .SetCompanyID(CurrentUser.CompanyID)
                            .SaveCustomOrder(orderMode, fromDate, milkOrderViewModels);

                        // Send mails
                        if (orderMode == OrderMode.Regular || orderMode == OrderMode.OneOff)
                        {
                            SendOrderChangeMail(CurrentUser.Company, orderMode, date);
                        }

                        if (orderMode == OrderMode.Regular && result.TotalNet == 0)
                        {
                            SendCancelOrderAlertEmail(CurrentUser.Company);
                        }
                    }
                    else
                    {
                        throw new Exception(AppSettings.FreezingTimeMessage);
                    }
                }
                else
                {
                    // Prospects
                    var result = SaveMilkOrderHelper.New()
                        .SetProspectToken(ProspectToken)
                        .SaveProspectOrder(milkOrderViewModels);
                }
                return Json(new { Success = true });
            }
            catch(Exception e)
            {
                return Json(new { Success = false, ErrorMessage = e.Message });
            }
        }

        // Get dates which has orders
        public ActionResult GetOrderWeekDates(string companyID)
        {
            var companyRepo = new CompanyRepository();

            Guid CompanyID = string.IsNullOrWhiteSpace(companyID)? CurrentUser.CompanyID : Guid.Parse(companyID);

            var company = companyRepo.Find(CompanyID);

            var orderWeeks = _repo.Read()
                                  .Where(fo => fo.OrderHeader.CompanyID == CompanyID)
                                  .Where(fo => fo.OrderHeader.OrderMode == OrderMode.OneOff)
                                  .Select(fo => fo.OrderHeader.FromDate);

            var viewModel = new MilkPageViewModel();
            viewModel.Set(company);
            foreach (var startdates in orderWeeks)
            {
                for (int i = 0; i < 7; i++)
                    viewModel.OrderWeekDates.Add(startdates.Value.AddDays(i));
            }

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult _AddToQuote(Guid milkProductID)
        {
            var service = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), new SnapDbContext());

            var milkProduct = new MilkProductRepository().Read().Where(f => f.ID == milkProductID).FirstOrDefault();

            if (milkProduct == null)
                return Json("No Content");

            var orderHeader = service.GetProspectOrder(ProspectToken);
            if (orderHeader == null)
            {
                orderHeader = new MilkOrderHeader()
                {
                    ProspectToken = ProspectToken,
                    OrderMode = OrderMode.Draft,
                    OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                };
                orderHeader = service.Create(orderHeader);
            }

            // Check if the product is already in the order
            if (orderHeader.Items != null && orderHeader.Items.Any(x => x.MilkProductID == milkProduct.ID))
            {
                return Json("Already exists");
            }

            service.AddItem(orderHeader.ID, milkProduct);

            return Json("Success");
        }

        #region Helper

        public void RecalculateOrderHeader(Guid orderHeaderID)
        {
            var repo = new MilkOrderHeaderRepository();
            var header = repo.Read()
                             .Where(x => x.ID == orderHeaderID)
                             .Include(x => x.Items)
                             .FirstOrDefault();

            var TotalVAT = 0m;
            var TotalNet = 0m;
            foreach(var item in header.Items)
            {
                TotalVAT += item.TotalVAT;
                TotalNet += item.TotalNet;
            }

            header.TotalVAT = TotalVAT;
            header.TotalNet = TotalNet;

            repo.Update(header, new[] { "TotalVAT", "TotalNet" });
        }

        #region Send Order Change Email

        public bool SendOrderChangeMail(Guid companyID, OrderMode orderMode, string date)
        {
            var company = new CompanyRepository().Find(companyID);
            return SendOrderChangeMail(company, orderMode, date);
        }

        public bool SendOrderChangeMail(Company company, OrderMode orderMode, string date)
        {
            SendOrderChangeMailToCustomer(company, orderMode, date);
            SendOrderChangeMailToSuppliers(company, orderMode, date);

            return true;
        }

        private void SendOrderChangeMailToCustomer(Company company, OrderMode orderMode, string date)
        {
            if (orderMode == OrderMode.OneOff)
                SendOneOffOrderChangeMailToCustomer(company, date);
            else if (orderMode == OrderMode.Regular)
                SendPermanentOrderChangeMailToCustomer(company);
        }

        // TO CUSTOMER 8
        private void SendOneOffOrderChangeMailToCustomer(Company company, string date)
        {
            var mailMessage = MilkOneOffOrderChangeEmailHelper.GetEmail(company, date);
            EmailService.SendEmail(mailMessage, Server);
        }

        // TO CUSTOMER 9
        private void SendPermanentOrderChangeMailToCustomer(Company company)
        {
            var mailMessage = MilkRegularOrderChangeEmailHelper.GetEmail(company);
            EmailService.SendEmail(mailMessage, Server);
        }

        private void SendOrderChangeMailToSuppliers(Company company, OrderMode orderMode, string date)
        {
            if (orderMode == OrderMode.OneOff)
                SendOneOffOrderChangeMailToSuppliers(company, date);
            else if (orderMode == OrderMode.Regular)
                SendPermanentOrderChangeMailToSuppliers(company);
        }

        // TO SUPPLIER 6
        private void SendOneOffOrderChangeMailToSuppliers(Company company, string date)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            DateTime fromDate = DateTime.Parse(date);
            DateTime toDate = fromDate.AddDays(6);
            List<CompanySupplier> companySuppliers = company.CompanySuppliers
                                                            .Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                                            .Where(x => x.StartDate.HasValue)
                                                            .Where(x => !(toDate < x.StartDate))
                                                            .Where(x => !x.EndDate.HasValue || !(fromDate > x.EndDate))
                                                            .OrderBy(x => x.StartDate)
                                                            .ToList();
            foreach (var companySupplier in companySuppliers)
            {
                try
                {
                    var mailMessage = new MailMessage();
                    if (companySupplier.Supplier.DailyEmailReminderEnabled)
                    {
                        mailMessage.AddToEmails("support@office-groceries.com");
                    }
                    else if (!string.IsNullOrWhiteSpace(companySupplier.Supplier.SecondaryEmail))
                    {
                        mailMessage.AddToEmails(companySupplier.Supplier.SecondaryEmail);
                    }
                    else
                    {
                        mailMessage.AddToEmails(companySupplier.Supplier.Email);
                    }
                    

                    DateTime startDate;

                    startDate = DateTime.Today.StartOfWeek(DayOfWeek.Monday) < fromDate ? fromDate
                                                                                        : DateTime.Now >= DateTime.Today.AddHours(CutOffTime.TimeOfDay) ? DateTime.Today.AddDays(2)
                                                                                                                                        : DateTime.Today.AddDays(1);


                    mailMessage.Subject = "One Off change to Milk Order (" + fromDate.ToString("dd/MM/yyyy") + " - " + toDate.ToString("dd/MM/yyyy") + ") Confirmation for " + company.Name + "(" + companySupplier.AccountNo + ") from Office-Groceries Ltd";
                    var companyDetails = string.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                                    "Hello<br /><br />" +
                                                    "Please accept this e-mail as confirmation for a <strong>One Off Change</strong> for the Office-Groceries Ltd account below: <br />" +
                                                    "<br />" +
                                                    "<hr />" +
                                                    "<table>" +
                                                        "<tr>" +
                                                            "<td><label>Week:</label></td><td>" + fromDate.ToString("dd/MM/yyyy") + " - " + toDate.ToString("dd/MM/yyyy") + "</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Account Number:</label></td><td>{0}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Company:</label></td><td>{1}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Delivery Address:</label></td><td>{2}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Postcode:</label></td><td>{3}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Delivery Instructions:</label><td>{4}</td>" +
                                                        "</tr>" +
                                                    "</table>" +
                                                    "<hr />" +
                                                    "<br />" +
                                                "</div>", companySupplier.AccountNo
                                                        , company.Name
                                                        , company.Address1
                                                        , company.Postcode
                                                        , company.DeliveryInstruction);



                    var weeklyOrder = GetWeeklyOrderBody(company, fromDate);
                    var regularOrder = GetRegularOrderBody(company);

                    mailMessage.Body = companyDetails + "" +
                        string.Format("<strong>Please action the One Off Change below for the {0} - {1} trading week</strong>", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy")) +
                        weeklyOrder +
                        "<br/><br/>" +
                        "The client's normal standing order is:" +
                        regularOrder;

                    try
                    {
                        string filePath = GenerateExcel(company, OrderMode.OneOff, date, companySupplier.AccountNo, startDate); // test
                        mailMessage.Attachments.Add(new Attachment(filePath) { Name = Path.GetFileName(filePath) });
                    }
                    catch (Exception e) { }
                    
                    mailMessage.IsBodyHtml = true;

                    EmailService.SendEmail(mailMessage, Server);
                }
                catch (Exception e) { }
            }
        }

        // TO SUPPLIER 7
        private void SendPermanentOrderChangeMailToSuppliers(Company company)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            List<CompanySupplier> companySuppliers;
            companySuppliers = company.CompanySuppliers
                                        .Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                        .Where(x => x.StartDate.HasValue)
                                        .Where(x => !x.EndDate.HasValue || x.EndDate.Value >= DateTime.Today)
                                        .OrderBy(x => x.StartDate)
                                        .ToList();
            
            foreach (var companySupplier in companySuppliers)
            {
                try
                {
                    var mailMessage = new MailMessage();
                    if (companySupplier.Supplier.DailyEmailReminderEnabled)
                    {
                        mailMessage.AddToEmails("support@office-groceries.com");
                    }
                    else if (!string.IsNullOrWhiteSpace(companySupplier.Supplier.SecondaryEmail))
                    {
                        mailMessage.AddToEmails(companySupplier.Supplier.SecondaryEmail);
                    }
                    else
                    {
                        mailMessage.AddToEmails(companySupplier.Supplier.Email);
                    }

                    DateTime startDate;
                    
                    // Permanent change
                    startDate = DateTime.Now >= DateTime.Today.AddHours(CutOffTime.TimeOfDay) ? DateTime.Today.AddDays(2) : DateTime.Today.AddDays(1);
                    if (startDate < companySupplier.StartDate.Value)
                        startDate = companySupplier.StartDate.Value;

                    var intermittentDeliveryMessage = "";
                    if (companySupplier.IsIntermittentDelivery && companySupplier.NextDeliveryWeek != null)
                    {
                        intermittentDeliveryMessage =
                            $"<br/><strong>Please note: this customer's next delivery week is {companySupplier.NextDeliveryWeek:dd/MM/yyyy} - {companySupplier.NextDeliveryWeek.Value.AddDays(6):dd/MM/yyyy} trading week</strong>";
                    }
                    
                    mailMessage.Subject = $"Permanent change to Milk Order Confirmation for {company.Name}({companySupplier.AccountNo}) from Office-Groceries Ltd";
                    var companyDetails = string.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                                    "Hello<br /><br />" +
                                                    "Please accept this e-mail as confirmation for a <strong>Permanent Change</strong> for the Office-Groceries Ltd account below: <br />" +
                                                    "<br />" +
                                                    "<strong>Any Permanent Changes received after " + CutOffTime.ToTimeString() + " should only be actioned the day after next.</strong>" +
                                                    intermittentDeliveryMessage +
                                                    "<hr />" +
                                                    "<table>" +
                                                        "<tr>" +
                                                            "<td><label>Account Number:</label></td><td>{0}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Company:</label></td><td>{1}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Delivery Address:</label></td><td>{2}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Postcode:</label></td><td>{3}</td>" +
                                                        "</tr>" +
                                                        "<tr>" +
                                                            "<td><label>Delivery Instructions:</label><td>{4}</td>" +
                                                        "</tr>" +
                                                    "</table>" +
                                                    "<hr />" +
                                                    "<br />" +
                                                "</div>", companySupplier.AccountNo
                                                        , company.Name
                                                        , company.Address1
                                                        , company.Postcode
                                                        , company.DeliveryInstruction);

                    var regularOrderBody = GetRegularOrderBody(company);

                    mailMessage.Body = companyDetails +
                        "<strong>Please action the Permanent Change to the client’s normal standing order below</strong>" +
                        regularOrderBody;

                    try
                    {
                        var lastRegularOrderBody = GetRegularOrderBody(company, true);
                        if (!string.IsNullOrWhiteSpace(lastRegularOrderBody))
                            mailMessage.Body +=
                                "<br/><br/>" +
                                "The client's previous standing order was:" +
                                lastRegularOrderBody;
                    }
                    catch (Exception e) { }
                    
                    try
                    {
                        string filePath = GenerateExcel(company, OrderMode.Regular, "", companySupplier.AccountNo, startDate); // test
                        mailMessage.Attachments.Add(new Attachment(filePath) { Name = Path.GetFileName(filePath) });
                    }
                    catch (Exception e) { }
                    
                    mailMessage.IsBodyHtml = true;

                    EmailService.SendEmail(mailMessage, Server);
                }
                catch(Exception e) { }
            }
        }

        private string GetWeeklyOrderBody(Company company, DateTime dateFrom)
        {
            return GetOrderBody(company, OrderMode.OneOff, dateFrom);
        }

        private string GetRegularOrderBody(Company company, bool depreciated = false)
        {
            return GetOrderBody(company, OrderMode.Regular, null, depreciated);
        }

        private string GetOrderBody(Company company, OrderMode orderMode, DateTime? dateFrom = null, bool depreciated = false)
        {
            try
            { 
                var service = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), new SnapDbContext());
                var weeklyOrder = service.GetCustomerOrder(company.ID, orderMode, dateFrom, depreciated);

                var tableHeader = "<table style='width: 700px; border: 1px solid white;'>" +
                                        "<thead>" +
                                            "<tr style='background-color: #c1c4be;'>" +
                                                "<th>Product</th>" +
                                                "<th>Unit</th>" +
                                                "<th>Mon</th>" +
                                                "<th>Tue</th>" +
                                                "<th>Wed</th>" +
                                                "<th>Thu</th>" +
                                                "<th>Fri</th>" +
                                                "<th>Sat</th>" +
                                                "<th>Weekly Volume</th>" +
                                            "</tr>" +
                                        "</thead>";

                var tableBody = "";
                foreach (var lineItem in weeklyOrder.Items.OrderBy(i => i.ProductName))
                {
                    var str = string.Format("<tr style='background-color: #c1c4be;'>" +
                                                "<td style='text-align: center;'>{0}</td>" +
                                                "<td style='text-align: center;'>{1}</td>" +
                                                "<td style='text-align: center;'>{2}</td>" +
                                                "<td style='text-align: center;'>{3}</td>" +
                                                "<td style='text-align: center;'>{4}</td>" +
                                                "<td style='text-align: center;'>{5}</td>" +
                                                "<td style='text-align: center;'>{6}</td>" +
                                                "<td style='text-align: center;'>{7}</td>" +
                                                "<td style='text-align: center;'>{8}</td>" +
                                            "</tr>", lineItem.ProductName
                                                   , lineItem.Unit
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Monday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Tuesday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Wednesday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Thursday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Friday)
                                                   , string.Format("{0:0.######;-0.######;\"\"}", lineItem.Saturday)
                                                   , lineItem.WeeklyVolume);
                    tableBody += str;
                }

                tableBody += string.Format("<tr style='background-color: #c1c4be;'>" +
                                                "<td style='text-align: center;' colspan = '8'>Total</td>" +
                                                "<td style='text-align: center;'>{0}</td>" +
                                            "</tr>", weeklyOrder.Items.Sum(x => x.WeeklyVolume));
                var tableFooter = "</table>";

                return tableHeader + tableBody + tableFooter;
            }
            catch(Exception e)
            {
                return "";
            }
        }

        private void SendCancelOrderAlertEmail(Company company)
        {
            var mailMessage = CancelOrderAlertEmailHelper.GetEmail(company, "milk");
            EmailService.SendEmail(mailMessage, Server);
        }
        
        public string GenerateExcel(Company company, OrderMode orderMode, string date, string acctNum, DateTime StartDate)
        {
            DateTime? FromDate = !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;

            var orderHelper = new CustomerOrderFormHelper();

            orderHelper.SetCustName(company.Name);
            orderHelper.SetCustInfo("");
            orderHelper.SetAcctNumber(acctNum);
            orderHelper.SetDateDeliveryRequired(orderMode == OrderMode.Regular ? String.Format("From {0}", StartDate.ToString("dd/MM/yyyy")) 
                                                                               : String.Format("{0} - {1}", FromDate.Value.ToString("dd/MM/yyyy"), FromDate.Value.AddDays(6).ToString("dd/MM/yyyy")));
            var orderHeaderRepo = new MilkOrderHeaderRepository();
            var orderHeader = orderHeaderRepo.Read()
                                             .Where(x => x.CompanyID == company.ID)
                                             .Where(x => x.OrderMode == orderMode)
                                             .Where(x => orderMode == OrderMode.Regular || x.FromDate == FromDate)
                                             .Include(x => x.Items)
                                             .FirstOrDefault();
            //var lineItems = new MilkOrderRepository().Read()
            //                                          .Where(x => x.OrderHeader.CompanyID == company.ID)
            //                                          .Where(x => x.OrderHeader.OrderMode == orderMode)
            //                                          .Where(x => x.OrderHeader.FromDate == FromDate)
            //                                          .ToList();

            foreach (var item in orderHeader.Items)
            {
                orderHelper.NewLineItem(item.ProductName, item.ProductName, item.Monday, item.Tuesday, item.Wednesday, item.Thursday, item.Friday, item.Saturday, item.OrderHeader.OrderMode);
            }

            //Write the workbook to a memory stream
            const string contentFolderRoot = "/MilkOrders/";
            var virtualPath = string.Format("{0}{1}", contentFolderRoot, company.ID.ToString());


            var physicalPath = HostingEnvironment.MapPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var fileName = "";
            if (orderMode == OrderMode.Regular)
                fileName = String.Format("/Regular_Order (Updated {0}).xls", DateTime.Now.ToString("dd-MM-yy HH-mm-ss"));
            else
                fileName = String.Format("/OneOff_Order({0} ~ {1}) (Updated {2}).xls", FromDate.Value.ToString("dd-MM-yyyy"), FromDate.Value.AddDays(6).ToString("dd-MM-yyyy"), DateTime.Now.ToString("dd-MM-yy HH-mm-ss"));

            string physicalSavePath = HostingEnvironment.MapPath(virtualPath) + fileName;
            var workbook = orderHelper.Workbook;
            using (FileStream file = new FileStream(physicalSavePath, FileMode.Create, FileAccess.ReadWrite))
            {
                workbook.Write(file);
                file.Close();
            }

            orderHeader.FileURL = virtualPath + fileName;
            orderHeaderRepo.Update(orderHeader);

            return physicalSavePath;
        }

        #endregion

        #region Initial Order set up email
        public bool SendOrderInitializeMail(Guid companyID, DateTime startFrom, CompanySupplier companySupplier)
        {
            var company = new CompanyRepository().Find(companyID);
            return SendOrderInitializeMail(company, startFrom, companySupplier);
        }

        public bool SendOrderInitializeMail(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            SendOrderIntializeMailToCustomer(company, startFrom, companySupplier);
            SendOrderIntializeMailToSupplier(company, startFrom, companySupplier);

            return true;
        }

        // TO CUSTOMER 4
        public void SendOrderIntializeMailToCustomer(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            var mailMessage = MilkOrderInitializeEmailHelper.GetEmail(company, startFrom, companySupplier);
            EmailService.SendEmail(mailMessage, Server);
        }

        // TO SUPPLIER 5
        public void SendOrderIntializeMailToSupplier(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            var setting = new SnapDbContext().EmailSettings.First();

            MailMessage mailMessage = new MailMessage();
            mailMessage.AddToEmails(companySupplier.Supplier.Email);
            mailMessage.From = new MailAddress(setting.Email);

            try
            {
                DateTime startDate;
                mailMessage.Subject = "Milk Order Confirmation from Office-Groceries Ltd";
                startDate = DateTime.Now >= DateTime.Today.AddHours(CutOffTime.TimeOfDay) ? DateTime.Today.AddDays(2) : DateTime.Today.AddDays(1);
                if (startDate < companySupplier.StartDate.Value)
                    startDate = companySupplier.StartDate.Value;

                mailMessage.Body = String.Format("<div class=\"row\" style='max-width: 700px;'>" +
                                                "Hello<br /><br />" +
                                                "Please accept this e-mail as confirmation for a new Office-Groceries Ltd account due to start on <br />" +
                                                "<br />" +
                                                "<strong>Please check final order on attachment as the original request may have changed.</strong>" +
                                                "<hr />" +
                                                "<table>" +
                                                    "<tr>" +
                                                        "<td><label>Start Date:</label></td><td>{0}</td>" +
                                                    "</tr>" +
                                                    "<tr>" +
                                                        "<td><label>Account Number:</label></td><td>{1}</td>" +
                                                    "</tr>" +
                                                    "<tr>" +
                                                        "<td><label>Company:</label></td><td>{2}</td>" +
                                                    "</tr>" +
                                                    "<tr>" +
                                                        "<td><label>Delivery Address:</label></td><td>{3}</td>" +
                                                    "</tr>" +
                                                    "<tr>" +
                                                        "<td><label>Postcode:</label></td><td>{4}</td>" +
                                                    "</tr>" +
                                                    "<tr>" +
                                                        "<td><label>Delivery Instructions:</label><td>{5}</td>" +
                                                    "</tr>" +
                                                "</table>" +
                                                "<hr />" +
                                                "<br />" +
                                            "</div>", startFrom.ToString("dd/MM/yyyy")
                                                    , companySupplier.AccountNo
                                                    , company.Name
                                                    , company.Address1
                                                    , company.Postcode
                                                    , company.DeliveryInstruction);
                try
                {
                    string filePath = GenerateExcel(company, OrderMode.Regular, "", companySupplier.AccountNo, startDate);
                    mailMessage.Attachments.Add(new Attachment(filePath) { Name = Path.GetFileName(filePath) });
                }
                catch (Exception e) { }
                mailMessage.IsBodyHtml = true;

                EmailService.SendEmail(mailMessage, Server);
            }
            catch (Exception e) { }
        }

        #endregion

        #endregion

    }

}