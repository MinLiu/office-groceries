﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NuIngredient.Startup))]
namespace NuIngredient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
#if !DEBUG
            Schedule.Schedule.ScheduleStart();
#endif
        }
    }
}
