﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class CheckOutPageViewModel
    {
        public CheckOutPageViewModel() { ShoppingCart = new ShoppingCartPageViewModel(); }
        public ShoppingCartPageViewModel ShoppingCart { get; set; }
        public BillingDetailsViewModel BillingDetails { get; set; }
        public List<OrderEventDetailViewModel> EventDetails { get; set; }
        public string CompanyID { get; set; }
    }

    public class BillingDetailsViewModel
    {
        [Required]
        [Display(Name="Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [Display(Name="Email Address")]
        public string EmailAddress { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        [Display(Name = "Delivery Address")]
        public string DeliveryAddress { get; set; }
        [Display(Name = "Delivery County")]
        public string DeliveryCounty { get; set; }
        [Required]
        [Display(Name = "Delivery PostCode")]
        public string DeliveryPostCode { get; set; }
        [Display(Name = "Billing Address")]
        public string BillingAddress { get; set; }
        [Display(Name = "Billing County")]
        public string BillingCounty { get; set; }
        [Display(Name = "Billing PostCode")]
        public string BillingPostCode { get; set; }
        [Display(Name = "Order Notes")]
        public string OrderNotes { get; set; }
        [Display(Name = "PO Number")]
        public string PONumber { get; set; }
    }
}