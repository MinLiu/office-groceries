﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{

    public class DashboardMilkOrderViewModel
    {
        public decimal WeekyTotal { get; set; }
        public List<MilkOrderViewModel> WeeklyOrder { get; set; }
    }

}

