﻿using System.Web;

namespace NuIngredient.Models
{
    public class OpayoPaymentResultViewModel
    {
        public string VendorTxCode { get; set; }
        public string VPSTxId { get; set; }
        public string Status { get; set; }
        public string StatusDetail { get; set; }
        public string TxAuthNo { get; set; }
        public string AVSCV2 { get; set; }
        public string AddressResult { get; set; }
        public string PostCodeResult { get; set; }
        public string CV2Result { get; set; }
        public string GiftAid { get; set; }
        public string ThreeDSecureStatus { get; set; }
        public string CardType { get; set; }
        public string Last4Digits { get; set; }
        public string DeclineCode { get; set; }
        public string ExpiryDate { get; set; }
        public string SchemeTraceID { get; set; }
        public string Amount { get; set; }
        public string BankAuthCode { get; set; }

        public OpayoPaymentResultViewModel(string queryParameters)
        {
            var resultDic = HttpUtility.ParseQueryString(queryParameters);
            VendorTxCode = resultDic["VendorTxCode"];
            VPSTxId = resultDic["VPSTxId"];
            Status = resultDic["Status"];
            StatusDetail = resultDic["StatusDetail"];
            TxAuthNo = resultDic["TxAuthNo"];
            AVSCV2 = resultDic["AVSCV2"];
            AddressResult = resultDic["AddressResult"];
            PostCodeResult = resultDic["PostCodeResult"];
            CV2Result = resultDic["CV2Result"];
            GiftAid = resultDic["GiftAid"];
            ThreeDSecureStatus = resultDic["3DSecureStatus"];
            CardType = resultDic["CardType"];
            Last4Digits = resultDic["Last4Digits"];
            DeclineCode = resultDic["DeclineCode"];
            ExpiryDate = resultDic["ExpiryDate"];
            SchemeTraceID = resultDic["SchemeTraceID"];
            Amount = resultDic["Amount"];
            BankAuthCode = resultDic["BankAuthCode"];
        }
    }
}