﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NuIngredient.Models
{

    public class FruitWeeklyOrderPageViewModel
    {
        public OrderMode OrderMode { get; set; }
        public bool HasFruitSupplier { get; set; }
        public bool LiveOnFruit { get; set; }
        public bool HasRegularFruitOrder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool HasSnackSupplier { get; set; }
        public bool LiveOnSnack { get; set; }
        public DateTime SnackStartDate { get; set; }
        public DateTime SnackEndDate { get; set; }
        public List<FruitOrderViewModel> FruitOrders { get; set; }
        public List<string> Weekdates { get; set; }
        public List<bool> Enables { get; set; }
        public List<bool> SnackEnables { get; set; }
        private List<bool> _deliveryDays { get; set; }
        private List<DateTime> _holidays { get; set; }
        private List<DateTime> _openDays { get; set; }
        private List<DateTime> _snackHolidays { get; set; }
        private List<DateTime> _snackOpenDays { get; set; }

        public FruitWeeklyOrderPageViewModel(OrderMode orderMode, string weekStart, Guid? companyID)
        {
            OrderMode = orderMode;
            HasFruitSupplier = false;
            LiveOnFruit = false;
            StartDate = DateTime.MaxValue.AddDays(-1);
            EndDate = DateTime.MaxValue;
            FruitOrders = new List<FruitOrderViewModel>();
            Weekdates = new List<string>();
            Enables = new List<bool>();
            SnackEnables = new List<bool>();
            
            LoadDeliveryDays(companyID);
            
            if (orderMode == OrderMode.OneOff)
            {
                DateTime dateWeekStart = DateTime.Parse(weekStart);
                var dateMon = dateWeekStart.StartOfWeek(DayOfWeek.Monday);
                var dateTue = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(1);
                var dateWed = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(2);
                var dateThu = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(3);
                var dateFri = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(4);
                var dateSat = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(5);

                LoadSpecialDays(dateMon, companyID);

                Weekdates.AddRange(new List<string>()
                {
                    dateMon.ToString("dd/MM"),
                    dateTue.ToString("dd/MM"),
                    dateWed.ToString("dd/MM"),
                    dateThu.ToString("dd/MM"),
                    dateFri.ToString("dd/MM"),
                    dateSat.ToString("dd/MM")
                });

                var dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date.AddDays(1);
                while (dateNotAllowedBefore.DayOfWeek == DayOfWeek.Saturday ||
                       dateNotAllowedBefore.DayOfWeek == DayOfWeek.Sunday ||
                       _holidays.Contains(dateNotAllowedBefore))
                {
                    dateNotAllowedBefore = dateNotAllowedBefore.AddDays(1);
                }
                
                Enables.AddRange(new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon, SupplierType.Fruits) || IsOpenDay(dateMon, SupplierType.Fruits)) && (IsOpenDay(dateMon, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue, SupplierType.Fruits) || IsOpenDay(dateTue, SupplierType.Fruits)) && (IsOpenDay(dateTue, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed, SupplierType.Fruits) || IsOpenDay(dateWed, SupplierType.Fruits)) && (IsOpenDay(dateWed, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu, SupplierType.Fruits) || IsOpenDay(dateThu, SupplierType.Fruits)) && (IsOpenDay(dateThu, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri, SupplierType.Fruits) || IsOpenDay(dateFri, SupplierType.Fruits)) && (IsOpenDay(dateFri, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat, SupplierType.Fruits) || IsOpenDay(dateSat, SupplierType.Fruits)) && (IsOpenDay(dateSat, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Saturday]),
                });
                
                dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date.AddDays(1);
                while (dateNotAllowedBefore.DayOfWeek == DayOfWeek.Saturday ||
                       dateNotAllowedBefore.DayOfWeek == DayOfWeek.Sunday ||
                       _snackHolidays.Contains(dateNotAllowedBefore))
                {
                    dateNotAllowedBefore = dateNotAllowedBefore.AddDays(1);
                }
                SnackEnables = new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon, SupplierType.Snacks) || IsOpenDay(dateMon, SupplierType.Snacks)) && (IsOpenDay(dateMon, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue, SupplierType.Snacks) || IsOpenDay(dateTue, SupplierType.Snacks)) && (IsOpenDay(dateTue, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed, SupplierType.Snacks) || IsOpenDay(dateWed, SupplierType.Snacks)) && (IsOpenDay(dateWed, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu, SupplierType.Snacks) || IsOpenDay(dateThu, SupplierType.Snacks)) && (IsOpenDay(dateThu, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri, SupplierType.Snacks) || IsOpenDay(dateFri, SupplierType.Snacks)) && (IsOpenDay(dateFri, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat, SupplierType.Snacks) || IsOpenDay(dateSat, SupplierType.Snacks)) && (IsOpenDay(dateSat, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Saturday]),

                };

            }
            else if (orderMode == OrderMode.Draft)
            {
                Weekdates.AddRange(new List<string>()
                {
                    "","","","","","",
                });

                Enables.AddRange(new List<bool>()
                {
                    true, true, true, true, true, true,
                });
                
                SnackEnables = new List<bool>()
                {
                    true, true, true, true, true, true,
                };
            }
            else // Regular
            {
                Weekdates.AddRange(new List<string>()
                {
                    "","","","","","",
                });

                var weekday = DateTime.Now.DayOfWeek;
                
                var editableDays = EditableDays[weekday];
                
                Enables.AddRange(new List<bool>()
                {
                    editableDays.Contains(DayOfWeek.Monday),
                    editableDays.Contains(DayOfWeek.Tuesday),
                    editableDays.Contains(DayOfWeek.Wednesday),
                    editableDays.Contains(DayOfWeek.Thursday),
                    editableDays.Contains(DayOfWeek.Friday),
                    editableDays.Contains(DayOfWeek.Saturday),
                });
                
                SnackEnables = new List<bool>()
                {
                    editableDays.Contains(DayOfWeek.Monday),
                    editableDays.Contains(DayOfWeek.Tuesday),
                    editableDays.Contains(DayOfWeek.Wednesday),
                    editableDays.Contains(DayOfWeek.Thursday),
                    editableDays.Contains(DayOfWeek.Friday),
                    editableDays.Contains(DayOfWeek.Saturday),
                };
            }
        }

        private static readonly Dictionary<DayOfWeek, List<DayOfWeek>> EditableDays = 
            new Dictionary<DayOfWeek, List<DayOfWeek>>()
            {
                { DayOfWeek.Monday, new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday } },
                { DayOfWeek.Tuesday, new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Thursday, DayOfWeek.Friday } },
                { DayOfWeek.Wednesday, new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Friday } },
                { DayOfWeek.Thursday, new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday } },
                { DayOfWeek.Friday, new List<DayOfWeek>() { DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday } },
                { DayOfWeek.Saturday, new List<DayOfWeek>() },
                { DayOfWeek.Sunday, new List<DayOfWeek>() },
            };
            
        public void Set(Company company)
        {
            HasFruitSupplier = company.Method_HasSupplier(SupplierType.Fruits);
            LiveOnFruit = company.Method_IsLive(SupplierType.Fruits);
            var fruitSupplier = new CompanySupplierRepository().Read()
                                                          .Where(cs => cs.CompanyID == company.ID)
                                                          .Where(cs => cs.Supplier.SupplierType == SupplierType.Fruits)
                                                          .Where(cs => cs.Supplier.Deleted == false)
                                                          .Where(cs => cs.StartDate.HasValue)
                                                          .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                                                          .OrderBy(cs => cs.StartDate.Value)
                                                          .FirstOrDefault();
            StartDate = fruitSupplier?.StartDate ?? DateTime.MaxValue.AddDays(-1);
            EndDate = fruitSupplier?.EndDate ?? DateTime.MaxValue;
            
            HasSnackSupplier = company.Method_HasSupplier(SupplierType.Snacks);
            LiveOnSnack = company.Method_IsLive(SupplierType.Snacks);
            var snackSupplier = new CompanySupplierRepository().Read()
                .Where(cs => cs.CompanyID == company.ID)
                .Where(cs => cs.Supplier.SupplierType == SupplierType.Snacks)
                .Where(cs => cs.Supplier.Deleted == false)
                .Where(cs => cs.StartDate.HasValue)
                .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                .OrderBy(cs => cs.StartDate.Value)
                .FirstOrDefault();
            SnackStartDate = snackSupplier?.StartDate ?? DateTime.MaxValue.AddDays(-1);
            SnackEndDate = snackSupplier?.EndDate ?? DateTime.MaxValue;
            
            HasRegularFruitOrder = new FruitOrderHeaderRepository().Read()
                                                                   .Where(x => x.OrderMode == OrderMode.Regular)
                                                                   .Where(x => x.CompanyID == company.ID)
                                                                   .Count() > 0;
        }
        public void Set(string prospectToken)
        {
            HasFruitSupplier = true;
            LiveOnFruit = true;
            StartDate = new DateTime(1900, 1, 1);
            EndDate = new DateTime(2100, 1, 1);
            HasRegularFruitOrder = true;
            _deliveryDays = new List<bool> { true, true, true, true, true, true };
        }

        private void LoadSpecialDays(DateTime dateFrom, Guid? companyID)
        {
            Guid? fruitSupplierId = null;
            Guid? snackSupplierId = null;

            if (companyID != null)
            {
                fruitSupplierId = SupplierHelper.GetSupplier(companyID.Value, SupplierType.Fruits, dateFrom)?.ID;
                snackSupplierId = SupplierHelper.GetSupplier(companyID.Value, SupplierType.Snacks, dateFrom)?.ID;
            }
            
            var dateTo = dateFrom.AddDays(6);
            _holidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(fruitSupplierId)
               .Where(x => dateFrom <= x.Date)
               .Where(x => dateTo >= x.Date)
               .Select(x => x.Date)
               .ToList();
            
            _openDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(fruitSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
            
            _snackHolidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(snackSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
            
            _snackOpenDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(snackSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
        }
        
        private void LoadDeliveryDays(Guid? companyId)
        {
            if (companyId == null)
            {
                _deliveryDays = new List<bool>
                {
                    true, true, true, true, true, true
                };
                return;
            }

            using (var db = new SnapDbContext())
            {
                var company = db.Companies.Find(companyId.Value);

                _deliveryDays = new List<bool>
                {
                    company.FruitsDeliveryMonday,
                    company.FruitsDeliveryTuesday,
                    company.FruitsDeliveryWednesday,
                    company.FruitsDeliveryThursday,
                    company.FruitsDeliveryFriday,
                    company.FruitsDeliverySaturday
                };
            }
        }

        private bool NotHoliday(DateTime date, SupplierType supplierType)
        {
            return supplierType == SupplierType.Fruits
                ? !_holidays.Contains(date.Date)
                : !_snackHolidays.Contains(date.Date);
        }
        
        private bool IsOpenDay(DateTime date, SupplierType supplierType)
        {
            return supplierType == SupplierType.Fruits
                ? _openDays.Contains(date.Date)
                : _snackOpenDays.Contains(date.Date);
        }

        /// <summary>
        /// Determines if a specific weekday is enabled for ordering fruit.
        /// </summary>
        public bool IsAvailableDay(OgDayOfWeek dayOfWeek, FruitProductType type)
        {
            if (OrderMode == OrderMode.Draft || OrderMode == OrderMode.Regular)
            {
                return _deliveryDays[(int)dayOfWeek];
            }

            if (OrderMode == OrderMode.OneOff)
            {
                return type == FruitProductType.Fruit
                    ? Enables[(int)dayOfWeek]
                    : SnackEnables[(int)dayOfWeek];
            }
            
            throw new NotImplementedException();
        }

    }

}

