﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NuIngredient.Models
{

    public class FruitOrderRowPageViewModel
    {
        private OrderMode OrderMode { get; set; }
        public List<FruitOrderViewModel> FruitOrders { get; set; }
        public List<bool> Enables { get; set; }
        public List<bool> SnackEnables { get; set; }
        private List<bool> _deliveryDays { get; set; }
        private List<DateTime> _holidays { get; set; }
        private List<DateTime> _openDays { get; set; }
        private List<DateTime> _snackHolidays { get; set; }
        private List<DateTime> _snackOpenDays { get; set; }

        public FruitOrderRowPageViewModel(OrderMode orderMode, DateTime? weekStart, Guid? companyID)
        {
            OrderMode = orderMode;
            FruitOrders = new List<FruitOrderViewModel>();
            Enables = new List<bool>();
            SnackEnables = new List<bool>();
            LoadDeliveryDays(companyID);
            
            if (orderMode == OrderMode.OneOff)
            {
                DateTime dateWeekStart = weekStart.Value;
                var dateMon = dateWeekStart.StartOfWeek(DayOfWeek.Monday);
                var dateTue = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(1);
                var dateWed = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(2);
                var dateThu = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(3);
                var dateFri = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(4);
                var dateSat = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(5);

                LoadSpecialDays(dateMon, companyID);

                var dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date;
                Enables.AddRange(new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon, SupplierType.Fruits) || IsOpenDay(dateMon, SupplierType.Fruits)) && (IsOpenDay(dateMon, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue, SupplierType.Fruits) || IsOpenDay(dateTue, SupplierType.Fruits)) && (IsOpenDay(dateTue, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed, SupplierType.Fruits) || IsOpenDay(dateWed, SupplierType.Fruits)) && (IsOpenDay(dateWed, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu, SupplierType.Fruits) || IsOpenDay(dateThu, SupplierType.Fruits)) && (IsOpenDay(dateThu, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri, SupplierType.Fruits) || IsOpenDay(dateFri, SupplierType.Fruits)) && (IsOpenDay(dateFri, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat, SupplierType.Fruits) || IsOpenDay(dateSat, SupplierType.Fruits)) && (IsOpenDay(dateSat, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Saturday]),
                });
                
                dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date;
                SnackEnables.AddRange(new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon, SupplierType.Snacks) || IsOpenDay(dateMon, SupplierType.Snacks)) && (IsOpenDay(dateMon, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue, SupplierType.Snacks) || IsOpenDay(dateTue, SupplierType.Snacks)) && (IsOpenDay(dateTue, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed, SupplierType.Snacks) || IsOpenDay(dateWed, SupplierType.Snacks)) && (IsOpenDay(dateWed, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu, SupplierType.Snacks) || IsOpenDay(dateThu, SupplierType.Snacks)) && (IsOpenDay(dateThu, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri, SupplierType.Snacks) || IsOpenDay(dateFri, SupplierType.Snacks)) && (IsOpenDay(dateFri, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat, SupplierType.Snacks) || IsOpenDay(dateSat, SupplierType.Snacks)) && (IsOpenDay(dateSat, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Saturday]),
                });
            }
            else
            {
                Enables.AddRange(new List<bool>()
                {
                    true, true, true, true, true, true,
                });
                
                SnackEnables.AddRange(new List<bool>()
                {
                    true, true, true, true, true, true,
                });
            }
        }

        private void LoadDeliveryDays(Guid? companyId)
        {
            if (companyId == null)
            {
                _deliveryDays = new List<bool>
                {
                    true, true, true, true, true, true
                };
                return;
            }
            
            using (var db = new SnapDbContext())
            {
                var company = db.Companies.Find(companyId.Value);
            
                _deliveryDays = new List<bool>
                {
                    company.FruitsDeliveryMonday,
                    company.FruitsDeliveryTuesday,
                    company.FruitsDeliveryWednesday,
                    company.FruitsDeliveryThursday,
                    company.FruitsDeliveryFriday,
                    company.FruitsDeliverySaturday
                };
            }
        }
        
        private void LoadSpecialDays(DateTime dateFrom, Guid? companyID)
        {
            Guid? fruitSupplierId = null;
            Guid? snackSupplierId = null;

            if (companyID != null)
            {
                fruitSupplierId = SupplierHelper.GetSupplier(companyID.Value, SupplierType.Fruits, dateFrom)?.ID;
                snackSupplierId = SupplierHelper.GetSupplier(companyID.Value, SupplierType.Snacks, dateFrom)?.ID;
            }
            
            var dateTo = dateFrom.AddDays(6);
            _holidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(fruitSupplierId)
               .Where(x => dateFrom <= x.Date)
               .Where(x => dateTo >= x.Date)
               .Select(x => x.Date)
               .ToList();
            
            _openDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(fruitSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
            
            _snackHolidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(snackSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
            
            _snackOpenDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(snackSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
        }

        private bool NotHoliday(DateTime date, SupplierType supplierType)
        {
            return supplierType == SupplierType.Fruits
                ? !_holidays.Contains(date.Date)
                : !_snackHolidays.Contains(date.Date);
        }
        
        private bool IsOpenDay(DateTime date, SupplierType supplierType)
        {
            return supplierType == SupplierType.Fruits
                ? _openDays.Contains(date.Date)
                : _snackOpenDays.Contains(date.Date);
        }
        
        /// <summary>
        /// Determines if a specific weekday is enabled for ordering fruit.
        /// </summary>
        public bool IsAvailableDay(OgDayOfWeek dayOfWeek, FruitProductType type)
        {
            if (OrderMode == OrderMode.Draft || OrderMode == OrderMode.Regular)
            {
                return _deliveryDays[(int)dayOfWeek];
            }

            if (OrderMode == OrderMode.OneOff)
            {
                return type == FruitProductType.Fruit
                    ? Enables[(int)dayOfWeek]
                    : SnackEnables[(int)dayOfWeek];
            }
            
            throw new NotImplementedException();
        }

    }

}

