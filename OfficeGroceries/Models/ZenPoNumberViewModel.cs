﻿using System;
using Fruitful.Utils.Encrypts;

namespace NuIngredient.Models
{
    public class ZenPoNumberViewModel
    {
        public string InvoiceNumber { get; set; }
        public decimal Amount { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string AccountNumber { get; set; }
        public string HashCode { get; set; }
        
        private readonly string _salt = "c86c21cb";
            
        public bool IsValid()
        {
            var plain = $"{InvoiceNumber}{Amount:0.00}{Year:0000}{Month:00}{AccountNumber}{_salt}";
            var encrypt = Sha1.Hash(plain);
            // return true;
            return string.Equals(HashCode,
                encrypt,
                StringComparison.InvariantCultureIgnoreCase);
        }

        
    }
}