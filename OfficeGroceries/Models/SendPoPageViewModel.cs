﻿using System;
using System.Collections.Generic;

namespace NuIngredient.Models
{
    public class SendPoPageViewModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}