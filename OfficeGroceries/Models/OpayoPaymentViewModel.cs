﻿using System;
using Fruitful.Extensions;

namespace NuIngredient.Models
{
    //https://developer-eu.elavon.com/docs/opayo-forms/api-reference/crypt-field
    public class OpayoPaymentViewModel
    {
        public OpayoPaymentViewModel(ZenPaymentInfoViewModel item)
        {
            InvoiceNumber = item.InvoiceNumber;
            Amount = item.Amount;
            AccountNumber = item.AccountNumber;
            Month = GetFullMonth(item.Month);
            // Email = item.Email?.Split(';')[0] ?? "";
            Email = item.Email?.Replace(" ", "").Replace(",", ":").Replace(";", ":");
            
            var nameSplits = SplitNames(item.BillingName, 20);
            BillingFirstnames = nameSplits[0].Trim().Truncate(20);
            BillingSurname = nameSplits[1].Trim().Truncate(20);
            BillingAddress1 = item.BillingAddress?.Trim().Truncate(45).Trim();
            BillingCity = ".";
            BillingPostCode = item.BillingPostCode?.Trim().Truncate(10);
        }
        
        public string InvoiceNumber { get; set; }
        public decimal Amount { get; set; }
        public string Month { get; set; }
        public string AccountNumber { get; set; }
        public string Email { get; set; }
        public string BillingSurname { get; set; }
        public string BillingFirstnames { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingCity { get; set; }
        public string BillingPostCode { get; set; }
        public string DeliverySurname { get; set; }
        public string DeliveryFirstnames { get; set; }
        public string DeliveryAddress1 { get; set; }
        public string DeliveryCity { get; set; }
        public string DeliveryPostCode { get; set; }
        public string CryptField { get; set; }

        public void SetDeliveryInfo(Company company)
        {
            var nameSplits = SplitNames(company.Name, 20);
            DeliveryFirstnames = nameSplits[0].Truncate(20);
            DeliverySurname = nameSplits[1].Truncate(20);
            DeliveryAddress1 = company.Address1?.Trim().Truncate(45).Trim();
            DeliveryCity = ".";
            DeliveryPostCode = company.Postcode?.Trim().Truncate(10);
            
            // For Testing, To Delete Later ON
            if (!string.IsNullOrWhiteSpace(company.BillingCompanyName))
            {
                var billingNameSplits = SplitNames(company.BillingCompanyName, 20);
                BillingFirstnames = billingNameSplits[0].Truncate(20);
                BillingSurname = billingNameSplits[1].Truncate(20);
            }
            else
            {
                BillingFirstnames = DeliveryFirstnames;
                BillingSurname = DeliverySurname;
            }
            BillingAddress1 = !string.IsNullOrWhiteSpace(company.BillingAddress)
                ? company.BillingAddress.Truncate(45).Trim()
                : DeliveryAddress1;
            BillingCity = ".";
            BillingPostCode = !string.IsNullOrWhiteSpace(company.BillingPostcode)
                ? company.BillingPostcode.Trim().Truncate(10)
                : DeliveryPostCode;
        }

        private string[] SplitNames(string name, int maxLength)
        {
            var splitNames = name.Trim().Split(' ');

            var firstName = "";
            var lastName = "";

            if (splitNames.Length == 2)
            {
                firstName = !string.IsNullOrWhiteSpace(splitNames[0]) ? splitNames[0] : ".";
                lastName = !string.IsNullOrWhiteSpace(splitNames[1]) ? splitNames[1] : ".";
                return new[] { firstName, lastName };
            }
            else if (splitNames.Length == 1)
            {
                firstName = !string.IsNullOrWhiteSpace(splitNames[0]) ? splitNames[0] : ".";
                lastName = ".";
                return new[] { firstName, lastName };
            }

            var firstNameFull = false;
            for (var i = 0; i < splitNames.Length - 1; i++)
            {
                var currentStr = splitNames[i];

                if (currentStr.Length > maxLength)
                    currentStr = currentStr.Truncate(maxLength);

                if (firstNameFull || firstName.Length + currentStr.Length > maxLength)
                {
                    firstNameFull = true;
                }
                else
                {
                    firstName += currentStr + " ";
                    continue;
                }

                if (lastName.Length + currentStr.Length > maxLength)
                {
                    break;
                }
                else
                {
                    lastName += currentStr + " ";
                }
            }
            
            if (lastName.Length + splitNames[splitNames.Length-1].Length <= maxLength)
            {
                lastName += splitNames[splitNames.Length-1];
            }

            if (string.IsNullOrEmpty(firstName))
            {
                firstName = ".";
            }

            if (string.IsNullOrEmpty(lastName))
            {
                lastName = ".";
            }
            
            return new[] { firstName, lastName };

        }
        
        // For Testing, To Remove Later on
        private static string GetFullMonth(string month)
        {
            var year = DateTime.Now.Year;
            switch (month)
            {
                case "Jan":
                    return $"January {year}";
                case "Feb":
                    return $"February {year}";
                case "Mar":
                    return $"March {year}";
                case "Apr":
                    return $"April {year}";
                case "May":
                    return $"May {year}";
                case "Jun":
                    return $"June {year}";
                case "Jul":
                    return $"July {year}";
                case "Aug":
                    return $"August {year}";
                case "Sep":
                    return $"September {year}";
                case "Oct":
                    return $"October {year}";
                case "Nov":
                    return $"November {year}";
                case "Dec":
                    return $"December {year - 1}";
                default:
                    return month;
            }
        }
    }
}