﻿using System;
using Fruitful.Utils.Encrypts;

namespace NuIngredient.Models
{
    public class ZenPaymentInfoViewModel
    {
        public string InvoiceNumber { get; set; }
        public decimal Amount { get; set; }
        public string Month { get; set; }
        public string AccountNumber { get; set; }
        public string Email { get; set; }
        public string HashCode { get; set; }
        public string BillingName { get; set; } = "Test";
        public string BillingAddress { get; set; } = "Test Address";
        public string BillingPostCode { get; set; } = "Test PostCode";
        
        private readonly string _salt = "c86c21cb";
            
        public bool IsValid()
        {
            var plain = $"{InvoiceNumber}{Amount:0.00}{Month}{AccountNumber}{Email}{_salt}";
            var encrypt = Sha1.Hash(plain);
            // return true;
            return string.Equals(HashCode,
                encrypt,
                StringComparison.InvariantCultureIgnoreCase);
        }

        
    }
}