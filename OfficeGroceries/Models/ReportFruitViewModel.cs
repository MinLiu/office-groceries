﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ReportFruitViewModel
    {
        public string ID { get; set; }
        public string AccountNo { get; set; }
        public string ProductName { get; set; }
        public int TotalQty { get; set; }
        public decimal Total { get; set; }
        public int Credit { get; set; }
    }

}

