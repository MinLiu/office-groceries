﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ReportFirstDeliveryDateGridViewModel
    {
        public string ID { get; set; }
        public string AccountNumber { get; set; }
        public string Supplier { get; set; }
        public string CompanyName { get; set; }
        public string Category { get; set; }
        public string StartDate { get; set; }
    }

}

