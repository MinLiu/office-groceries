﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class QuoteCartViewModel
    {
        public List<CartItemsBySuppliers> DryGoodsQuote { get; set; }
        public List<MilkOrderViewModel> MilkQuote { get; set; }
        public List<FruitOrderViewModel> FruitQuote { get; set; }
    }

}