﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class BreadCrumbViewModel
    {
        public BreadCrumbViewModel(string label) { Label = label; Url = null; }
        public BreadCrumbViewModel(string label, string url) { Label = label; Url = url; }
        public string Label { get; set; }
        public string Url { get; set; }
    }

}

