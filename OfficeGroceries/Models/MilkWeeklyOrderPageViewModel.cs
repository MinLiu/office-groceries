﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class MilkWeeklyOrderPageViewModel
    {
        public OrderMode OrderMode { get; set; }
        public bool HasMilkSupplier { get; set; }
        public bool LiveOnMilk { get; set; }
        public bool HasRegularMilkOrder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<MilkOrderViewModel> MilkOrders { get; set; }
        public List<string> Weekdates { get; set; }
        public List<bool> Enables { get; set; }
        private List<bool> _deliveryDays { get; set; }
        private List<DateTime> _holidays { get; set; }
        private List<DateTime> _openDays { get; set; }
        public decimal AdditionalCharge { get; set; }
        public string AdditionalChargeName { get; set; }

        public MilkWeeklyOrderPageViewModel(OrderMode orderMode, string weekStart, Guid? companyID)
        {
            OrderMode = orderMode;
            HasMilkSupplier = false;
            LiveOnMilk = false;
            StartDate = DateTime.MaxValue.AddDays(-1);
            EndDate = DateTime.MaxValue;
            MilkOrders = new List<MilkOrderViewModel>();
            Weekdates = new List<string>();
            Enables = new List<bool>();
                        
            LoadDeliveryDays(companyID);

            if (orderMode == OrderMode.OneOff)
            {
                DateTime dateWeekStart = DateTime.Parse(weekStart);
                var dateMon = dateWeekStart.StartOfWeek(DayOfWeek.Monday);
                var dateTue = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(1);
                var dateWed = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(2);
                var dateThu = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(3);
                var dateFri = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(4);
                var dateSat = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(5);

                LoadSpecialDays(dateMon, companyID);

                Weekdates.AddRange(new List<string>()
                {
                    dateMon.ToString("dd/MM"),
                    dateTue.ToString("dd/MM"),
                    dateWed.ToString("dd/MM"),
                    dateThu.ToString("dd/MM"),
                    dateFri.ToString("dd/MM"),
                    dateSat.ToString("dd/MM"),
                });

                var dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date;
                Enables.AddRange(new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon) || IsOpenDay(dateMon)) && (IsOpenDay(dateMon) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue) || IsOpenDay(dateTue)) && (IsOpenDay(dateTue) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed) || IsOpenDay(dateWed)) && (IsOpenDay(dateWed) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu) || IsOpenDay(dateThu)) && (IsOpenDay(dateThu) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri) || IsOpenDay(dateFri)) && (IsOpenDay(dateFri) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat) || IsOpenDay(dateSat)) && (IsOpenDay(dateSat) || _deliveryDays[(int)OgDayOfWeek.Saturday]),
                });
            }
            else
            {
                Weekdates.AddRange(new List<string>()
                {
                    "","","","","","",
                });
                Enables.AddRange(new List<bool>()
                {
                    true, true, true, true, true, true,
                });
            }
        }
        public void Set(Company company)
        {
            HasMilkSupplier = company.Method_HasSupplier(SupplierType.Milk);
            LiveOnMilk = company.Method_IsLive(SupplierType.Milk);
            var supplier = new CompanySupplierRepository().Read()
                                                          .Where(cs => cs.CompanyID == company.ID)
                                                          .Where(cs => cs.Supplier.SupplierType == SupplierType.Milk)
                                                          .Where(cs => cs.Supplier.Deleted == false)
                                                          .Where(cs => cs.StartDate.HasValue)
                                                          .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                                                          .OrderBy(cs => cs.StartDate.Value)
                                                          .FirstOrDefault();
            StartDate = supplier != null ? supplier.StartDate.Value : DateTime.MaxValue.AddDays(-1);
            EndDate = supplier != null ? (supplier.EndDate.HasValue ? supplier.EndDate.Value : DateTime.MaxValue) : DateTime.MaxValue;
            HasRegularMilkOrder = new MilkOrderHeaderRepository().Read()
                                                                   .Where(x => x.OrderMode == OrderMode.Regular)
                                                                   .Where(x => x.CompanyID == company.ID)
                                                                   .Count() > 0;

            var deliveryCharge = DeliveryChargeHelper.GetMilkDeliveryCharge(company);
            AdditionalCharge = deliveryCharge?.Charge ?? 0;
            AdditionalChargeName = deliveryCharge?.Name;
        }
        public void Set(string prospectToken)
        {
            HasMilkSupplier = true;
            LiveOnMilk = true;
            StartDate = new DateTime(1900, 1, 1);
            EndDate = new DateTime(2100, 1, 1);
            HasRegularMilkOrder = true;
            _deliveryDays = new List<bool> { true, true, true, true, true, true };
        }

        private void LoadSpecialDays(DateTime dateFrom, Guid? companyID)
        {
            Guid? supplierId = null;

            if (companyID != null)
            {
                supplierId = SupplierHelper.GetSupplier(companyID.Value, SupplierType.Milk, dateFrom)?.ID;
            }
            
            var dateTo = dateFrom.AddDays(6);
            _holidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(supplierId)
               .Where(x => dateFrom <= x.Date)
               .Where(x => dateTo >= x.Date)
               .Select(x => x.Date)
               .ToList();
            
            _openDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(supplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
        }
        
        private void LoadDeliveryDays(Guid? companyId)
        {
            if (companyId == null)
            {
                _deliveryDays = new List<bool>
                {
                    true, true, true, true, true, true
                };
                return;
            }
            
            using (var db = new SnapDbContext())
            {
                var company = db.Companies.Find(companyId.Value);
            
                _deliveryDays = new List<bool>
                {
                    company.MilkDeliveryMonday,
                    company.MilkDeliveryTuesday,
                    company.MilkDeliveryWednesday,
                    company.MilkDeliveryThursday,
                    company.MilkDeliveryFriday,
                    company.MilkDeliverySaturday
                };
            }
        }

        private bool NotHoliday(DateTime date)
        {
            return !_holidays.Contains(date.Date);
        }

        private bool IsOpenDay(DateTime date)
        {
            return _openDays.Contains(date.Date);
        }
        
        public bool IsAvailableDay(OgDayOfWeek dayOfWeek)
        {
            if (OrderMode == OrderMode.Draft || OrderMode == OrderMode.Regular)
            {
                return _deliveryDays[(int)dayOfWeek];
            }

            if (OrderMode == OrderMode.OneOff)
            {
                return Enables[(int)dayOfWeek];
            }
            
            throw new NotImplementedException();
        }
    }

}

