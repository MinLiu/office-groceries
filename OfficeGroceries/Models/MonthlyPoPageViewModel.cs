﻿using System;
using Fruitful.Utils.Encrypts;

namespace NuIngredient.Models
{
    public class MonthlyPoPageViewModel
    {
        public string InvoiceNumber { get; set; }
        public decimal Amount { get; set; }
        public DateTime YearMonth { get; set; }
        public string CompanyName{ get; set; }
        public Guid CompanyID { get; set; }
        public string PoNumber { get; set; }
        public string Hash { get; set; }

        private readonly string _salt = "c86c21cbss";
        
        public bool IsValid()
        {
            var plain = $"{InvoiceNumber}{Amount:0.00}{YearMonth:yyyy-MM}{CompanyID}{_salt}";
            var encrypt = Sha1.Hash(plain);
            // return true;
            return string.Equals(Hash,
                encrypt,
                StringComparison.InvariantCultureIgnoreCase);
        }
        
        public void SetHashCode()
        {
            var plain = $"{InvoiceNumber}{Amount:0.00}{YearMonth:yyyy-MM}{CompanyID}{_salt}";
            var encrypt = Sha1.Hash(plain);
            Hash = encrypt;
        }
    }
}