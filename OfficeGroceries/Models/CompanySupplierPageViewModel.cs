﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class CompanySupplierPageViewModel
    {
        public CompanySupplierViewModel CompanySupplier { get; set; }
        public CompanyViewModel CompanyViewModel { get; set; }
    }
}