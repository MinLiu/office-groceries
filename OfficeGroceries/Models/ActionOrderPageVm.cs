﻿namespace NuIngredient.Models
{
    public class ActionOrderPageVm
    {
        public ProformaOrderViewModel Order { get; set; }
        public ProformaApproverViewModel Approver { get; set; }
        public bool Accept { get; set; }
    }
}