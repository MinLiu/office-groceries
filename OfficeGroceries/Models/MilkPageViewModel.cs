﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class MilkPageViewModel
    {
        public List<DateTime> OrderWeekDates { get; set; }
        public bool AskingForMilkSupplier { get; set; }
        public bool HasMilkSupplier { get; set; }
        public bool LiveOnMilk { get; set; }
        public bool HasRegularMilkOrder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal MinimumValue { get; set; }
        public List<MilkProductCategory> Categories { get; set; }
        public AisleViewModel Aisle { get; set; }
        public string CompanyName { get; set; }
        public List<BreadCrumbViewModel> BreadCrumbs { get; set; }
        public MilkPageViewModel(string aisle = null)
        {
            OrderWeekDates = new List<DateTime>();
            AskingForMilkSupplier = false;
            HasMilkSupplier = false;
            LiveOnMilk = false;
            StartDate = DateTime.MaxValue.AddDays(-1);
            EndDate = DateTime.MaxValue;
            MinimumValue = new SnapDbContext().Suppliers.Where(x => x.SupplierType == SupplierType.Milk)
                                                        .Where(x => x.Deleted == false)
                                                        .OrderByDescending(x => x.MinimumValue)
                                                        .FirstOrDefault()
                                                        .MinimumValue;
            Aisle = !string.IsNullOrWhiteSpace(aisle)
                ? new AisleMapper().MapToViewModel(new AisleRepository().Read().Where(x => x.InternalName == aisle).FirstOrDefault())
                : new AisleMapper().MapToViewModel(new AisleRepository().Read().Where(x => x.Name == "Milk").FirstOrDefault());
            Categories = new MilkProductCategoryRepository().Read().Where(x => x.Deleted == false && x.AisleID.ToString() == Aisle.ID && x.ParentID == null).OrderBy(x => x.SortPos).ThenBy(x => x.Name).ToList();
            _SetBreadCrumb();
        }
        public void Set(Company company)
        {
            AskingForMilkSupplier = company.AskingForMilkSupplier != null;
            HasMilkSupplier = company.Method_HasSupplier(SupplierType.Milk);
            LiveOnMilk = company.Method_IsLive(SupplierType.Milk);
            var supplier = new CompanySupplierRepository().Read()
                                                          .Where(cs => cs.CompanyID == company.ID)
                                                          .Where(cs => cs.Supplier.SupplierType == SupplierType.Milk)
                                                          .Where(cs => cs.Supplier.Deleted == false)
                                                          .Where(cs => cs.StartDate.HasValue)
                                                          .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                                                          .OrderBy(cs => cs.StartDate.Value)
                                                          .FirstOrDefault();
            StartDate = supplier != null ? supplier.StartDate.Value : DateTime.MaxValue.AddDays(-1);
            EndDate = supplier != null ? (supplier.EndDate.HasValue ? supplier.EndDate.Value : DateTime.MaxValue) : DateTime.MaxValue;
            HasRegularMilkOrder = new MilkOrderHeaderRepository().Read()
                                                                   .Where(x => x.OrderMode == OrderMode.Regular)
                                                                   .Where(x => x.CompanyID == company.ID)
                                                                   .Count() > 0;
            CompanyName = company.Name;
        }

        private void _SetBreadCrumb()
        {
            BreadCrumbs = new List<BreadCrumbViewModel>();

            BreadCrumbs.Add(new BreadCrumbViewModel("Home", "/"));
            BreadCrumbs.Add(new BreadCrumbViewModel(Aisle.Label));
        }
    }

}

