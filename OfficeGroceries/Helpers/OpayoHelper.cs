﻿using System;
using System.Collections.Generic;
using Fruitful.Utils.Encrypts;
using NuIngredient.Models;

namespace NuIngredient.Helpers
{
    //https://developer-eu.elavon.com/docs/opayo-forms/api-reference/crypt-field
    public class OpayoHelper
    {
        private const string DefaultCurrency = "GBP";
        private const string SuccessUrl = "https://www.office-groceries.com/paynow/paymentsuccess";
        private const string FailureUrl = "https://www.office-groceries.com/paynow/paymentfailure";
        // private const string IntegrationPassword = "2a7473e251d2b839"; Test
        private const string IntegrationPassword = "5ebccfb407d996fd"; // Live
        private const string DefaultCountry = "GB";
        private const string COFUsage = "FIRST";
        private const string InitiatedType = "CIT";
        private const string VendorEmail = "accounts@office-groceries.com";

        public static string Decrypt(string hexString)
        {
            var key = Base64Encode(IntegrationPassword);
            var base64String = HexString2B64String(hexString);
            return AES.Decrypt(base64String, key, key);
        }
        
        public static string RenderCryptField(OpayoPaymentViewModel item)
        {
            var plainText = ConstructField(item);
            var key = Base64Encode(IntegrationPassword);
            var base64Crypt = AES.Encrypt(plainText, key, key);
            var bytes = Convert.FromBase64String(base64Crypt);
            var hex = BitConverter.ToString(bytes).Replace("-", "");
            return $"@{hex}";
        }
        
        private static string ConstructField(OpayoPaymentViewModel item)
        {
            var paras = new List<string>();
            paras.Add($"VendorTxCode={item.InvoiceNumber}");
            paras.Add($"Amount={item.Amount:0,0.00}");
            paras.Add($"Currency={DefaultCurrency}");
            paras.Add($"Description={item.InvoiceNumber}: Office-Groceries Payments For {item.Month}");
            paras.Add($"SuccessURL={SuccessUrl}");
            paras.Add($"FailureURL={FailureUrl}");
            // paras.Add("CustomerName");
            paras.Add($"CustomerEMail={item.Email}");
            paras.Add($"VendorEMail={VendorEmail}");
            paras.Add($"SendEMail=1");
            // paras.Add("EmailMessage");
            paras.Add($"BillingSurname={item.BillingSurname}");
            paras.Add($"BillingFirstnames={item.BillingFirstnames}");
            paras.Add($"BillingAddress1={item.BillingAddress1}");
            paras.Add($"BillingCity={item.BillingCity}");
            paras.Add($"BillingPostCode={item.BillingPostCode}");
            paras.Add($"BillingCountry={DefaultCountry}");
            paras.Add($"DeliverySurname={item.DeliverySurname}");
            paras.Add($"DeliveryFirstnames={item.DeliveryFirstnames}");
            paras.Add($"DeliveryAddress1={item.DeliveryAddress1}");
            paras.Add($"DeliveryCity={item.DeliveryCity}");
            paras.Add($"DeliveryPostCode={item.DeliveryPostCode}");
            paras.Add($"DeliveryCountry={DefaultCountry}");
            paras.Add($"ReferrerID={item.InvoiceNumber}"); // Optional
            paras.Add($"InitiatedType={InitiatedType}");
            paras.Add($"COFUsage={COFUsage}");

            return string.Join("&", paras);
        }

        private static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static string HexString2B64String(string input)
        {
            return System.Convert.ToBase64String(HexStringToHex(input));
        }
        
        private static byte[] HexStringToHex(string hexStr)
        {
            var resultantArray = new byte[hexStr.Length / 2];
            for (var i = 0; i < resultantArray.Length; i++)
            {
                resultantArray[i] = System.Convert.ToByte(hexStr.Substring(i * 2, 2), 16);
            }
            return resultantArray;
        }
    }

    // public class OpayoCryptFieldBuilder
    // {
    //     private string VendorTxCode { get; set; }
    //     private decimal Amount { get; set; }
    //     private string Currency { get; set; }
    //     private string Description { get; set; }
    //     private string SuccessURL { get; set; }
    //     private string FailureURL { get; set; }
    //     private string CustomerName { get; set; }
    //     private string CustomerEMail { get; set; }
    //     private string VendorEMail{ get; set; }
    //     private string SendEMail{ get; set; }
    //     private string EmailMessage{ get; set; }
    //     private string BillingSurname{ get; set; }
    //     private string BillingFirstnames { get; set; }
    //     private string BillingAddress1 { get; set; }
    //     private string BillingAddress2 { get; set; }
    //     private string BillingAddress3 { get; set; }
    //     private string BillingCity { get; set; }
    //     private string BillingPostCode { get; set; }
    //     private string BillingCountry { get; set; }
    //     private string BillingState { get; set; }
    //     private string BillingPhone { get; set; }
    //     private string DeliverySurname { get; set; }
    //     private string DeliveryFirstnames { get; set; }
    //     private string DeliveryAddress1 { get; set; }
    //     private string DeliveryAddress2 { get; set; }
    //     private string DeliveryAddress3 { get; set; }
    //     private string DeliveryCity { get; set; }
    //     private string DeliveryPostCode { get; set; }
    //     private string DeliveryCountry { get; set; }
    //     private string DeliveryState { get; set; }
    //     private string DeliveryPhone { get; set; }
    //     private string VendorData { get; set; }
    //     private string ReferrerID { get; set; }
    //     private string Language { get; set; }
    //     private string Website { get; set; }
    //     private string COFUsage { get; set; }
    //     private string InitiatedType { get; set; }
    // }
}