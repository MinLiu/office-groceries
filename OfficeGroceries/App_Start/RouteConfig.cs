﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NuIngredient
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "About",
                url: "About",
                defaults: new { controller = "Home", action = "About" }
            );

            routes.MapRoute(
                name: "Process",
                url: "Process",
                defaults: new { controller = "Home", action = "Process" }
            );

            routes.MapRoute(
                name: "FAQs",
                url: "FAQs",
                defaults: new { controller = "Home", action = "FAQs" }
            );

            routes.MapRoute(
                name: "Testimonials",
                url: "Testimonials",
                defaults: new { controller = "Home", action = "Testimonials" }
            );

            routes.MapRoute(
                name: "TermsConditions",
                url: "TermsConditions",
                defaults: new { controller = "Home", action = "TermsConditions" }
            );

            routes.MapRoute(
                name: "PrivacyPolicy",
                url: "PrivacyPolicy",
                defaults: new { controller = "Home", action = "PrivacyPolicy" }
            );

            routes.MapRoute(
                name: "Sitemap",
                url: "Sitemap",
                defaults: new { controller = "Home", action = "Sitemap" }
            );

            routes.MapRoute(
                name: "Contact",
                url: "Contact",
                defaults: new { controller = "Home", action = "Contact" }
            );

            routes.MapRoute(
                name: "SayCoffee",
                url: "say-coffee",
                defaults: new { controller = "Home", action = "SayCoffee" }
            );
            
            routes.MapRoute(
                name: "Ernie",
                url: "ernie",
                defaults: new { controller = "Home", action = "Ernie" }
            );

            routes.MapRoute(
                name: "DeliveryAreasIndex",
                url: "delivery-areas",
                defaults: new { controller = "DeliveryAreas", action = "Index" }
            );
                
            routes.MapRoute(
                name: "DeliveryAreas",
                url: "delivery-areas/{locationName}",
                defaults: new { controller = "DeliveryAreas", action = "Index", loocationName = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "LocationRedirect",
                url: "location/{locationName}",
                defaults: new { controller = "DeliveryAreas", action = "Redirection", loocationName = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "Aisle",
                url: "Aisle/{id}/{category}",
                defaults: new { controller = "Products", action = "Aisle", category = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Milk",
                url: "office-milk-deliveries/{aisle}",
                defaults: new { controller = "Milk", action = "Index", aisle = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Fruits",
                url: "office-fruit-basket-deliveries",
                defaults: new { controller = "Fruits", action = "Index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
        }
    }
}
