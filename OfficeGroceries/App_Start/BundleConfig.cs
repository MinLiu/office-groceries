﻿using System.Web;
using System.Web.Optimization;

namespace NuIngredient
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            BundleTable.EnableOptimizations = true;

            //Standard Bootstrap and Site CSS & JS
            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js",
            //          "~/Scripts/jquery.validate.min.js",
            //          "~/Scripts/jquery.validate.unobtrusive.min.js",
            //          "~/Scripts/jquery.unobtrusive-ajax.min.js",
            //          "~/Scripts/Fruitful.js",
            //          "~/Scripts/og-site.js",
            //          "~/Scripts/Views/ShoppingCart.js",
            //          "~/Scripts/Views/MilkOrder.js",
            //          "~/Scripts/Views/FruitOrder.js",
            //          "~/Content/fancybox/jquery.fancybox.pack.js",
            //          "~/Content/fancybox/helpers/jquery.fancybox-buttons.js",
            //          "~/Content/fancybox/helpers/jquery.fancybox-media.js"
            //          ));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/font-awesome.min.css",
            //          "~/Content/Fruitful.css",
            //          "~/Content/Site.css"
            //          ));

            //bundles.Add(new StyleBundle("~/CMS/css").Include(
            //          "~/Views/CMS/Content/Css/bootstrap.css",
            //          "~/Views/CMS/Content/Css/font-awesome.min.css",
            //          "~/Views/CMS/Content/Css//Fruitful.css",
            //          "~/Views/CMS/Content/Css//Site.css"
            //          ));

            //bundles.Add(new StyleBundle("~/CMS/bootstrap").Include( "~/Views/CMS/Content/Css/bootstrap.css" ));
            //bundles.Add(new StyleBundle("~/CMS/font-awesome").Include("~/Views/CMS/Content/Css/font-awesome.min.css"));
            //bundles.Add(new StyleBundle("~/CMS/site").Include("~/Views/CMS/Content/Css/Site.css"));
            //bundles.Add(new StyleBundle("~/CMS/fruitful").Include("~/Views/CMS/Content/Css/Fruitful.css"));
            //bundles.Add(new StyleBundle("~/CMS/styles-og").Include("~/Views/CMS/Content/Css/styles-og.css"));


            //bundles.Add(new StyleBundle("~/CMS/fancybox").Include("~/Content/fancybox/jquery.fancybox.css" ));
            //bundles.Add(new StyleBundle("~/CMS/fancybox-buttons").Include("~/Content/fancybox/helpers/jquery.fancybox-buttons.css"));

            //Kendo/Telerik CSS & JS
            //bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
            //        //"~/Scripts/kendo/2017.2.621/jquery.min.js",
            //        "~/Scripts/kendo.modernizr.custom.js",
            //        "~/Scripts/kendo/2017.2.621/jszip.min.js",
            //        "~/Scripts/kendo/2017.2.621/kendo.all.min.js",
            //        "~/Scripts/kendo/2017.2.621/kendo.aspnetmvc.min.js",
            //        "~/Scripts/kendo/2017.2.621/kendo.timezones.min.js",
            //        "~/Scripts/kendo.modernizr.custom.js",
            //        "~/Scripts/kendo/cultures/kendo.culture.en-GB.min.js"
            //));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/kendo/2017.2.621/New folder/jquery.min.js",
                "~/Scripts/bootstrap.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/kendo-for-prospect").Include(
                "~/Scripts/kendo/2017.2.621/New folder/kendo.core.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.data.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.culture.en-GB.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.listview.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/2017.2.621/New folder/kendo.core.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.data.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.columnsorter.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.userevents.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.draganddrop.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.sortable.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.selectable.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.calendar.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.popup.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.list.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.pager.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/jszip.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.modernizr.custom.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.culture.en-GB.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.grid.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.listview.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.dropdownlist.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.datepicker.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.timepicker.min.js",
                "~/Scripts/kendo/2017.2.621/New folder/kendo.datetimepicker.min.js"
            ));

            bundles.Add(new StyleBundle("~/kendo/css").Include(
                    "~/Content/kendo/2017.2.621/kendo.common-fruitful.css",
                    "~/Content/kendo/2017.2.621/kendo.mobile.all.min.css",
                    "~/Content/kendo/2017.2.621/kendo.dataviz.min.css",
                    "~/Content/kendo/2017.2.621/kendo.fruitful.css", //Fruitful Kendo Skin here
                    "~/Content/kendo/2017.2.621/kendo.dataviz.default.min.css"
            ));

            bundles.Add(new StyleBundle("~/site/css").Include(
                "~/css/main.css",
                "~/css/timeline-popover.css",
                "~/css/site.css"
            ));

            bundles.Add(new ScriptBundle("~/site/js").Include(
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/popper.min.js",
                "~/Scripts/Views/MilkOrder.js",
                "~/Scripts/Views/FruitOrder.js",
                "~/Scripts/Views/ShoppingCart.js",
                "~/Scripts/Fruitful.js",
                "~/Scripts/og-site.js"
            ));
            
            bundles.Add(new StyleBundle("~/site/style").Include(
                "~/css/style.css",
                "~/css/responsive.css"
            ));
        }
    }
}
