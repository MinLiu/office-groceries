﻿using DocumentFormat.OpenXml.Packaging;
using NuIngredient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Used in the generation of Word Document images
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.Drawing;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;


public static class Miscellaneous
{

    public static void CheckLoginMisuse(User User, ControllerContext Context, HttpRequestBase Request, HttpResponseBase Response)
    {
        if (User.Token != (Request.Cookies["GG"] ?? new HttpCookie("GG")).Value) {
            Context.HttpContext.GetOwinContext().Authentication.SignOut();
            //Controller.RedirectToAction("Index", "Home");
            Response.Redirect("/");
        } 
    }


    public static string Truncate(string source, int length)
    {
        if (source.Length > length)
        {
            source = source.Substring(0, length);
        }
        return source;
    }



    public static Drawing CreateImageElement(string filename, WordprocessingDocument wordDoc, HttpServerUtilityBase server, double maxWidthCm = 4.0, bool autoResize = false, bool matchHeight = false)
    {
        //This function creates then returns a Word Document image. 
        MainDocumentPart mainPart = wordDoc.MainDocumentPart;
        ImagePart imagePart = mainPart.AddImagePart(Miscellaneous.GetImagePartType(filename));
        Bitmap image = null;
        var imgName = filename.Split('\\').Last();

        using (FileStream stream = new FileStream(server.MapPath(filename), FileMode.Open, FileAccess.Read, FileShare.Read))
        {
            imagePart.FeedData(stream);
            image = new Bitmap(imagePart.GetStream());
        }

        long widthPx = (long)image.Width;
        long heightPx = (long)image.Height;
        const long emusPerInch = 914400L;
        const long emusPerCm = 360000L;
        const double dpi = 96.0;
        var widthEmus = (long)(widthPx / dpi * emusPerInch);
        var heightEmus = (long)(heightPx / dpi * emusPerInch);
        var maxWidthEmus = (long)(maxWidthCm * emusPerCm);

        //Scale the image to make sure it does not exceed max width
        if (widthEmus > maxWidthEmus || autoResize)
        {
            var ratio = (heightEmus * 1.0m) / widthEmus;
            widthEmus = maxWidthEmus;
            heightEmus = (long)(widthEmus * ratio);
        }

        if ((heightEmus > maxWidthEmus || autoResize) && heightEmus > widthEmus && matchHeight)
        {
            var ratio = (widthEmus * 1.0m) / heightEmus;
            heightEmus = maxWidthEmus;
            widthEmus = (long)(heightEmus * ratio);
        }

        string relationshipId = mainPart.GetIdOfPart(imagePart);

        // Define the reference of the image.
        // Code borrowed from https://msdn.microsoft.com/en-us/library/office/bb497430.aspx. How no idea how it works
        Drawing element =
             new Drawing(
                 new DW.Inline(

                     new DW.Extent() { Cx = widthEmus, Cy = heightEmus },
                    //new DW.EffectExtent() { LeftEdge = 914400L, TopEdge = 914400L, RightEdge = 914400L, BottomEdge = 914400L },
                     new DW.DocProperties() { Id = (UInt32Value)1U, Name = imgName },
                     new DW.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoChangeAspect = true }),
                     new A.Graphic(
                         new A.GraphicData(
                             new PIC.Picture(
                                 new PIC.NonVisualPictureProperties(
                                     new PIC.NonVisualDrawingProperties() { Id = (UInt32Value)0U, Name = imgName },
                                     new PIC.NonVisualPictureDrawingProperties()),
                                 new PIC.BlipFill(
                                     new A.Blip(new A.BlipExtensionList(new A.BlipExtension() { Uri = Guid.NewGuid().ToString("B") }))
                                     {
                                         Embed = relationshipId,
                                         CompressionState = A.BlipCompressionValues.Print
                                     },
                                     new A.Stretch(new A.FillRectangle())),
                                 new PIC.ShapeProperties(
                                     new A.Transform2D(new A.Offset() { X = 914400L, Y = 914400L }, new A.Extents() { Cx = widthEmus, Cy = heightEmus }),
                                     new A.PresetGeometry(new A.AdjustValueList()) { Preset = A.ShapeTypeValues.Rectangle }))
                         ) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                 )
                 {
                     DistanceFromTop = (UInt32Value)0U,
                     DistanceFromBottom = (UInt32Value)0U,
                     DistanceFromLeft = (UInt32Value)0U,
                     DistanceFromRight = (UInt32Value)0U,
                     EditId = new Random().Next().ToString("X")
                 });

        //element.
        return element;

    }


    public static ImagePartType GetImagePartType(string filename)
    {
        var fileExtension = filename.Split('.').DefaultIfEmpty("").Last().ToLower();

        switch (fileExtension) {
            case "bmp": return ImagePartType.Bmp;
            case "emf": return ImagePartType.Emf;
            case "gif": return ImagePartType.Gif;
            case "icon": return ImagePartType.Icon;
            case "jpg": return ImagePartType.Jpeg;
            case "jpeg": return ImagePartType.Jpeg;
            case "pcx": return ImagePartType.Pcx;
            case "png": return ImagePartType.Png;
            case "tiff": return ImagePartType.Tiff;
            case "wmf": return ImagePartType.Wmf;
            default: return ImagePartType.Bmp;
        }
    }

}



