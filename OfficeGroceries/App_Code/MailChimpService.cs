﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MailChimp.Net;
using MailChimp.Net.Interfaces;
using MailChimp.Net.Models;

namespace NuIngredient.Models
{
    public static class MailChimpService
    {
        private static string ApiKey = ConfigurationManager.AppSettings["MailChimpApiKey"];
        private static string ListID = ConfigurationManager.AppSettings["MailChimpListID"];
        public static async Task<bool> OptIn(string email, string firstName, string lastName)
        {
            try
            {
                IMailChimpManager mailChimpManager = new MailChimpManager(ApiKey);
                var member = new Member
                {
                    EmailAddress = email,
                    StatusIfNew = Status.Pending
                };
                member.MergeFields.Add("FNAME", firstName);
                member.MergeFields.Add("LNAME", lastName);
                member = await mailChimpManager.Members.AddOrUpdateAsync(ListID, member);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<bool> OptOut(string email)
        {
            try
            {
                IMailChimpManager mailChimpManager = new MailChimpManager(ApiKey);
                var member = new Member
                {
                    EmailAddress = email,
                    Status = Status.Unsubscribed
                };
                member = await mailChimpManager.Members.AddOrUpdateAsync(ListID, member);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<bool> Delete(string email)
        {
            try
            {
                IMailChimpManager mailChimpManager = new MailChimpManager(ApiKey);
                await mailChimpManager.Members.DeleteAsync(ListID, email);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
