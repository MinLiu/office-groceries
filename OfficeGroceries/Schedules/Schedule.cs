﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using NuIngredient.Models;

namespace NuIngredient.Schedule
{
    public class Schedule
    {
        private static IScheduler sched;

        public static void ScheduleStart()
        {
            // Construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // Get a scheduler
            sched = schedFact.GetScheduler();
            sched.Start();

            var schedulesToRun = new List<ScheduleEntity>
            {
                CopyOrders(),
                SendWeeklyOrderToAPFL(),
                CreateSiteMap(),
                SendWeeklyOrders(),
                IntermittentDelivery(),
                UpdateNextDeliveryDate(),
                SendPromote(),
                DailyOrderReminder()
            };

            foreach (var scheduleEntity in schedulesToRun)
            {
                sched.ScheduleJob(scheduleEntity.Job, scheduleEntity.Trigger);
            }
        }

        private static ScheduleEntity CopyOrders()
        {
            return CreateScheduleEntity<ScheduleCopyingOrders>(
                "ScheduleInvoicing",
                string.Format("0 0 {0} * * ? *", CutOffTime.TimeOfDay));
        }

        private static ScheduleEntity SendWeeklyOrderToAPFL()
        {
            return CreateScheduleEntity<ScheduleSendWeeklyOrdersForAPFL>(
                "SendWeeklyOrdersForAPFL",
                "0 0 12 ? * Thu *"); // fire at 12 pm every thursday
        }

        private static ScheduleEntity CreateSiteMap()
        {
            return CreateScheduleEntity<ScheduleCreateSitemap>(
                "CreateSitemap",
                "0 0 2 * * ? *"); // fire at 2am everyday
        }

        private static ScheduleEntity SendWeeklyOrders()
        {
            return CreateScheduleEntity<ScheduleSendWeeklyOrders>(
                "SendWeeklyOrders",
                "0 0,30 11,12,13,14,15 ? * Thu,Fri *"); // fire at 11, 12 am, 14 & 15 pm every Thursday & Friday
        }

        private static ScheduleEntity IntermittentDelivery()
        {
            return CreateScheduleEntity<ScheduleIntermittentDelivery>(
                "IntermittentDelivery",
                "0 0 7 ? * Mon,Tue,Wed,Thu,Fri *"); // fire at 07:00am every Weekday
        }

        private static ScheduleEntity UpdateNextDeliveryDate()
        {
            return CreateScheduleEntity<ScheduleUpdateNextDeliveryDate>(
                "UpdateNextDeliveryDate",
                "0 0 2 ? * Mon *"); // fire at 02:00am every Monday
        }

        private static ScheduleEntity SendPromote()
        {
            return CreateScheduleEntity<ScheduleSendPromote>(
                "SendPromote",
                "0 0 10 ? * Wed *"); // fire at 10:00am every Wed
        }
        
        private static ScheduleEntity DailyOrderReminder()
        {
            return CreateScheduleEntity<ScheduleDailyOrderReminder>(
                "DailyReminder",
                "0 15 14 ? * Mon,Tue,Wed,Thu,Fri *"); // fire at 14:15pm every Weekday
        }
        
        private static ScheduleEntity CreateScheduleEntity<T>(string scheduleName, string cronSchedule)
            where T: IJob
        {
            var job = JobBuilder.Create<T>()
                .WithIdentity($"Job_{scheduleName}")
                .Build();
            
            var trigger = TriggerBuilder.Create()
                .WithIdentity($"Trigger_{scheduleName}")
                .StartNow()
                .WithCronSchedule(cronSchedule)
                .Build();

            return new ScheduleEntity()
            {
                Job = job,
                Trigger = trigger
            };
        }
        
        private class ScheduleEntity
        {
            public IJobDetail Job { get; set; }
            public ITrigger Trigger { get; set; }
        }

    }
}