﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NuIngredient.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using System.Net.Mail;
using Fruitful.Email;
using System.Data.Entity;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleSendPromote : IJob
    {

        public void Execute(IJobExecutionContext context)
        {
            SendEmailToAdmin("ScheduleSendPromote executed at " + DateTime.Now.ToString("yy/MM/dd"), "Schedule executed at " + DateTime.Now.ToString("yy/MM/dd HH:mm"));

            var prospectRepository = new ProspectRepository();

            var prospectList = prospectRepository.Read()
                .Where(x => x.Deleted == false)
                .OrderBy(x => x.Created)
                .ToList();

            var successCount = 0;
            var errorMessages = new List<string>();
            foreach (var prospect in prospectList)
            {
                try
                {
                    var success = SendPromotionalQuoteMail(prospect);
                    if (success)
                    {
                        prospect.CountPromotionEmail += 1;
                        successCount++;
                    }
                }
                catch(Exception e)
                {
                    var message = $"{prospect.Email}, prospect token: {prospect.ProspectToken},<br /> {e.Message} <br /> {e.StackTrace}";
                    errorMessages.Add(message);
                }
            }
            prospectRepository.Update(prospectList);

            if (errorMessages.Any())
            {
                var subject = "Error Schedule Send Promote";
                var message = $"Successfully sent promote to {successCount} prospects. <br />" +
                              string.Join("<br/>", errorMessages);
                SendEmailToAdmin(subject, message);
            }
        }

        private bool SendPromotionalQuoteMail(Prospect prospect)
        {
            var mailMessage = ProspectQuotePromotionalEmailHelper.GetEmail(prospect, out List<string> categoryList);
            var success = EmailService.SendEmail(mailMessage, false);

            return success;
        }

        private void SendEmailToAdmin(string subject, string message)
        {
            try
            { 
                Emailer emailer = new Emailer(new SnapDbContext());
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("orders@office-groceries.com");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.To.Add("min.liu@zigaflow.com");
                mailMessage.IsBodyHtml = true;
                emailer.SendEmail(mailMessage);
            }
            catch{ }
        }

    }
}