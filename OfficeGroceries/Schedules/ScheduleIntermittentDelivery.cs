﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NuIngredient.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using System.Net.Mail;
using Fruitful.Email;
using System.Data.Entity;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleIntermittentDelivery : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            SendEmailToAdmin("ScheduleIntermittentDelivery executed at " + DateTime.Now.ToString("yy/MM/dd"), "Schedule executed at " + DateTime.Now.ToString("yy/MM/dd HH:mm"));

            var firstDayOfNextWeek = DateTime.Now.AddDays(7).StartOfWeek(DayOfWeek.Monday);

            var repo = new CompanySupplierRepository();
            var companySupplierGroups = repo.Read()
                .Where(x => x.Supplier.SupplierType == SupplierType.Fruits || x.Supplier.SupplierType == SupplierType.Snacks)
                .Where(x => x.StartDate <= firstDayOfNextWeek)
                .Where(x => x.EndDate == null || firstDayOfNextWeek <= x.EndDate)
                .Where(x => x.IsIntermittentDelivery)
                .Where(x => x.Company.DeleteData == false)
                .Include(x => x.Company)
                .Include(x => x.Supplier)
                .GroupBy(x => x.CompanyID)
                .ToList();
            
            foreach (var group in companySupplierGroups)
            {
                try
                {
                    CreateIntermittentOrder(group.ToList(), firstDayOfNextWeek);
                }
                catch(Exception e)
                {
                    var subject = $"Error creating intermittent fruit order for {group.FirstOrDefault()?.Company.Name}";
                    var message = $"Error creating intermittent fruit order for {group.FirstOrDefault()?.Company.Name},<br /> {e.Message} <br /> {e.StackTrace}";
                    SendEmailToAdmin(subject, message);
                }
            }

        }

        private void SendEmailToAdmin(string subject, string message)
        {
            try
            { 
                var setting = new SnapDbContext().EmailSettings.First();
                Emailer emailer = new Emailer(new SnapDbContext());
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(setting.Email);
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.To.Add("min@fruitfulgroup.com");
                mailMessage.IsBodyHtml = true;
                emailer.SendEmail(mailMessage);
            }
            catch{ }
        }

        private void CreateIntermittentOrder(List<CompanySupplier> companySuppliers, DateTime firstDayOfNextWeek)
        {
            // if regular order is zero
            // if (companySupplier.Company.FruitOrders
            //     .Where(x => x.OrderMode == OrderMode.Regular && x.Depreciated == false)
            //     .OrderByDescending(x => x.LatestUpdated).FirstOrDefault()?.TotalNet == 0)
            //     return;

            var deliveryWeekSuppliers = companySuppliers.Where(x => x.NextDeliveryWeek == firstDayOfNextWeek).ToList();
            
            foreach (var companySupplier in deliveryWeekSuppliers)
            {
                DeliveryWeekRoutine(companySupplier, firstDayOfNextWeek);
            }
            
            var nonDeliveryWeekSuppliers = companySuppliers.Where(x => x.NextDeliveryWeek != firstDayOfNextWeek).ToList();

            NonDeliveryWeekRoutine(nonDeliveryWeekSuppliers, firstDayOfNextWeek);
        }

        private void DeliveryWeekRoutine(CompanySupplier companySupplier, DateTime firstDayOfNextWeek)
        {
            if (DateTime.Today.DayOfWeek != DayOfWeek.Monday) return;
            
            var orderHeader = GetFruitOrder(companySupplier.Company, firstDayOfNextWeek);
            var message = IntermittentOrderHelper.GetEmail(companySupplier, firstDayOfNextWeek, orderHeader, companySupplier.Supplier.SupplierType);
            EmailService.SendEmail(message);
        }

        private void NonDeliveryWeekRoutine(List<CompanySupplier> companySuppliers, DateTime firstDayOfNextWeek)
        {
            if (companySuppliers == null || companySuppliers.Count == 0) return;
            
            var company = companySuppliers.First().Company;
            
            var supplierTypes = companySuppliers.Select(x => x.Supplier.SupplierType).ToList();
            var productTypes = supplierTypes
                .Select(x => x == SupplierType.Fruits
                    ? FruitProductType.Fruit
                    : FruitProductType.Snack)
                .ToList();
            
            var orderHeader = GetFruitOrder(company, firstDayOfNextWeek);

            if (orderHeader == null)
                return;

            // if it's one-off order instead of regular, it means it's already modified manually so no need to update again.
            if (orderHeader.OrderMode == OrderMode.OneOff)
                return;
            
            var orderItems = orderHeader.Items.ToList().Select(x => new FruitOrderViewModel
            {
                ProductName = x.ProductName,
                FruitID = x.FruitProductID.ToString(),
                Unit = x.Unit,
                VATRate = x.VATRate,
                Price = x.Price,
                Monday = productTypes.Contains(x.Fruit.FruitProductType) ? 0 : x.Monday,
                Tuesday = productTypes.Contains(x.Fruit.FruitProductType) ? 0 : x.Tuesday,
                Wednesday = productTypes.Contains(x.Fruit.FruitProductType) ? 0 : x.Wednesday,
                Thursday =  productTypes.Contains(x.Fruit.FruitProductType) ? 0 : x.Thursday,
                Friday = productTypes.Contains(x.Fruit.FruitProductType) ? 0 : x.Friday,
                Saturday = productTypes.Contains(x.Fruit.FruitProductType) ? 0 : x.Saturday,
                Sunday = 0, // doesn't matter
                WeeklyVolume = 0, // doesn't matter
                WeeklyTotal = 0, // doesn't matter
                FromDate = null, // doesn't matter
                OrderMode = OrderMode.Draft // doesn't matter
            }).ToList();

            var result = SaveFruitOrderHelper.New()
                .SetCompanyID(company.ID)
                .SaveCustomOrder(OrderMode.OneOff, firstDayOfNextWeek, orderItems);

            orderHeader = result.OrderHeader;

            foreach (var companySupplier in companySuppliers)
            {
                var message = IntermittentOrderHelper.GetEmail(companySupplier, firstDayOfNextWeek, orderHeader, companySupplier.Supplier.SupplierType);
                EmailService.SendEmail(message);
            }
        }
        
        private FruitOrderHeader GetFruitOrder(Company company, DateTime fromDate)
        {
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), new SnapDbContext());
            var orderHeader = service.GetWeeklyOrderAsNoTracking(OrderMode.OneOff, company.ID, fromDate);
            return orderHeader;
        }

        //private bool ActiveOnFruit(Company company)
        //{
        //    // find the companies if it's active next week
        //    return company.IsLiveWeek(SupplierType.Fruits, DateTime.Now.AddDays(7));
        //}

    }
}