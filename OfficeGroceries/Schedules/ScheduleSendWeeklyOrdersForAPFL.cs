﻿using System;
using System.Collections.Generic;
using NuIngredient.Models;
using Quartz;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleSendWeeklyOrdersForAPFL : IJob
    {
        private static readonly List<Guid> ApflIdList = new List<Guid>()
        {
            Guid.Parse("ded91bda-eb24-4eea-8698-5f9d6779c1df"), // APFL UK (Fruit)
            Guid.Parse("ba71f2b5-aa6b-42eb-bd4c-69a3fb8bd621"), // APFL UK (Milk)
            Guid.Parse("b54a4707-cfbb-4702-a4ea-bd9fc339f5b0"), // APFL LONDON (Milk)
            Guid.Parse("33d01e74-50c1-4dd7-8608-e92111a27c65"), // APFL LONDON (Fruit)
        };
        
        public void Execute(IJobExecutionContext context)
        {

            foreach (var supplierId in ApflIdList)
            {
                var helper = new WeeklyOrderReminderHelper(new WeeklyOrderReminderHelper.WeeklyOrderReminderConfig
                {
                    SupplierId = supplierId
                });

                helper.Execute();
            }
        }
    }
}