﻿using NuIngredient.Models;
using Quartz;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleSendWeeklyOrders : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var helper = new WeeklyOrderReminderHelper();
            helper.Execute();
        }
    }
}