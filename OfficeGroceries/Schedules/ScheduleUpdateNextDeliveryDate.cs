﻿using System;
using System.Linq;
using NuIngredient.Models;
using Quartz;
using System.Net.Mail;
using Fruitful.Email;
using System.Data.Entity;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleUpdateNextDeliveryDate : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            SendEmailToAdmin("ScheduleUpdateNextDeliveryDate executed at " + DateTime.Now.ToString("yy/MM/dd"), "Schedule executed at " + DateTime.Now.ToString("yy/MM/dd HH:mm"));

            var firstDayOfWeek = DateTime.Now.StartOfWeek(DayOfWeek.Monday);

            var repo = new CompanySupplierRepository();
            var companySuppliers = repo.Read()
                .Where(x => x.Supplier.SupplierType == SupplierType.Fruits || x.Supplier.SupplierType == SupplierType.Snacks)
                .Where(x => x.StartDate <= firstDayOfWeek)
                .Where(x => x.EndDate == null || firstDayOfWeek <= x.EndDate)
                .Where(x => x.IsIntermittentDelivery)
                .Where(x => x.NextDeliveryWeek == firstDayOfWeek)
                .Where(x => x.Company.DeleteData == false)
                .Include(x => x.Company)
                .Include(x => x.Supplier)
                .ToList();
            
            foreach (var companySupplier in companySuppliers)
            {
                try
                {
                    companySupplier.NextDeliveryWeek =
                        companySupplier.NextDeliveryWeek.Value.AddDays(Convert.ToInt32(7 * companySupplier.DeliveryFrequency));

                }
                catch(Exception e)
                {
                    var subject = $"Error calculating next delivery date for {companySupplier.Company.Name}";
                    var message = $"Error calculating next delivery date  for {companySupplier.Company.Name},<br /> {e.Message} <br /> {e.StackTrace}";
                    SendEmailToAdmin(subject, message);
                }
            }
            
            repo.Update(companySuppliers);
        }

        private void SendEmailToAdmin(string subject, string message)
        {
            try
            { 
                var setting = new SnapDbContext().EmailSettings.First();
                Emailer emailer = new Emailer(new SnapDbContext());
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(setting.Email);
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.To.Add("min@fruitfulgroup.com");
                mailMessage.IsBodyHtml = true;
                emailer.SendEmail(mailMessage);
            }
            catch{ }
        }

    }
}