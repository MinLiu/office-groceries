﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NuIngredient.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using System.Net.Mail;
using Fruitful.Email;
using System.Data.Entity;
using Kendo.Mvc.Extensions;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleCopyingOrders : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                return;

            SendEmailToAdmin("Schedule executed at " + DateTime.Now.ToString("yy/MM/dd"), "Schedule executed at " + DateTime.Now.ToString("yy/MM/dd HH:mm"));

            var companyList = new CompanyRepository().Read().Where(c => c.DeleteData == false).ToList();
            foreach (var company in companyList)
            {
                try
                {
                    GenerateFruitInvoices(company);
                }
                catch(Exception e)
                {
                    var message = $"Error creating fruit order for {company.Name},<br /> {e.Message} <br /> {e.StackTrace}";
                    SendEmailToAdmin($"Error creating fruit order for {company.Name}", message);
                }
                try
                {
                    GenerateMilkInvoices(company);
                }
                catch(Exception e)
                {
                    var message = $"Error creating milk order for {company.Name},<br /> {e.Message} <br /> {e.StackTrace}";
                    SendEmailToAdmin($"Error creating milk order for {company.Name}", message);
                }
            }
            
            SendEmailToAdmin("Finish at " + DateTime.Now.ToString("yy/MM/dd"), "Finish at " + DateTime.Now.ToString("yy/MM/dd HH:mm"));
        }

        private void SendEmailToAdmin(string subject, string message)
        {
            try
            { 
                var setting = new SnapDbContext().EmailSettings.First();
                Emailer emailer = new Emailer(new SnapDbContext());
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(setting.Email);
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.To.Add("min@fruitfulgroup.com");
                mailMessage.IsBodyHtml = true;
                emailer.SendEmail(mailMessage);
            }
            catch{ }
        }

        #region Fruit

        public void GenerateFruitInvoices(Company company)
        {
            var nextDeliveryDate = DateTime.Now.AddHours(CutOffTime.RestTime);
            var nextDeliveryDay = nextDeliveryDate.DayOfWeek;
            var firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            if (nextDeliveryDay == DayOfWeek.Sunday)
            {
                nextDeliveryDate = nextDeliveryDate.AddHours(24);
                nextDeliveryDay = nextDeliveryDate.DayOfWeek;
                firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            }

            if (!ActiveOnFruit(company)) return;

            var orderHeader = GetFruitOrder(company, firstDayOfWeek);

            if(orderHeader == null) return;

            using (var db = new SnapDbContext())
            {
                var companySuppliers = db.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Fruits || x.Supplier.SupplierType == SupplierType.Snacks)
                                                   .Where(x => x.CompanyID == company.ID)
                                                   .Where(x => x.StartDate <= nextDeliveryDate && (x.EndDate == null || x.EndDate >= nextDeliveryDate))
                                                   .ToList();

                if (companySuppliers.Count == 0) return;

                var fruitSupplier = companySuppliers.FirstOrDefault(x => x.Supplier.SupplierType == SupplierType.Fruits);
                var snackSupplier = companySuppliers.FirstOrDefault(x => x.Supplier.SupplierType == SupplierType.Snacks);

                var orderHistoryHeader = db.FruitOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();
                if (orderHistoryHeader == null)
                {
                    orderHistoryHeader = new FruitOrderHeader()
                    {
                        ID = Guid.NewGuid(),
                        CompanyID = company.ID,
                        OrderMode = OrderMode.History,
                        OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                        FromDate = firstDayOfWeek,
                        ToDate = firstDayOfWeek.AddDays(6),
                        AccountNo = fruitSupplier?.AccountNo,
                        SupplierID = fruitSupplier?.SupplierID,
                        SnackAccountNo = snackSupplier?.AccountNo,
                        SnackSupplierID = snackSupplier?.SupplierID,
                        LatestUpdated = DateTime.Now,
                    };
                    db.FruitOrderHeaders.Add(orderHistoryHeader);
                    db.SaveChanges();

                    orderHistoryHeader = db.FruitOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();
                }

                var validTypes = new List<FruitProductType>();
                if (fruitSupplier != null)
                    validTypes.Add(FruitProductType.Fruit);
                if (snackSupplier != null)
                    validTypes.Add(FruitProductType.Snack);

                var validItems = orderHeader.Items
                    .Where(x => validTypes.Contains(x.Fruit.FruitProductType))
                    .ToList();
                
                foreach (var orderItem in validItems)
                {
                    var historyItem = orderHistoryHeader.Items.Where(x => x.FruitProductID == orderItem.FruitProductID).FirstOrDefault();
                    if (historyItem == null)
                    {
                        historyItem = new FruitOrder()
                        {
                            ID = Guid.NewGuid(),
                            OrderHeaderID = orderHistoryHeader.ID,
                            FruitProductID = orderItem.FruitProductID,
                            ProductName = orderItem.ProductName,
                            Price = orderItem.Price,
                            VATRate = orderItem.VATRate,
                            Unit = orderItem.Unit,
                            Monday = 0,
                            Tuesday = 0,
                            Wednesday = 0,
                            Thursday = 0,
                            Friday = 0,
                            Saturday = 0,
                            Sunday = 0,
                            TotalNet = 0,
                            TotalVAT = 0
                        };
                        db.FruitOrders.Add(historyItem);
                        db.SaveChanges();
                    }
                }

                var historyItems = db.FruitOrders.Where(x => x.OrderHeader.CompanyID == company.ID)
                                                 .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                                 .Where(x => x.OrderHeader.FromDate == firstDayOfWeek)
                                                 .ToList();

                foreach (var orderItem in validItems)
                {
                    var historyItem = historyItems.Where(x => x.FruitProductID == orderItem.FruitProductID).FirstOrDefault();
                    switch (nextDeliveryDay)
                    {
                        case DayOfWeek.Sunday: // skip sunday
                        case DayOfWeek.Monday:
                            historyItem.Monday = orderItem.Monday;
                            break;
                        case DayOfWeek.Tuesday:
                            historyItem.Tuesday = orderItem.Tuesday;
                            break;
                        case DayOfWeek.Wednesday:
                            historyItem.Wednesday = orderItem.Wednesday;
                            break;
                        case DayOfWeek.Thursday:
                            historyItem.Thursday = orderItem.Thursday;
                            break;
                        case DayOfWeek.Friday:
                            historyItem.Friday = orderItem.Friday;
                            break;
                        case DayOfWeek.Saturday:
                            historyItem.Saturday = orderItem.Saturday;
                            break;
                        default:
                            break;
                    }
                }

                var orderTotalNet = 0m;
                var orderTotalVAT = 0m;
                foreach (var historyItem in historyItems)
                {
                    //historyItem.TotalNet = (historyItem.Monday + historyItem.Tuesday + historyItem.Wednesday + historyItem.Thursday + historyItem.Friday + historyItem.Saturday) * historyItem.Price;
                    var fruitProduct = db.FruitProducts.Find(historyItem.FruitProductID);//historyItem.Fruit;
                    historyItem.TotalNet = fruitProduct.GetPrice(historyItem.Monday, orderHistoryHeader.CompanyID)
                                           + fruitProduct.GetPrice(historyItem.Tuesday, orderHistoryHeader.CompanyID)
                                           + fruitProduct.GetPrice(historyItem.Wednesday, orderHistoryHeader.CompanyID)
                                           + fruitProduct.GetPrice(historyItem.Thursday, orderHistoryHeader.CompanyID)
                                           + fruitProduct.GetPrice(historyItem.Friday, orderHistoryHeader.CompanyID)
                                           + fruitProduct.GetPrice(historyItem.Saturday, orderHistoryHeader.CompanyID);
                    historyItem.TotalVAT = historyItem.TotalNet * historyItem.VATRate;
                    orderTotalNet += historyItem.TotalNet;
                    orderTotalVAT += historyItem.TotalVAT;
                }

                orderHistoryHeader.TotalNet = orderTotalNet;
                orderHistoryHeader.TotalVAT = orderTotalVAT;
                orderHistoryHeader.LatestUpdated = DateTime.Now;

                db.SaveChanges();
            }
        }

        private FruitOrderHeader GetFruitOrder(Company company, DateTime fromDate)
        {
            //var orderHeaderRepo = new FruitOrderHeaderRepository();
            //var orderHeader = orderHeaderRepo.Read()
            //                                 .Where(x => x.CompanyID == company.ID)
            //                                 .Where(x => x.OrderMode == OrderMode.OneOff)
            //                                 .Where(x => x.FromDate == fromDate)
            //                                 .Include(x => x.Items)
            //                                 .FirstOrDefault();
            //if (orderHeader == null)
            //{
            //    orderHeader = orderHeaderRepo.Read()
            //                                 .Where(x => x.CompanyID == company.ID)
            //                                 .Where(x => x.OrderMode == OrderMode.Regular)
            //                                 .Include(x => x.Items)
            //                                 .FirstOrDefault();
            //}

            //return orderHeader;

            var service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), new SnapDbContext());
            var orderHeader = service.GetWeeklyOrderAsNoTracking(OrderMode.OneOff, company.ID, fromDate);
            return orderHeader;
        }

        private bool ActiveOnFruit(Company company)
        {
            return company.Method_IsLive(SupplierType.Fruits, DateTime.Now.AddDays(1)) ||
                   company.Method_IsLive(SupplierType.Snacks, DateTime.Now.AddDays(1));
        }

        #endregion

        #region Milk
        public void GenerateMilkInvoices(Company company)
        {
            var nextDeliveryDate = DateTime.Now.AddHours(CutOffTime.RestTime);
            var nextDeliveryDay = nextDeliveryDate.DayOfWeek;
            var firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            if (nextDeliveryDay == DayOfWeek.Sunday)
            {
                nextDeliveryDate = nextDeliveryDate.AddHours(24);
                nextDeliveryDay = nextDeliveryDate.DayOfWeek;
                firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            }

            if (!ActiveOnMilk(company)) return;

            var orderHeader = GetMilkOrder(company, firstDayOfWeek);

            if (orderHeader == null) return;

            using (var db = new SnapDbContext())
            {
                var companySupplier = db.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                                   .Where(x => x.CompanyID == company.ID)
                                                   .Where(x => x.StartDate <= nextDeliveryDate && (x.EndDate == null || x.EndDate >= nextDeliveryDate))
                                                   .FirstOrDefault();

                if (companySupplier == null) return;

                var accountNo = companySupplier.AccountNo;
                var supplierID = companySupplier.SupplierID;

                var orderHistoryHeader = db.MilkOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();
                if (orderHistoryHeader == null)
                {
                    var deliveryCharge = DeliveryChargeHelper.GetMilkDeliveryCharge(company);
                    
                    orderHistoryHeader = new MilkOrderHeader()
                    {
                        ID = Guid.NewGuid(),
                        CompanyID = company.ID,
                        OrderMode = OrderMode.History,
                        OrderNumber = String.Format("M{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                        FromDate = firstDayOfWeek,
                        ToDate = firstDayOfWeek.AddDays(6),
                        AccountNo = accountNo,
                        SupplierID = supplierID,
                        LatestUpdated = DateTime.Now,
                        DeliveryChargeCode = deliveryCharge?.Code,
                        DeliveryChargeName = deliveryCharge?.Name,
                        DeliveryUnitCharge = deliveryCharge?.Charge ?? 0,
                        DeliveryChargeVAT = deliveryCharge?.VAT ?? 0
                    };
                    db.MilkOrderHeaders.Add(orderHistoryHeader);
                    db.SaveChanges();

                    orderHistoryHeader = db.MilkOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();

                    if (orderHistoryHeader.DeliveryUnitCharge > 0)
                    {
                        var deliveryChargeItem = new MilkOrder()
                        {
                            ID = Guid.NewGuid(),
                            LineItemType = LineItemType.DeliveryCharge,
                            OrderHeaderID = orderHistoryHeader.ID,
                            ProductName = orderHistoryHeader.DeliveryChargeName,
                            Price = orderHistoryHeader.DeliveryUnitCharge,
                            VATRate = orderHistoryHeader.DeliveryChargeVAT,
                            Unit = "Per Drop",
                            Monday = 0,
                            Tuesday = 0,
                            Wednesday = 0,
                            Thursday = 0,
                            Friday = 0,
                            Saturday = 0,
                            Sunday = 0,
                            TotalNet = 0,
                            TotalVAT = 0
                        };
                        db.MilkOrders.Add(deliveryChargeItem);
                        db.SaveChanges();
                    }
                }

                foreach (var orderItem in orderHeader.Items)
                {
                    var historyItem = orderHistoryHeader.Items.Where(x => x.MilkProductID == orderItem.MilkProductID).FirstOrDefault();
                    if (historyItem == null)
                    {
                        historyItem = new MilkOrder()
                        {
                            ID = Guid.NewGuid(),
                            OrderHeaderID = orderHistoryHeader.ID,
                            MilkProductID = orderItem.MilkProductID,
                            ProductName = orderItem.ProductName,
                            Price = orderItem.Price,
                            VATRate = orderItem.VATRate,
                            Unit = orderItem.Unit,
                            Monday = 0,
                            Tuesday = 0,
                            Wednesday = 0,
                            Thursday = 0,
                            Friday = 0,
                            Saturday = 0,
                            Sunday = 0,
                            TotalNet = 0,
                            TotalVAT = 0
                        };
                        db.MilkOrders.Add(historyItem);
                        db.SaveChanges();
                    }
                }

                var historyItems = db.MilkOrders.Where(x => x.OrderHeader.CompanyID == company.ID)
                                                 .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                                 .Where(x => x.OrderHeader.FromDate == firstDayOfWeek)
                                                 .ToList();

                var deliveryItem =
                    historyItems.FirstOrDefault(x => x.LineItemType == LineItemType.DeliveryCharge);
                
                foreach (var orderItem in orderHeader.Items)
                {
                    var historyItem = historyItems.Where(x => x.MilkProductID == orderItem.MilkProductID).FirstOrDefault();
                    switch (nextDeliveryDay)
                    {
                        case DayOfWeek.Sunday: // skip sunday
                        case DayOfWeek.Monday:
                            historyItem.Monday = orderItem.Monday;
                            if (historyItem.Monday > 0 && deliveryItem != null) deliveryItem.Monday = 1;
                            break;
                        case DayOfWeek.Tuesday:
                            historyItem.Tuesday = orderItem.Tuesday;
                            if (historyItem.Tuesday > 0 && deliveryItem != null) deliveryItem.Tuesday = 1;
                            break;
                        case DayOfWeek.Wednesday:
                            historyItem.Wednesday = orderItem.Wednesday;
                            if (historyItem.Wednesday > 0 && deliveryItem != null) deliveryItem.Wednesday = 1;
                            break;
                        case DayOfWeek.Thursday:
                            historyItem.Thursday = orderItem.Thursday;
                            if (historyItem.Thursday > 0 && deliveryItem != null) deliveryItem.Thursday = 1;
                            break;
                        case DayOfWeek.Friday:
                            historyItem.Friday = orderItem.Friday;
                            if (historyItem.Friday > 0 && deliveryItem != null) deliveryItem.Friday = 1;
                            break;
                        case DayOfWeek.Saturday:
                            historyItem.Saturday = orderItem.Saturday;
                            if (historyItem.Saturday > 0 && deliveryItem != null) deliveryItem.Saturday = 1;
                            break;
                        default:
                            break;
                    }
                    
                    //historyItem.TotalNet = (historyItem.Monday + historyItem.Tuesday + historyItem.Wednesday + historyItem.Thursday + historyItem.Friday + historyItem.Saturday) * historyItem.Price;
                    //historyItem.TotalVAT = historyItem.TotalNet * historyItem.VATRate;

                    //orderTotalNet += historyItem.TotalNet;
                    //orderTotalVAT += historyItem.TotalVAT;
                }

                var orderTotalNet = 0m;
                var orderTotalVAT = 0m;
                foreach (var historyItem in historyItems)
                {
                    historyItem.TotalNet = (historyItem.Monday + historyItem.Tuesday + historyItem.Wednesday + historyItem.Thursday + historyItem.Friday + historyItem.Saturday) * historyItem.Price;
                    historyItem.TotalVAT = historyItem.TotalNet * historyItem.VATRate;
                    orderTotalNet += historyItem.TotalNet;
                    orderTotalVAT += historyItem.TotalVAT;
                }

                orderHistoryHeader.TotalNet = orderTotalNet;
                orderHistoryHeader.TotalVAT = orderTotalVAT;
                orderHistoryHeader.LatestUpdated = DateTime.Now;

                db.SaveChanges();
            }
        }

        private MilkOrderHeader GetMilkOrder(Company company, DateTime fromDate)
        {
            //var orderHeaderRepo = new MilkOrderHeaderRepository();
            //var orderHeader = orderHeaderRepo.Read()
            //                                 .Where(x => x.CompanyID == company.ID)
            //                                 .Where(x => x.OrderMode == OrderMode.OneOff)
            //                                 .Where(x => x.FromDate == fromDate)
            //                                 .Include(x => x.Items)
            //                                 .FirstOrDefault();
            //if (orderHeader == null)
            //{
            //    orderHeader = orderHeaderRepo.Read()
            //                                 .Where(x => x.CompanyID == company.ID)
            //                                 .Where(x => x.OrderMode == OrderMode.Regular)
            //                                 .Include(x => x.Items)
            //                                 .FirstOrDefault();
            //}

            //return orderHeader;
            var service = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), new SnapDbContext());
            var orderHeader = service.GetWeeklyOrderNoTracking(OrderMode.OneOff, company.ID, fromDate);
            return orderHeader;
        }

        private bool ActiveOnMilk(Company company)
        {
            return company.Method_IsLive(SupplierType.Milk, DateTime.Now.AddDays(1));
        }
        #endregion

    }
}