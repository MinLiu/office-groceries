﻿using NuIngredient.Models;
using Quartz;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleDailyOrderReminder : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var helper = new DailyOrderReminderHelper();
            helper.Execute();
        }
    }
}