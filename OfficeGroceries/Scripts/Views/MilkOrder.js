﻿/*  parameters: 
    MilkMinimumValue: decimal
    IsAuthenticated: bool
    
*/

//function ShowMilkProspectForm() {
//    if (!CheckMilkMinimum()) {
//        alert('Dear Customer\nPlease note a minimum spend of £' + parseFloat(Math.round(MilkMinimumValue * 100) / 100).toFixed(2) + ' per week is required for Dairy deliveries');
//        return false;
//    }
//    SaveMilkOrders(false, 'Draft');
//    $('#Modal_Prospect .modal-title').html('Contact Details');
//    $('#Modal_Prospect .modal-body').load("/Prospects/_ProspectForm");
//    $('#Modal_Prospect').modal('show');
//}

function MilkQuoteExist() {
    if ($('#milk-quotes').length) {
        return true;
    }
    return false;
}

function GetMilkOrderTotal() {
    let totalPrice = 0;
    $('.order-products.milk').each(function () {
        const dataID = $(this).data("id");
        const price = kendo.parseFloat($(this).data('price'));
        const qty_mon = kendo.parseInt($(this).find('input#' + dataID + '-monday-val').val() | 0);
        const qty_tue = kendo.parseInt($(this).find('input#' + dataID + '-tuesday-val').val() | 0);
        const qty_wed = kendo.parseInt($(this).find('input#' + dataID + '-wednesday-val').val() | 0);
        const qty_thu = kendo.parseInt($(this).find('input#' + dataID + '-thursday-val').val() | 0);
        const qty_fri = kendo.parseInt($(this).find('input#' + dataID + '-friday-val').val() | 0);
        const qty_sat = kendo.parseInt($(this).find('input#' + dataID + '-saturday-val').val() | 0);
        const weeklyVolume = qty_mon + qty_tue + qty_wed + qty_thu + qty_fri + qty_sat;
        const weeklyTotal = weeklyVolume * price;

        totalPrice += weeklyTotal;
    });
    return totalPrice;
}

function CheckMilkMinimum() {
    const totalPrice = GetMilkOrderTotal();
    console.log("totalprice: " + totalPrice, ", Minimum: " + MilkMinimumValue);
    if (totalPrice < MilkMinimumValue)
        return false;

    return true;
}

var timeoutMilkFunc;
function ChangeMilkQuantity() {
    var totalPrice = 0;
    var totalVolume = 0;
    var totalVAT = 0;

    let hasDeliveryMon = false;
    let hasDeliveryTue = false;
    let hasDeliveryWed = false;
    let hasDeliveryThu = false;
    let hasDeliveryFri = false;
    let hasDeliverySat = false;
    
    $('.order-products.milk').each(function () {
        var dataID = $(this).data("id");
        var price = kendo.parseFloat($(this).data('price'));
        var vatRate = kendo.parseFloat($(this).data('vat'));
        var qty_mon = kendo.parseInt($(this).find('input#' + dataID + '-monday-val').val() | 0);
        var qty_tue = kendo.parseInt($(this).find('input#' + dataID + '-tuesday-val').val() | 0);
        var qty_wed = kendo.parseInt($(this).find('input#' + dataID + '-wednesday-val').val() | 0);
        var qty_thu = kendo.parseInt($(this).find('input#' + dataID + '-thursday-val').val() | 0);
        var qty_fri = kendo.parseInt($(this).find('input#' + dataID + '-friday-val').val() | 0);
        var qty_sat = kendo.parseInt($(this).find('input#' + dataID + '-saturday-val').val() | 0);
        var weeklyVolume = qty_mon + qty_tue + qty_wed + qty_thu + qty_fri + qty_sat;
        var weeklyTotal = weeklyVolume * price;
        var weeklyVATPrice = weeklyVolume * price * vatRate;

        console.log(weeklyVolume);
        console.log(weeklyTotal);
        console.log(weeklyVATPrice);
        
        if (qty_mon > 0) hasDeliveryMon = true;
        if (qty_tue > 0) hasDeliveryTue = true;
        if (qty_wed > 0) hasDeliveryWed = true;
        if (qty_thu > 0) hasDeliveryThu = true;
        if (qty_fri > 0) hasDeliveryFri = true;
        if (qty_sat > 0) hasDeliverySat = true;

        $(this).find('#product-quantity').text(weeklyVolume);
        $(this).find('#total-cost').text(_toCurrencyString(weeklyTotal));

        totalVolume += weeklyVolume;
        totalPrice += weeklyTotal;
        totalVAT += weeklyVATPrice;
        //console.log("price: " + price + ", qty_mon:" + qty_mon + ", qty_tue:" + qty_tue + ", qty_wed:" + qty_wed + ", qty_thu:" + qty_thu + ", qty_fri:" + qty_fri + ", qty_sat:" + qty_sat + ", weeklyVolume:" + weeklyVolume + ", weeklyTotal:" + weeklyTotal);
    })
    
    if ($('#delivery-charge-row').length > 0) {
        
        let countDrop = 0;
        if (hasDeliveryMon) countDrop += 1;
        if (hasDeliveryTue) countDrop += 1;
        if (hasDeliveryWed) countDrop += 1;
        if (hasDeliveryThu) countDrop += 1;
        if (hasDeliveryFri) countDrop += 1;
        if (hasDeliverySat) countDrop += 1;
        
        const charge = kendo.parseFloat($('#delivery-charge-row').data('charge'));
        const totalCharge = charge * countDrop;

        $('#mon-delivery').text(hasDeliveryMon ? 'v' : '');
        $('#tue-delivery').text(hasDeliveryTue ? 'v' : '');
        $('#wed-delivery').text(hasDeliveryWed ? 'v' : '');
        $('#thu-delivery').text(hasDeliveryThu ? 'v' : '');
        $('#fri-delivery').text(hasDeliveryFri ? 'v' : '');
        $('#sat-delivery').text(hasDeliverySat ? 'v' : '');
        $('#delivery-total-drop').text(countDrop);
        $('#delivery-total').text(_toCurrencyString(totalCharge));
        
        totalPrice += totalCharge;
    }

    $('.milk-order-area .grand-total-volume').each(function () {
        $(this).text(totalVolume);
    })

    $('.milk-order-area .grand-total-price').each(function () {
        $(this).text(_toCurrencyString(totalPrice));
    })

    $('.milk-order-area .grand-total-vat').each(function () {
        $(this).text(_toCurrencyString(totalVAT));
    })

    $('.milk-order-area .grand-total-gross').each(function () {
        $(this).text(_toCurrencyString(totalVAT + totalPrice));
    })

    if (!IsAuthenticated) {
        // Delete the area in quote car if there's no items
        if (!$('#milk-quotes .order-products.milk').length) {
            $('#milk-quotes').remove();
        }

        // To keep prospect's quote between tabs
        if (timeoutMilkFunc != null) clearTimeout(timeoutMilkFunc);
        timeoutMilkFunc = setTimeout(function() {
            SaveMilkOrders(false, 'Draft', true)
        }, 1000);
    }
    $('.save-order').addClass('blink');
}

function ClearMilkQty(weekday) {
    if (!confirm("Are you sure you want to clear all on this day?")) return false;

    $('.order-products.milk').each(function () {
        var dataID = $(this).data("id");
        $(this).find('input#' + dataID + '-' + weekday + '-val').val(0);
    })
    ChangeMilkQuantity();
}

function DeleteMilkRow(itemID) {
    if (!confirm('Remove this product?'))
        return false;
    $(".order-products.milk[data-id='" + itemID + "']").remove();
    ChangeMilkQuantity();
}

function AddMilkToOrder(that) {
    var milkID = $(that).data("id");

    if ($(".order-products.milk[data-milkid='" + milkID + "']").length > 0) {
        alert("The chosen product is already in your standing order.");
        return false;
    }
    $.ajax({
        url: '/Milk/_MilkOrderRow',
        type: 'POST',
        data: {
            milkProductID: milkID,
            orderMode: GetOrderMode(),
            date: GetSelectedDate()
        },
        success: function (response) {
            $('.table-orders tbody').append(response);
            $('.save-order ').addClass('blink');
            $('html,body').animate({
                scrollTop: $("#order-area").offset().top
            }, 500);
            $('#ProductOverView').modal('hide');
        }
    });
}

// For Prospects
function AddMilkToQuote(that) {
    var milkID = $(that).data("id");

    $.ajax({
        url: '/MilkOrders/_AddToQuote',
        type: 'POST',
        data: {
            milkProductID: milkID
        },
        success: function (response) {
            $('#ProductOverView').modal('hide');
            ShowQuoteCart();
        }
    });
}

function SaveMilkOrders(submit, orderMode, prospectQtyChange) {
    // Initializations
    submit = submit || false;
    orderMode = orderMode || 'Draft';
    prospectQtyChange = prospectQtyChange || false; // Prospect changing qty doesn't need to check miminum 

    if (submit == false && orderMode == 'Draft' && (!prospectQtyChange && !CheckMilkMinimum())) {
        alert('Dear Customer\nPlease note a minimum spend of £' + parseFloat(Math.round(MilkMinimumValue * 100) / 100).toFixed(2) + ' per week is required for Dairy deliveries');
        return false;
    }

    if (orderMode === 'Regular' && GetMilkOrderTotal() === 0) {
        alert('Dear Customer\n\nTo zero out your weekly order you must contact us by phone on 0345 463 8863 pressing option 1 and one of our customer service representatives is on hand to help.\n\n' +
            'Thank you\n');

        location.reload();

        return false;
    }
    
    var orders = [];

    $('.order-products.milk').each(function () {
        var dataID = $(this).data("id");
        var milkID = $(this).data('milkid');
        if (milkID != undefined) {
            var orderItem = {
                ID: dataID,
                MilkID: $(this).data('milkid'),
                ProductName: $(this).find('#product-name').text(),
                Unit: $(this).find('#product-unit').text(),
                Monday: kendo.parseInt($(this).find('input#' + dataID + '-monday-val').val() | 0),
                Tuesday: kendo.parseInt($(this).find('input#' + dataID + '-tuesday-val').val() | 0),
                Wednesday: kendo.parseInt($(this).find('input#' + dataID + '-wednesday-val').val() | 0),
                Thursday: kendo.parseInt($(this).find('input#' + dataID + '-thursday-val').val() | 0),
                Friday: kendo.parseInt($(this).find('input#' + dataID + '-friday-val').val() | 0),
                Saturday: kendo.parseInt($(this).find('input#' + dataID + '-saturday-val').val() | 0),
                Sunday: 0,
                Price: kendo.parseFloat($(this).data('price')),
                VATRate: kendo.parseFloat($(this).data('vat'))
            };
            orders.push(orderItem);
        }
    });

    $.ajax({
        url: '/MilkOrders/SaveOrders',
        type: 'POST',
        data: {
            orders: JSON.stringify(orders),
            orderMode: orderMode,
            submit: submit,
            companyID: $('#Company').val(),
            prospectToken: $('#Prospect').val(),
            date: $('#SpecificWeek').val() != undefined ? $('#SpecificWeek').val().substring(0, 10) : null
        },
        beforeSend: !prospectQtyChange ? AjaxBegin : null,
        complete: !prospectQtyChange ? AjaxComplete : null,
        success: function (response) {
            if (response.Success) {
                if (IsAuthenticated) {
                    $('#SavedMessage').show(0).delay(500).hide(0);
                }
                if ($('#Prospect').val() != "" && $('#Prospect').val() != undefined) {
                    alert("Successfully updated the prospect quote");
                }
                else if (submit == true) {
                    alert("Thank you \n An e-mail has been sent to yourself and the supplier denoting all relevant changes.");
                    if (orderMode == 'Regular') {
                        window.location.reload();
                    } else {
                        $('#Modal_Prospect').modal('hide');
                        UpdateOrderWeekDates();
                    }
                }
                $('.save-order').removeClass('blink');
                $('.activate-button').addClass('blink');
            } else {
                alert(response.ErrorMessage);
                location.reload();
            }
        }
    });
}

/*
function GetMilkOrders() {
    var div = $('#WeeklyOrder');
    $.ajax({
        url: '/Milk/_WeeklyOrder',
        type: 'POST',
        data: {
            orderMode: $('a[name=OrderType].aisle-action-button-active').data('value'),
            date: $('#SpecificWeek').val().substring(0, 10),
            companyID: $('#Company').val(),
            prospectToken: $('#Prospect').val()
        },
        success: function (response) {
            div.html(response);
            $.when(ChangeMilkQuantity()).done(function () {
                $('.save-order').removeClass('blink');
            });
        }
    });
}*/

function GetRegularMilkOrders() {
    GetMilkOrders('Regular');
}

function GetDraftMilkOrders() {
    GetMilkOrders('Draft');
}

function GetOneOffMilkOrders() {
    var date = $('#SpecificWeek').val().substring(0, 10);
    GetMilkOrders('OneOff', date);
}

//type: Regular or OneOff
function GetMilkOrders(type, date) {
    var div = $('#WeeklyOrder');
    $.ajax({
        url: '/Milk/_WeeklyOrder',
        type: 'POST',
        data: {
            orderMode: type,
            date: date,
            companyID: $('#Company').val(),
            prospectToken: $('#Prospect').val()
        },
        success: function (response) {
            div.html(response);
            $.when(ChangeMilkQuantity()).done(function () {
                $('.save-order').removeClass('blink');
            });
        }
    });
}

function GetOrderMode() {
    try {
        var activeBtn = $('.change-type.active')[0];
        if (activeBtn === undefined) {
            return "Regular";
        } else {
            return $(activeBtn).data('value');
        }
    } catch(e) {
        return "Regular";
    }
}

function GetSelectedDate() {
    return $('#SpecificWeek').val() !== undefined ? $('#SpecificWeek').val().substring(0, 10) : null;
}
