﻿function AddToCart(that) {
    var cartItem = {
        ProductID: $(that).data('productid'),
        Qty: $(that).data('qty')
    }

    $.ajax({
        url: '/Products/AddToCart',
        type: 'POST',
        data: {
            companyID: GetCompanyID(),
            quote: cartItem
        },
        success: function (response) {
            ShowShoppingCart();
            RefreshShoppingCartTotal();
        }
    })
}

function AddToQuote(that) {
    var cartItem = {
        ProductID: $(that).data('productid'),
        Qty: $(that).data('qty')
    }

    $.ajax({
        url: '/Products/AddToCart',
        type: 'POST',
        data: {
            quote: cartItem
        },
        success: function (response) {
            ShowQuoteCart();
        }
    })
}

function ShowShoppingCart() {
    $.ajax({
        url: '/Products/_ShoppingCart',
        type: 'GET',
        data: {
            companyID: GetCompanyID()
        },
        success: function (response) {
            $('#Modal-shoppingcart .cart-content').html(response);
            $('#Modal-shoppingcart').modal('show');
            RefreshShoppingCartTotal();
        }
    })
}

function ShowQuoteCart() {
    $.ajax({
        url: '/QuoteCarts/_QuoteCart',
        type: 'GET',
        success: function (response) {
            $('#Modal-quotecart .quote-body').html(response);
            $('#Modal-quotecart').modal('show');
        }
    })
}

function RemoveFromCart(id) {
    $.ajax({
        url: '/Products/RemoveCartItem',
        type: 'POST',
        data: {
            productID: id,
            companyID: GetCompanyID()
        },
        success: function (response) {
            CaculateSubtotal();
            RefreshShoppingCartTotal();
        }
    })
}

function OnCartQtyChange(that) {
    var cartItem = {
        ProductID: $(that).data('productid'),
        Qty: $(that).val()
    };
    $.ajax({
        url: '/Products/ChangeCartItemQty',
        type: 'POST',
        data: {
            quote: cartItem,
            companyID: GetCompanyID()
        },
        success: function (response) {
            CaculateSubtotal();
            RefreshShoppingCartTotal();
        }
    });
}

function CaculateSubtotal() {
    var totalNet = 0;
    var totalVAT = 0;
    var totalGross = 0;
    $('.supplier-cart').each(function () {
        var supplierNet = 0;
        var supplierVAT= 0;
        var supplierGross = 0;
        if ($(this).find('tr[name="CartItem"]').length == 0) {
            // no items, remove panel
            $(this).remove();
        } else {
            $(this).find('tr[name="CartItem"]').each(function () {
                var price = $(this).data('price');
                var qty = $(this).find('input[name="Qty"]').val();
                var productVAT = $(this).data('vat');

                // console.log("price:" + price);
                // console.log("qty:" + qty);
                // console.log("productVAT:" + productVAT);

                var itemNet = price * qty;
                //console.log("itemNet:" + itemNet);
                $(this).children('td[name="ItemNet"]').text(_toCurrencyString(itemNet));

                var itemVAT = itemNet * productVAT; 
                //console.log("itemVAT:" + itemVAT);
                $(this).children('td[name="ItemVAT"]').text(_toCurrencyString(itemVAT));

                var itemGross = itemNet + itemVAT;
                //console.log("itemGross:" + itemGross);
                $(this).children('td[name="ItemGross"]').text(_toCurrencyString(itemGross));

                supplierNet += itemNet;
                supplierVAT += itemVAT;
                supplierGross += itemGross;
            });
            var supplier_minimum = $(this).find('.supplier-minimum-value').val();
            if (supplierNet < supplier_minimum) {
                $(this).find('.label-minimum-prompt').show();
            } else {
                $(this).find('.label-minimum-prompt').hide();
            }

            $(this).find('span[name="SupplierNet"]').text(_toCurrencyString(supplierNet));
            $(this).find('span[name="SupplierVAT"]').text(_toCurrencyString(supplierVAT));
            $(this).find('span[name="SupplierGross"]').text(_toCurrencyString(supplierGross));
            totalNet += supplierNet;
            totalVAT += supplierVAT;
            totalGross += supplierGross;
        }
    });
    $('span[name="CartNet"]').text(_toCurrencyString(totalNet));
    $('span[name="CartVAT"]').text(_toCurrencyString(totalVAT));
    $('span[name="CartGross"]').text(_toCurrencyString(totalGross));
}

function CheckMinimum() {
    var alertMessage = "";
    $('.supplier-cart').each(function () {
        var subtotal = 0;
        $(this).find('tr[name="CartItem"]').each(function () {
            var price = $(this).data('price');
            var qty = $(this).find('input[name="Qty"]').val();
            var itemNet = price * qty;
            //$(this).children('td[name="ItemNet"]').text(kendo.toString(itemNet, "c"));
            subtotal += itemNet;
            console.log("price:" + price);
            console.log("qty:" + qty);
            console.log("itemNet:" + itemNet);
            console.log("subtotal:" + subtotal);
        });
        var supplier_minimum = $(this).find('.supplier-minimum-value').val();
        if (subtotal < supplier_minimum) {
            alertMessage = $(this).find('.label-minimum-prompt').text();
            return false;
        }
    })

    return alertMessage;

}

function ShowProspectForm() {
    if (MilkQuoteExist() && !CheckMilkMinimum()) {
        alert('Dear Customer\nPlease note a minimum spend of £' + parseFloat(Math.round(MilkMinimumValue * 100) / 100).toFixed(2) + ' per week is required for Dairy deliveries');
    } else if (FruitQuoteExist() && !CheckFruitMinimum()) {
        alert('Dear Customer\nPlease note a minimum spend of £' + parseFloat(Math.round(FruitMinimumValue * 100) / 100).toFixed(2) + ' per week is required for Fruit deliveries');
    } else {
        $('#Modal_Prospect .prospect-form-content').load("/Prospects/_ProspectForm");
        $('#Modal_Prospect').modal('show');
    }
}

function GoToCheckOut() {
    var message = CheckMinimum();
    if (message != "") return alert(message);

    location.href = '/Products/CheckOut?companyID=' + GetCompanyID(); 
}


function GetCompanyID() {
    return sessionStorage.CompanyID;
}

function SendQuote() {
    // Send quotes to customer but doesn't have a drygoods account
    $.ajax({
        url: '/Products/SendQuote',
        type: 'POST',
        data: {
            companyID: GetCompanyID()
        },
        success: function (response) {
            RefreshShoppingCartTotal();
            alert("Quote sent");
        },
        beforeSend: AjaxBegin,
        complete: AjaxComplete
    })
}

function RefreshShoppingCartTotal() {
    $.ajax({
        url: '/Products/_ShoppingCartTotal',
        type: 'POST',
        success: function (response) {
            if (response.ItemQty != null && response.Total != null) {
                $('.shopping-cart-qty').html(response.ItemQty);
                $('.shopping-cart-total').html(response.Total);
            }
        }
    })
}