﻿

//Important:
//---------------------
//The global variables are declared and set on the main edit page.
// - jobID
// - sectionID


function Refresh() {
    RefreshHeader();
    RefreshItems();
    KeepScrollPosition();
}

function RefreshHeader() {
    setTimeout(function () {
        var url = '/Jobs/_Header?jobID=' + jobID;
        $("#div_Header").load(url);
    }, 500);
}


function RefreshSections() {
    setTimeout(function () {
        var url = '/Jobs/_ItemsTabURL?jobID=' + jobID;
        var ts = $("#Items_Tabstrip").data("kendoTabStrip");
        var item = ts.contentElement(0);
        $(item).load(url);
        KeepScrollPosition();
    }, 200);
}

function RefreshItems() {

    $('[id*="GridItems"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();

        $('[id*="divSectionTotal"]').each(function () {
            var url = '/Jobs/_SectionTotal?sectionID=' + $(this).attr("data-section");
            $(this).load(url);
        });
    });
}


function OnGridItemsCancel(e) {
    RefreshItems();
}

function AddProducts(newSecID) {

    sectionID = newSecID;
    //var url = '/Jobs/_AddProducts?jobID=' + jobID;
    //$("#div_AddProducts").load(url);

    $("#SlideContent_AddProducts").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddProducts").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
    $("#GridAddProducts").data("kendoGrid").dataSource.read();
    KeepScrollPosition();
}


function AddProductItem(pdtID, qty) {

    //sectionID set in function approve
    $.ajax({
        url: '/Jobs/_AddProductItem',
        type: 'POST',
        data: {
            "id": pdtID,
            "jobID": jobID,
            "sectionID": sectionID,
            "qty": qty,
        },
        dataType: "json",
        success: function (data) {
            ProductAdded();  //function found on main Edit view
            $('#GridItems' + sectionID).data('kendoGrid').dataSource.read();
        }
    });
}


function AddCustomProduct(sectionID) {
    var url = '/Jobs/_AddCustomProduct?jobID=' + jobID + '&sectionID=' + sectionID;
    $("#div_AddCustom").load(url);

    $("#SlideContent_AddCustom").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
    KeepScrollPosition();
}


function AssociateQuotation() {
    var url = '/Jobs/_PickAssociateQuotation?jobID=' + jobID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function ShowApplyDiscount() {
    var url = '/Jobs/_ApplyDiscount?jobID=' + jobID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function ShowApplyMarkup() {
    var url = '/Jobs/_ApplyMarkup?jobID=' + jobID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function ShowApplyMargin() {
    var url = '/Jobs/_ApplyMargin?jobID=' + jobID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function ShowApplyVAT() {
    var url = '/Jobs/_ApplyVAT?jobID=' + jobID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function AddClient() {
    var url = '/Jobs/_AddClient?jobID=' + jobID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function AddClientAddress(delivery) {
    var url = '/Jobs/_AddClientAddress?jobID=' + jobID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function HideViews() {
    $("#SlideContent_AddProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").animate({ width: '0%' }, { queue: false, duration: 300 });
    setTimeout(function () {
        $("#SlideContent_AddProducts").css("display", "none");
        $("#SlideContent_AddCustom").css("display", "none");
        $("#Button_Close_Content").css("display", "none");
        $("#Content_ItemPhoto").css("display", "none");
    }, 300);
    KeepScrollPosition();
}

function ProductAdded() {
    $("#div_Added").css("display", "block");
    setTimeout(function () { $("#div_Added").css("display", "none"); }, 2000);

    RefreshHeader();
    RefreshItems();
}


function OpenItemImage(i) {
    var url = '/Jobs/_ItemImage/?id=' + i;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").css("display", "block");
    $("#Button_Close_Content").css("display", "block");
}

function OnItemImageUploadSuccess(ee) {
    //HideViews();
    RefreshItems();
    OpenItemImage($('#input_ItemID').val());

    /*
    var response = ee.response.data();
    alert(response);

    if (ee.response.status == "error") {
        alert("Error: " + ee.response.message);
    }
    else if (ee.response.status == "success") {
        //OpenItemImage(e.response.message);
    }
    */
}

function OnItemImageUpload(e) {
    var files = e.files;
    $.each(files, function () {
        if (this.size / 1024 / 1024 > 1) {
            alert("Max 1Mb file size is allowed!")
            e.preventDefault();
        }
    });
}


function RemoveItemImage(id) {
    $.ajax({
        url: '/JobItems/RemoveItemImage',
        type: 'POST',
        data: { "id": id },
        dataType: "json",
        success: function (data) {
            //HideViews();
            OpenItemImage(id);
            RefreshItems();
        }
    });
}


function KeepScrollPosition() {
    $("html, body").animate({ scrollTop: window.pageYOffset }, 100);
}

function GoToBottomPage() {
    //Scroll to the bottom of page.
    //Delay the scroll to stop conflict with GridCreated scroll animation.
    setTimeout(function () {
        $("html, body").animate({ scrollTop: $(document).height() }, 100);
    }, 130);
}


function ShowSavedMessage() {
    $("#div_Saved").css("display", "block");
    setTimeout(function () { $("#div_Saved").css("display", "none"); }, 2000);
}


function OnGridItemsRequestEnd(e) {
    //RequestEnd handler code
    if (e.type != 'read') {
        Refresh();
    }
}


function AddBlankLine(sectionID) {
    $.ajax({
        url: '/JobItems/_AddBlankItem',
        type: 'POST',
        data: { jobID: jobID, sectionID: sectionID },
        dataType: "json",
        success: function (data) { RefreshItems(); KeepScrollPosition(); }
    });
}


function AddItemToProduct(id) {
    $.ajax({
        url: '/JobItems/_AddItemToProduct',
        type: 'POST',
        data: {
            id: id,
            jobID: jobID,
        },
        dataType: "json",
        success: function () { RefreshItems(); RefreshHeader(); alert('Added to Products List'); }
    });

    //KeepScrollPosition();
    return false;
}

function ConvertItemToCustom(id) {
    $.ajax({
        url: '/JobItems/_ConvertItemToCustom"',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}


function ConvertItemToKit(id) {
    $.ajax({
        url: '/JobItems/_ConvertItemToKit',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}


function ConvertService(id, isService) {
    $.ajax({
        url: '/JobItems/_ConvertService',
        type: 'POST',
        data: { id: id, isService: isService },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}

function SetUseKitPrice(id, useKitPrice) {
    $.ajax({
        url: '/JobItems/_SetUseKitPrice',
        type: 'POST',
        data: { id: id, useKitPrice: useKitPrice },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}


function AddSection(id) {
    $.ajax({
        url: '/JobSections/_AddSection',
        type: 'POST',
        data: { jobID: jobID },
        dataType: "json",
        success: function (data) { RefreshSections(); GoToBottomPage(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}


function RemoveSection(id) {
    $.ajax({
        url: '/JobSections/_RemoveSection',
        type: 'POST',
        data: { sectionID: id },
        dataType: "json",
        success: function (data) { RefreshSections(); RefreshHeader(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}

function ReorderSection(sectionID, direction) {
    $.ajax({
        url: '/JobSections/_ReorderSection',
        type: 'POST',
        data: { sectionID: sectionID, direction: direction },
        dataType: "json",
        success: function (data) { RefreshSections(); },
    });

    //KeepScrollPosition();
    return false;
}


function GridItemsDatabound(e) {
    var grid = e.sender;

    //If no records are returned then insert No Records Message
    if (grid.dataSource.total() == 0) {
        var colCount = grid.columns.length;
        var options = grid.getOptions();
        var msg = options.pageable.messages.empty;
        if (!msg) msg = "No Records Found."; // Default message

        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class='kendo-data-row no-drag'><td colspan='" + colCount + "'><h1 style='color:lightgray; text-align:center; margin:40px;'>" + msg + "</h1></td></tr>");
    }

    //If the page number is less 1 then hide pager.
    var pages = grid.dataSource.total() / grid.dataSource.pageSize();
    if (pages < 1) {
        grid.pager.element.hide();
    }

    /*
    //Selects all delete buttons
    $("#GridItems tbody tr .k-grid-delete").each(function () {
        var currentDataItem = $("#GridItems").data("kendoGrid").dataItem($(this).closest("tr"));

        //Check in the current dataItem if the row is deletable
        if (currentDataItem.IsKitItem) {
            $(this).remove();
        }
    })
    */

    var rows = e.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        try {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);

            if (dataItem.get("IsKitItem") == true) {
                row.css('background-color', 'LightGrey');
                row.css('font-style', 'italic');
            }
        }
        catch (err) { }
    }

}

function OnGridItemsEdit(e) {
    //Hide input based on the type of line item
    if (e.model.IsComment || e.model.ID == 0) {

        e.container.find("input[name='ProductCode']").each(function () { $(this).css("display", "none") });
        e.container.find("input[name='Unit']").each(function () { $(this).css("display", "none") });
        e.container.find("input[name='VAT']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='Markup']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='ListPrice']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='Cost']").each(function () { $(this).css("display", "none") });
        e.container.find("input[name='Price']").each(function () { $(this).css("display", "none") });
        e.container.find("input[name='Quantity']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='Margin']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='MarginPrice']").each(function () { $(this).css("display", "none") });
    }
    if (e.model.HidePrices) {
        e.container.find("input[name='VAT']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='Markup']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='ListPrice']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='Cost']").each(function () { $(this).css("display", "none") });
        e.container.find("input[name='Price']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='Quantity']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='Margin']").each(function () { $(this).css("display", "none") });
        //e.container.find("input[name='MarginPrice']").each(function () { $(this).css("display", "none") });
    }

}