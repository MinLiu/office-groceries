﻿(function($){

    $(document).ajaxComplete(function () {
    });

	$(document).ready(function(){
        $.ajaxSetup({ cache: false });
        NumricInputsLoaded();
	});
	
    function NumricInputsLoaded() {
        $('html').on('click', '[class*=numeric-up]', function () {
            $(this).parent().find('input').each(function (e) {
                $(this).val(+$(this).val() + 1);
                $(this).change();
            });
        });
        //$('[class*=numeric-up]').click(function () {
        //    $(this).parent().find('input').each(function (e) {
        //        $(this).val(+$(this).val() + 1);
        //        $(this).change();
        //    });
        //});
        $('html').on('click', '[class*=numeric-down]', function () {
            $(this).parent().find('input').each(function (e) {
                if ($(this).val() != 0) {
                    $(this).val(+$(this).val() - 1);
                    $(this).change();
                }
            });
        });
        //$('[class*=numeric-down]').click(function () {
        //    $(this).parent().find('input').each(function (e) {
        //        $(this).val(+$(this).val() - 1);
        //        $(this).change();
        //    });
        //});


        $('html').on('keydown', '.weekday-val', function (e) {
            if (!((e.keyCode > 95 && e.keyCode < 106)
                || (e.keyCode > 47 && e.keyCode < 58)
                || e.keyCode == 8)) {
                return false;
            }
        })
    }
	
})(jQuery);

function ScrollToProductSection() {
    $('html, body').animate({
        scrollTop: ($('#product-section').offset().top) - 150
    },500);
}

function toggleContent() {
    const content = document.querySelector(".foldable-content");
    content.classList.toggle("expanded");

    const button = document.querySelector(".button-read-more");
    if (content.classList.contains("expanded")) {
        button.textContent = "Read less";
    } else {
        button.textContent = "Read more";
    }
}


