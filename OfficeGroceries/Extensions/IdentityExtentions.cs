﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using NuIngredient.Models;

namespace Microsoft.AspNet.Identity
{
    public static class IdentityExtentions
    {
        public static string GetName(this IIdentity identity)
        {
            String email = identity.GetUserName();
            using (var context = new SnapDbContext())
            {
                var user = context.Users.Where(x => x.Email == email).FirstOrDefault();
                if (user != null)
                    return user.FirstName;
                return "";
            }
        }

        public static string GetCompanyName(this IIdentity identity)
        {
            String email = identity.GetUserName();
            using (var context = new SnapDbContext())
            {
                var company = context.Users.Where(x => x.Email == email).Select(x => x.Company).FirstOrDefault();
                if (company != null)
                    return company.Name;
                return "";
            }
        }
    }
}