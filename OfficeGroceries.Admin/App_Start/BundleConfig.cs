﻿using System.Web;
using System.Web.Optimization;

namespace NuIngredient
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            BundleTable.EnableOptimizations = true;

            //Standard Bootstrap and Site CSS & JS
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Scripts/jquery.unobtrusive-ajax.min.js",
                      "~/Scripts/Fruitful.js",
                      "~/Scripts/og-site.js",
                      "~/Scripts/Views/ShoppingCart.js",
                      "~/Scripts/Views/MilkOrder.js",
                      "~/Scripts/Views/FruitOrder.js",
                      "~/Content/fancybox/jquery.fancybox.pack.js",
                      "~/Content/fancybox/helpers/jquery.fancybox-buttons.js",
                      "~/Content/fancybox/helpers/jquery.fancybox-media.js"
                      ));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/font-awesome.min.css",
            //          "~/Content/Fruitful.css",
            //          "~/Content/Site.css"
            //          ));

            //bundles.Add(new StyleBundle("~/CMS/css").Include(
            //          "~/Views/CMS/Content/Css/bootstrap.css",
            //          "~/Views/CMS/Content/Css/font-awesome.min.css",
            //          "~/Views/CMS/Content/Css//Fruitful.css",
            //          "~/Views/CMS/Content/Css//Site.css"
            //          ));

            bundles.Add(new StyleBundle("~/CMS/bootstrap").Include( "~/Views/CMS/Content/Css/bootstrap.css" ));
            bundles.Add(new StyleBundle("~/CMS/font-awesome").Include("~/Views/CMS/Content/Css/font-awesome.min.css"));
            bundles.Add(new StyleBundle("~/CMS/site").Include("~/Views/CMS/Content/Css/Site.css"));
            bundles.Add(new StyleBundle("~/CMS/fruitful").Include("~/Views/CMS/Content/Css/Fruitful.css"));
            bundles.Add(new StyleBundle("~/CMS/styles-og").Include("~/Views/CMS/Content/Css/styles-og.css"));


            bundles.Add(new StyleBundle("~/CMS/fancybox").Include("~/Content/fancybox/jquery.fancybox.css" ));
            bundles.Add(new StyleBundle("~/CMS/fancybox-buttons").Include("~/Content/fancybox/helpers/jquery.fancybox-buttons.css"));

            //Kendo/Telerik CSS & JS
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                    "~/Scripts/kendo/2017.2.621/jquery.min.js",
                    "~/Scripts/kendo.modernizr.custom.js",
                    "~/Scripts/kendo/2017.2.621/jszip.min.js",
                    "~/Scripts/kendo/2017.2.621/kendo.all.min.js",
                    "~/Scripts/kendo/2017.2.621/kendo.aspnetmvc.min.js",
                    "~/Scripts/kendo/2017.2.621/kendo.timezones.min.js",
                    "~/Scripts/kendo.modernizr.custom.js",
                    "~/Scripts/kendo/cultures/kendo.culture.en-GB.min.js"
            ));

            bundles.Add(new StyleBundle("~/kendo/css").Include(
                    "~/Content/kendo/2017.2.621/kendo.common-fruitful.css",
                    "~/Content/kendo/2017.2.621/kendo.mobile.all.min.css",
                    "~/Content/kendo/2017.2.621/kendo.dataviz.min.css",
                    "~/Content/kendo/2017.2.621/kendo.fruitful.css", //Fruitful Kendo Skin here
                    "~/Content/kendo/2017.2.621/kendo.dataviz.default.min.css"
            ));
            
            bundles.Add(new ScriptBundle("~/bundles/emailJs").Include(
                "~/Scripts/email.js"
            ));
        }
    }
}
