﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NuIngredient.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using System.Net.Mail;
using Fruitful.Email;
using System.Data.Entity;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleCopyingOrders : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                return;

            try
            {
                var setting = new SnapDbContext().EmailSettings.First();
                Emailer emailer = new Emailer(new SnapDbContext());
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(setting.Email);
                mailMessage.Subject = "Schedule executed at " + DateTime.Now.ToString("yy/MM/dd");
                mailMessage.Body = "Schedule executed at " + DateTime.Now.ToString("yy/MM/dd HH:mm");
                mailMessage.To.Add("min@fruitfulgroup.com");
                emailer.SendEmail(mailMessage);
            }
            catch{ }

            var companyList = new CompanyRepository().Read().Where(c => c.DeleteData == false).ToList();
            foreach (var company in companyList)
            {
                GenerateFruitInvoices(company);
                GenerateMilkInvoices(company);
            }
        }

        #region Fruit

        public void GenerateFruitInvoices(Company company)
        {
            var nextDeliveryDate = DateTime.Now.AddHours(CutOffTime.RestTime);
            var nextDeliveryDay = nextDeliveryDate.DayOfWeek;
            var firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            if (nextDeliveryDay == DayOfWeek.Sunday)
            {
                nextDeliveryDate = nextDeliveryDate.AddHours(24);
                nextDeliveryDay = nextDeliveryDate.DayOfWeek;
                firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            }

            var orderHeader = GetFruitOrder(company, firstDayOfWeek);

            if (!ActiveOnFruit(company)) return;
            else if(orderHeader == null || !orderHeader.Items.Any()) return;

            using (var db = new SnapDbContext())
            {
                var accountNo = db.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Fruits)
                                                   .Where(x => x.CompanyID == company.ID)
                                                   .Where(x => x.StartDate <= nextDeliveryDate && (x.EndDate == null || x.EndDate >= nextDeliveryDate))
                                                   .FirstOrDefault()
                                                   .AccountNo;

                var orderHistoryHeader = db.FruitOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();
                if (orderHistoryHeader == null)
                {
                    orderHistoryHeader = new FruitOrderHeader()
                    {
                        ID = Guid.NewGuid(),
                        CompanyID = company.ID,
                        OrderMode = OrderMode.History,
                        OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                        FromDate = firstDayOfWeek,
                        ToDate = firstDayOfWeek.AddDays(5),
                        AccountNo = accountNo,
                    };
                    db.FruitOrderHeaders.Add(orderHistoryHeader);
                    db.SaveChanges();

                    orderHistoryHeader = db.FruitOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();
                }

                foreach (var orderItem in orderHeader.Items)
                {
                    var historyItem = orderHistoryHeader.Items.Where(x => x.FruitProductID == orderItem.FruitProductID).FirstOrDefault();
                    if (historyItem == null)
                    {
                        historyItem = new FruitOrder()
                        {
                            ID = Guid.NewGuid(),
                            OrderHeaderID = orderHistoryHeader.ID,
                            FruitProductID = orderItem.FruitProductID,
                            ProductName = orderItem.ProductName,
                            Price = orderItem.Price,
                            VATRate = orderItem.VATRate,
                            Unit = orderItem.Unit,
                            Monday = 0,
                            Tuesday = 0,
                            Wednesday = 0,
                            Thursday = 0,
                            Friday = 0,
                            Saturday = 0,
                            Sunday = 0,
                            TotalNet = 0,
                            TotalVAT = 0
                        };
                        db.FruitOrders.Add(historyItem);
                        db.SaveChanges();
                    }
                }

                var historyItems = db.FruitOrders.Where(x => x.OrderHeader.CompanyID == company.ID)
                                                 .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                                 .Where(x => x.OrderHeader.FromDate == firstDayOfWeek)
                                                 .ToList();


                var orderTotalNet = 0m;
                var orderTotalVAT = 0m;
                foreach (var orderItem in orderHeader.Items)
                {
                    var historyItem = historyItems.Where(x => x.FruitProductID == orderItem.FruitProductID).FirstOrDefault();
                    switch (nextDeliveryDay)
                    {
                        case DayOfWeek.Sunday: // skip sunday
                        case DayOfWeek.Monday:
                            historyItem.Monday = orderItem.Monday;
                            break;
                        case DayOfWeek.Tuesday:
                            historyItem.Tuesday = orderItem.Tuesday;
                            break;
                        case DayOfWeek.Wednesday:
                            historyItem.Wednesday = orderItem.Wednesday;
                            break;
                        case DayOfWeek.Thursday:
                            historyItem.Thursday = orderItem.Thursday;
                            break;
                        case DayOfWeek.Friday:
                            historyItem.Friday = orderItem.Friday;
                            break;
                        case DayOfWeek.Saturday:
                            historyItem.Saturday = orderItem.Saturday;
                            break;
                        default:
                            break;
                    }
                    historyItem.TotalNet = (historyItem.Monday + historyItem.Tuesday + historyItem.Wednesday + historyItem.Thursday + historyItem.Friday + historyItem.Saturday) * historyItem.Price;
                    historyItem.TotalVAT = historyItem.TotalNet * historyItem.VATRate;

                    orderTotalNet += historyItem.TotalNet;
                    orderTotalVAT += historyItem.TotalVAT;

                }
                orderHistoryHeader.TotalNet = orderTotalNet;
                orderHistoryHeader.TotalVAT = orderTotalVAT;

                db.SaveChanges();
            }
        }

        private FruitOrderHeader GetFruitOrder(Company company, DateTime fromDate)
        {
            var orderHeaderRepo = new FruitOrderHeaderRepository();
            var orderHeader = orderHeaderRepo.Read()
                                             .Where(x => x.CompanyID == company.ID)
                                             .Where(x => x.OrderMode == OrderMode.OneOff)
                                             .Where(x => x.FromDate == fromDate)
                                             .Include(x => x.Items)
                                             .FirstOrDefault();
            if (orderHeader == null)
            {
                orderHeader = orderHeaderRepo.Read()
                                             .Where(x => x.CompanyID == company.ID)
                                             .Where(x => x.OrderMode == OrderMode.Regular)
                                             .Include(x => x.Items)
                                             .FirstOrDefault();
            }

            return orderHeader;
        }

        private bool ActiveOnFruit(Company company)
        {
            return company.Method_IsLive(SupplierType.Fruits);
        }

        #endregion

        #region Milk
        public void GenerateMilkInvoices(Company company)
        {
            var nextDeliveryDate = DateTime.Now.AddHours(CutOffTime.RestTime);
            var nextDeliveryDay = nextDeliveryDate.DayOfWeek;
            var firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            if (nextDeliveryDay == DayOfWeek.Sunday)
            {
                nextDeliveryDate = nextDeliveryDate.AddHours(24);
                nextDeliveryDay = nextDeliveryDate.DayOfWeek;
                firstDayOfWeek = nextDeliveryDate.StartOfWeek(DayOfWeek.Monday);
            }

            var orderHeader = GetMilkOrder(company, firstDayOfWeek);

            if (!ActiveOnMilk(company)) return;
            else if (orderHeader == null || !orderHeader.Items.Any()) return;

            using (var db = new SnapDbContext())
            {
                var accountNo = db.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                                   .Where(x => x.CompanyID == company.ID)
                                                   .Where(x => x.StartDate <= nextDeliveryDate && (x.EndDate == null || x.EndDate >= nextDeliveryDate))
                                                   .FirstOrDefault()
                                                   .AccountNo;

                var orderHistoryHeader = db.MilkOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();
                if (orderHistoryHeader == null)
                {
                    orderHistoryHeader = new MilkOrderHeader()
                    {
                        ID = Guid.NewGuid(),
                        CompanyID = company.ID,
                        OrderMode = OrderMode.History,
                        OrderNumber = String.Format("M{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                        FromDate = firstDayOfWeek,
                        ToDate = firstDayOfWeek.AddDays(5),
                        AccountNo = accountNo,
                    };
                    db.MilkOrderHeaders.Add(orderHistoryHeader);
                    db.SaveChanges();

                    orderHistoryHeader = db.MilkOrderHeaders.Where(x => x.CompanyID == company.ID)
                                                             .Where(x => x.OrderMode == OrderMode.History)
                                                             .Where(x => x.FromDate == firstDayOfWeek)
                                                             .Include(x => x.Items)
                                                             .FirstOrDefault();
                }

                foreach (var orderItem in orderHeader.Items)
                {
                    var historyItem = orderHistoryHeader.Items.Where(x => x.MilkProductID == orderItem.MilkProductID).FirstOrDefault();
                    if (historyItem == null)
                    {
                        historyItem = new MilkOrder()
                        {
                            ID = Guid.NewGuid(),
                            OrderHeaderID = orderHistoryHeader.ID,
                            MilkProductID = orderItem.MilkProductID,
                            ProductName = orderItem.ProductName,
                            Price = orderItem.Price,
                            VATRate = orderItem.VATRate,
                            Unit = orderItem.Unit,
                            Monday = 0,
                            Tuesday = 0,
                            Wednesday = 0,
                            Thursday = 0,
                            Friday = 0,
                            Saturday = 0,
                            Sunday = 0,
                            TotalNet = 0,
                            TotalVAT = 0
                        };
                        db.MilkOrders.Add(historyItem);
                        db.SaveChanges();
                    }
                }

                var historyItems = db.MilkOrders.Where(x => x.OrderHeader.CompanyID == company.ID)
                                                 .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                                 .Where(x => x.OrderHeader.FromDate == firstDayOfWeek)
                                                 .ToList();


                var orderTotalNet = 0m;
                var orderTotalVAT = 0m;
                foreach (var orderItem in orderHeader.Items)
                {
                    var historyItem = historyItems.Where(x => x.MilkProductID == orderItem.MilkProductID).FirstOrDefault();
                    switch (nextDeliveryDay)
                    {
                        case DayOfWeek.Sunday: // skip sunday
                        case DayOfWeek.Monday:
                            historyItem.Monday = orderItem.Monday;
                            break;
                        case DayOfWeek.Tuesday:
                            historyItem.Tuesday = orderItem.Tuesday;
                            break;
                        case DayOfWeek.Wednesday:
                            historyItem.Wednesday = orderItem.Wednesday;
                            break;
                        case DayOfWeek.Thursday:
                            historyItem.Thursday = orderItem.Thursday;
                            break;
                        case DayOfWeek.Friday:
                            historyItem.Friday = orderItem.Friday;
                            break;
                        case DayOfWeek.Saturday:
                            historyItem.Saturday = orderItem.Saturday;
                            break;
                        default:
                            break;
                    }
                    historyItem.TotalNet = (historyItem.Monday + historyItem.Tuesday + historyItem.Wednesday + historyItem.Thursday + historyItem.Friday + historyItem.Saturday) * historyItem.Price;
                    historyItem.TotalVAT = historyItem.TotalNet * historyItem.VATRate;

                    orderTotalNet += historyItem.TotalNet;
                    orderTotalVAT += historyItem.TotalVAT;

                }
                orderHistoryHeader.TotalNet = orderTotalNet;
                orderHistoryHeader.TotalVAT = orderTotalVAT;

                db.SaveChanges();
            }
        }

        private MilkOrderHeader GetMilkOrder(Company company, DateTime fromDate)
        {
            var orderHeaderRepo = new MilkOrderHeaderRepository();
            var orderHeader = orderHeaderRepo.Read()
                                             .Where(x => x.CompanyID == company.ID)
                                             .Where(x => x.OrderMode == OrderMode.OneOff)
                                             .Where(x => x.FromDate == fromDate)
                                             .Include(x => x.Items)
                                             .FirstOrDefault();
            if (orderHeader == null)
            {
                orderHeader = orderHeaderRepo.Read()
                                             .Where(x => x.CompanyID == company.ID)
                                             .Where(x => x.OrderMode == OrderMode.Regular)
                                             .Include(x => x.Items)
                                             .FirstOrDefault();
            }

            return orderHeader;
        }

        private bool ActiveOnMilk(Company company)
        {
            return company.Method_IsLive(SupplierType.Milk);
        }
        #endregion

    }
}