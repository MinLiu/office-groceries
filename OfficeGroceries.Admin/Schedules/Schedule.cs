﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Quartz.Job;

namespace NuIngredient.Schedule
{
    public class Schedule
    {
        private static IScheduler sched;

        public static void ScheduleStart()
        {
            // Construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // Get a scheduler
            sched = schedFact.GetScheduler();
            sched.Start();

            // Define the job and tie it to the class
            IJobDetail job = JobBuilder.Create<ScheduleCopyingOrders>()
                .WithIdentity("Job_ScheduleInvoicing")
                .Build();

            // Trigger the job to run every Monday
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("Trigger_ScheduleInvoicing")
                .StartNow()
                .WithCronSchedule("0 0 14 * * ? *") // fire at 2pm everyday
                .Build();

            IJobDetail jobCreateSitemap = JobBuilder.Create<ScheduleCreateSitemap>()
                .WithIdentity("Job_CreateSitemap")
                .Build();

            ITrigger triggerCreateSitemap = TriggerBuilder.Create()
                .WithIdentity("Trigger_CreateSitemap")
                .StartNow()
                .WithCronSchedule("0 0 2 * * ? *") // fire at 2am everyday
                .Build();

            //sched.ScheduleJob(job, trigger);
            //sched.ScheduleJob(jobCreateSitemap, triggerCreateSitemap);
        }

    }
}