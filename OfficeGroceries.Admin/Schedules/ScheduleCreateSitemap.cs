﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NuIngredient.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using System.Net.Mail;
using Fruitful.Email;
using System.Data.Entity;
using Boilerplate.Web.Mvc.Sitemap;
using System.Web.Hosting;

namespace NuIngredient.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleCreateSitemap : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var sitemapGenerator = new SitemapGeneratorImp();
            var sitemapNodes = sitemapGenerator.Generate();
            sitemapNodes[0].Save(HostingEnvironment.MapPath("/sitemap.xml"));
        }

    }
}