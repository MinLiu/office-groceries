﻿using System;

namespace NuIngredient.Models
{
    public class DeliveryChargePotentialCustomerViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Postcode { get; set; }
    }
}