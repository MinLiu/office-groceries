﻿using System;
using System.Collections.Generic;

namespace NuIngredient.Models
{
    public class SendPoPageViewModel
    {
        public Guid CompanyId { get; set; }
        public List<string> UserIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}