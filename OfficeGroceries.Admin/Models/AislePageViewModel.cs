﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class AislePageViewModel
    {
        public List<CarouselViewModel> Carousels { get; set; }
        public string CompanyName { get; set; }
        public bool Live { get; set; }
        public bool SupplierRequested { get; set; }
        public string DeliveryPrompt { get; set; }
        public AisleViewModel Aisle { get; set; }
        public Supplier AisleSupplier { get; set; }
        public List<ProductCategory> Categories { get; set; }
        public List<ProductListViewModel> Products { get; set; }
        public int Total { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public string Keywords { get; set; }
        public string SelectedCategory { get; set; }
        public string CompanyID { get; set; }
        public string ProspectToken { get; set; }

        public AislePageViewModel(Aisle aisle, int page, string keywords, string category, string companyID, string prospectToken)
        {
            Carousels = new CarouselRepository().Read()
                                                .Where(x => x.FromDate <= DateTime.Today)
                                                .Where(x => x.ToDate >= DateTime.Today)
                                                .Where(x => x.ImageURL != null && x.ImageURL != "")
                                                .Where(x => x.LinkURL != null && x.LinkURL != "")
                                                .Where(x => x.ProductID == null || x.Product.Deleted == false)
                                                .Where(x => x.ProductID == null || x.Product.IsActive == true)
                                                .OrderBy(x => x.ID)
                                                .Take(10)
                                                .ToList()
                                                .Select(x => new CarouselMapper().MapToViewModel(x))
                                                .ToList();
            Live = false;
            SupplierRequested = false;
            Aisle = new AisleMapper().MapToViewModel(aisle);
            AisleSupplier = aisle.Supplier;
            Categories = new ProductCategoryRepository().Read().Where(x => x.AisleID == aisle.ID && x.ParentID == null).OrderBy(x => x.SortPos).ThenBy(x => x.Name).ToList();
            Page = page;
            Keywords = keywords ?? "";
            SelectedCategory = category ?? "";
            CompanyID = companyID ?? "";
            ProspectToken = prospectToken ?? "";
        }

        public void Set(Company company)
        {
            var pdtService = new ProductService<ProductListViewModel>(new ProductListViewMapper());
            if (company != null)
            {
                var pdtQuery = pdtService.CustomerReadByAisle(Aisle.ID, company.ID, Keywords, SelectedCategory);
                CompanyName = company.Name;
                Live = IsAisleLive(company);
                SupplierRequested = AisleSupplier != null ? company.SupAcctRequestLogs.Where(x => x.SupplierID == AisleSupplier.ID).Any() : false;
                DeliveryPrompt = SetDeliveryPrompt(company);
                Total = pdtQuery.Count();
                TotalPages = (Total - 1) / 20 + 1;
                Products = pdtQuery
                     .Skip((Page - 1) * 20)
                     .Take(20)
                     .ToList()
                     .Select(x => pdtService.MapToViewModel(x, company))
                     .ToList();
            }
            else
            {
                var pdtQuery = pdtService.ProspectReadByAisle(Aisle.ID, Keywords, SelectedCategory);
                CompanyName = "";
                Total = pdtQuery.Count();
                TotalPages = (Total - 1) / 20 + 1;
                Products = pdtQuery
                     .Skip((Page - 1) * 20)
                     .Take(20)
                     .ToList()
                     .Select(x => pdtService.MapToViewModel(x, company))
                     .ToList();
            }

        }

        private bool IsAisleLive(Company company)
        {
            if (AisleSupplier != null)
            {
                if (!AisleSupplier.UseMasterAccount)
                {
                    return company.Method_IsLive(AisleSupplier);
                }
                else
                {
                    return company.EasySuppliersActivated;
                }
            }
            else
            {
                return true;
            }
        }

        private string SetDeliveryPrompt(Company company)
        {
            var prompt = "";
            if (AisleSupplier == null)
            {
                prompt = "Orders will be delivered within next 3 - 4 working days.";
            }
            else
            {
                if (AisleSupplier.CalendarNeeded)
                {
                    // Need event details and calendar, no prompt
                    prompt = "";
                }
                else if (!AisleSupplier.UseMasterAccount)
                {
                    // Difficult Suppliers
                    var nextDeliveryDate = company.NextDryGoodsDeliveryDate(AisleSupplier.ID);
                    if (nextDeliveryDate == null)
                    {
                        prompt = "No delivery date.";
                    }
                    else
                    {
                        prompt = String.Format("Order placed before 4pm today will be delivered on {0}", nextDeliveryDate.Value.ToString("ddd dd MMM"));
                    }
                }
                else
                {
                    prompt = "Orders will be delivered within next 3 - 4 working days.";
                }
            }
            //if (AisleSupplier != null && !AisleSupplier.UseMasterAccount)
            //{
            //    // Difficult Suppliers
            //    var nextDeliveryDate = company.NextDryGoodsDeliveryDate(AisleSupplier.ID);
            //    if (nextDeliveryDate == null)
            //    {
            //        prompt = "No delivery date.";
            //    }
            //    else
            //    {
            //        prompt = String.Format("Order placed before 4pm today will be delivered on {0}", nextDeliveryDate.Value.ToString("ddd dd MMM"));
            //    }
            //}
            //else
            //{
            //    // Easy Suppliers
            //    prompt = "Orders will be delivered within next 3 - 4 working days.";
            //}
            return prompt;
        }
    }

}

