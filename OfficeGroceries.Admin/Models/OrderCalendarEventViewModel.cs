﻿using System;
using System.Security.Cryptography;

namespace NuIngredient.Models
{
    public class OrderCalendarEventViewModel : Kendo.Mvc.UI.ISchedulerEvent
    {
        public OrderCalendarEventViewModel(MilkOrderHeader orderHeader)
        {
            var timestamp = orderHeader.LatestUpdated ?? DateTime.MinValue;
            var title = $"{orderHeader.Company.Name} ({timestamp:t}) ({orderHeader.TotalGross:C})";

            if (orderHeader.OrderMode == OrderMode.History)
            {
                var weekStart = orderHeader.FromDate ?? DateTime.MinValue; // Monday
                timestamp = weekStart.AddDays(6).AddHours(9); // Make the history order's timestamp to Sunday 9am

                title = $"{orderHeader.Company.Name} ({orderHeader.FromDate:dd/MM/yyyy} to {orderHeader.ToDate:dd/MM/yyyy})";
            }

            OrderID = orderHeader.ID.ToString();
            Title = title;
            Description = orderHeader.Company.Name;
            OrderMode = orderHeader.OrderMode;
            IsAllDay = false;
            Start = timestamp;
            End = timestamp.AddMinutes(30);
            Depreciated = orderHeader.Depreciated;
        }
        public string OrderID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
        public OrderMode OrderMode { get; set; }
        public bool Depreciated { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string SupplierName { get; set; }
        public string SupplierEmail { get; set; }
    }

}

