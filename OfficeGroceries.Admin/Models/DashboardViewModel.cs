﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;

namespace NuIngredient.Models
{

    public class DashboardViewModel
    {
        public bool ActiveOnFruit { get; set; }
        public bool ActiveOnMilk { get; set; }
        public bool ActiveOnDryGoods { get; set; }
        public bool AskingForMilkSupplier { get; set; }
        public bool AskingForFruitSupplier { get; set; }
        public bool AskingForDryGoodsSupplier { get; set; }
        public string AccountNo { get; set; }
        public string Message { get; set; }

        public void Set(Company company)
        {
            ActiveOnFruit = company.Method_IsLive(SupplierType.Fruits);/*Method_IsLive(SupplierType.Fruits);.*/
            ActiveOnMilk = company.Method_IsLive(SupplierType.Milk);//.Method_IsLive(SupplierType.Milk);//
            ActiveOnDryGoods = company.Method_IsLive(SupplierType.DryGoods);//.Method_IsLive(SupplierType.DryGoods);//
            AskingForMilkSupplier = company.AskingForMilkSupplier != null;
            AskingForFruitSupplier = company.AskingForFruitSupplier != null;
            AskingForDryGoodsSupplier = company.SupAcctRequestLogs.Where(x => x.Supplier.DefaultAisles.Where(a => a.Name == "Dry Goods").Any()).Any();
            AccountNo = company.AccountNo;
            Message = GetMessage(company);
        }

        public string GetMessage(Company company)
        {
            var today = DateTime.Today;
            var messageRepo = new MessageRepository();
            var messages = messageRepo.Read()
                                      .Where(x => x.FromDate <= today && x.ToDate >= today)
                                      .Where(x => !x.MessageRead.Select(m => m.CompanyID).Contains(company.ID))
                                      .Include(x => x.MessageTargets)
                                      .ToList();

            var messageReadRepo = new MessageReadRepository();
            var results = new List<string>();
            foreach (var message in messages)
            {
                if ((company.Method_HasSupplier(SupplierType.Milk) && message.MessageTargets.Select(x => x.TargetID).Contains(TargetValues.Milk))
                    || (company.Method_HasSupplier(SupplierType.Fruits) && message.MessageTargets.Select(x => x.TargetID).Contains(TargetValues.Fruits))
                    || (company.Method_HasSupplier(SupplierType.DryGoods) && message.MessageTargets.Select(x => x.TargetID).Contains(TargetValues.DryGoods)))
                {
                    results.Add(message.Content);
                    messageReadRepo.Create(new MessageRead
                    {
                        MessageID = message.ID,
                        CompanyID = company.ID
                    });
                }
            }

            return String.Join("<br />", results);
        }
    }

}

