﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class ShoppingCartGridViewModel
    {
        public Guid? CompanyID { get; set; }
        public string CompanyName { get; set; }
        public int Count { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public decimal Total { get; set; }
    }

}