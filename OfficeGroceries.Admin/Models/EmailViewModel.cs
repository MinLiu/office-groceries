﻿using System;
using System.Web.Mvc;

namespace NuIngredient.Models
{
    public class EmailViewModel
    {
        public EmailType Type { get; set; } 
        public Guid? SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public Guid? ReferenceID { get; set; } = Guid.NewGuid();
    }
    
    public enum EmailType
    {
        SupplierCustomers,
        AllSuppliers
    }
}