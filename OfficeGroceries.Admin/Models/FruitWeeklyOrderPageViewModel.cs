﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class FruitWeeklyOrderPageViewModel
    {
        public OrderMode OrderMode { get; set; }
        public bool HasFruitSupplier { get; set; }
        public bool LiveOnFruit { get; set; }
        public bool HasRegularFruitOrder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool HasSnackSupplier { get; set; }
        public bool LiveOnSnack { get; set; }
        public DateTime SnackStartDate { get; set; }
        public DateTime SnackEndDate { get; set; }
        public List<FruitOrderViewModel> FruitOrders { get; set; }
        public List<string> Weekdates { get; set; }
        public List<bool> Enables { get; set; }
        public List<bool> SnackEnables { get; set; }
        public bool IsCustomer { get; set; }
        public bool IsProspect { get; set; }
        public string IntermittentDeliveryMessage { get; set; } = "";
        private List<bool> _deliveryDays { get; set; }
        private List<DateTime> _holidays { get; set; }
        private List<DateTime> _openDays { get; set; }
        private List<DateTime> _snackHolidays { get; set; }
        private List<DateTime> _snackOpenDays { get; set; }

        public FruitWeeklyOrderPageViewModel(OrderMode orderMode, string weekStart, string companyID)
        {
            OrderMode = orderMode;
            HasFruitSupplier = false;
            LiveOnFruit = false;
            StartDate = DateTime.MaxValue.AddDays(-1);
            EndDate = DateTime.MaxValue;
            FruitOrders = new List<FruitOrderViewModel>();
            Weekdates = new List<string>();
            Enables = new List<bool>();
            SnackEnables = new List<bool>();
            
            LoadDeliveryDays(companyID);

            if (orderMode == OrderMode.OneOff)
            {
                DateTime dateWeekStart = DateTime.Parse(weekStart);
                var dateMon = dateWeekStart.StartOfWeek(DayOfWeek.Monday);
                var dateTue = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(1);
                var dateWed = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(2);
                var dateThu = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(3);
                var dateFri = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(4);
                var dateSat = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(5);

                LoadSpecialDays(dateMon, companyID);

                Weekdates.AddRange(new List<string>()
                {
                    dateMon.ToString("dd/MM"),
                    dateTue.ToString("dd/MM"),
                    dateWed.ToString("dd/MM"),
                    dateThu.ToString("dd/MM"),
                    dateFri.ToString("dd/MM"),
                    dateSat.ToString("dd/MM")
                });

                var dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date;
                Enables.AddRange(new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon, SupplierType.Fruits) || IsOpenDay(dateMon, SupplierType.Fruits)) && (IsOpenDay(dateMon, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue, SupplierType.Fruits) || IsOpenDay(dateTue, SupplierType.Fruits)) && (IsOpenDay(dateTue, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed, SupplierType.Fruits) || IsOpenDay(dateWed, SupplierType.Fruits)) && (IsOpenDay(dateWed, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu, SupplierType.Fruits) || IsOpenDay(dateThu, SupplierType.Fruits)) && (IsOpenDay(dateThu, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri, SupplierType.Fruits) || IsOpenDay(dateFri, SupplierType.Fruits)) && (IsOpenDay(dateFri, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat, SupplierType.Fruits) || IsOpenDay(dateSat, SupplierType.Fruits)) && (IsOpenDay(dateSat, SupplierType.Fruits) || _deliveryDays[(int)OgDayOfWeek.Saturday]),
                });
                
                dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date.AddDays(1);
                while (dateNotAllowedBefore.DayOfWeek == DayOfWeek.Saturday ||
                       dateNotAllowedBefore.DayOfWeek == DayOfWeek.Sunday ||
                       _snackHolidays.Contains(dateNotAllowedBefore))
                {
                    dateNotAllowedBefore = dateNotAllowedBefore.AddDays(1);
                }
                SnackEnables = new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon, SupplierType.Snacks) || IsOpenDay(dateMon, SupplierType.Snacks)) && (IsOpenDay(dateMon, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue, SupplierType.Snacks) || IsOpenDay(dateTue, SupplierType.Snacks)) && (IsOpenDay(dateTue, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed, SupplierType.Snacks) || IsOpenDay(dateWed, SupplierType.Snacks)) && (IsOpenDay(dateWed, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu, SupplierType.Snacks) || IsOpenDay(dateThu, SupplierType.Snacks)) && (IsOpenDay(dateThu, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri, SupplierType.Snacks) || IsOpenDay(dateFri, SupplierType.Snacks)) && (IsOpenDay(dateFri, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat, SupplierType.Snacks) || IsOpenDay(dateSat, SupplierType.Snacks)) && (IsOpenDay(dateSat, SupplierType.Snacks) || _deliveryDays[(int)OgDayOfWeek.Saturday]),
                };

            }
            else
            {
                Weekdates.AddRange(new List<string>()
                {
                    "","","","","","",
                });

                Enables.AddRange(new List<bool>()
                {
                    true, true, true, true, true, true,
                });
                
                SnackEnables = new List<bool>()
                {
                    true, true, true, true, true, true,
                };

            }
        }
        public void Set(Company company)
        {
            HasFruitSupplier = company.Method_HasSupplier(SupplierType.Fruits);
            LiveOnFruit = company.Method_IsLive(SupplierType.Fruits);
            var fruitSupplier = new CompanySupplierRepository().Read()
                                                          .Where(cs => cs.CompanyID == company.ID)
                                                          .Where(cs => cs.Supplier.SupplierType == SupplierType.Fruits)
                                                          .Where(cs => cs.Supplier.Deleted == false)
                                                          .Where(cs => cs.StartDate.HasValue)
                                                          .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                                                          .OrderBy(cs => cs.StartDate.Value)
                                                          .FirstOrDefault();
            StartDate = fruitSupplier?.StartDate ?? DateTime.MaxValue.AddDays(-1);
            EndDate = fruitSupplier?.EndDate ?? DateTime.MaxValue;
            
            IntermittentDeliveryMessage += (fruitSupplier?.IsIntermittentDelivery ?? false)
                ? $"Delivery occurs every {fruitSupplier.DeliveryFrequency} weeks intermittently. The upcoming delivery is scheduled for the week of {fruitSupplier.NextDeliveryWeek?.ToString("dd/MM/yyyy")}."
                : null;
            
            HasSnackSupplier = company.Method_HasSupplier(SupplierType.Snacks);
            LiveOnSnack = company.Method_IsLive(SupplierType.Snacks);
            var snackSupplier = new CompanySupplierRepository().Read()
                .Where(cs => cs.CompanyID == company.ID)
                .Where(cs => cs.Supplier.SupplierType == SupplierType.Snacks)
                .Where(cs => cs.Supplier.Deleted == false)
                .Where(cs => cs.StartDate.HasValue)
                .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                .OrderBy(cs => cs.StartDate.Value)
                .FirstOrDefault();
            SnackStartDate = snackSupplier?.StartDate ?? DateTime.MaxValue.AddDays(-1);
            SnackEndDate = snackSupplier?.EndDate ?? DateTime.MaxValue;
            
            IntermittentDeliveryMessage += (snackSupplier?.IsIntermittentDelivery ?? false)
                ? $"Delivery occurs every {snackSupplier.DeliveryFrequency} weeks intermittently. The upcoming delivery is scheduled for the week of {snackSupplier.NextDeliveryWeek?.ToString("dd/MM/yyyy")}."
                : null;

            HasRegularFruitOrder = new FruitOrderHeaderRepository().Read()
                                                                   .Where(x => x.OrderMode == OrderMode.Regular)
                                                                   .Where(x => x.CompanyID == company.ID)
                                                                   .Count() > 0;
            IsCustomer = true;
            IsProspect = false;
        }
        public void Set(string prospectToken)
        {
            HasFruitSupplier = true;
            LiveOnFruit = true;
            StartDate = new DateTime(1900, 1, 1);
            EndDate = new DateTime(2100, 1, 1);
            HasRegularFruitOrder = true;
            IsProspect = true;
            IsCustomer = false;
            _deliveryDays = new List<bool> { true, true, true, true, true, true };
        }

        private void LoadSpecialDays(DateTime dateFrom, string companyID)
        {
            Guid? fruitSupplierId = null;
            Guid? snackSupplierId = null;

            if (!string.IsNullOrWhiteSpace(companyID))
            {
                fruitSupplierId = SupplierHelper.GetSupplier(Guid.Parse(companyID), SupplierType.Fruits, dateFrom)?.ID;
                snackSupplierId = SupplierHelper.GetSupplier(Guid.Parse(companyID), SupplierType.Snacks, dateFrom)?.ID;
            }
            
            var dateTo = dateFrom.AddDays(6);
            _holidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(fruitSupplierId)
               .Where(x => dateFrom <= x.Date)
               .Where(x => dateTo >= x.Date)
               .Select(x => x.Date)
               .ToList();
            
            _openDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(fruitSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();

            _snackHolidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(snackSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
            
            _snackOpenDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(snackSupplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
        }

        private void LoadDeliveryDays(string companyId)
        {
            if (string.IsNullOrEmpty(companyId))
            {
                _deliveryDays = new List<bool>
                {
                    true, true, true, true, true, true
                };
                return;
            }

            using (var db = new SnapDbContext())
            {
                var company = db.Companies.Find(Guid.Parse(companyId));

                _deliveryDays = new List<bool>
                {
                    company.FruitsDeliveryMonday,
                    company.FruitsDeliveryTuesday,
                    company.FruitsDeliveryWednesday,
                    company.FruitsDeliveryThursday,
                    company.FruitsDeliveryFriday,
                    company.FruitsDeliverySaturday
                };
            }
        }

        private bool NotHoliday(DateTime date, SupplierType supplierType)
        {
            return supplierType == SupplierType.Fruits
                ? !_holidays.Contains(date.Date)
                : !_snackHolidays.Contains(date.Date);
        }
        
        private bool IsOpenDay(DateTime date, SupplierType supplierType)
        {
            return supplierType == SupplierType.Fruits
                ? _openDays.Contains(date.Date)
                : _snackOpenDays.Contains(date.Date);
        }
        
        /// <summary>
        /// Determines if a specific weekday is enabled for ordering fruit.
        /// </summary>
        public bool IsAvailableDay(OgDayOfWeek dayOfWeek, FruitProductType type)
        {
            if (OrderMode == OrderMode.Draft || OrderMode == OrderMode.Regular)
            {
                return _deliveryDays[(int)dayOfWeek];
            }

            if (OrderMode == OrderMode.OneOff)
            {
                return type == FruitProductType.Fruit
                    ? Enables[(int)dayOfWeek]
                    : SnackEnables[(int)dayOfWeek];
            }
            
            throw new NotImplementedException();
        }
    }

}

