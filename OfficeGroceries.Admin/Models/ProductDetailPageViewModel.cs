﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ProductDetailPageViewModel
    {
        public ProductDetailPageViewModel(Company company, Product product)
        {
            if (company != null)
            {
                Live = IsProductAvailable(company, product);
            }
            else
            {
                Live = false;
            }

            var mapper = new ProductMapper();
            Product = mapper.MapToViewModel(product, company);
        }

        public ProductViewModel Product { get; set; }
        public bool Live { get; set; }

        private bool IsProductAvailable(Company company, Product product)
        {
            if (product.Supplier != null)
            {
                if (!product.Supplier.UseMasterAccount)
                {
                    return company.Method_IsLive(product.Supplier);
                }
                else
                {
                    return company.EasySuppliersActivated;
                }
            }
            else
            {
                return false;
            }
        }

    }

}

