﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ReportMilkOneOffGridViewModel
    {
        public string ID { get; set; }
        public string Supplier { get; set; }
        public string AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public string ProductNameCode { get; set; }
        public string Mon { get; set; }
        public string Tues { get; set; }
        public string Wed { get; set; }
        public string Thurs { get; set; }
        public string Fri { get; set; }
        public string Sat { get; set; }
    }

}

