﻿namespace NuIngredient.Models
{
    public class RestrictedProductVm
    {
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string ProductCategory { get; set; }
    }
}