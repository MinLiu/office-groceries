﻿using System;

namespace NuIngredient.Models
{
    public class MonthlyPoNumberReportVm
    {
        public Guid ID { get; set; }
        public bool IsExported { get; set; }
        public string CompanyName { get; set; }
        public string AccountNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceMonth { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string PoNumber { get; set; }
    }
}