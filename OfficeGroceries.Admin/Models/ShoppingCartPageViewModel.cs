﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class ShoppingCartPageViewModel
    {
        public ShoppingCartPageViewModel()
        {
            CartItems = new List<ShoppingCartItemViewModel>();
            CartItemsBySuppliers = new List<CartItemsBySuppliers>();
            DeliveryDays = new List<DayOfWeek>();
            LiveOnDryGoods = false;
            AskingForDryGoodsSupplier = false;
            Gross = 0;
            Net = 0;
            VAT = 0;
        }
        public void Set(Company company)
        {
            if (company != null)
            {
                LiveOnDryGoods = company.Method_IsLive(SupplierType.DryGoods);
                AskingForDryGoodsSupplier = company.SupAcctRequestLogs.Where(x => x.Supplier.DefaultAisles.Where(a => a.Name == "Dry Goods").Any()).Any();
                if (company.DryGoodsDeliveryMonday) DeliveryDays.Add(DayOfWeek.Monday);
                if (company.DryGoodsDeliveryTuesday) DeliveryDays.Add(DayOfWeek.Tuesday);
                if (company.DryGoodsDeliveryWednesday) DeliveryDays.Add(DayOfWeek.Wednesday);
                if (company.DryGoodsDeliveryThursday) DeliveryDays.Add(DayOfWeek.Thursday);
                if (company.DryGoodsDeliveryFriday) DeliveryDays.Add(DayOfWeek.Friday);
                if (company.DryGoodsDeliverySaturday) DeliveryDays.Add(DayOfWeek.Saturday);
                NextDeliveryDate = DateTime.MaxValue;/*company.NextDryGoodsDeliveryDate();*/
                CompanyID = company.ID.ToString();
            }
        }
        public List<ShoppingCartItemViewModel> CartItems { get; set; }
        public List<CartItemsBySuppliers> CartItemsBySuppliers { get; set; }
        public decimal Gross { get; set; }
        public decimal Net { get; set; }
        public decimal VAT { get; set; }
        public bool LiveOnDryGoods { get; set; }
        public bool AskingForDryGoodsSupplier { get; set; }
        public List<DayOfWeek> DeliveryDays { get; set; }
        public DateTime NextDeliveryDate { get; set; }
        public string CompanyID { get; set; }
    }

    public class QuoteProductViewModel
    {
        public Guid ProductID { get; set; }
        public int Qty { get; set; }
    }

    public class CartItemsBySuppliers
    {
        public bool Live { get; set; }
        public SupplierViewModel Supplier { get; set; }
        public List<ShoppingCartItemViewModel> CartItems { get; set; }
        public decimal SubGross { get; set; }
        public decimal SubNet { get; set; }
        public decimal SubVAT { get; set; }
    }

    public class EventDetailsViewModel
    {
        public string eventID { get; set; }
        public string eventSupplierID { get; set; }
        public string functionName { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string town { get; set; }
        public string postcode { get; set; }
        public string roomName { get; set; }
        public string numberOfGuests { get; set; }
        public DateTime eventDate { get; set; }
        public DateTime startTime { get; set; }
        public string deliveryDetails { get; set; }
        public string specialRequirements { get; set; }
    }
}