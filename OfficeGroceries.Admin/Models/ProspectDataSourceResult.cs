﻿using Kendo.Mvc.UI;

namespace NuIngredient.Models
{
    public class ProspectDataSourceResult : DataSourceResult
    {
        public decimal TotalValue { get; set; }
    }
}