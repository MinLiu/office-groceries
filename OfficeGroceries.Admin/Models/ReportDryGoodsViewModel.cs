﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ReportDryGoodsViewModel
    {
        public string ID { get; set; }
        public string OrderNumber { get; set; }
        public string OGMasterAccount { get; set; }
        public string ClientName { get; set; }
        public string OrderDate { get; set; }
        public string ProductsPurchased { get; set; }
        public string Volume { get; set; }
        public decimal Net { get; set; }
        public decimal VAT { get; set; }
        public decimal Gross { get; set; }
        public int Credit { get; set; }
        public string PoNumber { get; set; }
        public string DeliveryPostcode { get; set; }
        public string ProductCode { get; set; }
        public string Supplier { get; set; }
    }

}

