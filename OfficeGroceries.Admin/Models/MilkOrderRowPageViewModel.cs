﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class MilkOrderRowPageViewModel
    {
        private OrderMode OrderMode { get; set; }
        public List<MilkOrderViewModel> MilkOrders { get; set; }
        public List<bool> Enables { get; set; }
        private List<bool> _deliveryDays { get; set; }
        private List<DateTime> _holidays { get; set; }
        private List<DateTime> _openDays { get; set; }

        public MilkOrderRowPageViewModel(OrderMode orderMode, DateTime? weekStart, string companyID)
        {
            OrderMode = orderMode;
            MilkOrders = new List<MilkOrderViewModel>();
            Enables = new List<bool>();
            LoadDeliveryDays(companyID);
            if (orderMode == OrderMode.OneOff)
            {
                DateTime dateWeekStart = weekStart.Value;
                var dateMon = dateWeekStart.StartOfWeek(DayOfWeek.Monday);
                var dateTue = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(1);
                var dateWed = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(2);
                var dateThu = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(3);
                var dateFri = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(4);
                var dateSat = dateWeekStart.StartOfWeek(DayOfWeek.Monday).AddDays(5);

                LoadSpecialDays(dateMon, companyID);

                var dateNotAllowedBefore = DateTime.Now.AddHours(CutOffTime.RestTime).Date;
                Enables.AddRange(new List<bool>()
                {
                    dateMon > dateNotAllowedBefore && (NotHoliday(dateMon) || IsOpenDay(dateMon)) && (IsOpenDay(dateMon) || _deliveryDays[(int)OgDayOfWeek.Monday]),
                    dateTue > dateNotAllowedBefore && (NotHoliday(dateTue) || IsOpenDay(dateTue)) && (IsOpenDay(dateTue) || _deliveryDays[(int)OgDayOfWeek.Tuesday]),
                    dateWed > dateNotAllowedBefore && (NotHoliday(dateWed) || IsOpenDay(dateWed)) && (IsOpenDay(dateWed) || _deliveryDays[(int)OgDayOfWeek.Wednesday]),
                    dateThu > dateNotAllowedBefore && (NotHoliday(dateThu) || IsOpenDay(dateThu)) && (IsOpenDay(dateThu) || _deliveryDays[(int)OgDayOfWeek.Thursday]),
                    dateFri > dateNotAllowedBefore && (NotHoliday(dateFri) || IsOpenDay(dateFri)) && (IsOpenDay(dateFri) || _deliveryDays[(int)OgDayOfWeek.Friday]),
                    dateSat > dateNotAllowedBefore && (NotHoliday(dateSat) || IsOpenDay(dateSat)) && (IsOpenDay(dateSat) || _deliveryDays[(int)OgDayOfWeek.Saturday]),
                });
            }
            else
            {
                Enables.AddRange(new List<bool>()
                {
                    true, true, true, true, true, true,
                });
            }
        }

        private void LoadDeliveryDays(string companyId)
        {
            if (string.IsNullOrEmpty(companyId))
            {
                _deliveryDays = new List<bool>
                {
                    true, true, true, true, true, true
                };
                return;
            }
            
            using (var db = new SnapDbContext())
            {
                var company = db.Companies.Find(Guid.Parse(companyId));
            
                _deliveryDays = new List<bool>
                {
                    company.MilkDeliveryMonday,
                    company.MilkDeliveryTuesday,
                    company.MilkDeliveryWednesday,
                    company.MilkDeliveryThursday,
                    company.MilkDeliveryFriday,
                    company.MilkDeliverySaturday
                };
            }
        }
        
        private void LoadSpecialDays(DateTime dateFrom, string companyID)
        {
            Guid? supplierId = null;

            if (!string.IsNullOrWhiteSpace(companyID))
            {
                supplierId = SupplierHelper.GetSupplier(Guid.Parse(companyID), SupplierType.Milk, dateFrom)?.ID;
            }
            
            var dateTo = dateFrom.AddDays(6);
             _holidays = new HolidayService(new SnapDbContext()).ReadSupplierHolidays(supplierId)
                .Where(x => dateFrom <= x.Date)
                .Where(x => dateTo >= x.Date)
                .Select(x => x.Date)
                .ToList();
             
             _openDays = new SupplierOpenDayRepository().ReadSupplierOpenDays(supplierId)
                 .Where(x => dateFrom <= x.Date)
                 .Where(x => dateTo >= x.Date)
                 .Select(x => x.Date)
                 .ToList();

        }

        private bool NotHoliday(DateTime date)
        {
            return !_holidays.Contains(date.Date);
        }
        
        private bool IsOpenDay(DateTime date)
        {
            return _openDays.Contains(date.Date);
        }
        
        public bool IsAvailableDay(OgDayOfWeek dayOfWeek)
        {
            if (OrderMode == OrderMode.Draft || OrderMode == OrderMode.Regular)
            {
                return _deliveryDays[(int)dayOfWeek];
            }

            if (OrderMode == OrderMode.OneOff)
            {
                return Enables[(int)dayOfWeek];
            }
            
            throw new NotImplementedException();
        }
    }

}

