﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel: IValidatableObject
    {
        [Required]
        public string ProspectToken { get; set; }

        [Required]
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }

        [Required]
        [Display(Name = "Delivery Address")]
        public string DeliveryAddress { get; set; }

        [Required]
        [Display(Name = "Post Code")]
        public string PostCode { get; set; }

        [Display(Name = "Billing Company Name")]
        public string BillingCompanyName { get; set; }
        [Display(Name = "Billing Email")]
        public string BillingEmail { get; set; }
        
        [Display(Name = "Billing Address")]
        public string BillingAddress { get; set; }

        [Display(Name = "Post Code")]

        public string BillingPostCode { get; set; }

        [Required]
        [Display]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Delivery Instruction")]
        public string DeliveryInstruction { get; set; }

        public bool AgreeToGetEmail { get; set; }

        public string Dummy { get; set; }

        public bool UseDeliveryAddrAsBillingAddr { get; set; }
        [Required(ErrorMessage = "Please select.")]
        public bool? IsMultiTenantedBuilding { get; set; }
        [Required(ErrorMessage = "Please select.")]
        public bool? HasMultiSites { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            if (UseDeliveryAddrAsBillingAddr == false)
            {
                if (string.IsNullOrWhiteSpace(BillingCompanyName))
                {
                    results.Add(new ValidationResult("Must provide Billing Company Name", new[] { "BillingCompanyName" }));
                }
                if (string.IsNullOrWhiteSpace(BillingEmail))
                {
                    results.Add(new ValidationResult("Must provide Billing Email", new[] { "BillingEmail" }));
                }
                if (string.IsNullOrWhiteSpace(BillingAddress))
                {
                    results.Add(new ValidationResult("Must provide Billing Address", new[] { "BillingAddress" }));
                }
                if (string.IsNullOrWhiteSpace(BillingPostCode))
                {
                    results.Add(new ValidationResult("Must provide Billing postcode", new[] { "BillingPostCode" }));
                }
            }
            return results;
        }
    }

    public class RegisterMultiCompanyViewModel : IValidatableObject
    {
        [Required]
        public string ProspectToken { get; set; }

        [Required]
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }

        [Required]
        [Display(Name = "Delivery Address")]
        public string DeliveryAddress { get; set; }

        [Required]
        [Display(Name = "Post Code")]
        public string PostCode { get; set; }

        [Display(Name = "Billing Company Name")]
        public string BillingCompanyName { get; set; }
        [Display(Name = "Billing Email")]
        public string BillingEmail { get; set; }
        [Display(Name = "Billing Address")]
        public string BillingAddress { get; set; }

        [Display(Name = "Post Code")]

        public string BillingPostCode { get; set; }

        [Required]
        [Display]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Delivery Instruction")]
        public string DeliveryInstruction { get; set; }

        public bool AgreeToGetEmail { get; set; }

        public string Dummy { get; set; }

        public bool UseDeliveryAddrAsBillingAddr { get; set; }
        [Required(ErrorMessage = "Please select.")]
        public bool? IsMultiTenantedBuilding { get; set; }
        [Required(ErrorMessage = "Please select.")]
        public bool? HasMultiSites { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            if (UseDeliveryAddrAsBillingAddr == false)
            {
                if (string.IsNullOrWhiteSpace(BillingCompanyName))
                {
                    results.Add(new ValidationResult("Must provide Billing Company Name", new[] { "BillingCompanyName" }));
                }
                if (string.IsNullOrWhiteSpace(BillingEmail))
                {
                    results.Add(new ValidationResult("Must provide Billing Email", new[] { "BillingEmail" }));
                }
                if (string.IsNullOrWhiteSpace(BillingAddress))
                {
                    results.Add(new ValidationResult("Must provide Billing Address", new[] { "BillingAddress" }));
                }
                if (string.IsNullOrWhiteSpace(BillingPostCode))
                {
                    results.Add(new ValidationResult("Must provide Billing postcode", new[] { "BillingPostCode" }));
                }
            }
            return results;
        }
    }

    public class EnquiryFormViewModel
    {
        [Required]
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }

        [Required]
        [Display]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Milk")]
        public bool InterestOfMilk { get; set; }
        [Display(Name = "Fruits")]
        public bool InterestOfFruits { get; set; }
        [Display(Name = "Dry Goods")]
        public bool InterestOfDryGoods { get; set; }

        public string Note { get; set; }
        [Required]
        public string Postcode { get; set; }

    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }


    // USER MANAGEMENT CLASSES

    public class UserViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public string ID { get; set; }
        public SelectItemViewModel Company { get; set; }

        //[Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        //[Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        //public string RoleID { get; set; }
        public string Role { get; set; }
        public SelectItemViewModel UserRole { get; set; }
        public bool Confirmed { get; set; }

        
        public bool AccessCRM { get; set; } //Added but not needed
        public bool AccessProducts { get; set; } //Added but not needed
        public bool AccessQuotations { get; set; }
        public bool AccessJobs { get; set; }
        public bool AccessInvoices { get; set; }
        public bool AccessDeliveryNotes { get; set; }
        public bool AccessPurchaseOrders { get; set; }
        public bool AccessStockControl { get; set; }

        public bool AgreeToGetEmails { get; set; }
        public bool CanSwitchCompany { get; set; }

    }


    public class NewUserViewModel
    {

        public int CompanyID { get; set; }


        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //[Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        //[Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public string Role { get; set; }
        public int ConsultantID { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public bool AgreeToGetEmail { get; set; }

        public bool AccessCRM { get; set; } //Added but not needed
        public bool AccessProducts { get; set; } //Added but not needed
        public bool AccessQuotations { get; set; }
        public bool AccessJobs { get; set; }
        public bool AccessInvoices { get; set; }
        public bool AccessDeliveryNotes { get; set; }
        public bool AccessPurchaseOrders { get; set; }
        public bool AccessStockControl { get; set; }
    }


    public class ChangeUserPasswordViewModel
    {

        public string ID { get; set; }
        public int CompanyID { get; set; }
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

    }


    public class DeleteUserViewModel
    {
        public string ID { get; set; }
        public string Email { get; set; }
    }


    public class RoleViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    

    
}

