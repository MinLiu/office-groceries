﻿namespace NuIngredient.Models
{

    public class ReportZeroOrderViewModel
    {
        public string CompanyName { get; set; }
        public string AccountNumber { get; set; }
        public string SupplierName { get; set; }
    }

}

