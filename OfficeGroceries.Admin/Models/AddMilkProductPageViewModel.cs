﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class AddMilkProductPageViewModel
    {
        public Guid CompanyID { get; set; }
        public Guid OrderHeaderID { get; set; }
    }
}