﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NuIngredient.Models
{

    public class DashboardFruitOrderViewModel
    {
        public decimal WeekyTotal { get; set; }
        public List<FruitOrderViewModel> WeeklyOrder { get; set; }
    }

}

