﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ReportMilkAndFruitViewModel
    {
        public string ID { get; set; }
        public ReportType Type { get; set; }
        public string AccountNo { get; set; }
        public string ClientName { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string SupplierName { get; set; }
        public int TotalQty { get; set; }
        public decimal Total { get; set; }
        public int Credit { get; set; }
        public string Comments { get; set; }
        public DateTime OrderDate { get; set; }
    }

    public enum ReportType
    {
        Milk = 1,
        Fruit = 2,
    }

}

