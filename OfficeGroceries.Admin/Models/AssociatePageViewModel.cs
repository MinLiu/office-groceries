﻿using System;
using System.Collections.Generic;

namespace NuIngredient.Models
{
    public class AssociatePageViewModel
    {
        public Guid CompanyID { get; set; }
        public List<string> AssociatedCompanies { get; set; }
    }
}