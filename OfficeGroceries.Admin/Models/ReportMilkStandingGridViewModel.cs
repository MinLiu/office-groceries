﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ReportMilkStandingGridViewModel
    {
        public string ID { get; set; }
        public string Supplier { get; set; }
        public string AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string ProductNameCode { get; set; }
        public int Mon { get; set; }
        public int Tues { get; set; }
        public int Wed { get; set; }
        public int Thurs { get; set; }
        public int Fri { get; set; }
        public int Sat { get; set; }
    }

}

