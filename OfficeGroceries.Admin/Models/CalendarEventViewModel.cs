﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{
    public enum CalendarEventType { Milk, Fruits, DryGoods }

    public class CalendarEventViewModel : Kendo.Mvc.UI.ISchedulerEvent
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
        public CalendarEventType EventType { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string DeliveryInstruction { get; set; }
        public string SupplierName { get; set; }
        public string SupplierEmail { get; set; }
    }

}

