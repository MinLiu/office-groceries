﻿using System;

namespace NuIngredient.Models
{
    public class ReportIntermittentNextWeekViewModel
    {
        public string AccountNumber { get; set; }
        public string Supplier { get; set; }
        public string SupplierEmail { get; set; }
        public string SupplierTelephone { get; set; }
        public Guid CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPostcode { get; set; }
        public DateTime? WeekStartDate { get; set; }
        public bool IsZeroContract { get; set; }
    }

}

