﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class FruitsPageViewModel
    {
        public List<DateTime> OrderWeekDates { get; set; }
        public bool AskingForFruitSupplier { get; set; }
        public bool HasFruitSupplier { get; set; }
        public bool LiveOnFruit { get; set; }
        public bool HasRegularFruitOrder { get; set; }
        public DateTime StartDate { get; set; } = DateTime.MaxValue.AddDays(-1);
        public DateTime EndDate { get; set; } = DateTime.MaxValue;
        public bool AskingForSnackSupplier { get; set; }
        public bool HasSnackSupplier { get; set; }
        public bool LiveOnSnacks { get; set; }
        public bool HasRegularSnackOrder { get; set; }
        public DateTime SnackStartDate { get; set; } = DateTime.MaxValue.AddDays(-1);
        public DateTime SnackEndDate { get; set; } = DateTime.MaxValue;
        public decimal MinimumValue { get; set; }
        public List<FruitProductCategory> Categories { get; set; }
        public AisleViewModel Aisle { get; set; }
        public string CompanyName { get; set; }
        public FruitsPageViewModel()
        {
            OrderWeekDates = new List<DateTime>();
            MinimumValue = new SnapDbContext().Suppliers.Where(x => x.SupplierType == SupplierType.Fruits)
                                            .Where(x => x.Deleted == false)
                                            .OrderByDescending(x => x.MinimumValue)
                                            .FirstOrDefault()
                                            .MinimumValue;
            Categories = new FruitProductCategoryRepository().Read().Where(x => x.Deleted == false && x.ParentID == null).OrderBy(x => x.SortPos).ThenBy(x => x.Name).ToList();
            Aisle = new AisleMapper().MapToViewModel(new AisleRepository().Read().Where(x => x.Name == "Fruit").FirstOrDefault());
        }
        public void Set(Company company)
        {
            AskingForFruitSupplier = company.AskingForFruitSupplier != null;
            AskingForSnackSupplier = company.AskingForSnackSupplier != null;
            HasFruitSupplier = company.Method_HasSupplier(SupplierType.Fruits);
            HasSnackSupplier = company.Method_HasSupplier(SupplierType.Snacks);
            LiveOnFruit = company.Method_IsLive(SupplierType.Fruits);
            LiveOnSnacks = company.Method_IsLive(SupplierType.Snacks);
            var fruitSupplier = new CompanySupplierRepository().Read()
                                                          .Where(cs => cs.CompanyID == company.ID)
                                                          .Where(cs => cs.Supplier.SupplierType == SupplierType.Fruits)
                                                          .Where(cs => cs.Supplier.Deleted == false)
                                                          .Where(cs => cs.StartDate.HasValue)
                                                          .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                                                          .OrderBy(cs => cs.StartDate.Value)
                                                          .FirstOrDefault();
            if (fruitSupplier != null)
            {
                StartDate = fruitSupplier.StartDate ?? DateTime.MaxValue.AddDays(-1);
                EndDate = fruitSupplier.EndDate ?? DateTime.MaxValue;
            }
            HasRegularFruitOrder = new FruitOrderHeaderRepository()
                .Read()
                .Any(x => x.OrderMode == OrderMode.Regular &&
                          x.Items.Any(i => i.Fruit.FruitProductType == FruitProductType.Fruit) &&
                          x.CompanyID == company.ID);
            
            var snackSupplier = new CompanySupplierRepository().Read()
                .Where(cs => cs.CompanyID == company.ID)
                .Where(cs => cs.Supplier.SupplierType == SupplierType.Snacks)
                .Where(cs => cs.Supplier.Deleted == false)
                .Where(cs => cs.StartDate.HasValue)
                .Where(cs => !cs.EndDate.HasValue || cs.EndDate.Value >= DateTime.Today)
                .OrderBy(cs => cs.StartDate.Value)
                .FirstOrDefault();
            if (snackSupplier != null)
            {
                SnackStartDate = snackSupplier.StartDate ?? DateTime.MaxValue.AddDays(-1);
                SnackEndDate = snackSupplier.EndDate ?? DateTime.MaxValue;
            }
            HasRegularSnackOrder = new FruitOrderHeaderRepository()
                .Read()
                .Any(x => x.OrderMode == OrderMode.Regular &&
                          x.Items.Any(i => i.Fruit.FruitProductType == FruitProductType.Snack) &&
                          x.CompanyID == company.ID);
            
            CompanyName = company.Name;
        }
    }

}

