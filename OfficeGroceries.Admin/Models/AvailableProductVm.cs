using System;

namespace NuIngredient.Models
{
    public class AvailableProductVm
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public bool Available { get; set; }
    }
}