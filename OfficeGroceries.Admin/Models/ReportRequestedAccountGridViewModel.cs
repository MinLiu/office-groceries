﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NuIngredient.Models
{

    public class ReportRequestedAccountGridViewModel
    {
        public string ID { get; set; }
        public string AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public string Category { get; set; }
        public string RequestDate { get; set; }
        public string Postcode { get; set; }
        public string Notes { get; set; }
        public List<string> PotentialSuppliers { get; set; }
    }

}

