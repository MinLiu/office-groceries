﻿using System;

namespace NuIngredient.Models
{
    public class RescheduleOrderVm
    {
        public Guid SupplierId { get; set; }
        public DateTime OldDate { get; set; } = DateTime.Now;
        public DateTime NewDate { get; set; } = DateTime.Now.AddDays(7);
        public OrderReschedulePolicy Policy { get; set; } = OrderReschedulePolicy.ReplaceExisting;
    }
}