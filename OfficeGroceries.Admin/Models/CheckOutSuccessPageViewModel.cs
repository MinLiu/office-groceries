﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuIngredient.Models
{
    public class CheckOutSuccessPageViewModel
    {
        public bool IsProforma => Approver != null;
        public ProformaApproverViewModel Approver { get; set; }
        public List<SubOrder> SubOrders { get; set; }
    }

    public class SubOrder
    {
        public OneOffOrderViewModel OrderHeader { get; set; }
        public List<OneOffOrderItemViewModel> OrderItems { get; set; }
        public DateTime? DeliveryDate { get; set; }
    }
}