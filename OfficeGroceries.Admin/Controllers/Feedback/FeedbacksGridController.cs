﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    public class FeedbacksGridController : GridController<Feedback, FeedbackGridViewModel>
    {
        public FeedbacksGridController()
            : base(new FeedbackRepository(), new FeedbackGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText, Guid? companyID, string userID, int dateType, DateTime? fromDate, DateTime? toDate, bool showCompleted)
        {
            var viewModels = ReadFeedBacks(filterText, companyID, userID, dateType, fromDate, toDate, showCompleted);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult Export([DataSourceRequest] DataSourceRequest request, string filterText, Guid? companyID, string userID, int dateType, DateTime? fromDate, DateTime? toDate, bool showCompleted)
        {
            var viewModels = ReadFeedBacks(filterText, companyID, userID, dateType, fromDate, toDate, showCompleted);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"\n",
                    "Type", "Customer", "User", "Message", "Time", "Replyer", "Comment", "Response Time"
                ));

            // Append to StringBuilder.
            foreach (var q in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"\n",
                        q.FeedbackType.Name,
                        q.CompanyName,
                        q.UserName,
                        q.Message,
                        q.Created.ToString("dd/MM/yyyy HH:mm"),
                        q.Replier,
                        q.NIComment,
                        q.ResponseTime != null ? q.ResponseTime.Value.ToString("dd/MM/yyyy HH:mm") : ""
                        ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Feedback Export {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        [HttpPost]
        public List<FeedbackGridViewModel> ReadFeedBacks(string filterText, Guid? companyID, string userID, int dateType, DateTime? fromDate, DateTime? toDate, bool showCompleted)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }
            var viewModels = _repo.Read()
                              .Where(x => companyID == null || x.CompanyID == companyID)
                              .Where(x => userID == "" || x.UserID == userID)
                              .Where(x => x.Created >= fromDate && x.Created < toDate)
                              .Where(x => showCompleted == true || x.ResponseTime == null)
                              .Where(x => filterText == null
                                       || filterText == ""
                                       || x.Company.Name.ToLower().Contains(filterText.ToLower())
                                       || (x.User.FirstName.ToLower() + " " + x.User.LastName.ToLower()).Contains(filterText.ToLower())
                                       || x.Message.ToLower().Contains(filterText.ToLower()))
                              .OrderByDescending(x => x.Created)
                              .ToList()
                              .Select(x => _mapper.MapToViewModel(x));
            return viewModels.ToList();
        }
    }

}