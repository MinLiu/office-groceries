﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class MilkProductGridController : GridController<MilkProduct, MilkProductGridViewModel>
    {

        public MilkProductGridController()
            : base(new MilkProductRepository(), new MilkProductGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? categoryID, string filterText, bool showInactive = false)
        {
            filterText = filterText.ToLower();
            var models = _repo.Read()
                              .Where(x => x.Deleted == false)
                              .Where(x => categoryID == null || x.Categories.Any(c => c.CategoryID == categoryID))
                              .Where(x => filterText == "" || x.ProductCode.ToLower().Contains(filterText) || x.Name.ToLower().Contains(filterText) || x.ShortDescription.ToLower().Contains(filterText));

            if (!showInactive)
            {
                models = models.Where(x => x.IsActive == true);
            }
            
            if (categoryID != null)
            {
                models = models.OrderBy(x => x.Categories.Where(c => c.CategoryID == categoryID).Select(c => c.SortPos).FirstOrDefault());
            }
            else
            {
                models = models
                    .OrderBy(x => x.Categories.Select(c => c.Category.SortPos).FirstOrDefault())
                    .ThenBy(x => x.SortPosition);
            }

            var viewModels = models.Skip((request.Page - 1) * request.PageSize)
                                   .Take(request.PageSize)
                                   .ToList()
                                   .Select(s => _mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = models.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult _UpdateProductPositions(Guid productID, int oldIndex, int newindex, Guid categoryID)
        {
            var milkCateRepo = new MilkCategoryLinkRepository();
            var productCategories = milkCateRepo.Read()
                .Where(x => x.MilkProduct.Deleted == false)
                .Where(x => x.CategoryID == categoryID)
                .OrderBy(x => x.SortPos)
                .ToList();

            var targetProduct = productCategories.Where(x => x.MilkProductID == productID).FirstOrDefault();

            productCategories.Remove(targetProduct);

            productCategories.Insert(newindex >= 0 ? newindex : 0, targetProduct);

            int sortPos = 0;
            foreach (var category in productCategories)
            {
                category.SortPos = sortPos++;
                milkCateRepo.Update(category);
            }

            return Json("Success");
        }

        [HttpGet]
        public ActionResult DropDown()
        {
            var viewModels = _repo.Read()
                .Where(s => s.Deleted == false)
                .ToList()
                .Select(s => new SelectItemViewModel()
                {
                    ID = s.ID.ToString(),
                    Name = s.Name,
                });
            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }
    }
}