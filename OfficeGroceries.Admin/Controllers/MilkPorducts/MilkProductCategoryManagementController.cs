﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize(Roles="NI-User")]
    public class MilkProductCategoryManagementController : GridController<MilkProductCategory, MilkProductCategoryViewModel>
    {

        public MilkProductCategoryManagementController()
            : base(new MilkProductCategoryRepository(), new MilkProductCategoryMapper())
        {

        }

        // GET: StockLocations
        public ActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, MilkProductCategoryViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _mapper.MapToModel(viewModel);
                _repo.Create(model);
                viewModel.ID = model.ID.ToString();
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid aisleID)
        {
            var result = _repo.Read()
                              .Where(x => x.Deleted != true)
                              .Where(x => x.AisleID == aisleID)
                              .OrderBy(x => x.SortPos).ThenBy(x =>x.Name)
                              .ToList()
                              .Select(x => _mapper.MapToViewModel(x))
                              .ToTreeDataSourceResult(
                                    request,
                                    e => e.ID,
                                    e => e.ParentID,
                                    e => e
                              );
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}