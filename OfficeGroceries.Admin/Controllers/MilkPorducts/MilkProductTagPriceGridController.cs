﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class MilkProductTagPriceGridController : GridController<MilkProductTagPrice, MilkProductTagPriceViewModel>
    {

        public MilkProductTagPriceGridController()
            : base(new MilkProductTagPriceRepository(), new MilkProductTagPriceMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid productID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.ProductID == productID)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, MilkProductTagPriceViewModel viewModel)
        {
            var result = base.Create(request, viewModel);

            RefreshMilkPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.ProductID));

            return result;
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, MilkProductTagPriceViewModel viewModel)
        {
            var result = base.Update(request, viewModel);

            RefreshMilkPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.ProductID));

            return result;
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, MilkProductTagPriceViewModel viewModel)
        {
            var result = base.Destroy(request, viewModel);

            RefreshMilkPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.ProductID));

            return result;
        }
    }
}