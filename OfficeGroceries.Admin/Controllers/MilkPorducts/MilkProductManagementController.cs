﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class MilkProductManagementController : EntityController<MilkProduct, EditMilkProductViewModel>
    {

        public MilkProductManagementController()
            : base(new MilkProductRepository(), new EditMilkProductModelMapper())
        {

        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _New()
        {
            var model = new MilkProduct();
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _New(EditMilkProductViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            viewModel.ID = model.ID.ToString();

            UpdateCategories(viewModel);

            return Json(new { ID = model.ID });
        }

        [HttpGet]
        public ActionResult _Edit(Guid id)
        {
            var viewModel = _mapper.MapToViewModel(_repo.Find(id));
            return PartialView(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _Edit(EditMilkProductViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _repo.Find(Guid.Parse(viewModel.ID));

            var isPriceChanged = model.Price != viewModel.Price || model.VAT != viewModel.VAT;

            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            if (isPriceChanged)
            { 
                var helper = RefreshMilkPriceHelper.New();
                helper.RefreshOrderPrice(model.ID);
            }

            UpdateCategories(viewModel);

            viewModel.ImageURL = model.ImageURL;

            ViewBag.Saved = true;

            return PartialView(viewModel);
        }

        public ActionResult UploadProductImage(HttpPostedFileBase uploadedProductImage, string id)
        {
            const string relativePath = "/Images/";

            if (uploadedProductImage == null) return Json(new { status = "error", message = "No Image uploaded." }, "text/plain");
            if (!uploadedProductImage.ContentType.Contains("image")) return Json(new { status = "error", message = "Uploaded file is not an Image." }, "text/plain");
            if (uploadedProductImage.ContentLength > 10 * 1024 * 1024) return Json(new { status = "error", message = "Uploaded file is too big." }, "text/plain");


            var model = _repo.Find(Guid.Parse(id));

            //Save file on Server
            string loc = SaveFile(uploadedProductImage, relativePath);

            //Update the image URL of the product    
            model.ImageURL = loc;
            _repo.Update(model, new string[] { "ImageURL" });

            //return the image loc. Javascript updates the image in view
            return Json(new { status = "success", id = id }, "text/plain");

        }

        public ActionResult RemoveProductImage(string id)
        {
            var model = _repo.Find(Guid.Parse(id));
            model.ImageURL = null;
            _repo.Update(model, new string[] { "ImageURL" });

            return Json(new { status = "success", id = id }, "text/plain");
        }

        private void UpdateCategories(EditMilkProductViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.Categories)
                             .FirstOrDefault();

            var oldList = model != null ? model.Categories.Select(x => x.CategoryID.ToString()) : new List<string>();
            var newList = viewModel.CategoryIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new MilkCategoryLinkRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.CategoryID.ToString() == id && x.MilkProductID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new MilkCategoryLink() { CategoryID = Guid.Parse(id), MilkProductID = Guid.Parse(viewModel.ID) });
            }
        }
    }
}