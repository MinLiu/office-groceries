﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net.Mail;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class AisleManagementController : EntityController<Aisle, AisleViewModel>
    {

        public AisleManagementController()
            : base(new AisleRepository(), new AisleMapper())
        {

        }

        // GET: AisleManagement
        public ActionResult Index(string ID)
        {
            return View();
        }

        [HttpGet]
        public ActionResult _New()
        {
            return PartialView(new AisleViewModel() { SortPosition = _repo.Read().OrderByDescending(x => x.SortPosition).FirstOrDefault().SortPosition + 1 });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _New(AisleViewModel viewModel)
        {
            if (!ModelState.IsValid) return PartialView(viewModel);

            var model = new Aisle();
            model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            UpdateInternalName(model);

            return Json(new { ID = model.ID });
        }

        [HttpGet]
        public ActionResult _Edit(Guid ID)
        {
            var model = _repo.Find(ID);
            var viewModel = _mapper.MapToViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _Edit(AisleViewModel viewModel)
        {
            if (!ModelState.IsValid) return PartialView(viewModel);

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            UpdateInternalName(model);

            ViewBag.Saved = true;
            return PartialView(viewModel);
        }

        public ActionResult UploadAisleImage(HttpPostedFileBase uploadedAisleImage, string id)
        {
            const string relativePath = "/Images/";

            if (uploadedAisleImage == null) return Json(new { status = "error", message = "No Image uploaded." }, "text/plain");
            if (!uploadedAisleImage.ContentType.Contains("image")) return Json(new { status = "error", message = "Uploaded file is not an Image." }, "text/plain");
            if (uploadedAisleImage.ContentLength > 10 * 1024 * 1024) return Json(new { status = "error", message = "Uploaded file is too big." }, "text/plain");


            var model = _repo.Find(Guid.Parse(id));

            //Save file on Server
            var result = SaveImg(uploadedAisleImage, relativePath);

            //Update the image URL of the product    
            model.AisleImageUrl = result.Path;
            _repo.Update(model, new string[] { "AisleImageUrl" });

            //return the image loc. Javascript updates the image in view
            return Json(new { status = "success", id = id }, "text/plain");
        }

        public ActionResult RemoveAisleImage(string id)
        {
            var model = _repo.Find(Guid.Parse(id));
            try
            {
                DeleteFile(model.AisleImageUrl);
            }
            catch { }
            model.AisleImageUrl = null;
            _repo.Update(model, new string[] { "AisleImageUrl" });

            return Json(new { status = "success", id = id }, "text/plain");
        }

        private void UpdateInternalName(Aisle aisle)
        {
            var internalName = InternalNameHelper.Convert(aisle.Label);

            var finalInternalName = internalName;
            var postFix = 2;

            while (_repo.Read().Where(p => p.ID != aisle.ID).Where(p => p.InternalName == finalInternalName).Any())
            {
                finalInternalName = finalInternalName + postFix++;
            }

            if (aisle.InternalName != finalInternalName)
            {
                aisle.InternalName = finalInternalName;
                _repo.Update(aisle, new string[] { "InternalName" });
            }

        }
    }
    
}