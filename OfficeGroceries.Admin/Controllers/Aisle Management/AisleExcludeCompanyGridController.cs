﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class AisleExcludeCompanyGridController : GridController<AisleExcludeCompany, AisleExcludeCompanyGridViewModel>
    {

        public AisleExcludeCompanyGridController()
            : base(new AisleExcludeCompanyRepository(), new AisleExcludeCompanyGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid aisleID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.AisleID == aisleID)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

    }
}