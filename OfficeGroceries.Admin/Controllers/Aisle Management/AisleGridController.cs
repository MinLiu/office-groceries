﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net.Mail;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class AisleGridController : GridController<Aisle, AisleGridViewModel>
    {

        public AisleGridController()
            : base(new AisleRepository(), new AisleGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText)
        {
            var models = _repo.Read().Where(x => filterText == null || filterText == "" || x.Name.ToLower().Contains(filterText.ToLower()));
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            if (!request.Sorts.Any())
            {
                request.Sorts.Add(new Kendo.Mvc.SortDescriptor { Member = "SortPosition", SortDirection = System.ComponentModel.ListSortDirection.Ascending });
            }
            return Json(viewModels.ToDataSourceResult(request));
        }


        [HttpPost]
        public ActionResult _UpdateAislePositions(Guid aisleID, int oldIndex, int newindex)
        {
            var model = _repo.Find(aisleID);

            var aisles = _repo.Read().Where(x => x.Deleted == false).OrderBy(x => x.SortPosition).ThenBy(x => x.Name).ToList();

            aisles.Remove(model);

            var top = newindex - 1 >= 0 ? aisles.ElementAt(newindex - 1) : null;
            var bottom = newindex < aisles.Count ? aisles.ElementAt(newindex) : null;

            aisles.Insert(newindex >= 0 ? newindex : 0, model);

            int sortPos = 0;
            foreach (var item in aisles)
            {
                item.SortPosition = sortPos++;
            }

            _repo.Update(aisles);

            return Json("Success");
        }
    }
    
}