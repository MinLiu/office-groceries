﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net.Mail;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class AisleMoreInfoContentGridController : GridController<AisleMoreInfoContent, AisleMoreInfoContentGridViewModel>
    {

        public AisleMoreInfoContentGridController()
            : base(new AisleMoreInfoContentRepository(), new AisleMoreInfoContentGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid aisleID)
        {
            var models = _repo.Read().Where(x => x.AisleID == aisleID).ToList();
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }


        public ActionResult _UpdateAisleMoreInfoPositions(Guid id, int oldIndex, int newindex)
        {
            var model = _repo.Find(id);

            var MoreInfos = _repo.Read().Where(x => x.AisleID == model.AisleID).OrderBy(x => x.SortPosition).ToList();

            MoreInfos.Remove(model);

            var top = newindex - 1 >= 0 ? MoreInfos.ElementAt(newindex - 1) : null;
            var bottom = newindex < MoreInfos.Count ? MoreInfos.ElementAt(newindex) : null;

            MoreInfos.Insert(newindex >= 0 ? newindex : 0, model);

            int sortPos = 0;
            foreach (var item in MoreInfos)
            {
                item.SortPosition = sortPos++;
            }

            _repo.Update(MoreInfos);

            return Json("Success");
        }
    }
    
}