﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ProspectsGridController : GridController<Prospect, ProspectGridViewModel>
    {
                
        public ProspectsGridController()
            : base(new ProspectRepository(), new ProspectGridMapper())
        {

        }
        
        private static Dictionary<string, ProspectValueObject> _cachedProspectValue = new Dictionary<string, ProspectValueObject>();

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText, int dateType, DateTime? fromDate, DateTime? toDate, bool existingCustomer = false, bool getQuoteValue = true)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            var models = _repo.Read()
                                  .Where(x => x.Deleted == false)
                                  .Where(x => x.Created >= fromDate && x.Created < toDate);
            
            if (existingCustomer)
            {
                var exstingCustomers = new CompanyRepository().Read().Select(x => x.Email).ToList();
                models = models.Where(x => exstingCustomers.Contains(x.Email));
            }

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                models = models
                    .Where(x => (x.CompanyName + " " + x.FirstName + " " + x.LastName).Contains(filterText));
            }

            var prospectTokens = models.Select(x => x.ProspectToken).ToList();

            // var viewModels = models.Skip(request.PageSize * (request.Page - 1))
            //                        .Take(request.PageSize)
            //                        .ToList()
            //                        .Select(s => _mapper.MapToViewModel(s));

            var viewModels = models
                .OrderByDescending(x => x.Created)
                .Skip(request.PageSize * (request.Page - 1))
                .Take(request.PageSize)
                .Select(x => new ProspectGridViewModel
                {
                    ID = x.ID.ToString(),
                    ProspectToken = x.ProspectToken,
                    Name = x.FirstName,
                    Email = x.Email,
                    Phone = x.Phone,
                    CompanyName = x.CompanyName,
                    Created = x.Created,
                    Postcode = x.Postcode,
                    Note = x.Note,
                    DateDeleted = x.DateDeleted,
                    CountQuoteEmail = x.CountQuoteEmail,
                    CountPromotionEmail = x.CountPromotionEmail
                })
                .ToList();

            if (getQuoteValue)
            {
                var pTokens = viewModels.Select(x => x.ProspectToken).ToList();
                
                var milkOrders = GetProspectMilkOrders(pTokens);
                var fruitOrders = GetProspectFruitOrders(pTokens);
                var dryGoodOrders = GetProspectDryGoodOrders(pTokens);
                foreach (var prospect in viewModels)
                {
                    prospect.TotalValue = milkOrders.Where(x => x.ProspectToken == prospect.ProspectToken).Select(x => x.Value).Sum()
                        + fruitOrders.Where(x => x.ProspectToken == prospect.ProspectToken).Select(x => x.Value).Sum()
                        + dryGoodOrders.Where(x => x.ProspectToken == prospect.ProspectToken).Select(x => x.Value).Sum();
                }
            }
            
            var result = new ProspectDataSourceResult()
            {
                Data = viewModels,
                Total = models.Count(),
                // TotalValue = GetTotalProspectValue(prospectTokens)
            };

            return Json(result);
        }

        public decimal TotalProspectValue(string filterText, int dateType, DateTime? fromDate, DateTime? toDate, bool existingCustomer = false)
        {
            var cacheKey = $"TotalProspectValue_{filterText}_{dateType}_{fromDate}_{toDate}_{existingCustomer}";
            
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            var exstingCustomers = new CompanyRepository().Read().Select(x => x.Email).ToList();

            var models = _repo.Read()
                .Where(x => x.Deleted == false)
                .Where(x => filterText == "" || (x.CompanyName + " " + x.FirstName + " " + x.LastName).ToLower().Contains(filterText.ToLower()))
                .Where(x => x.Created >= fromDate && x.Created < toDate)
                .Where(x => existingCustomer == false || exstingCustomers.Contains(x.Email))
                .OrderByDescending(x => x.Created);

            var prospectTokens = models.Select(x => x.ProspectToken).ToList();

            return GetTotalProspectValue(prospectTokens, cacheKey);
        }

        private class Test
        {
            public string ProspectToken { get; set; }
            public decimal Value { get; set; }
        }

        private class ProspectValueObject
        {
            public DateTime Date { get; set; }
            public decimal Value { get; set; }
        }
        
        private List<Test> GetProspectMilkOrders(List<string> prospectTokens)
        {
            return new SnapDbContext().MilkOrderHeaders
                .Where(x => prospectTokens.Contains(x.ProspectToken))
                .Select(x => new Test
                {
                    ProspectToken = x.ProspectToken,
                    Value = x.TotalNet
                }).ToList();
        }
        
        private List<Test> GetProspectFruitOrders(List<string> prospectTokens)
        {
            return new SnapDbContext().FruitOrderHeaders
                .Where(x => prospectTokens.Contains(x.ProspectToken))
                .Select(x => new Test
                {
                    ProspectToken = x.ProspectToken,
                    Value = x.TotalNet
                }).ToList();
        }
        
        private List<Test> GetProspectDryGoodOrders(List<string> prospectTokens)
        {
            return new SnapDbContext().ShoppingCartItems
                .Where(x => prospectTokens.Contains(x.ProspectToken))
                .Where(x => x.Product != null)
                .Select(x => new Test
                {
                    ProspectToken = x.ProspectToken,
                    Value = x.Product.Price * x.Qty
                }).ToList();
        }
        
        private decimal GetTotalProspectValue(List<string> prospectTokens, string cacheKey)
        {
            if (_cachedProspectValue.TryGetValue(cacheKey, out var value) && value.Date == DateTime.Today)
            {
                return value.Value;
            }
            
            var milkTotal = new SnapDbContext().MilkOrderHeaders
                .Where(x => prospectTokens.Contains(x.ProspectToken))
                .Select(x => x.TotalNet)
                .DefaultIfEmpty(0)
                .Sum();
            
            var fruitTotal = new SnapDbContext().FruitOrderHeaders
                .Where(x => prospectTokens.Contains(x.ProspectToken))
                .Select(x => x.TotalNet)
                .DefaultIfEmpty(0)
                .Sum();

            var drygoodsTotal = new SnapDbContext().ShoppingCartItems
                .Where(x => prospectTokens.Contains(x.ProspectToken))
                .Where(x => x.Product != null)
                .Select(x => x.Product.Price * x.Qty)
                .DefaultIfEmpty(0)
                .Sum();
            
            var total = milkTotal + fruitTotal + drygoodsTotal;
            
            _cachedProspectValue[cacheKey] = new ProspectValueObject
            {
                Date = DateTime.Today,
                Value = total
            };
            
            return total;
        }

        [HttpPost]
        public JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request, string filterText/*, int dateType, DateTime? fromDate, DateTime? toDate*/)
        {
            //switch (dateType)
            //{
            //    case 1: //Current Month
            //        fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
            //        toDate = fromDate.Value.AddMonths(1);
            //        break;
            //    case 2: //Last Month
            //        fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
            //        toDate = fromDate.Value.AddMonths(1);
            //        break;
            //    case 3: //Custom Date
            //        fromDate = fromDate.Value.Date;
            //        toDate = toDate.Value.AddDays(1);
            //        break;
            //    default:
            //        toDate = DateTime.Today.AddYears(20);
            //        fromDate = DateTime.Today.AddYears(-20);
            //        break;
            //}

            //var exstingCustomers = new CompanyRepository().Read().Select(x => x.Email).ToList();

            var models = _repo.Read()
                                  .Where(x => x.Deleted == true)
                                  .Where(x => filterText == "" || (x.CompanyName + " " + x.FirstName + " " + x.LastName).ToLower().Contains(filterText.ToLower()))
                                  //.Where(x => x.Created >= fromDate && x.Created < toDate)
                                  .OrderByDescending(x => x.Created);


            var viewModels = models.Skip(request.PageSize * (request.Page - 1))
                                   .Take(request.PageSize)
                                   .ToList()
                                   .Select(s => _mapper.MapToViewModel(s));

            DataSourceResult result = new DataSourceResult() { Data = viewModels, Total = models.Count() };

            return Json(result);
        }


        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, ProspectGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _repo.Find(Guid.Parse(viewModel.ID));
                _repo.Delete(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult Delete([DataSourceRequest] DataSourceRequest request, ProspectGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _repo.Find(Guid.Parse(viewModel.ID));
                model.Deleted = true;
                model.DateDeleted = DateTime.Now;
                _repo.Update(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Restore(Guid id)
        {
            var model = _repo.Find(id);
            model.Deleted = false;
            model.DateDeleted = null;
            _repo.Update(model);
            return OkResult();
        }
    }

}