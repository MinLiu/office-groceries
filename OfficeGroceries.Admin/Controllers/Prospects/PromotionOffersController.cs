﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class PromotionOffersController : GridController<PromotionOffer, PromotionOfferViewModel>
    {

        public PromotionOffersController()
            : base(new PromotionOfferRepository(), new PromotionOfferMapper())
        {

        }

        [HttpGet]
        public ActionResult Edit()
        {
            var model = _repo.Read().FirstOrDefault();
            var viewModel = _mapper.MapToViewModel(model);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(PromotionOfferViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _repo.Find(Guid.Parse(viewModel.ID));

                _mapper.MapToModel(viewModel, model);

                _repo.Update(model);

                ViewBag.Saved = true;
            }

            return View(viewModel);
        }
    }
}