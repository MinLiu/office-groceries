﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class ProspectManagementController : EntityController<Prospect, ProspectViewModel>
    {
                
        public ProspectManagementController()
            : base(new ProspectRepository(), new ProspectMapper())
        {

        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "NI-User")]
        public ActionResult Deleted()
        {
            if (User.Identity.Name == "bustin@nu-ingredient.com" || User.Identity.Name == "developer@fruitfulgroup.com")
                return View();

            return RedirectToAction("Index", "Dashboard");
        }

        public ActionResult _ProspectQuote(Guid id)
        {
            var model = _repo.Find(id);

            return PartialView(model);
        }

        public ActionResult SendQuoteMail(Guid id)
        {
            var model = _repo.Find(id);

            var mailMessage = ProspectQuoteEmailHelper.GetEmail(model, out List<string> categoryList);
            bool success = EmailService.SendEmail(mailMessage, Server);

            if (!success)
                return Json(new { Status = "Fail" });

            model.CountQuoteEmail += 1;
            _repo.Update(model);
            
            return Json(new { Status = "Success", Message = "Email sent." });
        }

        public ActionResult SendPromotionalQuoteMail(Guid id)
        {
            var model = _repo.Find(id);

            var mailMessage = ProspectQuotePromotionalEmailHelper.GetEmail(model, out List<string> categoryList);
            bool success = EmailService.SendEmail(mailMessage, Server);

            if (!success)
                return Json(new { Status = "Fail" });

            model.CountPromotionEmail += 1;
            _repo.Update(model);
            
            return Json(new { Status = "Success", Message = "Email sent." });
        }

        public ActionResult MoveToProspect(Guid id)
        {
            using (var context = new SnapDbContext())
            {
                var enquiry = context.Enquiries.Where(x => x.ID == id).FirstOrDefault();
                context.Prospects.Add(new Prospect()
                {
                    ID = Guid.NewGuid(),
                    CompanyName = enquiry.BusinessName,
                    FirstName = enquiry.FirstName,
                    LastName = enquiry.LastName,
                    Email = enquiry.Email,
                    Created = DateTime.Now,
                    Phone = enquiry.PhoneNumber,
                    ProspectToken = Guid.NewGuid().ToString(),
                    Postcode = enquiry.Postcode,
                    Note = enquiry.Note
                });
                context.Enquiries.Remove(enquiry);
                context.SaveChanges();
            }
            return Json(new { Status = "Success" });
        }

        public async Task<ActionResult> CreateAccount(Guid id)
        {
            var model = _repo.Find(id);

            var user = new User
            {
                UserName = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Company = new Company
                {
                    ID = Guid.NewGuid(),
                    Name = model.CompanyName,
                    Email = model.Email,
                    Telephone = model.Phone,
                    SignupDate = DateTime.Now,
                    DefaultCurrencyID = 1,
                    MilkDeliveryMonday = true,
                    MilkDeliveryTuesday = true,
                    MilkDeliveryWednesday = true,
                    MilkDeliveryThursday = true,
                    MilkDeliveryFriday = true,
                    MilkDeliverySaturday = true,
                    MilkDeliverySunday = false,
                    FruitsDeliveryMonday = true,
                    FruitsDeliveryTuesday = true,
                    FruitsDeliveryWednesday = true,
                    FruitsDeliveryThursday = true,
                    FruitsDeliveryFriday = true,
                    FruitsDeliverySaturday = true,
                    FruitsDeliverySunday = false,
                    DryGoodsDeliveryMonday = true,
                    DryGoodsDeliveryTuesday = true,
                    DryGoodsDeliveryWednesday = true,
                    DryGoodsDeliveryThursday = true,
                    DryGoodsDeliveryFriday = true,
                    DryGoodsDeliverySaturday = true,
                }
            };

            var password = Guid.NewGuid().ToString().Substring(0, 8);

            var result = await UserManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                UserManager.AddToRole(user.Id, "Admin");

                string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                string message = String.Format("Hello,\n\nPlease click the following link to confirm your Office Groceries account:\n\n<a href=\"{0}\">{0}</a>", callbackUrl);
                message += String.Format("\n\nUsername: {0}\nPassword: {1}", model.Email, password);
                message += String.Format("\n\nIf you received this message by mistake just delete it without clicking the link.\n\nNeed help? Contact Us on through our website");
                message += String.Format("\n\n\n<hr />This is an automated message, please do not reply to this email address. You can contact us via our website. <a href=\"www.snap-suite.com\">www.snap-suite.com</a>");

                EmailService.SendEmail(user.Email, "Confirm your Office Groceries Account", message, Server);

                try
                {
                    string newUserMessage = String.Format("Someone has signup for a Office Groceries account. \n\nBusiness Name:{0}\nName: {1} {2}\nEmail: {3}\nPhone:{4}",
                        model.CompanyName,
                        model.FirstName,
                        model.LastName,
                        model.Email,
                        model.Phone
                    );

                    EmailService.SendEmail("min@fruitfulgroup.com", "New Office Groceries User Signup", newUserMessage, Server);

                }
                catch { }

                // Move Order From Prospect to This User if there's a prospectToken and Delete Prospect
                if (!string.IsNullOrWhiteSpace(model.ProspectToken))
                {
                    new AccountController().MoveProspectOrderToUser(model.ProspectToken, user.Company.ID);
                    var prospectRepo = new ProspectRepository();
                    var prospect = prospectRepo.Read().Where(x => x.ProspectToken == model.ProspectToken).FirstOrDefault();
                    if (prospect != null)
                        prospectRepo.Delete(prospect);
                }

                return Json(new { Status = "Success" });
            }
            else
            {
                return Json(new { Status = "Fail", Message = result.Errors.First().ToString() });
            }
        }

    }

}