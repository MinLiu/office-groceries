﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Web.Hosting;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ProspectsController : EntityController<Prospect, ProspectViewModel>
    {
                
        public ProspectsController()
            : base(new ProspectRepository(), new ProspectMapper())
        {

        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult _ProspectForm()
        {
            return PartialView(_mapper.MapToViewModel(new Prospect()));
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(true)]
        public ActionResult _SendQuote(ProspectViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _mapper.MapToModel(viewModel);
                model.Created = DateTime.Now;
                model.ProspectToken = ProspectToken;
                _repo.Create(model);

                var mailMessage = ProspectQuoteEmailHelper.GetEmail(model, out List<string> categoryList);
                bool success = EmailService.SendEmail(mailMessage, Server);

                // change a new prospect token;
                ProspectToken = Guid.NewGuid().ToString();
                HttpCookie cookie = Request.Cookies["ProspectToken"];
                cookie.Value = ProspectToken;
                Response.Cookies.Add(cookie);

                return Json(new
                {
                    success = true,
                    message = String.Format("Hello {0}\n\n" +
                                            "Office-Groceries have just sent you an e-mail with your quote for our {1} service to your business.\n\n" +
                                            "To activate your account simply click the link in the body of the e-mail or alternatively contact us on 0345 463 8863.\n\n" +
                                            "Thank you from the Office-Groceries team"
                                            , viewModel.FirstName
                                            , String.Join(", ", categoryList))
                });
            }
            return Json(new { success = false });
        }

        //[AllowAnonymous]
        //public ActionResult _GetQuoteCart()
        //{
        //    var shoppingCartRepo = new ShoppingCartItemRepository();
        //    var fruitOrderRepo = new FruitOrderHeaderRepository();
        //    var milkOrderRepo = new MilkOrderHeaderRepository();

        //    var milkQuote = milkOrderRepo.Read().Where(x => x.ProspectToken == ProspectToken).Include(x => x.Items).ToList();
        //    var fruitQuote = fruitOrderRepo.Read().Where(x => x.ProspectToken == ProspectToken).Include(x => x.Items).ToList();
        //}

        #region Send Quote Email
        
        // TO CUSTOMER 1
        public bool SendQuoteEmail(Prospect prospect, out List<string> categoryList)
        {
            var mailMessage = ProspectQuoteEmailHelper.GetEmail(prospect, out categoryList);

            EmailService.SendEmail(mailMessage, Server);

            return true;
        }

        #endregion
    }

}