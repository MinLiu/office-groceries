﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class EmailAttachmentsController : GridController<EmailAttachment, EmailAttachmentGridViewModel>
    {
        public EmailAttachmentsController()
            : base(new EmailAttachmentRepository(), new EmailAttachmentGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid referenceID)
        {
            var models = _repo.Read().Where(a => a.ReferenceID == referenceID);

            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        /// <summary>
        /// For emails to supplier
        /// </summary>
        /// <param name="uploadedAttachments"></param>
        /// <returns></returns>
        public ActionResult UploadAttachment(IEnumerable<HttpPostedFileBase> uploadedAttachments, string supplierName, string customerName, Guid referenceID)
        {
            var toCreate = new List<EmailAttachment>();
            if (uploadedAttachments != null)
            {
                foreach (var file in uploadedAttachments)
                {
                    var loc = SaveFile(
                        file, 
                        $"/EmailAttachments/{DateTime.Now:yy-MM-dd}/{supplierName.Trim()}/{customerName.Trim()}/{referenceID}/");
                    
                    var newAttachment = new EmailAttachment()
                    {
                        ReferenceID = referenceID,
                        FileName = file.FileName,
                        FilePath = loc,
                        Created = DateTime.Now,
                    };
                    toCreate.Add(newAttachment);
                }
                _repo.Create(toCreate);
            }

            return Json(toCreate);
        }

        public ActionResult DeleteAttachment(Guid ID)
        {
            var model = _repo.Find(ID);
            DeleteFile(model.FilePath);
            _repo.Delete(model);
            return Json(new { Success = true });
        }

    }

}