﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ReportZeroFruitOrdersController : BaseController
    {
        public JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var viewModels = GetReport();
            return Json(viewModels.ToDataSourceResult(request));
        }

        //public ActionResult ExportCSV()
        //{
        //    var viewModels = GetReport();

        //    StringBuilder builder = new StringBuilder();
        //    builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\"\n",
        //            "Company Name", "Account Number", "Supplier Name"
        //        ));

        //    // Append to StringBuilder.
        //    foreach (var item in viewModels)
        //    {
        //        builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\"\n",
        //            item.CompanyName,
        //            item.AccountNumber,
        //            item.SupplierName
        //        ));
        //    }

        //    var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
        //    var contentType = "text/csv";
        //    var fileDownloadName = string.Format("Zero Fruit Order {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

        //    return File(fileContent, contentType, fileDownloadName);
        //}

        private List<ReportZeroOrderViewModel> GetReport()
        {
            var resultList = new List<ReportZeroOrderViewModel>();
            using (var context = new SnapDbContext())
            {
                var companyList = new CompanyRepository(context).Read().Where(c => c.DeleteData == false).ToList();

                var FruitOrderService = new FruitOrderService<FruitOrderHeaderViewModel>(null, context);
                foreach (var company in companyList)
                {
                    if (!company.Method_IsLive(SupplierType.Fruits)) continue;

                    var regularOrder = FruitOrderService.GetCustomerOrder(company.ID, OrderMode.Regular, null);
                    if (regularOrder == null || regularOrder.TotalGross == 0)
                    {
                        resultList.Add(new ReportZeroOrderViewModel
                        {
                            CompanyName = company.Name,
                            AccountNumber = company.AccountNo,
                            SupplierName = company.FruitSupplier(DateTime.Today)?.Select(x => x.Name).FirstOrDefault()
                        });
                    }
                }
            }

            return resultList;
        }

    }

}