﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class CalendarReportsController : BaseController
    {
        
        public JsonResult ReadRequestedAccounts([DataSourceRequest] DataSourceRequest request, string companyID)
        {
            var viewModels = GetRequestedAccountList(companyID);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportRequestAccountCSV(string companyID)
        {
            var viewModels = GetRequestedAccountList(companyID);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                    "Account Number", "Company Name", "Category", "Request Date", "Postcode", "Notes", "Suppliers"
                ));

            // Append to StringBuilder.
            foreach (var item in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"",
                    item.AccountNumber,
                    item.CompanyName,
                    item.Category,
                    item.RequestDate,
                    item.Postcode,
                    item.Notes?.Replace("\"", "'")
                ));

                if (item.Category == "Milk")
                {
                    foreach (var supplier in item.PotentialSuppliers)
                    {
                        builder.Append($",\"{supplier}\"");
                    }

                }
                else if (item.Category == "Fruit")
                {
                    foreach (var supplier in item.PotentialSuppliers)
                    {
                        builder.Append($",\"{supplier}\"");
                    }
                }

                builder.Append("\n");
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Requested Accounts {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportRequestedAccountGridViewModel> GetRequestedAccountList(string companyID)
        {
            using (var context = new SnapDbContext())
            {
                var companies = context.Companies
                    .Where(x => x.AskingForFruitSupplier != null || x.AskingForSnackSupplier != null || x.AskingForMilkSupplier != null || x.SupAcctRequestLogs.Any())
                    .Where(x => companyID == null || companyID == "" || x.ID.ToString() == companyID)
                    .ToList();

                var results = new List<ReportRequestedAccountGridViewModel>();
                foreach (var company in companies)
                {
                    var helper = new PotentialSupplierHelper();

                    if (company.AskingForMilkSupplier != null &&
                        company.CompanySuppliers.All(x => x.Supplier.SupplierType != SupplierType.Milk || (x.Supplier.SupplierType == SupplierType.Milk && x.EndDate != null)))
                    {
                        var potentialSuppliers = helper.GetPotentialMilkSuppliers(company.Postcode).Select(s => s.Name).ToList();
                        results.Add(new ReportRequestedAccountGridViewModel()
                        {
                            ID = Guid.NewGuid().ToString(),
                            AccountNumber = company.AccountNo,
                            RequestDate = company.AskingForMilkSupplier.Value.ToString("dd/MM/yyyy"),
                            CompanyName = company.Name,
                            Category = "Milk",
                            Postcode = company.Postcode,
                            Notes = company.Round,
                            PotentialSuppliers = potentialSuppliers,
                        });
                    }
                    if (company.AskingForFruitSupplier != null &&
                        company.CompanySuppliers.All(x => x.Supplier.SupplierType != SupplierType.Fruits || (x.Supplier.SupplierType == SupplierType.Fruits && x.EndDate != null)))
                    {
                        var potentialSuppliers = helper.GetPotentialFruitSuppliers(company.Postcode).Select(s => s.Name).ToList();
                        results.Add(new ReportRequestedAccountGridViewModel()
                        {
                            ID = Guid.NewGuid().ToString(),
                            AccountNumber = company.AccountNo,
                            RequestDate = company.AskingForFruitSupplier.Value.ToString("dd/MM/yyyy"),
                            CompanyName = company.Name,
                            Category = "Fruit",
                            Postcode = company.Postcode,
                            Notes = company.Round,
                            PotentialSuppliers = potentialSuppliers,
                        });
                    }
                    if (company.AskingForSnackSupplier != null &&
                        company.CompanySuppliers.All(x => x.Supplier.SupplierType != SupplierType.Snacks || (x.Supplier.SupplierType == SupplierType.Snacks && x.EndDate != null)))
                    {
                        var potentialSuppliers = helper.GetPotentialSnackSuppliers(company.Postcode).Select(s => s.Name).ToList();
                        results.Add(new ReportRequestedAccountGridViewModel()
                        {
                            ID = Guid.NewGuid().ToString(),
                            AccountNumber = company.AccountNo,
                            RequestDate = company.AskingForSnackSupplier.Value.ToString("dd/MM/yyyy"),
                            CompanyName = company.Name,
                            Category = "Snack",
                            Postcode = company.Postcode,
                            Notes = company.Round,
                            PotentialSuppliers = potentialSuppliers,
                        });
                    }
                    var supAcctRequestLogs = company.SupAcctRequestLogs.Where(x => x.Supplier.SupplierType == SupplierType.DryGoods).ToList();
                    foreach (var supAcctRequestLog in supAcctRequestLogs)
                    {
                        if (!company.CompanySuppliers.Select(x => x.SupplierID).Contains(supAcctRequestLog.SupplierID))
                        { 
                            results.Add(new ReportRequestedAccountGridViewModel()
                            {
                                ID = Guid.NewGuid().ToString(),
                                AccountNumber = company.AccountNo,
                                RequestDate = supAcctRequestLog.RequestTime.ToString("dd/MM/yyyy"),
                                CompanyName = company.Name,
                                Category = supAcctRequestLog.Supplier.Label,
                                Notes = company.Round,
                                Postcode = company.Postcode,
                            });
                        }
                    }
                }
                return results.OrderByDescending(x => DateTime.ParseExact(x.RequestDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToList();
            }
        }


        //public JsonResult ReadFirstDeliveryDate([DataSourceRequest] DataSourceRequest request, string companyID, int dateType, DateTime? month)
        //{
        //    var viewModels = GetFirstDeliveryDateList(companyID, dateType, month);
        //    return Json(viewModels.ToDataSourceResult(request));
        //}

        //public ActionResult ExportFirstDeliveryDateCSV(string companyID, int dateType, DateTime? month)
        //{
        //    var viewModels = GetFirstDeliveryDateList(companyID, dateType, month);

        //    StringBuilder builder = new StringBuilder();
        //    builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
        //            "Supplier", "Account Number", "Company Name", "Category", "Start Date"
        //        ));

        //    // Append to StringBuilder.
        //    foreach (var item in viewModels)
        //    {
        //        builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
        //            item.Supplier,
        //            item.AccountNumber,
        //            item.CompanyName,
        //            item.Category,
        //            item.StartDate
        //        ));
        //    }

        //    var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
        //    var contentType = "text/csv";
        //    var fileDownloadName = string.Format("First Delivery Date {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

        //    return File(fileContent, contentType, fileDownloadName);
        //}

        //private List<ReportFirstDeliveryDateGridViewModel> GetFirstDeliveryDateList(string companyID, int dateType, DateTime? month)
        //{
        //    using (var context = new SnapDbContext())
        //    {
        //        DateTime fromDate = new DateTime(1980, 1, 1);
        //        DateTime toDate = new DateTime(2030, 1, 1);
        //        switch (dateType)
        //        {
        //            case 1: //Current Month
        //                fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
        //                toDate = fromDate.AddMonths(1);
        //                break;
        //            case 2: //Last Month
        //                fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
        //                toDate = fromDate.AddMonths(1);
        //                break;
        //            case 3: //Custom Date
        //                fromDate = month.Value.Date;
        //                toDate = month.Value.AddMonths(1);
        //                break;
        //            default:
        //                toDate = DateTime.Today.AddYears(20);
        //                fromDate = DateTime.Today.AddYears(-20);
        //                break;
        //        }

        //        var viewModels = context.CompanySuppliers.Where(x => x.StartDate != null)
        //                                                 .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
        //                                                 .Where(x => x.StartDate >= fromDate && x.StartDate < toDate)
        //                                                 .Include(x => x.Supplier)
        //                                                 .Include(x => x.Company)
        //                                                 .OrderByDescending(x => x.StartDate)
        //                                                 .ToList();

        //        var results = new List<ReportFirstDeliveryDateGridViewModel>();
        //        foreach (var item in viewModels)
        //        {
        //            results.Add(new ReportFirstDeliveryDateGridViewModel()
        //            {
        //                ID = Guid.NewGuid().ToString(),
        //                Supplier = item.Supplier.Name,
        //                AccountNumber = item.AccountNo,
        //                Category = item.Supplier.SupplierType == SupplierType.Fruits ? "Fruit" : item.Supplier.SupplierType == SupplierType.Milk ? "Milk" : item.Supplier.SupplierType == SupplierType.DryGoods ? "Dry Goods" : "Others",
        //                CompanyName = item.Company.Name,
        //                StartDate = item.StartDate.Value.ToString("dd/MM/yyyy")
        //            });
        //        }
        //        return results;
        //    }
        //}

    }

}