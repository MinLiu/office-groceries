﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MilkReportsController : BaseController
    {
        
        public JsonResult ReadMilkStanding([DataSourceRequest] DataSourceRequest request, string companyID)
        {
            var viewModels = GetMilkStandingList(companyID);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportMilkStandingCSV(string companyID)
        {
            var viewModels = GetMilkStandingList(companyID);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\"\n",
                    "Supplier", "Account Number", "Company Name", "Address", "Postcode", "Product (Code)", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Email"
                ));

            // Append to StringBuilder.
            foreach (var item in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\"\n",
                    item.Supplier,
                    item.AccountNumber,
                    item.CompanyName,
                    item.Address,
                    item.Postcode,
                    item.ProductNameCode,
                    item.Mon,
                    item.Tues,
                    item.Wed,
                    item.Thurs,
                    item.Fri,
                    item.Sat,
                    item.Email
                ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Milk Standing Order {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportMilkStandingGridViewModel> GetMilkStandingList(string companyID)
        {
            using (var context = new SnapDbContext())
            {
                var viewModels = context.MilkOrderHeaders.Where(x => x.Company.DeleteData == false)
                                                         .Where(x => x.OrderMode == OrderMode.Regular)
                                                         .Where(x => x.Depreciated == false)
                                                         .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
                                                         .Include(x => x.Items)
                                                         .Include(x => x.Company)
                                                         .OrderBy(x => x.Company.Name)
                                                         .ToList();

                var results = new List<ReportMilkStandingGridViewModel>();
                foreach (var q in viewModels)
                {
                    var today = DateTime.Today;
                    var companySupplier = context.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                   .Where(x => x.CompanyID == q.Company.ID)
                                   .Where(x => x.StartDate <= today && (x.EndDate == null || x.EndDate >= today))
                                   .FirstOrDefault();

                    if (companySupplier == null)
                        continue;

                    foreach (var item in q.Items.OrderBy(x => x.ProductName))
                    {
                        results.Add(new ReportMilkStandingGridViewModel()
                        {
                            ID = Guid.NewGuid().ToString(),
                            Supplier = companySupplier.Supplier.Name,
                            AccountNumber = companySupplier.AccountNo,
                            CompanyName = item.OrderHeader.Company.Name,
                            Email = item.OrderHeader.Company.Email?.Replace("\"", "'"),
                            Address = item.OrderHeader.Company.Address1?.Replace("\"", "'"),
                            Postcode = item.OrderHeader.Company.Postcode,
                            ProductNameCode = item.ProductName + "(" + item.Milk.ProductCode + ")",
                            Mon = item.Monday,
                            Tues = item.Tuesday,
                            Wed = item.Wednesday,
                            Thurs = item.Thursday,
                            Fri = item.Friday,
                            Sat = item.Saturday
                        });
                    }
                }
                return results;
            }
        }

        public JsonResult ReadMilkOneOff([DataSourceRequest] DataSourceRequest request, string companyID, DateTime fromDate, DateTime toDate)
        {
            var viewModels = GetMilkOneOffList(companyID, fromDate, toDate);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportMilkOneOffCSV(string companyID, DateTime fromDate, DateTime toDate)
        {
            var viewModels = GetMilkOneOffList(companyID, fromDate, toDate);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"\n",
                    "Supplier", "Account Number", "Company Name", "Product (Code)", fromDate.ToString("dd-MMM"), fromDate.AddDays(1).ToString("dd-MMM"), fromDate.AddDays(2).ToString("dd-MMM"), fromDate.AddDays(3).ToString("dd-MMM"), fromDate.AddDays(4).ToString("dd-MMM"), fromDate.AddDays(5).ToString("dd-MMM")
                ));

            // Append to StringBuilder.
            foreach (var item in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"\n",
                    item.Supplier,
                    item.AccountNumber,
                    item.CompanyName,
                    item.ProductNameCode,
                    item.Mon,
                    item.Tues,
                    item.Wed,
                    item.Thurs,
                    item.Fri,
                    item.Sat
                ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Milk Standing Order {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportMilkOneOffGridViewModel> GetMilkOneOffList(string companyID, DateTime fromDate, DateTime toDate)
        {
            using (var context = new SnapDbContext())
            {
                var viewModels = context.MilkOrderHeaders.Where(x => x.Company.DeleteData == false)
                                                         .Where(x => x.OrderMode == OrderMode.OneOff)
                                                         .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
                                                         .Where(x => x.FromDate >= fromDate && x.ToDate <= toDate)
                                                         .Include(x => x.Items)
                                                         .Include(x => x.Company)
                                                         .OrderBy(x => x.Company.Name)
                                                         .ToList();

                var results = new List<ReportMilkOneOffGridViewModel>();
                // Append to StringBuilder.
                foreach (var q in viewModels)
                {
                    var regularOrderItems = context.MilkOrderHeaders.Where(x => x.CompanyID == q.CompanyID)
                                                                    .Where(x => x.OrderMode == OrderMode.Regular)
                                                                    .Include(x => x.Items)
                                                                    .FirstOrDefault()
                                                                    .Items
                                                                    .ToList();
                    var oneoffOrderItems = q.Items.ToList();
                    foreach (var regItem in regularOrderItems)
                    {
                        if (!oneoffOrderItems.Any(x => x.MilkProductID == regItem.MilkProductID))
                        {
                            oneoffOrderItems.Add(new MilkOrder()
                            {
                                MilkProductID = regItem.MilkProductID,
                                Monday = 0,
                                Tuesday = 0,
                                Wednesday = 0,
                                Thursday = 0,
                                Friday = 0,
                                Saturday = 0
                            });
                        }
                    }
                    foreach (var oneoffItem in oneoffOrderItems)
                    {
                        if (!regularOrderItems.Any(x => x.MilkProductID == oneoffItem.MilkProductID))
                        {
                            regularOrderItems.Add(new MilkOrder()
                            {
                                MilkProductID = oneoffItem.MilkProductID,
                                Monday = 0,
                                Tuesday = 0,
                                Wednesday = 0,
                                Thursday = 0,
                                Friday = 0,
                                Saturday = 0
                            });
                        }
                    }

                    var companySupplier = context.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                   .Where(x => x.CompanyID == q.Company.ID)
                                   .Where(x => x.StartDate <= fromDate && (x.EndDate == null || x.EndDate >= fromDate))
                                   .FirstOrDefault();

                    int index = 0;
                    foreach (var item in oneoffOrderItems.OrderBy(x => x.ProductName))
                    {
                        var originItem = regularOrderItems.Where(x => x.MilkProductID == item.MilkProductID).FirstOrDefault();

                        if (item.Monday != originItem.Monday || item.Tuesday != originItem.Tuesday || item.Wednesday != originItem.Wednesday || item.Thursday != originItem.Thursday || item.Friday != originItem.Friday || item.Saturday != item.Saturday)
                        {
                            if (index++ == 0)
                            {
                                results.Add(new ReportMilkOneOffGridViewModel()
                                {
                                    Supplier = companySupplier?.Supplier?.Name ?? "",
                                    AccountNumber = companySupplier?.AccountNo ?? "",
                                    CompanyName = q.Company.Name,
                                    ProductNameCode = item.ProductName + "(" + item.Milk?.ProductCode + ")",
                                    Mon = item.Monday != originItem.Monday ? item.Monday.ToString() : "",
                                    Tues = item.Tuesday != originItem.Tuesday ? item.Tuesday.ToString() : "",
                                    Wed = item.Wednesday != originItem.Wednesday ? item.Wednesday.ToString() : "",
                                    Thurs = item.Thursday != originItem.Thursday ? item.Thursday.ToString() : "",
                                    Fri = item.Friday != originItem.Friday ? item.Friday.ToString() : "",
                                    Sat = item.Saturday != originItem.Saturday ? item.Saturday.ToString() : ""
                                });
                            }
                            else
                            {
                                results.Add(new ReportMilkOneOffGridViewModel()
                                {
                                    ProductNameCode = item.ProductName + "(" + item.Milk?.ProductCode + ")",
                                    Mon = item.Monday != originItem.Monday ? item.Monday.ToString() : "",
                                    Tues = item.Tuesday != originItem.Tuesday ? item.Tuesday.ToString() : "",
                                    Wed = item.Wednesday != originItem.Wednesday ? item.Wednesday.ToString() : "",
                                    Thurs = item.Thursday != originItem.Thursday ? item.Thursday.ToString() : "",
                                    Fri = item.Friday != originItem.Friday ? item.Friday.ToString() : "",
                                    Sat = item.Saturday != originItem.Saturday ? item.Saturday.ToString() : ""
                                });
                            }
                        }
                    }
                }

                return results;
            }
        }

    }

}