﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ReportIntermittentNextWeekController : BaseController
    {
        
        public JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var viewModels = GetIntermittentNextWeek();
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportCSV()
        {
            var viewModels = GetIntermittentNextWeek();

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                    "Supplier", "Supplier Email", "Supplier Tel.", "Account Number", "Company Name", "Postcode", "Week"
                ));

            // Append to StringBuilder.
            foreach (var item in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                    item.Supplier,
                    item.SupplierEmail,
                    item.SupplierTelephone,
                    item.AccountNumber,
                    item.CompanyName,
                    item.CompanyPostcode,
                    item.WeekStartDate?.ToString("dd/MM/yyyy")
                ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = "Intermittent Delivery - Next Week.csv";

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportIntermittentNextWeekViewModel> GetIntermittentNextWeek()
        {
            var mondayNextWeek = DateTime.Today.AddDays(7).StartOfWeek(DayOfWeek.Monday);
            using (var context = new SnapDbContext())
            {
                var viewModels = context.CompanySuppliers
                    .Where(x => x.IsIntermittentDelivery)
                    .Where(x => x.NextDeliveryWeek == mondayNextWeek)
                    .Where(x => x.StartDate != null && x.StartDate <= mondayNextWeek)
                    .Where(x => x.EndDate == null || x.EndDate >= mondayNextWeek)
                    .Include(x => x.Supplier)
                    .Include(x => x.Company)
                    .OrderByDescending(x => x.Supplier.Name)
                    .ThenBy(x => x.Company.Name)
                    .Select(x => new ReportIntermittentNextWeekViewModel()
                    {
                        Supplier = x.Supplier.Name,
                        SupplierEmail = x.Supplier.Email,
                        SupplierTelephone = x.Supplier.Telephone,
                        AccountNumber = x.AccountNo,
                        CompanyID = x.Company.ID,
                        CompanyName = x.Company.Name,
                        CompanyPostcode = x.Company.Postcode,
                        WeekStartDate = x.NextDeliveryWeek,
                        IsZeroContract = x.Company.FruitOrders
                            .Where(f => f.OrderMode == OrderMode.Regular && f.Depreciated == false)
                            .OrderByDescending(f => f.LatestUpdated).FirstOrDefault().TotalNet == 0
                    })
                    .Where(v => v.IsZeroContract == false)
                    .ToList();

                return viewModels;
            }
        }

    }

}