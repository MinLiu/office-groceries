﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ReportFirstDeliveryDateController : BaseController
    {
        
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = GetFirstDeliveryDateList(companyID, dateType, fromDate, toDate);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportCSV(string companyID, int dateType, string fromDate, string toDate)
        {
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var fDate);
            DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var tDate);
            var viewModels = GetFirstDeliveryDateList(companyID, dateType, fDate, tDate);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
                    "Supplier", "Account Number", "Company Name", "Category", "Start Date"
                ));

            // Append to StringBuilder.
            foreach (var item in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
                    item.Supplier,
                    item.AccountNumber,
                    item.CompanyName,
                    item.Category,
                    item.StartDate.ToString("dd/MM/yyyy")
                ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("First Delivery Date {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportFirstDeliveryDateGridViewModel> GetFirstDeliveryDateList(string companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            using (var context = new SnapDbContext())
            {
                switch (dateType)
                {
                    case 1: //Current Month
                        fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                        toDate = fromDate.Value.AddMonths(1);
                        break;
                    case 2: //Last Month
                        fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                        toDate = fromDate.Value.AddMonths(1);
                        break;
                    case 3: //Custom Date
                        fromDate = fromDate.Value.Date;
                        toDate = toDate.Value.AddDays(1);
                        break;
                    default:
                        toDate = DateTime.Today.AddYears(20);
                        fromDate = DateTime.Today.AddYears(-20);
                        break;
                }

                var viewModels = context.CompanySuppliers.Where(x => x.StartDate != null)
                                                         .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
                                                         .Where(x => x.StartDate >= fromDate && x.StartDate < toDate)
                                                         .Include(x => x.Supplier)
                                                         .Include(x => x.Company)
                                                         .OrderByDescending(x => x.StartDate)
                                                         .ToList();

                var results = new List<ReportFirstDeliveryDateGridViewModel>();
                foreach (var item in viewModels)
                {
                    results.Add(new ReportFirstDeliveryDateGridViewModel()
                    {
                        ID = Guid.NewGuid().ToString(),
                        Supplier = item.Supplier.Name,
                        AccountNumber = item.AccountNo,
                        Category = item.Supplier.SupplierType == SupplierType.Fruits ? "Fruit" : item.Supplier.SupplierType == SupplierType.Milk ? "Milk" : item.Supplier.SupplierType == SupplierType.DryGoods ? "Dry Goods" : "Others",
                        CompanyName = item.Company.Name,
                        StartDate = item.StartDate.Value,
                        //StartDate = item.StartDate.Value.ToString("dd/MM/yyyy")
                    });
                }
                return results;
            }
        }

    }

}