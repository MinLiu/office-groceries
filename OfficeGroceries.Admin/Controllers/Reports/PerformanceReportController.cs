﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class PerformanceReportController : BaseController
    {
        public async Task<ActionResult> Index(int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var result = new PerformanceReportViewModel();
            
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            using (var context = new SnapDbContext())
            {
                var requestedCustomers = await context.Companies
                    .Where(x => x.SignupDate >= fromDate && x.SignupDate < toDate)
                    .Include(x => x.FruitOrders)
                    .Include(x => x.MilkOrders)
                    .OrderByDescending(x => x.SignupDate)
                    .ToListAsync();

                foreach (var customer in requestedCustomers)
                {
                    result.RequestedCustomers.Add(new NewCustomerReport
                    {
                        CustomerID = customer.ID,
                        Name = customer.Name,
                        MilkSale = customer.MilkOrders.OrderBy(x => x.OrderMode).Select(x => x.TotalGross).FirstOrDefault(),
                        FruitSale = customer.FruitOrders.OrderBy(x => x.OrderMode).Select(x => x.TotalGross).FirstOrDefault(),
                        SignupDate = customer.SignupDate,
                    });
                }
                
                result.CountNewProspects =
                    await context.Prospects.CountAsync(x => x.Created >= fromDate && x.Created < toDate && !x.Deleted);

                result.CancelledContracts =
                    await context.CompanySuppliers.Where(x => x.EndDate >= fromDate && x.EndDate < toDate)
                        .Select(x => new CancelledContractReport
                        {
                            CustomerID = x.Company.ID,
                            CustomerName = x.Company.Name,
                            SupplierName = x.Supplier.Name,
                            Type = x.Supplier.SupplierType,
                            EndDate = x.EndDate.Value
                        })
                        .OrderByDescending(x => x.EndDate)
                        .ToListAsync();

                result.CancelledSuppliers = result.CancelledContracts
                    .GroupBy(x => x.SupplierName)
                    .Select(g => new
                    {
                        SupplerName = g.Key,
                        Count = g.Count()
                    })
                    .Select(x => new CancelledSupplierReport
                    {
                        Name = x.SupplerName,
                        Count = x.Count,
                        Percentage = (decimal)x.Count / result.CancelledContracts.Count
                    })
                    .OrderByDescending(x => x.Count)
                    .ToList();
                
                var milkZeroContracts =                 
                    await context.Companies
                        .Where(c => c.CompanySuppliers.Any(cs => cs.Supplier.SupplierType == SupplierType.Milk && cs.EndDate == null)) // it doesn't count if it's cancelled 
                        .Select(c =>
                            c.MilkOrders.FirstOrDefault(x => x.Depreciated == false
                                                             && x.OrderMode == OrderMode.Regular))
                        .Where(x => x.TotalNet == 0 && x.LatestUpdated >= fromDate && x.LatestUpdated < toDate)
                        .Select(x => new ZeroContractReport
                        {
                            CustomerID = x.Company.ID,
                            CustomerName = x.Company.Name,
                            SupplierName = x.Company.CompanySuppliers.Where(s => s.Supplier.SupplierType == SupplierType.Milk)
                                .OrderByDescending(s => s.StartDate).Select(s => s.Supplier.Name).FirstOrDefault(),
                            Type = SupplierType.Milk,
                            UpdatedDate = x.LatestUpdated.Value
                        }).ToListAsync();

                var fruitZeroContracts = 
                    await context.Companies
                        .Where(c => c.CompanySuppliers.Any(cs => cs.Supplier.SupplierType == SupplierType.Fruits && cs.EndDate == null)) // it doesn't count if it's cancelled
                        .Select(c =>
                            c.FruitOrders.FirstOrDefault(x => x.Depreciated == false
                                                             && x.OrderMode == OrderMode.Regular))
                        .Where(x => x.TotalNet == 0 && x.LatestUpdated >= fromDate && x.LatestUpdated < toDate)
                        .Select(x => new ZeroContractReport
                        {
                            CustomerID = x.Company.ID,
                            CustomerName = x.Company.Name,
                            SupplierName = x.Company.CompanySuppliers.Where(s => s.Supplier.SupplierType == SupplierType.Fruits)
                                .OrderByDescending(s => s.StartDate).Select(s => s.Supplier.Name).FirstOrDefault(),
                            Type = SupplierType.Fruits,
                            UpdatedDate = x.LatestUpdated.Value
                        }).ToListAsync();
                
                result.ZeroContracts = milkZeroContracts.Concat(fruitZeroContracts).ToList();

                result.ZeroedSuppliers = result.ZeroContracts
                    .GroupBy(x => x.SupplierName)
                    .Select(g => new
                    {
                        SupplerName = g.Key,
                        Count = g.Count()
                    })
                    .Select(x => new CancelledSupplierReport
                    {
                        Name = x.SupplerName,
                        Count = x.Count,
                        Percentage = (decimal)x.Count / result.ZeroContracts.Count
                    })
                    .OrderByDescending(x => x.Count)
                    .ToList();
                
                result.LiveCustomers =
                    await context.Companies
                        .Where(c => c.CompanySuppliers.Any())
                        .Select(c => new
                        {
                            Company = c,
                            CompanySupplier = c.CompanySuppliers.Where(s => s.StartDate != null)
                                .OrderBy(s => s.StartDate).FirstOrDefault()
                        })
                        .Where(x => x.CompanySupplier.StartDate >= fromDate && x.CompanySupplier.StartDate < toDate)
                        .Select(x => new LiveCustomerReport
                        {
                            CustomerID = x.Company.ID,
                            Name = x.Company.Name,
                            Type = x.CompanySupplier.Supplier.SupplierType,
                            StartDate = x.CompanySupplier.StartDate.Value,
                            SignUpDate = x.Company.SignupDate
                        }).ToListAsync();

                
                var requestedCustomerIdList = result.RequestedCustomers.Select(c => c.CustomerID);

                var duplicateCount = result.LiveCustomers.Count(x => requestedCustomerIdList.Contains(x.CustomerID));
                var nonDuplicateCount = result.CountNewLiveCustomers - duplicateCount;

                var liveSuccessRate = nonDuplicateCount + result.CountRequestedCustomers != 0
                    ? Convert.ToDecimal(result.CountNewLiveCustomers) / Convert.ToDecimal(nonDuplicateCount + result.CountRequestedCustomers)
                    : 0;

                result.LiveSuccessRate = liveSuccessRate;
                
                return PartialView(result);
            }
        }
        
    }

    public class PerformanceReportViewModel
    {
        public ICollection<NewCustomerReport> RequestedCustomers { get; set; } = new List<NewCustomerReport>();
        public ICollection<LiveCustomerReport> LiveCustomers { get; set; } = new List<LiveCustomerReport>();
        public int CountRequestedCustomers => RequestedCustomers.Count;
        public int CountNewProspects { get; set; }
        public decimal ProspectSuccessRate =>
            CountRequestedCustomers == 0 ? 0 : Convert.ToDecimal(CountRequestedCustomers) / Convert.ToDecimal(CountRequestedCustomers + CountNewProspects);

        public int CountNewLiveCustomers => LiveCustomers.Count;
        
        // public decimal OldLiveSuccessRate => CountNewLiveCustomers == 0
        //     ? 0
        //     : Convert.ToDecimal(CountNewLiveCustomers) / Convert.ToDecimal(CountRequestedCustomers + CountNewLiveCustomers);
        
        public decimal LiveSuccessRate { get; set; }
        
        public ICollection<CancelledContractReport> CancelledContracts { get; set; } =
            new List<CancelledContractReport>();

        public int CountZeroContracts => ZeroContracts.Count;
        
        public ICollection<ZeroContractReport> ZeroContracts { get; set; } =
            new List<ZeroContractReport>();

        public int CountCancelledContracts => CancelledContracts.Count;

        // public decimal AvgSalePerNewCustomer => NewCustomers.Average(x => x.FruitSale + x.MilkSale);
        public decimal AvgFruitSalePerNewCustomer => RequestedCustomers.Where(x => x.FruitSale > 0).Count() > 0 ? RequestedCustomers.Where(x => x.FruitSale > 0).Average(x => x.FruitSale) : 0;
        public decimal AvgMilkSalePerNewCustomer => RequestedCustomers.Where(x => x.MilkSale > 0).Count() > 0 ? RequestedCustomers.Where(x => x.MilkSale > 0).Average(x => x.MilkSale) : 0;

        public decimal CustomerLostRate => CountRequestedCustomers == 0 ? 0 : Convert.ToDecimal(CountCancelledContracts) / Convert.ToDecimal(CountRequestedCustomers);

        public List<CancelledSupplierReport> CancelledSuppliers { get; set; }
        public List<CancelledSupplierReport> ZeroedSuppliers { get; set; }
    }

    public class NewCustomerReport
    {
        public Guid CustomerID { get; set; }
        public string Name { get; set; }
        public decimal MilkSale { get; set; }
        public decimal FruitSale { get; set; }
        public DateTime SignupDate { get; set; }
    }

    public class CancelledContractReport
    {
        public Guid CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string SupplierName { get; set; }
        public SupplierType Type { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class LiveCustomerReport
    {
        public Guid CustomerID { get; set; }
        public string Name { get; set; }
        public SupplierType Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime SignUpDate { get; set; }
    }

    public class ZeroContractReport
    {
        public Guid CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string SupplierName { get; set; }
        public SupplierType Type { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class CancelledSupplierReport
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public decimal Percentage { get; set; }
    }

}