﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class PoNumberReportsController : BaseController
    {
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? companyID = null)
        {
            var viewModels = GetReport(companyID);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult Export(bool onlyNew, Guid? companyID = null)
        {
            var viewModels = GetReport(companyID, onlyNew).ToList();

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
                    "Company Name", "Month/Year", "Invoice Number", "Invoice Amount", "PO Number"
                ));

            // Append to StringBuilder.
            foreach (var item in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1:yyyy-MM}\",\"{2}\",\"{3}\",\"{4}\"\n",
                    item.CompanyName,
                    item.InvoiceMonth,
                    item.InvoiceNumber,
                    item.InvoiceAmount,
                    item.PoNumber
                ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("PoNumber {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            SetExported(viewModels);
            
            return File(fileContent, contentType, fileDownloadName);
        }

        private IEnumerable<MonthlyPoNumberReportVm> GetReport(Guid? companyID, bool onlyNew = false)
        {
            var repo = new MonthlyPoNumberRepository();

            var query = repo.Read().AsQueryable();

            if (companyID != null)
            {
                query = query.Where(x => x.CompanyID == companyID);
            }
            
            if (onlyNew)
            {
                query = query.Where(x => x.IsExported == false);
            }
                
            return query.Select(x => new MonthlyPoNumberReportVm
            {
                ID = x.ID,
                IsExported = x.IsExported,
                CompanyName = x.Company.Name,
                AccountNumber = x.Company.AccountNo,
                InvoiceNumber = x.InvoiceNumber,
                InvoiceMonth = x.InvoiceMonth,
                InvoiceAmount = x.InvoiceAmount,
                PoNumber = x.PoNumber,
            }).OrderByDescending(x => x.InvoiceMonth);
        }
        
        private void SetExported(IEnumerable<MonthlyPoNumberReportVm> viewModels)
        {
            var repo = new MonthlyPoNumberRepository();

            var newPoRecords = viewModels.Where(x => x.IsExported == false).Select(x => x.ID).ToList();

            var models = repo.Read().Where(x => newPoRecords.Contains(x.ID)).ToList();

            foreach (var item in models)
            {
                item.IsExported = true;
            }
            
            repo.Update(models);
        }
        

    }

}