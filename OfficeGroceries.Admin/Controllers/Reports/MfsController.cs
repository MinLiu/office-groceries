﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MfsController : BaseController
    {
        public ActionResult ExportCSV(string supplier)
        {
            var viewModels = GetItems(supplier);

            StringBuilder builder = new StringBuilder();
            builder.Append(
                "\"Ref 1\"," +
                "\"Customer name\"," +
                "\"Address line 1\"," +
                "\"Address line 2\"," +
                "\"Address line 3\"," +
                "\"City\"," +
                "\"Post code\"," +
                "\"No of packages\"," +
                "\"Weight\"," +
                "\"Shipment date\"," +
                "\"Contact Name\"," +
                "\"Contact Telephone\"," +
                "\"Notification Text\"," +
                "\"Contact E-mail\"," +
                "\"\"," +
                "\"Delivery Instructions\"," +
                "\"Fruit Amendments\"" +
                "\n");

            // Append to StringBuilder.
            foreach (var item in viewModels)
            {
                builder.Append(
                    $"\"{item.Ref1}\"," +
                    $"\"{item.CustomerName}\"," +
                    $"\"{item.AddressLine1}\"," +
                    $"\"{item.AddressLine2}\"," +
                    $"\"{item.AddressLine3}\"," +
                    $"\"{item.City}\"," +
                    $"\"{item.Postcode}\"," +
                    $"\"{item.NumOfPackages}\"," +
                    $"\"{item.Weight}\"," +
                    $"\"{item.ShipmentDate:dd/MM/yyyy}\"," +
                    $"\"{item.ContactName}\"," +
                    $"\"{KeepLeadingZero(item.ContactTelephone)}\"," +
                    $"\"{KeepLeadingZero(item.NotificationText)}\"," +
                    $"\"{item.ContactEmail}\"," +
                    $"\"{item.Mfs13}\"," +
                    $"\"{item.DeliveryInstructions}\"," +
                    $"\"{item.FruitAmendments}\"" +
                    $"\n");
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());

            return File(fileContent, "text/csv", $"{supplier} Report.csv");
        }

        private string KeepLeadingZero(string value)
        {
            if (value?.All(x => x >= 48 && x <= 57 || x == 43) ?? false) // 0-9 or +
            {
                return value.Length > 6
                    ? new string(value.Take(5).ToArray()) + " " + new string(value.Skip(5).ToArray())
                    : new string(value.Take(1).ToArray()) + " " + new string(value.Skip(1).ToArray());
            }
            else
            {
                return value;
            }
            
            
        }

        private List<Item> GetItems(string supplier)
        {
            var result = new List<Item>();
            
            var mondayNextWeek = DateTime.Now.AddDays(7).StartOfWeek(DayOfWeek.Monday);
            var tuesdayNextWeek = mondayNextWeek.AddDays(1);
            var wednesdayNextWeek = mondayNextWeek.AddDays(2);
            var thursdayNextWeek = mondayNextWeek.AddDays(3);
            var fridayNextWeek = mondayNextWeek.AddDays(4);
            var saturdayNextWeek = mondayNextWeek.AddDays(5);
            
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(null, new SnapDbContext());
            var companies = GetCompanies(supplier);
            
            foreach (var company in companies)
            {
                var orderNextWeek = service.GetWeeklyOrderAsNoTracking(OrderMode.OneOff, company.ID, mondayNextWeek);

                if (orderNextWeek?.Items == null)
                    continue;

                foreach (var orderItem in orderNextWeek.Items.Where(x => x.ProductName != null && !x.ProductName.Contains("Nutribox")))
                {
                    AddIfNotZero(result, company, orderItem.ProductName, mondayNextWeek, orderItem.Monday);
                    AddIfNotZero(result, company, orderItem.ProductName, tuesdayNextWeek, orderItem.Tuesday);
                    AddIfNotZero(result, company, orderItem.ProductName, wednesdayNextWeek, orderItem.Wednesday);
                    AddIfNotZero(result, company, orderItem.ProductName, thursdayNextWeek, orderItem.Thursday);
                    AddIfNotZero(result, company, orderItem.ProductName, fridayNextWeek, orderItem.Friday);
                    AddIfNotZero(result, company, orderItem.ProductName, saturdayNextWeek, orderItem.Saturday);
                }
            }

            return result;
        }

        private void AddIfNotZero(List<Item> result, CompanyVm company, string pdtName, DateTime date, int qty)
        {
            if (company.LiveDays[date.DayOfWeek] == false)
                return;
            
            if (qty <= 0)
                return;

            var address = company.Address ?? "";
            var addressSplit = address.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            var address1 = "";
            var address2 = "";
            var address3 = "";
            var city = "";
            switch (addressSplit.Count)
            {
                case 0:
                    break;
                case 1:
                    address1 = addressSplit[0];
                    break;
                case 2:
                    address1 = addressSplit[0];
                    city = addressSplit[1];
                    break;
                case 3:
                    address1 = addressSplit[0];
                    address2 = addressSplit[1];
                    city = addressSplit[2];
                    break;
                case 4:
                    address1 = addressSplit[0];
                    address2 = addressSplit[1];
                    address3 = addressSplit[2];
                    city = addressSplit[3];
                    break;
                default:
                    address1 = addressSplit[0];
                    address2 = addressSplit[1];
                    address3 = string.Join(", ", addressSplit.Skip(2).Take(addressSplit.Count - 3));
                    city = addressSplit[addressSplit.Count - 1];
                    break;
            }
            
            result.Add(new Item
            {
                Ref1 = pdtName,
                CustomerName = company.Name,
                AddressLine1 = address1,
                AddressLine2 = address2,
                AddressLine3 = address3,
                City = city,
                Postcode = company.Postcode,
                NumOfPackages = qty,
                ShipmentDate = date.AddDays(-1), // the fruit will be collected for a next day delivery
                ContactName = company.CompanyRegistration,
                ContactTelephone = company.Telephone,
                NotificationText = company.Telephone,
                ContactEmail = company.Email?.Split(';')[0]?.Trim(),
                Mfs13 = company.IsMfs13 ? "13" : "",
                DeliveryInstructions = company.DeliveryInstructions,
                FruitAmendments = company.FruitAmendments,
            });
        }

        private List<CompanyVm> GetCompanies(string supplier)
        {
            var companiesWith13 = GetMfsCompanies(supplier, true);
            var companiesWithout13 = GetMfsCompanies(supplier, false);
            return companiesWith13.Concat(companiesWithout13).Distinct().ToList();
        }
        
        private List<CompanyVm> GetMfsCompanies(string supplier, bool isMfs13)
        {
            var supplierID = supplier == "DLRDPD"
                ? Guid.Parse("6bcce051-92e7-474c-9839-ebc4fdeb9e26") // DLR DPD
                : Guid.Parse("765a4128-5010-4c83-9dae-d526ff485d2c"); // MFS
            
            var nextWeekMonday = DateTime.Now.AddDays(7).StartOfWeek(DayOfWeek.Monday);
            return new SnapDbContext().Companies
                .Where(x => x.CompanySuppliers.Any(s =>
                    s.SupplierID == supplierID && s.IsMfs13 == isMfs13 &&
                    (s.EndDate == null || s.EndDate >= nextWeekMonday)))
                .Where(x => x.DeleteData == false)
                .ToList()
                .Where(x => x.IsLiveWeek(SupplierType.Fruits, nextWeekMonday))
                .Select(company => new CompanyVm(
                    company,
                    isMfs13,
                    company.CompanySuppliers.Where(s =>
                        s.SupplierID == supplierID &&
                        s.IsMfs13 == isMfs13 &&
                        (s.EndDate == null || s.EndDate >= nextWeekMonday))
                        .Select(s => s.EndDate)
                        .FirstOrDefault(),
                    nextWeekMonday))
                .ToList();
        }

        private class Item
        {
            public string Ref1 { get; set; }
            public string CustomerName { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string AddressLine3 { get; set; }
            public string City { get; set; }
            public string Postcode { get; set; }
            public int NumOfPackages { get; set; }
            public int Weight => NumOfPackages * 5;
            public DateTime ShipmentDate { get; set; }
            public string ContactName { get; set; }
            public string ContactTelephone { get; set; }
            public string NotificationText { get; set; }
            public string ContactEmail { get; set; }
            public string Mfs13 { get; set; }
            public string DeliveryInstructions { get; set; }
            public string FruitAmendments { get; set; }
        }

        private class CompanyVm : IEquatable<CompanyVm>
        {
            public CompanyVm(Company company, bool isMfs13, DateTime? endDate, DateTime nextWeekMonday)
            {
                ID = company.ID;
                Address = company.Address1;
                Name = company.Name;
                Postcode = company.Postcode;
                Telephone = company.Telephone;
                Email = company.Email;
                CompanyRegistration = company.CompanyRegistration;
                IsMfs13 = isMfs13;
                DeliveryInstructions = company.InternalDeliveryInstruction;
                FruitAmendments = company.DpdFruitAmendments;

                if (EndsNextWeek())
                {
                    LiveDays.Add(DayOfWeek.Monday, endDate.Value.DayOfWeek >= DayOfWeek.Monday);
                    LiveDays.Add(DayOfWeek.Tuesday, endDate.Value.DayOfWeek >= DayOfWeek.Tuesday);
                    LiveDays.Add(DayOfWeek.Wednesday, endDate.Value.DayOfWeek >= DayOfWeek.Wednesday);
                    LiveDays.Add(DayOfWeek.Thursday, endDate.Value.DayOfWeek >= DayOfWeek.Thursday);
                    LiveDays.Add(DayOfWeek.Friday, endDate.Value.DayOfWeek >= DayOfWeek.Friday);
                    LiveDays.Add(DayOfWeek.Saturday, endDate.Value.DayOfWeek >= DayOfWeek.Saturday);
                }
                else if (EndsBeforeNextWeek())
                {
                    LiveDays.Add(DayOfWeek.Monday, false);
                    LiveDays.Add(DayOfWeek.Tuesday, false);
                    LiveDays.Add(DayOfWeek.Wednesday, false);
                    LiveDays.Add(DayOfWeek.Thursday, false);
                    LiveDays.Add(DayOfWeek.Friday, false);
                    LiveDays.Add(DayOfWeek.Saturday, false);
                }
                else
                {
                    LiveDays.Add(DayOfWeek.Monday, true);
                    LiveDays.Add(DayOfWeek.Tuesday, true);
                    LiveDays.Add(DayOfWeek.Wednesday, true);
                    LiveDays.Add(DayOfWeek.Thursday, true);
                    LiveDays.Add(DayOfWeek.Friday, true);
                    LiveDays.Add(DayOfWeek.Saturday, true);
                }

                bool EndsNextWeek()
                {
                    return endDate?.StartOfWeek(DayOfWeek.Monday) == nextWeekMonday;
                }

                bool EndsBeforeNextWeek()
                {
                    return endDate?.StartOfWeek(DayOfWeek.Monday) < nextWeekMonday;
                }
            }

            public Guid ID { get; set; }
            public string Address { get; set; }
            public string Name { get; set; }
            public string Postcode { get; set; }
            public string Telephone { get; set; }
            public string Email { get; set; }
            public string CompanyRegistration { get; set; }
            public bool IsMfs13 { get; set; }
            public string DeliveryInstructions { get; set; }
            public string FruitAmendments { get; set; }
            public Dictionary<DayOfWeek, bool> LiveDays { get; set; } = new Dictionary<DayOfWeek, bool>();

            public bool Equals(CompanyVm other)
            {
                if (other == null)
                    return false;

                return ID == other.ID;
            }
        }
    }
}