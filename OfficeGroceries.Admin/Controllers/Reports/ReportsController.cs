﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.ExtendedProperties;
using NuIngredient.Models.Helpers.Reports;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ReportsController : BaseController
    {
        public ActionResult PoNumber()
        {
            return View();
        }
        
        public ActionResult ZeroMilkOrder()
        {
            return View();
        }

        public ActionResult ZeroFruitOrder()
        {
            return View();
        }

        public ActionResult DryGoods()
        {
            return View();
        }

        public JsonResult ReadDryGoods([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadDryGoodsReport(companyID, dateType, fromDate, toDate);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportDryGoodsCSV([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadDryGoodsReport(companyID, dateType, fromDate, toDate);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\"\n",
                    "Order No.", "OG Master Account #", "Client Name", "PO Number", "Order Date", "Products Purchased", "Volume", "Net", "VAT", "Gross", "credits / changes (+ or -)", "Product Code", "Supplier"
                ));

            // Append to StringBuilder.
            foreach (var q in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\"\n",
                        q.OrderNumber,
                        q.OGMasterAccount,
                        q.ClientName,
                        q.PoNumber,
                        q.OrderDate,
                        q.ProductsPurchased,
                        q.Volume,
                        q.Net,
                        q.VAT,
                        q.Gross,
                        q.Credit,
                        q.ProductCode,
                        q.Supplier
                        ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Dry Goods Export {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportDryGoodsViewModel> ReadDryGoodsReport(Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }
            using (var context = new SnapDbContext())
            {
                var viewModels = context.OneOffOrderItems
                                        .Where(x => x.OrderHeader.Status == OneOffOrderStatus.Accepted)
                                        .Where(x => companyID == null || x.OrderHeader.CompanyID == companyID)
                                        .Where(x => x.OrderHeader.Created >= fromDate && x.OrderHeader.Created < toDate)
                                        .Include(x => x.Product)
                                        .Include(x => x.Product.Supplier)
                                        .OrderByDescending(x => x.OrderHeader.Created)
                                        .ThenBy(x => x.OrderHeader.OrderNumber)
                                        .ToList()
                                        .Select(x => new ReportDryGoodsViewModel
                                        {
                                            ID = x.OrderHeaderID.ToString(),
                                            OrderNumber = x.OrderHeader.OrderNumber,
                                            OGMasterAccount = x.Company.AccountNo,
                                            ClientName = x.Company.Name,
                                            DeliveryPostcode = x.Company.Postcode,
                                            OrderDate = x.OrderHeader.Created.ToString("dd/MM/yyyy"),
                                            ProductsPurchased = x.ProductName,
                                            Volume = x.Qty.ToString(),
                                            Net = x.Total,
                                            VAT = x.ProductVAT * x.Total,
                                            Gross = x.Total + x.ProductVAT * x.Total,
                                            Credit = x.Credits.Any() ? x.Credits.Sum(c => c.Qty) : 0,
                                            PoNumber = !string.IsNullOrEmpty(x.OrderHeader.Company.PermanentDryGoodsPONo) ? x.OrderHeader.Company.PermanentDryGoodsPONo : x.OrderHeader.PONumber,
                                            ProductCode = x.ProductCode,
                                            Supplier = x.Product?.Supplier?.Name
                                        }).ToList();
                return viewModels;
            }
        }

        public ActionResult MilkAndFruit()
        {
            return View();
        }

        public JsonResult ReadMilkAndFruit([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadMilkAndFruitReport(companyID, dateType, fromDate, toDate);
            return Json(viewModels.ToDataSourceResult(request));
        }

        private List<ReportMilkAndFruitViewModel> ReadMilkAndFruitReport(Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            using (var context = new SnapDbContext())
            {
                var milkModels = context.MilkOrders
                                        .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                        .Where(x => companyID == null || x.OrderHeader.CompanyID == companyID)
                                        .Where(x => x.OrderHeader.FromDate >= fromDate && x.OrderHeader.ToDate < toDate)
                                        .OrderByDescending(x => x.OrderHeader.FromDate)
                                        .ThenBy(x => x.OrderHeader.OrderNumber)
                                        .GroupBy(x => new
                                        {
                                            ProductID = x.MilkProductID,
                                            ProductName = x.Milk.Name,
                                            ProductCode = x.Milk.ProductCode,
                                            CompanyID = x.OrderHeader.CompanyID,
                                            CompanyName = x.OrderHeader.Company.Name,
                                            AccountNo = x.OrderHeader.Company.AccountNo,
                                            SupplierName = x.OrderHeader.Supplier.Name,
                                        })
                                        .ToList()
                                        .Select(g => new ReportMilkAndFruitViewModel
                                        {
                                            ID = Guid.NewGuid().ToString(),
                                            SupplierName = g.Key.SupplierName,
                                            Type = ReportType.Milk,
                                            AccountNo = g.Key.AccountNo,
                                            ClientName = g.Key.CompanyName,
                                            ProductCode = g.Key.ProductCode,
                                            Description = g.Key.ProductName,
                                            Total = g.Sum(x => x.TotalNet),
                                            TotalQty = g.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday),
                                            Credit = g.Sum(x => x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday)),//x.Credits.Any() ? x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday) : 0
                                            Comments = string.Join(" | ", g.SelectMany(x => x.Credits.Select(c => c.MilkCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.MilkCreditHeader.Narrative)))
                                        }).ToList();

                var fruitModels = context.FruitOrders
                                        .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                        .Where(x => companyID == null || x.OrderHeader.CompanyID == companyID)
                                        .Where(x => x.OrderHeader.FromDate >= fromDate && x.OrderHeader.ToDate < toDate)
                                        .OrderByDescending(x => x.OrderHeader.FromDate)
                                        .ThenBy(x => x.OrderHeader.OrderNumber)
                                        .GroupBy(x => new
                                        {
                                            ProductID = x.FruitProductID,
                                            ProductName = x.Fruit.Name,
                                            ProductCode = x.Fruit.ProductCode,
                                            CompanyID = x.OrderHeader.CompanyID,
                                            CompanyName = x.OrderHeader.Company.Name,
                                            AccountNo = x.OrderHeader.Company.AccountNo,
                                            SupplierName = x.OrderHeader.Supplier.Name,
                                        })
                                        .ToList()
                                        .Select(g => new ReportMilkAndFruitViewModel
                                        {
                                            ID = Guid.NewGuid().ToString(),
                                            SupplierName = g.Key.SupplierName,
                                            Type = ReportType.Fruit,
                                            AccountNo = g.Key.AccountNo,
                                            ClientName = g.Key.CompanyName,
                                            ProductCode = g.Key.ProductCode,
                                            Description = g.Key.ProductName,
                                            Total = g.Sum(x => x.TotalNet),
                                            TotalQty = g.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday),
                                            Credit = g.Sum(x => x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday)),//x.Credits.Any() ? x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday) : 0
                                            Comments = string.Join(" | ", g.SelectMany(x => x.Credits.Select(c => c.FruitCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.FruitCreditHeader.Narrative)))
                                        }).ToList();

                var result = milkModels
                    .Concat(fruitModels)
                    .OrderBy(x => x.ClientName)
                    .ThenBy(x => x.Type)
                    .ThenBy(x => x.ProductCode)
                    .ToList();

                return result;
            }
        }

        public ActionResult ExportMilkAndFruitCSV([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadMilkAndFruitReport(companyID, dateType, fromDate, toDate);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"\n",
                    "OG Account Number", "AH Reference", "Client Name", "Product Code", "Description", "Volume", "Value", "Supplier", "Credits / Changes (+ or -)", "Comments"
                ));

            // Append to StringBuilder.
            foreach (var q in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"\n",
                        q.AccountNo,
                        "",
                        q.ClientName,
                        q.ProductCode,
                        q.Description,
                        q.TotalQty,
                        q.Total,
                        q.SupplierName,
                        q.Credit,
                        q.Comments.Replace("\"", "\"\"")
                        ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Milk & Fruit Export {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        public ActionResult NewMilkAndFruit()
        {
            return View();
        }

        public JsonResult NewReadMilkAndFruit([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = NewReadMilkAndFruitReport(companyID, dateType, fromDate, toDate);
            return Json(viewModels.ToDataSourceResult(request));
        }

        private List<ReportMilkAndFruitViewModel> NewReadMilkAndFruitReport(Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1).AddMilliseconds(-1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1).AddMilliseconds(-1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value;
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            using (var context = new SnapDbContext())
            {
                context.Database.CommandTimeout = 300;
                
                var weekStart = fromDate.Value.StartOfWeek(DayOfWeek.Monday);
                var weekEnd = toDate.Value.StartOfWeek(DayOfWeek.Monday).AddDays(6);

                var milkQuery = context.MilkOrderHeaders
                    .Where(x => x.OrderMode == OrderMode.History)
                    .Where(x => x.FromDate >= weekStart && x.ToDate <= weekEnd);
                
                if (companyID != null)
                {
                    milkQuery = milkQuery.Where(x => x.CompanyID == companyID);
                }
                
                var milkHeaders = milkQuery
                    .Include(x => x.Supplier)
                    .Include(x => x.Company)
                    .Include(x => x.Items.Select(milkOrder => milkOrder.Milk))
                    .Include(milkOrderHeader => milkOrderHeader.Items.Select(milkOrder1 => milkOrder1.Credits.Select(milkCreditItem =>
                        milkCreditItem.MilkCreditHeader.OrderHeader)))
                    .OrderByDescending(x => x.FromDate)
                    .ThenBy(x => x.ID)
                    .ToList();
                
                var milkModels = milkHeaders.SelectMany(x => x.Items).ToList();

                var milkReportList = new List<ReportMilkAndFruitViewModel>();
                foreach (var milkOrder in milkModels)
                {
                    var mon = GetMilkOrderByWeekday(milkOrder, DayOfWeek.Monday);
                    var tue = GetMilkOrderByWeekday(milkOrder, DayOfWeek.Tuesday);
                    var wed = GetMilkOrderByWeekday(milkOrder, DayOfWeek.Wednesday);
                    var thu = GetMilkOrderByWeekday(milkOrder, DayOfWeek.Thursday);
                    var fri = GetMilkOrderByWeekday(milkOrder, DayOfWeek.Friday);
                    var sat = GetMilkOrderByWeekday(milkOrder, DayOfWeek.Saturday);

                    if (mon.TotalQty != 0 || mon.Credit != 0)
                        milkReportList.Add(mon);
                    if (tue.TotalQty != 0 || tue.Credit != 0)
                        milkReportList.Add(tue);
                    if (wed.TotalQty != 0 || wed.Credit != 0)
                        milkReportList.Add(wed);
                    if (thu.TotalQty != 0 || thu.Credit != 0)
                        milkReportList.Add(thu);
                    if (fri.TotalQty != 0 || fri.Credit != 0)
                        milkReportList.Add(fri);
                    if (sat.TotalQty != 0 || sat.Credit != 0)
                        milkReportList.Add(sat);
                }

                milkReportList = milkReportList
                    .Where(r => r.OrderDate >= fromDate.Value)
                    .Where(r => r.OrderDate <= toDate.Value)
                    .ToList();
                
                var fruitQuery = context.FruitOrderHeaders
                    .Where(x => x.OrderMode == OrderMode.History)
                    .Where(x => x.FromDate >= weekStart && x.ToDate <= weekEnd);
                
                if (companyID != null)
                {
                    fruitQuery = fruitQuery.Where(x => x.CompanyID == companyID);
                }
                
                var fruitHeaders = fruitQuery
                    .Include(x => x.Supplier)
                    .Include(x => x.SnackSupplier)
                    .Include(x => x.Company)
                    .Include(x => x.Items.Select(fruitOrder => fruitOrder.Fruit))
                    .Include(fruitOrderHeader => fruitOrderHeader.Items.Select(fruitOrder1 => fruitOrder1.Credits.Select(fruitCreditItem =>
                        fruitCreditItem.FruitCreditHeader.OrderHeader)))
                    .OrderByDescending(x => x.FromDate)
                    .ThenBy(x => x.ID)
                    .ToList();
                
                var fruitModels = fruitHeaders
                    .SelectMany(x => x.Items.Where(product => product.Fruit.FruitProductType == FruitProductType.Fruit))
                    .ToList();
                
                var fruitReportList = new List<ReportMilkAndFruitViewModel>();
                foreach (var fruitOrder in fruitModels)
                {
                    var mon = GetFruitOrderByWeekday(fruitOrder, DayOfWeek.Monday, FruitProductType.Fruit);
                    var tue = GetFruitOrderByWeekday(fruitOrder, DayOfWeek.Tuesday, FruitProductType.Fruit);
                    var wed = GetFruitOrderByWeekday(fruitOrder, DayOfWeek.Wednesday, FruitProductType.Fruit);
                    var thu = GetFruitOrderByWeekday(fruitOrder, DayOfWeek.Thursday, FruitProductType.Fruit);
                    var fri = GetFruitOrderByWeekday(fruitOrder, DayOfWeek.Friday, FruitProductType.Fruit);
                    var sat = GetFruitOrderByWeekday(fruitOrder, DayOfWeek.Saturday, FruitProductType.Fruit);

                    if (mon.TotalQty != 0 || mon.Credit != 0)
                        fruitReportList.Add(mon);
                    if (tue.TotalQty != 0 || tue.Credit != 0)
                        fruitReportList.Add(tue);
                    if (wed.TotalQty != 0 || wed.Credit != 0)
                        fruitReportList.Add(wed);
                    if (thu.TotalQty != 0 || thu.Credit != 0)
                        fruitReportList.Add(thu);
                    if (fri.TotalQty != 0 || fri.Credit != 0)
                        fruitReportList.Add(fri);
                    if (sat.TotalQty != 0 || sat.Credit != 0)
                        fruitReportList.Add(sat);
                }

                fruitReportList = fruitReportList
                    .Where(r => r.OrderDate >= fromDate.Value)
                    .Where(r => r.OrderDate <= toDate.Value)
                    .ToList();
                
                var snackModels = fruitHeaders
                    .SelectMany(x => x.Items.Where(product => product.Fruit.FruitProductType == FruitProductType.Snack))
                    .ToList();
                
                var snackReportList = new List<ReportMilkAndFruitViewModel>();
                foreach (var snackOrder in snackModels)
                {
                    var mon = GetFruitOrderByWeekday(snackOrder, DayOfWeek.Monday, FruitProductType.Snack);
                    var tue = GetFruitOrderByWeekday(snackOrder, DayOfWeek.Tuesday, FruitProductType.Snack);
                    var wed = GetFruitOrderByWeekday(snackOrder, DayOfWeek.Wednesday, FruitProductType.Snack);
                    var thu = GetFruitOrderByWeekday(snackOrder, DayOfWeek.Thursday, FruitProductType.Snack);
                    var fri = GetFruitOrderByWeekday(snackOrder, DayOfWeek.Friday, FruitProductType.Snack);
                    var sat = GetFruitOrderByWeekday(snackOrder, DayOfWeek.Saturday, FruitProductType.Snack);

                    if (mon.TotalQty != 0 || mon.Credit != 0)
                        snackReportList.Add(mon);
                    if (tue.TotalQty != 0 || tue.Credit != 0)
                        snackReportList.Add(tue);
                    if (wed.TotalQty != 0 || wed.Credit != 0)
                        snackReportList.Add(wed);
                    if (thu.TotalQty != 0 || thu.Credit != 0)
                        snackReportList.Add(thu);
                    if (fri.TotalQty != 0 || fri.Credit != 0)
                        snackReportList.Add(fri);
                    if (sat.TotalQty != 0 || sat.Credit != 0)
                        snackReportList.Add(sat);
                }

                snackReportList = snackReportList
                    .Where(r => r.OrderDate >= fromDate.Value)
                    .Where(r => r.OrderDate <= toDate.Value)
                    .ToList();
                
                var result = milkReportList
                    .Concat(fruitReportList)
                    .Concat(snackReportList)
                    .OrderBy(x => x.ClientName)
                    .ThenBy(x => x.Type)
                    .ThenByDescending(x => x.OrderDate)
                    .ThenBy(x => x.ProductCode)
                    .ToList();

                return result;
            }

            ReportMilkAndFruitViewModel GetMilkOrderByWeekday(MilkOrder order, DayOfWeek dayOfWeek)
            {
                var viewModel = new ReportMilkAndFruitViewModel()
                {
                    ID = Guid.NewGuid().ToString(),
                    SupplierName = order.OrderHeader.Supplier.Name,
                    Type = ReportType.Milk,
                    AccountNo = order.OrderHeader.Company.AccountNo,
                    ClientName = order.OrderHeader.Company.Name,
                    ProductCode = order.LineItemType == LineItemType.Product
                        ? order.Milk.ProductCode
                        : order.OrderHeader.DeliveryChargeCode,
                    Description = order.LineItemType == LineItemType.Product
                        ? order.Milk.Name
                        : order.OrderHeader.DeliveryChargeName,
                };

                switch (dayOfWeek)
                {
                    case DayOfWeek.Monday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value;
                        viewModel.TotalQty = order.Monday;
                        viewModel.Total = order.Monday * order.Price;
                        viewModel.Credit = order.Credits.Sum(c => c.Monday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Monday != 0).Select(c => c.MilkCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.MilkCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Tuesday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(1);
                        viewModel.TotalQty = order.Tuesday;
                        viewModel.Total = order.Tuesday * order.Price;
                        viewModel.Credit = order.Credits.Sum(c => c.Tuesday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Tuesday != 0).Select(c => c.MilkCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.MilkCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Wednesday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(2);
                        viewModel.TotalQty = order.Wednesday;
                        viewModel.Total = order.Wednesday * order.Price;
                        viewModel.Credit = order.Credits.Sum(c => c.Wednesday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Wednesday != 0).Select(c => c.MilkCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.MilkCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Thursday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(3);
                        viewModel.TotalQty = order.Thursday;
                        viewModel.Total = order.Thursday * order.Price;
                        viewModel.Credit = order.Credits.Sum(c => c.Thursday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Thursday != 0).Select(c => c.MilkCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.MilkCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Friday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(4);
                        viewModel.TotalQty = order.Friday;
                        viewModel.Total = order.Friday * order.Price;
                        viewModel.Credit = order.Credits.Sum(c => c.Friday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Friday != 0).Select(c => c.MilkCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.MilkCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Saturday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(5);
                        viewModel.TotalQty = order.Saturday;
                        viewModel.Total = order.Saturday * order.Price;
                        viewModel.Credit = order.Credits.Sum(c => c.Saturday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Saturday != 0).Select(c => c.MilkCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.MilkCreditHeader.Narrative));
                        break;
                    default:
                        throw new Exception("Wrong weekday");
                }

                return viewModel;
            }

            ReportMilkAndFruitViewModel GetFruitOrderByWeekday(FruitOrder order, DayOfWeek dayOfWeek, FruitProductType type)
            {
                var viewModel = new ReportMilkAndFruitViewModel()
                {
                    ID = Guid.NewGuid().ToString(),
                    SupplierName = type == FruitProductType.Fruit ? order.OrderHeader.Supplier?.Name : order.OrderHeader.SnackSupplier?.Name,
                    Type = ReportType.Fruit,
                    AccountNo = order.OrderHeader.Company.AccountNo,
                    ClientName = order.OrderHeader.Company.Name,
                    ProductCode = order.Fruit.ProductCode,
                    Description = order.Fruit.Name,
                };

                switch (dayOfWeek)
                {
                    case DayOfWeek.Monday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value;
                        viewModel.TotalQty = order.Monday;
                        viewModel.Total = order.Fruit.GetPrice(order.Monday, order.OrderHeader.CompanyID);
                        viewModel.Credit = order.Credits.Sum(c => c.Monday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Monday != 0).Select(c => c.FruitCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.FruitCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Tuesday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(1);
                        viewModel.TotalQty = order.Tuesday;
                        viewModel.Total = order.Fruit.GetPrice(order.Tuesday, order.OrderHeader.CompanyID);
                        viewModel.Credit = order.Credits.Sum(c => c.Tuesday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Tuesday != 0).Select(c => c.FruitCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.FruitCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Wednesday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(2);
                        viewModel.TotalQty = order.Wednesday;
                        viewModel.Total = order.Fruit.GetPrice(order.Wednesday, order.OrderHeader.CompanyID);
                        viewModel.Credit = order.Credits.Sum(c => c.Wednesday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Wednesday != 0).Select(c => c.FruitCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.FruitCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Thursday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(3);
                        viewModel.TotalQty = order.Thursday;
                        viewModel.Total = order.Fruit.GetPrice(order.Thursday, order.OrderHeader.CompanyID);
                        viewModel.Credit = order.Credits.Sum(c => c.Thursday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Thursday != 0).Select(c => c.FruitCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.FruitCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Friday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(4);
                        viewModel.TotalQty = order.Friday;
                        viewModel.Total = order.Fruit.GetPrice(order.Friday, order.OrderHeader.CompanyID);
                        viewModel.Credit = order.Credits.Sum(c => c.Friday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Friday != 0).Select(c => c.FruitCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.FruitCreditHeader.Narrative));
                        break;
                    case DayOfWeek.Saturday:
                        viewModel.OrderDate = order.OrderHeader.FromDate.Value.AddDays(5);
                        viewModel.TotalQty = order.Saturday;
                        viewModel.Total = order.Fruit.GetPrice(order.Saturday, order.OrderHeader.CompanyID);
                        viewModel.Credit = order.Credits.Sum(c => c.Saturday);
                        viewModel.Comments = string.Join(" | ", order.Credits.Where(c => c.Saturday != 0).Select(c => c.FruitCreditHeader.OrderHeader.FromDate.Value.ToString("(Week dd/MM/yy) ") + c.FruitCreditHeader.Narrative));
                        break;
                    default:
                        throw new Exception("Wrong weekday");
                }

                return viewModel;
            }
        }

        public ActionResult NewExportMilkAndFruitCSV([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = NewReadMilkAndFruitReport(companyID, dateType, fromDate, toDate);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\"\n",
                    "Order Date", "OG Account Number", "AH Reference", "Client Name", "Product Code", "Description", "Volume", "Value", "Supplier", "Credits / Changes (+ or -)", "Comments"
                ));

            // Append to StringBuilder.
            foreach (var q in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\"\n",
                        q.OrderDate.ToString("dd/MM/yyyy"),
                        q.AccountNo,
                        "",
                        q.ClientName,
                        q.ProductCode,
                        q.Description,
                        q.TotalQty,
                        q.Total,
                        q.SupplierName,
                        q.Credit,
                        q.Comments.Replace("\"", "\"\"")
                        ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Milk & Fruit Export {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        public ActionResult Milk()
        {
            return View();
        }

        public JsonResult ReadMilk([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadMilkReport(companyID, dateType, fromDate, toDate);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportMilkCSV([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadMilkReport(companyID, dateType, fromDate, toDate);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
                    "", "", "Quantity (units)", "Invoiced Turnover", "credits / changes (+ or -)"
                ));

            // Append to StringBuilder.
            foreach (var q in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
                        q.AccountNo,
                        q.ProductName,
                        q.TotalQty,
                        q.Total,
                        q.Credit
                        ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Milk Export {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportMilkViewModel> ReadMilkReport(Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }
            using (var context = new SnapDbContext())
            {
                var viewModels = context.MilkOrders
                                        .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                        .Where(x => companyID == null || x.OrderHeader.CompanyID == companyID)
                                        .Where(x => x.OrderHeader.FromDate >= fromDate && x.OrderHeader.ToDate < toDate)
                                        .OrderByDescending(x => x.OrderHeader.FromDate)
                                        .ThenBy(x => x.OrderHeader.OrderNumber)
                                        .GroupBy(x => new
                                        {
                                            ProductID = x.MilkProductID,
                                            ProductName = x.Milk.Name,
                                            ProductCode = x.Milk.ProductCode,
                                            CompanyID = x.OrderHeader.CompanyID,
                                            CompanyName = x.OrderHeader.Company.Name,
                                            AccountNo = x.OrderHeader.Company.AccountNo
                                        })
                                        .ToList()
                                        .Select(g => new ReportMilkViewModel
                                        {
                                            ID = Guid.NewGuid().ToString(),
                                            AccountNo = String.Format("{0} / {1}", g.Key.AccountNo, g.Key.CompanyName),
                                            ProductName = String.Format("{0}", g.Key.ProductName),
                                            Total = g.Sum(x => x.TotalNet),
                                            TotalQty = g.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday),
                                            Credit = g.Sum(x => x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday))//x.Credits.Any() ? x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday) : 0
                                        }).ToList();
                return viewModels;
            }
        }

        public ActionResult Fruit()
        {
            return View();
        }

        public JsonResult ReadFruit([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadFruitReport(companyID, dateType, fromDate, toDate);
            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult ExportFruitCSV([DataSourceRequest] DataSourceRequest request, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            var viewModels = ReadFruitReport(companyID, dateType, fromDate, toDate);

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
                    "", "", "Quantity (units)", "Invoiced Turnover", "credits / changes (+ or -)"
                ));

            // Append to StringBuilder.
            foreach (var q in viewModels)
            {
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\n",
                        q.AccountNo,
                        q.ProductName,
                        q.TotalQty,
                        q.Total,
                        q.Credit
                        ));
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Fruit Export {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private List<ReportFruitViewModel> ReadFruitReport(Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }
            using (var context = new SnapDbContext())
            {
                var viewModels = context.FruitOrders
                                        .Where(x => x.OrderHeader.OrderMode == OrderMode.History)
                                        .Where(x => companyID == null || x.OrderHeader.CompanyID == companyID)
                                        .Where(x => x.OrderHeader.FromDate >= fromDate && x.OrderHeader.ToDate < toDate)
                                        .OrderByDescending(x => x.OrderHeader.FromDate)
                                        .ThenBy(x => x.OrderHeader.OrderNumber)
                                        .GroupBy(x => new
                                        {
                                            ProductID = x.FruitProductID,
                                            ProductName = x.Fruit.Name,
                                            ProductCode = x.Fruit.ProductCode,
                                            CompanyID = x.OrderHeader.CompanyID,
                                            CompanyName = x.OrderHeader.Company.Name,
                                            AccountNo = x.OrderHeader.Company.AccountNo
                                        })
                                        .ToList()
                                        .Select(g => new ReportFruitViewModel
                                        {
                                            ID = Guid.NewGuid().ToString(),
                                            AccountNo = String.Format("{0} / {1}", g.Key.AccountNo, g.Key.CompanyName),
                                            ProductName = String.Format("{0}", g.Key.ProductName),
                                            Total = g.Sum(x => x.TotalNet),
                                            TotalQty = g.Sum(x => x.Monday + x.Tuesday + x.Wednesday + x.Thursday + x.Friday + x.Saturday),
                                            Credit = g.Sum(x => x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday))//x.Credits.Any() ? x.Credits.Sum(c => c.Monday + c.Tuesday + c.Wednesday + c.Thursday + c.Friday + c.Saturday) : 0
                                        }).ToList();
                return viewModels;
            }
        }

        public ActionResult FruitStanding()
        {
            return View();
        }

        public ActionResult FruitOneOff()
        {
            return View();
        }

        public ActionResult MilkStanding()
        {
            return View();
        }

        public ActionResult MilkOneOff()
        {
            return View();
        }

        public ActionResult RequestedAccounts()
        {
            return View();
        }

        public ActionResult FirstDeliveryDate()
        {
            return View();
        }
        
        public ActionResult IntermittentNextWeek()
        {
            return View();
        }

        public ActionResult Performance()
        {
            return View();
        }
        
        public ActionResult ExportCustomerNotes()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("\"Name\",\"Account\",\"Notes\",\"{3}\"\n");

            using (var context = new SnapDbContext())
            {
                var repo = new CompanyRepository();
                var customers = repo.Read()
                    .Where(c => c.Name != "NuIngredient")
                    .OrderByDescending(c => c.Name)
                    .Select(c => new
                    {
                        Name = c.Name,
                        Account = c.AccountNo,
                        Notes = c.Round,
                    });

                foreach (var customer in customers)
                {
                    builder.Append($"\"{customer.Name}\",\"{customer.Account}\",\"{customer.Notes}\"\n");
                }
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Customer Note Export {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        public async Task<ActionResult> ExportCustomerList()
        {
            var helper = new CustomerListReportHelper();
            var reportCsv = await helper.GetReportCsvAsync();
            var fileContent = Encoding.ASCII.GetBytes(reportCsv);
            var contentType = "text/csv";
            var fileDownloadName = $"Customer List {DateTime.Today:dd-MM-yyyy}.csv";
            return File(fileContent, contentType, fileDownloadName);
        }

    }

}