﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class MessageGridController : GridController<Message, MessageGridViewModel>
    {

        public MessageGridController()
            : base(new MessageRepository(), new MessageGridMapper())
        {

        }

        [HttpPost]
        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _repo.Read()
                              .OrderByDescending(x => x.CreatedDate);

            var viewModels = models.Skip((request.Page - 1) * request.PageSize)
                                   .Take(request.PageSize)
                                   .ToList()
                                   .Select(s => _mapper.MapToViewModel(s));

            var result = viewModels.ToDataSourceResult(request);
            result.Total = models.Count();

            return Json(result);
        }

    }
}