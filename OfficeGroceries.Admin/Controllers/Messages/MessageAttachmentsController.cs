﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MessageAttachmentsController : GridController<MessageAttachment, MessageAttachmentGridViewModel>
    {
        public MessageAttachmentsController()
            : base(new MessageAttachmentRepository(), new MessageAttachmentGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, IEnumerable<Guid> ids, Guid? messageID)
        {
            ids = ids ?? new List<Guid>();

            var models = messageID != null ?
                 _repo.Read().Where(a => a.MessageID == messageID.Value)
                 : _repo.Read().Where(a => ids.Contains(a.ID));

            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public ActionResult UploadAttachment(IEnumerable<HttpPostedFileBase> uploadedAttachments, Guid messageID)
        {
            var toCreate = new List<MessageAttachment>();
            if (uploadedAttachments != null)
            {
                foreach (var file in uploadedAttachments)
                {
                    var loc = SaveFile(file, $"/MessageAttachments/{messageID.ToString()}/", Path.GetFileNameWithoutExtension(file.FileName));
                    var newAttachment = new MessageAttachment()
                    {
                        FileName = file.FileName,
                        FilePath = loc,
                        Created = DateTime.Now,
                    };
                    toCreate.Add(newAttachment);
                }
                _repo.Create(toCreate);
            }

            return Json(toCreate);
        }

        public ActionResult DeleteAttachment(Guid ID)
        {
            var model = _repo.Find(ID);
            DeleteFile(model.FilePath);
            _repo.Delete(model);
            return Json(new { Success = true });
        }

    }

}