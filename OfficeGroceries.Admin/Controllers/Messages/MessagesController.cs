﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MessagesController : EntityController<Message, MessageViewModel>
    {
        public MessagesController()
            : base(new MessageRepository(), new MessageViewMapper())
        {

        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _New()
        {
            return PartialView(new MessageViewModel() { ID = Guid.NewGuid().ToString() });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _New(MessageViewModel viewModel, string attachmentIDs)
        {
            if (!ModelState.IsValid)
                return PartialView(viewModel);

            var model = _mapper.MapToModel(viewModel);
            model.CreatedDate = DateTime.Now;

            _repo.Create(model);

            viewModel.ID = model.ID.ToString();
            UpdateMessageTargets(viewModel);

            attachmentIDs = attachmentIDs ?? "";
            if (!string.IsNullOrEmpty(attachmentIDs))
            {
                var attIDs = attachmentIDs.Split(',').Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => Guid.Parse(s)).ToList();
                UpdateAttachments(attIDs, model.ID);
            }

            SendMessage(viewModel);

            return Json(new { ID = model.ID });
        }

        [HttpGet]
        public ActionResult _Edit(Guid id)
        {
            var model = _repo.Find(id);
            var viewModel = _mapper.MapToViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _Edit(MessageViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return PartialView(viewModel);

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);

            _repo.Update(model);

            UpdateMessageTargets(viewModel);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
        }

        private void UpdateMessageTargets(MessageViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.MessageTargets)
                             .FirstOrDefault();

            var oldList = model != null ? model.MessageTargets.Select(x => x.TargetID.ToString()) : new List<string>();
            var newList = viewModel.MessageTargets ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new MessageTargetRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.TargetID.ToString() == id && x.MessageID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new MessageTarget() { TargetID = int.Parse(id), MessageID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateAttachments(List<Guid> attachmentIDs, Guid messageID)
        {
            var repo = new MessageAttachmentRepository();
            var attachments = repo.Read().Where(a => attachmentIDs.Contains(a.ID));
            attachments.Each(a => a.MessageID = messageID);
            repo.Update(attachments);
        }
        private void SendMessage(MessageViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.MessageTargets)
                             .Include(x => x.Attachments)
                             .FirstOrDefault();

            if (model == null) return;

            var emailsToList = new List<string>();
            foreach (var target in model.MessageTargets)
            {
                emailsToList.AddRange(EmailsToList(target.TargetID));
            }

            SendMessage(viewModel.Title, viewModel.Content, emailsToList, model.Attachments.ToList());
            
        }

        private void SendMessage(string title, string content, List<string> emailsTo, List<MessageAttachment> attachments)
        {
            if (emailsTo.Count == 0) return;

            var success = 0;
            const int batchNumber = 499; // microsoft only allows 500 recipients maximum.
            while (success < emailsTo.Count)
            {
                var mailMessage = SystemMessageEmailHelper.GetEmail(title, content, emailsTo.Skip(success).Take(batchNumber).ToList(), attachments);

                EmailService.SendEmail(mailMessage, Server);

                success += batchNumber;
            }
        }

        private List<string> EmailsToList(int target)
        {
            var customerRepo = new CompanyRepository();
            var customerList = customerRepo.Read().ToList();

            var query = customerList.AsQueryable();
            
            switch (target)
            {
                case TargetValues.Milk:
                    query = query.Where(x => x.Method_HasSupplier(SupplierType.Milk));
                    break;
                case TargetValues.Fruits:
                    query = query.Where(x => x.Method_HasSupplier(SupplierType.Fruits));
                    break;
                case TargetValues.DryGoods:
                    query = query.Where(x => x.Method_HasSupplier(SupplierType.DryGoods));
                    break;
                default:
                    return new List<string>();
            }

            var clients = query
                .Where(x => x.Email != null || x.SecondaryEmail != null)
                .Select(x => new
                {
                    x.Email,
                    x.SecondaryEmail
                })
                .ToList();

            var emailList = new List<string>();
            
            foreach (var client in clients)
            {
                // use secondary email for sending bulk message if it exists, otherwise use primary email
                if (!string.IsNullOrWhiteSpace(client.SecondaryEmail))
                {
                    emailList.Add(client.SecondaryEmail);
                }
                else if (!string.IsNullOrWhiteSpace(client.Email))
                {
                    emailList.Add(client.Email);
                }
            }
            
            var results = emailList
                .SelectMany(m => m.Split(';'))
                .Select(m => m.Trim())
                .Where(m => !string.IsNullOrWhiteSpace(m))
                .Where(m => Fruitful.Email.Emailer.IsValidEmail(m))
                .ToList();
            
            return results;
        }


    }

}