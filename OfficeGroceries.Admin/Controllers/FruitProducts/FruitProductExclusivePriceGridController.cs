﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class FruitProductExclusivePriceGridController : GridController<FruitProductExclusivePrice, FruitProductExclusivePriceViewModel>
    {

        public FruitProductExclusivePriceGridController()
            : base(new FruitProductExclusivePriceRepository(), new FruitProductExclusivePriceMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid productID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.ProductID == productID)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, FruitProductExclusivePriceViewModel viewModel)
        {
            var result = base.Create(request, viewModel);

            var fruitID = Guid.Parse(viewModel.ProductID);
            var companyID = Guid.Parse(viewModel.Customer.ID);

            RefreshFruitPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(fruitID, companyID);

            return result;
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, FruitProductExclusivePriceViewModel viewModel)
        {
            var result = base.Update(request, viewModel);

            var fruitID = Guid.Parse(viewModel.ProductID);
            var companyID = Guid.Parse(viewModel.Customer.ID);

            RefreshFruitPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(fruitID, companyID);

            return result;
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, FruitProductExclusivePriceViewModel viewModel)
        {
            var result = base.Destroy(request, viewModel);

            var fruitID = Guid.Parse(viewModel.ProductID);
            var companyID = Guid.Parse(viewModel.Customer.ID);

            RefreshFruitPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(fruitID, companyID);

            return result;
        }

    }
}