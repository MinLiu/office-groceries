﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class FruitProductTagPriceGridController : GridController<FruitProductTagPrice, FruitProductTagPriceViewModel>
    {

        public FruitProductTagPriceGridController()
            : base(new FruitProductTagPriceRepository(), new FruitProductTagPriceMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid productID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.ProductID == productID)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, FruitProductTagPriceViewModel viewModel)
        {
            var result = base.Create(request, viewModel);

            RefreshFruitPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.ProductID));

            return result;
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, FruitProductTagPriceViewModel viewModel)
        {
            var result = base.Update(request, viewModel);

            RefreshFruitPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.ProductID));

            return result;
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, FruitProductTagPriceViewModel viewModel)
        {
            var result = base.Destroy(request, viewModel);

            RefreshFruitPriceHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.ProductID));

            return result;
        }
    }
}