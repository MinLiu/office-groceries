﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize(Roles="NI-User")]
    public class FruitProductCategoryManagementController : GridController<FruitProductCategory, FruitProductCategoryViewModel>
    {

        public FruitProductCategoryManagementController()
            : base(new FruitProductCategoryRepository(), new FruitProductCategoryMapper())
        {

        }

        // GET: StockLocations
        public ActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, FruitProductCategoryViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _mapper.MapToModel(viewModel);
                _repo.Create(model);
                viewModel.ID = model.ID.ToString();
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = _repo.Read()
                              .Where(x => x.Deleted != true)
                              .OrderBy(x => x.SortPos).ThenBy(x =>x.Name)
                              .ToList()
                              .Select(x => _mapper.MapToViewModel(x))
                              .ToTreeDataSourceResult(
                                    request,
                                    e => e.ID,
                                    e => e.ParentID,
                                    e => e
                              );
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}