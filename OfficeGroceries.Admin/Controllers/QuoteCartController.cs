﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    public class QuoteCartsController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult _QuoteCart()
        {
            var dbContext = new SnapDbContext();
            var shoppingCartService = new ShoppingCartService(dbContext);
            var milkOrderService = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), dbContext);
            var fruitOrderService = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), dbContext);

            var viewModel = new QuoteCartViewModel()
            {
                DryGoodsQuote = shoppingCartService.GetProspectQuote(ProspectToken).GroupBy(x => x.Product.Supplier).ToList().Select(g => new CartItemsBySuppliers
                {
                    CartItems = g.ToList().Select(i => new ShoppingCartItemMapper().MapToViewModel(i)).ToList(),
                    Live = false,
                    Supplier = new SupplierMapper().MapToViewModel(g.Key),
                    SubNet = g.Sum(i => i.Qty * i.Product.Price),
                    SubVAT = g.Sum(i => i.Qty * i.Product.Price * i.Product.VAT),
                    SubGross = g.Sum(i => i.Qty * (i.Product.Price + i.Product.Price * i.Product.VAT))
                }).ToList(),//new List<CartItemsBySuppliers>(),
                MilkQuote = milkOrderService.GetProspectOrderItems(ProspectToken).Select(x => new MilkOrderMapper().MapToViewModel(x)).ToList(),
                FruitQuote = fruitOrderService.GetProspectOrderItems(ProspectToken).Select(x => new FruitOrderMapper().MapToViewModel(x)).ToList()
            };

            return PartialView(viewModel);
        }

    }

}