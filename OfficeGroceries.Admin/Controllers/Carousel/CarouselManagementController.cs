﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize(Roles ="NI-User")]
    public class CarouselManagementController : EntityController<Carousel, CarouselNIViewModel>
    {
      
        public CarouselManagementController()
            : base(new CarouselRepository(), new CarouselNIMapper())
        {

        }

        // GET
        public ActionResult Index(string ID)
        {
            return View();
        }

        [HttpGet]
        public ActionResult _NewCarousel()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult _NewCarousel(CarouselNIViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            return Json(new { ID = model.ID });
        }

        [HttpPost]
        public ActionResult _EditCarousel(CarouselNIViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            viewModel = _mapper.MapToViewModel(model);

            ViewBag.Saved = true;

            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult _EditCarousel(Guid id)
        {
            var viewModel = _mapper.MapToViewModel(_repo.Find(id));
            return PartialView(viewModel);
        }

        public ActionResult UploadCarouselImage(HttpPostedFileBase uploadedImage, string id)
        {
            const string relativePath = "/Images/";

            if (uploadedImage == null) return Json(new { status = "error", message = "No Image uploaded." }, "text/plain");
            if (!uploadedImage.ContentType.Contains("image")) return Json(new { status = "error", message = "Uploaded file is not an Image." }, "text/plain");
            if (uploadedImage.ContentLength > 1 * 512 * 1024) return Json(new { status = "error", message = "Uploaded file is too big." }, "text/plain");


            var model = _repo.Find(Guid.Parse(id));

            //Save file on Server
            string loc = SaveFile(uploadedImage, relativePath);

            //Update the image URL of the product    
            model.ImageURL = loc;
            _repo.Update(model, new string[] { "ImageURL" });

            //return the image loc. Javascript updates the image in view
            return Json(new { status = "success", id = id }, "text/plain");

        }

        public ActionResult RemoveCarouselImage(string id)
        {
            var model = _repo.Find(Guid.Parse(id));
            model.ImageURL = null;
            _repo.Update(model, new string[] { "ImageURL" });

            return Json(new { status = "success", id = id }, "text/plain");
        }
    }
    
}