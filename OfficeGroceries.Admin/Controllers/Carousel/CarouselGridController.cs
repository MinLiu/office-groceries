﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize(Roles ="NI-User")]
    public class CarouselGridController : GridController<Carousel, CarouselNIViewModel>
    {
        public CarouselGridController()
            : base(new CarouselRepository(), new CarouselNIMapper())
        {

        }

    }
    
}