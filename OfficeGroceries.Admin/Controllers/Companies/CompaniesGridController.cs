﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class CompaniesGridController :GridController<Company, CompanyGridViewModel>
    {
      
        public CompaniesGridController()
            : base(new CompanyRepository(), new CompanyGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText)
        {
            filterText = filterText?.Trim() ?? "";
            
            var query = _repo.Read()
                .Where(c => c.Name != "NuIngredient");

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                query = query.Where(c => c.Name.Contains(filterText)
                                         || c.BillingCompanyName.Contains(filterText)
                                         || c.AccountNo.Contains(filterText)
                                         || c.Address1.Contains(filterText)
                                         || c.Postcode.Contains(filterText)
                                         || c.BillingPostcode.Contains(filterText)
                                         || c.Email.Contains(filterText)
                                         || c.BillingEmail.Contains(filterText)
                );
            }

            var total = query.Count();

            var take = request.PageSize;
            var skip = (request.Page - 1) * request.PageSize;
            if (skip >= total)
            {
                skip = 0;
            }

            var viewModels = query
                .OrderByDescending(c => c.SignupDate)
                .Skip(skip)
                .Take(take)
                .ToList()
                .Select(s => _mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = total,
            };

            return Json(result);
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, CompanyGridViewModel viewModel)
        {
            var helper = IsCompanyDeletableHelper.New();

            var result = helper.IsDeletable(Guid.Parse(viewModel.ID));
            if (result.IsDeletable)
            {
                var model = new Company { ID = Guid.Parse(viewModel.ID) };
                _repo.Delete(model);

                return Json(new { Success = true });
            }
            else
            {
                return Json(new { Success = false, Message = result.Message });
            }
        }

        public FileResult Export(bool all = false)
        {
            var helper = ExportCompanyHelper.New();
            var outputByteArray= helper
                .BuildSpreadSheet(all)
                .Export();
            return File(outputByteArray,   //The binary data of the XLS file
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",    //MIME type of Excel files
                        string.Format("Customer Export {0}.xlsx", DateTime.Today.ToString("dd-MM-yyyy")));   //Suggested file name in the "Save as" dialog which will be displayed to the end user
        }

        public FileResult ExportMultiTenant()
        {
            var outputByteArray = ExportMultiTenantCompanyHelper.New()
                .BuildSpreadSheet()
                .Export();
            return File(outputByteArray,   //The binary data of the XLS file
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",    //MIME type of Excel files
                        string.Format("Multi-Tenant Building Export {0}.xlsx", DateTime.Today.ToString("dd-MM-yyyy")));   //Suggested file name in the "Save as" dialog which will be displayed to the end user
        }
        
        public FileResult ExportMultiSites()
        {
            var outputByteArray = ExportMultiSitesCompanyHelper.New()
                .BuildSpreadSheet()
                .Export();
            return File(outputByteArray,   //The binary data of the XLS file
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",    //MIME type of Excel files
                string.Format("Multi-Sites Customer Export {0}.xlsx", DateTime.Today.ToString("dd-MM-yyyy")));   //Suggested file name in the "Save as" dialog which will be displayed to the end user
        }

    }
}