﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class InactiveCompaniesGridController :GridController<Company, InactiveCompanyGridViewModel>
    {
        public InactiveCompaniesGridController()
            : base(new CompanyRepository(), new InactiveCompanyGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText, int month)
        {
            var query = GetInactiveCompanies(filterText, month);
            
            var total = query.Count();

            var take = request.PageSize;
            var skip = (request.Page - 1) * request.PageSize;
            if (skip >= total)
            {
                skip = 0;
            }

            var viewModels = query
                .OrderByDescending(c => c.SignupDate)
                .Skip(skip)
                .Take(take)
                .ToList()
                .Select(s => _mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = total,
            };

            return Json(result);
        }
        
        public FileResult Export(string filterText, int month)
        {
            var query = GetInactiveCompanies(filterText, month);

            var data = query.OrderBy(company => company.Name)
                .Select(company => new
                {
                    company.Name,
                    company.CompanyRegistration, // contact name
                    company.Email,
                    company.Telephone,
                });
            
            var csv = new StringBuilder();
            csv.AppendLine("Name,Contact Name,Email,Telephone");
            foreach (var item in data)
            {
                csv.AppendLine($"{SafeString(item.Name)},{SafeString(item.CompanyRegistration)},{SafeString(item.Email)},{SafeString(item.Telephone)}");
            }
            
            return File(Encoding.UTF8.GetBytes(csv.ToString()), "text/csv", "InactiveCompanies.csv");
        }

        [HttpPost]
        public async Task<ActionResult> DeleteAll(string filterText, int month)
        {
			if (TotalToDelete > 0)
			{
				return BadRequestResult($"{TotalToDelete} companies are currently being deleted. Please wait for the process to complete.");
			}

            var companyIds = await GetInactiveCompanies(filterText, month).OrderBy(x => x.SignupDate).Select(x => x.ID).ToListAsync();

            // delete companies in a separate task
            // the task will run in the background and return immediately to the user
            _ = Task.Run(() => DeleteCompanies(companyIds));

            return OkResult();
        }

		private static int TotalToDelete = 0;

        private async Task DeleteCompanies(List<Guid> companyIds)
        {
			TotalToDelete = companyIds.Count;

            foreach (var companyId in companyIds)
            {
                try
                {
                    var repo = new CompanyRepository();
                    var company = await repo.FindAsync(companyId);
                    if (company == null) continue;
					Console.WriteLine($"Deleting company {companyId}, {company.AccountNo}, {company.Name}");
                    repo.Delete(company);
                }
                catch (Exception e)
                {
                    // log error
					Console.WriteLine($"Error deleting company {companyId}");
					Console.WriteLine(e);
                }
				finally
				{
					TotalToDelete--;
				}
            }
        }

        private string SafeString(string value)
        {
            // remove commas and double quotes
            value = value?.Replace(",", " ").Replace("\"", " ") ?? "";
                
            // add leading ` to prevent Excel from interpreting as a formula if they are numbers
            if (double.TryParse(value, out _))
            {
                // add a space after the 3rd character to prevent Excel from interpreting as a date
                if (value.Length > 3)
                {
                    value = value.Insert(3, " ");
                }
                else
                {
                    value = value.Insert(1, " ");
                }
            }
                
            // enclose in double quotes
            value = $"\"{value}\"";
            return value;
        }
    
        private IQueryable<Company> GetInactiveCompanies(string filterText, int month)
        {
            filterText = filterText?.Trim() ?? "";
                
            var testCompanyId = new Guid("f162322d-ba42-4426-b53f-d5476d80d75a"); // this is a test company, so never show it in the grid and never delete it
            
            var query = _repo.Read()
                .Where(c => c.Name != "NuIngredient" && c.ID != testCompanyId);

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                query = query.Where(c => c.Name.Contains(filterText)
                                         || c.BillingCompanyName.Contains(filterText)
                                         || c.AccountNo.Contains(filterText)
                                         || c.Address1.Contains(filterText)
                                         || c.Postcode.Contains(filterText)
                                         || c.BillingPostcode.Contains(filterText)
                                         || c.Email.Contains(filterText)
                                         || c.BillingEmail.Contains(filterText)
                );
            }

            if (month == 0)
            {
                month = 6;
            }
                
            var date = DateTime.Now.AddMonths(-month);
                
            query = query
                .Where(x => x.SignupDate <= date &&
                            x.CompanySuppliers.All(s => s.EndDate <= date) &&
	                        x.MilkOrders.Where(o => o.OrderMode == OrderMode.History).All(o => o.FromDate <= date) &&
	                        x.FruitOrders.Where(o => o.OrderMode == OrderMode.History).All(o => o.FromDate <= date) &&
							x.MonthlyPoNumbers.All(p => p.Created <= date) &&
							x.IssueEmails.All(i => i.CreatedDateUtc <= date) &&
                            x.OneOffOrders.All(o => o.Created <= date));
            
            return query;
        }
    }

}