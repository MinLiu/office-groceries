﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using Fruitful.Email;
using Fruitful.PDF;
using NuIngredient.Models;


namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class PoCalculatorController : BaseController
    {
        [HttpGet]
        public ActionResult _PoCalculator(Guid companyId)
        {
            return PartialView(new SendPoPageViewModel
            {
                CompanyId = companyId,
                DateFrom = DateTime.Today,
                DateTo = DateTime.Today.AddMonths(1)
            });
        }
        
        [HttpPost]
        public async Task<ActionResult> Calculate(SendPoPageViewModel viewModel)
        {
            var helper = new PoCalculationHelper(new SnapDbContext());
            var result = await helper.Calculate(viewModel.CompanyId, viewModel.DateFrom, viewModel.DateTo);
            return PartialView("_PoResult", result);
        }

        [HttpPost]
        public async Task<string> SendPo(WeeklyAverageReport viewModel)
        {
            var otherEmails = viewModel.OtherEmails?.Split(';').Select(x => x.Trim()).ToList() ?? new List<string>();

            var toEmailList = otherEmails.Concat(viewModel.ToEmailList ?? new List<string>()).ToList();
            
            if (!toEmailList.Any())
            {
                return "No recipients";
            }

            var helper = new PoCalculationHelper(new SnapDbContext());
            var result = await helper.Calculate(viewModel.CompanyID, viewModel.FromDate, viewModel.ToDate);
            result.Company = new CompanyRepository().Find(viewModel.CompanyID);
            result.ToEmailList = toEmailList;
            
            var success = SendEmail(result);
            if (success)
            {
                return "PO is successfully sent to " + string.Join(", ", toEmailList);
            }
            else
            {
                return "Failed to send email";
            }
        }
        
        private bool SendEmail(WeeklyAverageReport viewModel)
        {
            var emailer = new Emailer(new SnapDbContext());
            var emailAttachments = new List<Attachment>();

            var template = new PDFTemplate
            {
                headerView = "",
                bodyView = "_PdfBody",
                footerView = "_PdfFooter",
                pagerView = "",
                showHeaderEveryPage = false,
                showFooterEveryPage = true
            };

            var pdfBytes = PDFService<WeeklyAverageReport>.GenerateOrder(viewModel, ControllerContext, template);

            var pdfAttachment = new Attachment(new MemoryStream(pdfBytes), "Quotation from Office-Groceries.pdf");
            emailAttachments.Add(pdfAttachment);
        
            var body =
                $"<p style='white-space: pre-line;'>Dear {viewModel.Company.Name},<br/><br/>Please find the attached PO quotation.</p><p>Best Regards,<br /> The Accounts Team";
            MailMessage mailMessage = emailer.CreateMailMessage(viewModel.ToEmailList, "Quotation from Office-Groceries", body, emailAttachments);
            var success = EmailService.SendEmail(mailMessage, Server);

            return success;
        }
    }
}