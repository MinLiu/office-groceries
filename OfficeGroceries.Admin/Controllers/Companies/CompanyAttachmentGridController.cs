﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    public class CompanyAttachmentGridController : GridController<CompanyAttachment, AttachmentViewModel>
    {
        public CompanyAttachmentGridController()
            : base(new CompanyAttachmentRepository(), new AttachmentGridMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid companyID)
        {
            var list = _repo.Read().Where(p => p.CompanyID == companyID).ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Destroy(DataSourceRequest request, AttachmentViewModel viewModel)
        {
            try
            {
                var physicalPath = Server.MapPath(viewModel.FilePath);

                if (System.IO.File.Exists(physicalPath))
                {
                    System.IO.File.Delete(physicalPath);
                }
            }
            catch { }
            return base.Destroy(request, viewModel);
        }


        public ActionResult Upload(IEnumerable<HttpPostedFileBase> files, Guid companyID)
        {
            try
            {
                if (files == null)
                    throw new Exception("No file");

                foreach (var file in files)
                {
                    //If larger then 20mbs then skip
                    if (file.ContentLength > 20 * 1024 * 1024)
                        throw new Exception($"{file.FileName} is larger than 20MB");
                }
                
                foreach (var file in files)
                {
                    //Save file on Server
                    var filePath = SaveAttachment(file, companyID);

                    var upload = new CompanyAttachment { CompanyID = companyID, Filename = file.FileName, FilePath = filePath };
                    _repo.Create(upload);
                }

                // Return an empty string to signify success
                return Content("");
            
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        private string SaveAttachment(HttpPostedFileBase file, Guid companyID)
        {
            var relativePath = $"/CompanyAttachments/{companyID}/";
            var physicalFolderPath = Server.MapPath(relativePath);

            if (!Directory.Exists(physicalFolderPath))
                Directory.CreateDirectory(physicalFolderPath);
            
            //Save file on Server
            var extension = Path.GetExtension(file.FileName);
            var uniqueName = Guid.NewGuid() + extension;
            var physicalSavePath = physicalFolderPath + uniqueName;
            var location = relativePath + uniqueName;
            file.SaveAs(physicalSavePath);

            return location;
        }

    }
}