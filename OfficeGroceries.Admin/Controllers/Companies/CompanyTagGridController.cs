﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class CompanyTagItemGridController : GridController<CompanyTagItem, CompanyTagItemViewModel>
    {

        public CompanyTagItemGridController()
            : base(new CompanyTagItemRepository(), new CompanyTagItemMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid companyID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.CompanyID == companyID)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, CompanyTagItemViewModel viewModel)
        {
            var result = base.Create(request, viewModel);

            RefreshMilkPriceByCustomerHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.CompanyID));
            RefreshFruitPriceByCustomerHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.CompanyID));

            return result;
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, CompanyTagItemViewModel viewModel)
        {
            var result = base.Update(request, viewModel);

            RefreshMilkPriceByCustomerHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.CompanyID));
            RefreshFruitPriceByCustomerHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.CompanyID));

            return result;
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, CompanyTagItemViewModel viewModel)
        {
            var result = base.Destroy(request, viewModel);

            RefreshMilkPriceByCustomerHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.CompanyID));
            RefreshFruitPriceByCustomerHelper.New(new SnapDbContext()).RefreshOrderPrice(Guid.Parse(viewModel.CompanyID));

            return result;
        }
    }
}