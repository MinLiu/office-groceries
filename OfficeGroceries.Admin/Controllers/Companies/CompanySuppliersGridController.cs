﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class CompanySuppliersGridController : BaseController
    {
        private readonly CompanySupplierRepository _repo;
        private readonly CompanySupplierMapper _mapper;

        public CompanySuppliersGridController()
        {
            _repo = new CompanySupplierRepository();
            _mapper = new CompanySupplierMapper();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request, Guid companyID)
        {
            var viewModels = _repo.Read()
                                  .Where(cs => cs.CompanyID == companyID)
                                  .ToList()
                                  .Select(cs => _mapper.MapToViewModel(cs));

            return Json(viewModels.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Destroy([DataSourceRequest] DataSourceRequest request, CompanySupplierViewModel viewModel)
        {
                // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
                var model = new CompanySupplier { ID = Guid.Parse(viewModel.ID) };
                _repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
    }
    
}