﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Net.Mail;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class CalendarsController : BaseController
    {

        public ActionResult RequestedAccounts()
        {
            return View();
        }

        public ActionResult FirstDeliveryDate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ReadOnWaiting([DataSourceRequest] DataSourceRequest request, Guid? companyID, string filterText)
        {
            var results = new List<CalendarEventViewModel>();

            using (var context = new SnapDbContext())
            {
                // wait for Milk supplier
                var milkWaiting = context.Companies.Where(x => x.AskingForMilkSupplier != null)
                                                   .Where(x => companyID == null || x.ID == companyID)
                                                   .Where(x => filterText == null || filterText == "" || x.Name.ToLower().Contains(filterText.ToLower()))
                                                   .ToList()
                                                   .Select(x => new CalendarEventViewModel()
                                                   {
                                                       CustomerName = x.Name,
                                                       EventType = CalendarEventType.Milk,
                                                       Title = x.Name + " - Milk",
                                                       Description = "Waiting For Milk Supplier",
                                                       IsAllDay = true,
                                                       Start = x.AskingForMilkSupplier.Value,
                                                       End = x.AskingForMilkSupplier.Value,
                                                       Email = x.Email,
                                                       Telephone = x.Telephone,
                                                       PostCode = x.Postcode,
                                                       Address = x.Address1,
                                                       DeliveryInstruction = x.DeliveryInstruction
                                                   });

                var fruitsWaiting = context.Companies.Where(x => x.AskingForFruitSupplier != null)
                                                     .Where(x => companyID == null || x.ID == companyID)
                                                     .Where(x => filterText == null || filterText == "" || x.Name.ToLower().Contains(filterText.ToLower()))
                                                     .ToList()
                                                     .Select(x => new CalendarEventViewModel()
                                                     {
                                                         CustomerName = x.Name,
                                                         EventType = CalendarEventType.Fruits,
                                                         Title = x.Name + " - Fruits",
                                                         Description = "Waiting For Fruits Supplier",
                                                         IsAllDay = true,
                                                         Start = x.AskingForFruitSupplier.Value,
                                                         End = x.AskingForFruitSupplier.Value,
                                                         Email = x.Email,
                                                         Telephone = x.Telephone,
                                                         PostCode = x.Postcode,
                                                         Address = x.Address1,
                                                         DeliveryInstruction = x.DeliveryInstruction,
                                                     });
                
                var snackWaiting = context.Companies.Where(x => x.AskingForSnackSupplier != null)
                    .Where(x => companyID == null || x.ID == companyID)
                    .Where(x => filterText == null || filterText == "" || x.Name.ToLower().Contains(filterText.ToLower()))
                    .ToList()
                    .Select(x => new CalendarEventViewModel()
                    {
                        CustomerName = x.Name,
                        EventType = CalendarEventType.Fruits,
                        Title = x.Name + " - Snacks",
                        Description = "Waiting For Snacks Supplier",
                        IsAllDay = true,
                        Start = x.AskingForSnackSupplier.Value,
                        End = x.AskingForSnackSupplier.Value,
                        Email = x.Email,
                        Telephone = x.Telephone,
                        PostCode = x.Postcode,
                        Address = x.Address1,
                        DeliveryInstruction = x.DeliveryInstruction,
                    });

                var dryGoodsWaiting = context.SupAcctRequestLogs.Where(x => x.Supplier.SupplierType == SupplierType.DryGoods)
                                                     .Where(x => companyID == null || x.CompanyID == companyID)
                                                     .Where(x => filterText == null || filterText == "" || x.Company.Name.ToLower().Contains(filterText.ToLower()))
                                                     .Where(x => !x.Company.CompanySuppliers.Select(cs => cs.SupplierID).Contains(x.Supplier.ID))
                                                     .ToList()
                                                     .Select(x => new CalendarEventViewModel()
                                                     {
                                                         CustomerName = x.Company.Name,
                                                         EventType = CalendarEventType.DryGoods,
                                                         Title = x.Company.Name + " - Dry Goods",
                                                         Description = "Waiting For Dry Goods Supplier",
                                                         IsAllDay = true,
                                                         Start = x.RequestTime,
                                                         End = x.RequestTime,
                                                         Email = x.Company.Email,
                                                         Telephone = x.Company.Telephone,
                                                         PostCode = x.Company.Postcode,
                                                         Address = x.Company.Address1,
                                                         DeliveryInstruction = x.Company.DeliveryInstruction?? "",
                                                     })
                                                     .ToList();
                //var dryGoodsWaiting = context.Companies.Where(x => x.AskingForDryGoodsSupplier != null)
                //                                       .Where(x => companyID == null || x.ID == companyID)
                //                                       .Where(x => filterText == null || filterText == "" || x.Name.ToLower().Contains(filterText.ToLower()))
                //                                     .ToList()
                //                                     .Select(x => new CalendarEventViewModel()
                //                                     {
                //                                         CustomerName = x.Name,
                //                                         EventType = CalendarEventType.DryGoods,
                //                                         Title = x.Name + " - Dry Goods",
                //                                         Description = "Waiting For Dry Goods Supplier",
                //                                         IsAllDay = true,
                //                                         Start = x.AskingForDryGoodsSupplier.Value,
                //                                         End = x.AskingForDryGoodsSupplier.Value,
                //                                         Email = x.Email,
                //                                         Telephone = x.Telephone,
                //                                         PostCode = x.Postcode,
                //                                         Address = x.Address1,
                //                                         DeliveryInstruction = x.DeliveryInstruction
                //                                     });

                results.AddRange(milkWaiting);
                results.AddRange(fruitsWaiting);
                results.AddRange(snackWaiting);
                results.AddRange(dryGoodsWaiting);
            }

            return Json(results.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult ReadFirstDeliveryDate([DataSourceRequest] DataSourceRequest request, Guid? companyID, string filterText)
        {
            var results = new List<CalendarEventViewModel>();
            var companySupRepo = new CompanySupplierRepository();
            // wait for Milk supplier
            var milkFirstDeliveryDate = companySupRepo.Read()
                                                .Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                                .Where(x => x.StartDate != null)
                                                .Where(x => companyID == null || x.CompanyID == companyID)
                                                .Where(x => filterText == null || filterText == "" || x.Company.Name.ToLower().Contains(filterText.ToLower()) || x.Supplier.Name.ToLower().Contains(filterText.ToLower()))
                                                .Include(x => x.Supplier)
                                                .ToList()
                                                .Select(x => new CalendarEventViewModel()
                                                {
                                                    CustomerName = x.Company.Name,
                                                    EventType = CalendarEventType.Milk,
                                                    Title = x.Company.Name + " - Milk",
                                                    Description = "Milk First Delivery Date",
                                                    IsAllDay = true,
                                                    Start = x.StartDate.Value,
                                                    End = x.StartDate.Value,
                                                    Email = x.Company.Email,
                                                    Telephone = x.Company.Telephone,
                                                    PostCode = x.Company.Postcode,
                                                    Address = x.Company.Address1,
                                                    DeliveryInstruction = x.Company.DeliveryInstruction,
                                                    SupplierName = x.Supplier != null ? x.Supplier.Name : "",
                                                    SupplierEmail = x.Supplier != null ? x.Supplier.Email : ""
                                                });

            var fruitsFirstDeliveryDate = companySupRepo.Read()
                                                .Where(x => x.Supplier.SupplierType == SupplierType.Fruits)
                                                .Where(x => x.StartDate != null)
                                                .Where(x => companyID == null || x.CompanyID == companyID)
                                                .Where(x => filterText == null || filterText == "" || x.Company.Name.ToLower().Contains(filterText.ToLower()) || x.Supplier.Name.ToLower().Contains(filterText.ToLower()))
                                                .Include(x => x.Supplier)
                                                .ToList()
                                                .Select(x => new CalendarEventViewModel()
                                                {
                                                    CustomerName = x.Company.Name,
                                                    EventType = CalendarEventType.Fruits,
                                                    Title = x.Company.Name + " - Fruits",
                                                    Description = "Fruits First Delivery Date",
                                                    IsAllDay = true,
                                                    Start = x.StartDate.Value,
                                                    End = x.StartDate.Value,
                                                    Email = x.Company.Email,
                                                    Telephone = x.Company.Telephone,
                                                    PostCode = x.Company.Postcode,
                                                    Address = x.Company.Address1,
                                                    DeliveryInstruction = x.Company.DeliveryInstruction,
                                                    SupplierName = x.Supplier != null ? x.Supplier.Name : "",
                                                    SupplierEmail = x.Supplier != null ? x.Supplier.Email : ""
                                                });
            
            var snacksFirstDeliveryDate = companySupRepo.Read()
                                                .Where(x => x.Supplier.SupplierType == SupplierType.Snacks)
                                                .Where(x => x.StartDate != null)
                                                .Where(x => companyID == null || x.CompanyID == companyID)
                                                .Where(x => filterText == null || filterText == "" || x.Company.Name.ToLower().Contains(filterText.ToLower()) || x.Supplier.Name.ToLower().Contains(filterText.ToLower()))
                                                .Include(x => x.Supplier)
                                                .ToList()
                                                .Select(x => new CalendarEventViewModel()
                                                {
                                                    CustomerName = x.Company.Name,
                                                    EventType = CalendarEventType.Fruits,
                                                    Title = x.Company.Name + " - Snacks",
                                                    Description = "Snacks First Delivery Date",
                                                    IsAllDay = true,
                                                    Start = x.StartDate.Value,
                                                    End = x.StartDate.Value,
                                                    Email = x.Company.Email,
                                                    Telephone = x.Company.Telephone,
                                                    PostCode = x.Company.Postcode,
                                                    Address = x.Company.Address1,
                                                    DeliveryInstruction = x.Company.DeliveryInstruction,
                                                    SupplierName = x.Supplier != null ? x.Supplier.Name : "",
                                                    SupplierEmail = x.Supplier != null ? x.Supplier.Email : ""
                                                });

            results.AddRange(milkFirstDeliveryDate);
            results.AddRange(fruitsFirstDeliveryDate);
            results.AddRange(snacksFirstDeliveryDate);
            return Json(results.ToDataSourceResult(request));
        }
    }

}