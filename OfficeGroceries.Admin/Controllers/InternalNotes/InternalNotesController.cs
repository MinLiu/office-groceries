﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    public class InternalNotesController : EntityController<InternalNote, InternalNoteViewModel>
    {
        public InternalNotesController()
            : base(new InternalNoteRepository(), new InternalNoteMapper())
        {

        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _New()
        {
            return PartialView(new InternalNoteViewModel());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _New(InternalNoteViewModel viewModel)
        {
            var model = _mapper.MapToModel(viewModel);
            model.Creator = CurrentUser.ToFullName();
            _repo.Create(model);
            return Json(new { ID = model.ID });
        }

        [HttpGet]
        public ActionResult _Edit(string ID)
        {
            var model = _repo.Find(Guid.Parse(ID));
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _Edit(InternalNoteViewModel viewModel, string complete, string incomplete)
        {
            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);

            if (!string.IsNullOrWhiteSpace(complete))
            {
                model.IsCompleted = true;
                model.CompletedUser = CurrentUser.ToFullName();
                model.CompletedTime = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(incomplete))
            {
                model.IsCompleted = false;
                model.CompletedUser = null;
                model.CompletedTime = null;
            }
            
            _repo.Update(model);

            _mapper.MapToViewModel(model, viewModel);
            
            ViewBag.Saved = true;
            
            return PartialView(viewModel);
        }
    }
}