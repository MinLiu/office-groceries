﻿using System;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using NuIngredient.Models;
using Kendo.Mvc.UI;

namespace NuIngredient.Controllers
{
    public class InternalNoteGridController : GridController<InternalNote, InternalNoteGridViewModel>
    {
        public InternalNoteGridController()
            : base(new InternalNoteRepository(), new InternalNoteGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText, bool includeCompleted)
        {
            var query = _repo.Read()
                .Where(x => includeCompleted || x.IsCompleted == false);

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                filterText = filterText?.ToLower().Trim() ?? "";
                query = query.Where(x => x.Title.ToLower().Contains(filterText)
                                         || x.Content.ToLower().Contains(filterText));
            }

            var models = query
                .OrderByDescending(x => x.Created)
                .Skip(request.PageSize * (request.Page - 1))
                .Take(request.PageSize);
            
            var result = new DataSourceResult()
            {
                Data = models.ToList().Select(_mapper.MapToViewModel),
                Total = query.Count()
            };

            return Json(result);
        }

        public override JsonResult Destroy(DataSourceRequest request, InternalNoteGridViewModel viewModel)
        {
            var model = new InternalNote { ID = Guid.Parse(viewModel.ID) };
            _repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
    }
}