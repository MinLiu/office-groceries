﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    public class MirrorController : Controller
    {
        // GET
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> ReadRestrictedProducts([DataSourceRequest] DataSourceRequest request, Guid? companyID)
        {
            if (companyID == null)
                return Json(new DataSourceResult());
            
            var dryGoodProducts = (await new DryGoodsExcludeCompanyRepository(new SnapDbContext())
                .Read()
                .Where(x => x.CompanyID == companyID)
                .Select(x => new
                {
                    ProductName = x.DryGoodProduct.Name,
                    ProductCode = x.DryGoodProduct.ProductCode,
                    Category = x.DryGoodProduct.DryGoodsCategories.Select(c => c.Category.Name).OrderBy(c => c)
                })
                .ToListAsync())
                .Select(x => new RestrictedProductVm
                {
                    ProductName = x.ProductName,
                    ProductCode = x.ProductCode,
                    ProductCategory = string.Join(", ", x.Category),
                });

            var milkProducts = (await new MilkExcludeCompanyRepository(new SnapDbContext())
                .Read()
                .Where(x => x.CompanyID == companyID)
                .Select(x => new
                {
                    ProductName = x.MilkProduct.Name,
                    ProductCode = x.MilkProduct.ProductCode,
                    Category = x.MilkProduct.Categories.Select(c => c.Category.Name).OrderBy(c => c)
                })
                .ToListAsync())
                .Select(x => new RestrictedProductVm
                {
                    ProductName = x.ProductName,
                    ProductCode = x.ProductCode,
                    ProductCategory = string.Join(", ", x.Category),
                });

            var fruitProducts = (await new FruitExcludeCompanyRepository(new SnapDbContext())
                .Read()
                .Where(x => x.CompanyID == companyID)
                .Select(x => new
                {
                    ProductName = x.FruitProduct.Name,
                    ProductCode = x.FruitProduct.ProductCode,
                    Category = x.FruitProduct.Categories.Select(c => c.Category.Name).OrderBy(c => c)
                })
                .ToListAsync())
                .Select(x => new RestrictedProductVm
                {
                    ProductName = x.ProductName,
                    ProductCode = x.ProductCode,
                    ProductCategory = string.Join(", ", x.Category),
                });

            var resultVms = dryGoodProducts.Concat(milkProducts).Concat(fruitProducts).OrderBy(x => x.ProductCategory);

            return Json(await resultVms.ToDataSourceResultAsync(request));
        }

        [HttpPost]
        public ActionResult MirrorTo(Guid fromCompanyID, Guid toCompanyID)
        {
            var dbContext = new SnapDbContext();

            var excludeRepo = new DryGoodsExcludeCompanyRepository(dbContext);

            var productIdList = excludeRepo.Read()
                .Where(x => x.CompanyID == fromCompanyID)
                .Select(x => x.DryGoodProductID)
                .ToList();

            var toAddList = new List<DryGoodsExcludeCompany>();
            foreach (var productId in productIdList)
            {
                if (excludeRepo.Read().Any(x => x.DryGoodProductID == productId && x.CompanyID == toCompanyID) == false)
                {
                    toAddList.Add(new DryGoodsExcludeCompany
                    {
                        CompanyID = toCompanyID,
                        DryGoodProductID = productId,
                    });
                }
            }
            
            excludeRepo.Create(toAddList);

            return Content($"Successfully mirrored {toAddList.Count} products");
        }
    }
}