﻿using NuIngredient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;
using System.Net.Mail;

namespace NuIngredient.Controllers
{
    public class AislesController : BaseController
    {
        public ActionResult Category(string id)
        {
            id = id.Trim();
            var page = new AisleRepository().Read().Where(x => x.Name == id).FirstOrDefault();
            if (page != null)
            {
                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;
            }

            ViewBag.category = id;
            return View();
        }

    }
}