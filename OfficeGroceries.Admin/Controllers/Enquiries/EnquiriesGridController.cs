﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class EnquiriesGridController : GridController<Enquiry, EnquiryGridViewModel>
    {

        public EnquiriesGridController()
            : base(new EnquiryRepository(), new EnquiryGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText)
        {
            var models = _repo.Read()
                              .Where(x => x.Deleted == false)
                              .Where(x => filterText == null || filterText == "" || x.BusinessName.ToLower().Contains(filterText.ToLower()) || (x.FirstName + " " + x.LastName).Contains(filterText.ToLower()));

            models = ApplyQueryFiltersSort(request, models, "Timestamp desc");

            var viewModels = models.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            return Json(new DataSourceResult { Data = viewModels.ToList(), Total = models.Count() });
        }

        [HttpPost]
        public JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request, string filterText)
        {
            var models = _repo.Read()
                .Where(x => x.Deleted == true)
                .Where(x => filterText == null || filterText == "" || x.BusinessName.ToLower().Contains(filterText.ToLower()) || (x.FirstName + " " + x.LastName).Contains(filterText.ToLower()));

            models = ApplyQueryFiltersSort(request, models, "Timestamp desc");

            var viewModels = models.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));

            return Json(new DataSourceResult { Data = viewModels.ToList(), Total = models.Count() });
        }

        public JsonResult Delete([DataSourceRequest] DataSourceRequest request, EnquiryGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _repo.Find(Guid.Parse(viewModel.ID));
                model.Deleted = true;
                model.DateDeleted = DateTime.Now;
                _repo.Update(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Restore(Guid id)
        {
            var model = _repo.Find(id);
            model.Deleted = false;
            model.DateDeleted = null;
            _repo.Update(model);
            return OkResult();
        }
    }
    
}