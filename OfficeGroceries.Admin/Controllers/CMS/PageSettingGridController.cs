﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class PageSettingGridController : GridController<PageSetting, PageSettingGridViewModel>
    {

        public PageSettingGridController()
            : base(new PageSettingRepository(), new PageSettingGridMapper())
        {

        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _repo.Read().OrderBy(x => x.Label);
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }
    }
}