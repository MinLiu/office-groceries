﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class PageSettingsController : GridController<PageSetting, PageSettingViewModel>
    {

        public PageSettingsController()
            : base(new PageSettingRepository(), new PageSettingMapper())
        {

        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _New()
        {
            return PartialView(new PageSettingViewModel());
        }

        [HttpPost]
        public ActionResult _New(PageSettingViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return PartialView(viewModel);

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            return Json(new { ID = model.ID });
        }

        [HttpGet]
        public ActionResult _Edit(Guid id)
        {
            var model = _repo.Find(id);
            var viewModel = _mapper.MapToViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _Edit(PageSettingViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _repo.Find(Guid.Parse(viewModel.ID));

                _mapper.MapToModel(viewModel, model);

                _repo.Update(model);

                ViewBag.Saved = true;
            }

            return PartialView(viewModel);
        }
    }
}