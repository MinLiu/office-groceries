﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class StaticPagesController : GridController<StaticPage, StaticPageViewModel>
    {

        public StaticPagesController()
            : base(new StaticPageRepository(), new StaticPageMapper())
        {

        }

        [HttpGet]
        public ActionResult Edit(string name)
        {
            var model = _repo.Read().Where(x => x.Name == name).FirstOrDefault();
            if (model == null)
            {
                model = new StaticPage() { Name = name };
                _repo.Create(model);
            }
            var viewModel = _mapper.MapToViewModel(model);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(StaticPageViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _repo.Find(Guid.Parse(viewModel.ID));

                _mapper.MapToModel(viewModel, model);

                _repo.Update(model);

                ViewBag.Saved = true;
            }

            return View(viewModel);
        }
    }
}