﻿using NuIngredient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Text;
using HtmlAgilityPack;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class CMSController : BaseController
    {
        protected static string previewServerPath = ConfigurationManager.AppSettings["PreviewServerPath"];
        protected static string basePath = "/Views/CMS/Content";
        protected static string previewServerBasePath = previewServerPath + basePath;

        public ActionResult Index()
        {
            if (TempData["SuccessMessage"] != null)
            {
                ViewBag.SuccessMessage = TempData["SuccessMessage"];
            }
            else if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }
            return View();
        }

        public ActionResult CreateFolder(string newPath)
        {
            var relativePath = previewServerBasePath + newPath;
            System.IO.Directory.CreateDirectory(relativePath);

            return Json(new
            {
                result = new { success = true }
            });
        }

        public ActionResult List(string path)
        {
            DirectoryInfo di = new DirectoryInfo(previewServerBasePath + path);
            var directories = di.GetDirectories();
            var files = di.GetFiles();

            var result = new List<CMSFileInfo>();
            foreach (var directory in directories)
            {
                result.Add(new CMSFileInfo
                {
                    name = directory.Name,
                    rights = "drwxr-xr-x",
                    size = "",
                    date = directory.CreationTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    type = "dir"
                });
            }
            foreach (var file in files)
            {
                result.Add(new CMSFileInfo
                {
                    name = file.Name,
                    rights = "drwxr-xr-x",
                    size = file.Length.ToString(),
                    date = file.LastWriteTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    type = "file"
                });
            }
            

            return Json(new { result });
        }

        public ActionResult Download(string path)
        {
            byte[] fileBytes = null;
            try
            {
                fileBytes = System.IO.File.ReadAllBytes(previewServerBasePath + path);
            }
            catch { }
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(path));
        }

        public ActionResult Upload(string destination)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    file.SaveAs(previewServerBasePath + destination + "/" +file.FileName);
                }
            }
            return Json(new
            {
                result = new
                {
                    success = true
                }
            });
        }

        public ActionResult Remove(List<string> items)
        {
            foreach (var item in items)
            {
                var physicalPath = previewServerBasePath + item;

                var fileAttr = System.IO.File.GetAttributes(physicalPath);

                if (fileAttr == FileAttributes.Directory)
                {
                    System.IO.Directory.Delete(physicalPath, true);
                }
                else
                {
                    System.IO.File.Delete(physicalPath);
                }
            }

            return Json(new
            {
                result = new
                {
                    success = true
                }
            });
        }

        public ActionResult Rename(string item, string newItemPath)
        {
            var physicalPath = previewServerBasePath + item;
            var newPhysicalPath = previewServerBasePath + newItemPath;

            var fileAttr = System.IO.File.GetAttributes(physicalPath);

            if (fileAttr == FileAttributes.Directory)
            {
                System.IO.Directory.Move(physicalPath, newPhysicalPath);
            }
            else
            {
                System.IO.File.Move(physicalPath, newPhysicalPath);
            }

            return Json(new
            {
                result = new
                {
                    success = true
                }
            });
        }

        public ActionResult Move(List<string> items, string newPath)
        {
            foreach (var item in items)
            {
                try
                {
                    var physicalPath = previewServerBasePath + item;
                    var fileAttr = System.IO.File.GetAttributes(physicalPath);

                    var newPhysicalPath = previewServerBasePath + newPath + "/" + Path.GetFileName(item);

                    if (fileAttr == FileAttributes.Directory)
                    {
                        System.IO.Directory.Move(physicalPath, newPhysicalPath);
                    }
                    else
                    {
                        System.IO.File.Move(physicalPath, newPhysicalPath);
                    }

                }
                catch
                { }
            }

            return Json(new
            {
                result = new
                {
                    success = true
                }
            });
        }

        public ActionResult GetContent(string item)
        {
            var relativePath = previewServerBasePath + item;
            var physicalPath = relativePath;

            var content = System.IO.File.ReadAllText(physicalPath);

            return Json(new { result = content });
        }

        public ActionResult Edit(string item, string content)
        {
            var relativePath = previewServerBasePath + item;
            var physicalPath = relativePath;

            System.IO.File.WriteAllText(physicalPath, content);

            return Json(new
            {
                result = new
                {
                    success = true
                }
            });
        }

        [HttpPost]
        public ActionResult Reset()
        {
            if (Directory.Exists(previewServerBasePath))
                System.IO.Directory.Delete(previewServerBasePath, true);

            DirectoryCopy(Server.MapPath(basePath), previewServerBasePath);

            TempData["SuccessMessage"] = "Reset Successfully";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Publish()
        {
            if (Directory.Exists(Server.MapPath(basePath)))
                System.IO.Directory.Delete(Server.MapPath(basePath), true);

            DirectoryCopy(previewServerBasePath, Server.MapPath(basePath));

            TempData["SuccessMessage"] = "Publish Successfully";
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult GetHtmlPage(string label)
        {
            try
            {
                var pageSettingRepo = new PageSettingRepository();
                var page = pageSettingRepo.Read().Where(x => x.Label == label).Where(x => x.Active == true).FirstOrDefault();

                if (page == null)
                    throw new Exception();

                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;

                return GetPageFileContentResult(page.FilePath);
                    
            }
            catch (Exception e)
            {
                return new EmptyResult();
            }
        }

        [AllowAnonymous]
        public ActionResult GetAisleLandingPage(string name)
        {
            try
            {
                var aisle = new AisleRepository().Read().Where(x => x.Name == name).FirstOrDefault();
                ViewBag.MetaTitle = aisle.MetaTitle;
                ViewBag.MetaKeyword = aisle.MetaKeyword;
                ViewBag.MetaDescription = aisle.MetaDescription;
                if (string.IsNullOrEmpty(aisle.LandingPageFilePath))
                {
                    var defaultLandingPage = GetPageFileData("/Aisles/aisles.html");
                    //defaultLandingPage = defaultLandingPage.Replace("{AISLENAME}", aisle.Label);
                    //defaultLandingPage = defaultLandingPage.Replace("{LANDINGPAGECONTENT}", aisle.LandingPageContent ?? "");

                    HtmlDocument document = new HtmlDocument();
                    document.LoadHtml(defaultLandingPage);

                    var html = document.DocumentNode.SelectNodes("//html").FirstOrDefault();
                    if (html != null)
                    {
                        html.InnerHtml = html.InnerHtml.Replace("{AISLENAME}", aisle.Label);
                        html.InnerHtml = html.InnerHtml.Replace("{LANDINGPAGECONTENT}", aisle.LandingPageContent ?? "");
                    }

                    if (aisle.MoreInfo.Any())
                    {
                        var moreInfoArea = document.GetElementbyId("more-info-area");
                        var label = document.GetElementbyId("more-info-label");
                        
                        foreach (var moreInfo in aisle.MoreInfo.OrderBy(x => x.SortPosition))
                        {
                            var newLabel = label.Clone();
                            newLabel.InnerHtml = newLabel.InnerHtml.Replace("{MOREINFOTITLE}", moreInfo.Label);
                            newLabel.InnerHtml = newLabel.InnerHtml.Replace("{MOREINFOCONTENT}", HttpUtility.HtmlEncode(moreInfo.Content));
                            moreInfoArea.AppendChild(newLabel);
                        }

                        // remove the original one
                        label.Remove();
                    }
                    else
                    {
                        document.GetElementbyId("more-info-area").Remove();
                    }

                    return Content(document.DocumentNode.OuterHtml);
                }
                return GetPageFileContentResult(aisle.LandingPageFilePath);
            }
            catch
            {
                return new EmptyResult();
            }
        }

        [AllowAnonymous]
        public ActionResult GetAisleProspectContent(string name)
        {
            try
            {
                var aisle = new AisleRepository().Read().Where(x => x.Name == name).FirstOrDefault();
                ViewBag.MetaTitle = aisle.MetaTitle;
                ViewBag.MetaKeyword = aisle.MetaKeyword;
                ViewBag.MetaDescription = aisle.MetaDescription;
                return GetPageFileContentResult(aisle.ProspectContentFilePath);
            }
            catch
            {
                return new EmptyResult();
            }
        }

        [AllowAnonymous]
        public ActionResult GetAisleCustomerContent(string name)
        {
            try
            {
                var aisle = new AisleRepository().Read().Where(x => x.Name == name).FirstOrDefault();
                ViewBag.MetaTitle = aisle.MetaTitle;
                ViewBag.MetaKeyword = aisle.MetaKeyword;
                ViewBag.MetaDescription = aisle.MetaDescription;
                return GetPageFileContentResult(aisle.CustomerContentFilePath);
            }
            catch
            {
                return new EmptyResult();
            }
        }

        private ActionResult GetPageFileContentResult(string path)
        {
            try
            {
                var fileData = GetPageFileData(path);
                return Content(fileData);

            }
            catch
            {
                return new EmptyResult();
            }
        }

        private string GetPageFileData(string path)
        {
            try
            {
                var relativePath = basePath + path;
                var physicalPath = Server.MapPath(relativePath);

                if (!System.IO.File.Exists(physicalPath))
                    throw new Exception();

                var fileData = System.IO.File.ReadAllText(physicalPath, Encoding.UTF8);
                return fileData;
            }
            catch
            {
                return "";
            }
        }
        #region Helper

        private void DirectoryCopy(string sourceDirName = null, string destDirName = null)
        {
            var dir = new DirectoryInfo(sourceDirName);

            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            var files = dir.GetFiles();
            foreach (var file in files)
            {
                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, true);
            }

            var subdirs = dir.GetDirectories();
            foreach (var subdir in subdirs)
            {
                string tempPath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, tempPath);
            }
        }

        #endregion
    }



    public class CMSFileInfo
    {
        public string name { get; set; }
        public string rights { get; set; }
        public string size { get; set; }
        public string date { get; set; }
        public string type { get; set; }
    }
}

