﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NuIngredient.Models;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Net.Mail;
using System.Collections.Generic;
using System.Web.Routing;
using System.Web.Configuration;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private UserSignInManager _signInManager;
        private UserManager _userManager;

        public AccountController()
        {
            
        }

        public void InitializeController(RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        public AccountController(UserManager userManager, UserSignInManager signInManager )
        {
            try
            {
                UserManager = userManager;
                SignInManager = signInManager;
            }
            catch { }
        }

        public UserSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<UserSignInManager>(); }
            private set  {  _signInManager = value; }
        }

        public UserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>(); }
            private set { _userManager = value; }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            Session["Blah"] = "Prevent Anti-XSRF token failed.";
            ViewBag.ReturnUrl = returnUrl;

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard");  
            }

            if (!String.IsNullOrEmpty(returnUrl) && User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Unauthorized", "Home");  
            }

            
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            Session["Blah"] = "Prevent Anti-XSRF token failed.";

            if (!ModelState.IsValid)
            {
                ViewBag.ErrorMessage = "Invalid login attempt";
                return View(model);
            }
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var emailConfirmed = false;

            try { emailConfirmed = UserManager.IsEmailConfirmed(UserManager.FindByEmail(model.Email).Id); } //takes in a UserID to check if email is confirmed 
            catch {
                ViewBag.ErrorMessage = "Invalid login attempt";
                return View(model);
            }

            if (emailConfirmed.Equals(true)) {

                //AddSampleAccountItems(UserManager.FindByEmail(model.Email).Company);

                var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        if (!IsNiUser(model.Email))
                        {
                            AuthenticationManager.AuthenticationResponseGrant = null;
                            ViewBag.ErrorMessage = "Invalid login attempt";
                            return View(model);
                        }
                        SaveUserTokenCookie(model.Email);
                        //MoveProspectOrderToUser(model.Email);
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                    case SignInStatus.Failure:
                    default:
                        ViewBag.ErrorMessage = "Invalid login attempt";
                        return View(model);
                }
            }
            else
            {
                ViewBag.ConfirmEmail = model.Email;
                return View(model);
            }
        }

        // GET: /Account/Register
        public ActionResult RegisterForProspect(string prospectToken)
        {
            if (string.IsNullOrWhiteSpace(prospectToken))
            {
                return View(new RegisterViewModel());
            }

            var prospect = new ProspectRepository()
                .Read()
                .Where(p => p.ProspectToken == prospectToken)
                .OrderByDescending(p => p.Created)
                .FirstOrDefault();

            if (prospect == null)
            {
                return View(new RegisterViewModel());
            }

            var user = UserManager.FindByEmail(prospect.Email);

            if (user == null)
            {
                var viewModel = new RegisterViewModel()
                {
                    ProspectToken = prospect.ProspectToken,
                    Email = prospect.Email,
                    PhoneNumber = prospect.Phone,
                    BusinessName = prospect.CompanyName,
                    FirstName = prospect.FirstName,
                    LastName = prospect.LastName,
                    UseDeliveryAddrAsBillingAddr = true,
                    PostCode = prospect.Postcode
                };
                return View(viewModel);
            }
            else
            {
                var viewModel = new RegisterMultiCompanyViewModel()
                {
                    ProspectToken = prospect.ProspectToken,
                    Email = prospect.Email,
                    PhoneNumber = prospect.Phone,
                    BusinessName = prospect.CompanyName,
                    FirstName = prospect.FirstName,
                    LastName = prospect.LastName,
                    UseDeliveryAddrAsBillingAddr = true,
                    PostCode = prospect.Postcode,
                };

                return View("RegisterMultiCompany", viewModel);
            }
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterForProspect(RegisterViewModel model, bool CaptchaValid)
        {
            if (ModelState.IsValid)
            {

                if (!CaptchaValid)
                {
                    ViewBag.ErrorMessage = "reCaptcha failed. Please complete reCaptcha.";
                    return View(model);
                }

                if (!string.IsNullOrEmpty(model.Dummy))
                {
                    ViewBag.ErrorMessage = "Invalid form submission.";
                    return View(model);
                }

                if (!CheckUnwantedEmailDomains(model.Email))
                {
                    ViewBag.ErrorMessage = "Invalid Email address.";
                    return View(model);
                }

                if (UserManager.FindByEmail(model.Email) != null)
                {
                    ViewBag.ErrorMessage = "The email address entered is already being used.";
                    return View(model);
                }

                var user = new User
                {
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    EmailConfirmed = true,
                    AgreeToGetEmails = model.AgreeToGetEmail,
                    Company = new Company
                    {
                        ID = Guid.NewGuid(),
                        Name = model.BusinessName,
                        CompanyRegistration = model.FirstName + " " + model.LastName,
                        Address1 = model.DeliveryAddress,
                        Postcode = model.PostCode,
                        BillingAddress = model.UseDeliveryAddrAsBillingAddr ? model.DeliveryAddress : model.BillingAddress,
                        BillingPostcode = model.UseDeliveryAddrAsBillingAddr ? model.PostCode : model.BillingPostCode,
                        BillingCompanyName = model.UseDeliveryAddrAsBillingAddr ? model.BusinessName : model.BillingCompanyName,
                        BillingEmail = model.UseDeliveryAddrAsBillingAddr ? model.Email : model.BillingEmail,
                        Email = model.Email,
                        Telephone = model.PhoneNumber,
                        DeliveryInstruction = model.DeliveryInstruction,
                        AgreeToGetEmail = model.AgreeToGetEmail,
                        SignupDate = DateTime.Now,
                        DefaultCurrencyID = 1,
                        MilkDeliveryMonday = true,
                        MilkDeliveryTuesday = true,
                        MilkDeliveryWednesday = true,
                        MilkDeliveryThursday = true,
                        MilkDeliveryFriday = true,
                        MilkDeliverySaturday = true,
                        MilkDeliverySunday = false,
                        FruitsDeliveryMonday = true,
                        FruitsDeliveryTuesday = true,
                        FruitsDeliveryWednesday = true,
                        FruitsDeliveryThursday = true,
                        FruitsDeliveryFriday = true,
                        FruitsDeliverySaturday = true,
                        FruitsDeliverySunday = false,
                        DryGoodsDeliveryMonday = true,
                        DryGoodsDeliveryTuesday = true,
                        DryGoodsDeliveryWednesday = true,
                        DryGoodsDeliveryThursday = true,
                        DryGoodsDeliveryFriday = true,
                        DryGoodsDeliverySaturday = true,
                        AccountNo = GetOGAccountNo(),
                        IsMultiTenantedBuilding = model.IsMultiTenantedBuilding ?? false,
                        HasMultiSites = model.HasMultiSites ?? false,
                    },
                };

                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Admin");

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with a confirmation link

                    //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                    //string message = String.Format("Hello,\n\nPlease click the following link to confirm your Office Groceries account:\n\n<a href=\"{0}\">{0}</a>", callbackUrl);
                    //message += String.Format("\n\nIf you received this message by mistake just delete it without clicking the link.\n\nNeed help? Contact Us on through our website");
                    //message += String.Format("\n\n\n<hr />This is an automated message, please do not reply to this email address. You can contact us via our website. <a href=\"www.snap-suite.com\">www.snap-suite.com</a>");

                    //EmailService.SendEmail(user.Email, "Confirm your Office Groceries Account", message, Server);


                    try
                    {
                        string newUserMessage = String.Format("Someone has signup for a Office Groceries account. \n\nBusiness Name:{0}\nName: {1} {2}\nEmail: {3}\nPhone:{4}",
                            model.BusinessName,
                            model.FirstName,
                            model.LastName,
                            model.Email,
                            model.PhoneNumber
                        );

                        EmailService.SendEmail("min@fruitfulgroup.com", "New Office Groceries User Signup", newUserMessage, Server);

                    }
                    catch { }

                    // Move Order From Prospect to This User if there's a prospectToken and Delete Prospect
                    if (!string.IsNullOrWhiteSpace(model.ProspectToken))
                    {
                        MoveProspectOrderToUser(model.ProspectToken, user.Company.ID);
                        var prospectRepo = new ProspectRepository();
                        var prospect = prospectRepo.Read().Where(x => x.ProspectToken == model.ProspectToken).FirstOrDefault();
                        if (prospect != null)
                            prospectRepo.Delete(prospect);
                    }

                    // Send mails to supplier center asking for suppliers
                    if (!string.IsNullOrWhiteSpace(model.ProspectToken))
                    {
                        AskForSuppliers(user.Company);
                    }

                    SendAccountSetUpEmailWithPassword(user.Id, model.Password);

                    // Mailchimp Opt in
                    if (model.AgreeToGetEmail)
                    {
                        var success = await MailChimpService.OptIn(model.Email, model.FirstName, model.LastName);
                    }

                    return RedirectToAction("RegistrationForProspectRedirect", "Account");
                }
                else
                {

                    //AddErrors(result);
                    ViewBag.ErrorMessage = result.Errors.First().ToString();
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterMultiCompany(RegisterMultiCompanyViewModel model, bool CaptchaValid)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CaptchaValid)
            {
                ViewBag.ErrorMessage = "reCaptcha failed. Please complete reCaptcha.";
                return View(model);
            }

            if (!string.IsNullOrEmpty(model.Dummy))
            {
                ViewBag.ErrorMessage = "Invalid form submission.";
                return View(model);
            }

            if (!CheckUnwantedEmailDomains(model.Email))
            {
                ViewBag.ErrorMessage = "Invalid Email address.";
                return View(model);
            }

            var user = UserManager.FindByEmail(model.Email);
            var originalCompany = user.Company;
            if (originalCompany.CompanyCollectionID == null)
            {
                var companyCollection = new CompanyCollection()
                {
                    Name = originalCompany.Name,
                };
                new CompanyCollectionRepository().Create(companyCollection);
                originalCompany.CompanyCollectionID = companyCollection.ID;
                UserManager.Update(user);
            }

            user.Company = new Company
            {
                ID = Guid.NewGuid(),
                Name = model.BusinessName,
                CompanyRegistration = model.FirstName + " " + model.LastName,
                Address1 = model.DeliveryAddress,
                Postcode = model.PostCode,
                BillingAddress = model.UseDeliveryAddrAsBillingAddr ? model.DeliveryAddress : model.BillingAddress,
                BillingPostcode = model.UseDeliveryAddrAsBillingAddr ? model.PostCode : model.BillingPostCode,
                BillingCompanyName = model.UseDeliveryAddrAsBillingAddr ? model.BusinessName : model.BillingCompanyName,
                BillingEmail = model.UseDeliveryAddrAsBillingAddr ? model.Email : model.BillingEmail,
                Email = model.Email,
                Telephone = model.PhoneNumber,
                DeliveryInstruction = model.DeliveryInstruction,
                AgreeToGetEmail = model.AgreeToGetEmail,
                SignupDate = DateTime.Now,
                DefaultCurrencyID = 1,
                MilkDeliveryMonday = true,
                MilkDeliveryTuesday = true,
                MilkDeliveryWednesday = true,
                MilkDeliveryThursday = true,
                MilkDeliveryFriday = true,
                MilkDeliverySaturday = true,
                MilkDeliverySunday = false,
                FruitsDeliveryMonday = true,
                FruitsDeliveryTuesday = true,
                FruitsDeliveryWednesday = true,
                FruitsDeliveryThursday = true,
                FruitsDeliveryFriday = true,
                FruitsDeliverySaturday = true,
                FruitsDeliverySunday = false,
                DryGoodsDeliveryMonday = true,
                DryGoodsDeliveryTuesday = true,
                DryGoodsDeliveryWednesday = true,
                DryGoodsDeliveryThursday = true,
                DryGoodsDeliveryFriday = true,
                DryGoodsDeliverySaturday = true,
                AccountNo = GetOGAccountNo(),
                IsMultiTenantedBuilding = model.IsMultiTenantedBuilding ?? false,
                HasMultiSites = model.HasMultiSites ?? false,
                CompanyCollectionID = user.Company.CompanyCollectionID,
            };
            user.CompanyID = user.Company.ID;

            UserManager.Update(user);

            try
            {
                string newUserMessage = String.Format("Someone has signup for a Office Groceries account. \n\nBusiness Name:{0}\nName: {1} {2}\nEmail: {3}\nPhone:{4}",
                    model.BusinessName,
                    model.FirstName,
                    model.LastName,
                    model.Email,
                    model.PhoneNumber
                );

                EmailService.SendEmail("min@fruitfulgroup.com", "New Office Groceries User Signup", newUserMessage, Server);

            }
            catch { }

            // Move Order From Prospect to This User if there's a prospectToken and Delete Prospect
            if (!string.IsNullOrWhiteSpace(model.ProspectToken))
            {
                MoveProspectOrderToUser(model.ProspectToken, user.Company.ID);
                var prospectRepo = new ProspectRepository();
                var prospect = prospectRepo.Read().Where(x => x.ProspectToken == model.ProspectToken).FirstOrDefault();
                if (prospect != null)
                    prospectRepo.Delete(prospect);
            }

            AddTagIfNecessary(user.Company);

            // Send mails to supplier center asking for suppliers
            if (!string.IsNullOrWhiteSpace(model.ProspectToken))
            {
                AskForSuppliers(user.Company);
            }

            SendAccountSetUpEmail(user.Id);

            return RedirectToAction("RegistrationRedirect", "Account");
        }

        private void AddTagIfNecessary(Company company)
        {
            try
            {
                using (var db = new SnapDbContext())
                {
                    switch (company.Email.Trim().ToLower())
                    {
                        case "od@office-groceries.com":
                        {
                            var companyTagRepo = new CompanyTagItemRepository(db);
                            var odTagId = Guid.Parse("70AF075A-1A8B-4C56-B23F-0A78E4CE98F7");
                            companyTagRepo.Create(new CompanyTagItem
                            {
                                CompanyID = company.ID,
                                TagID = odTagId,
                            });
                            RefreshMilkPriceByCustomerHelper.New(db).RefreshOrderPrice(company.ID);
                            break;
                        }
                        case "ef@office-groceries.com":
                        {
                            var companyTagRepo = new CompanyTagItemRepository(db);
                            var efTagId = Guid.Parse("9A00B482-EF4F-46AE-B521-9726ED561A82");
                            companyTagRepo.Create(new CompanyTagItem
                            {
                                CompanyID = company.ID,
                                TagID = efTagId,
                            });
                            RefreshMilkPriceByCustomerHelper.New(db).RefreshOrderPrice(company.ID);
                            break;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                var subject = $"Failed to create tag for {company.Name}({company.Email})";
                var content = e.Message + "<br />" + e.StackTrace;
                EmailService.SendEmail("min@fruitfulgroup.com", subject, content, Server);
            }
        }

        [AllowAnonymous]
        public ActionResult SendConfirmEmail(string email)
        {
            var user = UserManager.FindByEmail(email);

            if (user == null)
            {
                ViewBag.ErrorMessage = "Could not find email address.";
                return View("Login");
            }
            else
            {
                string code = UserManager.GenerateEmailConfirmationToken(user.Id);
#if DEBUG
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
# else
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new RouteValueDictionary() { { "userId", user.Id }, { "code", code } }, protocol: "https", hostName: ConfigurationManager.AppSettings["CustomerSiteHost"]);
#endif

                string message = String.Format("Hello,\n\nPlease click the following link to confirm your Office Groceries account:\n\n<a href=\"{0}\">{0}</a>", callbackUrl);
                message += String.Format("\n\nIf you received this message by mistake just delete it without clicking the link.\n\nNeed help? Contact Us on through our website");
                message += String.Format("\n\n\n<hr />This is an automated message, please do not reply to this email address. You can contact us via our website. <a href=\"www.snap-suite.com\">www.snap-suite.com</a>");

                EmailService.SendEmail(user.Email, "Confirm your Office Groceries Account", message, Server);

                ViewBag.SuccessMessage = "Confirmation email has been resent.";
                return View("Login");
            }

        }

        [AllowAnonymous]
        public void SendAccountSetUpEmail(string userID)
        {
            var user = UserManager.FindById(userID);

            if (user != null)
            {
                using (var db = new SnapDbContext())
                {
                    var milk = db.MilkOrderHeaders.Where(x => x.CompanyID == user.CompanyID).Any();
                    var fruit = db.FruitOrderHeaders.Where(x => x.CompanyID == user.CompanyID).Any();
                    var dryGoods = db.ShoppingCartItems.Where(x => x.CompanyID == user.CompanyID).Any();
                    var categoryList = new List<string>();
                    if (milk) categoryList.Add("Milk");
                    if (fruit) categoryList.Add("Fruit/Snacks");
                    if (dryGoods) categoryList.Add("Dry Goods");

                    var categories = String.Join(", ", categoryList);

                    var mailMessage = AccountSetupEmailHelper.GetEmail(user.Company, categories);
                    EmailService.SendEmail(mailMessage, Server);
                }

            }
        }

        [AllowAnonymous]
        public void SendAccountSetUpEmailWithPassword(string userID, string password)
        {
            var user = UserManager.FindById(userID);

            if (user != null)
            {
                using (var db = new SnapDbContext())
                {
                    var milk = db.MilkOrderHeaders.Where(x => x.CompanyID == user.CompanyID).Any();
                    var fruit = db.FruitOrderHeaders.Where(x => x.CompanyID == user.CompanyID).Any();
                    var dryGoods = db.ShoppingCartItems.Where(x => x.CompanyID == user.CompanyID).Any();
                    var categoryList = new List<string>();
                    if (milk) categoryList.Add("Milk");
                    if (fruit) categoryList.Add("Fruit/Snacks");
                    if (dryGoods) categoryList.Add("Dry Goods");

                    var categories = String.Join(", ", categoryList);

                    var mailMessage = AccountSetupWithPasswordEmailHelper.GetEmail(user, password, categories);
                    EmailService.SendEmail(mailMessage, Server);
                }

            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult RegistrationRedirect()
        {
            return View();
        }

        public ActionResult RegistrationForProspectRedirect()
        {
            return View();
        }


        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                SendAccountSetUpEmail(userId);
            }
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                await SendResetPasswordEmail(user);

                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public async Task<bool> SendResetPasswordEmail(User user)
        {
            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

#if DEBUG
            var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
#else
            var callbackUrl = Url.Action("ResetPassword", "Account", new RouteValueDictionary() { { "userId", user.Id }, { "code", code} }, protocol: "https", hostName: ConfigurationManager.AppSettings["CustomerSiteHost"]);
#endif
            string message = String.Format("Hello,\n\nPlease click the following link to reset your Office Groceries account:\n\n<a href=\"{0}\">{0}</a>", callbackUrl);
            message += String.Format("\n\nIf you received this message by mistake just delete it without clicking the link.\n\nNeed help? Contact Us on through our website");
            message += String.Format("\n\n\n<hr />This is an automated message, please do not reply to this email address. You can contact us via our website. Orders@office-groceries.com");

            EmailService.SendEmail(user.Email, "Reset your Office Groceries Password", message, Server);

            return true;
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code, string userId)
        {
            var user = UserManager.FindById(userId);
            if (user == null || code == null)
            {
                return View("Error");
            }

            return View(new ResetPasswordViewModel() { Code = code, Email = user.Email });
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            //AddErrors(result);
            ViewBag.ErrorMessage = result.Errors.First().ToString();
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        

#region Manage User

        public ActionResult Manage()
        {
            
            using (var context = new SnapDbContext())
            {
                //Get the currently logged in user.
                User CurrentUser = UserManager.FindById(User.Identity.GetUserId().ToString());
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);


                //Create model then return it
                UserViewModel model = new UserViewModel
                {
                    ID = CurrentUser.Id,
                    Email = CurrentUser.Email,
                    FirstName = CurrentUser.FirstName,
                    LastName = CurrentUser.LastName,
                    PhoneNumber = CurrentUser.PhoneNumber,
                    Role = (CurrentUser.Roles.Count > 0) ? roleManager.FindById(CurrentUser.Roles.FirstOrDefault().RoleId).Name : null, //Get the first role name if existing
                };

                return View("Manage", model);
            }

            
         
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(UserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            try
            {
                User user = UserManager.FindById(model.ID);

                //Check to see if the email address is already taken if the same email address is not input.
                if (user.Email != model.Email)
                {
                    if (UserManager.FindByEmail(model.Email) != null) //If a user already exsists with the user name then stop saving.
                    {
                        ViewBag.ErrorMessage = "Could not save changes. The Email address entered is already taken.";
                        return View("Manage", model);
                    }
                }

                //Update the user
                user.UserName = model.Email;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;

                //Save Changes
                UserManager.Update(user);                
                ViewBag.SuccessMessage = "User Account Details have successfully been Saved.";
            }
            catch
            {
                ViewBag.ErrorMessage = "Could not save changes. Unknown Exception.";
            }

            return View(model);
        }


        // GET: Change Password
        public ActionResult ChangePassword()
        {
            try
            {
                //Get the currently logged in user.
                User CurrentUser = UserManager.FindById(User.Identity.GetUserId().ToString());
                
                //Create model then return it
                ChangeUserPasswordViewModel model = new ChangeUserPasswordViewModel
                {
                    ID = CurrentUser.Id,
                    Email = CurrentUser.Email
                };

                return View(model);
            }
            catch { return RedirectToAction("Index"); }
            
        }



        // POST: Change Password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangeUserPasswordViewModel model)
        {
            if (model == null) return RedirectToAction("Index");

            if (!ModelState.IsValid)
            {
                return View("ChangePassword", model);
            }

            try
            {
                //Try Get User account
                var user = UserManager.FindById(model.ID);

                if (user == null)
                {
                    ViewBag.ErrorMessage = "User Account not found.";
                    return View("ChangePassword", model);
                }

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                IdentityResult result = UserManager.ResetPassword(user.Id, code, model.Password);

                model.Password = "";
                model.ConfirmPassword = "";

                if (result.Succeeded)
                {
                    ViewBag.SuccessMessage = "Password was successfully reset.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Failed to reset password. " + result.Errors.First().ToString();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Failed to reset password. " + ex.ToString();
            }

            return View(model);
        }


        //
        // GET: /Account/EnquiryForm
        [AllowAnonymous]
        public ActionResult EnquiryForm()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult EnquiryForm(EnquiryFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            using (var context = new SnapDbContext())
            {
                context.Enquiries.Add(new Enquiry()
                {
                    ID = Guid.NewGuid(),
                    BusinessName = viewModel.BusinessName,
                    FirstName = viewModel.FirstName,
                    LastName = viewModel.LastName,
                    Email = viewModel.Email,
                    PhoneNumber = viewModel.PhoneNumber,
                    InterestOfMilk = viewModel.InterestOfMilk,
                    InterestOfFruits = viewModel.InterestOfFruits,
                    InterestOfDryGoods = viewModel.InterestOfDryGoods,
                    Note = viewModel.Note,
                    NoGo = false,
                    Timestamp = DateTime.Now
                });
                context.SaveChanges();
            }
            var categories = new List<string>();
            if (viewModel.InterestOfMilk) categories.Add("Milk");
            if (viewModel.InterestOfFruits) categories.Add("Fruit/Snacks");
            if (viewModel.InterestOfDryGoods) categories.Add("Dry Goods");

            var categoryList = String.Join(", ", categories);

            var mailMessage = EnquiryFormEmailHelper.GetEmail(viewModel.Email, viewModel.FirstName, categoryList);
            EmailService.SendEmail(mailMessage, Server);

            return View("EnquirySuccess", viewModel);
        }

#endregion



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        protected void SaveUserTokenCookie(string email){
            using (var context = new SnapDbContext())
            {
                //Update token in database
                var user = context.Users.Where(u => u.Email == email).First();
                user.Token = Guid.NewGuid().ToString("N");

                context.Users.Attach(user);
                context.Entry(user).Property(u => u.Token).IsModified = true;
                context.SaveChanges();

                //Save token in cookies
                HttpCookie cookie = Request.Cookies["GG"] ?? new HttpCookie("GG");
                cookie.Value = user.Token;
                cookie.Expires = DateTime.Now.AddMonths(3);
                Response.Cookies.Add(cookie);
            }
        }

        protected bool IsNiUser(string email)
        {
            var user = new ClaimsPrincipal(AuthenticationManager.AuthenticationResponseGrant.Identity);
            return user.IsInRole("NI-User");
        }

        protected void MoveProspectOrderToUser(string email)
        {
            var currentUser = new UserRepository().Read().Where(u => u.Email == email).First();

            HttpCookie cookie = Request.Cookies["ProspectToken"];
            if (cookie == null)
                return;

            var prospectToken = cookie.Value;

            MoveProspectOrderToUser(prospectToken, currentUser.CompanyID);

        }

        public void MoveProspectOrderToUser(string prospectToken, Guid companyID)
        {
            MoveMilkOrder(prospectToken, companyID);

            MoveFruitOrder(prospectToken, companyID);

            MoveOneOffOrder(prospectToken, companyID);
        }

        private void MoveMilkOrder(string prospectToken, Guid companyID)
        {
            var orderHeaderRepo = new MilkOrderHeaderRepository();
            var orderHeaders = orderHeaderRepo.Read().Where(x => x.ProspectToken == prospectToken).ToList();
            orderHeaders.ForEach(f =>
            {
                f.ProspectToken = null;
                f.CompanyID = companyID;
            });
            orderHeaderRepo.Update(orderHeaders);
        }

        private void MoveFruitOrder(string prospectToken, Guid companyID)
        {
            var orderHeaderRepo = new FruitOrderHeaderRepository();
            var orderHeaders = orderHeaderRepo.Read().Where(x => x.ProspectToken == prospectToken).ToList();
            orderHeaders.ForEach(f =>
            {
                f.ProspectToken = null;
                f.CompanyID = companyID;
            });
            orderHeaderRepo.Update(orderHeaders);
        }

        private void MoveOneOffOrder(string prospectToken, Guid companyID)
        {
            var shoppingCartItemRepo = new ShoppingCartItemRepository();
            var shoppingCartItems = shoppingCartItemRepo.Read()
                                                        .Where(x => x.ProspectToken == prospectToken)
                                                        .ToList();
            foreach (var item in shoppingCartItems)
            {
                var existItem = shoppingCartItemRepo.Read()
                                                    .Where(x => x.CompanyID == companyID)
                                                    .Where(x => x.ProductID == item.ProductID)
                                                    .FirstOrDefault();
                if (existItem == null)
                {
                    item.ProspectToken = null;
                    item.CompanyID = companyID;
                    shoppingCartItemRepo.Update(item);
                }
                else
                {
                    existItem.Qty += item.Qty;
                    shoppingCartItemRepo.Update(existItem);
                    shoppingCartItemRepo.Delete(item);
                }
            }
        }

        private void AskForSuppliers(Company company)
        {
            AskForFruitSupplier(company);
            AskForMilkSupplier(company);
            AskForSnackSupplier(company);
            AskForDryGoodsSupplier(company);
        }

        private void AskForFruitSupplier(Company company)
        {
            var helper = new RequestFruitAcctHelper();
            helper.RequestFruitSupplier(company.ID, false);
        }

        private void AskForMilkSupplier(Company company)
        {
            var helper = new RequestMilkAcctHelper();
            helper.RequestMilkSupplier(company.ID, false);
        }
        
        private void AskForSnackSupplier(Company company)
        {
            var helper = new RequestSnackAcctHelper();
            helper.RequestSnackSupplier(company.ID, false);
        }

        private void AskForDryGoodsSupplier(Company company)
        {
            var shoppingCartRepo = new ShoppingCartItemRepository();

            var shoppingCartItems = shoppingCartRepo
                .Read()
                .Where(x => x.CompanyID == company.ID);

            var supplierIDs = shoppingCartItems
                .Where(x => x.Product.SupplierID != null)
                .Select(x => x.Product.SupplierID.Value)
                .Distinct()
                .ToList();

            if (supplierIDs.Any())
            {
                var helper = new RequestDryGoodsAcctHelper();
                helper.RequestDryGoodsSupplier(company.ID, supplierIDs, false);
            }
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Dashboard");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        private bool CheckUnwantedEmailDomains(string email)
        {
            string [] blockList = new string [] { "-delete", "@mail.ru"};

            foreach (string s in blockList)
            {
                if(email.ToLower().Contains(s)) return false;
            }

            return true;
        }

        public string GetOGAccountNo()
        {
            using (var context = new SnapDbContext())
            {
                var setting = context.Settings.FirstOrDefault();
                string accountNo = setting.DefaultReference + setting.StartingNumber.ToString().PadLeft(6, '0');
                setting.StartingNumber++;
                context.SaveChanges();
                return accountNo;
            }
        }
#endregion
    }
}