﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class DeliveryChargesController : EntityController<DeliveryCharge, DeliveryChargeViewModel>
    {
      
        public DeliveryChargesController()
            : base(new DeliveryChargeRepository(), new DeliveryChargeMapper())
        {

        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _NewDeliveryCharge()
        {
            return PartialView(new DeliveryChargeViewModel());
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewDeliveryCharge(DeliveryChargeViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }
            
            var model = _mapper.MapToModel(viewModel);

            model.SupplierID = !string.IsNullOrWhiteSpace(viewModel.SupplierID)
                ? Guid.Parse(viewModel.SupplierID)
                : null as Guid?;
            
            model.Category = model.SupplierID != null
                ? DeliveryChargeCategory.Supplier
                : DeliveryChargeCategory.General;

            // For now just make it milk and general
            model.Type = DeliveryChargeType.Milk;
            
            _repo.Create(model);

            return Json(new { ID = model.ID });
        }
        
        [HttpGet]
        public ActionResult _EditDeliveryCharge(Guid ID)
        {
            var model = _repo.Find(ID);
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }


        [HttpGet]
        public ActionResult _EditDeliveryChargeDetails(Guid ID)
        {
            var model = _repo.Find(ID);
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }

        // POST: /New Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditDeliveryChargeDetails(DeliveryChargeViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.

            return PartialView(viewModel);

        }
    }
    
}