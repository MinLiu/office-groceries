﻿using System;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class DeliveryChargePostcodeGridController : GridController<DeliveryChargePostcode, DeliveryChargePostcodeGridViewModel>
    {
      
        public DeliveryChargePostcodeGridController()
            : base(new DeliveryChargePostcodeRepository(), new DeliveryChargePostcodeGridMapper())
        {

        }
        
        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid deliveryChargeID)
        {
            var models = _repo.Read().Where(x => x.DeliveryChargeID == deliveryChargeID);
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

    }
    
}