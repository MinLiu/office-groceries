﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class DeliveryChargeGridController : GridController<DeliveryCharge, DeliveryChargeGridViewModel>
    {
      
        public DeliveryChargeGridController()
            : base(new DeliveryChargeRepository(), new DeliveryChargeGridMapper())
        {

        }
        
        [HttpPost]
        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText)
        {
            var models = _repo.Read().Where(x => x.Name.Contains(filterText) || x.Postcodes.Any(p => p.Postcode.Contains(filterText)));
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }
        
        [HttpPost]
        public virtual JsonResult ReadPotentialCustomers([DataSourceRequest] DataSourceRequest request, Guid? supplierID, string postcode)
        {
            var viewModels = new CompanySupplierRepository().Read()
                .Where(x => x.SupplierID == supplierID)
                .Where(x => x.EndDate == null || x.EndDate <= DateTime.Now)
                .Where(x => x.Company.NoDeliveryCharge == false)
                .Select(x => new DeliveryChargePotentialCustomerViewModel
                {
                    ID = x.CompanyID,
                    Name = x.Company.Name,
                    Postcode = x.Company.Postcode,
                })
                .ToList();
            
            return Json(viewModels.ToDataSourceResult(request));
        }

    }
    
}