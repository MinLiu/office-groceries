﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class FruitOrderHeadersGridController : GridController<FruitOrderHeader, FruitOrderHeaderViewModel>
    {
        public FruitOrderHeadersGridController()
            : base(new FruitOrderHeaderRepository(), new FruitOrderHeaderMapper())
        {

        }

        [Authorize(Roles = "NI-User")]
        [HttpPost]
        public JsonResult ReadAll([DataSourceRequest] DataSourceRequest request, string filterText, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate, List<OrderMode> statuses, bool includeDepreciated = false)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            fromDate = fromDate.Value.AddDays(-7);

            var models = _repo.Read()
                .Where(x => !(x.FromDate <= fromDate) || x.OrderMode == OrderMode.Regular || x.OrderMode == OrderMode.Draft)
                .Where(x => !(x.FromDate > toDate) || x.OrderMode == OrderMode.Regular || x.OrderMode == OrderMode.Draft)
                .Where(x => statuses.Contains(x.OrderMode))
                .Where(x => companyID == null || x.CompanyID == companyID)
                .Where(x => includeDepreciated || x.Depreciated == false);

            var totalCount = models.Count();

            var skip = request.PageSize * (request.Page - 1);
            var take = request.PageSize;

            skip = skip >= totalCount
                ? 0
                : skip;

            var viewModels = models
                .OrderByDescending(x => x.OrderMode == OrderMode.Regular)
                .ThenByDescending(x => x.FromDate)
                .ThenByDescending(x => x.LatestUpdated)
                .Skip(skip)
                .Take(take)
                .ToList()
                .Select(s => _mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = totalCount,
            };

            return Json(result);


        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, int dateType, DateTime? fromDate, DateTime? toDate, List<OrderMode> statuses)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            fromDate = fromDate.Value.AddDays(-7);
            var viewModels = _repo.Read()
                                  .Where(x => !(x.FromDate <= fromDate) || x.OrderMode == OrderMode.Regular)
                                  .Where(x => !(x.FromDate > toDate) || x.OrderMode == OrderMode.Regular)
                                  .Where(x => statuses.Contains(x.OrderMode))
                                  .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                  .ToList()
                                  .OrderBy(x => x.OrderMode).ThenBy(x => x.FromDate)
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));


        }

        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, FruitOrderHeaderViewModel viewModel)
        {
            // Find the existing entity.
            var model = _repo.Find(Guid.Parse(viewModel.ID));

            // Map the view model to the model and update the database.
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);
            _mapper.MapToViewModel(model, viewModel);

            return Json(new[] { viewModel }.ToDataSourceResult(request));
        }
    }

}