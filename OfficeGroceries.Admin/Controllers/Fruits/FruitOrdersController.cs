﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Web.Script.Serialization;
using System.Net.Mail;
using NPOI.HSSF.UserModel;
using System.Web.Hosting;
using System.Web.Configuration;
using System.Configuration;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class FruitOrdersController : EntityController<FruitOrder, FruitOrderViewModel>
    {
        public FruitOrdersController()
            : base(new FruitOrderRepository(), new FruitOrderMapper())
        {

        }

        [HttpPost]
        public ActionResult SaveOrders(string orders, OrderMode orderMode, bool submit, string date, string companyID, string prospectToken, bool sendDraftToCustomer, bool sendEmailToBoth = true, bool sendEmailToSupplierOnly = false)
        {
            DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;
            List<FruitOrderViewModel> fruitOrderViewModels = new JavaScriptSerializer().Deserialize<List<FruitOrderViewModel>>(orders);

            var emailResults = new List<EmailResult>();
            var message = "";
            if (!string.IsNullOrWhiteSpace(companyID))
            {
                Guid CompanyID = Guid.Parse(companyID);

                var result = SaveFruitOrderHelper.New()
                    .SetCompanyID(CompanyID)
                    .SaveCustomOrder(orderMode, fromDate, fruitOrderViewModels);

                // Send mails
                if (orderMode == OrderMode.Regular || orderMode == OrderMode.OneOff)
                {
                    FruitSnackSupplierChecker.CheckAndInitialize(result.OrderHeader.ID, CompanyID);

                    message = "Saved Successfully.";
                    
                    if (sendEmailToBoth)
                    {
                        emailResults = SendOrderChangeMail(CompanyID, orderMode, date, result.ChangedProductTypes);
                        if (emailResults.Count == 1) // only customers
                        {
                            message += "\nNo Emails are sent to suppliers as there are no changes in the order.";
                        }
                    }
                    else if (sendEmailToSupplierOnly)
                    {
                        emailResults = SendOrderChangeMailToSuppliers(CompanyID, orderMode, date, result.ChangedProductTypes);
                        if (!emailResults.Any())
                        {
                            message += "\nNo Emails are sent as there are no changes in the order.";
                        }
                    }
                    else
                    {
                        message += " No Emails are sent";
                    }

                    if (orderMode == OrderMode.Regular && result.OrderHeader.TotalNet == 0)
                    {
                        SendCancelOrderAlertEmail(CompanyID);
                    }
                    
                    if (emailResults.Any(x => x.Success))
                        message += "\nSuccessfully sent email(s) to: " + string.Join(", ", emailResults.Where(x => x.Success).Select(x => x.Message));
                    if (emailResults.Any(x => !x.Success))
                        message += "\nFailed to send Email(s) to " + string.Join(", ", emailResults.Where(x => !x.Success).Select(x => x.Message));
                    
                }
                else if (sendDraftToCustomer)
                {
                    SendDraftChangeMailToCustomer(CompanyID);
                }
            }
            else if (!string.IsNullOrWhiteSpace(prospectToken))
            {
                var result = SaveFruitOrderHelper.New()
                    .SetProspectToken(prospectToken)
                    .SaveProspectOrder(fruitOrderViewModels);
            }
            
            return Json(new
            {
                Success = true,
                Message = message,
            });
        }

        // Get dates which has orders
        public ActionResult GetOrderWeekDates(string companyID)
        {
            var companyRepo = new CompanyRepository();

            Guid CompanyID = string.IsNullOrWhiteSpace(companyID)? CurrentUser.CompanyID : Guid.Parse(companyID);

            var company = companyRepo.Find(CompanyID);

            var orderWeeks = _repo.Read()
                                  .Where(fo => fo.OrderHeader.CompanyID == CompanyID)
                                  .Where(fo => fo.OrderHeader.OrderMode == OrderMode.OneOff)
                                  .Select(fo => fo.OrderHeader.FromDate);

            var viewModel = new FruitsPageViewModel();
            viewModel.Set(company);
            foreach (var startdates in orderWeeks)
            {
                for (int i = 0; i < 7; i++)
                    viewModel.OrderWeekDates.Add(startdates.Value.AddDays(i));
            }

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult _AddToQuote(Guid fruitProductID)
        {
            var service = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), new SnapDbContext());

            var fruitProduct = new FruitProductRepository().Read().Where(f => f.ID == fruitProductID).FirstOrDefault();

            if (fruitProduct == null)
                return Json("No Content");

            var orderHeader = service.GetProspectOrder(ProspectToken);
            if (orderHeader == null)
            {
                orderHeader = new FruitOrderHeader()
                {
                    ProspectToken = ProspectToken,
                    OrderMode = OrderMode.Draft,
                    OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                };
                orderHeader = service.Create(orderHeader);
            }

            // Check if the product is already in the order
            if (orderHeader.Items != null && orderHeader.Items.Any(x => x.FruitProductID == fruitProduct.ID))
            {
                return Json("Already exists");
            }

            service.AddItem(orderHeader.ID, fruitProduct, null);

            return Json("Success");
        }

        #region Helper

        public void RecalculateOrderHeader(Guid orderHeaderID)
        {
            var repo = new FruitOrderHeaderRepository();
            var header = repo.Read()
                             .Where(x => x.ID == orderHeaderID)
                             .Include(x => x.Items)
                             .FirstOrDefault();

            var TotalVAT = 0m;
            var TotalNet = 0m;
            foreach(var item in header.Items)
            {
                TotalVAT += item.TotalVAT;
                TotalNet += item.TotalNet;
            }

            header.TotalVAT = TotalVAT;
            header.TotalNet = TotalNet;

            repo.Update(header, new[] { "TotalVAT", "TotalNet" });
        }

        #region Send Order Change Email

        private List<EmailResult> SendOrderChangeMail(Guid companyID, OrderMode orderMode, string date, List<FruitProductType> changedProductTypes)
        {
            var company = new CompanyRepository().Find(companyID);
            return SendOrderChangeMail(company, orderMode, date, changedProductTypes);
        }

        private List<EmailResult> SendOrderChangeMail(Company company, OrderMode orderMode, string date, List<FruitProductType> changedProductTypes)
        {
            var resultCustomer = SendOrderChangeMailToCustomer(company, orderMode, date);
            var resultSuppliers = SendOrderChangeMailToSuppliers(company, orderMode, date, changedProductTypes);

            resultSuppliers.Insert(0, resultCustomer);
            
            return resultSuppliers;
        }

        private EmailResult SendOrderChangeMailToCustomer(Company company, OrderMode orderMode, string date)
        {
            if (orderMode == OrderMode.OneOff)
                return SendOneOffOrderChangeMailToCustomer(company, date);
            else if (orderMode == OrderMode.Regular)
                return SendPermanentOrderChangeMailToCustomer(company);

            throw new Exception($"{orderMode} is not supported");
        }

        private void SendDraftChangeMailToCustomer(Guid companyID)
        {
            var company = new CompanyRepository().Find(companyID);
            SendDraftChangeMailToCustomer(company);
        }

        private void SendDraftChangeMailToCustomer(Company company)
        {
            var mailMessage = FruitDraftOrderChangeEmailHelper.GetEmail(company);
            EmailService.SendEmail(mailMessage, Server);
        }

        // TO CUSTOMER 8
        private EmailResult SendOneOffOrderChangeMailToCustomer(Company company, string date)
        {
            var mailMessage = FruitOneOffOrderChangeEmailHelper.GetEmail(company, date);
            var success = EmailService.SendEmail(mailMessage, Server);
            return new EmailResult(success, company.Name);
        }

        // TO CUSTOMER 9
        private EmailResult SendPermanentOrderChangeMailToCustomer(Company company)
        {
            var mailMessage = FruitRegularOrderChangeEmailHelper.GetEmail(company);
            var success = EmailService.SendEmail(mailMessage, Server);
            return new EmailResult(success, company.Name);
        }

        private List<EmailResult> SendOrderChangeMailToSuppliers(Guid companyID, OrderMode orderMode, string date, List<FruitProductType> changedProductTypes)
        {
            var company = new CompanyRepository().Find(companyID);
            return SendOrderChangeMailToSuppliers(company, orderMode, date, changedProductTypes);
        }

        private List<EmailResult> SendOrderChangeMailToSuppliers(Company company, OrderMode orderMode, string date, List<FruitProductType> changedProductTypes)
        {
            if (orderMode == OrderMode.OneOff)
                return SendOneOffOrderChangeMailToSuppliers(company, date, changedProductTypes);
            else if (orderMode == OrderMode.Regular)
                return SendPermanentOrderChangeMailToSuppliers(company, changedProductTypes);

            throw new Exception($"{orderMode} is not supported");
        }

        // TO SUPPLIER 6
        private List<EmailResult> SendOneOffOrderChangeMailToSuppliers(Company company, string date, List<FruitProductType> changedProductTypes)
        {
            DateTime fromDate = DateTime.Parse(date);
            DateTime toDate = fromDate.AddDays(6);
            
            var supplierTypes = new List<SupplierType>();
            if (changedProductTypes.Contains(FruitProductType.Fruit))
                supplierTypes.Add(SupplierType.Fruits);
            if (changedProductTypes.Contains(FruitProductType.Snack))
                supplierTypes.Add(SupplierType.Snacks);
            
            List<CompanySupplier> companySuppliers = company.CompanySuppliers
                                                            .Where(x => supplierTypes.Contains(x.Supplier.SupplierType))
                                                            .Where(x => x.StartDate.HasValue)
                                                            .Where(x => !(toDate < x.StartDate))
                                                            .Where(x => !x.EndDate.HasValue || !(fromDate > x.EndDate))
                                                            .OrderBy(x => x.StartDate)
                                                            .ToList();

            var result = new List<EmailResult>();
            foreach (var companySupplier in companySuppliers)
            {
                var success = false;
                try
                {
                    var mailMessage = FruitOneOffOrderChangeSupplierEmailHelper.GetEmail(company, companySupplier, fromDate, toDate);
                    success = EmailService.SendEmail(mailMessage, Server);
                }
                catch (Exception e) { }

                result.Add(new EmailResult(success, $"{companySupplier.Supplier.Name}({companySupplier.Supplier.SupplierType})"));
                
            }

            return result;
        }

        // TO SUPPLIER 7
        private List<EmailResult> SendPermanentOrderChangeMailToSuppliers(Company company, List<FruitProductType> changedProductTypes)
        {
            var supplierTypes = new List<SupplierType>();
            if (changedProductTypes.Contains(FruitProductType.Fruit))
                supplierTypes.Add(SupplierType.Fruits);
            if (changedProductTypes.Contains(FruitProductType.Snack))
                supplierTypes.Add(SupplierType.Snacks);
            
            List<CompanySupplier> companySuppliers = company.CompanySuppliers
                                                            .Where(x => supplierTypes.Contains(x.Supplier.SupplierType))
                                                            .Where(x => x.StartDate.HasValue)
                                                            .Where(x => !x.EndDate.HasValue || x.EndDate.Value >= DateTime.Today)
                                                            .OrderBy(x => x.StartDate)
                                                            .ToList();
            var result = new List<EmailResult>();
            foreach (var companySupplier in companySuppliers)
            {
                var success = false;
                try
                {
                    var mailMessage = FruitRegularOrderChangeSupplierEmailHelper.GetEmail(company, companySupplier);
                    success = EmailService.SendEmail(mailMessage, Server);
                }
                catch { }
                
                result.Add(new EmailResult(success, $"{companySupplier.Supplier.Name}({companySupplier.Supplier.SupplierType})"));
                
            }
            
            return result;
        }

        #endregion

        #region Initial Order set up email
        public bool SendOrderInitializeMail(Guid companyID, DateTime startFrom, CompanySupplier companySupplier)
        {
            var company = new CompanyRepository().Find(companyID);
            return SendOrderInitializeMail(company, startFrom, companySupplier);
        }

        public bool SendOrderInitializeMail(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            if (company.IsTransferred == false &&
                company.CompanySuppliers.Count(x => x.Supplier.SupplierType == SupplierType.Fruits || x.Supplier.SupplierType == SupplierType.Snacks) < 2)
            {
                // only send to customer if it is not transferred and it's doesn't receive initialize email from other (fruit or snack) suppliers before.
                SendOrderIntializeMailToCustomer(company, startFrom, companySupplier);
            }
            SendOrderIntializeMailToSupplier(company, startFrom, companySupplier);
            return true;
        }

        // TO CUSTOMER 4
        public void SendOrderIntializeMailToCustomer(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            var mailMessage = FruitOrderInitializeEmailHelper.GetEmail(company, startFrom, companySupplier);
            EmailService.SendEmail(mailMessage, Server);
        }

        // TO SUPPLIER 5
        public void SendOrderIntializeMailToSupplier(Company company, DateTime startFrom, CompanySupplier companySupplier)
        {
            try
            {
                var mailMessage = FruitOrderInitializeSupplierEmailHelper.GetEmail(company, startFrom, companySupplier);
                EmailService.SendEmail(mailMessage, Server);
            }
            catch (Exception e) { }
        }
        #endregion

        private void SendCancelOrderAlertEmail(Guid companyID)
        {
            var company = new CompanyRepository().Find(companyID);
            SendCancelOrderAlertEmail(company);
        }

        private void SendCancelOrderAlertEmail(Company company)
        {
            var mailMessage = CancelOrderAlertEmailHelper.GetEmail(company, "Fruit");
            EmailService.SendEmail(mailMessage, Server);
        }

        #endregion

        private struct EmailResult
        {
            public EmailResult(bool success, string message)
            {
                Success = success;
                Message = message;
            }
            public bool Success { get; set; }
            public string Message { get; set; }
        }
    }

}