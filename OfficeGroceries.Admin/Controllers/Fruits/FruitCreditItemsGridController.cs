﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class FruitCreditItemsGridController : GridController<FruitCreditItem, FruitCreditItemViewModel>
    {
        public FruitCreditItemsGridController()
            : base(new FruitCreditItemRepository(), new FruitCreditItemMapper())
        {

        }

        [Authorize(Roles = "NI-User")]
        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid orderHeaderID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.FruitCreditHeader.OrderHeaderID == orderHeaderID)
                                  .ToList()
                                  .OrderByDescending(x => x.FruitCreditHeader.Created).ThenBy(x => x.FruitOrderItem.ProductName)
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));


        }
    }

}