﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class MilkController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index(string companyID, string prospectToken)
        {
            var viewModel = new MilkPageViewModel();
            var milkOrderRepo = new MilkOrderRepository();
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("NI-User"))
                {
                    if (!string.IsNullOrEmpty(companyID))
                    {
                        SetCustomerStatus(companyID);
                        ViewBag.CompanyID = companyID;
                        var onBehalfOfcompany = new CompanyRepository().Find(Guid.Parse(companyID));

                        viewModel.Set(onBehalfOfcompany);
                        var orderWeeks = milkOrderRepo.Read()
                                                       .Where(fo => fo.OrderHeader.CompanyID == onBehalfOfcompany.ID)
                                                       .Where(fo => fo.OrderHeader.OrderMode == OrderMode.OneOff)
                                                       .Select(fo => fo.OrderHeader.FromDate);
                        foreach (var startdates in orderWeeks)
                        {
                            for (int i = 0; i < 7; i++)
                                viewModel.OrderWeekDates.Add(startdates.Value.AddDays(i));
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(prospectToken))
                    {
                        ViewBag.ProspectToken = prospectToken;
                    }
                }
                else
                {
                    viewModel.Set(CurrentUser.Company);
                    var orderWeeks = milkOrderRepo.Read()
                                                   .Where(fo => fo.OrderHeader.CompanyID == CurrentUser.Company.ID)
                                                   .Where(fo => fo.OrderHeader.OrderMode == OrderMode.OneOff)
                                                   .Select(fo => fo.OrderHeader.FromDate);
                    foreach (var startdates in orderWeeks)
                    {
                        for (int i = 0; i < 7; i++)
                            viewModel.OrderWeekDates.Add(startdates.Value.AddDays(i));
                    }
                }
            }

            var page = new AisleRepository().Read().Where(x => x.Name == "Milk").FirstOrDefault();
            if (page != null)
            {
                ViewBag.MetaTitle = page.MetaTitle;
                ViewBag.MetaKeyword = page.MetaKeyword;
                ViewBag.MetaDescription = page.MetaDescription;
            }

            return View(viewModel);
        }

        public ActionResult Goto(string companyID, string prospectToken)
        {
            if (!string.IsNullOrEmpty(companyID))
            {
                return RedirectToAction("Index", new { companyID = companyID });
            }
            else
            {
                return RedirectToAction("Index", new { prospectToken = prospectToken });
            }
        }

        [HttpGet]
        public ActionResult Invoice()
        {
            return View();
        }

        public ActionResult _MilkOrderRow(Guid milkProductID, string companyID, string prospectToken, OrderMode orderMode, DateTime? date)
        {
            var viewModel = new MilkOrderRowPageViewModel(orderMode, date, companyID);

            var milk = new MilkProductRepository().Read().Where(f => f.ID == milkProductID).FirstOrDefault();
            var rowViewModel = new MilkOrderViewModel()
            {
                ID = Guid.NewGuid().ToString(),
                MilkID = milk.ID.ToString(),
                ImageUrl = milk.ImageURL,
                ProductName = milk.Name,
                Unit = milk.Unit,
                Monday = 0,
                Tuesday = 0,
                Wednesday = 0,
                Thursday = 0,
                Friday = 0,
                Saturday = 0,
                Sunday = 0,
                WeeklyVolume = 0,
                WeeklyTotal = 0,
                OrderMode = OrderMode.Draft,
            };

            rowViewModel.Price = !string.IsNullOrWhiteSpace(companyID)
                ? milk.ExclusivePrice(Guid.Parse(companyID))
                : milk.Price;

            // Get Non-Delivery Days
            if (!string.IsNullOrWhiteSpace(companyID))
                SetNonDeliveryDays(companyID);
            else
                SetNonDeliveryDays();

            viewModel.MilkOrders.Add(rowViewModel);

            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult MilkOrderDetails(Guid id)
        {
            var repo = new MilkOrderHeaderRepository();
            var mapper = new MilkOrderHeaderMapper();

            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return View(viewModel);
        }

        //[AllowAnonymous]
        //public ActionResult GetOrders(OrderMode orderMode, string date, string companyID)
        //{
        //    DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date)? DateTime.Parse(date) : (DateTime?)null;

        //    List<MilkOrderViewModel> milkOrders = GetOrders(orderMode, fromDate, companyID);

        //    if (milkOrders.Count > 0)
        //        return PartialView("_MilkOrderRow", milkOrders);
        //    else
        //        return Json("");
        //}

        [AllowAnonymous]
        public ActionResult _WeeklyOrder(OrderMode? orderMode, string date, string companyID, string prospectToken)
        {
            if (!orderMode.HasValue) orderMode = OrderMode.Draft;
            DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date) : (DateTime?)null;

            var viewModel = new MilkWeeklyOrderPageViewModel(orderMode.Value, date, companyID);
            if (User.Identity.IsAuthenticated)
            {
                // NI-Users
                if (User.IsInRole("NI-User"))
                {
                    if (!string.IsNullOrWhiteSpace(companyID))
                    {
                        var company = new CompanyRepository().Find(Guid.Parse(companyID));
                        viewModel.Set(company);
                    }
                    else if (!string.IsNullOrEmpty(prospectToken))
                    {
                        viewModel.Set(prospectToken);
                        viewModel.MilkOrders = GetOrders(prospectToken);

                        return PartialView(viewModel);
                    }
                }
                // Customers
                else
                {
                    viewModel.Set(CurrentUser.Company);
                }
            }

            viewModel.MilkOrders = GetOrders(orderMode.Value, fromDate, companyID);

            return PartialView(viewModel);
        }

        public ActionResult _CopyOrders(OrderMode orderMode, string date, string companyID)
        {
            DateTime? fromDate = orderMode == OrderMode.OneOff && !string.IsNullOrWhiteSpace(date) ? DateTime.Parse(date).AddDays(-7) : (DateTime?)null;

            List<MilkOrderViewModel> milkOrders = GetOrders(orderMode, fromDate, companyID);

            milkOrders.Each(f => f.ID = Guid.NewGuid().ToString());

            if (milkOrders.Count > 0)
                return PartialView("_MilkOrderRow", milkOrders);
            else
                return Json("");
        }

        public ActionResult _CalendarForm()
        {
            return PartialView();
        }

        #region Helper

        private List<MilkOrderViewModel> GetOrders(OrderMode orderMode, DateTime? fromDate, string companyID)
        {
            var service = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), new SnapDbContext());
            var milkOrderMapper = new MilkOrderMapper();

            List<MilkOrderViewModel> milkOrders = new List<MilkOrderViewModel>();
            // NI-Users
            if (string.IsNullOrWhiteSpace(companyID))
            {
                throw new Exception();
            }
            var CompanyID = Guid.Parse(companyID);
            milkOrders = service
                .GetCustomerOrderItems(CompanyID, orderMode, fromDate)
                .OrderBy(f => f.ProductName)
                .ThenBy(f => f.ID)
                .Select(f => milkOrderMapper.MapToViewModel(f))
                .ToList();

            // Get Non-Delivery Days
            SetNonDeliveryDays(CompanyID);

            return milkOrders;
        }

        private List<MilkOrderViewModel> GetOrders(string prospectToken)
        {
            var milkOrderRepo = new MilkOrderRepository();
            var milkOrderMapper = new MilkOrderMapper();

            List<MilkOrderViewModel> milkOrders = new List<MilkOrderViewModel>();
            milkOrders = milkOrderRepo
                .Read()
                .Where(f => f.OrderHeader.ProspectToken == prospectToken)
                .OrderBy(f => f.ProductName)
                .ThenBy(f => f.ID)
                .ToList()
                .Select(f => milkOrderMapper.MapToViewModel(f))
                .ToList();

            SetNonDeliveryDays();

            return milkOrders;
        }

        private void SetNonDeliveryDays(string companyID)
        {
            SetNonDeliveryDays(Guid.Parse(companyID));
        }

        private void SetNonDeliveryDays()
        {
            SetNonDeliveryDays((Company)null);
        }

        private void SetNonDeliveryDays(Guid companyID)
        {
            var company = new CompanyRepository().Find(companyID);
            SetNonDeliveryDays(company);
            
        }

        private void SetNonDeliveryDays(Company company)
        {
            if (company != null)
            {
                ViewBag.Monday = company.MilkDeliveryMonday;
                ViewBag.Tuesday = company.MilkDeliveryTuesday;
                ViewBag.Wednesday = company.MilkDeliveryWednesday;
                ViewBag.Thursday = company.MilkDeliveryThursday;
                ViewBag.Friday = company.MilkDeliveryFriday;
                ViewBag.Saturday = company.MilkDeliverySaturday;
                ViewBag.Sunday = company.MilkDeliverySunday;
            }
            else
            {
                ViewBag.Monday = true;
                ViewBag.Tuesday = true;
                ViewBag.Wednesday = true;
                ViewBag.Thursday = true;
                ViewBag.Friday = true;
                ViewBag.Saturday = true;
                ViewBag.Sunday = true;
            }
        }

        #endregion


    }

}