﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using Kendo.Mvc.Extensions;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class MilkOrderHeadersGridController : GridController<MilkOrderHeader, MilkOrderHeaderViewModel>
    {
        public MilkOrderHeadersGridController()
            : base(new MilkOrderHeaderRepository(), new MilkOrderHeaderMapper())
        {

        }

        [HttpPost]
        public JsonResult ReadAll([DataSourceRequest] DataSourceRequest request, string filterText, Guid? companyID, int dateType, DateTime? fromDate, DateTime? toDate, List<OrderMode> statuses, bool includeDepreciated = false)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            fromDate = fromDate.Value.AddDays(-7);

            var models = _repo.Read()
                .Where(x => !(x.FromDate <= fromDate) || x.OrderMode == OrderMode.Regular || x.OrderMode == OrderMode.Draft)
                .Where(x => !(x.FromDate > toDate) || x.OrderMode == OrderMode.Regular || x.OrderMode == OrderMode.Draft)
                .Where(x => statuses.Contains(x.OrderMode))
                .Where(x => companyID == null || x.CompanyID == companyID)
                .Where(x => includeDepreciated || x.Depreciated == false);

            var totalCount = models.Count();

            var skip = request.PageSize * (request.Page - 1);
            var take = request.PageSize;

            skip = skip >= totalCount
                ? 0
                : skip;

            var viewModels = models
                .OrderByDescending(x => x.OrderMode == OrderMode.Regular && x.Depreciated == false)
                .ThenByDescending(x => new { date = (x.OrderMode == OrderMode.History || x.OrderMode== OrderMode.OneOff) ? x.FromDate : x.LatestUpdated})
                //.ThenByDescending(x => x.LatestUpdated)
                .Skip(skip)
                .Take(take)
                .ToList()
                .Select(s => _mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = totalCount,
            };

            return Json(result);
        }

        [HttpPost]
        public JsonResult ReadCalendar([DataSourceRequest] DataSourceRequest request, Guid? companyID,
            DateTime fromDate, DateTime toDate, List<OrderMode> statuses,
            bool includeDepreciated = false)
        {
            var models = ReadData(companyID, fromDate, toDate, statuses, includeDepreciated);
            var viewModels = models.ToList().Select(x => new OrderCalendarEventViewModel(x));

            return Json(viewModels.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, MilkOrderHeaderViewModel viewModel)
        {
            // Find the existing entity.
            var model = _repo.Find(Guid.Parse(viewModel.ID));

            // Map the view model to the model and update the database.
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);
            _mapper.MapToViewModel(model, viewModel);

            return Json(new[] { viewModel }.ToDataSourceResult(request));
        }

        private IQueryable<MilkOrderHeader> ReadData(Guid? companyID, DateTime fromDate, DateTime toDate, List<OrderMode> statuses, bool includeDepreciated)
        {
            toDate = toDate.AddDays(1);

            var query = _repo.Read()
                .Where(x => x.LatestUpdated <= toDate)
                .Where(x => x.LatestUpdated >= fromDate)
                .Where(x => statuses.Contains(x.OrderMode))
                .Where(x => companyID == null || x.CompanyID == companyID)
                .Where(x => includeDepreciated || x.Depreciated == false);


            // exclude history orders if company is null, otherwise there are too many
            if (companyID == null)
                query = query.Where(x => x.OrderMode != OrderMode.History);

            return query;
        }
    }

}