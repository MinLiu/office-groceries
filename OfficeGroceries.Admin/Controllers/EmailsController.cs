﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class EmailsController : BaseController
    {
        public ActionResult _NewEmail(EmailType type, Guid? supplierID)
        {
            var supplierName = supplierID != null
                ? new SupplierRepository().Find(supplierID.Value).Name
                : "";
            
            return PartialView(new EmailViewModel
            {
                Type = type,
                SupplierID = supplierID,
                SupplierName = supplierName,
            });
        }

        [HttpPost]
        public ActionResult Send(EmailViewModel viewModel)
        {
            var emailAttachmentRepo = new EmailAttachmentRepository();
            var attachments = emailAttachmentRepo.Read().Where(x => x.ReferenceID == viewModel.ReferenceID).ToList();

            var toEmails = GetEmailList(viewModel);

            foreach (var toEmail in toEmails)
            {
                var mailMessage = GeneralEmailHelper.GetEmail(
                    viewModel.Title
                    , viewModel.Content
                    , new List<string>(){toEmail}
                    , null
                    , attachments);
                
                EmailService.SendEmail(mailMessage, Server);
            }
            
            return OkResult();
        }

        private List<string> GetEmailList(EmailViewModel viewModel)
        {
            var toEmailList = new List<string>();
            
            switch (viewModel.Type)
            {
                case EmailType.SupplierCustomers:
                    toEmailList = new CompanySupplierRepository().Read()
                        .Where(cs => cs.SupplierID == viewModel.SupplierID)
                        .Where(cs => cs.EndDate == null || cs.EndDate >= DateTime.Now)
                        .Select(cs => cs.Company.Email)
                        .ToList();
                    break;
                case EmailType.AllSuppliers:
                    var suppliers = new SupplierRepository().Read()
                        .Where(s => s.SupplierType == SupplierType.Milk || s.SupplierType == SupplierType.Fruits)
                        .Where(s => s.Deleted == false);
                    var primaryEmails = suppliers.Select(x => x.Email).ToList();
                    var secondaryEmails = suppliers.Select(x => x.SecondaryEmail).ToList();
                    toEmailList = primaryEmails.Concat(secondaryEmails).ToList();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return toEmailList;
        }
    }

}