﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize(Roles="NI-User")]
    public class ProductCategoryManagementController : GridController<ProductCategory, ProductCategoryViewModel>
    {

        public ProductCategoryManagementController()
            : base(new ProductCategoryRepository(), new ProductCategoryMapper())
        {

        }

        public ActionResult Edit()
        {
            return View();
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, ProductCategoryViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Find the existing entity.
                var model = _repo.Find(Guid.Parse(viewModel.ID));

                // Map the view model to the model and update the database.
                _mapper.MapToModel(viewModel, model);
                _repo.Update(model);

                UpdateInternalName(model);

                _mapper.MapToViewModel(model, viewModel);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, ProductCategoryViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _mapper.MapToModel(viewModel);
                _repo.Create(model);
                UpdateInternalName(model);
                viewModel.ID = model.ID.ToString();
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string aisleID)
        {
            var result = _repo.Read()
                              .Where(x => aisleID == null || aisleID == "" || x.AisleID.ToString() == aisleID)
                              .OrderBy(x => x.SortPos).ThenBy(x =>x.Name)
                              .ToList()
                              .Select(x => _mapper.MapToViewModel(x))
                              .ToTreeDataSourceResult(
                                    request,
                                    e => e.ID,
                                    e => e.ParentID,
                                    e => e
                              );
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private void UpdateInternalName(ProductCategory category)
        {
            var internalName = InternalNameHelper.Convert(category.Name);

            var finalInternalName = internalName;
            var postFix = 2;

            while (_repo.Read().Where(p =>p.AisleID == category.AisleID).Where(p => p.ID != category.ID).Where(p => p.InternalName == finalInternalName).Any())
            {
                finalInternalName = finalInternalName + postFix++;
            }

            if (category.InternalName != finalInternalName)
            {
                category.InternalName = finalInternalName;
                _repo.Update(category, new string[] { "InternalName" });
            }

        }
    }
}