﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class DryGoodExcludeCompanyGridController : GridController<DryGoodsExcludeCompany, ProductExcludeCompanyGridViewModel>
    {

        public DryGoodExcludeCompanyGridController()
            : base(new DryGoodsExcludeCompanyRepository(), new DryGoodsExcludeCompanyGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid productID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.DryGoodProductID == productID)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

    }
}