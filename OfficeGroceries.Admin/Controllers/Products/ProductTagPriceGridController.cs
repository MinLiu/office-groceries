﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class ProductTagPriceGridController : GridController<ProductTagPrice, ProductTagPriceViewModel>
    {
        public ProductTagPriceGridController()
            : base(new ProductTagPriceRepository(), new ProductTagPriceMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid productID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.ProductID == productID)
                                  .ToList()
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, ProductTagPriceViewModel viewModel)
        {
            var result = base.Create(request, viewModel);

            return result;
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, ProductTagPriceViewModel viewModel)
        {
            var result = base.Update(request, viewModel);

            return result;
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, ProductTagPriceViewModel viewModel)
        {
            var result = base.Destroy(request, viewModel);

            return result;
        }
    }
}