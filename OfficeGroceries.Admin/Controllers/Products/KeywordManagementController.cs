﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using System;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class KeywordManagementController : BaseController
    {

        private readonly ProductRepository _repo = new ProductRepository();
        
        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? categoryID, Guid? aisleID, Guid? supplierID, string filterText)
        {
            filterText = filterText.ToLower();
            var models = _repo.Read()
                              .Where(x => x.Deleted == false)
                              .Where(x => supplierID == null || x.SupplierID == supplierID)
                              .Where(x => aisleID == null || x.DryGoodsAisles.Select(a => a.AisleID).Contains(aisleID.Value))
                              .Where(x => categoryID == null || x.DryGoodsCategories.Select(c => c.CategoryID).Contains(categoryID.Value))
                              .Where(x => filterText == "" || x.ProductCode.ToLower().Contains(filterText) || x.Name.ToLower().Contains(filterText) || x.ShortDescription.ToLower().Contains(filterText));

            if (categoryID != null)
            {
                models = models.OrderBy(x => x.DryGoodsCategories
                                                .Where(dc => dc.Category.AisleID == aisleID)
                                                .Where(dc => dc.CategoryID == categoryID)
                                                .OrderBy(dc => dc.Category.SortPos)
                                                .FirstOrDefault()
                                                .Category
                                                .SortPos)
                                 .ThenBy(x => x.DryGoodsCategories
                                               .Where(dc => dc.Category.AisleID == aisleID)
                                               .Where(dc => dc.CategoryID == categoryID)
                                               .OrderBy(dc => dc.Category.SortPos)
                                               .FirstOrDefault()
                                               .SortPos);
            }
            else
            {
                models = models.OrderBy(x => x.DryGoodsCategories
                                                .Where(dc => dc.Category.AisleID == aisleID)
                                                .OrderBy(dc => dc.Category.SortPos)
                                                .FirstOrDefault()
                                                .Category
                                                .SortPos)
                                 .ThenBy(x => x.DryGoodsCategories
                                               .Where(dc => dc.Category.AisleID == aisleID)
                                               .OrderBy(dc => dc.Category.SortPos)
                                               .FirstOrDefault()
                                               .SortPos);
            }

            var viewModels = models
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new ProductKeywordGridVm()
                {
                    ID = x.ID,
                    ImageURL = x.ImageURL,
                    ThumbnailImageURL = x.ThumbnailImageURL,
                    ProductCode = x.ProductCode,
                    Name = x.Name,
                    Keywords = x.Keywords
                })
                .ToList();

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = models.Count()
            };

            return Json(result);
        }

        public ActionResult Update(ProductKeywordGridVm viewModel)
        {
            var model = _repo.Read().FirstOrDefault(x => x.ID == viewModel.ID);

            model.Keywords = viewModel.Keywords;
            
            _repo.Update(model);
            
            return Json(new { success = true });
        }
    }
}