﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class ShoppingCartGridController : GridController<ShoppingCartItem, ShoppingCartItemViewModel>
    {

        public ShoppingCartGridController()
            : base(new ShoppingCartItemRepository(), new ShoppingCartItemMapper())
        {

        }

        //[HttpPost]
        //[Authorize(Roles = "NI-User")]
        //public JsonResult ReadAll([DataSourceRequest] DataSourceRequest request, string filterText, Guid? companyID)
        //{
        //    var viewModels = _repo.Read()
        //                          .Where(x => companyID == null || x.CompanyID == companyID)
        //                          .Where(x => x.CompanyID != null)
        //                          .GroupBy(x => x.CompanyID)
        //                          .ToList()
        //                          .Select(group => new ShoppingCartGridViewModel
        //                          {
        //                              CompanyID = group.Key,
        //                              CompanyName = group.First().Company.Name,
        //                              Count = group.Count(),
        //                              LastUpdatedTime = group.OrderByDescending(x => x.UpdateTime).First().UpdateTime,
        //                              Total = group.Sum(x => x.Qty * x.Product.ExclusivePrice(x.CompanyID))
        //                          });
        //    return Json(viewModels.ToDataSourceResult(request));
        //}

        [HttpPost]
        [Authorize(Roles = "NI-User")]
        public JsonResult ReadAll([DataSourceRequest] DataSourceRequest request, string filterText, Guid? companyID)
        {
            var result = new List<ShoppingCartGridViewModel>();

            var allCartItemsByCompany = _repo
                .Read()
                .Where(x => companyID == null || x.CompanyID == companyID)
                .Where(x => x.CompanyID != null)
                .GroupBy(x => x.Company)
                .ToList();

            foreach(var companyCartItems in allCartItemsByCompany)
            {
                var company = companyCartItems.Key;

                var gridViewModel = new ShoppingCartGridViewModel()
                {
                    CompanyID = company.ID,
                    CompanyName = company.Name,
                    Count = 0,
                    LastUpdatedTime = DateTime.MinValue,
                    Total = 0
                };

                foreach (var item in companyCartItems)
                {
                    if (item.Product.Deleted == false && item.Product.IsActive == true && item.Product.IsExclusive == false || item.Product.ExclusivePrices.Where(ep => ep.CusomterID == company.ID).Any())
                    {
                        if ((item.Product.Supplier.UseMasterAccount && company.EasySuppliersActivated) || company.Method_IsLive(item.Product.Supplier))
                        {
                            if (item.UpdateTime >= gridViewModel.LastUpdatedTime)
                                gridViewModel.LastUpdatedTime = item.UpdateTime;
                            gridViewModel.Count += item.Qty;
                            gridViewModel.Total += item.Qty * item.Product.Price * (1 + item.Product.VAT);
                        }
                    }
                }

                if (gridViewModel.Count > 0)
                    result.Add(gridViewModel);
            }
                                  
            return Json(result.ToDataSourceResult(request));
        }

        [HttpPost]
        [Authorize(Roles = "NI-User")]
        public JsonResult ReadProspectQuotes([DataSourceRequest] DataSourceRequest request, string prospectToken)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.ProspectToken == prospectToken)
                                  .ToList()
                                  .Select(x => _mapper.MapToViewModel(x));

            return Json(viewModels.ToDataSourceResult(request));      
        }
    }
}