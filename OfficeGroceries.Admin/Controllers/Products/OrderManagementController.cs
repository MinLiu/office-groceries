﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class OrderManagementController : EntityController<OneOffOrder, OneOffOrderViewModel>
    {

        public OrderManagementController()
            : base(new OneOffOrderRepository(), new OneOffOrderMapper())
        {

        }

        [HttpGet]
        public ActionResult DryGoodsProforma(string id)
        {
            if (!string.IsNullOrEmpty(id))
                ViewBag.ID = id;
            return View();
        }
        
        [HttpGet]
        public ActionResult DryGoodsOrders(string id)
        {
            if (!string.IsNullOrEmpty(id))
                ViewBag.ID = id;
            return View();
        }

        [HttpGet]
        public ActionResult FruitOrders()
        {
            return View();
        }

        [HttpGet]
        public ActionResult MilkOrders(string id)
        {
            ViewBag.ID = id;
            return View();
        }

        [HttpGet]
        public ActionResult FruitOrderDetails(Guid id)
        {
            var repo = new FruitOrderHeaderRepository();
            var mapper = new FruitOrderHeaderMapper();

            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult MilkOrderDetails(Guid id)
        {
            var repo = new MilkOrderHeaderRepository();
            var mapper = new MilkOrderHeaderMapper();

            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult MilkOrderDetailsPage(Guid id)
        {
            var repo = new MilkOrderHeaderRepository();
            var mapper = new MilkOrderHeaderMapper();

            var viewModel = mapper.MapToViewModel(repo.Find(id));

            ViewBag.Page = true;

            return View("MilkOrderDetails", viewModel);
        }

        [HttpGet]
        public ActionResult DryGoodsOrderDetails(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var mapper = new OneOffOrderMapper();

            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return View(viewModel);
        }
        
        [HttpGet]
        public ActionResult ProformaOrderDetails(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var mapper = new ProformaOrderMapper();

            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult _CreateMilkCredit(Guid id)
        {
            var repo = new MilkOrderHeaderRepository();
            var viewModel = repo.Read().Where(x => x.ID == id)
                                       .Include(x => x.Items)
                                       .First();
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _CreateMilkCredit(List<MilkOrder> items, Guid orderHeaderID, string narrative)
        {
            var milkOrderHeaderRepo = new MilkOrderHeaderRepository();
            var orderHeader = milkOrderHeaderRepo.Find(orderHeaderID);

            var milkCreditHeaderRepo = new MilkCreditHeaderRepository();
            var creditHeader = new MilkCreditHeader()
            {
                ID = Guid.NewGuid(),
                CreateByID = CurrentUser.Id,
                Created = DateTime.Now,
                Narrative = narrative,
                OrderHeaderID = orderHeaderID,
                CompanyID = orderHeader.CompanyID,
                SupplierID = orderHeader.SupplierID
            };
            milkCreditHeaderRepo.Create(creditHeader);

            var orderItemRepo = new MilkOrderRepository();
            var creditItemRepo = new MilkCreditItemRepository();
            foreach (var item in items)
            {
                var oriItem = orderItemRepo.Find(item.ID);

                var creditItem = new MilkCreditItem()
                {
                    MilkCreditHeaderID = creditHeader.ID,
                    MilkOrderItemID = oriItem.ID,
                    Monday = item.Monday - oriItem.Monday,
                    Tuesday = item.Tuesday - oriItem.Tuesday,
                    Wednesday = item.Wednesday - oriItem.Wednesday,
                    Thursday = item.Thursday - oriItem.Thursday,
                    Friday = item.Friday - oriItem.Friday,
                    Saturday = item.Saturday - oriItem.Saturday
                };

                if (creditItem.Monday == 0 && creditItem.Tuesday == 0 && creditItem.Wednesday == 0 && creditItem.Thursday == 0 && creditItem.Friday == 0 && creditItem.Saturday == 0)
                {
                    continue;
                }

                creditItemRepo.Create(creditItem);

                oriItem.Monday = item.Monday;
                oriItem.Tuesday = item.Tuesday;
                oriItem.Wednesday = item.Wednesday;
                oriItem.Thursday = item.Thursday;
                oriItem.Friday = item.Friday;
                oriItem.Saturday = item.Saturday;

                oriItem.CalculateTotal();
                orderItemRepo.Update(oriItem);

            }

            // Calculate header
            orderHeader = milkOrderHeaderRepo.Read().Where(x => x.ID == orderHeaderID).Include(x => x.Items).FirstOrDefault();
            orderHeader.CalculateTotal();
            milkOrderHeaderRepo.Update(orderHeader);

            return Json(new { Success = true });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SendMilkIssueMail(string subject, string message, Guid orderID, Guid? referenceID)
        {
            var milkOrderHeaderRepo = new MilkOrderHeaderRepository();
            var orderHeader = milkOrderHeaderRepo.Find(orderID);

            var attachments = new EmailAttachmentRepository().Read().Where(x => x.ReferenceID == referenceID).ToList();
            
            var mailMessage = MilkOrderIssueEmailHelper.GetEmail(orderHeader.Supplier, subject, message, attachments);
            var success = EmailService.SendEmail(mailMessage, Server);

            var result = success
                ? $"Successfully sent email to {string.Join(", ", mailMessage.To.Select(x => x.Address))}"
                : $"Failed to send email";

            if (success)
            {
                var issueEmailRepo = new IssueEmailRepository();
                var issueEmail = new IssueEmail()
                {
                    ID = Guid.NewGuid(),
                    CompanyID = orderHeader.Company.ID,
                    SupplierID = orderHeader.Supplier.ID,
                    Subject = subject,
                    Message = message,
                    UserName = CurrentUser.ToFullName()
                };
                issueEmailRepo.Create(issueEmail);
            }
            
            return Content(result);
        }
        
        [HttpGet]
        public ActionResult _CreateFruitCredit(Guid id)
        {
            var repo = new FruitOrderHeaderRepository();
            var viewModel = repo.Read().Where(x => x.ID == id)
                                       .Include(x => x.Items)
                                       .First();
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _CreateFruitCredit(List<FruitOrder> items, Guid orderHeaderID, string narrative)
        {
            var FruitOrderHeaderRepo = new FruitOrderHeaderRepository();
            var orderHeader = FruitOrderHeaderRepo.Find(orderHeaderID);

            var FruitCreditHeaderRepo = new FruitCreditHeaderRepository();
            var creditHeader = new FruitCreditHeader()
            {
                ID = Guid.NewGuid(),
                CreateByID = CurrentUser.Id,
                Created = DateTime.Now,
                Narrative = narrative,
                OrderHeaderID = orderHeaderID,
                CompanyID = orderHeader.CompanyID,
                SupplierID = orderHeader.SupplierID
            };
            FruitCreditHeaderRepo.Create(creditHeader);

            var orderItemRepo = new FruitOrderRepository();
            var creditItemRepo = new FruitCreditItemRepository();
            var fruitPdtService = new FruitProductService(new SnapDbContext());
            foreach (var item in items)
            {
                var oriItem = orderItemRepo.Find(item.ID);

                var creditItem = new FruitCreditItem()
                {
                    FruitCreditHeaderID = creditHeader.ID,
                    FruitOrderItemID = oriItem.ID,
                    Monday = item.Monday - oriItem.Monday,
                    Tuesday = item.Tuesday - oriItem.Tuesday,
                    Wednesday = item.Wednesday - oriItem.Wednesday,
                    Thursday = item.Thursday - oriItem.Thursday,
                    Friday = item.Friday - oriItem.Friday,
                    Saturday = item.Saturday - oriItem.Saturday
                };

                if (creditItem.Monday == 0 && creditItem.Tuesday == 0 && creditItem.Wednesday == 0 && creditItem.Thursday == 0 && creditItem.Friday == 0 && creditItem.Saturday == 0)
                {
                    continue;
                }

                creditItemRepo.Create(creditItem);

                var subtotalResult = fruitPdtService.CalculateSubtotal(oriItem.FruitProductID, oriItem.OrderHeader.CompanyID, item.Monday, item.Tuesday, item.Wednesday, item.Thursday, item.Friday, item.Saturday);
                oriItem.Monday = item.Monday;
                oriItem.Tuesday = item.Tuesday;
                oriItem.Wednesday = item.Wednesday;
                oriItem.Thursday = item.Thursday;
                oriItem.Friday = item.Friday;
                oriItem.Saturday = item.Saturday;
                oriItem.Price = subtotalResult.UnitPrice;
                oriItem.TotalNet = subtotalResult.Subtotal;
                oriItem.TotalVAT = oriItem.TotalNet * oriItem.VATRate;
                orderItemRepo.Update(oriItem);

            }

            // Calculate header
            orderHeader = FruitOrderHeaderRepo.Read().Where(x => x.ID == orderHeaderID).Include(x => x.Items).FirstOrDefault();
            orderHeader.CalculateTotal();
            FruitOrderHeaderRepo.Update(orderHeader);

            return Json(new { Success = true });
        }

        [HttpGet]
        public ActionResult _CreateDryGoodsCredit(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var viewModel = repo.Read().Where(x => x.ID == id)
                                       .Include(x => x.OneOffOrderItems)
                                       .First();
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _CreateDryGoodsCredit(List<OneOffOrderItem> items, Guid orderHeaderID, string narrative)
        {
            var oneOffOrderRepo = new OneOffOrderRepository();
            var orderHeader = oneOffOrderRepo.Find(orderHeaderID);

            var dryGoodsCreditHeaderRepo = new DryGoodsCreditHeaderRepository();
            var creditHeader = new DryGoodsCreditHeader()
            {
                ID = Guid.NewGuid(),
                CreateByID = CurrentUser.Id,
                Created = DateTime.Now,
                Narrative = narrative,
                OrderHeaderID = orderHeaderID,
                CompanyID = orderHeader.CompanyID,
                //SupplierID = orderHeader.SupplierID
            };
            dryGoodsCreditHeaderRepo.Create(creditHeader);

            var orderItemRepo = new OneOffOrderItemRepository();
            var creditItemRepo = new DryGoodsCreditItemRepository();
            foreach (var item in items)
            {
                var oriItem = orderItemRepo.Find(item.ID);

                var creditItem = new DryGoodsCreditItem()
                {
                    DryGoodsCreditHeaderID = creditHeader.ID,
                    DryGoodsOrderItemID = oriItem.ID,
                    Qty = item.Qty - oriItem.Qty
                };

                if (creditItem.Qty == 0)
                {
                    continue;
                }

                creditItemRepo.Create(creditItem);

                oriItem.Qty = item.Qty;

                oriItem.CalculateTotal();
                orderItemRepo.Update(oriItem);

            }

            // Calculate header
            orderHeader = oneOffOrderRepo.Read().Where(x => x.ID == orderHeaderID).Include(x => x.OneOffOrderItems).FirstOrDefault();
            orderHeader.CalculateTotal();
            oneOffOrderRepo.Update(orderHeader);

            return Json(new { Success = true });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SendFruitIssueMail(string subject, string message, Guid orderID, Guid? referenceID)
        {
            var fruitOrderHeaderRepo = new FruitOrderHeaderRepository();
            var orderHeader = fruitOrderHeaderRepo.Find(orderID);

            var attachments = new EmailAttachmentRepository().Read().Where(x => x.ReferenceID == referenceID).ToList();
            
            var mailMessage = FruitOrderIssueEmailHelper.GetEmail(orderHeader.Supplier, subject, message, attachments);
            var success = EmailService.SendEmail(mailMessage, Server);

            var result = success
                ? $"Successfully sent email to {string.Join(", ", mailMessage.To.Select(x => x.Address))}"
                : $"Failed to send email";

            if (success)
            {
                var issueEmailRepo = new IssueEmailRepository();
                var issueEmail = new IssueEmail()
                {
                    ID = Guid.NewGuid(),
                    CompanyID = orderHeader.Company.ID,
                    SupplierID = orderHeader.Supplier.ID,
                    Subject = subject,
                    Message = message,
                    UserName = CurrentUser.ToFullName()
                };
                issueEmailRepo.Create(issueEmail);
            }
            
            return Content(result);
        }

        public ActionResult _DryGoodsOrderHeader(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var mapper = new OneOffOrderMapper();
            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return PartialView(viewModel);
        }
        
        public ActionResult _ProformaOrderHeader(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var mapper = new ProformaOrderMapper();
            var viewModel = mapper.MapToViewModel(repo.Find(id));
            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult _EditOneOffOrder(Guid id)
        {
            var repo = new OneOffOrderRepository();
            var mapper = new OneOffOrderMapper();
            var viewModel = mapper.MapToViewModel(repo.Find(id));

            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult _AddProducts(Guid orderID)
        {
            var repo = new OneOffOrderRepository();
            var mapper = new OneOffOrderMapper();
            var viewModel = mapper.MapToViewModel(repo.Find(orderID));
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult AddProductToOrder(Guid orderID, Guid productID, int qty)
        {
            var orderRepo = new OneOffOrderRepository();
            var orderHeader = orderRepo.Find(orderID);

            var orderItemRepo = new OneOffOrderItemRepository();
            var item = orderItemRepo.Read()
                                    .Where(x => x.OrderHeaderID == orderHeader.ID)
                                    .Where(x => x.ProductID == productID)
                                    .FirstOrDefault();
            if (item != null)
            {
                // Already exist

                // update order item
                item.Qty += qty;
                item.Total += item.ProductPrice * qty;
                orderItemRepo.Update(item);

                // update order header
                var controller = new OneOffOrderItemGridController();
                controller.UpdateOrderTotal(orderID);
            }
            else
            {
                // Create new one
                var productRepo = new ProductRepository();
                var productMapper = new ProductMapper();
                var product = productMapper.MapToViewModel(productRepo.Find(productID), orderHeader.Company);

                var newOrderItem = new OneOffOrderItem()
                {
                    CompanyID = orderHeader.CompanyID,
                    ImageURL = product.ImageURL,
                    OrderHeaderID = orderHeader.ID,
                    ProductCode = product.ProductCode,
                    ProductID = Guid.Parse(product.ID),
                    ProductName = product.ProductName,
                    ProductPrice = product.Price,
                    ProductVAT = product.VAT,
                    Qty = qty,
                    Total = qty * product.Price
                };
                new OneOffOrderItemRepository().Create(newOrderItem);

                // update order header
                var controller = new OneOffOrderItemGridController();
                controller.UpdateOrderTotal(orderID);
            }

            return Json(new { Status = "Success" });
        }

        [HttpPost]
        public ActionResult CreateCreditOrder(Guid orderID)
        {
            var orderItemRepo = new OneOffOrderItemRepository();

            // Get the order
            var orderHeader = _repo.Find(orderID);
            var orderItems = orderItemRepo.Read()
                                          .Where(x => x.OrderHeaderID == orderHeader.ID)
                                          .ToList();

            // Create the credit order
            var creditOrderHeader = new OneOffOrder()
            {
                BillingAddress = orderHeader.BillingAddress,
                BillingCounty = orderHeader.BillingCounty,
                BillingPostCode = orderHeader.BillingPostCode,
                CompanyID = orderHeader.CompanyID,
                CompanyName = orderHeader.CompanyName,
                Created = DateTime.Now,
                DeliveryAddress = orderHeader.DeliveryAddress,
                DeliveryCounty = orderHeader.DeliveryCounty,
                DeliveryPostCode = orderHeader.DeliveryPostCode,
                EmailAddress = orderHeader.EmailAddress,
                LastUpdated = DateTime.Now,
                OrderNotes = "Credit order of Order No: " + orderHeader.OrderNumber,
                Phone = orderHeader.Phone,
                Status = OneOffOrderStatus.CreditOrder,
                TotalNet = -orderHeader.TotalNet
            };
            _repo.Create(creditOrderHeader);

            foreach (var item in orderItems)
            {
                var creditOrderItem = new OneOffOrderItem()
                {
                    CompanyID = item.CompanyID,
                    ImageURL = item.ImageURL,
                    OrderHeaderID = creditOrderHeader.ID,
                    ProductCode = item.ProductCode,
                    ProductID = item.ProductID,
                    ProductName = item.ProductName,
                    ProductPrice = item.ProductPrice,
                    Qty = -item.Qty,
                    Total = -item.Total
                };
                orderItemRepo.Create(creditOrderItem);
            }

            return Json(new { Status = true });
        }

        [HttpPost]
        public ActionResult CreateFruitCreditOrder(Guid orderID)
        {
            var orderHeaderRepo = new FruitOrderHeaderRepository();
            var orderItemRepo = new FruitOrderRepository();

            // Get the order
            var orderHeader = orderHeaderRepo.Read().Where(x => x.ID == orderID)
                                                    .Include(x => x.Items)
                                                    .FirstOrDefault();

            // Create the credit order
            var creditOrder = new FruitOrderHeader()
            {
                ID = Guid.NewGuid(),
                CompanyID = orderHeader.CompanyID,
                OrderMode = OrderMode.Credit,
                FromDate = orderHeader.FromDate,
                ToDate = orderHeader.ToDate,
                OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                SupplierID = orderHeader.SupplierID,
                TotalNet = -orderHeader.TotalNet,
                TotalVAT = -orderHeader.TotalVAT
            };

            var creditItemsToAdd = new List<FruitOrder>();
            foreach (var item in orderHeader.Items)
            {
                var creditOrderItem = new FruitOrder()
                {
                    ID = Guid.NewGuid(),
                    OrderHeaderID = creditOrder.ID,
                    FruitProductID = item.FruitProductID,
                    ProductName = item.ProductName,
                    Price = item.Price,
                    Unit = item.Unit,
                    VATRate = item.VATRate,
                    Monday = -item.Monday,
                    Tuesday = -item.Tuesday,
                    Wednesday = -item.Wednesday,
                    Thursday = -item.Thursday,
                    Friday = -item.Friday,
                    Saturday = -item.Saturday,
                    Sunday = -item.Sunday,
                    TotalNet = -item.TotalNet,
                    TotalVAT = -item.TotalVAT,
                };
                creditItemsToAdd.Add(creditOrderItem);
            }

            orderHeaderRepo.Create(creditOrder);
            orderItemRepo.Create(creditItemsToAdd);

            return Json(new { Success = true });
        }

        [HttpPost]
        public ActionResult CreateMilkCreditOrder(Guid orderID)
        {
            var orderHeaderRepo = new MilkOrderHeaderRepository();
            var orderItemRepo = new MilkOrderRepository();

            // Get the order
            var orderHeader = orderHeaderRepo.Read().Where(x => x.ID == orderID)
                                                    .Include(x => x.Items)
                                                    .FirstOrDefault();

            // Create the credit order
            var creditOrder = new MilkOrderHeader()
            {
                ID = Guid.NewGuid(),
                CompanyID = orderHeader.CompanyID,
                OrderMode = OrderMode.Credit,
                FromDate = orderHeader.FromDate,
                ToDate = orderHeader.ToDate,
                OrderNumber = String.Format("F{0}", DateTime.Now.ToString("yyddMMHHmmssfff")),
                SupplierID = orderHeader.SupplierID,
                TotalNet = -orderHeader.TotalNet,
                TotalVAT = -orderHeader.TotalVAT
            };

            var creditItemsToAdd = new List<MilkOrder>();
            foreach (var item in orderHeader.Items)
            {
                var creditOrderItem = new MilkOrder()
                {
                    ID = Guid.NewGuid(),
                    OrderHeaderID = creditOrder.ID,
                    MilkProductID = item.MilkProductID,
                    ProductName = item.ProductName,
                    Price = item.Price,
                    Unit = item.Unit,
                    VATRate = item.VATRate,
                    Monday = -item.Monday,
                    Tuesday = -item.Tuesday,
                    Wednesday = -item.Wednesday,
                    Thursday = -item.Thursday,
                    Friday = -item.Friday,
                    Saturday = -item.Saturday,
                    Sunday = -item.Sunday,
                    TotalNet = -item.TotalNet,
                    TotalVAT = -item.TotalVAT,
                };
                creditItemsToAdd.Add(creditOrderItem);
            }

            orderHeaderRepo.Create(creditOrder);
            orderItemRepo.Create(creditItemsToAdd);

            return Json(new { Success = true });
        }

        public ActionResult ExportMilkStandingCSV(string companyID)
        {
            using (var context = new SnapDbContext())
            {
                var viewModels = context.MilkOrderHeaders.Where(x => x.Company.DeleteData == false)
                                                         .Where(x => x.OrderMode == OrderMode.Regular)
                                                         .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
                                                         .Include(x => x.Items)
                                                         .Include(x => x.Company)
                                                         .OrderBy(x => x.Company.Name)
                                                         .ToList();


                StringBuilder builder = new StringBuilder();
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"\n",
                        "Supplier", "Account Number", "Company Name", "Product (Code)", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"
                    ));

                // Append to StringBuilder.
                foreach (var q in viewModels)
                {
                    var today = DateTime.Today;
                    var companySupplier = context.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                   .Where(x => x.CompanyID == q.Company.ID)
                                   .Where(x => x.StartDate <= today && (x.EndDate == null || x.EndDate >= today))
                                   .FirstOrDefault();


                    builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",",
                            companySupplier != null ? companySupplier.Supplier.Name : "",
                            companySupplier != null ? companySupplier.AccountNo : "",
                            q.Company.Name
                            ));

                    int index = 0;
                    foreach (var item in q.Items.OrderBy(x => x.ProductName))
                    {
                        if (index++ == 0)
                        {
                            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                item.ProductName + "(" + item.Milk.ProductCode + ")",
                                item.Monday,
                                item.Tuesday,
                                item.Wednesday,
                                item.Thursday,
                                item.Friday,
                                item.Saturday
                            ));
                        }
                        else
                        {
                            builder.Append(string.Format("\"\",\"\",\"\",\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                item.ProductName + "(" + item.Milk.ProductCode + ")",
                                item.Monday,
                                item.Tuesday,
                                item.Wednesday,
                                item.Thursday,
                                item.Friday,
                                item.Saturday
                            ));
                        }
                    }
                }

                var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
                var contentType = "text/csv";
                var fileDownloadName = string.Format("Milk Standing Order {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

                return File(fileContent, contentType, fileDownloadName);
            }
        }

        public ActionResult ExportMilkOneOffCSV(string companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.StartOfWeek(DayOfWeek.Monday).AddDays(6);
                    fromDate =  DateTime.Today.StartOfWeek(DayOfWeek.Monday);
                    break;
            }

            using (var context = new SnapDbContext())
            {
                var viewModels = context.MilkOrderHeaders.Where(x => x.Company.DeleteData == false)
                                                         .Where(x => x.OrderMode == OrderMode.OneOff)
                                                         .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
                                                         .Where(x => x.FromDate >= fromDate && x.ToDate <= toDate)
                                                         .Include(x => x.Items)
                                                         .Include(x => x.Company)
                                                         .OrderBy(x => x.Company.Name)
                                                         .ToList();


                StringBuilder builder = new StringBuilder();


                builder.Append("\"Supplier\",\"Account Number\",\"Company Name\",\"Product (Code)\"");
                for (var i = 0; i < (toDate.Value - fromDate.Value).TotalDays; i++)
                {
                    builder.Append(string.Format(",\"{0}\"", fromDate.Value.AddDays(i).ToString("dd-MMM")));
                }
                builder.Append("\n");

                // Append to StringBuilder.
                foreach (var q in viewModels)
                {
                    var regularOrderItems = context.MilkOrderHeaders.Where(x => x.CompanyID == q.CompanyID)
                                                                    .Where(x => x.OrderMode == OrderMode.Regular)
                                                                    .Include(x => x.Items)
                                                                    .FirstOrDefault()
                                                                    .Items
                                                                    .ToList();
                    var oneoffOrderItems = q.Items.ToList();
                    foreach (var regItem in regularOrderItems)
                    {
                        if (!oneoffOrderItems.Any(x => x.MilkProductID == regItem.MilkProductID))
                        {
                            oneoffOrderItems.Add(new MilkOrder()
                            {
                                MilkProductID = regItem.MilkProductID,
                                Monday = 0,
                                Tuesday = 0,
                                Wednesday = 0,
                                Thursday = 0,
                                Friday = 0,
                                Saturday = 0
                            });
                        }
                    }
                    foreach (var oneoffItem in oneoffOrderItems)
                    {
                        if (!regularOrderItems.Any(x => x.MilkProductID == oneoffItem.MilkProductID))
                        {
                            regularOrderItems.Add(new MilkOrder()
                            {
                                MilkProductID = oneoffItem.MilkProductID,
                                Monday = 0,
                                Tuesday = 0,
                                Wednesday = 0,
                                Thursday = 0,
                                Friday = 0,
                                Saturday = 0
                            });
                        }
                    }

                    var companySupplier = context.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Milk)
                                   .Where(x => x.CompanyID == q.Company.ID)
                                   .Where(x => x.StartDate <= fromDate.Value && (x.EndDate == null || x.EndDate >= fromDate.Value))
                                   .FirstOrDefault();


                    builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",",
                            companySupplier != null ? companySupplier.Supplier.Name : "",
                            companySupplier != null ? companySupplier.AccountNo : "",
                            q.Company.Name
                            ));

                    int index = 0;
                    foreach (var item in oneoffOrderItems.OrderBy(x => x.ProductName))
                    {
                        var originItem = regularOrderItems.Where(x => x.MilkProductID == item.MilkProductID).FirstOrDefault();

                        if (item.Monday != originItem.Monday || item.Tuesday != originItem.Tuesday || item.Wednesday != originItem.Wednesday || item.Thursday != originItem.Thursday || item.Friday != originItem.Friday || item.Saturday != item.Saturday)
                        {
                            if (index++ == 0)
                            {
                                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                    item.ProductName + "(" + item.Milk.ProductCode + ")",
                                    item.Monday != originItem.Monday ? item.Monday.ToString() : "",
                                    item.Tuesday != originItem.Tuesday ? item.Tuesday.ToString() : "",
                                    item.Wednesday != originItem.Wednesday ? item.Wednesday.ToString() : "",
                                    item.Thursday != originItem.Thursday ? item.Thursday.ToString() : "",
                                    item.Friday != originItem.Friday ? item.Friday.ToString() : "",
                                    item.Saturday != item.Saturday ? item.Saturday.ToString() : ""
                                ));
                            }
                            else
                            {
                                builder.Append(string.Format("\"\",\"\",\"\",\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                    item.ProductName + "(" + item.Milk.ProductCode + ")",
                                    item.Monday != originItem.Monday ? item.Monday.ToString() : "",
                                    item.Tuesday != originItem.Tuesday ? item.Tuesday.ToString() : "",
                                    item.Wednesday != originItem.Wednesday ? item.Wednesday.ToString() : "",
                                    item.Thursday != originItem.Thursday ? item.Thursday.ToString() : "",
                                    item.Friday != originItem.Friday ? item.Friday.ToString() : "",
                                    item.Saturday != item.Saturday ? item.Saturday.ToString() : ""
                                ));
                            }
                        }
                    }
                }

                var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
                var contentType = "text/csv";
                var fileDownloadName = string.Format("Milk One Off Order Updates {0} ~ {1}.csv", fromDate.Value.ToString("dd-MM-yyyy"), toDate.Value.AddDays(-1).ToString("dd-MM-yyyy"));

                return File(fileContent, contentType, fileDownloadName);
            }
        }

        public ActionResult ExportFruitStandingCSV(string companyID)
        {
            using (var context = new SnapDbContext())
            {
                var viewModels = context.FruitOrderHeaders.Where(x => x.Company.DeleteData == false)
                                                         .Where(x => x.OrderMode == OrderMode.Regular)
                                                         .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
                                                         .Include(x => x.Items)
                                                         .Include(x => x.Company)
                                                         .OrderBy(x => x.Company.Name)
                                                         .ToList();


                StringBuilder builder = new StringBuilder();
                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"\n",
                        "Supplier", "Account Number", "Company Name", "Product (Code)", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"
                    ));

                // Append to StringBuilder.
                foreach (var q in viewModels)
                {
                    var today = DateTime.Today;
                    var companySupplier = context.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Fruits)
                                   .Where(x => x.CompanyID == q.Company.ID)
                                   .Where(x => x.StartDate <= today && (x.EndDate == null || x.EndDate >= today))
                                   .FirstOrDefault();


                    builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",",
                            companySupplier != null ? companySupplier.Supplier.Name : "",
                            companySupplier != null ? companySupplier.AccountNo : "",
                            q.Company.Name
                            ));

                    int index = 0;
                    foreach (var item in q.Items.OrderBy(x => x.ProductName))
                    {
                        if (index++ == 0)
                        {
                            builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                item.ProductName + "(" + item.Fruit.ProductCode + ")",
                                item.Monday,
                                item.Tuesday,
                                item.Wednesday,
                                item.Thursday,
                                item.Friday,
                                item.Saturday
                            ));
                        }
                        else
                        {
                            builder.Append(string.Format("\"\",\"\",\"\",\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                item.ProductName + "(" + item.Fruit.ProductCode + ")",
                                item.Monday,
                                item.Tuesday,
                                item.Wednesday,
                                item.Thursday,
                                item.Friday,
                                item.Saturday
                            ));
                        }
                    }
                }

                var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
                var contentType = "text/csv";
                var fileDownloadName = string.Format("Fruit Standing Order {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

                return File(fileContent, contentType, fileDownloadName);
            }
        }

        public ActionResult ExportFruitOneOffCSV(string companyID, int dateType, DateTime? fromDate, DateTime? toDate)
        {
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.Value.AddMonths(1);
                    break;
                case 3: //Custom Date
                    fromDate = fromDate.Value.Date;
                    toDate = toDate.Value.AddDays(1);
                    break;
                default:
                    toDate = DateTime.Today.StartOfWeek(DayOfWeek.Monday).AddDays(6);
                    fromDate = DateTime.Today.StartOfWeek(DayOfWeek.Monday);
                    break;
            }

            using (var context = new SnapDbContext())
            {
                var viewModels = context.FruitOrderHeaders.Where(x => x.Company.DeleteData == false)
                                                         .Where(x => x.OrderMode == OrderMode.OneOff)
                                                         .Where(x => companyID == null || companyID == "" || x.CompanyID.ToString() == companyID)
                                                         .Where(x => x.FromDate >= fromDate && x.ToDate <= toDate)
                                                         .Include(x => x.Items)
                                                         .Include(x => x.Company)
                                                         .OrderBy(x => x.Company.Name)
                                                         .ToList();


                StringBuilder builder = new StringBuilder();


                builder.Append("\"Supplier\",\"Account Number\",\"Company Name\",\"Product (Code)\"");
                for (var i = 0; i < (toDate.Value - fromDate.Value).TotalDays; i++)
                {
                    builder.Append(string.Format(",\"{0}\"", fromDate.Value.AddDays(i).ToString("dd-MMM")));
                }
                builder.Append("\n");

                // Append to StringBuilder.
                foreach (var q in viewModels)
                {
                    var regularOrderItems = context.FruitOrderHeaders.Where(x => x.CompanyID == q.CompanyID)
                                                                    .Where(x => x.OrderMode == OrderMode.Regular)
                                                                    .Include(x => x.Items)
                                                                    .FirstOrDefault()
                                                                    .Items
                                                                    .ToList();
                    var oneoffOrderItems = q.Items.ToList();
                    foreach (var regItem in regularOrderItems)
                    {
                        if (!oneoffOrderItems.Any(x => x.FruitProductID == regItem.FruitProductID))
                        {
                            oneoffOrderItems.Add(new FruitOrder()
                            {
                                FruitProductID = regItem.FruitProductID,
                                Monday = 0,
                                Tuesday = 0,
                                Wednesday = 0,
                                Thursday = 0,
                                Friday = 0,
                                Saturday = 0
                            });
                        }
                    }
                    foreach (var oneoffItem in oneoffOrderItems)
                    {
                        if (!regularOrderItems.Any(x => x.FruitProductID == oneoffItem.FruitProductID))
                        {
                            regularOrderItems.Add(new FruitOrder()
                            {
                                FruitProductID = oneoffItem.FruitProductID,
                                Monday = 0,
                                Tuesday = 0,
                                Wednesday = 0,
                                Thursday = 0,
                                Friday = 0,
                                Saturday = 0
                            });
                        }
                    }

                    var companySupplier = context.CompanySuppliers.Where(x => x.Supplier.SupplierType == SupplierType.Fruits)
                                   .Where(x => x.CompanyID == q.Company.ID)
                                   .Where(x => x.StartDate <= fromDate.Value && (x.EndDate == null || x.EndDate >= fromDate.Value))
                                   .FirstOrDefault();


                    builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",",
                            companySupplier != null ? companySupplier.Supplier.Name : "",
                            companySupplier != null ? companySupplier.AccountNo : "",
                            q.Company.Name
                            ));

                    int index = 0;
                    foreach (var item in oneoffOrderItems.OrderBy(x => x.ProductName))
                    {
                        var originItem = regularOrderItems.Where(x => x.FruitProductID == item.FruitProductID).FirstOrDefault();

                        if (item.Monday != originItem.Monday || item.Tuesday != originItem.Tuesday || item.Wednesday != originItem.Wednesday || item.Thursday != originItem.Thursday || item.Friday != originItem.Friday || item.Saturday != item.Saturday)
                        {
                            if (index++ == 0)
                            {
                                builder.Append(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                    item.ProductName + "(" + item.Fruit.ProductCode + ")",
                                    item.Monday != originItem.Monday ? item.Monday.ToString() : "",
                                    item.Tuesday != originItem.Tuesday ? item.Tuesday.ToString() : "",
                                    item.Wednesday != originItem.Wednesday ? item.Wednesday.ToString() : "",
                                    item.Thursday != originItem.Thursday ? item.Thursday.ToString() : "",
                                    item.Friday != originItem.Friday ? item.Friday.ToString() : "",
                                    item.Saturday != item.Saturday ? item.Saturday.ToString() : ""
                                ));
                            }
                            else
                            {
                                builder.Append(string.Format("\"\",\"\",\"\",\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\n",
                                    item.ProductName + "(" + item.Fruit.ProductCode + ")",
                                    item.Monday != originItem.Monday ? item.Monday.ToString() : "",
                                    item.Tuesday != originItem.Tuesday ? item.Tuesday.ToString() : "",
                                    item.Wednesday != originItem.Wednesday ? item.Wednesday.ToString() : "",
                                    item.Thursday != originItem.Thursday ? item.Thursday.ToString() : "",
                                    item.Friday != originItem.Friday ? item.Friday.ToString() : "",
                                    item.Saturday != item.Saturday ? item.Saturday.ToString() : ""
                                ));
                            }
                        }
                    }
                }

                var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
                var contentType = "text/csv";
                var fileDownloadName = string.Format("Fruit One Off Order Updates {0} ~ {1}.csv", fromDate.Value.ToString("dd-MM-yyyy"), toDate.Value.AddDays(-1).ToString("dd-MM-yyyy"));

                return File(fileContent, contentType, fileDownloadName);
            }
        }

        public ActionResult _AddMilkProductPage(Guid milkOrderHeaderID)
        {
            var repo = new MilkOrderHeaderRepository();
            var orderHeader = repo.Find(milkOrderHeaderID);
            var viewModel = new AddMilkProductPageViewModel()
            {
                CompanyID = orderHeader.CompanyID.Value,
                OrderHeaderID = orderHeader.ID,
            };
            return PartialView(viewModel);
        }

        public ActionResult _AddMilkProduct(Guid orderID, Guid productID)
        {
            try
            { 
                var orderService = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), new SnapDbContext());
                var order = orderService.GetOrderByID(orderID);

                if (order.Items.Any(i => i.MilkProductID == productID))
                {
                    throw new Exception("The product is already in the order.");
                }

                var product = new MilkProductRepository(new SnapDbContext()).Find(productID);
                product.GetPrice(order.Company);

                orderService.AddItem(orderID, product);
                orderService.CalculateTotal(orderID);

                return OkResult();
            }
            catch(Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        public ActionResult _AddFruitProductPage(Guid fruitOrderHeaderID)
        {
            var repo = new FruitOrderHeaderRepository();
            var orderHeader = repo.Find(fruitOrderHeaderID);
            var viewModel = new AddFruitProductPageViewModel()
            {
                CompanyID = orderHeader.CompanyID.Value,
                OrderHeaderID = orderHeader.ID,
            };
            return PartialView(viewModel);
        }

        public ActionResult _AddFruitProduct(Guid orderID, Guid productID)
        {
            try
            {
                var dbContext = new SnapDbContext();
                var orderService = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), dbContext);
                var order = orderService.GetOrderByID(orderID);

                if (order.Items.Any(i => i.FruitProductID == productID))
                {
                    throw new Exception("The product is already in the order.");
                }

                var product = new FruitProductRepository(dbContext).Find(productID);

                orderService.AddItem(orderID, product, order.CompanyID);
                orderService.CalculateTotal(orderID);

                return OkResult();
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

    }
}