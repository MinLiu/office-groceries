﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class ProductGridController : GridController<Product, ProductGridViewModel>
    {

        public ProductGridController()
            : base(new ProductRepository(), new ProductGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? categoryID, Guid? aisleID, Guid? supplierID, string filterText, bool showInactive = false)
        {
            filterText = filterText.ToLower();
            var models = _repo.Read()
                              .Where(x => x.Deleted == false)
                              .Where(x => supplierID == null || x.SupplierID == supplierID)
                              .Where(x => aisleID == null || x.DryGoodsAisles.Select(a => a.AisleID).Contains(aisleID.Value))
                              .Where(x => categoryID == null || x.DryGoodsCategories.Select(c => c.CategoryID).Contains(categoryID.Value))
                              .Where(x => filterText == "" || x.ProductCode.ToLower().Contains(filterText) || x.Name.ToLower().Contains(filterText) || x.ShortDescription.ToLower().Contains(filterText));
            
            if (!showInactive)
            {
                models = models.Where(x => x.IsActive == true);
            }

            if (categoryID != null)
            {
                models = models.OrderBy(x => x.DryGoodsCategories
                                                .Where(dc => dc.Category.AisleID == aisleID)
                                                .Where(dc => dc.CategoryID == categoryID)
                                                .OrderBy(dc => dc.Category.SortPos)
                                                .FirstOrDefault()
                                                .Category
                                                .SortPos)
                                 .ThenBy(x => x.DryGoodsCategories
                                               .Where(dc => dc.Category.AisleID == aisleID)
                                               .Where(dc => dc.CategoryID == categoryID)
                                               .OrderBy(dc => dc.Category.SortPos)
                                               .FirstOrDefault()
                                               .SortPos);
            }
            else
            {
                models = models.OrderBy(x => x.DryGoodsCategories
                                                .Where(dc => dc.Category.AisleID == aisleID)
                                                .OrderBy(dc => dc.Category.SortPos)
                                                .FirstOrDefault()
                                                .Category
                                                .SortPos)
                                 .ThenBy(x => x.DryGoodsCategories
                                               .Where(dc => dc.Category.AisleID == aisleID)
                                               .OrderBy(dc => dc.Category.SortPos)
                                               .FirstOrDefault()
                                               .SortPos);
            }

            var viewModels = models.Skip((request.Page - 1) * request.PageSize)
                                   .Take(request.PageSize)
                                   .ToList()
                                   .Select(s => _mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = models.Count()
            };

            return Json(result);
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, ProductGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _repo.Find(Guid.Parse(viewModel.ID));
                _repo.Delete(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult _UpdateProductPositions(Guid productID, int oldIndex, int newindex, Guid supplierID, Guid aisleID, Guid categoryID)
        {
            //var model = _repo.Find(productID);

            //var products = _repo.Read()
            //                    .Where(x => x.Deleted == false)
            //                    .Where(x => x.SupplierID == model.SupplierID)
            //                    .Where(x => x.DryGoodsAisles.Any(a => a.AisleID == aisleID))
            //                    .Where(x => x.DryGoodsCategories.Any(c => c.CategoryID == categoryID))
            //                    .OrderBy(x => x.SortPosition)
            //                    .ThenBy(x => x.Name)
            //                    .ToList();

            var repo = new DryGoodsCategoryRepository();
            var model = repo.Read().Where(x => x.ProductID == productID).Where(x => x.CategoryID == categoryID).FirstOrDefault();
            var dryGoodsCategories = repo.Read().Where(x => x.CategoryID == categoryID).OrderBy(x => x.SortPos).ThenBy(x => x.Product.Name).ToList();

            dryGoodsCategories.Remove(model);

            var top = newindex - 1 >= 0 ? dryGoodsCategories.ElementAt(newindex - 1) : null;
            var bottom = newindex < dryGoodsCategories.Count ? dryGoodsCategories.ElementAt(newindex) : null;

            dryGoodsCategories.Insert(newindex >= 0 ? newindex : 0, model);

            int sortPos = 0;
            foreach (var item in dryGoodsCategories)
            {
                item.SortPos = sortPos++;
            }

            repo.Update(dryGoodsCategories);

            return Json("Success");
        }
    }
}