﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class DryGoodsCreditItemsGridController : GridController<DryGoodsCreditItem, DryGoodsCreditItemViewModel>
    {
        public DryGoodsCreditItemsGridController()
            : base(new DryGoodsCreditItemRepository(), new DryGoodsCreditItemMapper())
        {

        }

        [Authorize(Roles = "NI-User")]
        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid orderHeaderID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => x.DryGoodsCreditHeader.OrderHeaderID == orderHeaderID)
                                  .ToList()
                                  .OrderByDescending(x => x.DryGoodsCreditHeader.Created).ThenBy(x => x.DryGoodsOrderItem.ProductName)
                                  .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));


        }
    }

}