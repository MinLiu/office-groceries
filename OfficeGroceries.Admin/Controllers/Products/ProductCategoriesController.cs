﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;

namespace NuIngredient.Controllers
{
    public class ProductCategoriesController : EntityController<ProductCategory, ProductCategoryViewModel>
    {

        public ProductCategoriesController()
            : base(new ProductCategoryRepository(), new ProductCategoryMapper())
        {

        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult DropDownAllCategories(Guid? aisleID)
        {
            var viewModels = _repo.Read()
                                  .Where(x => aisleID == null || x.AisleID == aisleID.Value)
                                  .Select(x => new SelectItemViewModel
                                  {
                                      ID = x.ID.ToString(),
                                      Name = x.Name
                                  })
                                  .OrderBy(x => x.Name)
                                  .ToList();

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TreeViewProductCategories(Guid? aisleID, Guid? id)
        {
            var categories = _repo.Read()
                                  .Where(x => aisleID == null || x.AisleID == aisleID)
                                  .Where(x => x.ParentID == id)
                                  .OrderBy(x => x.SortPos).ThenBy(x => x.Name)
                                  .Select(x => new
                                  {
                                      id = x.ID,
                                      Name = x.Name,
                                      hasChildren = x.Children.Any(),
                                  })
                                  .ToList();
            if (!id.HasValue)
            {
                categories.Insert(0, new { id = Guid.NewGuid(), Name = "All", hasChildren = false });
            }
            return Json(categories, JsonRequestBehavior.AllowGet);
        }
    }
}