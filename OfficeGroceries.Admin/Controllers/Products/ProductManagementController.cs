﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class ProductManagementController : EntityController<Product, EditProductViewModel>
    {

        public ProductManagementController()
            : base(new ProductRepository(), new EditProductModelMapper())
        {

        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult Keywords()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _NewProduct()
        {
            var model = new Product();
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _NewProduct(EditProductViewModel viewModel)
        {
            var pdtService = new ProductService<ProductViewModel>(new ProductMapper());
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            if (!pdtService.IsUniquePdtCode(viewModel.ProductCode))
            {
                ModelState.AddModelError("ProductCode", "*Duplicate Product Code");
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            viewModel.ID = model.ID.ToString();

            UpdateDryGoodsCategories(viewModel);
            UpdateDryGoodsAisles(viewModel);
            UpdateInternalName(model);

            return Json(new { ID = model.ID });
        }

        [HttpGet]
        public ActionResult _EditProduct(Guid id)
        {
            var viewModel = _mapper.MapToViewModel(_repo.Find(id));
            return PartialView(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult _EditProduct(EditProductViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            UpdateDryGoodsCategories(viewModel);
            UpdateDryGoodsAisles(viewModel);
            UpdateInternalName(model);

            //viewModel = _mapper.MapToViewModel(model);
            viewModel.ImageURL = model.ImageURL;

            ViewBag.Saved = true;

            return PartialView(viewModel);
        }

        public ActionResult UploadProductImage(HttpPostedFileBase uploadedProductImage, string id)
        {
            const string relativePath = "/Images/";

            if (uploadedProductImage == null) return Json(new { status = "error", message = "No Image uploaded." }, "text/plain");
            if (!uploadedProductImage.ContentType.Contains("image")) return Json(new { status = "error", message = "Uploaded file is not an Image." }, "text/plain");
            if (uploadedProductImage.ContentLength > 10 * 1024 * 1024) return Json(new { status = "error", message = "Uploaded file is too big." }, "text/plain");


            var model = _repo.Find(Guid.Parse(id));

            //Save file on Server
            var result = SaveImg(uploadedProductImage, relativePath);

            //Update the image URL of the product    
            model.ImageURL = result.Path;
            model.ThumbnailImageURL = result.ThumbnailPath;
            _repo.Update(model, new string[] { "ImageURL", "ThumbnailImageURL" });

            //return the image loc. Javascript updates the image in view
            return Json(new { status = "success", id = id }, "text/plain");

        }

        //public ActionResult CreateThum()
        //{
        //    try
        //    {
        //        var count = _repo.Read().Count();
        //        for(var i = 0; i < count; i = i + 100)
        //        {
        //            var products = _repo.Read().OrderBy(x => x.ProductCode).Skip(i).Take(100);
        //            foreach(var product in products)
        //            {
        //                if (!string.IsNullOrEmpty(product.ImageURL))
        //                {
        //                    var thumbImgPath = CreateThumb(product.ImageURL);
        //                    product.ThumbnailImageURL = thumbImgPath;
        //                }
        //            }
        //            _repo.Update(products);
        //        }
        //        return Content("Success");
        //    }
        //    catch(Exception e)
        //    {
        //        return Content(e.Message);
        //    }
        //}

        public ActionResult RemoveProductImage(string id)
        {
            var model = _repo.Find(Guid.Parse(id));
            model.ImageURL = null;
            model.ThumbnailImageURL = null;
            _repo.Update(model, new string[] { "ImageURL", "ThumbnailImageURL" });

            return Json(new { status = "success", id = id }, "text/plain");
        }

        public ActionResult ExportToCSV()
        {
            var viewModels = _repo.Read()
                .Where(x => x.Deleted == false)
                .Select(x => new
                {
                    SupplierName = x.Supplier.Name,
                    ProductCode = x.ProductCode,
                    Name = x.Name,
                    Unit = x.Unit,
                    Price = x.Price,
                    VAT = x.VAT,
                    Cost = x.Cost,
                    Category = x.DryGoodsCategories.Select(c => c.Category.Name).FirstOrDefault()
                })
                .OrderBy(x => x.SupplierName)
                .ThenBy(x => x.Category)
                .ThenBy(x => x.ProductCode)
                .ToList();

            StringBuilder builder = new StringBuilder();
            builder.Append(
                "\"Supplier\"," +
                "\"Product Code\"," +
                "\"Product Name\"," +
                "\"Unit\"," +
                "\"Net Price\"," +
                "\"VAT\"," +
                "\"Retail Price\"," +
                "\"Cost\"," +
                "\"GP%\"," +
                "\"Category\"" +
                "\n");

            // Append to StringBuilder.
            foreach (var q in viewModels)
            {
                builder.Append(
                    $"\"{q.SupplierName ?? ""}\",\"" +
                    $"{q.ProductCode}\"," +
                    $"\"{q.Name}\"," +
                    $"\"{q.Unit}\"," +
                    $"\"{q.Price}\"," +
                    $"\"{q.VAT.ToString("p")}\"," +
                    $"\"{q.Price + q.Price * q.VAT}\"," +
                    $"\"{q.Cost}\"," +
                    $"\"{(q.Price != 0 ? ((q.Price - q.Cost) / q.Price).ToString("p") : "N/A")}\"," +
                    $"\"{q.Category}\"" +
                    $"\n");
            }

            var fileContent = Encoding.ASCII.GetBytes(builder.ToString());
            var contentType = "text/csv";
            var fileDownloadName = string.Format("Product GP% {0}.csv", DateTime.Today.ToString("dd-MM-yyyy"));

            return File(fileContent, contentType, fileDownloadName);
        }

        private void UpdateDryGoodsCategories(EditProductViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.DryGoodsCategories)
                             .FirstOrDefault();

            var oldList = model != null ? model.DryGoodsCategories.Select(x => x.CategoryID.ToString()) : new List<string>();
            var newList = viewModel.DryGoodsCategoryIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new DryGoodsCategoryRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.CategoryID.ToString() == id && x.ProductID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new DryGoodsCategory() { CategoryID = Guid.Parse(id), ProductID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateDryGoodsAisles(EditProductViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.DryGoodsAisles)
                             .FirstOrDefault();

            var oldList = model != null ? model.DryGoodsAisles.Select(x => x.AisleID.ToString()) : new List<string>();
            var newList = viewModel.DryGoodsAisleIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new DryGoodsAisleRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.AisleID.ToString() == id && x.ProductID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new DryGoodsAisle() { AisleID = Guid.Parse(id), ProductID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateInternalName(Product product)
        {
            var internalName = InternalNameHelper.Convert(product.Name);

            var finalInternalName = internalName;
            var postFix = 1;

            while(_repo.Read().Where(p => p.ID != product.ID).Where(p => p.InternalName == finalInternalName).Any())
            {
                finalInternalName = finalInternalName + postFix++;
            }

            if (product.InternalName != finalInternalName)
            {
                product.InternalName = finalInternalName;
                _repo.Update(product, new string[] { "InternalName" });
            }

        }
    }
}