﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class DryGoodProductExcludeCompanyController : BaseController
    {
        [HttpPost]
        public JsonResult Read(string filterText, Guid companyId)
        {
            var drygoods = QueryProducts(filterText)
                .Select(x => new
                {
                    x.ID,
                    x.Name,
                })
                .ToList();
            
            var companyExcludeRepo = new DryGoodsExcludeCompanyRepository();
            var companyExcludes = companyExcludeRepo.Read()
                .Where(x => x.CompanyID == companyId)
                .Select(x => x.DryGoodProductID)
                .ToList();
            
            var result = drygoods.Select(x => new AvailableProductVm
            {
                ProductId = x.ID,
                Name = x.Name,
                Available = !companyExcludes.Contains(x.ID)
            })
            .ToList();
            
            return Json(new DataSourceResult()
            {
                Data = result,
                Total = result.Count()   
            });
        }

        private IQueryable<Product> QueryProducts(string filterText)
        {
            var query = new ProductService<ProductViewModel>(new ProductMapper())
                .Read();

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                query = query.Where(x => x.Name.Contains(filterText));
            }

            return query;
        }

        [HttpPost]
        public JsonResult Update(Guid companyId, Guid productId, bool available)
        {
            var companyExcludeRepo = new DryGoodsExcludeCompanyRepository();
            var entry = companyExcludeRepo.Read()
                .FirstOrDefault(x =>
                    x.DryGoodProductID == productId &&
                    x.CompanyID == companyId);

            if (entry == null && !available)
            {
                companyExcludeRepo.Create(new DryGoodsExcludeCompany
                {
                    DryGoodProductID = productId,
                    CompanyID = companyId
                });
            }
            else if (entry != null && available)
            {
                companyExcludeRepo.Delete(entry);
            }

            return Json(new { success = true });
        }

        [HttpPost]
        public JsonResult IncludeAll(Guid companyId, string filterText)
        {
            var products = QueryProducts(filterText)
                .Select(x => x.ID)
                .ToList();
            
            var companyExcludeRepo = new DryGoodsExcludeCompanyRepository();
            var entries = companyExcludeRepo.Read()
                .Where(x => x.CompanyID == companyId)
                .Where(x => products.Contains(x.DryGoodProductID))
                .ToList();

            companyExcludeRepo.Delete(entries);
            
            return Json(new { success = true });
        }
        
        [HttpPost]
        public JsonResult ExcludeAll(Guid companyId, string filterText)
        {
            var drygoods = QueryProducts(filterText)
                .Select(x => new
                {
                    x.ID,
                    x.Name,
                })
                .ToList();
            
            var companyExcludeRepo = new DryGoodsExcludeCompanyRepository();
            var companyExcludes = companyExcludeRepo.Read()
                .Where(x => x.CompanyID == companyId)
                .Select(x => x.DryGoodProductID)
                .ToList();

            var toExclude = new List<DryGoodsExcludeCompany>();
            foreach (var drygood in drygoods)
            {
                if (!companyExcludes.Contains(drygood.ID))
                {
                    toExclude.Add(new DryGoodsExcludeCompany
                    {
                        DryGoodProductID = drygood.ID,
                        CompanyID = companyId
                    });
                }
            }
            
            companyExcludeRepo.Create(toExclude);
            
            return Json(new { success = true });
        }
        
        // [HttpPost]
        // public JsonResult UpdateAll(Guid companyId, List<ExcludeVm> productVms, bool exclude)
        // {
        //     var companyExcludeRepo = new DryGoodsExcludeCompanyRepository();
        //     var entries = companyExcludeRepo.Read()
        //         .Where(x => x.CompanyID == companyId)
        //         .ToList();
        //
        //     foreach (var productVm in productVms)
        //     {
        //         var entry = entries.FirstOrDefault(x => x.DryGoodProductID == productVm.ProductId);
        //
        //         if (entry == null && productVm.Exclude)
        //         {
        //             companyExcludeRepo.Create(new DryGoodsExcludeCompany
        //             {
        //                 DryGoodProductID = productVm.ProductId,
        //                 CompanyID = companyId
        //             });
        //         }
        //         else if (entry != null && !productVm.Exclude)
        //         {
        //             companyExcludeRepo.Delete(entry);
        //         }
        //     }
        //     
        //     return Json(new { success = true });
        // }

    }
    
    
}