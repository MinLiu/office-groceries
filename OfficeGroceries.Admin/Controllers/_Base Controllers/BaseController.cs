﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using System.Web.Routing;
using Kendo.Mvc.UI;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Data.Entity;
using System;
using System.IO;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using System.Drawing;
using System.Configuration;
using System.Net;

namespace NuIngredient.Controllers
{
    public class BaseController : Controller
    {
        public UserManager UserManager;
        public User CurrentUser;  //This is the current user logged in.
        public string ProspectToken;
        private static readonly string _customerSideRootFolderName = ConfigurationManager.AppSettings["CustomerSideRootFolderName"];
        private static readonly string _adminSideRootFolderName = ConfigurationManager.AppSettings["AdminSideRootFolderName"];

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            try
            {
                SetAisles();
                SetDefaultConfiguration();

                if (!User.Identity.IsAuthenticated)
                {
                    ViewBag.Milk = false;
                    ViewBag.Fruits = false;
                    ViewBag.DryGoods = false;

                    // Save Prospect's token
                    HttpCookie cookie = Request.Cookies["ProspectToken"];
                    if (cookie == null)
                    {
                        cookie = new HttpCookie("ProspectToken");
                        cookie.Value = Guid.NewGuid().ToString();
                    }
                    cookie.Expires = DateTime.Now.AddHours(1);

                    ProspectToken = cookie.Value;
                    Response.Cookies.Add(cookie);

                    return;
                }
                else
                {
                    Response.Cookies["ProspectToken"].Expires = DateTime.Now.AddDays(-1);
                }

                UserManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();

                var userId = User.Identity.GetUserId().ToString();
                var userRepo = new UserRepository();
                CurrentUser = userRepo.Read().Where(u => u.Id == userId).Include(u => u.Company).First();
               
                //Check Login Misuse by looking at the cookies
                //if (CurrentUser.Token != (Request.Cookies["GG"] ?? new HttpCookie("GG")).Value)
                //{
                //    ControllerContext.HttpContext.GetOwinContext().Authentication.SignOut();
                //    Response.Redirect("/Home/LoginMisuse");
                //}

                //Used for navbar settings
                ViewBag.ColSize = 4;
                ViewBag.AccessCount = 0;

                SetCustomerStatus();

            }
            catch {

                ViewBag.ColSize = 4;
                ViewBag.AccessCount = 0;
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }
            }

            base.Dispose(disposing);
        }


        public void CheckSubscription() {

            return;

        }

        protected void SetCustomerStatus()
        {
            ViewBag.Milk = false;
            ViewBag.Fruits = false;
            ViewBag.DryGoods = false;

            if (User.IsInRole("Admin") || User.IsInRole("User"))
            {
                ViewBag.Milk = CurrentUser.Company.Method_HasSupplier(SupplierType.Milk) || CurrentUser.Company.AskingForMilkSupplier != null;
                ViewBag.Fruits = CurrentUser.Company.Method_HasSupplier(SupplierType.Fruits) || CurrentUser.Company.AskingForFruitSupplier != null;
                ViewBag.DryGoods = CurrentUser.Company.Method_HasSupplier(SupplierType.DryGoods) || CurrentUser.Company.AskingForDryGoodsSupplier != null;
                SetShoppingCartStatus();
            }
        }

        protected void SetDefaultConfiguration()
        {
            using (var context = new SnapDbContext())
            { 
                ViewBag.MilkMinimumValue = context.Suppliers.Where(x => x.SupplierType == SupplierType.Milk)
                                                            .Where(x => x.Deleted == false)
                                                            .Select(x => x.MinimumValue)
                                                            .DefaultIfEmpty(0)
                                                            .OrderByDescending(x => x)
                                                            .FirstOrDefault();
                ViewBag.FruitMinimumValue = context.Suppliers.Where(x => x.SupplierType == SupplierType.Fruits)
                                                            .Where(x => x.Deleted == false)
                                                            .Select(x => x.MinimumValue)
                                                            .DefaultIfEmpty(0)
                                                            .OrderByDescending(x => x)
                                                            .FirstOrDefault();
            }
        }

        protected void SetCustomerStatus(string companyID)
        {
            var company = new CompanyRepository().Find(Guid.Parse(companyID));

            ViewBag.Milk = company.Method_HasSupplier(SupplierType.Milk) || company.AskingForMilkSupplier != null;
            ViewBag.Fruits = company.Method_HasSupplier(SupplierType.Fruits) || company.AskingForFruitSupplier != null || company.Method_HasSupplier(SupplierType.Snacks) || company.AskingForSnackSupplier != null;
            ViewBag.DryGoods = company.Method_HasSupplier(SupplierType.DryGoods) || company.AskingForDryGoodsSupplier != null;
        }

        protected void SetAisles()
        {
            var repo = new AisleRepository();
            var mapper = new AisleMapper();
            var aisles = repo.Read().Where(x => x.StartDate <= DateTime.Now)
                                    .Where(x => x.EndDate == null || x.EndDate >= DateTime.Now)
                                    .OrderBy(x => x.SortPosition)
                                    .ToList()
                                    .Select(x => mapper.MapToViewModel(x))
                                    .ToList();
            ViewBag.Aisles = aisles;
        }

        protected void SetShoppingCartStatus()
        {
            var result = GetShoppingCartStatus();
            ViewBag.ItemQty = result.Item1;
            ViewBag.Total = result.Item2;
        }

        /// <summary>
        /// return item qty, total
        /// </summary>
        /// <returns></returns>
        protected Tuple<int, string> GetShoppingCartStatus(Guid? companyID = null)
        {
            if (companyID is null)
            {
                return new Tuple<int, string>(0, 0.ToString("C"));
            }

            var company = new CompanyRepository().Find(companyID.Value);
            var shoppingCartItems = new ShoppingCartItemRepository()
                .Read()
                .Where(x => x.CompanyID == companyID)
                .ToList()
                .Where(x => (x.Product.Supplier.UseMasterAccount && company.EasySuppliersActivated) || company.Method_IsLive(x.Product.Supplier))
                .ToList();

            var qty = shoppingCartItems.Sum(x => x.Qty);

            var total = 0m;
            var productRepo = new ProductRepository();
            foreach (var item in shoppingCartItems)
            {
                var pdt = productRepo.Find(item.ProductID);
                var price = pdt.ExclusivePrice(item.CompanyID);
                var itemTotal = item.Qty * price + item.Qty * price * pdt.VAT;
                total += itemTotal;
            }

            var result = new Tuple<int, string>(qty, total.ToString("C"));
            return result;
        }

        public string SaveFile(HttpPostedFileBase file, string subFolder = "/", string filename = "", string companyID = null)
        {
            if (file == null) return null;

            const string contentFolderRoot = "/Uploads/";
            var virtualPath = string.Format("{0}{1}{2}", contentFolderRoot, companyID ?? CurrentUser.CompanyID.ToString(), subFolder);

            var extension = Path.GetExtension(file.FileName);
            var str = (string.IsNullOrEmpty(filename)) ? Guid.NewGuid().ToString() + extension : filename + extension;

            try
            { 
                // Save to admin side
                var adminPhysicalPath = MapAdminFolderPath(virtualPath);
                var adminPhysicalSavePath = adminPhysicalPath + str;
                file.SaveAs(adminPhysicalSavePath);
            }
            catch { }

            try
            { 
                // Save to customer side
                var customerPhysicalPath = MapCustomerFolderPath(virtualPath);
                var customerPhysicalSavePath = customerPhysicalPath + str;
                file.SaveAs(customerPhysicalSavePath);
            }
            catch { }

            return virtualPath + str; 
        }

        private string MapAdminFolderPath(string virtualPath)
        {
            var physicalPath = Server.MapPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }
            return physicalPath;
        }

        private string MapCustomerFolderPath(string virtualPath)
        {
            var physicalPath = Server.MapPath(virtualPath).Replace(_adminSideRootFolderName, _customerSideRootFolderName);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }
            return physicalPath;
        }

        public bool DeleteFile(string filepath)
        {
            try
            {
                var fileName = Path.GetFileName(filepath);
                var fileFolder = Path.GetDirectoryName(filepath);
                // Admin side
                var adminPhysicalPath = MapAdminFolderPath(fileFolder) + "/" + fileName;
                if (System.IO.File.Exists(adminPhysicalPath)) System.IO.File.Delete(adminPhysicalPath);

                // Customer side
                var customerPhysicalPath = MapCustomerFolderPath(fileFolder) + "/" + fileName;
                if (System.IO.File.Exists(customerPhysicalPath)) System.IO.File.Delete(customerPhysicalPath);

                return true;
            }
            catch { return false; }
        }

        //public bool DoesFileExists(string subFolder = "/", string filename = "")
        //{
        //    const string contentFolderRoot = "/Uploads/";
        //    var virtualPath = string.Format("{0}{1}{2}{3}", contentFolderRoot, CurrentUser.CompanyID.ToString(), subFolder, filename);

        //    return (System.IO.File.Exists(virtualPath));
        //}

        //public string GetUploadVirtualFilePath(string subFolder = "/", string filename = "", string companyID = null)
        //{
        //    const string contentFolderRoot = "/Uploads/";
        //    return string.Format("{0}{1}{2}{3}", contentFolderRoot, companyID ?? CurrentUser.CompanyID.ToString(), subFolder, filename);
        //}

        //public string GetUploadPhysicalFilePath(string subFolder = "/", string filename = "", string companyID = null)
        //{   
        //    return Server.MapPath(GetUploadVirtualFilePath(subFolder, filename, companyID));
        //}

        public ImgLocation SaveImg(HttpPostedFileBase file, string subFolder = "/", string filename = "", string companyID = null)
        {
            var imgLocation = SaveFile(file, subFolder, filename, companyID);
            var thumbImgLocation = Path.GetDirectoryName(imgLocation) + "/" + Path.GetFileNameWithoutExtension(imgLocation) + "-thumb.jpg";

            var physicalSavePath = Server.MapPath(thumbImgLocation);

            Image image = Image.FromStream(file.InputStream);
            var thumbWidth = 300;
            var thumbHeight = (thumbWidth * image.Height) / image.Width;
            Image thumb = image.GetThumbnailImage(thumbWidth, thumbHeight, () => false, IntPtr.Zero);
            thumb.Save(physicalSavePath);

            return new ImgLocation(imgLocation, thumbImgLocation);
        }

        //public string CreateThumb(string filePath)
        //{
        //    var physicalPath = Server.MapPath(filePath);

        //    var thumbImgLocation = Path.GetDirectoryName(filePath) + "/" + Path.GetFileNameWithoutExtension(filePath) + "-thumb.jpg";

        //    var physicalSavePath = Server.MapPath(thumbImgLocation);

        //    Image image = Image.FromFile(physicalPath);
        //    var thumbWidth = 300;
        //    var thumbHeight = (thumbWidth * image.Height) / image.Width;
        //    Image thumb = image.GetThumbnailImage(thumbWidth, thumbHeight, () => false, IntPtr.Zero);
        //    thumb.Save(physicalSavePath);

        //    return thumbImgLocation;
        //}

        public class ImgLocation
        {
            public string Path { get; set; }
            public string ThumbnailPath { get; set; }
            public ImgLocation (string path, string thumbnailPath)
            {
                Path = path;
                ThumbnailPath = thumbnailPath;
            }
        }

        public ActionResult BadRequestResult(object obj)
        {
            Server.ClearError();

            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return Json(obj);

        }

        public ActionResult BadRequestResult(string errorMsg)
        {
            var obj = new
            {
                errorMsg
            };

            return BadRequestResult(obj);

        }

        public ActionResult OkResult()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}