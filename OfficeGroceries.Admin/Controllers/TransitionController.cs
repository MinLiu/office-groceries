﻿using NuIngredient.Models;
using NuIngredient.Models.Services.HtmlSitemapGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Fruitful.Import;
using Microsoft.AspNet.Identity;

namespace NuIngredient.Controllers
{
    public class TransitionController : BaseController
    {
        public async Task<ActionResult> Import(string file)
        {
            var dataTable = ImportService.CopyExcelFileToTable(
                $"D:/WorkSpace/office-groceries/{file}.xlsx",
                false);
            
            var result = await CompleteImport(dataTable);
            
            var strBuilder = new StringBuilder();
            strBuilder.AppendLine("Success,Email,Company,IsMulti,Reason");
            foreach (var row in result)
            {
                strBuilder.AppendLine($"{row.Success},\"{row.Email}\", {row.Company}, {row.IsMulti}, {row.Message}");
            }
            
            var byteArray = Encoding.ASCII.GetBytes(strBuilder.ToString());
            return File(byteArray, "text/csv", $"result-{file}.csv");
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Upload(HttpPostedFileBase file)
        {
            var virtualPath = "/TempFiles/";

            var extension = Path.GetExtension(file.FileName);
            var fileName = $"{Guid.NewGuid()}{extension}";

            var physicalPath = Server.MapPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }
            
            var physicalSavePath = physicalPath + fileName;
            file.SaveAs(physicalSavePath);

            var dataTable = ImportService.CopyExcelFileToTable(physicalSavePath, true);

            var result = await CompleteImport(dataTable);

            return Json(result);

        }
        
        private async Task<List<UploadResult>> CompleteImport(DataTable dataTable)
        {
            var result = new List<UploadResult>();
            // var strBuilder = new StringBuilder();
            // strBuilder.AppendLine("Success,Email,Company,IsMulti,Reason");
            foreach (DataRow row in dataTable.Rows)
            {
                try
                {
                    var accountName = row.Get("Account Name");
                    var street1 = row.Get("Street 1");
                    var postcode = (row.Get("Postcode") ?? "").Trim();
                    var contactName = row.Get("Contact Name").Split(',')[0].Trim();
                    var telephone = row.Get("Telephone Number");
                    var companyEmail = row.Get("EMail");
                    var contactEmail = companyEmail.Split(';')[0].Trim();
                    var deliveryInstructions = row.Get("Delivery Instructions");
                    var billingEmail = row.Get("Billing E-Mail");
                    var billingCompany = row.Get("Billing Company");
                    var billingAddress = row.Get("Billing Address");

                    if (string.IsNullOrWhiteSpace(accountName))
                        continue;

                    billingCompany = string.IsNullOrWhiteSpace(billingCompany) ? accountName : billingCompany;
                    billingEmail = string.IsNullOrWhiteSpace(billingEmail) ? companyEmail : billingEmail;
                    var billingPostcode = string.IsNullOrWhiteSpace(billingAddress)
                        ? postcode
                        : billingAddress.Split(',').Last().Trim();
                    billingAddress = string.IsNullOrWhiteSpace(billingAddress) ? street1 : billingAddress;

                    var nameSplit = (contactName ?? "").Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    var firstName = (nameSplit.Length > 0) ? nameSplit[0] : "";
                    var lastName = (nameSplit.Length > 0) ? string.Join(" ", nameSplit.Skip(1).ToArray()) : "";


                    var user = UserManager.FindByEmail(contactEmail);

                    if (user == null)
                    {
                        await RegisterUserAndCompany(
                            companyEmail,
                            contactEmail,
                            firstName,
                            lastName,
                            accountName,
                            street1,
                            postcode,
                            billingAddress,
                            billingPostcode,
                            billingCompany,
                            billingEmail,
                            telephone,
                            deliveryInstructions);
                        
                        result.Add(new UploadResult
                        {
                            Success = true,
                            Email = companyEmail,
                            Company = accountName,
                            IsMulti = false,
                        });
                        // strBuilder.AppendLine($"Y,\"{companyEmail}\", {accountName}, N");
                    }
                    else
                    {
                        await RegisterMultiCompany(
                            user,
                            companyEmail,
                            contactEmail,
                            firstName,
                            lastName,
                            accountName,
                            street1,
                            postcode,
                            billingAddress,
                            billingPostcode,
                            billingCompany,
                            billingEmail,
                            telephone,
                            deliveryInstructions);
                        
                        result.Add(new UploadResult
                        {
                            Success = true,
                            Email = companyEmail,
                            Company = accountName,
                            IsMulti = true,
                        });
                        // strBuilder.AppendLine($"Y,\"{companyEmail}\", {accountName}, Y");
                    }
                }
                catch (Exception e)
                {
                    result.Add(new UploadResult
                    {
                        Success = false,
                        IsMulti = false,
                        Message = e.Message
                    });
                    // strBuilder.AppendLine($"N, , , \"{e.Message}\"");
                }
            }

            return result;
            // return Encoding.ASCII.GetBytes(strBuilder.ToString());
        }

        private async Task<bool> RegisterUserAndCompany(
            string companyEmail,
            string contactEmail,
            string firstName,
            string lastName,
            string accountName,
            string street1,
            string postcode,
            string billingAddress,
            string billingPostcode,
            string billingCompany,
            string billingEmail,
            string telephone,
            string deliveryInstructions)
        {
            //var password = "YourNewOgPassword!";
            var password = GetRandomPassword();

            var user = new User
            {
                UserName = contactEmail,
                FirstName = firstName,
                LastName = lastName,
                Email = contactEmail,
                EmailConfirmed = true,
                AgreeToGetEmails = false,
                TempPlainPassword = password,
                Company = new Company
                {
                    ID = Guid.NewGuid(),
                    Name = accountName,
                    Address1 = street1,
                    Postcode = postcode,
                    BillingAddress = billingAddress,
                    BillingPostcode = billingPostcode,
                    BillingCompanyName = billingCompany, // To change later
                    BillingEmail = billingEmail, // To change later
                    Email = companyEmail,
                    Telephone = telephone,
                    DeliveryInstruction = deliveryInstructions,
                    AgreeToGetEmail = false,
                    SignupDate = DateTime.Now,
                    DefaultCurrencyID = 1,
                    MilkDeliveryMonday = true,
                    MilkDeliveryTuesday = true,
                    MilkDeliveryWednesday = true,
                    MilkDeliveryThursday = true,
                    MilkDeliveryFriday = true,
                    MilkDeliverySaturday = true,
                    MilkDeliverySunday = false,
                    FruitsDeliveryMonday = true,
                    FruitsDeliveryTuesday = true,
                    FruitsDeliveryWednesday = true,
                    FruitsDeliveryThursday = true,
                    FruitsDeliveryFriday = true,
                    FruitsDeliverySaturday = true,
                    FruitsDeliverySunday = false,
                    DryGoodsDeliveryMonday = true,
                    DryGoodsDeliveryTuesday = true,
                    DryGoodsDeliveryWednesday = true,
                    DryGoodsDeliveryThursday = true,
                    DryGoodsDeliveryFriday = true,
                    DryGoodsDeliverySaturday = true,
                    AccountNo = GetOGAccountNo(),
                    IsMultiTenantedBuilding = false,
                    HasMultiSites = false,
                    IsTransferred = true,
                },
            };


            var result = await UserManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                UserManager.AddToRole(user.Id, "Admin");
                return true;
            }
            
            throw new Exception(string.Join("| ", result.Errors));
            
        }

        private async Task<bool> RegisterMultiCompany(
            User user,
            string companyEmail,
            string contactEmail,
            string firstName,
            string lastName,
            string accountName,
            string street1,
            string postcode,
            string billingAddress,
            string billingPostcode,
            string billingCompany,
            string billingEmail,
            string telephone,
            string deliveryInstructions)
        {
            var originalCompany = user.Company;
            if (originalCompany.CompanyCollectionID == null)
            {
                var companyCollection = new CompanyCollection()
                {
                    Name = originalCompany.Name,
                };
                new CompanyCollectionRepository().Create(companyCollection);
                originalCompany.CompanyCollectionID = companyCollection.ID;
                UserManager.Update(user);
            }

            user.Company = new Company
            {
                ID = Guid.NewGuid(),
                Name = accountName,
                Address1 = street1,
                Postcode = postcode,
                BillingAddress = billingAddress,
                BillingPostcode = billingPostcode,
                BillingCompanyName = billingCompany, // To change later
                BillingEmail = billingEmail, // To change later
                AgreeToGetEmail = false,
                Email = companyEmail,
                Telephone = telephone,
                DeliveryInstruction = deliveryInstructions,
                SignupDate = DateTime.Now,
                DefaultCurrencyID = 1,
                MilkDeliveryMonday = true,
                MilkDeliveryTuesday = true,
                MilkDeliveryWednesday = true,
                MilkDeliveryThursday = true,
                MilkDeliveryFriday = true,
                MilkDeliverySaturday = true,
                MilkDeliverySunday = false,
                FruitsDeliveryMonday = true,
                FruitsDeliveryTuesday = true,
                FruitsDeliveryWednesday = true,
                FruitsDeliveryThursday = true,
                FruitsDeliveryFriday = true,
                FruitsDeliverySaturday = true,
                FruitsDeliverySunday = false,
                DryGoodsDeliveryMonday = true,
                DryGoodsDeliveryTuesday = true,
                DryGoodsDeliveryWednesday = true,
                DryGoodsDeliveryThursday = true,
                DryGoodsDeliveryFriday = true,
                DryGoodsDeliverySaturday = true,
                AccountNo = GetOGAccountNo(),
                IsMultiTenantedBuilding = false,
                HasMultiSites = false,
                CompanyCollectionID = user.Company.CompanyCollectionID,
                IsTransferred = true,
            };
            user.CompanyID = user.Company.ID;

            UserManager.Update(user);

            return true;
        }

        public string GetOGAccountNo()
        {
            using (var context = new SnapDbContext())
            {
                var setting = context.Settings.FirstOrDefault();
                string accountNo = setting.DefaultReference + setting.StartingNumber.ToString().PadLeft(6, '0');
                setting.StartingNumber++;
                context.SaveChanges();
                return accountNo;
            }
        }

        private string GetRandomPassword(int length = 8)
        {
            var random = new Random(DateTime.Now.Millisecond);
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        
        public DataTable ImportExcelFile()
        {
            var dataTable = ImportService.CopyExcelFileToTable(
                "D:/WorkSpace/office-groceries/Dustin Customer Transferr (1).xlsx",
                false);
            return dataTable;
        }

    }

    public static class DataRowExtensions
    {
        public static string Get(this DataRow row, string columnName)
        {
            if (row[columnName] != null)
                return row[columnName].ToString();
            return null;
        }
    }

    public class UploadResult
    {
        public bool Success { get; set; }
        public string Email { get; set; } = "";
        public string Company { get; set; } = "";
        public bool IsMulti { get; set; }
        public string Message { get; set; } = "";
    }

}