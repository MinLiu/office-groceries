﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class TeamMembersController : GridController<TeamMember, TeamMemberViewModel>
    {

        public TeamMembersController()
            : base(new TeamMemberRepository(), new TeamMemberMapper())
        {

        }

        
        [AcceptVerbs(HttpVerbs.Post)]
        public  JsonResult Read([DataSourceRequest] DataSourceRequest request, string teamID)
        {
            var list = _repo.Read().Where(t => t.TeamID.ToString() == teamID)
                .OrderByDescending(o => o.User.Email)
                .ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<TeamMember>().ToDataSourceResult(request));
        }


    }

}