﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Net;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User,Admin")]
    public class UsersController : BaseController
    {

        public ActionResult ManageAccount(string successMessage = null, string errorMessage = null)
        {
            var userRepo = new UserRepository();
            var roleRepo = new RoleRepository();

            var roles = roleRepo.Read().ToList();

            var users = userRepo.Read().Where(x => x.CompanyID == CurrentUser.CompanyID)
                                            .Include(x => x.Roles)
                                            .OrderBy(x => x.Email)
                                            .ToList()
                                            .Select(u => new UserViewModel
                                            {
                                                ID = u.Id,
                                                Email = u.Email,
                                                FirstName = u.FirstName,
                                                LastName = u.LastName,
                                                PhoneNumber = u.PhoneNumber,
                                                Role = string.Join(",", roles.Where(r => u.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                                                Confirmed = u.EmailConfirmed,
                                            }).ToList();

            if (!string.IsNullOrEmpty(successMessage))
            {
                ViewBag.SuccessMessage = successMessage;
            }
            else if (!string.IsNullOrEmpty(errorMessage))
            {
                ViewBag.ErrorMessage = errorMessage;
            }
            return View(users);
        }

        public ActionResult NiManageAccount()
        {
            return View();
        }

        #region User Management List View

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        // READ: GridUsers
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {

            var userRepo = new UserRepository();
            var roleRepo = new RoleRepository();

            var roles = roleRepo.Read().ToList();

            var users = userRepo.Read().Where(x => x.CompanyID == CurrentUser.CompanyID && (
                                                 (x.Email.ToLower().Contains(filterText) || filterText == "" || filterText == null)
                                                    || (x.FirstName.ToLower().Contains(filterText) || filterText == "" || filterText == null)
                                                    || (x.LastName.ToLower().Contains(filterText) || filterText == "" || filterText == null)
                                                   )
                                            )
                                            .Where(x => x.Archived == false)
                                            .Include(x => x.Roles)
                                            .OrderBy(x => x.Email)
                                            .ToList()
                                            .Select(u => new UserViewModel
                                            {
                                                ID = u.Id,
                                                Email = u.Email,
                                                FirstName = u.FirstName,
                                                LastName = u.LastName,
                                                PhoneNumber = u.PhoneNumber,
                                                Role = string.Join(",", roles.Where(r => u.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                                                Confirmed = u.EmailConfirmed,
                                            });

            return Json(users.ToDataSourceResult(request));
        }

        // READ: GridUsers
        public ActionResult ReadAll([DataSourceRequest]DataSourceRequest request, Guid companyID)
        {

            var roleRepo = new RoleRepository();
            var company = new CompanyRepository().Find(companyID);

            var roles = roleRepo.Read().ToList();

            var companies = company.CompanyCollection?.Companies ?? new List<Company> {company};
            var users = companies.SelectMany(x => x.ApplicationUsers)
                .Where(u => u.CanSwitchCompany || u.CompanyID == companyID)
                .ToList()
                .Select(u => new UserViewModel
                {
                    ID = u.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    PhoneNumber = u.PhoneNumber,
                    Role = string.Join(",", roles.Where(r => u.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                    UserRole = new SelectItemViewModel(roles.FirstOrDefault(r => r.Id == u.Roles.FirstOrDefault()?.RoleId)?.Id, roles.FirstOrDefault(r => r.Id == u.Roles.FirstOrDefault()?.RoleId)?.Name),
                    Confirmed = u.EmailConfirmed,
                    CanSwitchCompany = u.CanSwitchCompany,
                    Company = new SelectItemViewModel(u.CompanyID.ToString(), u.Company.Name),
                });

            return Json(users.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> UpdateUser([DataSourceRequest] DataSourceRequest request, UserViewModel viewModel)
        {
            var userRepo = new UserRepository();
            // Find the existing entity.
            var model = userRepo.Find(viewModel.ID);

            var email = viewModel.Email.Trim();
            if (model.Email != email)
            {
                if (userRepo.Read().Any(x => x.Email == email))
                {
                    return Json(new DataSourceResult
                    {
                        Errors = "Email is used"
                    });
                }

                model.Email = email;
                model.UserName = email;
            }
            
            model.FirstName = viewModel.FirstName;
            model.LastName = viewModel.LastName;
            model.PhoneNumber = viewModel.PhoneNumber;
            model.CompanyID = Guid.Parse(viewModel.Company.ID);
            model.CanSwitchCompany = viewModel.CanSwitchCompany;

            userRepo.Update(model);

            if (viewModel.UserRole.ID != model.Roles.Select(x => x.RoleId).FirstOrDefault())
            {
                var roleRepo = new RoleRepository();
                var toRemoveRole = roleRepo.Find(model.Roles.Select(x => x.RoleId).FirstOrDefault());
                await UserManager.RemoveFromRoleAsync(model.Id, toRemoveRole.Name);
                await UserManager.AddToRoleAsync(model.Id, viewModel.UserRole.Name);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreateUser([DataSourceRequest] DataSourceRequest request, UserViewModel viewModel)
        {
            var existing = await UserManager.FindByEmailAsync(viewModel.Email);

            if (existing != null)
            {
                
                return Json(new DataSourceResult
                {
                    Errors = $"Email '{viewModel.Email}' is used"
                });
            }

            var user = new User
            {
                UserName = viewModel.Email,
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName,
                Email = viewModel.Email,
                EmailConfirmed = true,
                AgreeToGetEmails = false,
                CanSwitchCompany = viewModel.CanSwitchCompany,
                CompanyID = Guid.Parse(viewModel.Company.ID),
            };
            
            var result = await UserManager.CreateAsync(user, "JustRandomPasswords!23");

            if (result.Succeeded)
            {
                await UserManager.AddToRoleAsync(user.Id, viewModel.UserRole.Name);
            }

            viewModel.ID = user.Id;
            
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Destroy(UserViewModel viewModel)
        {
            var repo = new UserRepository();

            var model = repo.Read().First(u => u.Id == viewModel.ID);

            repo.Delete(model);

            return OkResult();
        }

        public ActionResult NiDestroy([DataSourceRequest]DataSourceRequest request, UserViewModel viewModel)
        {
            var repo = new UserRepository();

            // Check whether this user is deleting himself
            if (CurrentUser.Id == viewModel.ID)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "You can't remove the yourself");
            }

            // Check whether this user is the last user in this company
            if (repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID).Count() <= 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "You can't remove the last user");
            }

            var model = repo.Read().Where(u => u.Id == viewModel.ID).First();

            // Delete admin
            if (model.Roles.Select(r => r.RoleId).Contains("1"))
            {
                if (repo.Read().Where(x => x.CompanyID == CurrentUser.CompanyID && x.Roles.Select(r => r.RoleId).Contains("1")).Count() <= 1)
                {
                    return RedirectToAction("ManageAccount", new { errorMessage = "You can't remove the last admin" });
                }
            }

            model.Archived = true;
            model.Email = $"archived-{DateTime.Now:dd-MM-yy}-{model.Email}";
            model.UserName = $"archived-{DateTime.Now:dd-MM-yy}-{model.UserName}";
            model.Token = null;
            model.EmailConfirmed = false;
            
            repo.Update(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }



        #endregion


        #region Create User

        public ActionResult Create()
        {
            return View(
                new NewUserViewModel 
                {
                    AccessCRM = true,
                    AccessProducts = true,
                    AccessQuotations = true,
                    AccessJobs = true,
                    AccessInvoices = true,
                    AccessDeliveryNotes = true,
                    AccessPurchaseOrders = true,
                }
            );
        }


        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(NewUserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "UserManagement");

            var repo = new UserRepository();

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var currentTime = DateTime.Now.AddDays(-2);

            //Check to see if email is already taken
            if (UserManager.FindByEmail(model.Email) != null)
            {
                ViewBag.ErrorMessage = "The email address entered is already being used.";
                return View(model);
            }

            var user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                CompanyID = CurrentUser.CompanyID,  //Set the company ID as the same Current User
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                EmailConfirmed = true,
                AgreeToGetEmails = model.AgreeToGetEmail
                
            };

            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                if (model.AgreeToGetEmail)
                {
                    var success = await MailChimpService.OptIn(model.Email, model.FirstName, model.LastName);
                }

                ViewBag.SuccessMessage = "New user account created.";
                UserManager.AddToRole(user.Id, model.Role);
                UserManager.Update(user);

                if (User.IsInRole("Admin") || User.IsInRole("User"))
                {
                    return RedirectToAction("ManageAccount", "Users");
                }
                else
                { 
                    return RedirectToAction("NiManageAccount", "Users");
                }
            }
            else
            {
                ViewBag.ErrorMessage = result.Errors.First().ToString();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion


        #region Edit User


        // GET: User Edit
        public ActionResult Edit(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "Users");

            try
            {
                var roleRepo = new RoleRepository();
                var roles = roleRepo.Read().ToList();

                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.CompanyID == CurrentUser.CompanyID && x.Id.Equals(ID)).Include(u => u.Roles).First();

                //Create model then return it
                UserViewModel model = new UserViewModel
                {
                    ID = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    Role = string.Join(",", roles.Where(r => user.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                    AgreeToGetEmails = user.AgreeToGetEmails
                };

                return View("Edit", model);
            }
            catch
            {
                return RedirectToAction("Index", "Users");
            }
        }



        //
        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "Users");

            if (!ModelState.IsValid)
            {
                return PartialView("Edit", model);
            }
                        
            try
            {

                User user = UserManager.FindById(model.ID);

                //Check to see if the email address is already taken if the same email address is not input.
                if (user.Email != model.Email)
                {
                    if (UserManager.FindByEmail(model.Email) != null) //If a user already exsists with the user name then stop saving.
                    {
                        ViewBag.ErrorMessage = "Could not save changes. The Email address entered is already taken.";
                        return View("Edit", model);
                    }
                }

                //Update the user
                user.UserName = model.Email;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;

                //Remove all the roles then add the selected role
                var roleRepo = new RoleRepository();
                var roleNames = roleRepo.Read().Select(r => r.Name).ToArray();
                foreach (var roleName in roleNames)
                {
                    UserManager.RemoveFromRole(user.Id, roleName);
                }
                UserManager.AddToRole(user.Id, model.Role);

                //Save Changes
                UserManager.Update(user);
                
                ViewBag.SuccessMessage = "User Account Details have successfully been Saved.";
            }
            catch
            {
                ViewBag.ErrorMessage = "Could not save changes. Unknown Exception.";
            }


            return PartialView("Edit", model);
        }

        #endregion


        #region Change Password

        // GET: Change Password
        public ActionResult ChangePassword(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "Users");

            try
            {
                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.CompanyID == CurrentUser.CompanyID && x.Id.Equals(ID)).First();

                //Create model then return it
                ChangeUserPasswordViewModel model = new ChangeUserPasswordViewModel
                {
                    ID = user.Id,
                    Email = user.Email
                };

                return View(model);
            }
            catch { return RedirectToAction("Index", "Users"); }

        }


        // POST: Change Password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangeUserPasswordViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "Users");

            if (!ModelState.IsValid)
            {
                return PartialView("ChangePassword", model);
            }

            try
            {
                //Try Get User account
                var user = UserManager.FindById(model.ID);

                if (user == null)
                {
                    ViewBag.ErrorMessage = "User Account not found.";
                    return View("ChangePassword", model);
                }

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                IdentityResult result = UserManager.ResetPassword(user.Id, code, model.Password);

                model.Password = "";
                model.ConfirmPassword = "";

                if (result.Succeeded)
                {
                    ViewBag.SuccessMessage = "Password was successfully reset.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Failed to reset password. " + result.Errors.First().ToString();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Failed to reset password. " + ex.ToString();
            }

            return PartialView(model);
        }


        #endregion



        #region Teams

        // GET: Teams
        public ActionResult Teams()
        {
            var mapper = new CompanyMapper();
            var teamSettings = mapper.MapToViewModel(CurrentUser.Company);
            return View(teamSettings);            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Teams(CompanyViewModel model)
        {
            if (model == null) return RedirectToAction("Teams");


            var repo = new CompanyRepository();
            var mapper = new CompanyMapper();

            var edit = repo.Read().Where(c => c.ID == CurrentUser.CompanyID).First();

            if (model.UseTeams) model.UseHierarchy = false;
            edit.UseTeams = model.UseTeams;
            edit.UseHierarchy = model.UseHierarchy;
            edit.AccessSameLevelHierarchy = model.AccessSameLevelHierarchy;

            repo.Update(edit, new string[] { "UseTeams", "UseHierarchy", "AccessSameLevelHierarchy" });
            return View(model);
        }


        public ActionResult _TeamMembers(string ID)
        {
            
            try
            {
                var repo = new TeamRepository();
                var mapper = new TeamMapper();

                TeamViewModel team = mapper.MapToViewModel(repo.Find(Guid.Parse(ID)));
                return PartialView(team);
            }
            catch
            {
                return PartialView("_TeamBlank");
            }
          
        }

        //This returns the blank team view
        public ActionResult _TeamBlank()
        {
            return PartialView();
        }


        #endregion

        #region MailChimp

        public async Task<ActionResult> OptIn(string id)
        {
            User user = UserManager.FindById(id);

            if (user != null)
            { 
                var success = await MailChimpService.OptIn(user.Email, user.FirstName, user.LastName);
                if (success)
                {
                    user.AgreeToGetEmails = true;
                    UserManager.Update(user);
                }
            }

            return RedirectToAction("Edit", new { id = id });

        }

        public async Task<ActionResult> OptOut(string id)
        {
            User user = UserManager.FindById(id);

            if (user != null)
            {
                var success = await MailChimpService.OptOut(user.Email);
                if (success)
                {
                    user.AgreeToGetEmails = false;
                    UserManager.Update(user);
                }
            }

            return RedirectToAction("Edit", new { id = id });

        }

        #endregion

        #region Helpers


        public JsonResult DropDownRoles()
        {
            using (var context = new SnapDbContext())
            {                
                var roles = context.Roles
                    .Where(r => !r.Name.Contains("Super"))
                    .OrderBy(r => r.Name).ToArray()
                    .Select(r => new RoleViewModel { ID = r.Id, Name = r.Name })
                    .ToList();

                if (!User.IsInRole("NI-User"))
                    roles = roles.Where(r => !r.Name.Contains("NI-User")).ToList();

                return Json(roles, JsonRequestBehavior.AllowGet);
            }
        }
        
        #endregion

        [HttpGet]
        public JsonResult DropDownUsers(Guid companyID)
        {
            var company = new CompanyRepository().Find(companyID);

            var companies = company.CompanyCollection?.Companies ?? new List<Company> {company};
            var users = companies.SelectMany(x => x.ApplicationUsers)
                .ToList()
                .Select(u => new SelectItemViewModel()
                {
                    ID = u.Email,
                    Name = u.FirstName + " " + u.LastName + "<" + u.Email + ">"
                });

            return Json(users, JsonRequestBehavior.AllowGet);
        }
    }
}