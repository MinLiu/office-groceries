﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class TeamsController : GridController<Team, TeamViewModel>
    {

        public TeamsController()
            : base(new TeamRepository(), new TeamMapper())
        {

        }

        
        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = _repo.Read().Where(t => t.CompanyID == CurrentUser.CompanyID)
                .OrderByDescending(o => o.Name)
                .ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

       
        public override JsonResult Create(DataSourceRequest request, TeamViewModel viewModel)
        {
            viewModel.CompanyID = CurrentUser.CompanyID.ToString();
            return base.Create(request, viewModel);
        }


    }

}