﻿using NuIngredient.Models;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class DryGoodsPriceImportController : BaseController
    {
        public ActionResult Import(HttpPostedFileBase upload)
        {
            try
            {
                if (upload == null)
                {
                    throw new Exception("No file has been uploaded.");
                }
                else if (Path.GetExtension(upload.FileName) != ".xls" && Path.GetExtension(upload.FileName) != ".xlsx")
                {
                    throw new Exception("File is the wrong file format.");
                }

                var filePath = SaveFile(upload, "/DryGoodsPrice/");

                var helper = ImportDryGoodsPriceHelper.New();
                var result = helper.ImportExcelFile(Server.MapPath(filePath));

                return Json(result);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }
    }
}