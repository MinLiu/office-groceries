﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class SupplierOpenDayGridController : GridController<SupplierOpenDay, SupplierOpenDayGridViewModel>
    {
        public SupplierOpenDayGridController()
            :base(new SupplierOpenDayRepository(), new SupplierOpenDayGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid supplierID)
        {
            var models = _repo.Read().Where(x => x.SupplierID == supplierID);
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }
    }
}