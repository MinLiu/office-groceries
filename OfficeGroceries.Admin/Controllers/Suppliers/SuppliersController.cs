﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class SuppliersController : EntityController<Supplier, SupplierViewModel>
    {
      
        public SuppliersController()
            : base(new SupplierRepository(), new SupplierMapper())
        {

        }

        // GET: Suppliers
        public ActionResult Index()
        {
            return View();
        }

        // GET: /New Supplier
        public ActionResult _NewSupplier()
        {
            return PartialView(new SupplierViewModel());
        }


        // POST: /New Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewSupplier(SupplierViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            return Json(new { ID = model.ID });

        }

        // GET: /Edit Supplier
        public ActionResult _EditSupplier(Guid ID)
        {
            var model = _repo.Find(ID);
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }


        [HttpGet]
        public ActionResult _EditSupplierDetails(Guid ID)
        {
            var model = _repo.Find(ID);
            var viewModel = _mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }

        // POST: /New Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditSupplierDetails(SupplierViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.

            return PartialView(viewModel);

        }

        [HttpPost]
        public ActionResult SendWeeklyReminder(Guid supplierID)
        {
            var helper = new WeeklyOrderReminderHelper(new WeeklyOrderReminderHelper.WeeklyOrderReminderConfig
            {
                SupplierId = supplierID
            });
            
            helper.Execute();
            
            return OkResult();
        }
        
        [HttpPost]
        public ActionResult SendDailyReminder(Guid supplierID)
        {
            var config = new DailyOrderReminderHelper.DailyOrderReminderConfig()
            {
                SupplierId = supplierID,
            };
            
            var helper = new DailyOrderReminderHelper(config);
            var success = helper.Execute();
            return Content(success? "Success" : "Failed, please check dustin@office-groceries to see the error message.");
        }

        [HttpPost]
        public ActionResult SendWeeklyReminderToAll()
        {
            var helper = new WeeklyOrderReminderHelper(new WeeklyOrderReminderHelper.WeeklyOrderReminderConfig
            {
                IgnoreWeekday = true
            });
            
            helper.Execute();

            return OkResult();
        }

        [HttpGet]
        public ActionResult _TabCustomers(string supplierID)
        {
            return PartialView(new SupplierViewModel {ID = supplierID});
        }
        
        [HttpGet]
        public ActionResult _TabSupplierHolidays(string supplierID)
        {
            return PartialView(new SupplierViewModel {ID = supplierID});
        }
        
        [HttpGet]
        public ActionResult _TabSupplierOpenDays(string supplierID)
        {
            return PartialView(new SupplierViewModel {ID = supplierID});
        }
        
        [HttpGet]
        public ActionResult _TabSupplierMilkProductCodes(string supplierID)
        {
            return PartialView(new SupplierViewModel {ID = supplierID});
        }
        
        [HttpGet]
        public ActionResult _TabSupplierFruitProductCodes(string supplierID)
        {
            return PartialView(new SupplierViewModel {ID = supplierID});
        }
        
        [HttpPost]
        public ActionResult ReadSupplierCustomers([DataSourceRequest] DataSourceRequest request, Guid supplierID)
        {
            var repo = new CompanySupplierRepository();
            var viewModels = repo.Read()
                .Where(cs => cs.SupplierID == supplierID)
                .Where(cs => cs.EndDate == null || cs.EndDate >= DateTime.Now)
                .ToList()
                .Select(cs => new CompanySupplierGridViewModel()
                {
                    CompanyName = cs.Company.Name,
                    CompanyPostcode = cs.Company.Postcode,
                    StartDate = cs.StartDate
                });

            return Json(viewModels.ToDataSourceResult(request));
        }
        
        public ActionResult AddAllMissingMilkProducts(Guid supplierID)
        {
            var milkProductRepo = new MilkProductRepository();
            var supplierMilkProductCodeRepo = new SupplierMilkProductCodeRepository();
            
            var allMilkProductIds = milkProductRepo.Read()
                .Where(mp => mp.Deleted == false)
                .Select(mp => mp.ID)
                .ToList();
            
            var existingMilkProductIds = supplierMilkProductCodeRepo.Read()
                .Where(smpc => smpc.SupplierID == supplierID)
                .Select(smpc => smpc.MilkProductID)
                .ToList();
            
            var missingMilkProductIds = allMilkProductIds.Except(existingMilkProductIds).ToList();
            
            var toCreate = missingMilkProductIds.Select(productId => new SupplierMilkProductCode()
            {
                SupplierID = supplierID,
                MilkProductID = productId,
                Code = ""
            }).ToList();

            supplierMilkProductCodeRepo.Create(toCreate);            
            
            return OkResult();
        }

        public ActionResult AddAllMissingFruitProducts(Guid supplierID)
        {
            var fruitProductRepo = new FruitProductRepository();
            var supplierFruitProductCodeRepo = new SupplierFruitProductCodeRepository();
            
            var allFruitProductIds = fruitProductRepo.Read()
                .Where(mp => mp.Deleted == false)
                .Select(mp => mp.ID)
                .ToList();
            
            var existingFruitProductIds = supplierFruitProductCodeRepo.Read()
                .Where(smpc => smpc.SupplierID == supplierID)
                .Select(smpc => smpc.FruitProductID)
                .ToList();
            
            var missingFruitProductIds = allFruitProductIds.Except(existingFruitProductIds).ToList();
            
            var toCreate = missingFruitProductIds.Select(productId => new SupplierFruitProductCode()
            {
                SupplierID = supplierID,
                FruitProductID = productId,
                Code = ""
            }).ToList();
            
            supplierFruitProductCodeRepo.Create(toCreate);
            
            return OkResult();
        }

        //// GET: /Edit Supplier
        //public ActionResult _EditSupplier(string ID)
        //{
        //    try
        //    {
        //        //Make sure the supplier being returned belongs to the user of the user
        //        Supplier clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
        //        SupplierViewModel model = _mapper.MapToViewModel(clt);

        //        ViewBag.CurrentUserID = CurrentUser.Id;
        //        ViewBag.CurrentUserEmail = CurrentUser.Email;
        //        return PartialView(model);
        //    }
        //    catch {
        //        return PartialView("_NewSupplier", new SupplierViewModel());
        //    }

        //}

        public JsonResult DropDownSuppliers(SupplierType? supplierType)
        {
            var viewModels = _repo.Read()
                                  .Where(s => (supplierType == null || s.SupplierType == supplierType))
                                  .Where(s => s.Deleted == false)
                                  .ToList()
                                  .Select(s => new DropDownListItem()
                                  {
                                      Text = s.Name,
                                      Value = s.ID.ToString()
                                  });

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownDifficultSuppliers(SupplierType? supplierType)
        {
            var viewModels = _repo.Read()
                                  .Where(s => (supplierType == null || s.SupplierType == supplierType))
                                  .Where(s => s.UseMasterAccount == false)
                                  .Where(s => s.Deleted == false)
                                  .ToList()
                                  .Select(s => new DropDownListItem()
                                  {
                                      Text = s.Name,
                                      Value = s.ID.ToString()
                                  });

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult _ReschedulePopup(Guid supplierId)
        {
            return PartialView(new RescheduleOrderVm { SupplierId = supplierId });
        }
        
        public async Task<ActionResult> Reschedule(RescheduleOrderVm model)
        {
            var supplier = _repo.Find(model.SupplierId);
            switch (supplier.SupplierType)
            {
                case SupplierType.Milk:
                {
                    var helper = new MilkOrderRescheduleHelper(model.SupplierId);
                    await helper.RescheduleOrderAsync(model.OldDate, model.NewDate, model.Policy);
                    return OkResult();
                }
                case SupplierType.Fruits:
                {
                    var helper = new FruitOrderRescheduleHelper(model.SupplierId);
                    await helper.RescheduleOrderAsync(model.OldDate, model.NewDate, model.Policy);
                    return OkResult();
                }
                default:
                    throw new Exception("Supplier type not supported");
            }
        }


        private IQueryable<Supplier> GetSuppliers()
        {
            var suppliers = _repo.Read()
                               .Where(c => c.Deleted == false)
                               .OrderBy(c => c.Name)
                               .AsQueryable();

            return suppliers;
        }

    }
    
}