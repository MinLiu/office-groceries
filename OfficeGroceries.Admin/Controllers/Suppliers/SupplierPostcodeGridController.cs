﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class SupplierPostcodeGridController : GridController<SupplierPostcode, SupplierPostcodeGridViewModel>
    {
      
        public SupplierPostcodeGridController()
            : base(new SupplierPostcodeRepository(), new SupplierPostcodeGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid supplierID)
        {
            var models = _repo.Read().Where(x => x.SupplierID == supplierID);
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

    }
    
}