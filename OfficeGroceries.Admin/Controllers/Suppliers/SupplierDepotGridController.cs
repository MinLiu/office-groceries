﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class SupplierDepotGridController : GridController<SupplierDepot, SupplierDepotGridViewModel>
    {
        public SupplierDepotGridController()
            : base(new SupplierDepotRepository(), new SupplierDepotGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid supplierID)
        {
            var models = _repo.Read().Where(x => x.SupplierID == supplierID);
            var viewModels = models
                .OrderBy(x => x.Name)
                .ToList()
                .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public JsonResult DropDown(Guid supplierID)
        {
            var models = _repo.Read()
                .Where(x => x.SupplierID == supplierID)
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    Email = x.Email
                })
                .ToList()
                .Select(x => new
                {
                    ID = x.ID.ToString(),
                    Name = $"{x.Name} ({x.Email})"
                })
                .Select(x => new SelectItemViewModel
                {
                    ID = x.ID,
                    Name = x.Name.Length > 50 ? x.Name.Substring(0, 50) + "..." : x.Name
                });

            return Json(models, JsonRequestBehavior.AllowGet);
        }

        
    }
    
}