﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class SuppliersGridController : GridController<Supplier, SupplierGridViewModel>
    {
      
        public SuppliersGridController()
            : base(new SupplierRepository(), new SupplierGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string postcode, string filterText)
        {
            var models = _repo.Read().Where(x => x.Deleted == false);
            if (!string.IsNullOrEmpty(postcode))
            {
                postcode = (postcode ?? "").ToUpper().Trim().Replace(" ", "");
                var postcodePrefix = Regex.Split(postcode, @"[0-9]+")[0];
                models = models.Where(x => x.SupplierPostcode.Any(p => postcode.StartsWith(p.Postcode) && p.Postcode.StartsWith(postcodePrefix)));
            }

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                filterText = filterText.Trim();
                models = models.Where(x => x.Name.Contains(filterText));
            }
            
            var viewModels = models.OrderBy(x => x.SupplierType).ThenBy(x => x.UseMasterAccount).ThenBy(x => x.Name).ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public JsonResult ReadPotentialMilkSuppliers([DataSourceRequest] DataSourceRequest request, string postcode)
        {
            var helper = new PotentialSupplierHelper();
            var models = helper.GetPotentialMilkSuppliers(postcode);

            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));
            return Json(viewModels.ToDataSourceResult(request));
        }

        public JsonResult ReadPotentialFruitSuppliers([DataSourceRequest] DataSourceRequest request, string postcode)
        {
            var helper = new PotentialSupplierHelper();
            var models = helper.GetPotentialFruitSuppliers(postcode);

            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));
            return Json(viewModels.ToDataSourceResult(request));
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, SupplierGridViewModel viewModel)
        {

            // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
    }
    
}