﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Data.Entity;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class SupplierMilkProductCodeGridController : GridController<SupplierMilkProductCode, SupplierMilkProductCodeGridViewModel>
    {
        public SupplierMilkProductCodeGridController()
            : base(new SupplierMilkProductCodeRepository(), new SupplierMilkProductCodeGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? supplierID, Guid? productID)
        {
            var query = _repo.Read().AsQueryable();

            if (supplierID != null)
            {
                query = query.Where(x => x.SupplierID == supplierID);
            }
            
            if (productID != null)
            {
                query = query.Where(x => x.MilkProductID == productID);
            }

            var viewModels = query
                .Include(x => x.MilkProduct)
                .Include(x => x.Supplier)
                .OrderBy(x => x.Code)
                .ToList()
                .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

    }
    
}