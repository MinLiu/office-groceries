﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class IssueEmailGridController : GridController<IssueEmail, IssueEmailGridViewModel>
    {
        public IssueEmailGridController()
            : base(new IssueEmailRepository(), new IssueEmailGridMapper())
        {

        }

        [HttpPost]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid? supplierID, Guid? companyID)
        {
            var models = _repo.Read().AsQueryable();
            
            if (supplierID.HasValue)
            {
                models = models.Where(x => x.SupplierID == supplierID.Value);
            }
            
            if (companyID != Guid.Empty)
            {
                models = models.Where(x => x.CompanyID == companyID.Value);
            }
            
            var viewModels = models
                .OrderBy(x => x.Subject)
                .ToList()
                .Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }
    }
    
}