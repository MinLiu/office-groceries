﻿using NuIngredient.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        public ActionResult Index()
        {
            if (User.IsInRole("NI-User"))
            {
                return View("DashboardNI");
            }
            else
            {
                var viewModel = new DashboardViewModel();
                viewModel.Set(CurrentUser.Company);
                return View(viewModel);
            }
        }

        public ActionResult _FruitOrderPanel()
        {
            var fruitPdtService = new FruitProductService(new SnapDbContext());

            var startOfThisWeek = DateTime.Today.StartOfWeek(DayOfWeek.Monday);

            var orderHeaderRepo = new FruitOrderHeaderRepository();
            var orderRepo = new FruitOrderRepository();
            var orderMapper = new FruitOrderMapper();

            // History Order of this week
            var historyOrder = orderHeaderRepo.Read()
                                              .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                              .Where(x => x.OrderMode == OrderMode.History)
                                              .Where(x => x.FromDate == startOfThisWeek)
                                              .Include(x => x.Items)
                                              .FirstOrDefault();

            // Order of this week
            var orderThisWeek = orderHeaderRepo.Read()
                                               .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                               .Where(x => x.OrderMode == OrderMode.OneOff)
                                               .Where(x => x.FromDate == startOfThisWeek)
                                               .Include(x => x.Items)
                                               .FirstOrDefault();
            if (orderThisWeek == null)
            {
                orderThisWeek = orderHeaderRepo.Read()
                                               .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                               .Where(x => x.OrderMode == OrderMode.Regular)
                                               .Include(x => x.Items)
                                               .FirstOrDefault();
            }

            var combinedOrder = new List<FruitOrder>();
            if (historyOrder != null)
                combinedOrder.AddRange(historyOrder.Items);

            if (orderThisWeek != null)
            { 
                var seperaterWeekday = DateTime.Now.AddHours(CutOffTime.RestTime).DayOfWeek;
                foreach (var item in orderThisWeek.Items)
                {
                    var combinedOrderItem = combinedOrder.Where(x => x.FruitProductID == item.FruitProductID).FirstOrDefault();
                    if (combinedOrderItem == null)
                    {
                        combinedOrderItem = new FruitOrder()
                        {
                            ID = Guid.NewGuid(),
                            Monday = 0,
                            Tuesday = 0,
                            Wednesday = 0,
                            Thursday = 0,
                            Friday = 0,
                            Saturday = 0,
                            Sunday = 0,
                            TotalNet = 0,
                            TotalVAT = 0,
                            Price = item.Price,
                            ProductName = item.ProductName,
                            FruitProductID = item.FruitProductID,
                            Fruit = new FruitProductRepository().Find(item.FruitProductID),
                            Unit = item.Unit,
                            VATRate = item.VATRate,
                        };
                        combinedOrder.Add(combinedOrderItem);
                    }

                    switch (seperaterWeekday)
                    {
                        case DayOfWeek.Monday:
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                break;
                            combinedOrderItem.Tuesday = item.Tuesday;
                            combinedOrderItem.Wednesday = item.Wednesday;
                            combinedOrderItem.Thursday = item.Thursday;
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Tuesday:
                            combinedOrderItem.Wednesday = item.Wednesday;
                            combinedOrderItem.Thursday = item.Thursday;
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Wednesday:
                            combinedOrderItem.Thursday = item.Thursday;
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Thursday:
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Friday:
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Saturday:
                        case DayOfWeek.Sunday:
                            break;
                    }

                    var subtotalResult = fruitPdtService.CalculateSubtotal(combinedOrderItem.FruitProductID, CurrentUser.CompanyID, combinedOrderItem.Monday, combinedOrderItem.Tuesday, combinedOrderItem.Wednesday, combinedOrderItem.Thursday, combinedOrderItem.Friday, combinedOrderItem.Saturday);
                    combinedOrderItem.Price = subtotalResult.UnitPrice;
                    combinedOrderItem.TotalNet = subtotalResult.Subtotal;
                    combinedOrderItem.TotalVAT = combinedOrderItem.TotalNet * combinedOrderItem.VATRate;
                }
            }
            var viewModel = new DashboardFruitOrderViewModel()
            {
                WeeklyOrder = combinedOrder.Select(x => orderMapper.MapToViewModel(x)).ToList(),
                WeekyTotal = combinedOrder.Any() ? combinedOrder.Sum(fo => fo.Price * (fo.Monday + fo.Tuesday + fo.Wednesday + fo.Thursday + fo.Friday + fo.Saturday)) : 0
            };
            return PartialView(viewModel);
        }

        public ActionResult _MilkOrderPanel()
        {
            var startOfThisWeek = DateTime.Today.StartOfWeek(DayOfWeek.Monday);

            var orderHeaderRepo = new MilkOrderHeaderRepository();
            var orderRepo = new MilkOrderRepository();
            var orderMapper = new MilkOrderMapper();

            // History Order of this week
            var historyOrder = orderHeaderRepo.Read()
                                              .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                              .Where(x => x.OrderMode == OrderMode.History)
                                              .Where(x => x.FromDate == startOfThisWeek)
                                              .Include(x => x.Items)
                                              .FirstOrDefault();

            // Order of this week
            var orderThisWeek = orderHeaderRepo.Read()
                                               .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                               .Where(x => x.OrderMode == OrderMode.OneOff)
                                               .Where(x => x.FromDate == startOfThisWeek)
                                               .Include(x => x.Items)
                                               .FirstOrDefault();
            if (orderThisWeek == null)
            {
                orderThisWeek = orderHeaderRepo.Read()
                                               .Where(x => x.CompanyID == CurrentUser.CompanyID)
                                               .Where(x => x.OrderMode == OrderMode.Regular)
                                               .Include(x => x.Items)
                                               .FirstOrDefault();
            }

            var combinedOrder = new List<MilkOrder>();
            if (historyOrder != null)
                combinedOrder.AddRange(historyOrder.Items);

            if (orderThisWeek != null)
            {
                var seperaterWeekday = DateTime.Now.AddHours(CutOffTime.RestTime).DayOfWeek;
                foreach (var item in orderThisWeek.Items)
                {
                    var combinedOrderItem = combinedOrder.Where(x => x.MilkProductID == item.MilkProductID).FirstOrDefault();
                    if (combinedOrderItem == null)
                    {
                        combinedOrderItem = new MilkOrder()
                        {
                            ID = Guid.NewGuid(),
                            Monday = 0,
                            Tuesday = 0,
                            Wednesday = 0,
                            Thursday = 0,
                            Friday = 0,
                            Saturday = 0,
                            Sunday = 0,
                            TotalNet = 0,
                            TotalVAT = 0,
                            Price = item.Price,
                            ProductName = item.ProductName,
                            MilkProductID = item.MilkProductID,
                            Milk = new MilkProductRepository().Find(item.MilkProductID),
                            Unit = item.Unit,
                            VATRate = item.VATRate,
                        };
                        combinedOrder.Add(combinedOrderItem);
                    }

                    switch (seperaterWeekday)
                    {
                        case DayOfWeek.Monday:
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                break;
                            combinedOrderItem.Tuesday = item.Tuesday;
                            combinedOrderItem.Wednesday = item.Wednesday;
                            combinedOrderItem.Thursday = item.Thursday;
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Tuesday:
                            combinedOrderItem.Wednesday = item.Wednesday;
                            combinedOrderItem.Thursday = item.Thursday;
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Wednesday:
                            combinedOrderItem.Thursday = item.Thursday;
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Thursday:
                            combinedOrderItem.Friday = item.Friday;
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Friday:
                            combinedOrderItem.Saturday = item.Saturday;
                            break;
                        case DayOfWeek.Saturday:
                        case DayOfWeek.Sunday:
                            break;
                    }

                    combinedOrderItem.TotalNet = (combinedOrderItem.Monday + combinedOrderItem.Tuesday + combinedOrderItem.Wednesday + combinedOrderItem.Thursday + combinedOrderItem.Friday + combinedOrderItem.Saturday) * combinedOrderItem.Price;
                    combinedOrderItem.TotalVAT = combinedOrderItem.TotalNet * combinedOrderItem.VATRate;
                }
            }
            var viewModel = new DashboardMilkOrderViewModel()
            {
                WeeklyOrder = combinedOrder.Select(x => orderMapper.MapToViewModel(x)).ToList(),
                WeekyTotal = combinedOrder.Any() ? combinedOrder.Sum(fo => fo.Price * (fo.Monday + fo.Tuesday + fo.Wednesday + fo.Thursday + fo.Friday + fo.Saturday)) : 0
            };
            return PartialView(viewModel);
        }

        public ActionResult _ThisWeekMilkORderPanel()
        {
            return PartialView();
        }
    }
}