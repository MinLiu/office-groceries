﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.ComponentModel;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class TestimonialGridController : GridController<Testimonial, TestimonialGridViewModel>
    {

        public TestimonialGridController()
            : base(new TestimonialRepository(), new TestimonialGridMapper())
        {

        }

        [HttpPost]
        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _repo.Read()
                              .Where(x => x.Deleted == false)
                              .OrderBy(x => x.SortPos);

            var viewModels = models.ToList()
                                   .Select(s => _mapper.MapToViewModel(s));

            var result = new DataSourceResult()
            {
                Data = viewModels,
                Total = models.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult _UpdatePositions(Guid testimonialID, int oldIndex, int newindex)
        {
            var model = _repo.Find(testimonialID);

            var allTestimonials = _repo.Read().Where(x => x.Deleted == false).OrderBy(x => x.SortPos).ToList();

            allTestimonials.Remove(model);

            var top = newindex - 1 >= 0 ? allTestimonials.ElementAt(newindex - 1) : null;
            var bottom = newindex < allTestimonials.Count ? allTestimonials.ElementAt(newindex) : null;

            allTestimonials.Insert(newindex >= 0 ? newindex : 0, model);

            int sortPos = 0;
            foreach (var item in allTestimonials)
            {
                item.SortPos = sortPos++;
            }

            _repo.Update(allTestimonials);

            return Json("Success");
        }
    }
}