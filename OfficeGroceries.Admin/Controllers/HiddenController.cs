﻿using System.Web.Mvc;
using NuIngredient.Models;
using System.Linq;
using System;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class HiddenController : BaseController
    {
        public ActionResult DryGoodsPriceImport()
        {
            return View();
        }

        public ActionResult CalculateAll()
        {
            var fromDate = new DateTime(2020, 4, 1);

            using(var ctx = new SnapDbContext())
            {
                var milkOrderHeaders = new MilkOrderHeaderRepository(ctx)
                    .Read()
                    .Where(c => c.OrderMode == OrderMode.OneOff || c.OrderMode == OrderMode.Regular)
                    .Where(c => c.FromDate == null || c.FromDate >= fromDate)
                    .ToList();
                var milkService = new MilkOrderService<MilkOrderHeaderViewModel>(new MilkOrderHeaderMapper(), ctx);
                milkOrderHeaders.ForEach(m => milkService.CalculateTotal(m));
            }

            using (var ctx = new SnapDbContext())
            {
                var fruitOrderHeaders = new FruitOrderHeaderRepository(ctx)
                    .Read()
                    .Where(c => c.OrderMode == OrderMode.OneOff || c.OrderMode == OrderMode.Regular)
                    .Where(c => c.FromDate == null || c.FromDate >= fromDate)
                    .ToList();
                var fruitservice = new FruitOrderService<FruitOrderHeaderViewModel>(new FruitOrderHeaderMapper(), ctx);
                fruitOrderHeaders.ForEach(m => fruitservice.CalculateTotal(m));
            }

            return Content("Success");
        }
    }
}