﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class ProformaApproverGridController : GridController<ProformaApprover, ProformaApproverGridViewModel>
    {
        public ProformaApproverGridController()
            :base(new ProformaApproverRepository(), new ProformaApproverGridMapper())
        {

        }
        
        public JsonResult DropDown()
        {
            var models = _repo.Read()
                .Select(u => new SelectItemViewModel()
                {
                    ID = u.ID.ToString(),
                    Name = u.Name
                })
                .ToList();

            return Json(models, JsonRequestBehavior.AllowGet);
        }
        
    }
}