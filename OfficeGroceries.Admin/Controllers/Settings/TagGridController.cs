﻿using System.Linq;
using System.Web.Mvc;
using NuIngredient.Models;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class TagGridController : GridController<Tag, TagGridViewModel>
    {
        public TagGridController()
            :base(new TagRepository(), new TagGridMapper())
        {

        }

        public JsonResult DropDownTags()
        {
            var result = _repo.Read()
                .OrderBy(t => t.Name)
                .ToList()
                .Select(t => _mapper.MapToViewModel(t));
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}