﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using NuIngredient.Models.Extenstions;

namespace NuIngredient.Controllers
{
    [Authorize]
    public class CustomerSettingsController : BaseController
    {
        CompanyRepository _repo = new CompanyRepository();
        CompanyMapper _mapper = new CompanyMapper();
        // GET: Settings
        [HttpGet]
        public ActionResult _EditCustomerDetails()
        {
            var viewModel = _mapper.MapToViewModel(CurrentUser.Company);
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditCustomerDetails(CompanyViewModel viewModel)
        {
            var model = _repo.Find(Guid.Parse(viewModel.ID));

            SendCustomerDetailChangeEmail(model, viewModel);
            _mapper.MapToModel(viewModel, model);
            _repo.Update(model, new string[] { "Name", "Address1", "Postcode", "Telephone", "Email", "DeliveryInstruction" });
            ViewBag.Saved = true;

            return PartialView(_mapper.MapToViewModel(model));
        }

        public void SendCustomerDetailChangeEmail(Company before, CompanyViewModel after)
        {
            var changeLog = string.Empty;
            changeLog += AddDifference("Name", before.Name, after.Name);
            changeLog += AddDifference("Delivery Address", before.Address1, after.Address1);
            changeLog += AddDifference("Postcode", before.Postcode, after.Postcode);
            changeLog += AddDifference("Telephone", before.Telephone, after.Telephone);
            changeLog += AddDifference("Email", before.Email, after.Email);
            changeLog += AddDifference("Delivery Instruction", before.DeliveryInstruction, after.DeliveryInstruction);

            if (changeLog == string.Empty) return;

            using (var context = new SnapDbContext())
            {
                var mailServer = context.EmailSettings.First();
                var overallSetting = context.Settings.First();
                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = String.Format("Detail Change from {0} ({1})", before.Name, before.AccountNo);
                mailMessage.AddToEmails(overallSetting.FeedbackCenterEmail);
                mailMessage.From = new MailAddress(mailServer.Email);
                mailMessage.Body = String.Format("<div class=\"row\" style='max-width: 500px;'>" +
                                                    "Customer Feedback" +
                                                    "<hr />" +
                                                    "<table>" +
                                                        changeLog +
                                                    "</table>" +
                                                    "<hr />" +
                                                "</div>");
                mailMessage.IsBodyHtml = true;

                // Send to feedback center
                EmailService.SendEmail(mailMessage, Server);
            }
        }

        private string AddDifference(string label, string before, string after)
        {
            if (before == after)
                return string.Empty;

            var str = "<tr>" +
                          "<td>" +
                              "<label>" + label + ":</label>" +
                          "</td>" +
                          "<td>" +
                              before +
                          "</td>" +
                          "<td>" +
                              after +
                          "</td>" +
                      "</tr>";
            return str;
        }
    }
}