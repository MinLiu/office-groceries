﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NuIngredient.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace NuIngredient.Controllers
{
    [Authorize(Roles = "NI-User")]
    public class HolidayGridController : GridController<Holiday, HolidayGridViewModel>
    {
        public HolidayGridController()
            :base(new HolidayRepository(), new HolidayGridMapper())
        {

        }

        public override JsonResult Read(DataSourceRequest request)
        {
            var models = _repo.Read().Where(x => x.SupplierID == null).OrderByDescending(s => s.Date);
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        public JsonResult ReadSupplierHolidays([DataSourceRequest] DataSourceRequest request, Guid supplierID)
        {
            var models = _repo.Read().Where(x => x.SupplierID == supplierID).OrderByDescending(s => s.Date);
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }
    }
}