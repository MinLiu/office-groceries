﻿(function($){

    $(document).ajaxComplete(function () {
    });

	$(document).ready(function(){
        circles();
        NumricInputsLoaded();

        $('#step-1').each(function () {
            $(this).mouseover(function () {
			    $('.step-circles#step-1-show').show();	
            });
        });
		
		$('#step-2').mouseover(function(){
			$('.step-circles#step-2-show').show();	
		});
		
		$('#step-3').mouseover(function(){
			$('.step-circles#step-3-show').show();	
		});
		
		$('#step-4').mouseover(function(){
			$('.step-circles#step-4-show').show();	
		});
		
		$('#step-5').mouseover(function(){
			$('.step-circles#step-5-show').show();	
		});
		
		$('#step-6').mouseover(function(){
			$('.step-circles#step-6-show').show();	
		});
		
		$('#step-1, #step-2, #step-3, #step-4, #step-5, #step-6').mouseout(function(){
			$('.step-circles#step-1-show,.step-circles#step-2-show, .step-circles#step-3-show, .step-circles#step-4-show, .step-circles#step-5-show, .step-circles#step-6-show').hide();	
        });


        


	});
	
	$(window).resize(function(){
		circles ();
	});


    function circles() {
        $(".step-circles").each(function () {

            if ($(this).width() > $(this).height())
                $(this).height($(this).width());
            else 
                $(this).width($(this).height());
        });
    }    

    function NumricInputsLoaded() {
        $('html').on('click', '[class*=numeric-up]', function () {
            $(this).parent().find('input').each(function (e) {
                $(this).val(+$(this).val() + 1);
                $(this).change();
            });
        });
        //$('[class*=numeric-up]').click(function () {
        //    $(this).parent().find('input').each(function (e) {
        //        $(this).val(+$(this).val() + 1);
        //        $(this).change();
        //    });
        //});
        $('html').on('click', '[class*=numeric-down]', function () {
            $(this).parent().find('input').each(function (e) {
                if ($(this).val() != 0) {
                    $(this).val(+$(this).val() - 1);
                    $(this).change();
                }
            });
        });
        //$('[class*=numeric-down]').click(function () {
        //    $(this).parent().find('input').each(function (e) {
        //        $(this).val(+$(this).val() - 1);
        //        $(this).change();
        //    });
        //});


        $('html').on('keydown', '.weekday-val', function (e) {
            if (!((e.keyCode > 95 && e.keyCode < 106)
                || (e.keyCode > 47 && e.keyCode < 58)
                || e.keyCode == 8)) {
                return false;
            }
        })
    }
	
})(jQuery);

