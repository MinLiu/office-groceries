﻿/*
* Make sure _Layout page has the following element
* - NewEmail_Body
* - SlideContent_NewEmail
* - Button_Close_Email_Content
* 
* */

const emailJs = {
    newEmail: (supplierId, type) => {
        $("#NewEmail_Body").load(`/Emails/_NewEmail?supplierID=${supplierId}&type=${type}`);

        $("#SlideContent_NewEmail").animate({ width: '90%' }, { queue: false, duration: 300 });
        $("#SlideContent_NewEmail").css("display", "block");
        $("#Button_Close_Email_Content").css("display", "block");
    },

    hideNewEmailSlide: () => {
        $("#SlideContent_NewEmail").animate({ width: '0%' }, { queue: false, duration: 300 });
        setTimeout(function () {
            $("#SlideContent_NewEmail").css("display", "none");
            $("#Button_Close_Email_Content").css("display", "none");
        }, 300);
    }
}