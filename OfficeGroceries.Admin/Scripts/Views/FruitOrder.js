﻿/*  parameters: 
    FruitMinimumValue: decimal
    IsAuthenticated: bool
    
*/

//function ShowFruitProspectForm() {
//    if (!CheckFruitMinimum()) {
//        alert('Dear Customer\nPlease note a minimum spend of £' + parseFloat(Math.round(FruitMinimumValue * 100) / 100).toFixed(2) + ' per week is required for Fruit deliveries');
//        return false;
//    }
//    SaveFruitOrders(false, 'Draft');
//    $('#Modal_Prospect .modal-title').html('Contact Details');
//    $('#Modal_Prospect .modal-body').load("/Prospects/_ProspectForm");
//    $('#Modal_Prospect').modal('show');
//}

function FruitQuoteExist() {
    if ($('#fruit-quotes').length) {
        return true;
    }
    return false;
}

function CheckFruitMinimum() {
    var totalPrice = 0;
    $('div.order-products.fruit').each(function () {
        var weeklyTotal = kendo.parseFloat($(this).data('subtotal') | 0);
        totalPrice += weeklyTotal;
    });
    console.log("totalprice: " + totalPrice, ", Minimum: " + FruitMinimumValue);
    if (totalPrice < FruitMinimumValue)
        return false;

    return true;
}

function GetFruitSubTotal(rowId) {
    var fruitRow = $('div.order-products.fruit#' + rowId);
    var fruitId = fruitRow.data('fruitid');
    var mon = kendo.parseInt($('#' + rowId + '-monday-val').val() | 0);
    var tue = kendo.parseInt($('#' + rowId + '-tuesday-val').val() | 0);
    var wed = kendo.parseInt($('#' + rowId + '-wednesday-val').val() | 0);
    var thu = kendo.parseInt($('#' + rowId + '-thursday-val').val() | 0);
    var fri = kendo.parseInt($('#' + rowId + '-friday-val').val() | 0);
    var sat = kendo.parseInt($('#' + rowId + '-saturday-val').val() | 0);

    $.ajax({
        url: '/Fruits/_GetSubtotal',
        type: 'POST',
        data: {
            fruitProductID: fruitId,
            companyID: $('#Company').val(),
            mon: mon,
            tue: tue,
            wed: wed,
            thu: thu,
            fri: fri,
            sat: sat
        },
        success: function (response) {
            if (response.Subtotal != null) {
                var weeklyVolume = mon + tue + wed + thu + fri + sat;
                fruitRow.find('#cost-per-prouct .row-price').text(kendo.toString(response.UnitPrice, "c"));
                fruitRow.find('#cost-per-prouct-mobile .row-price').text(kendo.toString(response.UnitPrice, "c"));
                fruitRow.find('#product-quantity .row-quantity').text(weeklyVolume);
                fruitRow.find('#product-quantity-mobile .row-quantity').text(weeklyVolume);
                fruitRow.find('#total-cost .row-total').text(kendo.toString(response.Subtotal, "c"));
                fruitRow.find('#total-cost-mobile .row-total').text(kendo.toString(response.Subtotal, "c"));
                fruitRow.data('subtotal', response.Subtotal);
                fruitRow.data('volume', weeklyVolume);
                RecalculateFruitTotal();
            }
            console.log(response);
        }
    })
}

function RecalculateFruitTotal()
{
    var totalNet = 0;
    var totalVat = 0;
    var totalVolume = 0;
    $('div.order-products.fruit').each(function () {
        var subtotal = kendo.parseFloat($(this).data('subtotal'));
        var vatRate = kendo.parseFloat($(this).data('vat'));
        var volume = kendo.parseFloat($(this).data('volume'));
        var vatSubtotal = subtotal * vatRate;
        totalNet += subtotal;
        totalVat += vatSubtotal;
        totalVolume += volume;
    });

    $('.fruit-order-area .grand-total-price').each(function () {
        $(this).text(kendo.toString(totalNet, "c"));
    })

    $('.fruit-order-area .grand-total-vat').each(function () {
        $(this).text(kendo.toString(totalVat, "c"));
    })

    $('.fruit-order-area .grand-total-gross').each(function () {
        $(this).text(kendo.toString(totalVat + totalNet, "c"));
    })

    $('.fruit-order-area .grand-total-volume').each(function () {
        $(this).text(totalVolume);
    })

    if (!IsAuthenticated) {
        // Delete the area in quote car if there's no items
        if (!$('#fruit-quotes .order-products.fruit').length) {
            $('#fruit-quotes').remove();
        }

        // To keep prospect's quote between tabs
        if (timeoutFruitFunc != null) clearTimeout(timeoutFruitFunc);
        timeoutFruitFunc = setTimeout(function () {
            SaveFruitOrders(false, 'Draft', true)
        }, 1000);
    }
}

var timeoutFruitFunc;
function ChangeFruitQuantity() {
    RecalculateFruitTotal();   
}

function ClearFruitQty(weekday) {
    $('div.order-products.fruit').each(function () {
        var dataID = $(this).data("id");
        $(this).find('input#' + dataID + '-' + weekday + '-val').val(0).trigger('change');
    })
}

function DeleteFruitRow(itemID) {
    if (!confirm('Remove this product?'))
        return false;
    $(".order-products.fruit[data-id='" + itemID + "']").remove();
    ChangeFruitQuantity();
}

function AddFruitToOrder(that) {
    var fruitID = $(that).data("id");

    if ($(".order-products.fruit[data-fruitid='" + fruitID + "']").length > 0) {
        alert("The chosen product is already in your standing order.");
        return false;
    }
    $.ajax({
        url: '/Fruits/_FruitOrderRow',
        type: 'POST',
        data: {
            fruitProductID: fruitID,
            companyID: $('#Company').val(),
            orderMode: GetOrderMode(),
            date: GetSelectedDate()
        },
        success: function (response) {
            $(response).insertBefore($('.row-grand'));
            $('.save-order ').addClass('blink');
            $('html,body').animate({
                scrollTop: $("#order-area").offset().top
            }, 500);
            $('#ProductOverView').modal('hide');
        }
    });
}

// For Prospects
function AddFruitToQuote(that) {
    var fruitID = $(that).data("id");

    $.ajax({
        url: '/FruitOrders/_AddToQuote',
        type: 'POST',
        data: {
            fruitProductID: fruitID
        },
        success: function (response) {
            $('#ProductOverView').modal('hide');
            ShowQuoteCart();
        }
    });
}

function SaveFruitOrders(submit, orderMode, prospectQtyChange, sendDraftToCustomer, sendEmailToBoth = true, sendEmailToSupplierOnly = false) {
    // Initializations
    submit = submit || false;
    orderMode = orderMode || 'Draft';
    prospectQtyChange = prospectQtyChange || false; // Prospect changing qty doesn't need to check miminum 
    sendDraftToCustomer = sendDraftToCustomer || false;

    if (submit == false && orderMode == 'Draft' && (!prospectQtyChange && !CheckFruitMinimum())) {
        alert('Dear Customer\nPlease note a minimum spend of £' + parseFloat(Math.round(FruitMinimumValue * 100) / 100).toFixed(2) + ' per week is required for Dairy deliveries');
        return false;
    }

    var orders = [];

    $('.order-products.fruit').each(function () {
        var dataID = $(this).data("id");
        var fruitID = $(this).data('fruitid');
        if (fruitID != undefined) {
            var orderItem = {
                ID: dataID,
                FruitID: $(this).data('fruitid'),
                ProductName: $(this).find('#product-name').text(),
                Unit: $(this).find('#product-unit').text(),
                Monday: kendo.parseInt($(this).find('input#' + dataID + '-monday-val').val() | 0),
                Tuesday: kendo.parseInt($(this).find('input#' + dataID + '-tuesday-val').val() | 0),
                Wednesday: kendo.parseInt($(this).find('input#' + dataID + '-wednesday-val').val() | 0),
                Thursday: kendo.parseInt($(this).find('input#' + dataID + '-thursday-val').val() | 0),
                Friday: kendo.parseInt($(this).find('input#' + dataID + '-friday-val').val() | 0),
                Saturday: kendo.parseInt($(this).find('input#' + dataID + '-saturday-val').val() | 0),
                Sunday: 0,
                Price: kendo.parseFloat($(this).data('price')),
                VATRate: kendo.parseFloat($(this).data('vat'))
            };
            orders.push(orderItem);
        }
    });

    $.ajax({
        url: '/FruitOrders/SaveOrders',
        type: 'POST',
        data: {
            orders: JSON.stringify(orders),
            orderMode: orderMode,
            submit: submit,
            companyID: $('#Company').val(),
            prospectToken: $('#Prospect').val(),
            sendDraftToCustomer: sendDraftToCustomer,
            sendEmailToBoth: sendEmailToBoth,
            sendEmailToSupplierOnly: sendEmailToSupplierOnly,
            date: $('#SpecificWeek').val() != undefined ? $('#SpecificWeek').val().substring(0, 10) : null
        },
        beforeSend: !prospectQtyChange ? AjaxBegin : null,
        complete: !prospectQtyChange ? AjaxComplete : null,
        success: function (response) {
            if (response.Success) {
                if (IsAuthenticated) {
                    $('#SavedMessage').show(0).delay(500).hide(0);
                }
                if ($('#Prospect').val() != "" && $('#Prospect').val() != undefined) {
                    alert("Successfully updated the prospect quote");
                }
                else if (submit == true) {
                    if (response.Message !== null && response.Message !== undefined && response.Message !== "" && response.Message !== "null"){
                        alert(response.Message);
                    } else {
                        if (sendEmailToBoth) {
                            alert("Thank you \n The system has sent emails to the customer and the supplier denoting all relevant changes.");
                        } else if (sendEmailToSupplierOnly) {
                            alert("Thank you \n The system has sent emails to the supplier denoting all relevant changes.");
                        } else {
                            alert("Updated.");
                        }
                    }

                    if (orderMode == 'Regular') {
                        window.location.reload();
                    } else {
                        $('#Modal_Prospect').modal('hide');
                        UpdateOrderWeekDates();
                    }
                }
                $('.save-order').removeClass('blink');
                $('.activate-button').addClass('blink');
            } else {
                alert(response.ErrorMessage);
            }
        }
    });
}

function GetFruitOrders() {
    var div = $('#WeeklyOrder');
    $.ajax({
        url: '/Fruits/_WeeklyOrder',
        type: 'POST',
        data: {
            orderMode: $('a[name=OrderType].aisle-action-button-active').data('value'),
            date: $('#SpecificWeek').val().substring(0, 10),
            companyID: $('#Company').val(),
            prospectToken: $('#Prospect').val()
        },
        success: function (response) {
            div.html(response);
            $.when(ChangeFruitQuantity()).done(function () {
                $('.save-order').removeClass('blink');
            });
        }
    });
}

function GetOrderMode() {
    try {
        var activeBtn = $('.aisle-action-button-active')[0];
        if (activeBtn === undefined) {
            return "Regular";
        } else {
            return $(activeBtn).data('value');
        }
    } catch(e) {
        return "Regular";
    }
}

function GetSelectedDate() {
    return $('#SpecificWeek').val() !== undefined ? $('#SpecificWeek').val().substring(0, 10) : null;
}
