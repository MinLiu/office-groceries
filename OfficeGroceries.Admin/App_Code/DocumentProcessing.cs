﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;



public static class DocumentProcessing {


    public static byte[] PdfToPdf(string filepath, Dictionary<string, string> replacements)
    {
        if (!File.Exists(filepath)) throw new Exception("File does not exist");
        
        for (int i = 0; i < 20; i++)
        {
            try
            {
                
                using (FileStream existingFileStream = new FileStream(filepath, FileMode.Open, FileAccess.ReadWrite))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        PdfReader pdfReader = new PdfReader(existingFileStream);
                        PdfStamper stamper = new PdfStamper(pdfReader, ms);
                        AcroFields form = stamper.AcroFields;
                    
                        //Replace Acro Fields ()
                        foreach (var field in replacements)
                        {
                            try
                            {
                                form.SetField(field.Key, field.Value, true);
                                form.SetFieldProperty(field.Key.ToString(), "setfflags", PdfFormField.FF_READ_ONLY, null);
                            }
                            catch{ }
                        }
                    
                        stamper.Close();
                        pdfReader.Close();

                        return ms.ToArray();
                    }
                }


            }
            catch
            {
                System.Threading.Thread.Sleep(500);
            }
        }

        throw new Exception("Failed to open pdf file.");
    }

}
